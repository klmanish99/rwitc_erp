<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem();
echo 'done';exit;
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem(){ 
		$current_date = date('Y-m-d');
		$current_lease_query = $this->query("SELECT * FROM horse_to_owner WHERE owner_share_status = 1 AND parent_owner_id <> 0 AND end_date_of_ownership = '".date('Y-m-d', strtotime('-1 day', strtotime($current_date)))."' ",$this->conn);
		if($current_lease_query->num_rows > 0){
			foreach ($current_lease_query->rows as $clkey => $clvalue) {
				$exist_owner_lease_query = $this->query("SELECT * FROM horse_to_owner WHERE from_owner_id = '".$clvalue['parent_owner_id']."' AND horse_id = '".$clvalue['horse_id']."' AND owner_share_status = 0",$this->conn);
				if($exist_owner_lease_query->num_rows > 0){
					$existvalue =$exist_owner_lease_query->row; 
					//foreach ($exist_owner_lease_query->rows as $existkey => $existvalue) {
						$this->query("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$existvalue['horse_id']."', from_owner = '".$this->escape($existvalue['from_owner'], $this->conn)."',from_owner_id = '".$existvalue['from_owner_id']."', to_owner = '".$this->escape($existvalue['from_owner'], $this->conn)."',to_owner_id = '".$existvalue['from_owner_id']."',owner_percentage ='".$existvalue['owner_percentage']."',ownership_type ='".$existvalue['ownership_type']."' ,date_of_ownership ='".date('Y-m-d')."',remark_horse_to_owner = '".$this->escape($existvalue['remark_horse_to_owner'], $this->conn)."' , owner_color = '".$this->escape($existvalue['owner_color'], $this->conn)."'" ,$this->conn);
						
					//}
				}
			}
			$this->query("UPDATE `horse_to_owner` SET owner_share_status = 0 WHERE horse_to_owner_id = '".$clvalue['horse_to_owner_id']."'  ",$this->conn);
			
		}


	}

	public function token($length = 32) {
		// Create random token
		$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		
		$max = strlen($string) - 1;
		
		$token = '';
		
		for ($i = 0; $i < $length; $i++) {
			$token .= $string[mt_rand(0, $max)];
		}	
		
		return $token;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>