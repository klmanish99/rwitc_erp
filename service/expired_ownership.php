<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem();
echo 'done';exit;
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem(){ 

		$allhorses = $this->query("SELECT horse_id FROM `horse_to_owner` WHERE 1=1 GROUP BY horse_id " ,$this->conn);
		if ($allhorses->num_rows > 0) {
			foreach ($allhorses->rows as $akey => $avalue) {
				$current_owners = $this->query("SELECT horse_to_owner_id, end_date_of_ownership, owner_percentage, grandpa_id, horse_id,entry_count FROM `horse_to_owner` WHERE horse_id = '".$avalue['horse_id']."' AND owner_share_status = 1 ",$this->conn);
				// echo 'current owners';
				// echo'<pre>';
				// print_r($current_owners->rows);

				foreach ($current_owners->rows as $ckey => $cvalue) {
					$date_added = date('Y-m-d');
					$current_date = date('Y-m-d');
					if(($current_date > $cvalue['end_date_of_ownership']) && $cvalue['end_date_of_ownership'] != '0000-00-00'){
						
						$owner_per = $cvalue['owner_percentage'];
						// echo'<br />';
						// echo 'own per';
						// echo'<pre>';
						// print_r($owner_per);

						$this->query("UPDATE `horse_to_owner` SET owner_share_status = 0 ,cancel_lease_status = 'Cancel' WHERE horse_to_owner_id = '".$cvalue['horse_to_owner_id']."' AND horse_id = '".$avalue['horse_id']."' ",$this->conn);
						// echo'<br />';
						// echo 'update';
						// echo'<pre>';
						// print_r("UPDATE `horse_to_owner` SET owner_share_status = 0 ,cancel_lease_status = 'Cancel' WHERE horse_to_owner_id = '".$cvalue['horse_to_owner_id']."' AND horse_id = '".$avalue['horse_id']."' ");

						$grand_parent_datas = $this->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id = '".$cvalue['grandpa_id']."' ",$this->conn)->row;
						// echo'<br />';
						// echo"grandpa id";
						// echo'<pre>';
						// print_r($grand_parent_datas);


						$pre_own_datas = $this->query("SELECT * FROM `horse_to_owner` WHERE to_owner_id = '".$grand_parent_datas['to_owner_id']."' AND owner_share_status = 1 AND  horse_id = '".$avalue['horse_id']."' AND owner_percentage < 100 AND ownership_type = 'Sale' ",$this->conn);
						// echo'<br />';
						// echo"pre_own_datas";
						// echo'<pre>';
						// print_r("SELECT * FROM `horse_to_owner` WHERE to_owner_id = '".$grand_parent_datas['to_owner_id']."' AND owner_share_status = 1 AND  horse_id = '".$avalue['horse_id']."' AND owner_percentage < 100 ");
						// print_r($pre_own_datas);


						if($pre_own_datas->num_rows > 0){
							$this->query("UPDATE `horse_to_owner` SET owner_share_status = 0 WHERE horse_to_owner_id = '".$pre_own_datas->row['horse_to_owner_id']."' AND horse_id = '".$avalue['horse_id']."' ",$this->conn);
							$owner_per = $pre_own_datas->row['owner_percentage'] + $cvalue['owner_percentage'];
						}

						// echo'<br />';
						// echo'own per';
						// echo'<pre>';

						// print_r($owner_per);


						$entry_count = $cvalue['entry_count'] + 1;
						$this->query("INSERT INTO `horse_to_owner` SET  
						`horse_id` = '" . $grand_parent_datas['horse_id'] . "',
						`horse_group_id` = '" . $grand_parent_datas['horse_group_id'] . "',
						`trainer_id` = '" . $grand_parent_datas['trainer_id'] . "',
						`ownership_type` = '".$grand_parent_datas['ownership_type']."',
						`parent_group_id` = '".$grand_parent_datas['horse_group_id']."',
						`date_of_ownership` = '" . $grand_parent_datas['date_of_ownership'] . "',
						`end_date_of_ownership` = '" . $grand_parent_datas['end_date_of_ownership'] . "',
						`remark_horse_to_owner` = '" . $grand_parent_datas['remark_horse_to_owner'] . "',
						`owner_percentage` = '" . $owner_per . "',
						`to_owner` = '" . $grand_parent_datas['to_owner'] . "',
						`to_owner_id` = '" . $grand_parent_datas['to_owner_id'] . "',
						`contengency` = '" . $grand_parent_datas['contengency'] . "',
						`cont_percentage` = '" . $grand_parent_datas['cont_percentage'] . "',
						`cont_amount` = '" . $grand_parent_datas['cont_amount'] . "',
						`cont_place` = '" . $grand_parent_datas['cont_place'] . "',
						`win_gross_stake` = '" . $grand_parent_datas['win_gross_stake'] . "',
						`win_net_stake` = '" . $grand_parent_datas['win_net_stake'] . "',
						`millionover` = '" . $grand_parent_datas['millionover'] . "',
						`millionover_amt` = '" . $grand_parent_datas['millionover_amt'] . "',
						`grade1` = '" . $grand_parent_datas['grade1'] . "',
						`grade2` = '" . $grand_parent_datas['grade2'] . "',
						`grade3` = '" . $grand_parent_datas['grade3'] . "',
						`owner_share_status` = '1',
						`entry_count` = '".$entry_count."',
						`grandpa_id` = '".$cvalue['grandpa_id']."',
						`date_added` = '".$date_added."',
						`user_id` = '".$this->session->data['user_id']."',
						`provisional_ownership` = '".$grand_parent_datas['provisional_ownership']."'
						",$this->conn);
					}
				}


				$history_owners = $this->query("SELECT `batch_id` FROM `horse_to_owner_history` WHERE horse_id = '".$avalue['horse_id']."' ORDER BY id DESC LIMIT 1 ",$this->conn);
				$batch_id = 1;
				if($history_owners->num_rows > 0){
					$batch_id = $history_owners->row['batch_id'] + 1;
					$this->query("UPDATE `horse_to_owner_history` SET status = 1 WHERE batch_id = '".$history_owners->row['batch_id']."' ",$this->conn);
				}

				$allCurrOwners = $this->query("SELECT horse_id, horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$avalue['horse_id']."' AND owner_share_status = 1 ",$this->conn);
				if($allCurrOwners->num_rows > 0){
					foreach ($allCurrOwners->rows as $ohkey => $ohvalue) {
						$this->query("INSERT INTO `horse_to_owner_history` SET 
							`horse_id` = '" . $ohvalue['horse_id'] . "',
							`batch_id` = '" . $batch_id . "',
							`horse_to_owner_id` = '" . $ohvalue['horse_to_owner_id'] . "',
							`status` = 0
							",$this->conn);
					}
				}
			}
		}
	}

	public function token($length = 32) {
		// Create random token
		$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		
		$max = strlen($string) - 1;
		
		$token = '';
		
		for ($i = 0; $i < $length; $i++) {
			$token .= $string[mt_rand(0, $max)];
		}	
		
		return $token;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>
