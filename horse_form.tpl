<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-horse" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<link type="text/css" href="view/stylesheet/myform.css" rel="stylesheet" media="screen" />
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-horse" class="form-horizontal">
					<ul class="nav nav-tabs">
						<li id="basic_tab_class" class="active"><a href="#tab-horse_basic" data-toggle="tab"><?php echo "Horse Basic Details"; ?></a></li>
						<li><a href="#tab-trainer" data-toggle="tab"><?php echo "Trainer Details"; ?></a></li>
						<li id="ownership_tab_class"><a href="#tab-ownership" data-toggle="tab"><?php echo "Ownership/Lease"; ?></a></li>
						<?php if($horse_id != '') { ?> 
							<li><a href="#tab-BanDetails" data-toggle="tab"><?php echo "Ban Details"; ?></a></li>
							<li><a href="#tab-Change-Equipment" data-toggle="tab"><?php echo "Change Equipment"; ?></a></li>
							<li><a href="#tab-Shoeing-and-bits" data-toggle="tab"><?php echo "Shoeing and bits"; ?></a></li>
							<li><a href="#tab-Stakes-Earned-Outstation" data-toggle="tab"><?php echo "Stakes Earned/Outstation"; ?></a></li>
						<?php } ?>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab-horse_basic">
							<div class="form-group" >
								<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Horse Name :"; ?></label>
								<div class="col-sm-3" >
									<input type="text" name="horse_name" value="<?php echo $horse_name; ?>" placeholder="Horse Name" id="input-horse" class="form-control" tabindex="1"/>
									<input type="hidden" name="horse_id" value="<?php echo $horse_id; ?>" />
									<?php if (isset($valierr_horse_name)) { ?><span class="errors"><?php echo $valierr_horse_name; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-top"><?php echo "Official Name Change"; ?></label>
								<div class="col-sm-2">
								  <div class="checkbox" >
										<label>
											<input type="hidden" name="official_name_change" value="0" />
										  	<input type="checkbox" name="official_name_change" value="1"  id="official_name_change" <?php if ($official_name_change == 1){ echo 'checked="checked"'; } ?> class="form-control" />
										  </label>
									</div>
								</div>
							</div>
							<div class="form-group" id="hourse_names_changes" style="display: none">
								<label class="col-sm-2 control-label" for="input-horse_change"><b style="color: red">*</b><?php echo "Horse Late Name :"; ?></label>
								<div class="col-sm-3" >
									<input type="text" name="changehorse_name" value="<?php echo $horse_name; ?>" placeholder="Change Horse Name" id="change_horse_name" class="form-control" tabindex="1" readonly="readonly" />
									<?php if (isset($valierr_change_horse_name)) { ?><span class="errors"><?php echo $valierr_change_horse_name; ?></span><?php } ?> 
								</div>
								<label class="col-sm-2 control-label" for="input-date-available"><?php echo "Change Horse Date:"; ?></label>
								<div class="col-sm-3">
									<div class="input-group input-change_horse_dates">
										<input type="text" name="change_horse_dates" value="<?php echo $change_horse_dates; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="change_horse_date" class="form-control" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<?php if (isset($valierr_change_horse_date)) { ?><span class="errors" id="error_change_horse_date_post"><?php echo $valierr_change_horse_date; ?></span><?php } ?>
									<span style="display: none;color: red;font-weight: bold;" id="error_change_horse_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-passport"><?php echo "Passport/Life Number:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="passport_no" value="<?php echo $passport_no; ?>" placeholder="<?php echo "Passport/Life Number"; ?>" id="input-passport" class="form-control"  tabindex="2" />
									<input type="hidden" name="passport_no_hidden" value="<?php echo $passport_no; ?>" />
									<?php if (isset($valierr_passport_no)) { ?><span class="errors"><?php echo $valierr_passport_no; ?></span><?php } ?>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-date-available"><?php echo "Registration Date:"; ?></label>
								<div class="col-sm-3">
									<div class="input-group date input-registeration_date">
										<input type="text" name="registeration_date" value="<?php echo $registeration_date; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="registeration_date" class="form-control" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<?php if (isset($valierr_registeration_date)) { ?><span class="errors" id="error_registeration_date_post"><?php echo $valierr_registeration_date; ?></span><?php } ?>
									<span style="display: none;color: red;font-weight: bold;" id="error_registeration_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
								</div>
								<label class="col-sm-2 control-label" for="input-registration_auth"><b style="color: red">*</b><?php echo "Registration Auth:"; ?></label>
								<div class="col-sm-3">
									<select name="registerarton_authentication" id="registerarton_authentication_id" class="form-control" tabindex="5">
										<option value="" >Please Select</option>
										<?php foreach ($regiteration_authenticaton_datas as $reg_aut_key => $reg_aut_value) { ?>
										<?php if ($reg_aut_value == $registerarton_authentication) { ?>
										<option value="<?php echo $reg_aut_value; ?>" selected="selected"><?php echo $reg_aut_value; ?></option>
										<?php } else { ?>
										<option value="<?php echo $reg_aut_value; ?>"><?php echo $reg_aut_value; ?></option>
										<?php } ?>
										<?php } ?>
								  	</select>
									<?php if (isset($valierr_registerarton_authentication)) { ?><span class="errors"><?php echo $valierr_registerarton_authentication; ?></span><?php } ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-sire"><b style="color: red">*</b><?php echo "Sire/ Nat"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="sire" value="<?php echo $sire; ?>" placeholder="<?php echo "Sire/ Nat"; ?>" id="input-sire" class="form-control" tabindex="6"/>
									<?php if (isset($valierr_sire)) { ?><span class="errors"><?php echo $valierr_sire; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-dam_nat"><b style="color: red">*</b><?php echo "Dam/ Nat:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="dam" value="<?php echo $dam; ?>" placeholder="<?php echo "Dam/ Nat"; ?>" id="input-dam_nat" class="form-control" tabindex="7"/>
									<?php if (isset($valierr_dam)) { ?><span class="errors"><?php echo $valierr_dam; ?></span><?php } ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-micro_chip1"><?php echo "Micro Chip no 1:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="chip_no_one" value="<?php echo $chip_no_one; ?>" placeholder="<?php echo "Micro Chip no 1"; ?>" id="input-micro_chip1" class="form-control" tabindex="8"/>
									<?php if (isset($valierr_chip_no_one)) { ?><span class="errors"><?php echo $valierr_chip_no_one; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-micro_chip_2"><?php echo "Micro Chip no 2:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="chip_no_two" value="<?php echo $chip_no_two; ?>" placeholder="<?php echo "Micro Chip no 2"; ?>" id="input-micro_chip_2" class="form-control" tabindex="9" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-horse"><?php echo "Arrival Charges to be Paid:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="arrival_charges_to_be_paid" value="<?php echo $arrival_charges_to_be_paid; ?>" id = "arrival_charges_to_be_paid" placeholder="<?php echo "Arrival Charges to be Paid"; ?>"  class="form-control"  tabindex="10"/>
									<?php if (isset($valierr_arrival_charges_to_be_paid)) { ?><span class="errors"><?php echo $valierr_arrival_charges_to_be_paid; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Foal Date:"; ?></label>
								<div class="col-sm-3">
									<div class="input-group date input-date_foal_date">
										<input type="text" name="date_foal_date" value="<?php echo $date_foal_date; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_foal_date" class="form-control" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<span style="display: none;color: red;font-weight: bold;" id="error_date_foal_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
									<?php if (isset($valierr_foal_date)) { ?><span class="errors" id="error_date_foal_date_post"><?php echo $valierr_foal_date; ?></span><?php } ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-color"><b style="color: red">*</b><?php echo "Color:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="color" value="<?php echo $color; ?>" placeholder="<?php echo "Color"; ?>" id="input-color" class="form-control"  tabindex="13"/>
									<?php if (isset($valierr_color)) { ?><span class="errors"><?php echo $valierr_color; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-origin"><b style="color: red">*</b><?php echo "Origin:"; ?></label>
								<div class="col-sm-3">
									<select name="origin" id="input_origin" class="form-control"  tabindex="14">
										<option value="" >Please Select</option>
										<?php foreach ($origins as $originkey => $originvalue) { ?>
										<?php if ($originkey == $origin) { ?>
										<option value="<?php echo $originvalue; ?>" selected="selected"><?php echo $originvalue; ?></option>
										<?php } else { ?>
										<option value="<?php echo $originvalue; ?>" ><?php echo $originvalue; ?></option>
										<?php } ?>
										<?php } ?>
								  </select>
								<?php if (isset($valierr_origin)) { ?><span class="errors"><?php echo $valierr_origin; ?></span><?php } ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-sex"><b style="color: red">*</b><?php echo "Sex:"; ?></label>
								<div class="col-sm-3">
								<select name="sex" id="input_sex" class="form-control"  tabindex="15">
									<option value="" >Please Select</option>
									<?php foreach ($sexs as $sexkey =>  $sexvalue) { ?>
									<?php if ($sexkey == $sex) { ?>
									<option value="<?php echo $sexkey ?>" selected="selected"><?php echo $sexvalue; ?></option>
									<?php } else { ?>
									<option value="<?php echo $sexkey; ?>"><?php echo $sexvalue; ?></option>
									<?php } ?>
									<?php } ?>
								 </select>
								<?php if (isset($valierr_sex)) { ?><span class="errors"><?php echo $valierr_sex; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-horse"><?php echo "Age:"; ?></label>
								<div class="col-sm-1">
									<input type="Number" name="age" value="<?php echo $age ?>" placeholder="Age" id="input-age" class="form-control"  tabindex="12" readonly="readonly" />
									<?php if (isset($valierr_age)) { ?><span class="errors"><?php echo $valierr_age; ?></span><?php } ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-stud_farm"><?php echo "Stud Farm:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="stud_form" value="<?php echo $stud_form; ?>" placeholder="<?php echo "Stud Farm"; ?>" id="input-stud_farm" class="form-control"  tabindex="16"/>
									<?php if (isset($valierr_stud_form)) { ?><span class="errors"><?php echo $valierr_stud_form; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-rating"><?php echo "Rating:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="rating" value="<?php echo $rating; ?>" placeholder="<?php echo "Rating"; ?>" id="input-rating" class="form-control" readonly  tabindex="18"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-breeder"><?php echo "Breeder:"; ?></label>
								<div class="col-sm-8">
									<input type="text" name="breeder" value="<?php echo $breeder; ?>" placeholder="<?php echo "Breeder"; ?>" id="input-breeder" class="form-control"  tabindex="19" />
									<?php if (isset($valierr_breeder)) { ?><span class="errors"><?php echo $valierr_breeder; ?></span><?php } ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-saddle_no"><?php echo "Saddle No:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="saddle_no" value="<?php echo $saddle_no; ?>" placeholder="<?php echo "Saddle No"; ?>" id="input-saddle_no" class="form-control"  tabindex="20" />
									<?php if (isset($valierr_saddle_no)) { ?><span class="errors"><?php echo $valierr_saddle_no; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-octroi"><?php echo "Octroi Details:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="octroi_details" value="<?php echo $octroi_details; ?>" placeholder="<?php echo "Octroi Details"; ?>" id="input-octroi" class="form-control"  tabindex="21" />
									<?php if (isset($valierr_octroi_details)) { ?><span class="errors"><?php echo $valierr_octroi_details; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-awbi"><?php echo "AWBI Registration No:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="awbi_registration_no" value="<?php echo $awbi_registration_no; ?>" placeholder="<?php echo "AWBI Registration No:"; ?>" id="input-awbi" class="form-control" tabindex="22"/>
									<?php if (isset($valierr_awbi_registration_no)) { ?><span class="errors"><?php echo $valierr_awbi_registration_no; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-file"><?php echo "" ?></label>
								<div class="col-sm-3"  >
									<input readonly="readonly" type="text" name="awbi_registration_file" value="<?php echo $awbi_registration_file; ?>" placeholder="Awbi Registration File" id="input-other_document_1" class="form-control" />
									<input type="hidden" name="awbi_registration_file_source" value="<?php echo $awbi_registration_file_source; ?>" id="input-other_document_1_source" class="form-control" />
									<span class="input-group-btn" style="padding-top: 2%;">
			  							<button type="button" id="button-other_document_1" style = "border-radius: 3px;" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Document'; ?></button>
										<span id="button-other_document_1_new"></span>
										<?php if($awbi_registration_file_source != ''){ ?>
											<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="awbi_registration_file_source" href="<?php echo $awbi_registration_file_source; ?>">View Document</a>
										<?php } ?>
									</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "St Stall Certificate:"; ?></label>
								<div class="col-sm-3">
									<div class="input-group date input-date_std_stall_certificate">
										<input type="text" name="date_std_stall_certificate" value="<?php echo $date_std_stall_certificate; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_std_stall_certificate" class="form-control" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<?php if (isset($valierr_std_stall_certificate_date)) { ?><span class="errors" id="error_date_std_stall_certificate_post"><?php echo $valierr_std_stall_certificate_date; ?></span><?php } ?>
									<span style="display: none;color: red;font-weight: bold;" id="error_date_std_stall_certificate" class="error"><?php echo 'Please Select Valid Date'; ?></span>
								</div>
								<label class="col-sm-2 control-label" for="input-id_by_brand"><?php echo "ID By Brand:"; ?></label>
								<div class="col-sm-3">
									<textarea rows="2" name="Id_by_brand"  placeholder="ID By Brand" id="input-id_by_brand" class="form-control" tabindex="25"><?php echo $Id_by_brand;?></textarea>
									<?php if (isset($valierr_Id_by_brand)) { ?><span class="errors"><?php echo $valierr_Id_by_brand; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "St Stall Remark:"; ?></label>
								<div class="col-sm-8">
									<input type="text" name="std_stall_remarks" value="<?php echo $std_stall_remarks; ?>" placeholder="<?php echo "St Stall Remark"; ?>" id="input-stall_certificate" class="form-control" tabindex="26" />
									<?php if (isset($valierr_std_stall_remarks)) { ?><span class="errors"><?php echo $valierr_std_stall_remarks; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Remarks:"; ?></label>
								<div class="col-sm-8">
									<textarea rows="10" name="horse_remarks" placeholder="Remarks" id = "horse_remarks"  class="form-control" tabindex="27"><?php echo $horse_remarks; ?></textarea>
									<?php if (isset($valierr_horse_remarks)) { ?><span class="errors"><?php echo $valierr_horse_remarks; ?></span><?php } ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-trainer">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-trainer_name"><b style="color: red">*</b><?php echo "Trainer Name:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="trainer_name"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $trainer_name; ?>" id="input-trainer_name" class="form-control" />
									<input type="hidden" name="trainer_id"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $trainer_id; ?>" id="trainer_id" class="form-control" />
									<?php if (isset($valierr_trainer_name)) { ?><span class="errors"><?php echo $valierr_trainer_name; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-trainer_code"><b style="color: red">*</b><?php echo "Trainer Code:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="trainer_codes_id" value="<?php echo $trainer_codes_id; ?>" placeholder="<?php echo "Trainer Name"; ?>" id="trainer_codes_id" class="form-control"  readonly/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-date_of_charge"><b style="color: red">*</b><?php echo "Date Of Charge:"; ?></label>
								<div class="col-sm-3">
									<div class="input-group date input-date_charge_trainer">
										<input type="text" name="date_charge_trainer" value="<?php echo $date_charge_trainer; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_charge_trainer" class="form-control" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<span style="display: none;color: red;font-weight: bold;" id="error_date_charge_trainer" class="error"><?php echo 'Please Select Valid Date'; ?></span>
									<?php if (isset($valierr_date_charge_trainer)) { ?><span class="errors" id="error_date_charge_trainer_post"><?php echo $valierr_date_charge_trainer; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-trainer_name"><b style="color: red">*</b><?php echo "Arrival Time:"; ?></label>
								<div class="col-sm-3">
									<select name="arrival_time_trainers" id="arrival_time_trainer" class="form-control">
										<option value="" >Please Select</option>
										<?php foreach ($arrival_time_trainer as $arrival_time_value) { ?>
											<?php if ($arrival_time_value == $arrival_time_trainers) { ?> 
												<option value="<?php echo $arrival_time_value; ?>"  selected="selected" ><?php echo $arrival_time_value; ?></option>
									    	<?php } else { ?> 
												<option value="<?php echo $arrival_time_value; ?>"><?php echo $arrival_time_value ?></option>
										 	<?php } ?> 
										<?php } ?>
								 	</select>
									<?php if (isset($valierr_arrival_time_trainer)) { ?><span class="errors"><?php echo $valierr_arrival_time_trainer; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Extra Narration:"; ?></label>
								<div class="col-sm-8">
									<textarea rows="4" name="extra_narrration_trainer"  id ="extra_narrration_trainer" placeholder=""  class="form-control"><?php echo $extra_narrration_trainer; ?></textarea>
									<?php if (isset($valierr_extra_narrration_trainer)) { ?><span class="errors"><?php echo $valierr_extra_narrration_trainer; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-horse"><?php echo "Left Charge:"; ?></label>
								<div class="col-sm-3">
									<div class="input-group date input-date_left_trainer">
										<input type="text" name="date_left_trainer" value="<?php echo $date_left_trainer; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_left_trainer" class="form-control" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<span style="display: none;color: red;font-weight: bold;" id="error_date_left_trainer" class="error"><?php echo 'Please Select Valid Date'; ?></span>
									<?php if (isset($valierr_date_left_trainer)) { ?><span class="errors" id="error_date_left_trainer_post"><?php echo $valierr_date_left_trainer; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Extra Narration 2:"; ?></label>
								<div class="col-sm-8">
									<textarea rows="4" name="extra_narrration_two_trainer"  id = "extra_narrration_two_trainer" placeholder=""  class="form-control"><?php echo $extra_narrration_two_trainer; ?></textarea>
									<?php if (isset($valierr_extra_narrration_two_trainer)) { ?><span class="errors"><?php echo $valierr_extra_narrration_two_trainer; ?></span><?php } ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-ownership">
							<form id= "ownership_form_id">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-from_owner"><?php echo "From Owner:"; ?></label>
									<div class="col-sm-3">
										<input type="text" name="frm_owner"  placeholder="From Owner" id="frm_owner_id" class="form-control from_owner" />
										<input type="hidden" name="frm_owner_hidden"  placeholder="From Owner" id="frm_owner_id_hidden" class="form-control"  />
										<span style="display: none;color: red;font-weight: bold;" id="error_frm_owner" class="error"><?php echo 'Please Select From Owner'; ?></span>
									</div>
									<label class="col-sm-2 control-label" for="input-to_owner"><?php echo "To Owner:"; ?></label>
									<div class="col-sm-3">
										<input type="text" name="to_owner"  placeholder="To Owner" id="to_owner_id" class="form-control"  />
										<input type="hidden" name="to_owner_hidden"  placeholder="To Owner" id="to_owner_id_hidden" class="form-control"  />
										<span style="display: none;color: red;font-weight: bold;" id="error_to_owner" class="error"><?php echo 'Please Select To Owner'; ?></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-to_owner"><?php echo "Owner Percentage(%):"; ?></label>
									<div class="col-sm-3">
										<input type="Number" name="owner_percentage"  placeholder="Owner Percentage(%)" id="owner_percentage_id" class="form-control" />
										<span style="display: none;color: red;font-weight: bold;" id="error_owner_percentage" class="error"><?php echo 'Please Enter Valid Owner Percentage'; ?></span>
									</div>
									<label class="col-sm-2 control-label" for="input-trainer_name"><b style="color: red">*</b><?php echo "Ownership Type:"; ?></label>
									<div class="col-sm-3">
										<select name="ownership_type_name" id="ownership_type_id" class="form-control">
											<option value="" >Please Select</option>	
											<?php foreach ($ownerships_types as $o_typekey => $o_typevalue) { ?>
												<option value="<?php echo $o_typevalue; ?>" ><?php echo $o_typevalue; ?></option>
											<?php } ?>
									  	</select>
										<span style="display: none;color: red;font-weight: bold;" id="error_ownership_type" class="error"><?php echo 'Please Select Ownership Type'; ?></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-date-available"><?php echo "Date Of Ownership:"; ?></label>
									<div class="col-sm-3">
										<div class="input-group date input-date_ownership">
											<input type="text" name="date_ownership" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_ownership" class="form-control" />
											<span class="input-group-btn">
												<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
										<span style="display: none;color: red;font-weight: bold;" id="error_datawonership" class="error"><?php echo 'Please Select Valid Date'; ?></span>
									</div>
									<label class="col-sm-2 control-label" for="input-date-available"><?php echo "End Date of Ownership:"; ?></label>
									<div class="col-sm-3">
										<div class="input-group date input-end_date_ownership">
											<input type="text" name="end_date_ownership" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="end_date_ownership" class="form-control" />
											<span class="input-group-btn">
												<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
										<span style="display: none;color: red;font-weight: bold;" id="error_end_date_ownership" class="error"><?php echo 'Please Select Valid Date'; ?></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Remark:"; ?></label>
									<div class="col-sm-8">
										<textarea rows="2" name="remark_description_owner" placeholder="" id="ownership_remarks" class="form-control"></textarea>
										<span style="display: none;color: red;font-weight: bold;" id="error_ownership_remarks" class="error"><?php echo 'Please Enter Remark'; ?></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-stock-status">Color:</label>
									<div class="col-sm-2">
									  <div class="checkbox" >
										<label>
										  <input type="checkbox" name="color_owner_jk_id" value="1"  id="color_owner_jk_id"  class="form-control" />
										  </label>
									  </div>
									</div>
									<div class="col-sm-8" >
										<input type="hidden" name="ownership_hidden_id" value="<?php echo $ownership_hidden_id; ?>" id="ownership_hidden_id" class="form-control" />
										<input type="hidden" name="id_hidden_saveownership" value="" id="id_hidden_saveownership" class="form-control" />
										<input type="hidden" name="idowner_old_increment_id" value="" id="idowner_old_increment_id" class="form-control" />
										<button type="button" style="margin-left: 100px;" class="btn btn-primary btn-lg" id="savefunctionownership"  onclick="SaveOwnershp()">Save</button>
									</div>
								</div>
							</form>
							<div class="form-group">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
											  <td class="text-left"><?php echo "From Owner"; ?></td>
											  <td class="text-left"><?php echo "To Owner"; ?></td>
											  <td class="text-left"><?php echo "Owner Percentage(%)"; ?></td>
											  <td class="text-left"><?php echo "Ownership Type"; ?></td>
											  <td class="text-left"><?php echo "Date Of Ownership"; ?></td>
											  <td class="text-left"><?php echo "End Date of Ownership"; ?></td>
											  <td class="text-left"><?php echo "Remark"; ?></td>
											  <td class="text-left"><?php echo "Color"; ?></td>
											  <td class="text-left"><?php echo "Action"; ?></td>
											</tr>
										</thead>
										<tbody id = "ownershipdeatilsbody">
											<?php if($owners_olddatas) { echo'<pre>';print_r($bandatas); exit(); ?>
												<?php foreach($owners_olddatas as $okey => $ovalue) { ?>
													<tr id='par<?php echo $okey ?>'>
														<td>
															<span id="from_owner_historys_<?php echo $okey ?>"><?php echo $ovalue['from_owner'] ?></span>
															<input type= "hidden" name="ownerdatas[<?php echo $okey ?>][from_owner]" id="from_owner_history_<?php echo $okey ?>"  value="<?php echo $ovalue['from_owner'] ?>">
															<input type= "hidden"  name="ownerdatas[<?php echo $okey ?>][from_owner_id]" id="from_owner_id_history_<?php echo $okey ?>"  value="<?php echo $ovalue['from_owner_id'] ?>">
														</td>
														<td>
															<span id="to_owner_historys_<?php echo $okey ?>"><?php echo $ovalue['to_owner'] ?></span>
															<input type= "hidden" name="ownerdatas[<?php echo $okey?>][to_owner]" id="to_owner_history_<?php echo $okey ?>"  value="<?php echo $ovalue['to_owner'] ?>">
															<input type= "hidden"  name="ownerdatas[<?php echo $okey?>][to_owner_id]" id="to_owner_idhistory_<?php echo $okey ?>"  value="<?php echo $ovalue['to_owner_id'] ?>">
														</td>
														<td>
															<span id="owner_percentage_historys_<?php echo $okey ?>" ><?php echo $ovalue['owner_percentage'] ?></span>
															<input type= "hidden"  name="ownerdatas[<?php echo $okey ?>][owner_percentage]" id="owner_percentage_history_<?php echo $okey ?>"  value="<?php echo $ovalue['owner_percentage'] ?>">
														</td>
														<td>
															<span id="ownership_type_historys_<?php echo $okey ?>" ><?php echo $ovalue['ownership_type'] ?></span>
															<input type= "hidden"  name="ownerdatas[<?php echo $okey ?>][ownership_type]" id="ownership_type_history_<?php echo $okey ?>"  value="<?php echo $ovalue['ownership_type'] ?>">
														</td>

														<td>
															<span id="date_of_ownership_historys_<?php echo $okey ?>" ><?php echo date('d-m-Y', strtotime($ovalue['date_of_ownership'])); ?></span>
															<input type= "hidden"  name="ownerdatas[<?php echo $okey ?>][date_of_ownership]" id="date_of_ownership_history_<?php echo $okey ?>"  value="<?php echo date('d-m-Y', strtotime($ovalue['date_of_ownership'])); ?>">
														</td>
														<td>
															<span id="end_date_of_ownership_historys_<?php echo $okey ?>" ><?php echo date('d-m-Y', strtotime($ovalue['end_date_of_ownership'])); ?></span>
															<input type= "hidden"  name="ownerdatas[<?php echo $okey ?>][end_date_of_ownership]" id="end_date_of_ownership_history_<?php echo $okey ?>"  value="<?php echo date('d-m-Y', strtotime($ovalue['end_date_of_ownership'])); ?>">
														</td>
														
														<td>
															<span id="remark_historys_<?php echo $okey ?>" ><?php echo $ovalue['remark_horse_to_owner'] ?></span>
															<input type= "hidden"  name="ownerdatas[<?php echo $okey?>][remark_horse_to_owner]" id="remark_history_<?php echo $okey ?>"  value="<?php echo $ovalue['remark_horse_to_owner'] ?>">
														</td>
														<td>
															<span id="color_owner_historys_<?php echo $okey ?>" ><?php echo $ovalue['owner_color'] ?></span>
															<input type= "hidden"  name="ownerdatas[<?php echo $okey?>][owner_color]" id="color_owner_history_<?php echo $okey ?>"  value="<?php echo $ovalue['owner_color'] ?>">
														</td>
														<td> <a onclick='updateownerhistory(<?php echo $okey ?>);' class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
													</tr>
												<?php } ?>
                   	 						<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div> <!-- closed-ownership -->
						<div class="tab-pane" id="tab-BanDetails">
							<div class="col-sm-11">
								<h4>Ban Details</h4>
							</div>
							<div style="float: right;padding-bottom: 10px;">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"  onclick="closeaddbuu1()"><i class="fa fa-plus-circle"></i></button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="id_hidden_band" value="<?php echo $id_hidden_band ?>" id="id_hidden_band" class="form-control" />

								<div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Ban</h4>
											</div>
											<div class="modal-body">
												<div class="form-group">
												<label class="col-sm-2 control-label" for="input-club"><b style="color: red">*</b><?php echo "Club:"; ?></label>
													<div class="col-sm-8">
														<select  name="club" placeholder="Club" id="input-club" class="form-control">
															<option value="" selected="selected" disabled="disabled" >Please Select</option>
															<?php foreach ($clubs as $ckey => $cvalue) { ?>
															<option value="<?php echo $cvalue; ?>" ><?php echo $cvalue ?></option>
															<?php } ?>
													  </select>
													  <span style="display: none;color: red;font-weight: bold;" id="error_club_ban" class="error"><?php echo 'Please Select Club'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Authority:"; ?></label>
													<div class="col-sm-8">
														<select name="authority_ban" id="authority_ban_details" class="form-control">
															<option value="" selected="selected" disabled="disabled" >Please Select</option>	
															<?php foreach ($authorty_bans as $authorutykey => $authorutyvalue) { ?>
															<option value="<?php echo $authorutyvalue; ?>" ><?php echo $authorutyvalue ?></option>
															<?php } ?>
													  </select>
													  <span style="display: none;color: red;font-weight: bold;" id="error_authority_ban" class="error"><?php echo 'Please Select Authority'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Start Date:"; ?></label>
													<div class="col-sm-8">
														<div class="input-group date input-start_date_ban">
															<input type="text" name="date_start_date_ban" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="start_date_ban" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_date_start_date_ban" class="error"><?php echo 'Please Select Valid Date'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><?php echo "End Date:"; ?></label>
													<div class="col-sm-8">
														<div class="input-group date input-end_date_ban">
															<input type="text" name="date_end_date_ban" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="end_date_ban" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_date_end_date_ban" class="error"><?php echo 'Please Select Valid Date'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Reason:"; ?></label>
													<div class="col-sm-8">
														<textarea rows="2" name="reason_description_ban" placeholder="" id="reason_ban" class="form-control"></textarea>
													  	<span style="display: none;color: red;font-weight: bold;" id="error_reason_ban" class="error"><?php echo 'Please Enter Reason'; ?></span>
													</div>
												</div>
											</div>
										  <div class="modal-footer">
											<input type="hidden" name="id_hidden_BanFunction" value="" id="id_hidden_BanFunction" class="form-control" />
											<input type="hidden" name="idban_increment_id" value="" id="idban_increment_id" class="form-control" />
											<input type="hidden" name="hidden_horse_id" value="" id="hidden_horse_id" class="form-control" />
											<input type="hidden" name="hidden_horse_ban_id" value="" id="hidden_horse_ban_id" class="form-control" />
											<button type="button" class="btn btn-primary" id = "ban_save_id" onclick="BanFunction()"  >Save</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  </div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">

									<thead>
										<tr>
										  <td class="text-left"><?php echo "Club"; ?></td>
										  <td class="text-left"><?php echo "Start Date"; ?></td>
										  <td class="text-left"><?php echo "End Date"; ?></td>
										  <td class="text-left"><?php echo "Authority"; ?></td>
										  <td class="text-left"><?php echo "Reason"; ?></td>
										  <td class="text-left"><?php echo "Action"; ?></td>
										</tr>
									</thead>

									<tbody id="bandeatilsbody">
										<?php if($bandeatils) {  ?>
											<?php foreach($bandeatils as $bankey => $banvalue) { 
												if($banvalue['startdate_ban'] == '' || $banvalue['startdate_ban'] == '0000-00-00'){
													$startdatebans = ''; 
												} else {
													$startdatebans = date('d-m-Y', strtotime($banvalue['startdate_ban']));
												} 
												if($banvalue['enddate_ban'] == '' || $banvalue['enddate_ban'] == '0000-00-00'){
													$enddatebans = '';
												} else {
													$enddatebans = date('d-m-Y', strtotime($banvalue['enddate_ban']));
												}
											?>
												<tr id='bandetail_<?php echo $bankey ?>'>
													<td><span id="clubs_<?php echo $bankey ?>" ><?php echo $banvalue['club_ban'] ?></span>
														<input type= "hidden"  name="bandats[<?php echo $bankey ?>][club_ban]" id="club_<?php echo $bankey ?>"  value="<?php echo $banvalue['club_ban'] ?>">
													</td>
													<td><span id="start_dateban_<?php echo $bankey ?>"><?php echo $startdatebans; ?></span>
														<input type= "hidden"  name="bandats[<?php echo $bankey ?>][startdate_ban]" id="start_date_bans_<?php echo $bankey ?>"  value="<?php echo $startdatebans; ?>">
													</td>
													<td><span id="end_dateban_<?php echo $bankey ?>"><?php echo $enddatebans; ?></span>
														<input type= "hidden"  name="bandats[<?php echo $bankey ?>][enddate_ban]" id="end_date_ban_<?php echo $bankey ?>"  value="<?php echo $enddatebans; ?>">
													</td>
													<td><span id="authorityban_<?php echo $bankey ?>" ><?php echo $banvalue['authority'] ?></span>
														<input type= "hidden"  name="bandats[<?php echo $bankey ?>][authority]" id="authority_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['authority'] ?>">
													</td>
													<td><span  id="reasonban_<?php echo $bankey ?>"><?php echo $banvalue['reason_ban'] ?></span>
														<input type= "hidden"  name="bandats[<?php echo $bankey ?>][reason_ban]" id="reason_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['reason_ban'] ?>">
													</td>
													<td> 
														<a onclick='updateban(<?php echo $bankey ?>);' class="btn btn-primary"><i class="fa fa-pencil"></i></a>
														<a onclick="removeBanDetail(<?php echo $banvalue['horse_id'] ?>,<?php echo $banvalue['horse_ban_id'] ?>,'bandetail_<?php echo $bankey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
													</td>
													<input type= "hidden"  name="bandats[<?php echo $bankey ?>][horse_ban_id]"  id= "hidden_increment_ban_id_<?php echo $bankey ?>"  value="<?php echo $banvalue['horse_ban_id'] ?>">
												</tr>
											<?php }  ?>
                   	 					<?php } ?>
									</tbody>
								</table>
							</div>
							<div class="col-sm-11">
								<h4>Ban History</h4>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-left" ><?php echo "Club"; ?></td>
										  <td class="text-left" ><?php echo "Start Date"; ?></td>
										  <td class="text-left" ><?php echo "End Date"; ?></td>
										  <td class="text-left" ><?php echo "Authority"; ?></td>
										  <td class="text-left" ><?php echo "Reason"; ?></td>
										   <td class="text-left" ><?php echo "Action"; ?></td>
										</tr>
									</thead>
									<tbody id="banhistorybody">
										<?php if($bandatas) { //echo'<pre>';print_r($bandatas); exit(); ?>
											<?php foreach($bandatas as $bkey => $bvalue) { ?>
												<tr id='banhistorybody<?php echo $bkey ?>'>
													<td class="text-left" >	
														<?php echo $bvalue['club_ban'] ?>
														<input type= "hidden" name="banhistorydatas[<?php echo $bkey ?>][club_ban]" id="history_club_<?php echo $bkey ?>"  value="<?php echo $bvalue['club_ban'] ?>" >
													</td>
													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($bvalue['startdate_ban'])); ?>
														<input type= "hidden" name="banhistorydatas[<?php echo $bkey ?>][startdate_ban]" id="history_startdate_ban_<?php echo $bkey ?>"  value="<?php echo date('d-m-Y', strtotime($bvalue['startdate_ban'])); ?>" >
													</td>
													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($bvalue['enddate_ban'])); ?>
														<input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][enddate_ban]" id="history_enddate_ban_<?php echo $bkey ?>"  value="<?php echo date('d-m-Y', strtotime($bvalue['enddate_ban'])); ?>" >
													</td>
													<td class="text-left" >
														<?php echo $bvalue['authority']; ?>
														<input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][authority]" id="history_authority_<?php echo $bkey ?>"  value="<?php echo $bvalue['authority']; ?>" >
													</td>
													<td class="text-left" >
														<?php echo $bvalue['reason_ban']; ?>
														<input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][reason_ban]" id="history_reason_ban_<?php echo $bkey ?>"  value="<?php echo $bvalue['reason_ban']; ?>" >
													</td>
													<td class="text-left">
														<a onclick="removeBanDetail(<?php echo $bvalue['horse_id'] ?>,<?php echo $bvalue['horse_ban_id'] ?>,'banhistorybody<?php echo $bkey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
													</td>
													<input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][horse_ban_id]"  value="<?php echo $bvalue['horse_ban_id']; ?>">
													<input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][horse_id]" value="<?php echo $bvalue['horse_id']; ?>">
												</tr>
											<?php } ?>
										<?php } else { ?>
                      						<tr><td colspan="6" class="text-center emptyRows">No BAN To Display</td></tr>
                   	 					<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="tab-Change-Equipment">
							<div class="col-sm-11">
								<h4>Add Equipment</h4>
							</div>
							<div style="float: right;padding-bottom: 10px;">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1"  onclick="closeaddequpment()"><i class="fa fa-plus-circle"></i></button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="id_hidden_eqipment" value="1" id="id_hidden_eqipment" class="form-control" />

								<div id="myModal1" class="modal fade" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Change Equipment</h4>
											</div>
											<div class="modal-body">
												<div class="form-group">
												<label class="col-sm-2 control-label" for="input-club"><?php echo "Equipment:"; ?></label>
													<div class="col-sm-10">
														<input type="text" name="equipment"  placeholder="<?php echo "Equipment"; ?>" id="equipment" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_equipement" class="error"><?php echo 'Please Enter Equipment'; ?></span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Choose Date:"; ?></label>
													<div class="col-sm-8">
														<div class="input-group date input-date_choose_date">
															<input type="text" name="change_horse_date" value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_choose_date" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_date_choose_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" >On/Off:</label>
													<div class="col-sm-10 radio-inline">
														<input type="radio" class="custom-control-input" id="defaultChecked" name="eqipment_checked" value="1" checked>
  														<label class="custom-control-label" for="defaultUnchecked">On</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  														<input type="radio" class="custom-control-input" id="defaultChecked_off" name="eqipment_checked"  value="0">
  														<label class="custom-control-label" for="defaultChecked">Off</label>
														<span style="display: none;color: red;font-weight: bold;" id="error_equipement_on_off" class="error"><?php echo 'Please select on/off'; ?></span>
													</div>
												</div>
											</div>
										  <div class="modal-footer">
											 <button type="button" class="btn btn-primary" id ="equipemnt_save"onclick="EquipemntFunction()" >Save</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">

									<thead>
										<tr>
										  <td class="text-left col-sm-4"><?php echo "Change-Equipment"; ?></td>
										  <td class="text-left col-sm-4"><?php echo "On"; ?></td>
										  <td class="text-left col-sm-4"><?php echo "Off"; ?></td>
										</tr>
									</thead>
									<tbody id="equipmentdeatilsbody">
										<?php if($equipment_datas) { //echo'<pre>';print_r($bandatas); exit(); ?>
											<?php foreach($equipment_datas as $eqkey => $eqvalue) { ?>
												<?php if($eqvalue['equipment_status'] == '1'){ ?>
												<tr id="equipmentdatasrow<?php echo $eqkey ?>'">
													<td class="text-left" >
														<?php echo $eqvalue['equipment_name'] ?>
														<input type= "hidden"  name= "equipment_dtas[<?php echo $eqkey ?>][equipment_name]"  value = "<?php echo $eqvalue['equipment_name'] ?>" >
													</td>
													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($eqvalue['equipment_date'])); ?>
														<input type= "hidden"  name= "equipment_dtas[<?php echo $eqkey ?>][equipment_date]"  value = "<?php echo date('d-m-Y', strtotime($eqvalue['equipment_date'])); ?>" >
													</td>
													<td class="text-left" >------</td>
													<input type= "hidden"  name= "equipment_dtas[<?php echo $eqkey ?>][equipment_status]"  value = "<?php echo $eqvalue['equipment_status'] ?>" >
												</tr>
												<?php } elseif($eqvalue['equipment_status'] == '0') { ?>
													<tr id="equipmentdatasrow<?php echo $eqkey ?>'">
														<td class="text-left" >
															<?php echo $eqvalue['equipment_name'] ?>
															<input type= "hidden"  name= "equipment_dtas[<?php echo $eqkey ?>][equipment_name]"  value = "<?php echo $eqvalue['equipment_name'] ?>" >
														</td>
														<td class="text-left" >------</td>
														<td class="text-left" >
															<?php echo date('d-m-Y', strtotime($eqvalue['equipment_date'])); ?>
															<input type= "hidden"  name= "equipment_dtas[<?php echo $eqkey ?>][equipment_date]"  value = "<?php echo date('d-m-Y', strtotime($eqvalue['equipment_date'])); ?>" >
														</td>
														<input type= "hidden"  name= "equipment_dtas[<?php echo $eqkey ?>][equipment_status]"  value = "<?php echo $eqvalue['equipment_status'] ?>" >
													</tr>
												<?php } ?>
											<?php } ?>
                   	 					<?php } ?>
									</tbody>
								</table>
							</div>
							<div class="col-sm-11">
								<h4>History of Equipment Change</h4>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-left col-sm-4"><?php echo "Change-Equipment"; ?></td>
										  <td class="text-left col-sm-4"><?php echo "On"; ?></td>
										  <td class="text-left col-sm-4"><?php echo "Off"; ?></td>
										</tr>
									</thead>
									<tbody >
										<?php if($equipmentdatas) { //echo'<pre>';print_r($bandatas); exit(); ?>
											<?php foreach($equipmentdatas as $ekey => $evalue) { ?>
												<?php if($evalue['equipment_status'] == '1'){ ?>
												<tr id="equipmentdatasrow<?php echo $ekey ?>'">
													<td class="text-left" >
														<?php echo $evalue['equipment_name'] ?>
														<input type= "hidden"  name= "equipment_datas[<?php echo $ekey ?>][equipment_name]"  value = "<?php echo $evalue['equipment_name'] ?>" >
													</td>
													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($evalue['equipment_date'])); ?>
														<input type= "hidden"  name= "equipment_datas[<?php echo $ekey ?>][equipment_date]"  value = "<?php echo date('d-m-Y', strtotime($evalue['equipment_date'])); ?>" >
													</td>
													<td class="text-left" >------</td>
													<input type= "hidden"  name= "equipment_datas[<?php echo $ekey ?>][equipment_status]"  value = "<?php echo $evalue['equipment_status'] ?>" >
													<input type= "hidden"  name= "equipment_datas[<?php echo $ekey ?>][horse_equipments_id]"  value = "<?php echo $evalue['horse_equipments_id'] ?>" >
												</tr>
												<?php } elseif($evalue['equipment_status'] == '0') { ?>
													<tr id="equipmentdatasrow<?php echo $ekey ?>'">
														<td class="text-left" >
															<?php echo $evalue['equipment_name'] ?>
															<input type= "hidden"  name= "equipment_datas[<?php echo $ekey ?>][equipment_name]"  value = "<?php echo $evalue['equipment_name'] ?>" >
														</td>
														<td class="text-left" >------</td>
														<td class="text-left" >
															<?php echo date('d-m-Y', strtotime($evalue['equipment_date'])); ?>
															<input type= "hidden"  name= "equipment_datas[<?php echo $ekey ?>][equipment_date]"  value = "<?php echo date('d-m-Y', strtotime($evalue['equipment_date'])); ?>" >
														</td>
														<input type= "hidden"  name= "equipment_datas[<?php echo $ekey ?>][equipment_status]"  value = "<?php echo $evalue['equipment_status'] ?>" >
														<input type= "hidden"  name= "equipment_datas[<?php echo $ekey ?>][horse_equipments_id]"  value = "<?php echo $evalue['horse_equipments_id'] ?>" >
													</tr>
												<?php } ?>
											<?php } ?>
										<?php } else { ?>
                      						<tr><td colspan="3" class="text-center emptyRows">No Equipment To Display</td></tr>
                   	 					<?php } ?> 
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="tab-Shoeing-and-bits">
							<div class="col-sm-11">
								<h4>Add Shoeing and Bits</h4>
							</div>
							<div style="float: right;padding-bottom: 10px;">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2" onclick="closeaddshoe()"><i class="fa fa-plus-circle"></i></button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="id_hidden_shoe" value="1" id="id_hidden_shoe" class="form-control" />
								<div id="myModal2" class="modal fade" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Add Shoeing and Bits</h4>
											</div>
											<div class="modal-body">
												<div class="form-group">
												<label class="col-sm-2 control-label" for="input-club"><?php echo "Type:"; ?></label>
													<div class="col-sm-10">
														<input type="text" name="type_shoe"  placeholder="<?php echo "Type"; ?>" id="type_shoe" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_type_shoeing" class="error"><?php echo 'Please Enter Type'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Shoe:"; ?></label>
													<div class="col-sm-10">
														<input type="text" name="shoe_name"  placeholder="<?php echo "Shoe"; ?>" id="shoe_name" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_shoe" class="error"><?php echo 'Please Enter Shoe'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Description:"; ?></label>
													<div class="col-sm-10">
														<input type="text" name="description_shoe"  placeholder="<?php echo "Description"; ?>" id="description_shoe" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_shoe_description" class="error"><?php echo 'Please Enter Description'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Bit:"; ?></label>
													<div class="col-sm-10">
														<input type="text" name="bit_shoe"  placeholder="<?php echo "Bit Shoe"; ?>" id="bit_shoe" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_bit_shoe" class="error"><?php echo 'Please Enter Bit'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Description:"; ?></label>
													<div class="col-sm-10">
														<input type="text" name="bit_description"  placeholder="<?php echo "Description"; ?>" id="bit_description" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_bit_shoe_description" class="error"><?php echo 'Please Enter Description'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Choose Date:"; ?></label>
													<div class="col-sm-8">
														<div class="input-group date input-date_choose_datess">
															<input type="text"  value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_choose_datess" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_date_choose_datess" class="error"><?php echo 'Please Select Valid Date'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-stock-status">On/Off:</label>
													<div class="col-sm-10 radio-inline">
														<input type="radio" name="shoe_checkeds" id ="shoe_checkeds"value="1"  class="custom-control-input always_selcted" checked/><?php echo "On"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" name="shoe_checkeds"  id ="shoe_checkeds" value="0" class="custom-control-input" /> <?php echo "Off"; ?>
														<span style="display: none;color: red;font-weight: bold;" id="error_shoie_on_off" class="error"><?php echo 'Please select on/off'; ?></span>
													</div>
												</div>
											</div>
										  	<div class="modal-footer">
												<button type="button" class="btn btn-primary"  id="shoeFunction_id" onclick="ShoeFunction()"  >Save</button>
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  	</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<td class="text-left"><?php echo "Type"; ?></td>
											<td class="text-left"><?php echo "Shoe"; ?></td>
											<td class="text-left"><?php echo "Description"; ?></td>
											<td class="text-left"><?php echo "Bit"; ?></td>
											<td class="text-left"><?php echo "Description"; ?></td>
											<td class="text-left"><?php echo "On"; ?></td>
											<td class="text-left"><?php echo "Off"; ?></td>
											<td class="text-left"><?php echo "Action"; ?></td>
										</tr>
									</thead>
									 <tbody id = "shoedeatilsbody">
									 	<?php if($new_shoe_dats) { ?>
											<?php foreach($new_shoe_dats as $shoekey => $shoevalue) { ?>
												<?php if($shoevalue['shoe_and_bit_status'] == '1'){ ?>
												<tr id = 'shoeing_history<?php echo $shoekey ?>'>
													<td class="text-left"><?php echo $shoevalue['type']?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][type]"  value ="<?php echo $shoevalue['type']?>" >

													<td class="text-left"><?php echo $shoevalue['shoe_name']?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][shoe_name]"  value ="<?php echo $shoevalue['shoe_name']?>" >

													<td class="text-left"><?php echo $shoevalue['shoe_description']?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][shoe_description]"  value ="<?php echo $shoevalue['shoe_description']?>" >

													<td class="text-left"><?php echo $shoevalue['bit_name']?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][bit_name]"  value ="<?php echo $shoevalue['bit_name']?>" >

													<td class="text-left"><?php echo $shoevalue['bit_description']?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][bit_description]"  value ="<?php echo $shoevalue['bit_description']?>" >

													<td class="text-left"><?php echo date('d-m-Y', strtotime($shoevalue['shoe_and_bit_date'])); ?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][shoe_and_bit_date]"  value ="<?php echo date('d-m-Y', strtotime($shoevalue['shoe_and_bit_date'])); ?>" >
													<td class="text-left">------</td>
													<td class="text-left">
														<a onclick="removeShoeingDetail(<?php echo $shoevalue['horse_id'] ?>,<?php echo $shoevalue['horse_shoeing_id'] ?>,'shoeing_history<?php echo $shoekey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
													</td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][shoe_and_bit_status]"  value ="<?php echo $shoevalue['shoe_and_bit_status']?>" >
												<tr>
												<?php } else if($shoevalue['shoe_and_bit_status'] == '0') { ?>
												<tr id = 'shoeing_history<?php echo $shoekey ?>'>
													<td class="text-left"><?php echo $shoevalue['type']?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][type]"  value ="<?php echo $shoevalue['type']?>" >

													<td class="text-left"><?php echo $shoevalue['shoe_name']?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][shoe_name]"  value ="<?php echo $shoevalue['shoe_name']?>" >

													<td class="text-left"><?php echo $shoevalue['shoe_description']?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][shoe_description]"  value ="<?php echo $shoevalue['shoe_description']?>" >

													<td class="text-left"><?php echo $shoevalue['bit_name']?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][bit_name]"  value ="<?php echo $shoevalue['bit_name']?>" >

													<td class="text-left"><?php echo $shoevalue['bit_description']?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][bit_description]"  value ="<?php echo $shoevalue['bit_description']?>" >

													<td class="text-left">------</td>
													<td class="text-left"><?php echo date('d-m-Y', strtotime($shoevalue['shoe_and_bit_date'])); ?></td>
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][shoe_and_bit_date]"  value ="<?php echo date('d-m-Y', strtotime($shoevalue['shoe_and_bit_date'])); ?>" >
													<input type= "hidden"  name= "shoe_datas[<?php echo $shoekey ?>][shoe_and_bit_status]"   value ="<?php echo $shoevalue['shoe_and_bit_status']?>"  >
													<td class="text-left">
														<a onclick="removeShoeingDetail(<?php echo $shoevalue['horse_id'] ?>,<?php echo $shoevalue['horse_shoeing_id'] ?>,'shoeing_history<?php echo $shoekey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
													</td>
								 				</tr>
												<?php } ?>
											<?php } ?>
               	 						<?php } ?> 
									</tbody>
								</table>
							</div>
							<div class="col-sm-11">
								<h4>History Of Shoeing and bits</h4>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<td class="text-left"><?php echo "Type"; ?></td>
											<td class="text-left"><?php echo "Shoe"; ?></td>
											<td class="text-left"><?php echo "Description"; ?></td>
											<td class="text-left"><?php echo "Bit"; ?></td>
											<td class="text-left"><?php echo "Description"; ?></td>
											<td class="text-left"><?php echo "On"; ?></td>
											<td class="text-left"><?php echo "Off"; ?></td>
											<td class="text-left"><?php echo "Action"; ?></td>
										</tr>
									</thead>
									 <tbody>
								 		<?php if($history_shoe) { ?>
											<?php foreach($history_shoe as $historykey => $historyvalue) { ?>
												<?php if($historyvalue['shoe_and_bit_status'] == '1'){ ?>
												<tr id = 'shoeing_history<?php echo $historykey ?>'>
													<td class="text-left"><?php echo $historyvalue['type']?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][type]"  value ="<?php echo $historyvalue['type']?>" >

													<td class="text-left"><?php echo $historyvalue['shoe_name']?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][shoe_name]"  value ="<?php echo $historyvalue['shoe_name']?>" >

													<td class="text-left"><?php echo $historyvalue['shoe_description']?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][shoe_description]"  value ="<?php echo $historyvalue['shoe_description']?>" >

													<td class="text-left"><?php echo $historyvalue['bit_name']?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][bit_name]"  value ="<?php echo $historyvalue['bit_name']?>" >

													<td class="text-left"><?php echo $historyvalue['bit_description']?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][bit_description]"  value ="<?php echo $historyvalue['bit_description']?>" >

													<td class="text-left"><?php echo date('d-m-Y', strtotime($historyvalue['shoe_and_bit_date'])); ?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][shoe_and_bit_date]"  value ="<?php echo date('d-m-Y', strtotime($historyvalue['shoe_and_bit_date'])); ?>" >
													<td class="text-left">------</td>
													<td class="text-left">
														<a onclick="removeShoeingDetail(<?php echo $historyvalue['horse_id'] ?>,<?php echo $historyvalue['horse_shoeing_id'] ?>,'shoeing_history<?php echo $historykey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
													</td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][shoe_and_bit_status]"  value ="<?php echo $historyvalue['shoe_and_bit_status']?>" >
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][horse_shoeing_id]"  value ="<?php echo $historyvalue['horse_shoeing_id']?>" >
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][horse_id]"  value ="<?php echo $historyvalue['horse_id']?>" >
												<tr>
												<?php } else if($historyvalue['shoe_and_bit_status'] == '0') { ?>
												<tr id = 'shoeing_history<?php echo $historykey ?>'>
													<td class="text-left"><?php echo $historyvalue['type']?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][type]"  value ="<?php echo $historyvalue['type']?>" >

													<td class="text-left"><?php echo $historyvalue['shoe_name']?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][shoe_name]"  value ="<?php echo $historyvalue['shoe_name']?>" >

													<td class="text-left"><?php echo $historyvalue['shoe_description']?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][shoe_description]"  value ="<?php echo $historyvalue['shoe_description']?>" >

													<td class="text-left"><?php echo $historyvalue['bit_name']?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][bit_name]"  value ="<?php echo $historyvalue['bit_name']?>" >

													<td class="text-left"><?php echo $historyvalue['bit_description']?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][bit_description]"  value ="<?php echo $historyvalue['bit_description']?>" >

													<td class="text-left">------</td>
													<td class="text-left"><?php echo date('d-m-Y', strtotime($historyvalue['shoe_and_bit_date'])); ?></td>
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][shoe_and_bit_date]"  value ="<?php echo date('d-m-Y', strtotime($historyvalue['shoe_and_bit_date'])); ?>" >
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][shoe_and_bit_status]"   value ="<?php echo $historyvalue['shoe_and_bit_status']?>" >
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][horse_shoeing_id]"  value ="<?php echo $historyvalue['horse_shoeing_id']?>" >
													<input type= "hidden"  name= "shoe_datas_history[<?php echo $historykey ?>][horse_id]"  value ="<?php echo $historyvalue['horse_id']?>" >
													<td class="text-left">
														<a onclick="removeShoeingDetail(<?php echo $historyvalue['horse_id'] ?>,<?php echo $historyvalue['horse_shoeing_id'] ?>,'shoeing_history<?php echo $historykey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
													</td>
								 				</tr>
												<?php } ?>
											<?php } ?>
										<?php } else { ?>
              								<tr><td colspan="8" class="text-center emptyRows">No Shoeing and Bits To Display</td></tr>
           	 							<?php } ?> 
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="tab-Stakes-Earned-Outstation">
							<div class="col-sm-11">
								<h4>Stakes Earned at RWITC</h4>
							</div>
							<div class="col-sm-1">
								<div id="myModal4" class="modal fade" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Stakes Outstation</h4>
											</div>
											<div class="modal-body">
												<div class="form-group">
												<label class="col-sm-2 control-label" for="input-club"><?php echo "Venue:"; ?></label>
													<div class="col-sm-8">
														<select name="stacked_earneds_vanue" name="stacked_earneds_vanue" placeholder="Venue" id="stacked_earneds_vanue" class="form-control">
															<option value="" >Please Select</option>	
															<?php foreach ($venus as $vkey => $vvalue) { ?>
															<option value="<?php echo $vvalue; ?>"><?php echo $vvalue ?></option>
															<?php } ?>
													 	 </select>
														<span style="display: none;color: red;font-weight: bold;" id="error_stacked_earneds_vanue" class="error"><?php echo 'Please Select Venue'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Season:"; ?></label>
													<div class="col-sm-10">
														<input type="text" name="season_name"  placeholder="Season" id="season_name" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_season" class="error"><?php echo 'Please Enter Season'; ?></span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><?php echo "Race No:"; ?></label>
													<div class="col-sm-10">
														<input type="Number" name="race_no"  placeholder="Race No" id="race_no" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_race_no" class="error"><?php echo 'Please Enter Race No'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><?php echo "Stackes:"; ?></label>
													<div class="col-sm-10">
														<input type="Number" name="stacks_id"  placeholder="Stackes" id="stacks_id" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_stacks_id" class="error"><?php echo 'Please Enter Stackes'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><?php echo "Race Date:"; ?></label>
													<div class="col-sm-8">
														<div class="input-group date input-race_date">
															<input type="text" name="" value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="race_date" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_race_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Grade"; ?></label>
													<div class="col-sm-5">
														<select name="grade_stack_id" id="grade_stack_id" class="form-control">
															<option value="" >Please Select</option>	
															<?php foreach ($grads as $gkey => $gvalue) { ?>
																<option value="<?php echo $gvalue; ?>" selected="selected"><?php echo $gvalue; ?></option>
															<?php } ?>
													  	</select>
														<span style="display: none;color: red;font-weight: bold;" id="error_grade_stack_id" class="error"><?php echo 'Please Select Grade'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><?php echo "Placing:"; ?></label>
													<div class="col-sm-10">
														<input type="Number" name="placing_id"  placeholder="Placing" id="placing_id" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_placing_id" class="error"><?php echo 'Please Enter Place'; ?></span>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-primary" id= "Stackedoutstationsave_id" onclick="StackedoutstationFunction()">Save</button>
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-left"><?php echo "Venue"; ?></td>
										  <td class="text-left"><?php echo "Season"; ?></td>
										  <td class="text-left"><?php echo "Race No"; ?></td>
										  <td class="text-left"><?php echo "Placing"; ?></td>
										  <td class="text-left"><?php echo "Stackes"; ?></td>
										  <td class="text-left"><?php echo "Date"; ?></td>
										  <td class="text-left"><?php echo "Grade"; ?></td>
										</tr>
									</thead>
								</table>
							</div>
							<div style="float: right;padding-bottom: 10px;">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal4" onclick="closestackesearned()"><i class="fa fa-plus-circle"></i></button>
								<input type="hidden" name="stacked_hidden_outstation_id" value="<?php  echo $stacked_hidden_outstation_id ?>" id="stacked_hidden_outstation_id" class="form-control" />
							</div>
							<div class="col-sm-11">
								<h4>Stakes Earned At Outstation</h4>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-left"><?php echo "Venue"; ?></td>
										  <td class="text-left"><?php echo "Season"; ?></td>
										  <td class="text-left"><?php echo "Race No"; ?></td>
										  <td class="text-left"><?php echo "Placing"; ?></td>
										  <td class="text-left"><?php echo "Stackes"; ?></td>
										  <td class="text-left"><?php echo "Date"; ?></td>
										  <td class="text-left"><?php echo "Grade"; ?></td>
										   <td class="text-left"><?php echo "Action"; ?></td>
										</tr>
									</thead>
									<tbody id="stackedoutstationbody">
									 	<?php $sumof_stacks = 0 ;?>
									 	<?php if(isset($stackoutstation_datas)) {  ?>
											<?php foreach($stackoutstation_datas as $stackkey => $stackvalue) { ?>
												<?php  $sumof_stacks = $sumof_stacks +  $stackvalue['stack_id'] ?>
												<tr id='stackedoutstationrow<?php echo $stackkey ?>'>
													<td class="text-left" >	
														<?php echo $stackvalue['stack_venue'] ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][stack_venue]" value="<?php echo $stackvalue['stack_venue'] ?>" >
													</td>
													<td class="text-left" >
														<?php echo $stackvalue['season_name'] ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][season_name]"  value="<?php echo $stackvalue['season_name'] ?>" >
													</td>
													<td class="text-left" >
														<?php echo $stackvalue['stack_race_no']; ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][stack_race_no]" value="<?php echo $stackvalue['stack_race_no']; ?> " >
													</td>
													<td class="text-left" >
														<?php echo $stackvalue['stack_placing_no']; ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][stack_placing_no]" value="<?php echo $stackvalue['stack_placing_no']; ?>" >
													</td>
													<td class="text-left" >
														<?php echo $stackvalue['stack_id']; ?>
														<input type= "hidden" class="stacks_out_id" name= "stackesdatas[<?php echo $stackkey ?>][stack_id]"  value="<?php echo $stackvalue['stack_id']; ?>" >
													</td>
													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($stackvalue['stack_race_date'])); ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][stack_race_date]"  value="<?php echo date('d-m-Y', strtotime($stackvalue['stack_race_date'])); ?>" >
													</td>
													<td class="text-left" >
														<?php echo $stackvalue['grade_stack_id']; ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][grade_stack_id]" value="<?php echo $stackvalue['grade_stack_id']; ?>" >
													</td>
													<td class="text-left">
														<a onclick="removeStackOtDetail(<?php echo $stackvalue['horse_id'] ?>,<?php echo $stackvalue['horse_stackoutstation_id'] ?>,'stackedoutstationrow<?php echo $stackkey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
													</td>
													<input type= "hidden"  name="stackesdatas[<?php echo $stackkey ?>][horse_stackoutstation_id]"   value="<?php echo $stackvalue['horse_stackoutstation_id'] ?>">
													<input type= "hidden"  name="stackesdatas[<?php echo $stackkey ?>][horse_id]"   value="<?php echo $stackvalue['horse_id'] ?>">
												</tr>
											<?php } ?>
           	 							<?php } ?> 
									</tbody>
								</table>
								<center style = 'font-weight: bold;'>Total Stackes ear..<span id = "stacks_dats" ><?php echo $sumof_stacks ?></span></center>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript"><!--
		show_owner_tab = '<?php echo $show_owner_tab; ?>';
		if(show_owner_tab == 1){
			$('#basic_tab_class').removeClass('active');
			$('#ownership_tab_class').addClass('active');
			$('#tab-horse_basic').removeClass('active');
			$('#tab-ownership').addClass('active');
			$('#frm_owner_id').focus();
		}
		$('.date').datetimepicker({
			pickTime: false
		});
	</script>
	<script type="text/javascript">
	    $("#owner_percentage_id").keyup(function()  {
	      	var owner_percentage_valid =  $('#owner_percentage_id').val();
	      	var owner_percentage_valid1 = owner_percentage_valid.replace(/[^A-Z0-9.]+/i, '');
	      	$("#owner_percentage_id").val(owner_percentage_valid1);
	      	if(parseInt($(this).val()) >= 0  && parseInt($(this).val()) <= 100){
	      		$('#error_owner_percentage').hide();
	      	} else {
	   			$('#error_owner_percentage').show();
	   		} 
	    });

	    var timer = null;
		$('#date_ownership').keyup(function(){
	       	clearTimeout(timer); 
	        var date_ownership_valid =  $('#date_ownership').val();
	       	date_ownership_valid1 = date_ownership_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_ownership").val(date_ownership_valid1);
	       	timer = setTimeout(dateOwnership, 800)
		});

		function dateOwnership() {
		   var date_ownership_valid =  $('#date_ownership').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_ownership_valid))) {
				$('#error_datawonership').show();
			    return false;
			} else {
				$('#error_datawonership').hide();
			}
		}

		$('.input-date_ownership').datetimepicker().on('dp.change', function (e) {  
		    $('#error_datawonership').css('display','none');
		});

		var timer = null;
		$('#end_date_ownership').keyup(function(){
	       	clearTimeout(timer); 
	        var end_date_ownership_valid =  $('#end_date_ownership').val();
	       	end_date_ownership_valid1 = end_date_ownership_valid.replace(/[^0-9-]+/i, '');
	       	$("#end_date_ownership").val(end_date_ownership_valid1);
	       	timer = setTimeout(end_date_ownership, 800)
		});

		function end_date_ownership() {
		   var end_date_ownership_valid =  $('#end_date_ownership').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(end_date_ownership_valid))) {
				$('#error_end_date_ownership').show();
			    return false;
			} else {
				$('#error_end_date_ownership').hide();
			}
		}

		$('.input-end_date_ownership').datetimepicker().on('dp.change', function (e) {  
		    $('#error_end_date_ownership').css('display','none');
		});

	   			 /*For Ownership and laese tab */
		function SaveOwnershp(){
			var date_ownership =  $( "#date_ownership" ).val();
			var end_date_ownership =  $( "#end_date_ownership" ).val();
			var ownership_remarks_checked = $('#ownership_remarks').val();
			var frm_owner_id_checked = $('#frm_owner_id').val();
			var to_owner_id_checked = $('#to_owner_id').val();
			var owner_percentage_id_checked= $('#owner_percentage_id').val();
			var owner_percentage_id_checked_length= $('#owner_percentage_id').val().length;
			var ownership_type_id_ckecked = $('#ownership_type_id').val();

			if(date_ownership != ''){
				$('#error_datawonership').hide();
				var date_ownerships = date_ownership;
			} else {
				$('#error_datawonership').show();
			}

			if(frm_owner_id_checked != ''){
				$('#error_frm_owner').hide();
				frm_owner_id = frm_owner_id_checked;
			} else {
				$('#error_frm_owner').show();
			}

			if(to_owner_id_checked != ''){
				$('#error_to_owner').hide();
				to_owner_id = to_owner_id_checked;
			} else {
				$('#error_to_owner').show();
			}

			if(owner_percentage_id_checked != '' && owner_percentage_id_checked_length > 0 && owner_percentage_id_checked >= 0 && owner_percentage_id_checked <= 100 ){
				$('#error_owner_percentage').hide();
				owner_percentage_id = owner_percentage_id_checked;
			} else {
				$('#error_owner_percentage').show();
			}


			if(ownership_type_id_ckecked != null && ownership_type_id_ckecked != ''){
				$('#error_ownership_type').hide();
				ownership_type_ids = ownership_type_id_ckecked ;
				if(ownership_type_id_ckecked == 'Lease' || ownership_type_id_ckecked == 'Lease with Contingency' ){
					if(end_date_ownership != ''){
						$('#error_end_date_ownership').hide();
						var end_date_ownerships = end_date_ownership;
					} else {
						$('#error_end_date_ownership').show();
					}
				} else {
					if(end_date_ownership == ''){
						$('#error_end_date_ownership').hide();
						var end_date_ownerships = end_date_ownership;
					} 
				}
			} else{
				$('#error_ownership_type').show();
			}

			if(ownership_remarks_checked != ''){
				$('#error_ownership_remarks').hide();
				ownership_remarks = ownership_remarks_checked;
			} else {
				$('#error_ownership_remarks').show();
			}

			inerror = 0;
			if(ownership_type_id_ckecked == 'Lease' || ownership_type_id_ckecked == 'Lease with Contingency'){
				if(date_ownership != '' || end_date_ownership != ''){
					var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
					if (!(date_regex.test(date_ownership))) {
						$('#error_datawonership').show();
						 inerror = 1;
					} else {
						$('#error_datawonership').hide();
					}
					if (!(date_regex.test(end_date_ownership))) {
						$('#error_end_date_ownership').show();
						 inerror = 1;
					} else {
						$('#error_end_date_ownership').hide();
					}
				}
				if( date_ownership == '' || end_date_ownership == '' ||  frm_owner_id_checked == '' || to_owner_id_checked == '' || owner_percentage_id_checked == ''  || ownership_type_id_ckecked == null ||  ownership_type_id_ckecked == '' || ownership_type_id_ckecked == null ||ownership_type_id_ckecked == '' || ownership_remarks_checked  == '' || owner_percentage_id_checked <= 0 && owner_percentage_id_checked >= 100 ){
					
					return false;
				}else if(inerror == 1){
					return false;
				}
			} else {
				if(date_ownership != ''){
					var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
					if (!(date_regex.test(date_ownership))) {
						$('#error_datawonership').show();
						 inerror = 1;
					} else {
						$('#error_datawonership').hide();
					}
					if(date_ownership != '' && end_date_ownership != ''){
						if (!(date_regex.test(end_date_ownership))) {
							$('#error_end_date_ownership').show();
							 inerror = 1;
						} else {
							$('#error_end_date_ownership').hide();
						}
					}
				}
				if( date_ownership == '' || frm_owner_id_checked == '' || to_owner_id_checked == '' || owner_percentage_id_checked == ''  || ownership_type_id_ckecked == null ||  ownership_type_id_ckecked == '' || ownership_type_id_ckecked == null ||ownership_type_id_ckecked == '' || ownership_remarks_checked  == '' || owner_percentage_id_checked <= 0 && owner_percentage_id_checked >= 100){
					return false;
				} else if(inerror == 1){
					return false;
				}
			}

			var ownership_date_id = date_ownerships;
			if(end_date_ownership != '' ){
				var ownership_date_end_id = end_date_ownership;
			} else {
				var ownership_date_end_id = '';
			}
			
			var auto_id = $('#ownership_hidden_id').val();
			var frm_owner_id_hidden = $('#frm_owner_id_hidden').val();
			var to_owner_id_hidden = $('#to_owner_id_hidden').val();
			
			var iscolor_checked = $('input[name=\'color_owner_jk_id\']:checked').val();
			if(iscolor_checked != '1'){
				var iscolors_checked = 'N0';
			} else {
				var iscolors_checked = 'Yes';
			}
			var id_hidden_saveownership = $('#id_hidden_saveownership').val();

			if(id_hidden_saveownership != '0'){ 
				html = '<tr id = "tr_'+auto_id+'">';
					html += '<td class="text-left frm_ownername_'+auto_id+'">';
						html += '<span id="from_owner_historys_'+auto_id+'">' + frm_owner_id + '</span>';
						html += '<input type= "hidden"  name= "ownerdatas['+auto_id+'][from_owner]" id="from_owner_history_'+auto_id+'" value = \'' + frm_owner_id + '\'>';
						html += '<input type= "hidden"  name= "ownerdatas['+auto_id+'][from_owner_id]"  id="from_owner_id_history_'+auto_id+'" value = \'' + frm_owner_id_hidden + '\'>';
					html += '</td>';

					html += '<td class="text-left to_ownername_'+auto_id+'" >';
						html += '<span id="to_owner_historys_'+auto_id+'">' + to_owner_id + '</span>';
						html += '<input type= "hidden"   name= "ownerdatas['+auto_id+'][to_owner]"  id="to_owner_history_'+auto_id+'" value = \'' + to_owner_id + '\'>';
						html += '<input type= "hidden"  name= "ownerdatas['+auto_id+'][to_owner_id]"  id="to_owner_idhistory_'+auto_id+'" value = \'' + to_owner_id_hidden + '\'>';
					html += '</td>';

					html += '<td class="text-left owner_percentagename_'+auto_id+'" >';
						html += '<span id="owner_percentage_historys_'+auto_id+'">' +owner_percentage_id+ '</span>';
						html += '<input type= "hidden"   name= "ownerdatas['+auto_id+'][owner_percentage]" id="owner_percentage_history_'+auto_id+'" value = '+owner_percentage_id+'>';
					html += '</td>';

					html += '<td class="text-left ownership_typename_'+auto_id+'" >';
						html += '<span id="ownership_type_historys_'+auto_id+'">' + ownership_type_ids + '</span>';
						html += '<input type= "hidden"   name= "ownerdatas['+auto_id+'][ownership_type]" id="ownership_type_history_'+auto_id+'"  value = \'' + ownership_type_ids + '\'>';
					html += '</td>';

					html += '<td class="text-left ownership_date_name'+auto_id+'" >';
						html += '<span id="date_of_ownership_historys_'+auto_id+'">' +ownership_date_id+ '</span>';
						html += '<input type= "hidden"   name= "ownerdatas['+auto_id+'][date_of_ownership]" id="date_of_ownership_history_'+auto_id+'"  value = '+ownership_date_id+'>';
					html += '</td>';

					html += '<td class="text-left ownership_date_end_'+auto_id+'">';
						html += '<span id="end_date_of_ownership_historys_'+auto_id+'">'+ownership_date_end_id+ '</span>';
						html += '<input type= "hidden"   name= "ownerdatas['+auto_id+'][end_date_of_ownership]"  id="end_date_of_ownership_history_'+auto_id+'" value = '+ownership_date_end_id+'>';
					html += '</td>';

					html += '<td class="ownership_remarks_name_'+auto_id+'">' 
						html += '<span id="remark_historys_'+auto_id+'">'+ ownership_remarks + '</span>';
						html += '<input type= "hidden"  name= "ownerdatas['+auto_id+'][remark_horse_to_owner]"  id="remark_history_'+auto_id+'" value = \'' + ownership_remarks + '\'>'; 
					html += '</td>';

					html += '<td class="text-left iscolors_'+auto_id+'">';
						html += '<span id="color_owner_historys_'+auto_id+'">'+iscolors_checked+ '</span>';
						html += '<input type= "hidden"  name= "ownerdatas['+auto_id+'][owner_color]" id="color_owner_history_'+auto_id+'" value = '+iscolors_checked+'>';
					html += '</td>';
					html += '<td>';
						html += '<a onclick=updateownerhistory("'+auto_id+'") class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>';
				html += '</tr >';
				auto_id++;
				$('#ownershipdeatilsbody').append(html);
				$('#ownership_hidden_id').val(auto_id);
				$('#to_owner_id').val('');
				$('#owner_percentage_id').val('');
				$('#ownership_type_id').val('');
				$('#ownership_date_id').val('');
				$('#ownership_date_end_id').val('');
				$('#ownership_remarks').val('');
				$('#color_owner_jk_id').attr('checked', false);
				$('#frm_owner_id').val('');
				$( "#date_ownership" ).val('');
				$( "#month_ownership" ).val('');
				$( "#year_ownership" ).val('');
				$( "#end_date_ownership" ).val('');
				$( "#end_month_ownership" ).val('');
				$( "#end_year_ownership" ).val('');
				$( "#frm_owner_id_hidden" ).val('');
				$( "#to_owner_id_hidden" ).val('');
				$("#frm_owner_id").focus();
			} else { 

				var old_ownership_date_id = date_ownerships;
				if(end_date_ownership != ''){
					var old_ownership_date_end_id = end_date_ownership;
				} else {
					var old_ownership_date_end_id = '';
				}
				var idowner_old_increment_id = $('#idowner_old_increment_id').val();
				//alert(idowner_old_increment_id);
				$('#date_of_ownership_history_'+idowner_old_increment_id+'').val(old_ownership_date_id);
				$('#end_date_of_ownership_history_'+idowner_old_increment_id+'').val(old_ownership_date_end_id);
				$('#from_owner_history_'+idowner_old_increment_id+'').val(frm_owner_id);
				$('#from_owner_id_history_'+idowner_old_increment_id+'').val(frm_owner_id_hidden);
				$('#to_owner_history_'+idowner_old_increment_id+'').val(to_owner_id);
				$('#to_owner_idhistory_'+idowner_old_increment_id+'').val(to_owner_id_hidden);
				$('#owner_percentage_history_'+idowner_old_increment_id+'').val(owner_percentage_id);
				$('#ownership_type_history_'+idowner_old_increment_id+'').val(ownership_type_ids);
				$('#remark_history_'+idowner_old_increment_id+'').val(ownership_remarks);
				$('#color_owner_history_'+idowner_old_increment_id+'').val(iscolors_checked);

				$('#date_of_ownership_historys_'+idowner_old_increment_id+'').html(old_ownership_date_id);
				$('#end_date_of_ownership_historys_'+idowner_old_increment_id+'').html(old_ownership_date_end_id);
				$('#from_owner_historys_'+idowner_old_increment_id+'').html(frm_owner_id);
				$('#to_owner_historys_'+idowner_old_increment_id+'').html(to_owner_id);
				$('#owner_percentage_historys_'+idowner_old_increment_id+'').html(owner_percentage_id);
				$('#ownership_type_historys_'+idowner_old_increment_id+'').html(ownership_type_ids);
				$('#remark_historys_'+idowner_old_increment_id+'').html(ownership_remarks);
				$('#color_owner_historys_'+idowner_old_increment_id+'').html(iscolors_checked);
				
				$('#to_owner_id').val('');
				$('#owner_percentage_id').val('');
				$('#ownership_type_id').val('');
				$('#ownership_remarks').val('');
				$('#color_owner_jk_id').attr('checked', false);
				$('#frm_owner_id').val('');
				$( "#date_ownership" ).val('');
				$( "#end_date_ownership" ).val('');
				$( "#frm_owner_id_hidden" ).val('');
				$( "#to_owner_id_hidden" ).val('');
				$('#id_hidden_saveownership').val('');

			}
		}

		function updateownerhistory(auto_id){
			$('#id_hidden_saveownership').val(0);
			$('#idowner_old_increment_id').val(auto_id);
			var from_owner_owner = $('#from_owner_history_'+auto_id+'').val();
			var from_owner_id_owner = $('#from_owner_id_history_'+auto_id+'').val();
			var to_owner_owner = $('#to_owner_history_'+auto_id+'').val();
			var to_owner_id_owner = $('#to_owner_idhistory_'+auto_id+'').val();
			var start_date_owner = $('#date_of_ownership_history_'+auto_id+'').val();
			var end_date_owner = $('#end_date_of_ownership_history_'+auto_id+'').val();
			var ownership_type_owner = $('#ownership_type_history_'+auto_id+'').val();
			var percentage_owner = $('#owner_percentage_history_'+auto_id+'').val();
			var remark_owner = $('#remark_history_'+auto_id+'').val();
			var color_owner_owner = $('#color_owner_history_'+auto_id+'').val();

			$('#to_owner_id').val(to_owner_owner);
			$('#owner_percentage_id').val(percentage_owner);
			$('#ownership_type_id').val(ownership_type_owner);
			$('#ownership_remarks').val(remark_owner);
			if(color_owner_owner == 'Yes'){
				$('#color_owner_jk_id').prop('checked', true);
			} else{
				$('#color_owner_jk_id').attr('checked', false);
			}
			$('#frm_owner_id').val(from_owner_owner);
			$( "#date_ownership" ).val(start_date_owner);
			$( "#end_date_ownership" ).val(end_date_owner);
			$( "#frm_owner_id_hidden" ).val(from_owner_id_owner);
			$( "#to_owner_id_hidden" ).val(to_owner_id_owner);
		}


		$(document).on('keydown', '.form-control', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'owner_percentage_id'){
					$('#ownership_type_id').focus();
				}
				if(id == 'ownership_type_id'){
					$('#date_ownership').focus();
				}
				if(id == 'date_ownership'){
					$('#end_date_ownership').focus();
				}
				if(id == 'end_date_ownership'){
					$('#ownership_remarks').focus();
				}
				if(id == 'ownership_remarks'){
					$('#color_owner_jk_id').focus();
				}
				if(id == 'color_owner_jk_id'){
					$('#savefunctionownership').focus();
				}
				if(id == 'frm_owner_id' && value == '' && $('.frm_ownername_1').length > 0 ){
					var conn = confirm('Do You want to Save');				
					if(conn != false){
						$('#form-horse').submit();
					} else {
					}
				}
			}
		});

		$( "#ownership_type_id" ).select(function() {
			$('#date_ownership').focus();
		});
		
		$( document ).ready(function() {
			$( "#input-horse" ).focus();
		});

		$("input, textarea, select, checkbox").keypress(function(event) {
			if (event.which == 13) {
				event.preventDefault();
			}
		});

		$('.from_owner').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
				if(request != ''){
					$.ajax({
				  		url: 'index.php?route=catalog/horse/autocompleteFromOwner&token=<?php echo $token; ?>&from_owner_name=' +  encodeURIComponent(request),
				  		dataType: 'json',
				  		success: function(json) {   
							response($.map(json, function(item) {
					  			return {
									label: item.fromowner_name,
									value: item.fromowner_code
					  			}
							}));
				 	 	}
					});
				}
		  	}, 
		  	select: function(item) {
				$('#frm_owner_id').val(item.label);
				$('#frm_owner_id_hidden').val(item.value);
				$('.dropdown-menu').hide();
				$('#to_owner_id').focus();
				return false;
		  	},
		});

		$('#to_owner_id').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
				if(request != ''){
					$.ajax({
				  		url: 'index.php?route=catalog/horse/autocompleteToOwner&token=<?php echo $token; ?>&to_owner_name=' +  encodeURIComponent(request),
				  		dataType: 'json',
				  		success: function(json) {   
							response($.map(json, function(item) {
						  		return {
									label: item.toowner_name,
									value: item.toowner_code
						  		}
							}));
				  		}
					});
				}
			}, 
			select: function(item) {
				$('#to_owner_id').val(item.label);
				$('#to_owner_id_hidden').val(item.value);
				$('.dropdown-menu').hide();
				$('#owner_percentage_id').focus();
				return false;
			},
		});
	</script>

	<script type="text/javascript">
		$("input, textarea, select, checkbox").keypress(function(event) {
			if (event.which == 13) {
				event.preventDefault();
				//SaveOwnershp();
			}
		});
		$('#start_date_ban').keyup(function(){
	        var start_date_ban_valid =  $('#start_date_ban').val();
	       	start_date_ban_valid1 = start_date_ban_valid.replace(/[^0-9-]+/i, '');
	       	$("#start_date_ban").val(start_date_ban_valid1);
	       	var start_date_ban_valid_again =  $('#start_date_ban').val();
		   	var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(start_date_ban_valid_again))) {
				$('#error_date_start_date_ban').show();
			    return false;
			} else {
				$('#error_date_start_date_ban').hide();
			}
		});
		$('.input-start_date_ban').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_start_date_ban').css('display','none');
		});

		$('#end_date_ban').keyup(function(){
	        var end_date_ban_valid =  $('#end_date_ban').val();
	       	end_date_ban_valid1 = end_date_ban_valid.replace(/[^0-9-]+/i, '');
	       	$("#end_date_ban").val(end_date_ban_valid1);
	       	var end_date_ban_valid_again =  $('#end_date_ban').val();
		   	var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(end_date_ban_valid))) {
				$('#error_date_end_date_ban').show();
			    return false;
			} else {
				$('#error_date_end_date_ban').hide();
			}
		});
		$('.input-end_date_ban').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_end_date_ban').css('display','none');
		});

	    //Ban details tab
		function BanFunction(){
			var date_start_date_ban =  $( "#start_date_ban" ).val();
			var date_end_date_ban = $( "#end_date_ban" ).val();
			var auto_id = $('#id_hidden_band').val();
			var id_hidden_BanFunction = $('#id_hidden_BanFunction').val();

			if( date_start_date_ban != ''){
				$('#error_date_start_date_ban').hide();
			} else {
				$('#error_date_start_date_ban').show();
			}

			var club = $('#input-club').val();

			if(club != null && club != ''){
				$('#error_club_ban').hide();
				clubs = club;
			} else {
				$('#error_club_ban').show();
			}
			
			var authority_ban = $('#authority_ban_details').val();
			if(authority_ban != '' && authority_ban !=  null){
				$('#error_authority_ban').hide();
				var authority_bans = authority_ban.replace(/\s\s+/g, ' ');
			} else {
				$('#error_authority_ban').show();
			}

			var reason_bans = $('#reason_ban').val();
			if(reason_bans != ''  ){
				$('#error_reason_ban').hide();
			} else {
				$('#error_reason_ban').show();
			}
			inerror = 0;
			if (date_start_date_ban != '' || date_end_date_ban != '' || date_start_date_ban == ''){
				if(date_start_date_ban != ''){
					var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
					if (!(date_regex.test(date_start_date_ban))) {
						$('#error_date_start_date_ban').show();
						 inerror = 1;
					} else {
						$('#error_date_start_date_ban').hide();
					}
					if(date_start_date_ban != '' && date_end_date_ban != ''){
						if (!(date_regex.test(date_end_date_ban))) {
							$('#error_date_end_date_ban').show();
							 inerror = 1;
						} else {
							$('#error_date_end_date_ban').hide();
						}
					}
				}
				if (date_start_date_ban == '' || club == null || club == '' || authority_ban == '' || authority_ban == null || reason_bans == '' ){
					return false;
				} else if(inerror == 1){
					return false;
				}
			} 
			date_start_date_ban == ''
				if(id_hidden_BanFunction == '0'){ 
					html = '<tr id ="bandetail_'+auto_id+'">';
						html += '<td>';
							html += '<span id="clubs_'+auto_id+'">'+clubs+ '</span>';
							html += '<input type= "hidden"  name= "bandats['+auto_id+'][club_ban]" id="club_'+auto_id+'" value = '+clubs+'>';
						html += '</td>';
						html += '<td>';
							html += '<span id="start_dateban_'+auto_id+'">'+date_start_date_ban+ '</span>';
							html += '<input type= "hidden"  name= "bandats['+auto_id+'][startdate_ban]" id="start_date_bans_'+auto_id+'" value = '+date_start_date_ban+'>';
						html += '</td>';
						html += '<td>';
							html += '<span id="end_dateban_'+auto_id+'">'+date_end_date_ban+ '</span>';
							html += '<input type= "hidden"  name= "bandats['+auto_id+'][enddate_ban]" id="end_date_ban_'+auto_id+'" value = '+date_end_date_ban+'>';
						html += '</td>';
						html += '<td>';
							html += '<span id="authorityban_'+auto_id+'">'+authority_bans+ '</span>';
							html += '<input type= "hidden"  name= "bandats['+auto_id+'][authority]" id="authority_ban_'+auto_id+'" value = \'' + authority_bans + '\'>';
						html += '</td>';
						html += '<td>';
							html += '<span id="reasonban_'+auto_id+'">'+reason_bans+ '</span>';
							html += '<input type= "hidden" name= "bandats['+auto_id+'][reason_ban]" id="reason_ban_'+auto_id+'" value = \'' + reason_bans + '\'>'; 
						html += '</td>';
						html += '<td>';
							html += '<a onclick=updateban("'+auto_id+'") class="btn btn-primary"><i class="fa fa-pencil"></i></a>&nbsp';
							html += '<a onclick="removeBanDetail(0,0,"bandetail_'+auto_id+'")" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a></td>';
							html += '<input type= "hidden" name= "bandats['+auto_id+'][horse_id]"  value ="0" >';
							html += '<input type= "hidden" name= "bandats['+auto_id+'][horse_ban_id]" value ="0"  >';
						html += '</tr >';
					auto_id++;
					$('#bandeatilsbody').append(html);
					$('#id_hidden_band').val(auto_id);
				} else {
					var idban_increment_id = $('#idban_increment_id').val();
					$('#start_date_bans_'+idban_increment_id+'').val(date_start_date_ban);
					$('#end_date_ban_'+idban_increment_id+'').val(date_end_date_ban);
					$('#reason_ban_'+idban_increment_id+'').val(reason_bans);
					$('#authority_ban_'+idban_increment_id+'').val(authority_bans);
					$('#club_'+idban_increment_id+'').val(clubs);

					$('#start_dateban_'+idban_increment_id+'').html(date_start_date_ban);
					$('#end_dateban_'+idban_increment_id+'').html(date_end_date_ban);
					$('#reasonban_'+idban_increment_id+'').html(reason_bans);
					$('#authorityban_'+idban_increment_id+'').html(authority_bans);
					$('#clubs_'+idban_increment_id+'').html(clubs);
				}

			
			$("#myModal").modal("toggle");
		}

		//blanked value for Ban details Tab
		function closeaddbuu1(){
			 $('#input-club').val('');
			$('#reason_ban').val('');
			$('#authority_ban_details').val('');
			$('#id_hidden_BanFunction').val(0);
			$( "#start_date_ban" ).val('');
			$( "#end_date_ban" ).val('');
			$('#error_club_ban').hide();
			$('#error_authority_ban').hide();
			$('#error_date_start_date_ban').hide();
			$('#error_date_end_date_ban').hide();
			$('#error_reason_ban').hide();
		}
		$('#myModal').on('shown.bs.modal', function () {
		    $('#input-club').focus();
		}); 

		//update function ban details tab
		function updateban(auto_id){
			$('#myModal').modal('show');
			$('#idban_increment_id').val(auto_id);
			$('#id_hidden_BanFunction').val(1);
			var start_date_ban = $('#start_date_bans_'+auto_id+'').val();
			var end_date_ban = $('#end_date_ban_'+auto_id+'').val();
			var reason_ban = $('#reason_ban_'+auto_id+'').val();
			var authority_ban = $('#authority_ban_'+auto_id+'').val();
			var club_ban = $('#club_'+auto_id+'').val();
			$("#start_date_ban").val(start_date_ban);
        	$("#end_date_ban").val(end_date_ban);
			$('#authority_ban_details').val(authority_ban);
			$('#reason_ban').val(reason_ban);
			$('#input-club').val(club_ban);
		}

		$(document).on('keydown', '.form-control', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'input-club'){
					$('#authority_ban_details').focus();
				}
				if(id == 'authority_ban_details'){
					$('#start_date_ban').focus();
				}
				if(id == 'start_date_ban'){
					$('#end_date_ban').focus();
				}
				if(id == 'end_date_ban'){
					$('#reason_ban').focus();
				}
				if(id == 'reason_ban'){
					$('#ban_save_id').focus();
				}
			}
		});

		function removeBanDetail(hourse_id,hourse_ban_id,id_remove){
			if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
			 	if(hourse_id == '0' && hourse_ban_id == '0'){
					alert('Delete Record Sucessfully');
					$('#'+id_remove+'').closest("tr").remove();
					return false;
				}

		        $.ajax({
                	url:'index.php?route=catalog/horse/deletebandeatils&token=<?php echo $token; ?>'+'&hourse_id='+hourse_id+'&hourse_ban_id='+hourse_ban_id,
		           	method: "POST",
		           	dataType: 'json',
		            success: function(json)
		            {
						$('#'+id_remove+'').closest("tr").remove();
						alert(json['success']);
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Error deleting data');
		            }
		        });
		         return false;
     		}
		}
	</script>

	<script type="text/javascript">
		$('#myModal1').on('shown.bs.modal', function () {
    		$('#equipment').focus();
		})
		//blanke value for Change Equipment
		function closeaddequpment(){
			$('#equipment').val('');
			var today = new Date();
			var dd = String(today.getDate()).padStart(2, '0');
			var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
			var yyyy = today.getFullYear();
    		var day_auto_choose_dates = dd + "-" + mm + "-" + yyyy;
    		$('#date_choose_date').val(day_auto_choose_dates);
			$('#defaultChecked').prop('checked', true);
			$('#error_equipement').hide();
			$('#error_date_choose_date').hide();
    		$('#error_equipement_on_off').hide();
		}

		$('#date_choose_date').keyup(function(){
	        var date_choose_date_valid =  $('#date_choose_date').val();
	       	date_choose_date_valid1 = date_choose_date_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_choose_date").val(date_choose_date_valid1);
	       	var date_choose_date_valid_again =  $('#date_choose_date').val();
		   	var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_choose_date_valid_again))) {
				$('#error_date_choose_date').show();
			    return false;
			} else {
				$('#error_date_choose_date').hide();
			}
		});
		$('.input-date_choose_date').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_choose_date').css('display','none');
		});


		// Change equipement tab
		function EquipemntFunction(){
			//alert('innn');
			var auto_id = $('#id_hidden_eqipment').val();
			var iseqipment_checked = $('input[name=\'eqipment_checked\']:checked').val();
			var date_choose_date =  $( "#date_choose_date" ).val();

			var equipments = $('#equipment').val();
			if(date_choose_date != ''){
				$('#error_date_choose_date').hide();
				var date_choose_dates = date_choose_date;
			} else {
				$('#error_date_choose_date').show();
			}

			if(equipments != ''){
				$('#error_equipement').hide();
			} else {
				$('#error_equipement').show();
			}

			if(iseqipment_checked != undefined){
				$('#error_equipement_on_off').hide();
			} else {
				$('#error_equipement_on_off').show();
			}

			inerror = 0;
			if(date_choose_date != '' || date_choose_date == ''){
				var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
				if (!(date_regex.test(date_choose_date))) {
					$('#error_date_choose_date').show();
					 inerror = 1;
				} else {
					$('#error_date_choose_date').hide();
				}

				if (date_choose_date == '' || iseqipment_checked == undefined || equipments == ''){
					return false;
				} else if(inerror == 1){
					return false;
				}
			}
			
			if(iseqipment_checked == '1'){
				//alert(equipments);
				html = '<tr id = "tr_'+auto_id+'">';
					html += '<td class="text-left" name= "equipmentsname_'+auto_id+'" >'+equipments+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "equipment_dtas['+auto_id+'][equipment_name]"  value = \'' + equipments + '\'>'; 
					
					html += '<td class="text-left" name= "choose_dates_'+auto_id+'">'+date_choose_dates+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "equipment_dtas['+auto_id+'][equipment_date]"  value = '+date_choose_dates+'>';
					
					html += '<td>------';
					html += '</td>';
					html += '<input type= "hidden"  name= "equipment_dtas['+auto_id+'][equipment_status]"  value = "1" >';
				html += '</tr >';
				auto_id++;
				$('#equipmentdeatilsbody').append(html);
				$('#id_hidden_eqipment').val(auto_id);
			}else if(iseqipment_checked == '0') {
				//alert(equipments);
				html = '<tr id = "tr_'+auto_id+'">';
					html += '<td class="text-left" name= "equipmentsname_'+auto_id+'">'+equipments+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "equipment_dtas['+auto_id+'][equipment_name]"  value =  \'' + equipments + '\'>';
					
					html += '<td>------';
					html += '</td>';
					html += '<input type= "hidden"  name= "equipment_dtas['+auto_id+'][equipment_status]"  value = "0" >';
					
					html += '<td class="text-left" name= "choose_dates_'+auto_id+'">'+date_choose_dates+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "equipment_dtas['+auto_id+'][equipment_date]"  value = '+date_choose_dates+'>';
				html += '</tr >';
				auto_id++;
				$('#equipmentdeatilsbody').append(html);
				$('#id_hidden_eqipment').val(auto_id);
			} 
			$('#myModal1').modal('hide');
		}
		$(document).on('keydown', '.form-control','.custom-control-input', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'equipment'){
					$('#date_choose_date').focus();
				}
				if(id == 'date_choose_date'){
					$('#defaultChecked').focus();
				}
				if(id == 'defaultChecked'){
					$('#equipemnt_save').focus();
				}
				
			}
		});
		$(document).on('keydown', '.custom-control-input', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			console.log(name);
			console.log(class_name);
			//console.log(id);
			if(class_name == 'custom-control-input'){
				$('#equipemnt_save').focus();
			}
		});
		$('#equipment').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
				if(request != ''){
					$.ajax({
				  		url: 'index.php?route=catalog/horse/autocompleteEquipement&token=<?php echo $token; ?>&equipment_name=' +  encodeURIComponent(request),
				  		dataType: 'json',
				  		success: function(json) {   
							response($.map(json, function(item) {
					  			return {
									label: item.equipment_name,
									value: item.equipment_name
					  			}
							}));
				 	 	}
					});
				}
		  	}, 
		  	select: function(item) {
				$('#equipment').val(item.label);
				$('.dropdown-menu').hide();
				$('#date_choose_date').focus();
				return false;
		  	},
		});
	</script>

	<script type="text/javascript">
	    /* Closed value for Shoeing and bits tab*/
		function closeaddshoe(){
			$('#type_shoe').val('');
			$('#shoe_name').val('');
			$('#description_shoe').val('');
			$('#bit_shoe').val('');
			$('#bit_description').val('');
			$('#type_shoe').focus();
			$('.always_selcted').prop('checked', true);
    		var today = new Date();
			var dd = String(today.getDate()).padStart(2, '0');
			var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
			var yyyy = today.getFullYear();
    		var day_auto_choose_dates_shoe = dd + "-" + mm + "-" + yyyy;
    		$('#date_choose_datess').val(day_auto_choose_dates_shoe);
    		$('#error_type_shoeing').hide();
    		$('#error_shoe').hide();
    		$('#error_shoe_description').hide();
    		$('#error_bit_shoe').hide();
    		$('#error_bit_shoe_description').hide();
    		$('#error_shoie_on_off').hide();
    		$('#error_date_choose_datess').hide();

		}
		$('#myModal2').on('shown.bs.modal', function () {
		    $('#type_shoe').focus();
		}); 

		$('#date_choose_datess').keyup(function(){
	        var date_choose_datess_valid =  $('#date_choose_datess').val();
	       	date_choose_datess_valid1 = date_choose_datess_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_choose_datess").val(date_choose_datess_valid1);
	       	var date_choose_datess_valid_again =  $('#date_choose_datess').val();
		   	var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_choose_datess_valid_again))) {
				$('#error_date_choose_datess').show();
			    return false;
			} else {
				$('#error_date_choose_datess').hide();
			}
		});
		$('.input-date_choose_datess').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_choose_datess').css('display','none');
		});

		/* For Shoeing and bits tab*/
		function ShoeFunction(){
			var auto_id = $('#id_hidden_shoe').val();
			var isshoe_checked = $('input[name=\'shoe_checkeds\']:checked').val();
			var date_choose_datess =  $( "#date_choose_datess" ).val();

			if(date_choose_datess != ''){
				$('#error_date_choose_datess').hide();
				var date_choose_datesss = date_choose_datess;
			} else {
				$('#error_date_choose_datess').show();
			}

			var type_shoe = $('#type_shoe').val();

			if(type_shoe != ''  ){
				$('#error_type_shoeing').hide();
			} else {
				$('#error_type_shoeing').show();
			}

			var shoe_name = $('#shoe_name').val();

			if(shoe_name != ''  ){
				$('#error_shoe').hide();
			} else {
				$('#error_shoe').show();
			}

			var description_shoe = $('#description_shoe').val();

			if(description_shoe != ''  ){
				$('#error_shoe_description').hide();
			} else {
				$('#error_shoe_description').show();
			}

			var bit_shoe = $('#bit_shoe').val();

			if(bit_shoe != ''  ){
				$('#error_bit_shoe').hide();
			} else {
				$('#error_bit_shoe').show();
			}

			var bit_description = $('#bit_description').val();

			if(bit_description != ''  ){
				$('#error_bit_shoe_description').hide();
			} else {
				$('#error_bit_shoe_description').show();
			}

			if(isshoe_checked != undefined){
				$('#error_shoie_on_off').hide();
			} else {
				$('#error_shoie_on_off').show();
			}

			inerror = 0;
			if(date_choose_datess != '' || date_choose_datess == ''){
				var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
				if (!(date_regex.test(date_choose_datess))) {
					$('#error_date_choose_datess').show();
					 inerror = 1;
				} else {
					$('#error_date_choose_datess').hide();
				}

				if (date_choose_datess == '' || type_shoe == ''  || shoe_name == '' || description_shoe == '' || bit_shoe == '' || bit_description == '' || isshoe_checked == undefined){
					return false;
				} else if(inerror == 1){
					return false;
				}
			}
			//alert(isshoe_checked);
			if(isshoe_checked == '1'){
				html = '<tr id = "shoeing_history'+auto_id+'">';
					html += '<td class="text-left" name= "typeshoename_'+auto_id+'">'+type_shoe+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][type]"  value = \'' + type_shoe + '\'>';
					
					html += '<td class="text-left" name= "shoename_'+auto_id+'" >'+shoe_name+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][shoe_name]"  value = \'' + shoe_name + '\'>';
					
					html += '<td class="text-left" name= "descriptionshoename_'+auto_id+'" >'+description_shoe+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][shoe_description]"  value = \'' + description_shoe + '\'>';
					
					html += '<td class="text-left" name= "bitname_'+auto_id+'" >'+bit_shoe+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][bit_name]"  value = \'' + bit_shoe + '\'>';
					
					html += '<td class="text-left" name= "bit_description_name'+auto_id+'" >'+bit_description+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][bit_description]"  value = \'' + bit_description + '\'>';
					
					html += '<td class="text-left" name= "choose_dates_'+auto_id+'">'+date_choose_datesss+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][shoe_and_bit_date]"  value = '+date_choose_datesss+'>';
					html += '<td>------';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][shoe_and_bit_status]"  value = "1" >';

					html += '<td>';
					html += '<a onclick=removeShoeingDetail(0,0,"shoeing_history'+auto_id+'") class="btn btn-danger"><i class="fa fa-minus-circle"></i></a></td>';
					html += '<input type= "hidden" name= "shoe_datas['+auto_id+'][horse_id]"  value ="0" >';
					html += '<input type= "hidden" name= "shoe_datas['+auto_id+'][horse_shoeing_id]" value ="0"  >';
				html += '</tr >';

				html += '</tr >';
				auto_id++;
				$('#shoedeatilsbody').append(html);
				$('#id_hidden_shoe').val(auto_id);
			}else {
				//alert(type_shoe);
				html = '<tr id = "shoeing_history'+auto_id+'">';
					html += '<td class="text-left" name= "typeshoename_'+auto_id+'">'+type_shoe+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][type]"  value = \'' + type_shoe + '\'>';

					html += '<td class="text-left" name= "shoename_'+auto_id+'" >'+shoe_name+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][shoe_name]"  value = \'' + shoe_name + '\'>';

					html += '<td class="text-left" name= "descriptionshoename_'+auto_id+'" >'+description_shoe+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][shoe_description]"  value =  \'' + description_shoe + '\'>'; 
					
					html += '<td class="text-left" name= "bitname_'+auto_id+'" >'+bit_shoe+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][bit_name]"  value =  \'' + bit_shoe + '\'>';

					html += '<td class="text-left" name= "bit_description_name'+auto_id+'" >'+bit_description+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][bit_description]"  value = \'' + bit_description + '\'>';
					html += '<td>------';
					html += '</td>';
					html += '<td class="text-left" name= "choose_dates_'+auto_id+'">'+date_choose_datesss+'';
					html += '</td>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][shoe_and_bit_date]"  value = '+date_choose_datesss+'>';
					html += '<input type= "hidden"  name= "shoe_datas['+auto_id+'][shoe_and_bit_status]"  value = "0" >';
					html += '<td>';
					html += '<a onclick=removeShoeingDetail(0,0,"shoeing_history'+auto_id+'") class="btn btn-danger"><i class="fa fa-minus-circle"></i></a></td>';
					html += '<input type= "hidden" name= "shoe_datas['+auto_id+'][horse_id]"  value ="0" >';
					html += '<input type= "hidden" name= "shoe_datas['+auto_id+'][horse_shoeing_id]" value ="0"  >';
				html += '</tr >';
				auto_id++;
				$('#shoedeatilsbody').append(html);
				$('#id_hidden_shoe').val(auto_id);
			}
			$("#myModal2").modal("toggle")
		}

		$(document).on('keydown', '.form-control','.custom-control-input', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'type_shoe'){
					$('#shoe_name').focus();
				}
				if(id == 'shoe_name'){
					$('#description_shoe').focus();
				}
				if(id == 'description_shoe'){
					$('#bit_shoe').focus();
				}
				if(id == 'bit_shoe'){
					$('#bit_description').focus();
				}
				if(id == 'bit_description'){
					$('#date_choose_datess').focus();
				}
				if(id == 'date_choose_datess'){
					$('#shoe_checkeds').focus();
				}
			}
		});

		$(document).on('keydown', '.custom-control-input', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(id == 'shoe_checkeds'){
				$('#shoeFunction_id').focus();
			}
		});

		function removeShoeingDetail(hourse_id,hourse_shoeing_id,id_remove){
			console.log(hourse_id);
			console.log(hourse_shoeing_id);
			console.log(id_remove);
			if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
			 	if(hourse_id == '0' && hourse_shoeing_id == '0'){
					alert('Delete Record Sucessfully');
					$('#'+id_remove+'').closest("tr").remove();
					return false;
				}

		        $.ajax({
                	url:'index.php?route=catalog/horse/deleteShoeDeatils&token=<?php echo $token; ?>'+'&hourse_id='+hourse_id+'&hourse_shoeing_id='+hourse_shoeing_id,
		           	method: "POST",
		           	dataType: 'json',
		            success: function(json)
		            {
						$('#'+id_remove+'').closest("tr").remove();
						alert(json['success']);
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Error deleting data');
		            }
		        });
		         return false;
     		}
		}
	</script>

	<script type="text/javascript">
	    // blanked value for stacked outstation tab 
		function closestackesearned(){
			$('#stacked_earneds_vanue').val('');
			$('#season_name').val('');
			$('#race_no').val('');
			$('#stacks_id').val('');
			$('#race_date').val('');
			$('#grade_stack_id').val('');
			$('#placing_id').val('');
			$('#error_race_date').hide();
			$('#error_stacked_earneds_vanue').hide();
			$('#error_season').hide();
			$('#error_race_no').hide();
			$('#error_stacks_id').hide();
			$('#error_placing_id').hide();
			$('#error_grade_stack_id').hide()

		}
		$('#myModal4').on('shown.bs.modal',function (){
			$('#stacked_earneds_vanue').focus();
		});

		$('#race_date').keyup(function(){
	        var race_date_valid =  $('#race_date').val();
	       	race_date_valid1 = race_date_valid.replace(/[^0-9-]+/i, '');
	       	$("#race_date").val(race_date_valid1);
	       	var race_date_valid_again =  $('#race_date').val();
		   	var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(race_date_valid_again))) {
				$('#error_race_date').show();
			    return false;
			} else {
				$('#error_race_date').hide();
			}
		});
		$('.input-race_date').datetimepicker().on('dp.change', function (e) {  
		    $('#error_race_date').css('display','none');
		});

		//stacked outstation tab 
		function StackedoutstationFunction(){
			var auto_id = $('#stacked_hidden_outstation_id').val();
			var race_date =  $( "#race_date" ).val();
			if(race_date != ''){
				$('#error_race_date').hide();
				var race_dates = race_date;
			} else {
				$('#error_race_date').show();
			}

			var stacked_earneds_vanues = $('#stacked_earneds_vanue').val();
			if (stacked_earneds_vanues != null && stacked_earneds_vanues != ''){
				$('#error_stacked_earneds_vanue').hide();
				var stacked_earneds_vanue = stacked_earneds_vanues;
			} else {
				$('#error_stacked_earneds_vanue').show();
			}

			var season_name = $('#season_name').val();
			if (season_name  != ''){
				$('#error_season').hide();
				var season_names = season_name;
			} else {
				$('#error_season').show();
			}

			var race_no = $('#race_no').val();
			if (race_no  != ''){
				$('#error_race_no').hide();
				var race_nos = race_no;
			} else {
				$('#error_race_no').show();
			}

			var stacks_id = $('#stacks_id').val();
			if (stacks_id  != ''){
				$('#error_stacks_id').hide();
				var stacks_ids = stacks_id;
			} else {
				$('#error_stacks_id').show();
			}

			var placing_id = $('#placing_id').val();
			if (placing_id  != ''){
				$('#error_placing_id').hide();
				var placing_ids = placing_id;
			} else {
				$('#error_placing_id').show();
			}

			var grade_stack_id = $('#grade_stack_id').val();
			if (grade_stack_id != null && grade_stack_id != ''){
				$('#error_grade_stack_id').hide();
				var grade_stack_ids = grade_stack_id.replace(/\s\s+/g, ' ');
			} else {
				$('#error_grade_stack_id').show();
			}

			inerror = 0;
			if(race_date != '' || race_date == ''){
				var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
				if (!(date_regex.test(race_date))) {
					$('#error_race_date').show();
					 inerror = 1;
				} else {
					$('#error_race_date').hide();
				}

				if (race_date == '' || stacked_earneds_vanues == '' || stacked_earneds_vanues == null  || season_name == '' || race_no == '' || stacks_id == '' || placing_id == '' || grade_stack_id == null  || grade_stack_id == ''){
					return false;
				}else if(inerror == 1){
					return false;
				}
			}

			html = '<tr id = "stackedoutstationrow'+auto_id+'">';
				html += '<td class="text-left" name= "stacked_earneds_vanuename_'+auto_id+'">'+stacked_earneds_vanue+'';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][stack_venue]"  value = '+stacked_earneds_vanue+'>';

				html += '<td class="text-left" name= "season_name'+auto_id+'" >'+season_names+'';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][season_name]"  value = \'' +season_names + '\'>';

				html += '<td class="text-left" name= "race_no'+auto_id+'" >'+race_nos+'';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][stack_race_no]"  value = '+race_nos+'>';

				html += '<td class="text-left" name= "placing_no'+placing_id+'" >'+placing_ids+'';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][stack_placing_no]"  value = '+placing_ids+'>';

				html += '<td class="text-left " name= "stacks_id'+auto_id+'" >'+stacks_ids+'';
				html += '</td>';
				html += '<input type= "hidden" class="stacks_out_id" name= "stackesdatas['+auto_id+'][stack_id]" id="stacks_id'+auto_id+'" value = '+stacks_ids+'>';
				
				html += '<td class="text-left" name= "race_date'+auto_id+'" >'+race_dates+'';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][stack_race_date]"  value = '+race_dates+'>';

				html += '<td class="text-left" name= "grade_stack_id'+auto_id+'" >'+ grade_stack_ids + '';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][grade_stack_id]"  value = \'' + grade_stack_ids + '\'>';
				html += '<td>';
				html += '<a onclick=removeStackOtDetail(0,0,"stackedoutstationrow'+auto_id+'") class="btn btn-danger"><i class="fa fa-minus-circle"></i></a></td>'; 
				html += '<input type= "hidden" name= "stackesdatas['+auto_id+'][horse_id]"  value ="0" >';
				html += '<input type= "hidden" name= "stackesdatas['+auto_id+'][horse_stackoutstation_id]" value ="0"  >';
			html += '</tr >';
			
			auto_id++;
			$('#stackedoutstationbody').append(html);
			$('#stacked_hidden_outstation_id').val(auto_id);
			var sum = 0;
			var from_db =  $('#stacks_dats').html();
			$('.stacks_out_id').each(function() {
				sum = parseInt(sum) + parseInt($(this).val());
			});
			$('#stacks_dats').html('');
			$('#stacks_dats').append(sum);
			$("#myModal4").modal("toggle");
		}

		$(document).on('keydown', '.form-control', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'stacked_earneds_vanue'){
					$('#season_name').focus();
				}
				if(id == 'season_name'){
					$('#race_no').focus();
				}
				if(id == 'race_no'){
					$('#stacks_id').focus();
				}
				if(id == 'stacks_id'){
					$('#race_date').focus();
				}
				if(id == 'race_date'){
					$('#grade_stack_id').focus();
				}
				if(id == 'grade_stack_id'){
					$('#placing_id').focus();
				}
				if(id == 'placing_id'){
					$('#Stackedoutstationsave_id').focus();
				}
			}
		});
		function removeStackOtDetail(hourse_id,hourse_stack_id,id_remove){
			console.log(hourse_id);
			console.log(hourse_stack_id);
			console.log(id_remove);
			if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
			 	if(hourse_id == '0' && hourse_stack_id == '0'){
			 		$('#'+id_remove+'').closest("tr").remove();
					var sum = 0;
					var from_db =  $('#stacks_dats').html();
					$('.stacks_out_id').each(function() {
						sum = parseInt(sum) + parseInt($(this).val());
					});
					$('#stacks_dats').html('');
					$('#stacks_dats').append(sum);
					alert('Delete Record Sucessfully');
					return false;
				}

		        $.ajax({
                	url:'index.php?route=catalog/horse/deleteStackDeatils&token=<?php echo $token; ?>'+'&hourse_id='+hourse_id+'&hourse_stack_id='+hourse_stack_id,
		           	method: "POST",
		           	dataType: 'json',
		            success: function(json)
		            {
						$('#'+id_remove+'').closest("tr").remove();
						var sum = 0;
						var from_db =  $('#stacks_dats').html();
						$('.stacks_out_id').each(function() {
							sum = parseInt(sum) + parseInt($(this).val());
						});
						$('#stacks_dats').html('');
						$('#stacks_dats').append(sum);
						alert(json['success']);
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Error deleting data');
		            }
		        });
		         return false;
     		}
		}
	</script>
	<script type="text/javascript">
		//trainer tab 
	    $('#input-trainer_name').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
				if(request != ''){
					$.ajax({
				  		url: 'index.php?route=catalog/horse/autocompleteTrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
				  		dataType: 'json',
				  		success: function(json) {   
				  			$('#trainer_codes_id').find('option').remove();
							response($.map(json, function(item) {
					  			return {
									label: item.trainer_name,
									value: item.trainer_name,
									trainer_id:item.trainer_id,
									trainer_codes:item.trainer_code
					  			}
							}));
				 	 	}
					});
				}
		  	}, 
		  	select: function(item) {
		  		console.log(item);
				$('#input-trainer_name').val(item.value);
				$('#trainer_id').val(item.trainer_id);
				$('#trainer_codes_id').val(item.trainer_codes);
				$('.dropdown-menu').hide();
				$('#date_charge_trainer').focus();
				return false;
		  	},
		});

	    $(document).on('keydown', '.form-control', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
	  		//alert(value);
			if(e.which == 13){
				console.log(name);
				console.log(class_name);
				console.log(id);
				if(id == 'date_charge_trainer' &&  value != ''){
					$('#arrival_time_trainer').focus();
				}
				if(id == 'arrival_time_trainer'){
					$('#extra_narrration_trainer').focus();
				}
				if(id == 'extra_narrration_trainer'){
					$('#date_left_trainer').focus();
				}
				if(id == 'date_left_trainer'){
					$('#extra_narrration_two_trainer').focus();
				}
				if(id == 'extra_narrration_two_trainer'){
					$('#input-trainer_name').focus();
				}
			}
		});
	</script>
	<script type="text/javascript">
		//hourse tab
		var iscolor_checked = $('input[name=\'official_name_change\']:checked').val();
		if (iscolor_checked == 1) {
	    $('#hourse_names_changes').show();
	    } else {
	    	$('#hourse_names_changes').hide();
	    }

		$('#official_name_change').change(function() {
    	if ($(this).is(':checked')) {
		    $('#hourse_names_changes').show();
		    } else {
		    	$('#hourse_names_changes').hide();
		    }
		});
		
		var timer = null;
		$('#registeration_date').keyup(function(){
	       	clearTimeout(timer); 
	       $('#error_registeration_date_post').hide();
	        var registeration_date_valid =  $('#registeration_date').val();
	       	registeration_date_valid1 = registeration_date_valid.replace(/[^0-9-]+/i, '');
	       	$("#registeration_date").val(registeration_date_valid1);
	       	timer = setTimeout(checkregi, 800)
		});

		function checkregi() {
		   var registeration_date_valid =  $('#registeration_date').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(registeration_date_valid))) {
				$('#error_registeration_date').show();
			    return false;
			} else {
				$('#error_registeration_date').hide();
			}
		}

		$('.input-registeration_date').datetimepicker().on('dp.change', function (e) {  
		    $('#error_registeration_date').css('display','none');
	      	$('#error_registeration_date_post').hide();
		});

		var timer = null;
		$('#date_left_trainer').keyup(function(){
	       	clearTimeout(timer); 
	       	$('#error_date_left_trainer_post').hide();
	        var date_left_trainer_valid =  $('#date_left_trainer').val();
	       	date_left_trainer_valid1 = date_left_trainer_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_left_trainer").val(date_left_trainer_valid1);
	       	timer = setTimeout(checklefttrainer, 800)
		});

		function checklefttrainer() {
		   var date_left_trainer_valid =  $('#date_left_trainer').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_left_trainer_valid))) {
				$('#error_date_left_trainer').show();
			    return false;
			} else {
				$('#error_date_left_trainer').hide();
			}
		}

		$('.input-date_left_trainer').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_left_trainer').css('display','none');
		    $('#error_date_left_trainer_post').css('display','none');
		});

		var timer = null;
		$('#date_charge_trainer').keyup(function(){
	       	clearTimeout(timer); 
	       	$('#error_date_charge_trainer_post').hide();
	        var date_charge_trainer_valid =  $('#date_charge_trainer').val();
	       	date_charge_trainer_valid1 = date_charge_trainer_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_charge_trainer").val(date_charge_trainer_valid1);
	       	timer = setTimeout(checkchargetrainer, 800)
		});

		function checkchargetrainer() {
		   var date_charge_trainer_valid =  $('#date_charge_trainer').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_charge_trainer_valid))) {
				$('#error_date_charge_trainer').show();
			    return false;
			} else {
				$('#error_date_charge_trainer').hide();
			}
		}

		$('.input-date_charge_trainer').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_charge_trainer').css('display','none');
		    $('#error_date_charge_trainer_post').css('display','none');
		});

		var timer = null;
		$('#date_std_stall_certificate').keyup(function(){
	       	clearTimeout(timer); 
	       	$('#error_date_std_stall_certificate_post').hide();
	        var date_std_stall_certificate_valid =  $('#date_std_stall_certificate').val();
	       	date_std_stall_certificate_valid1 = date_std_stall_certificate_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_std_stall_certificate").val(date_std_stall_certificate_valid1);
	       	timer = setTimeout(stallcertificate, 800)
		});

		function stallcertificate() {
		   var date_std_stall_certificate_valid =  $('#date_std_stall_certificate').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_std_stall_certificate_valid))) {
				$('#error_date_std_stall_certificate').show();
			    return false;
			} else {
				$('#error_date_std_stall_certificate').hide();
			}
		}

		$('.input-date_std_stall_certificate').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_std_stall_certificate').css('display','none');
		    $('#error_date_std_stall_certificate_post').css('display','none');
		});

		var timer = null;
		$('#date_foal_date').keyup(function(){
	       	clearTimeout(timer); 
	       	$('#error_date_foal_date_post').hide();
	        var date_foal_date_valid =  $('#date_foal_date').val();
	       	date_foal_date_valid1 = date_foal_date_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_foal_date").val(date_foal_date_valid1);
	       	foaldate();
		});

		function foaldate() {
		   var date_foal_date_valid =  $('#date_foal_date').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_foal_date_valid))) {
				$('#error_date_foal_date').show();
			    return false;
			} else {
					$('#error_date_foal_date').hide();
					if(date_foal_date_valid != ''){
					var today = new Date();
					var dd = String(today.getDate()).padStart(2, '0');
					var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
					var yyyy = today.getFullYear();
		    		var current_date = yyyy + "-" + mm + "-" + dd;

		    		var date_foal_date_for_age1 = date_foal_date_valid.split('-');
			        var s_date  = date_foal_date_for_age1[0];
			        var s_month = date_foal_date_for_age1[1];
			        var s_year  = date_foal_date_for_age1[2];

			        var start = s_year + "-" + s_month + "-" + s_date;

					var starts = new Date(start);
				    var end   = new Date(current_date);

				    diff  = new Date(end - starts);
				    days  = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
					$('#input-age').val(days);
					$('#input-age').attr('value', days);
				}
			}
		}



		$('.input-date_foal_date').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_foal_date').css('display','none');
		    $('#error_date_foal_date_post').css('display','none');

		    var date_foal_date_for_age =  $('#date_foal_date').val();
			if(date_foal_date_for_age != ''){
				var today = new Date();
				var dd = String(today.getDate()).padStart(2, '0');
				var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
				var yyyy = today.getFullYear();
	    		var current_date = yyyy + "-" + mm + "-" + dd;

	    		var date_foal_date_for_age1 = date_foal_date_for_age.split('-');
		        var s_date  = date_foal_date_for_age1[0];
		        var s_month = date_foal_date_for_age1[1];
		        var s_year  = date_foal_date_for_age1[2];

		        var start = s_year + "-" + s_month + "-" + s_date;

				var starts = new Date(start);
			    var end   = new Date(current_date);

			    diff  = new Date(end - starts);
			    days  = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
				$('#input-age').val(days);
				$('#input-age').attr('value', days);
			}

		    
		});
		var timer = null;
		$('#change_horse_date').keyup(function(){
	       	clearTimeout(timer); 
	       	$('#error_change_horse_date_post').hide();
	        var change_horse_date_valid =  $('#change_horse_date').val();
	       	change_horse_date_valid1 = change_horse_date_valid.replace(/[^0-9-]+/i, '');
	       	$("#change_horse_date").val(change_horse_date_valid1);
	       	timer = setTimeout(changehorse, 800)
		});

		function changehorse() {
		   var change_horse_date_valid =  $('#change_horse_date').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(change_horse_date_valid))) {
				$('#error_change_horse_date').show();
			    return false;
			} else {
				$('#error_change_horse_date').hide();
			}
		}

		$('.input-change_horse_dates').datetimepicker().on('dp.change', function (e) {  
		    $('#error_change_horse_date').css('display','none');
		    $('#error_change_horse_date_post').css('display','none');
		});

		$(document).on('keydown', '.form-control', '.form-group', function(e) {
		var name = $(this).attr('name'); 
	  	var class_name = $(this).attr('class'); 
	  	var id = $(this).attr('id');
	  	var value = $(this).val();
			if(e.which == 13){
				if(id == 'input-horse'){
					$('#official_name_change').focus();
				}

				if(id == 'official_name_change'){
					if ($('#hourse_names_changes').is(':visible')) {
						$('#change_horse_name').focus();
					} else {
						 $('#input-passport').focus();
					}
				}

				if(id == 'change_horse_name'){
					$('#change_horse_date').focus();
				}

				if(id == 'change_horse_date'){
					$('#month_change_horse_date').focus();
				}

				if(id == 'month_change_horse_date'){
					$('#year_change_horse_date').focus();
				}

				if(id == 'year_change_horse_date'){
					$('#input-passport').focus();
				}

				if(id == 'input-passport'){
					$('#registeration_date').focus();
				}

				if(id == 'registeration_date'){
					$('#month_registeration_date').focus();
				}

				if(id == 'month_registeration_date'){
					$('#year_registeration_date').focus();
				}

				if(id == 'year_registeration_date'){
					$('#registerarton_authentication_id').focus();
				}

				if(id == 'registerarton_authentication_id'){
					$('#input-sire').focus();
				}

				if(id == 'input-sire'){
					$('#input-dam_nat').focus();
				}

				if(id == 'input-dam_nat'){
					$('#input-micro_chip1').focus();
				}

				if(id == 'input-micro_chip1'){
					$('#input-micro_chip_2').focus();
				}

				if(id == 'input-micro_chip_2'){
					$('#arrival_charges_to_be_paid').focus();
				}

				if(id == 'arrival_charges_to_be_paid'){
					$('#date_foal_date').focus();
				}

				if(id == 'date_foal_date'){
					$('#month_foal_date').focus();
				}

				if(id == 'month_foal_date'){
					$('#year_foal_date').focus();
				}

				if(id == 'year_foal_date'){
					$('#input-color').focus();
				}

				if(id == 'input-age'){
					$('#input-color').focus();
				}

				if(id == 'input-color'){
					$('#input_origin').focus();
				}
				if(id == 'input_origin'){
					$('#input_sex').focus();
				}

				if(id == 'input_sex'){
					$('#input-age').focus();
				}

				if(id == 'input-age'){
					$('#input-stud_farm').focus();
				}

				if(id == 'input-stud_farm'){
					$('#input-rating').focus();
				}

				if(id == 'input-rating'){
					$('#input-breeder').focus();
				}

				if(id == 'input-breeder'){
					$('#input-saddle_no').focus();
				}

				if(id == 'input-saddle_no'){
					$('#input-octroi').focus();
				}

				if(id == 'input-octroi'){
					$('#input-awbi').focus();
				}

				if(id == 'input-awbi'){
					$('#awbi_registration_file').focus();
				}

				if(id == 'awbi_registration_file'){
					$('#date_std_stall_certificate').focus();
				}

				if(id == 'date_std_stall_certificate'){
					$('#month_std_stall_certificate').focus();
				}

				if(id == 'month_std_stall_certificate'){
					$('#year_std_stall_certificate').focus();
				}

				if(id == 'year_std_stall_certificate'){
					$('#input-id_by_brand').focus();
				}

				if(id == 'input-id_by_brand'){
					$('#input-stall_certificate').focus();
				}

				if(id == 'input-stall_certificate'){
					$('#horse_remarks').focus();
				}

				if(id == 'horse_remarks'){
					$('#input-horse').focus();
				}
			}
		});

		$('#button-other_document_1').on('click', function() {
		$('#form-other_document_1').remove();
		$('body').prepend('<form enctype="multipart/form-data" id="form-other_document_1" style="display: none;"><input type="file" name="file" /></form>');
		$('#form-other_document_1 input[name=\'file\']').trigger('click');
		if (typeof timer != 'undefined') {
			  clearInterval(timer);
		}
		timer = setInterval(function() {
			if ($('#form-other_document_1 input[name=\'file\']').val() != '') {
			  clearInterval(timer); 
			  image_name = 'Awbi_Registeration_file';  
			  $.ajax({ 
				url: 'index.php?route=catalog/horse/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
				type: 'post',   
				dataType: 'json',
				data: new FormData($('#form-other_document_1')[0]),
				cache: false,
				contentType: false,
				processData: false,   
				beforeSend: function() {
				  $('#button-upload').button('loading');
				},
				complete: function() {
				  $('#button-upload').button('reset');
				},  
				success: function(json) {
				  if (json['error']) {
					alert(json['error']);
				  }
				  if (json['success']) {
					alert(json['success']);
					console.log(json);
					$('input[name=\'awbi_registration_file\']').attr('value', json['filename']);
					$('input[name=\'awbi_registration_file_source\']').attr('value', json['link_href']);
					d = new Date();
					var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="awbi_registration_file_source" href="'+json['link_href']+'">View Document</a>';
					$('#awbi_registration_file_source').remove();
					$('#button-other_document_1_new').append(previewHtml);
				  }
				},      
				error: function(xhr, ajaxOptions, thrownError) {
				  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			  });
			}
		  }, 500);
		});
	</script>
</div>
<?php echo $footer; ?>