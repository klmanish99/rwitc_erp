<?php
class Calender {

	public function __construct($registry) { 
		$this->session = $registry->get('session'); 
	}
	
/* draws a calendar */
	function draw_calendar($month,$year,$events = array()){

	/* draw table */
		$calendar = '<table style="width: -webkit-fill-available;height:100px;" cellpadding="0" cellspacing="0" class="calendar">';

		/* table headings */
		$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
		$calendar.= '<tr style="text-decoration: underline; font-size: initial;font-size: small;" class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">&nbsp;&nbsp;',$headings).'</td></tr>';

		/* days and weeks vars now ... */
		$running_day = date('w',mktime(0,0,0,$month,1,$year));
		$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
		$days_in_this_week = 1;
		$day_counter = 0;
		$dates_array = array();

		/* row for week one */
		$calendar.= '<tr class="calendar-row">';

		/* print "blank" days until the first of the current week */
		for($x = 0; $x < $running_day; $x++):
			$calendar.= '<td class="calendar-day-np">&nbsp;</td>';
			$days_in_this_week++;
		endfor;

		/* keep going with days.... */
		for($list_day = 01; $list_day <= $days_in_month; $list_day++):
			$calendar.= '<td class="calendar-day"><div style="position:relative;height:40px;width: 80%;font-size: 175%;">';
				/* add in the day number */
				if(strlen($list_day) == 1) {
					$list_day = '0'.$list_day;
				}
				if(strlen($month) == 1) {
					$month = '0'.$month; 
				}
				$calendar.= '<div style="display: none;" class="day-number">'.$list_day.'</div>';
				$event_day = $year.'-'.$month.'-'.$list_day;
				//if(isset($events[$event_day])) {
					//foreach($events[$event_day] as $event) {
						$dailyadd = HTTP_SERVER.'index.php?route=catalog/events&token='.$this->session->data['token'].'&date='.$event_day; 
						$calendar.= '<div style="text-align: center; width: 100%; border: groove;" class="event"><a href = "'.$dailyadd.'" class = "selected1">'.$list_day.'</a></div>';
					
					//}
				//}
				//else {
					//$calendar.= str_repeat('<p>&nbsp;</p>',2);
				//}
			$calendar.= '</div></td>';
			if($running_day == 6):
				$calendar.= '</tr>';
				if(($day_counter+1) != $days_in_month):
					$calendar.= '<tr class="calendar-row">';
				endif;
				$running_day = -1;
				$days_in_this_week = 0;
			endif;
			$days_in_this_week++; $running_day++; $day_counter++;
		endfor;

		/* finish the rest of the days in the week */
		if($days_in_this_week < 8):
			for($x = 1; $x <= (8 - $days_in_this_week); $x++):
				$calendar.= '<td class="calendar-day-np">&nbsp;</td>';
			endfor;
		endif;

		/* final row */
		$calendar.= '</tr>';
		

		/* end the table */
		$calendar.= '</table>';

		/** DEBUG **/
		$calendar = str_replace('</td>','</td>'."\n",$calendar);
		$calendar = str_replace('</tr>','</tr>'."\n",$calendar);
		
		/* all done, return result */
		return $calendar;
	}

	function random_number() {
		srand(time());
		return (rand() % 7);
	}

	/* sample usages */
	// echo '<h2>July 2009</h2>';
	// echo draw_calendar(7,2009);
}
?>