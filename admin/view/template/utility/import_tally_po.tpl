<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <!-- <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div> -->
  </div>
  <link type="text/css" href="view/stylesheet/myform.css" rel="stylesheet" media="screen" />
  <div class="container-fluid">
     <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-cloud-upload" aria-hidden="true"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="pull-right btn btn-default"><i class="fa fa-reply"></i></a></div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-amount" class="form-horizontal">
            <p style="font-size: 20px;font-weight: bolder;">Choose Excel File Only</p>
            <div class="row  mb-3 mt-3">
                <div class="custom-file col-sm-8 form-control" style="width: 85%;margin-left: 1%">
                    <input type="file" name="import_data" class="custom-file-input" accept="DBF" id="import"  >
                </div>
                <div class="col-sm-2" style="margin-top:3%">
                    <button type="submit" name="save" class="btn btn-primary" >Import</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
    pickTime: false
});
//--></script></div>
<script type="text/javascript">
    $('.input').keypress(function (e) {
      if (e.which == 13) {
        $('#form-amount').submit();
        return false;    //<---- Add this line
      }
    });
</script>
<?php echo $footer; ?>