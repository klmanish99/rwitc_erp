<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
      </div>
      <h1>Events</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Events List</h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td class="text-center">Event Name</td>
                        <td style="width: 15%" class="text-center">Event Date</td>
                        <td style="width: 40%" class="text-center">Description</td>
                        <td style="width: 15%" class="text-center">Edit</td>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($categories) { ?>
                    <?php foreach ($categories as $category) { ?>
                    <tr>
                        <td class="text-left"><?php echo $category['event_name']; ?></td>
                        <td style="width: 15%" class="text-center"><?php echo date('d-m-Y', strtotime($category['date_of_event'])); ?></td>
                        <td style="width: 40%" class="text-center"><?php echo $category['description']; ?></td>
                        <td style="width: 15%" class="text-center"><a href="<?php echo $category['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td> 
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <!-- <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div> -->
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>