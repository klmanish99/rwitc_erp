<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
                </div>
                <div class="panel-body">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="race_type"><?php echo "Race Type:"; ?></label>
                                <div class="col-sm-3">
                                    <select name="race_type" id="race_type" class="form-control"  tabindex="">
                                        <option value="0">Please Select</option>
                                        <?php foreach ($race_types as $skey => $svalue) { ?>
                                        <?php if ($skey == $race_type) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>


                                    </select>
                                    <?php if (isset($race_type)) { ?><span class="errors" style="color: #ff0000;"><?php echo $error_racetype; ?></span><?php } ?>
                                     
                                </div>
                                <div id="hide_class" style="display: none;">
                                    <label class="col-sm-2 control-label" for="class"><?php echo "Class:"; ?></label>
                                    <div class="col-sm-3">
                                        <select name="race_class" id="class" class="form-control"  tabindex="">
                                            <option value="0">Please Select</option>
                                            <?php foreach ($classs as $skey => $svalue) { ?>
                                                <?php if ($skey == $race_class) { ?>
                                                    <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                        
                                    </div>
                                </div>
                                <div id="hide_grade" style="display: none;">
                                    <label class="col-sm-2 control-label" for="grade"><?php echo "Grade:"; ?></label>
                                    <div class="col-sm-3">
                                        <select name="race_grade" id="grade" class="form-control"  tabindex="">
                                            <option value="<?php  ?>">Please Select</option>
                                            <?php foreach ($terms as $skey => $svalue) { ?>
                                            <?php if ($svalue == $race_grade) { ?>
                                            <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>

                                    </div>
                                </div>
                            </div>
                        <!-- <div class="form-group lower_class">
                            <label class="col-sm-2 control-label" for="lower">Lower Class Eligible</label>
                           <div class="custom-control custom-checkbox">
                            <?php if($lower == 'Yes' ){ ?>
                                <input type="checkbox" name="lower" class="form-control" id="lower" checked="checked">
                            <?php } else { ?>
                                <input type="checkbox" name="lower" class="form-control" id="lower">
                            <?php } ?>  
							</div>
                        </div> -->
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="desc">Description</label>
                                <div class="col-sm-8">
                                    <input type="text" name="desc" value="<?php echo $desc?>" placeholder="Description" id="desc" class="form-control" data-index="1" />
                                    <?php if (isset($valierr_desc)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_desc; ?></span><?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="cat">Category</label>
                                <div class="col-sm-8">
                                    <input type="text" name="cat" value="<?php echo $cat?>" placeholder="Category" id="cat" class="form-control" data-index="1" />
                                    <?php if (isset($valierr_cat)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_cat; ?></span><?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="rating_from">Rating From</label>
                                <div class="col-sm-2">
                                    <div class="input-group date input-rating_from">
                                        <input type="number" name="rating_from" value="<?php echo $rating_from?>" placeholder="Rating From"  id="rating_from" class="form-control" />
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label" for="rating_to">Rating to</label>
                                <div class="col-sm-2">
                                    <div class="input-group date input-rating_to">
                                        <input type="number" name="rating_to" value="<?php echo $rating_to?>" placeholder="Rating From"  id="rating_to" class="form-control" />
                                    </div>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_race_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="lower">Lower Class Eligible</label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="l_desc">Description</label>
                                <div class="col-sm-8">
                                    <input type="text" name="l_desc" value="<?php echo $l_desc?>" placeholder="Description" id="l_desc" class="form-control" data-index="1" />
                                    <!-- <?php if (isset($valierr_desc)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_desc; ?></span><?php } ?> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="l_cat">Category</label>
                                <div class="col-sm-8">
                                    <input type="text" name="l_cat" value="<?php echo $l_cat?>" placeholder="Category" id="l_cat" class="form-control" data-index="1" />
                                    <!-- <?php if (isset($valierr_cat)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_cat; ?></span><?php } ?> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="l_rating_from">Rating From</label>
                                <div class="col-sm-2">
                                    <div class="input-group date input-l_rating_from">
                                       <input type="number" name="l_rating_from" value="<?php echo $l_rating_from?>" placeholder="Rating From"  id="l_rating_from" class="form-control" />
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label" for="l_rating_to">Rating to</label>
                                <div class="col-sm-2">
                                    <div class="input-group date input-l_rating_to">
                                        <input type="number" name="l_rating_to" value="<?php echo $l_rating_to?>" placeholder="Rating From"  id="l_rating_to" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
           
            
<script type="text/javascript">
    $('#language a:first').tab('show');
</script></div>

<script type="text/javascript">
    $( document ).ready(function() {
        var select_h =  $('#race_type').val();
        if (select_h == 'Handicap Race') {
            $('#hide_class').show();

            $('.lower_class').show();
        } else if (select_h == 'Term Race') {
            $('#hide_grade').show();
            $('.lower_class').hide();
        } else {
            $('#hide_class').hide();
            $('.lower_class').show();
            $('#hide_grade').hide();

        
        }
    });

    $('#race_type').change(function() {
        var select_t =  $('#race_type').val();
        if (select_t == 'Term Race') {
            $('#hide_grade').show();
            $('.lower_class').hide();
            $('#hide_class').hide();
        } else  if (select_t == 'Handicap Race') {
            $('#hide_class').show();
            $('.lower_class').show();
            $('#hide_grade').hide();
            
        } else {
            $('#hide_grade').hide();
            $('#hide_class').hide();
            $('.lower_class').show();
        }
    });

    $(document).ready(function(){
        $('.date').datetimepicker({
            pickTime: false
        });
    });
</script>
<?php echo $footer; ?>