<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-search"></i> <?php echo 'Jockey'; ?></h3>
            </div>
            <div class="panel-body">
                <div class="form-group" >
                    <label class="col-sm-2 control-label" for="input-trainer"><b style="color: red;"> * </b> <?php echo "Jockey Name:"; ?></label>
                    <div class="col-sm-3">
                        <input type="text"  name="filterJockeyName" id="filterJockeyName" value="" placeholder="Jockey Name" class="form-control" />
                        <input type="hidden" name="filterJockeyId" id="filterJockeyId" value="<?php echo $horse_id ?>"  class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">

        $('#filterJockeyName').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#filterTrainerId').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/jockey_at_glance/autocompleteJockey&token=<?php echo $token; ?>&jockey_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.jockey_name,
                                    value: item.jockey_name,
                                    jockey_id:item.jockey_id,
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                console.log(ui.item);
                $('#filterJockeyName').val(ui.item.value);
                $('#filterJockeyId').val(ui.item.jockey_id);
                var jockey_id =  $('#filterJockeyId').val();
                url = 'index.php?route=catalog/jockey/edit1&token=<?php echo $token; ?>';
                url += '&back=' + 1;
                if (jockey_id) {
                    url += '&jockey_id=' + encodeURIComponent(jockey_id);
                }
                location = url;
                return false;
            },
        });
    </script>
</div>
<?php echo $footer; ?>