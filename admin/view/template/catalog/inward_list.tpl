<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header" >
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> 
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Go To Dashboard" class="btn btn-primary">Go To Dashboard</a>
				<button style="display: none;"> type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-inward').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<h1>Inward</h1>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i>Inward</h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-inward">
					<div class="well">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="col-sm-3">
										<input type="text" name="filter_inward" value="<?php echo $filter_inward; ?>" placeholder="<?php echo "Inward Req. No"; ?>" id="input-filter_inward" class="form-control" />
									</div>
									<div class="col-sm-3">
										<input type="text" name="filter_order_no" value="<?php echo $filter_order_no; ?>" placeholder="<?php echo "Inward Code"; ?>" id="input-filter_order_no" class="form-control" />
									</div>
									<div class="col-sm-3">
					                    <select name="filter_status" id="filter_status" class="form-control">
					                        <?php foreach ($status as $key => $tvalue) { ?>
					                            <?php if($key == $filter_status){ ?>
					                            	<option value="<?php echo $key ?>" selected="selected"><?php echo $tvalue?></option>
					                            <?php } else { ?>
					                            	<option value="<?php echo $key ?>"><?php echo $tvalue?></option>
					                            <?php } ?>
					                        <?php } ?>    
					                    </select>
					                </div>
									<div class="col-sm-3">
										<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<?php if($is_user == '0'){ ?>
										<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
										<?php } ?>
										
										<td class="text-left"><?php echo 'Inward Code:'; ?></td>
										<td class="text-left"><?php echo 'Date'; ?></td>
										<td class="text-left"><?php echo 'Logs'; ?></td>
										<td class="text-right"><?php echo 'View'; ?></td>
									</tr>
								</thead>
								<tbody>
									<?php if ($inwards) { ?>
										<?php $i = 1; ?>
										<?php foreach ($inwards as $inward) { ?>
										<tr>
											<?php if($is_user == '0'){ ?>
											<td style="display: none;" class="text-center"><?php if (in_array($inward['order_id'], $selected)) { ?>
												<input type="checkbox" name="selected[]" value="<?php echo $inward['order_id']; ?>" checked="checked" />
												<?php } else { ?>
												<input type="checkbox" name="selected[]" value="<?php echo $inward['order_id']; ?>" />
												<?php } ?>
											</td>
											<?php } ?>
										 
											
											<td class="text-left">
												<a data-toggle="modal" data-target="#myModal" class=""><?php echo $inward['order_no']; ?></a>
												<input type="hidden" name="count" class="count" value="<?php echo $i; ?>" />
												<input type="hidden" name="order_id-<?php echo $inward['order_id'] ?>" id="order_id-<?php echo $i ?>" value="<?php echo $inward['order_id']; ?>" />
											</td>
											<td class="text-left"><?php echo date('d-M-Y', strtotime($inward['date'])); ?></td>
											<td class="text-left"><?php echo $inward['log_datas']; ?></td>
											<td style= "display:none;" class="text-right">
												<a data-toggle="modal" data-target="#myModal" class=""><?php echo $inward['narration']; ?></a>
											</td>
											<td class="text-right">
												<?php if($inward['approval_status'] == 1){ ?>
													<a href="<?php echo $inward['email']; ?>" data-toggle="tooltip" title="<?php echo 'Email'; ?>" class="btn btn-primary"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
													<a href="<?php echo $inward['print']; ?>" data-toggle="tooltip" title="<?php echo 'Print'; ?>" class="btn btn-primary"><i class="fa fa-print"></i></a>
												<?php } ?>
												<?php if($inward['approval_status'] == 0){ ?>
													<a href="<?php echo $inward['edit']; ?>" data-toggle="tooltip" title="<?php echo 'Edit'; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
												<?php } ?>
												<a href="<?php echo $inward['edit1']; ?>" data-toggle="tooltip" title="<?php echo 'View'; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
											</td>
										</tr>
										<?php $i ++; ?>
										<?php } ?>
									<?php } else { ?>
									<tr>
										<td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>
		        <div class="row">
		        	<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		        	<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		        </div>
	        </div>
		</div>
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Finished Product Overview</h4>
					</div>
					<div class="modal-body edit-content">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--

$('#myModal').on('show.bs.modal', function(e) {
	var $modal = $(this),
	esseyId = e.relatedTarget.id;
	idsss = e.relatedTarget.className;
	s_ids = idsss.split(' ');
	idss = s_ids[1];
	s_id = idss.split('-');
	id = s_id[1];
	order_id = $('#order_id-'+id).val();
	//console.log(productfinished_id);
	//return false;
	$.ajax({
		cache: false,
		type: 'POST',
		url: 'index.php?route=catalog/inward/getdata&token=<?php echo $token; ?>&filter_order_id=' +  encodeURIComponent(order_id),
		data: 'filter_order_id=' + order_id,
		success: function(data) {
			//console.log(data.html);
			$modal.find('.edit-content').html(data.html);
		}
	});
})

$('#button-filter').on('click', function() {
  var url = 'index.php?route=catalog/inward&token=<?php echo $token; ?>';

  var filter_inward = $('input[name=\'filter_inward\']').val();
 
  if (filter_inward) {
	var filter_order_id = $('input[name=\'filter_order_id\']').val();
	if (filter_order_id) {
	  url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}
	url += '&filter_inward=' + encodeURIComponent(filter_inward);
  
  }

  var filter_order_no = $('input[name=\'filter_order_no\']').val();
  if (filter_order_no) {
    url += '&filter_order_no=' + encodeURIComponent(filter_order_no);
  }

  var filter_status = $('select[name=\'filter_status\']').val();
  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

 //   var filter_order_no = $('input[name=\'filter_order_no\']').val();
 	
 //  if (filter_order_no) {
	// var filter_order_id = $('input[name=\'filter_order_id\']').val();
	// if (filter_order_id) {
	//   url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	// }
	// url += '&filter_order_no=' + encodeURIComponent(filter_order_no);
  
 //  }

	var filter_productsort = $('select[name=\'filter_productsort\']').val();
	//alert(filter_productsort);

	if (filter_productsort) {
	  url += '&filter_productsort=' + encodeURIComponent(filter_productsort);
	}

  /*
  var filter_supplier = $('input[name=\'filter_supplier\']').val();

  if (filter_supplier) {
	var filter_supplier_id = $('input[name=\'filter_supplier_id\']').val();
	if (filter_supplier_id) {
	  url += '&filter_supplier_id=' + encodeURIComponent(filter_supplier_id);
	}
	url += '&filter_supplier=' + encodeURIComponent(filter_supplier);
  }
  */

  location = url;
});

$('input[name=\'filter_inward\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/inward/autocomplete&token=<?php echo $token; ?>&filter_order_id=' +  encodeURIComponent(request),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['order_id'],
			value: item['order_id']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter_inward\']').val(item['label']);
	$('input[name=\'filter_order_id\']').val(item['value']);
  }
});

// $('input[name=\'filter_order_no\']').autocomplete({
//   'source': function(request, response) {
// 	$.ajax({
// 	  url: 'index.php?route=catalog/inward/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
// 	  dataType: 'json',
// 	  success: function(json) {
// 		response($.map(json, function(item) {
// 		  return {
// 			label: item['order_no'],
// 			value: item['order_id']
// 		  }
// 		}));
// 	  }
// 	});
//   },
//   'select': function(item) {
// 	$('input[name=\'filter_order_no\']').val(item['label']);
// 	$('input[name=\'filter_order_id\']').val(item['label']);
//   }
// });

/*
var DELAY = 700, clicks = 0, timer = null;
$(function(){
  $("a.common_class").on("click", function(e){
	//console.log(e.toElement.className);
	clicks++;  //count clicks
	if(clicks === 1) {
	  timer = setTimeout(function() {
		
		//idsss = $('.common_class').attr('class');
		idsss = e.toElement.className;
		s_ids = idsss.split(' ');
		idss = s_ids[1];
		s_id = idss.split('-');
		id = s_id[1];
		productfinished_id = $('#productfinished_id-'+id).val();
		base_link = '<?php echo $base_link ?>';
		base_link = base_link.replace('&amp;', '&');
		final_url = base_link+'&productfinished_id='+productfinished_id;
		//alert(final_url);
		window.location.href = final_url;  //perform single-click action    
		
		clicks = 0;             //after action performed, reset counter
	  }, DELAY);
	} else {
	  clearTimeout(timer);    //prevent single-click action
	  //idsss = $('.common_class').attr('class');
	  idsss = e.toElement.className;
	  s_ids = idsss.split(' ');
	  idss = s_ids[1];
	  s_id = idss.split('-');
	  id = s_id[1];
	  productfinished_id = $('#productfinished_id-'+id).val();
	  base_link = '<?php echo $base_link_1 ?>';
	  base_link = base_link.replace('&amp;', '&');
	  final_url = base_link+'&productfinished_id='+productfinished_id;
	  //alert(final_url);
	  window.location.href = final_url;  //perform double-click action
	  clicks = 0;             //after action performed, reset counter
	}
  }).on("dblclick", function(e){
	e.preventDefault();  //cancel system double-click event
  });
});
*/

$(document).ready(function()               
    {
        // enter keyd
        $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                 $('#button-filter').trigger('click');
             }
        });
    });

//--></script>
<script type="text/javascript">
$( document ).ready(function() {
	var refer = "<?php echo $refer; ?>";
	if(refer == '1') {
	  close();
	}
});
</script>
<?php echo $footer; ?>