<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="short_name">Short Name</label>
                <div class="col-sm-4">
                  <input type="text" name="short_name" value="<?php echo $short_name; ?>" placeholder="Short Name" id="short_name" class="form-control" data-index="1" />
                  <?php if (isset($valierr_short_name)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_short_name; ?></span><?php } ?>
                </div>
                <label class="col-sm-2 control-label" for="long_name">Long Name</label>
                <div class="col-sm-4">
                  <input type="text" name="long_name" value="<?php echo $long_name; ?>" placeholder="Long Name" id="long_name" class="form-control" data-index="2" />
                </div>
              </div>
              <!-- <div class="form-group">
                <label class="col-sm-2 control-label" for="select-type">Status</label>
                <div class="col-sm-3">
                    <select name="type" id="select-type" class="form-control" data-index="3">
                        <?php foreach ($Active as $key => $value) { ?>
                        <?php if ($value == $type) { ?>
                          <option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
                        <?php } else { ?>
                          <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
              </div> -->
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</script>

<script type="text/javascript">
    $(document).ready(function(){
    $('[data-index= 1]').focus();
});
$('#form-category').on('keydown','input', function (event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-index'));
        $('[data-index="' + (index + 1).toString() + '"]').focus();
    }
});

$('#form-category').on('keydown','select', function (event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-index'));
        //console.log(index);
        $('[data-index="' + (index + 1).toString() + '"]').focus();
    }
});

</script>
  
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script></div>
<?php echo $footer; ?>