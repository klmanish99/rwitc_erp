<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-search"></i> <?php echo 'Horse'; ?></h3>
            </div>
            <div class="panel-body">
                <div class="form-group" >
                    
                    <div class="col-sm-4">
                    <label class="control-label" for="input-horse"> <?php echo "Horse Name:"; ?></label>
                        <input type="text"  name="filterHorseName" id="filterHorseName" value="<?php echo $horse_name ?>" placeholder="Search By Horse Name" class="form-control" />
                        <input type="hidden" name="filterHorseId" id="filterHorseId" value="<?php echo $horse_id ?>"  class="form-control">
                    </div>

                    <div class="col-sm-4">
                    <label class="control-label" for="input-horse"> <?php echo "Old Horse Name:"; ?></label>
                        <input type="text"  name="filterOldHorseName" id="filterOldHorseName" value="" placeholder="Search By Old Horse Name" class="form-control" />
                        <input type="hidden" name="filterOldHorseId" id="filterOldHorseId" value=""  class="form-control">
                    </div>

                    <div class="col-sm-4">
                    <label class="control-label" for="input-horse"><!-- <b style="color: red;"> * </b> --> <?php echo "Dame Name:"; ?></label>
                        <input type="text"  name="filterMareName" id="filterMareName" value="" placeholder=" Search By Dame Name" class="form-control" />
                        <input type="hidden" name="filterMareId" id="filterMareId" value=""  class="form-control">
                    </div>
                </div>

               

            </div>
             <div class="form-group mare_datas">
                    
                </div>

        </div>
    </div>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">

        $('#filterHorseName').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#filterHorseId').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/horse_at_glance/autocompleteHorse&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.horse_name,
                                    value: item.horse_name,
                                    horse_id:item.horse_id,
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                console.log(ui.item);
                $('#filterHorseName').val(ui.item.value);
                $('#filterHorseId').val(ui.item.horse_id);
                var horse_id =  $('#filterHorseId').val();
                url = 'index.php?route=catalog/horse/view&token=<?php echo $token; ?>';
                url += '&back=' + 1;
                if (horse_id) {
                    url += '&horse_id=' + encodeURIComponent(horse_id);
                }
                location = url;
                return false;
            },
        });


        // old horse name


        $('#filterOldHorseName').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/horse_at_glance/autocompleteHorse&token=<?php echo $token; ?>&old_horse_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.horse_name,
                                    value: item.horse_name,
                                    horse_id:item.horse_id,
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                //console.log(ui.item);
                $('#filterOldHorseName').val(ui.item.value);
                $('#filterOldHorseId').val(ui.item.horse_id);
                var horse_id =  $('#filterOldHorseId').val();
                url = 'index.php?route=catalog/horse/view&token=<?php echo $token; ?>';
                url += '&back=' + 1;
                if (horse_id) {
                    url += '&horse_id=' + encodeURIComponent(horse_id);
                }
                location = url;
                return false;
            },
        });


       // Mare  name search
        $('#filterMareName').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/horse_at_glance/autocompleteHorse&token=<?php echo $token; ?>&mare_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.dam_name,
                                    value: item.dam_name,
                                    
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                //console.log(ui.item);
                $('#filterMareName').val(ui.item.value);
                $.ajax({
                        url: 'index.php?route=catalog/horse_at_glance/getAllHorseOfMare&token=<?php echo $token; ?>&mare_name='+ui.item.value,
                        dataType: 'json',
                        success: function(json) {  
                            // console.log('ierjieifi');

                            // console.log(json);

                            $('.mare_datas').html('');
                            $('.mare_datas').append(json.html);

                        }
                    });
                // var horse_id =  $('#filterMareId').val();
                // url = 'index.php?route=catalog/horse/view&token=<?php echo $token; ?>';
                // if (horse_id) {
                //     url += '&horse_id=' + encodeURIComponent(horse_id);
                // }
                // location = url;
                // return false;
            },
        });

    </script>
</div>
<?php echo $footer; ?>