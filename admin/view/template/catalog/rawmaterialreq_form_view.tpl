<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
				<?php if($user_group_id == 15 && $approval_status == 0){ ?>
					<a href="<?php echo $approval; ?>" data-toggle="tooltip" title="<?php echo 'Approve'; ?>" class="btn btn-primary"><i class="fa fa-check"></i></a>
					<a onclick="reason_approve()" data-toggle="tooltip" title="<?php echo 'Reject'; ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
				<?php } ?>
			</div>
			<div class="pull-right" >
				<textarea style="display: none; " rows="3" placeholder="Reason" class="col-sm-8" id="reason"></textarea>
				<a onclick="rejected()" style="display: none; margin-left: 10px;" data-toggle="tooltip" title="<?php echo 'Reject'; ?>" class="btn btn-danger col-sm-3" id="reject" >Reject</a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo 'View Indent'; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-po_number"><?php echo 'Indent Code : '; ?></label>
						<div class="col-sm-2">
							<input type="hidden" value="<?php echo $order_id; ?>" id="order_id">
							<input disabled="disabled" type="text" value="<?php echo $order_no; ?>" class="form-control" />
						</div>
						<label class="col-sm-2 control-label" for="input-mname"><?php echo 'Date : '; ?></label>
						<div class="col-sm-2">
							<input disabled="disabled" type="text" value="<?php echo date('d-M-Y', strtotime($date)); ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<table class="table table-bordered table-hover">
								<thead>
									<tr>
									 	<td style="text-align: center;">Requested By</td>
									 	<td style="width: 10%;text-align: center;" >Code</td>
									 	<td style="text-align: center;">Medicine Name</td>
									 	<td style="text-align: center;">Supplier Name</td>
									 	<td style="text-align: center;">Available Qty</td>
									 	<td style="text-align: center;">Volume</td>
									 	<td style="text-align: center;">Packing Type</td>
									 	<td style="text-align: center;">Quantity</td>
									 	<td style="text-align: center;">Converted Qty</td>
										<td style="text-align: center;">Rate Rs</td>
										<td style="text-align: center;">Value Rs</td>
										<td style="text-align: center;">GST Rate</td>
										<td style="text-align: center;">GST Value Rs</td>
										<td style="text-align: center;">Total Rs</td>
									</tr>
								</thead>
								 	<?php $extra_field_row = 0; ?>
								 	<?php 
									$total_quantity = 0;
									$total_total = 0;
								 	?>
								 	<?php foreach ($productraw_datas as $pkey => $productraw_data) { ?>
										<tbody>  
											<tr>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['request']; ?>
												</td>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['product_id']; ?>
												</td>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['productraw_name']; ?>
												</td>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['supplier_name']; ?>
												</td>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['current_qty']; ?>
												</td>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['volume']; ?>
												</td>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['packing']; ?>
												</td>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['quantity']; ?>
												</td>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['ordered_qty']; ?>
												</td>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['rate']; ?>
												</td>
												<td style="text-align: right;">
												 	<?php echo $productraw_data['purchase_value']; ?>
												</td>
												<td style="text-align: left;">
												 	<?php echo $productraw_data['gst_rate']; ?>
												</td>
												<td style="text-align: right;">
												 	<?php echo $productraw_data['gst_value']; ?>
												</td>
												<td style="text-align: right;">
												 	<?php echo $productraw_data['total']; ?>
												</td>
											</tr>
										</tbody>
									<?php } ?>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td><b>Total</b></td>
								<td style="text-align: right;"><b><?php echo $final_value; ?></b></td>
								<td></td>
								<td style="text-align: right;"><b><?php echo $final_gst_val; ?></b></td>
								<td style="text-align: right;"><b><?php echo $final_total; ?></b></td>
							<?php $extra_field_row++; ?>
							<?php  ?>
							<input type="hidden" id="extra_field_row" name="extra_field_row" value="<?php echo $extra_field_row; ?>" />
						</table>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-mname"><?php echo 'Narration:'; ?></label>
						<div class="col-sm-2 sup_div">
							<input disabled="disabled" type="text" value="<?php echo $narration; ?>" class="form-control" />
						</div>
						<?php if ($approval_status == 2) { ?>
							<label class="col-sm-2 control-label" for="input-mname"><?php echo 'Rejected Reason:'; ?></label>
							<div class="col-sm-2 sup_div">
								<textarea readonly="readonly" class="form-control"><?php echo $reason; ?></textarea>
							</div>
						<?php } ?>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function reason_approve() {
		$('#reason').show();
		$('#reject').show();
	}

	function rejected() {
		var reason = $('#reason').val();
		var order_id = $('#order_id').val();
		url = 'index.php?route=catalog/rawmaterialreq/indent_dis_approval&token=<?php echo $token; ?>';
		if (reason != '') {
			url += '&reason=' + encodeURIComponent(reason);
			url += '&order_id=' + encodeURIComponent(order_id);
		} else {
			return false;
		}
		window.location.href = url;
	}
</script>

<?php echo $footer; ?>