<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header" >
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> 
		        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Go To Dashboard" class="btn btn-primary">Go To Dashboard</a>
				<button style="display: none;"> type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-inward').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<h1>Medicine Return</h1>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i>Medicine Transfer</h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-inward">
					<div class="well">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="col-sm-3">
										<input type="text" name="filter_clinic_name" value="<?php echo $filter_clinic_name ?>" placeholder="Clinic Name" id="input-filter_clinic_name" class="form-control" />
										<input type="hidden" name="filter_clinic_id" value="<?php echo $filter_clinic_id ?>" placeholder="Clinic Name" id="input-filter_clinic_id" class="form-control" />

									</div>
									<div class="col-sm-3">
										<input type="text" name="filter_doctor" value="<?php echo $filter_doctor ?>" placeholder="<?php echo "Doctor Name"; ?>" id="input-filter_doctor" class="form-control" />
										<input type="hidden" name="filter_doctor_id" value="<?php echo $filter_doctor_id ?>" placeholder="doctor Name" id="input-filter_doctor_id" class="form-control" />

									</div>
									<div class="col-sm-3">
										<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
										<!-- <a href="<?php echo $medicine_report; ?>" data-toggle="tooltip" title="<?php echo 'Transfer Report'; ?>" class="btn btn-primary">Transfer Report</i></a> -->
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<?php if($is_user == '0'){ ?>
										<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
										<?php } ?>
										
										<td class="text-left"><?php echo 'Code:'; ?></td>
										<td class="text-left"><?php echo 'Date'; ?></td>
										<td class="text-left"><?php echo 'Clinic Name'; ?></td>
										<td class="text-left"><?php echo 'Doctor Name'; ?></td>
										<td class="text-left"><?php echo 'Total Item'; ?></td>
										<td class="text-left"><?php echo 'Total Qty'; ?></td>
										<td class="text-left"><?php echo 'Total Amt'; ?></td>
										<td class="text-left"><?php echo 'Logs'; ?></td>
										<td class="text-right"><?php echo 'Action'; ?></td>
									</tr>
								</thead>
								<tbody>
									<?php if ($medicine_trans_datas) { ?>
										<?php $i = 1; ?>
										<?php foreach ($medicine_trans_datas as $medicine_transfer) { ?>
										<tr>
											<?php if($is_user == '0'){ ?>
											<td style="display: none;" class="text-center"><?php if (in_array($medicine_transfer['id'], $selected)) { ?>
												<input type="checkbox" name="selected[]" value="<?php echo $medicine_transfer['id']; ?>" checked="checked" />
												<?php } else { ?>
												<input type="checkbox" name="selected[]" value="<?php echo $medicine_transfer['id']; ?>" />
												<?php } ?>
											</td>
											<?php } ?>
										 
											
											<td class="text-left">
												<?php echo $medicine_transfer['issue_no']; ?>
												<input type="hidden" name="count" class="count" value="<?php echo $i; ?>" />
											</td>
											<td class="text-left"><?php echo date('d-M-Y', strtotime($medicine_transfer['entry_date'])); ?></td>
											<td class="text-left"><?php echo $medicine_transfer['clinic_name']; ?></td>
											<td class="text-left"><?php echo $medicine_transfer['doctor_name']; ?></td>

											<td  class="text-right">
												<?php echo $medicine_transfer['total_item']; ?>
											</td>
											<td  class="text-left">
												<?php echo $medicine_transfer['total_qty']; ?>
											</td>
											<td  class="text-right">
												<?php echo $medicine_transfer['total_amt']; ?>
											</td>
											<td class="text-left"><?php echo $medicine_transfer['log_datas']; ?></td>
											
											<td class="text-right">
												<a href="<?php echo $medicine_transfer['print']; ?>" data-toggle="tooltip" title="<?php echo 'Print'; ?>" class="btn btn-primary"><i class="fa fa-print"></i></a>
												<a href="<?php echo $medicine_transfer['edit']; ?>" data-toggle="tooltip" title="<?php echo 'View'; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
											</td>
										</tr>
										<?php $i ++; ?>
										<?php } ?>
									<?php } else { ?>
									<tr>
										<td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>
		        <div class="row">
		        	<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		        	<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		        </div>
	        </div>
		</div>
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Finished Product Overview</h4>
					</div>
					<div class="modal-body edit-content">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--

$('#myModal').on('show.bs.modal', function(e) {
	var $modal = $(this),
	esseyId = e.relatedTarget.id;
	idsss = e.relatedTarget.className;
	s_ids = idsss.split(' ');
	idss = s_ids[1];
	s_id = idss.split('-');
	id = s_id[1];
	order_id = $('#order_id-'+id).val();
	//console.log(productfinished_id);
	//return false;
	$.ajax({
		cache: false,
		type: 'POST',
		url: 'index.php?route=catalog/inward/getdata&token=<?php echo $token; ?>&filter_order_id=' +  encodeURIComponent(order_id),
		data: 'filter_order_id=' + order_id,
		success: function(data) {
			//console.log(data.html);
			$modal.find('.edit-content').html(data.html);
		}
	});
})

$('#button-filter').on('click', function() {
  var url = 'index.php?route=catalog/medicine_return/getList&token=<?php echo $token; ?>';

  var filter_clinic_name = $('input[name=\'filter_clinic_name\']').val();
 
  if (filter_clinic_name) {
	var filter_clinic_id = $('input[name=\'filter_clinic_id\']').val();
	if (filter_clinic_id) {
	  url += '&filter_clinic_id=' + encodeURIComponent(filter_clinic_id);
	}
	url += '&filter_clinic_name=' + encodeURIComponent(filter_clinic_name);
  
  }

  var filter_doctor = $('input[name=\'filter_doctor\']').val();
 
  if (filter_doctor) {
	var filter_doctor_id = $('input[name=\'filter_doctor_id\']').val();
	if (filter_doctor_id) {
	  url += '&filter_doctor_id=' + encodeURIComponent(filter_doctor_id);
	}
	url += '&filter_doctor=' + encodeURIComponent(filter_doctor);
  
  }

  location = url;
});

$('input[name=\'filter_clinic_name\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/medicine_return/autocompleteClinic&token=<?php echo $token; ?>&filter_clinic_name=' +  encodeURIComponent(request),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['clinic_name'],
			value: item['clinic_id']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter_clinic_name\']').val(item['label']);
	$('input[name=\'filter_clinic_id\']').val(item['value']);
  }
});


$('input[name=\'filter_doctor\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/medicine_return/autocompleteDoctor&token=<?php echo $token; ?>&filter_doctor_name=' +  encodeURIComponent(request),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['doctor_name'],
			value: item['doctor_id']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter_doctor\']').val(item['label']);
	$('input[name=\'filter_doctor_id\']').val(item['value']);
  }
});


$(document).ready(function()               
    {
        // enter keyd
        $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                 $('#button-filter').trigger('click');
             }
        });
    });

//--></script>
<script type="text/javascript">
$( document ).ready(function() {
	var refer = "<?php echo $refer; ?>";
	if(refer == '1') {
	  close();
	}
});
</script>
<?php echo $footer; ?>