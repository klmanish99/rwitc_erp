<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1>Events</h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> Events</h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="event_name">Event Name</label>
                                <div class="col-sm-5">
                                    <input type="text" name="event_name" value="<?php echo $event_name; ?>" placeholder="Event Name" id="event_name" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="event_name">Event Date</label>
                                <div class="col-sm-3">
                                    <div class="input-group date input-date_of_event">
                                        <input type="text" name="date_of_event" value="<?php echo $date_of_event; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_of_event" class="form-control" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <?php if (isset($valierr_date_of_event)) { ?><span class="errors" id="error_date_of_events"><?php echo $valierr_date_of_event; ?></span><?php } ?>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_date_of_event" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="description">Description</label>
                                <div class="col-sm-5">
                                    <input type="text" name="description" value="<?php echo $description; ?>" placeholder="Event Name" id="description" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
  
<script type="text/javascript">
    $('#language a:first').tab('show');
</script>

<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('#date_of_event').keyup(function(){
        var date_of_event_valid =  $('#date_of_event').val();
        date_of_event_valid1 = date_of_event_valid.replace(/[^0-9-]+/i, '');
        $("#date_of_event").val(date_of_event_valid1);
        var date_of_event_valid_again =  $('#date_of_event').val();
        var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
        if (!(date_regex.test(date_of_event_valid_again))) {
            $('#error_date_of_event').show();
            return false;
        } else {
            $('#error_date_of_event').hide();
        }
    });
    $('.input-date_of_event').datetimepicker().on('dp.change', function (e) {  
        $('#error_date_of_event').css('display','none');
    });
</script>

<?php echo $footer; ?>