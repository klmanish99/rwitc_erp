<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab-general">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="doctor_code"><?php echo $entry_doctor_code; ?></label>
								<div class="col-sm-4">
									<input type="hidden"  name="alpha" id="alpha" value="<?php echo $alpha; ?>">
                                    <input type="hidden"  name="alpha_no" id="alpha_no" value="<?php echo $alpha_no; ?>">
									<input type="hidden" value="<?php echo $user_log_grp_id; ?>" name="user_log_grp_id">
									<input type="hidden" value="<?php echo $user_log_id; ?>" name="user_log_id">
									<input type="text" readonly name="doctor_code" value="<?php echo $doctor_code; ?>" placeholder="<?php echo $entry_doctor_code; ?>" id="doctor_code" class="form-control" tabindex="2"/>
									<?php if (isset($valierr_doctor_code)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_doctor_code; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="doctor_name"><?php echo $entry_name; ?></label>
								<div class="col-sm-4">
									<input type="text" name="doctor_name" value="<?php echo $doctor_name; ?>" placeholder="<?php echo $entry_name; ?>" id="doctor_name" class="form-control" tabindex="1"/>
									<?php if (isset($valierr_doctor_name)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_doctor_name; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="ambulance_no">Ambulance No</label>
								<div class="col-sm-4">
									<input type="text" name="ambulance_no" value="<?php echo $ambulance_no; ?>" placeholder="Ambulance No" id="ambulance_no" class="form-control" tabindex="3"/>
								</div>
								<label class="col-sm-2 control-label" for="input-parent">Assistant Too</label>
								<div class="col-sm-3">
									<input tabindex="" type="text" name="parent" value="<?php echo $parent; ?>" placeholder="Assistant Too" id="input-parent" class="form-control" />
									<input tabindex="" type="hidden" name="hidden_parent" value="<?php echo $hidden_parent; ?>"id="hidden_parent" class="form-control" />
									<?php if (isset($valierr_parent)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_parent; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="select-isActive">Status</label>
								<div class="col-sm-3">
										<select name="isActive" id="select-isActive" class="form-control" tabindex="5">
												<?php foreach ($Active as $key => $value) { ?>
												<?php if ($value == $isActive) { ?>
													<option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
												<?php } else { ?>
													<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
												<?php } ?>
												<?php } ?>
										</select>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</script>
	
	<script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script>
<script type="text/javascript">
		$(document).on('keypress','input,select, textarea', function (e) {
//$('input,select').on('keypress', function (e) {
		if (e.which == 13) {
				e.preventDefault();
				var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
				if (!$next.length) {
						$next = $('[tabIndex=1]');
				}
				$next.focus();
		}
});
</script>
<script type="text/javascript">
$(document).ready(function(){
$('[tabindex= 1]').focus();
});	
$('#input-parent').autocomplete({
	'source': function(request, response) {
	$.ajax({
		url: 'index.php?route=catalog/doctor/autocompleteparent&token=<?php echo $token; ?>&parent=' +  encodeURIComponent(request),
		dataType: 'json',
		success: function(json) {
		response($.map(json, function(item) {
			return {
			label: item['doctor_name'],
			id: item['id'],
			value: item['doctor_name'],
			}
		}));
		}
	});
	},
	'select': function(item) {
	$('#input-parent').val(item['label']);
	$('#input-parent').val(item['value']);
	$('#hidden_parent').val(item['id']);
	$('.dropdown-menu').hide();
	}
});

 $(document).on("keyup","#doctor_name",function()  {
                item_name = $(this).val();
                if(item_name.length > 0 ){
                    $.ajax({
                        url: 'index.php?route=catalog/doctor/DoctorIdGenerate&token=<?php echo $token; ?>&filter_doctor_name=' + item_name[0], 
                        dataType: 'json',
                        success: function(json) {
                        	console.log(json); 
                            $('#doctor_code').val(json.doc_code); 
                            $('#alpha').val(json.alpha); 
                            $('#alpha_no').val(json.alpha_no); 

                        } 
                    });
                } else {
                    if(item_name == ''){
                        $('#doctor_code').val('');
                    }
                }
            });
</script>
</div>
<?php echo $footer; ?>