<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
            
                <button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
           <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
                    <div class="well" style="background-color: #ffffff;">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" name="trainer_name" id="trainer_name" value="<?php echo $filter_trainer_name?>" placeholder="Trainer Racing Name" class="form-control">
                                        <input type="hidden" name="hidden_trainer_name_id" id="hidden_trainer_name_id" value="" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="trainer_code" id="trainer_code"  value="" placeholder="Trainer Code" class="form-control">
                                        <input type="hidden" name="hidden_trainer_code_id" id="hidden_trainer_code_id" value="" >
                                    </div>
                                    <div class="col-sm-3">
                                        <select name="filter_status" id="filter_status" class="form-control">
                                            <?php foreach ($status as $key => $tvalue) { ?>
                                                <?php if($key == $filter_status){ ?>
                                                  <option value="<?php echo $key ?>" selected="selected"><?php echo $tvalue?></option>
                                                <?php } else { ?>
                                                  <option value="<?php echo $key ?>"><?php echo $tvalue?></option>
                                                <?php } ?>
                                            <?php } ?>    
                                        </select>
                                      </div>
                                    <div class="col-sm-1">
                                        <a onclick="trainerfilter()" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="display: none;" "width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                    <td class="text-center"><?php if ($sort == 'trainers.id') { ?>
                                        <a href="<?php echo $sr_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Sr No'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sr_no; ?>"><?php echo 'Sr No'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'trainer_code') { ?>
                                        <a href="<?php echo $trainer_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Trainer Code'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $trainer_code; ?>"><?php echo 'Trainer Code'; ?></a>
                                        <?php } ?>
                                    </td>
                                   <td class="text-center"><?php if ($sort == 'name') { ?>
                                        <a href="<?php echo $trainer_racing_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Trainer Racing Name'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $trainer_racing_name; ?>"><?php echo 'Trainer Racing Name'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'tcd.mobile_no') { ?>
                                        <a href="<?php echo $mobile_number; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Mobile Number'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $mobile_number; ?>"><?php echo 'Mobile Number'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'tcd.email_id') { ?>
                                        <a href="<?php echo $email_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Email Id'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $email_id; ?>"><?php echo 'Email Id'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'pan_no') { ?>
                                        <a href="<?php echo $pan_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Pan No'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $pan_no; ?>"><?php echo 'Pan No'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'license_type') { ?>
                                        <a href="<?php echo $license_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'License Type'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $license_type; ?>"><?php echo 'License Type'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 't.horses_in_charge') { ?>
                                        <a href="<?php echo $horses_in_charge; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Horses In Charge'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $horses_in_charge; ?>"><?php echo 'Horses In Charge'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 't.ban') { ?>
                                        <a href="<?php echo $ban; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Ban'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $ban; ?>"><?php echo 'Ban'; ?></a>
                                        <?php } ?>
                                    </td>
                                     <td class="text-center"><?php if ($sort == 't.ban') { ?>
                                        <a href="<?php echo $ban; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Ban Start Date'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $ban; ?>"><?php echo 'Ban Start Date'; ?></a>
                                        <?php } ?>
                                    </td>
                                     <td class="text-center"><?php if ($sort == 't.ban') { ?>
                                        <a href="<?php echo $ban; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Ban End Date'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $ban; ?>"><?php echo 'Ban End Date'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 't.status') { ?>
                                        <a href="<?php echo $statuses; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Status'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $statuses; ?>"><?php echo 'Status'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php echo $column_action; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($Trainers) { ?>
                                    <?php $i=1; ?>
                                <?php foreach ($Trainers as $Trainer) { ?>
                                <tr>
                                    <td style="display: none;" class="text-center"><?php if (in_array($Trainer['id'], $selected)) { ?>
                                        <input type="checkbox" name="selected[]" value="<?php echo $Trainer['id']; ?>" checked="checked" />
                                        <?php } else { ?>
                                        <input type="checkbox" name="selected[]" value="<?php echo $Trainer['id']; ?>" />
                                        <?php } ?>
                                    </td>
                                    <td class="text-left"><?php echo $i++; ?></td>
                                    <td class="text-left"><?php echo $Trainer['trainer_code']; ?></td>
                                    <td class="text-left"><?php echo $Trainer['name']; ?></td>
                                    <td class="text-left"><?php echo $Trainer['mobile_no']; ?></td>
                                    <td class="text-left"><?php echo $Trainer['email_id']; ?></td>
                                    <td class="text-left"><?php echo $Trainer['pan_no']; ?></td>
                                    <td class="text-left"><?php echo $Trainer['license_type']; ?></td>
                                    <td class="text-left"><?php echo $Trainer['horses_in_charge']; ?></td>
                                    <td class="text-left"><?php echo $Trainer['ban']; ?></td>
                                    <td class="text-left"><?php echo $Trainer['start']; ?></td>
                                    <td class="text-left"><?php echo $Trainer['end']; ?></td>
                                    <?php if ($Trainer['trainer_Status'] == '1') { ?>
                                        <td class="text-left">Active</td>
                                    <?php } else { ?>
                                        <td class="text-left">Inactive</td>
                                    <?php } ?>
                                    <td style="width: 10%; " class="col-sm-2">
                                        <a href="<?php echo $Trainer['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>   <a href="<?php echo $Trainer['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                                <?php } else { ?>
                                <tr>
                                    <td class="text-center" colspan="10"><?php echo $text_no_results; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php echo $footer; ?>
 <script type="text/javascript">
        function trainerfilter() { //alert('innnn');
            var filter_trainer_code = $('#trainer_code').val();
           // var filter_trainer_id = $('#hidden_trainer_name_id').val();
            var filter_trainer_name = $('#trainer_name').val();
            var filter_status = $('select[name=\'filter_status\']').val();
            if(filter_trainer_name != '' || filter_trainer_code != '' ||filter_status != ''){
                url = 'index.php?route=catalog/trainer&token=<?php echo $token; ?>';
                if (filter_trainer_name) {
                  url += '&filter_trainer_name=' + encodeURIComponent(filter_trainer_name);
                }

                // if (filter_trainer_id) {
                //   url += '&filter_trainer_id=' + encodeURIComponent(filter_trainer_id);
                // }
                if (filter_status) {
                    url += '&filter_status=' + encodeURIComponent(filter_status);
                }
                if (filter_trainer_code) {
                    url += '&filter_trainer_code=' + encodeURIComponent(filter_trainer_code);
                } 
                window.location.href = url;
            } else {
                alert('Please select the filters');
                return false;
            }
        }

        $('#trainer_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#trainer_id').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/trainer/autocompleteTrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
                        dataType: 'json',
                        success: function(json) {   
                            $('#trainer_codes_id').find('option').remove();
                            response($.map(json, function(item) {
                                return {
                                    label: item.trainer_name,
                                    value: item.trainer_name,
                                    trainer_id:item.trainer_id,
                                    trainer_codes:item.trainer_code
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(item) {
                console.log(item);
                $('#trainer_name').val(item.value);
                $('#trainer_id').val(item.trainer_id);
                $('.dropdown-menu').hide();
                trainerfilter();
                return false;
            },
        });
    </script>