<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-search"></i> <?php echo 'Owner'; ?></h3>
            </div>
            <div class="panel-body">
                <div class="form-group" >
                    <label class="col-sm-2 control-label" for="input-owner"><b style="color: red;"> * </b> <?php echo "Owner Name:"; ?></label>
                    <div class="col-sm-3">
                        <input type="text"  name="filterOwnerName" id="filterOwnerName" value="" placeholder="Owner Name" class="form-control" />
                        <input type="hidden" name="filterOwnerId" id="filterOwnerId" value="<?php echo $horse_id ?>"  class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">

        $('#filterOwnerName').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#filterOwnerId').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/owner_at_glance/autocompleteOwner&token=<?php echo $token; ?>&owner_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.owner_name,
                                    value: item.owner_name,
                                    owner_id:item.owner_id,
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                console.log(ui.item);
                $('#filterOwnerName').val(ui.item.value);
                $('#filterOwnerId').val(ui.item.owner_id);
                var owner_id =  $('#filterOwnerId').val();
                url = 'index.php?route=catalog/owner/view&token=<?php echo $token; ?>';
                url += '&back=' + 1;
                if (owner_id) {
                    url += '&ownerId=' + encodeURIComponent(owner_id);
                }
                location = url;
                return false;
            },
        });
    </script>
</div>
<?php echo $footer; ?>