<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
	<div class="container-fluid">
	  <div class="pull-right">
		<button onclick="$('#form-manufacturer').submit()" type="button" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
		<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
	  </div>
	  <h1><?php echo $heading_title; ?></h1>
	  <ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	  </ul>
	</div>
  </div>
  <div class="container-fluid">
	<?php if ($error_warning) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>
	<div class="panel panel-default">
	  <div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	  </div>
	  <div class="panel-body">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
		  <ul class="nav nav-tabs">
			<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo 'Profile'; ?></a></li>
			<li><a href="#tab-contact" data-toggle="tab"><?php echo 'Contact Details'; ?></a></li>
		  </ul>
		  <div class="tab-content">
			<div class="tab-pane active" id="tab-general">
			  <div class="form-group">
		        <label class="col-sm-2 control-label" for="input-vendor_code"><?php echo 'Supplier Code'; ?></label>
				<div class="col-sm-4">
					<input type="hidden" value="<?php echo $user_log_grp_id; ?>" name="user_log_grp_id">
					<input type="hidden" value="<?php echo $user_log_id; ?>" name="user_log_id">
				  <input tabindex="1" type="text" name="vendor_code" value="<?php echo $vendor_code; ?>" placeholder="<?php echo 'Supplier Code'; ?>" id="input-vendor_code" class="form-control" />
				  <?php if ($error_code) { ?>
				  	<div class="text-danger"><?php echo $error_code; ?></div>
				  <?php } ?>
				</div>
				<label class="col-sm-2 control-label" for="input-vendor_name"><?php echo $entry_name; ?></label>
				<div class="col-sm-4">
				  <input tabindex="2" type="text" name="vendor_name" value="<?php echo $vendor_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-vendor_name" class="form-control" />
				  <?php if ($error_name) { ?>
				  	<div class="text-danger"><?php echo $error_name; ?></div>
				  <?php } ?>
				</div>
			  </div>

			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-place"><?php echo 'Place'; ?></label>
				<div class="col-sm-4">
				  <input tabindex="3" type="text" name="place" value="<?php echo $place; ?>" placeholder="<?php echo 'Place'; ?>" id="input-place" class="form-control" />
				</div>
			  
				<label class="col-sm-2 control-label" for="input-gst_no"><?php echo 'GST NO'; ?></label>
				<div class="col-sm-4">
				  <input tabindex="4" type="text" name="gst_no" value="<?php echo $gst_no; ?>" placeholder="<?php echo 'GST NO'; ?>" id="input-gst_no" class="form-control" />
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="select-isActive">Status</label>
				<div class="col-sm-3">
					<select name="isActive" id="select-isActive" class="form-control" tabindex="5">
						<?php foreach ($Active as $key => $value) { ?>
						<?php if ($key == $isActive) { ?>
						  <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
						<?php } else { ?>
						  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
				<?php if (isset($error_status)) { ?><span class="errors" style="color: #ff0000;"><?php echo $error_status; ?></span><?php } ?>
				</div>
			  </div>

			  <!-- <div class="form-group">
				<label class="col-sm-1 control-label" for="input-payment_details" style="width:9%;"><?php echo 'Payment Terms'; ?></label>
				<div class="col-sm-2" style="">
				  <select tabindex="5" name="payment_details" id="input-payment_details" class="form-control payment_details">
					<?php foreach($payment_types as $vkey => $vvalue){ ?>
					  <?php if($vkey == $payment_details){ ?>
						<option value="<?php echo $vkey ?>" selected="selected"><?php echo $vvalue; ?></option>
					  <?php } else { ?>
						<option value="<?php echo $vkey ?>"><?php echo $vvalue; ?></option>
					  <?php } ?>
					<?php } ?>
				  </select>
				</div>
				<?php $tab_1 = 5; ?>
				<?php $tab_1 ++; ?>
				<label class="col-sm-1 control-label payment_text" for="input-percent_text" style="width:45%;">
				  <?php if($payment_details == 'Advance'){ ?>
					<input style="width:10%;display: inline-block;" tabindex="<?php echo $tab_1 ?>" type="text" name="percent_text" value="<?php echo $percent_text; ?>" placeholder="<?php echo ''; ?>" id="input-percent_text" class="form-control" style="font-size:16px;" /><?php echo ' % Advance and Balance after'; ?>
					<?php $tab_1 ++; ?>
					<input style="width:10%;display: inline-block;" tabindex="<?php echo $tab_1 ?>" type="text" name="days_text" value="<?php echo $days_text; ?>" placeholder="<?php echo ''; ?>" id="input-days_text" class="form-control" style="font-size:16px;" /><?php echo ' days of delivery & submission of Invoice'; ?> 
				  <?php } elseif($payment_details == 'Credit'){ ?>
					<input style="width:10%;display: inline-block;" tabindex="<?php echo $tab_1 ?>" type="text" name="days_text" value="<?php echo $days_text; ?>" placeholder="<?php echo ''; ?>" id="input-days_text" class="form-control" style="font-size:16px;" /><?php echo ' days of delivery & submission of Invoice'; ?> 
				  <?php } elseif($payment_details == 'Pdc'){ ?>
					<input style="width:10%;display: inline-block;" tabindex="<?php echo $tab_1 ?>" type="text" name="days_text" value="<?php echo $days_text; ?>" placeholder="<?php echo ''; ?>" id="input-days_text" class="form-control" style="font-size:16px;" /><?php echo ' days From date of order'; ?> 
				  <?php } ?>
				</label>
			  </div> -->

			<div class="form-group">
				<?php $tab_1 ++; ?>
				<label class="col-sm-3 control-label" for="input-filename"><?php echo 'Upload Attachment'; ?></label>
				<div class="col-sm-5">
				  <div class="input-group">
					<input tabindex="<?php echo $tab_1 ?>" type="text" name="filename" value="<?php echo $filename; ?>" placeholder="<?php echo 'File Name'; ?>" id="input-filename" class="form-control" />
					<input type="hidden" name="mask" value="<?php echo $mask; ?>" placeholder="<?php echo 'File Name'; ?>" id="input-mask" class="form-control" />
					<input type="hidden" name="uploaded_file_source" value="<?php echo $uploaded_file_source; ?>" id="input-other_document_1_source" class="form-control" />
					<span class="input-group-btn">
					  <button type="button" id="button-upload" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload'; ?>&nbsp;&nbsp;&nbsp;</button>
					<span id="button-other_document_1_new"></span>
					<?php if($uploaded_file_source != ''){ ?>
						<a target="_blank" class = "btn btn-default" style="cursor: pointer;margin-left:10px; border-radius: 3px;" id="uploaded_file_source" href="<?php echo $uploaded_file_source; ?>">View Document</a>
					<?php } ?>
				  </div>
				  <?php if ($error_filename) { ?>
				  <div class="text-danger"><?php echo $error_filename; ?></div>
				  <?php } ?>
				</div>
			</div>
			<div class="form-group">
				<?php $tab_1 ++; ?>
				<label class="col-sm-3 control-label" for="input-filename"><?php echo 'Upload Attachment 2'; ?></label>
				<div class="col-sm-5" >
				  <div class="input-group">
					<input tabindex="<?php echo $tab_1 ?>" type="text" name="filename2" value="<?php echo $filename2; ?>" placeholder="<?php echo 'File Name 2'; ?>" id="input-filename2" class="form-control" />
					<input type="hidden" name="mask2" value="<?php echo $mask2; ?>" placeholder="<?php echo 'File Name 2'; ?>" id="input-mask2" class="form-control" />
					<input type="hidden" name="uploaded_file_source2" value="<?php echo $uploaded_file_source2; ?>" id="input-other_document_1_source" class="form-control" />
					<span class="input-group-btn">
					  <button type="button" id="button-upload2" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload 2'; ?></button>
					  <span id="button-other_document_2_new"></span>
						<?php if($uploaded_file_source2 != ''){ ?>
							<a target="_blank" class = "btn btn-default" style="cursor: pointer;margin-left:10px; border-radius: 3px;" id="uploaded_file_source2" href="<?php echo $uploaded_file_source2; ?>">View Document</a>
						<?php } ?>
					</span> 
				  </div>
				  <?php if ($error_filename2) { ?>
				  <div class="text-danger"><?php echo $error_filename2; ?></div>
				  <?php } ?>
				</div>
			  </div>
			  <div class="form-group">
			  	<?php $tab_1 ++; ?>
				<label class="col-sm-3 control-label" for="input-filename" ><?php echo 'Upload Attachment 3'; ?></label>
				<div class="col-sm-5" >
				  <div class="input-group">
					<input tabindex="<?php echo $tab_1 ?>" type="text" name="filename3" value="<?php echo $filename3; ?>" placeholder="<?php echo 'File Name 3'; ?>" id="input-filename3" class="form-control" />
					<input type="hidden" name="mask3" value="<?php echo $mask3; ?>" placeholder="<?php echo 'File Name 3'; ?>" id="input-mask3" class="form-control" />
					<input type="hidden" name="uploaded_file_source3" value="<?php echo $uploaded_file_source3; ?>" id="input-other_document_1_source" class="form-control" />
					<span class="input-group-btn">
					  <button type="button" id="button-upload3" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload 3'; ?></button>
					  <span id="button-other_document_3_new"></span>
						<?php if($uploaded_file_source3 != ''){ ?>
							<a target="_blank" class = "btn btn-default" style="cursor: pointer;margin-left:10px; border-radius: 3px;" id="uploaded_file_source3" href="<?php echo $uploaded_file_source3; ?>">View Document</a>
						<?php } ?>
					</span> 
				  </div>
				  <?php if ($error_filename3) { ?>
				  <div class="text-danger"><?php echo $error_filename3; ?></div>
				  <?php } ?>
				</div>
			</div>
			<div class="form-group">
				<?php $tab_1 ++; ?>
				<label class="col-sm-3 control-label" for="input-filename4" ><?php echo 'Upload Attachment 4'; ?></label>
				<div class="col-sm-5">
				  <div class="input-group">
					<input tabindex="<?php echo $tab_1 ?>" type="text" name="filename4" value="<?php echo $filename4; ?>" placeholder="<?php echo 'File Name 4'; ?>" id="input-filename4" class="form-control" />
					<input type="hidden" name="mask4" value="<?php echo $mask4; ?>" placeholder="<?php echo 'File Name 4'; ?>" id="input-mask4" class="form-control" />
					<input type="hidden" name="uploaded_file_source4" value="<?php echo $uploaded_file_source4; ?>" id="input-other_document_1_source" class="form-control" />
					<span class="input-group-btn">
					  <button type="button" id="button-upload4" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload 4'; ?></button>
					  <span id="button-other_document_4_new"></span>
						<?php if($uploaded_file_source4 != ''){ ?>
							<a target="_blank" class = "btn btn-default" style="cursor: pointer;margin-left:10px; border-radius: 3px;" id="uploaded_file_source4" href="<?php echo $uploaded_file_source4; ?>">View Document</a>
						<?php } ?>
					</span> 
				  </div>
				  <?php if ($error_filename4) { ?>
				  <div class="text-danger"><?php echo $error_filename4; ?></div>
				  <?php } ?>
				</div>
			  </div>
			</div>

			<div class="tab-pane"  id="tab-contact">
			  <div class="form-group">
				<div class="pull-right col-sm-1">
				  <button type="button" onclick="add_new_2();"class="btn btn-primary" data-original-title="add"><i class="fa fa-plus-circle"></i></button>
				</div>
			  </div>
			  <div class="main_div_2"> 
				<?php $extra_field_2 = 1; ?>
				<?php $tab_index_1 = 0; ?>
				<?php if($contact_datas){ ?>
				  <?php foreach($contact_datas as $ckey => $cvalue){ ?>
					<div class="form-group contact_row<?php echo $extra_field_2; ?>">
					   <?php $tab_index_1 ++; ?>
					  <label class="col-sm-1 control-label" for="input-name"><?php echo 'Name' . $extra_field_2; ?></label>
					  <div class="col-sm-3">
						<input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][name]" value="<?php echo $cvalue['name']; ?>" placeholder="<?php echo 'Name'; ?>" id="input-bank_name<?php echo $extra_field_2; ?>" class="form-control" />
					  </div>
					  <?php $tab_index_1 ++; ?>
					  <label class="col-sm-1 control-label" for="input-designation" ><?php echo 'Designation'; ?></label>
					  <div class="col-sm-3">
						<input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][designation]" value="<?php echo $cvalue['designation']; ?>" placeholder="<?php echo 'Designation'; ?>" id="input-designation<?php echo $extra_field_2; ?>" class="form-control" />
					  </div>
					  <?php $tab_index_1 ++; ?>
					  <label class="col-sm-1 control-label" for="input-phone_no" ><?php echo 'Phone No'; ?></label>
					  <div class="col-sm-3">
						<input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][phone_no]" value="<?php echo $cvalue['phone_no']; ?>" placeholder="<?php echo 'Phone Number'; ?>" id="input-phone_no<?php echo $extra_field_2; ?>" class="form-control" />
					  </div>
					</div>
					<div class="form-group contact_row<?php echo $extra_field_2; ?>">
					  <?php $tab_index_1 ++; ?>
					  <label class="col-sm-1 control-label" for="input-mobile1" ><?php echo 'Mobile 1'; ?></label>
					  <div class="col-sm-3">
						<input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][mobile1]" value="<?php echo $cvalue['mobile1']; ?>" placeholder="<?php echo 'Mobile 1'; ?>" id="input-mobile1<?php echo $extra_field_2; ?>" class="form-control" />
					  </div>
					  <?php $tab_index_1 ++; ?>
					  <label class="col-sm-1 control-label" for="input-mobile2" ><?php echo 'Mobile 2'; ?></label>
					  <div class="col-sm-3">
						<input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][mobile2]" value="<?php echo $cvalue['mobile2']; ?>" placeholder="<?php echo 'Mobile 2'; ?>" id="input-mobile2<?php echo $extra_field_2; ?>" class="form-control" />
					  </div>
					  <?php $tab_index_1 ++; ?>                    
					  <label class="col-sm-1 control-label" for="input-email_id" ><?php echo 'Email'; ?></label>
					  <div class="col-sm-3">
						<input tabindex="<?php echo $tab_index_1; ?>" type="email" name="contact_datas[<?php echo $extra_field_2 ?>][email_id]" value="<?php echo $cvalue['email_id']; ?>" placeholder="<?php echo 'Email'; ?>" id="input-email_id<?php echo $extra_field_2; ?>" class="form-control" />
					  </div>
					   <div style="margin-top: 1%;" class="pull-right col-sm-1">
						<button type="button" onclick="remove_folder_2('<?php echo $extra_field_2; ?>')" data-toggle="tooltip" class="btn btn-danger" data-original-title="Remove" id="remove_2<?php echo $extra_field_2; ?>" ><i class="fa fa-minus-circle"></i></button>
					  </div>
					</div>
					<?php $extra_field_2 ++; ?>
				  <?php } ?>
				<?php } else { ?>
				  <div class="form-group contact_row<?php echo $extra_field_2; ?>">
					<?php $tab_index_1 ++; ?>
					<label class="col-sm-1 control-label" for="input-name" ><?php echo 'Name' . $extra_field_2; ?></label>
					<div class="col-sm-3">
					  <input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][name]" value="" placeholder="<?php echo 'Name'; ?>" id="input-bank_name<?php echo $extra_field_2; ?>" class="form-control" />
					</div>
					<?php $tab_index_1 ++; ?>
					<label class="col-sm-1 control-label" for="input-designation" ><?php echo 'Designation'; ?></label>
					<div class="col-sm-3">
					  <input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][designation]" value="" placeholder="<?php echo 'Designation'; ?>" id="input-designation<?php echo $extra_field_2; ?>" class="form-control" />
					</div>
					<?php $tab_index_1 ++; ?>
					<label class="col-sm-1 control-label" for="input-phone_no" ><?php echo 'Phone No'; ?></label>
					<div class="col-sm-3">
					  <input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][phone_no]" value="" placeholder="<?php echo 'Phone Number'; ?>" id="input-phone_no<?php echo $extra_field_2; ?>" class="form-control" />
					</div>
				  </div>
				  <div class="form-group contact_row<?php echo $extra_field_2; ?>">
					<?php $tab_index_1 ++; ?>
					<label class="col-sm-1 control-label" for="input-mobile1"><?php echo 'Mobile 1'; ?></label>
					<div class="col-sm-3">
					  <input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][mobile1]" value="" placeholder="<?php echo 'Mobile 1'; ?>" id="input-mobile1<?php echo $extra_field_2; ?>" class="form-control" />
					</div>
					<?php $tab_index_1 ++; ?>
					<label class="col-sm-1 control-label" for="input-mobile2"><?php echo 'Mobile 2'; ?></label>
					<div class="col-sm-3">
					  <input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][mobile2]" value="" placeholder="<?php echo 'Mobile 2'; ?>" id="input-mobile2<?php echo $extra_field_2; ?>" class="form-control" />
					</div>
					<?php $tab_index_1 ++; ?>
					<label class="col-sm-1 control-label" for="input-email_id"><?php echo 'Email'; ?></label>
					<div class="col-sm-3">
					  <input tabindex="<?php echo $tab_index_1; ?>" type="email" name="contact_datas[<?php echo $extra_field_2 ?>][email_id]" value="" placeholder="<?php echo 'Email'; ?>" id="input-email_id<?php echo $extra_field_2; ?>" class="form-control" />
					</div>
					<div style="margin-top: 1%;" class="pull-right col-sm-1">
					  	<button type="button" onclick="remove_folder_2('<?php echo $extra_field_2; ?>')" data-toggle="tooltip" class="btn btn-danger" data-original-title="Remove" id="remove_2<?php echo $extra_field_2; ?>" ><i class="fa fa-minus-circle"></i></button>
					</div>
				  </div>
				  <?php $extra_field_2 ++; ?>
				<?php } ?>
			  </div>
			  <input type="hidden" id="extra_field_2" name="extra_field_2" value="<?php echo $extra_field_2; ?>" />
			  <input type="hidden" id="tab_index_1" name="tab_index_1" value="<?php echo $tab_index_1; ?>" />
			</div>
			</div>
		  </div>
		</form>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript">
$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY',
});
</script>
<script type="text/javascript"><!--

var extra_field = $('#extra_field').val();
var tab_index_3 = $('#tab_index_3').val();

function remove_folder(extra_field){
  $('#vat_row'+extra_field).remove();
}

$(document).on('keypress','input,select, textarea', function (e) {
//$('input,select').on('keypress', function (e) {
	if (e.which == 13) {
		e.preventDefault();
		var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
		if (!$next.length) {
			$next = $('[tabIndex=1]');
		}
		$next.focus();
	}
});

$('.payment_details').on('change', function() {
  payment_details = $('.payment_details').val();
  if(payment_details == 'Advance'){
	html = '<input style="width:10%;display: inline-block;" tabindex="6" type="text" name="percent_text" value="" placeholder="" id="input-percent_text" class="form-control" /> % Advance and Balance after';
	html += '<input style="width:10%;display: inline-block;" tabindex="7" type="text" name="days_text" value="" placeholder="" id="input-days_text" class="form-control" /> days of delivery & submission of Invoice';
  } else if(payment_details == 'Credit') {
	html = '<input style="width:10%;display: inline-block;" tabindex="6" type="text" name="days_text" value="" placeholder="" id="input-days_text" class="form-control" /> days of delivery & submission of Invoice';
  } else if(payment_details == 'Pdc') {
	html = '<input style="width:10%;display: inline-block;" tabindex="6" type="text" name="days_text" value="" placeholder="" id="input-days_text" class="form-control" /> days from date of order';
  } else {
	html = '';
  }
  $('.payment_text').html(html);
});

$('#button-upload').on('click', function() {
  $('#form-upload').remove();
  
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');
  
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  
  timer = setInterval(function() {
	if ($('#form-upload input[name=\'file\']').val() != '') {
	  clearInterval(timer);   
	  
	  $.ajax({
		url: 'index.php?route=catalog/vendor/upload&token=<?php echo $token; ?>',
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-upload')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
			console.log(json);
		  if (json['error']) {
			alert(json['error']);
		  }
				
		  if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'filename\']').attr('value', json['filename']);
			$('input[name=\'mask\']').attr('value', json['mask']);
			$('input[name=\'uploaded_file_source\']').attr('value', json['link_href']);
		  }
		  var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="uploaded_file_source" href="'+json['link_href']+'">View Document</a>';
		  $('#uploaded_file_source').remove();
		  $('#button-other_document_1_new').append(previewHtml);
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});

$('#button-upload2').on('click', function() {
  $('#form-upload2').remove();
  
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload2" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload2 input[name=\'file\']').trigger('click');
  
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  
  timer = setInterval(function() {
	if ($('#form-upload2 input[name=\'file\']').val() != '') {
	  clearInterval(timer);   
	  
	  $.ajax({
		url: 'index.php?route=catalog/vendor/upload&token=<?php echo $token; ?>',
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-upload2')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload2').button('loading');
		},
		complete: function() {
		  $('#button-upload2').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
				
		  if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'filename2\']').attr('value', json['filename']);
			$('input[name=\'mask2\']').attr('value', json['mask']);
			$('input[name=\'uploaded_file_source2\']').attr('value', json['link_href']);
		  }
		  var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="uploaded_file_source2" href="'+json['link_href']+'">View Document</a>';
		  $('#uploaded_file_source2').remove();
		  $('#button-other_document_2_new').append(previewHtml);
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});

$('#button-upload3').on('click', function() {
  $('#form-upload3').remove();
  
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload3" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload3 input[name=\'file\']').trigger('click');
  
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  
  timer = setInterval(function() {
	if ($('#form-upload3 input[name=\'file\']').val() != '') {
	  clearInterval(timer);   
	  
	  $.ajax({
		url: 'index.php?route=catalog/vendor/upload&token=<?php echo $token; ?>',
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-upload3')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload3').button('loading');
		},
		complete: function() {
		  $('#button-upload3').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
				
		  if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'filename3\']').attr('value', json['filename']);
			$('input[name=\'mask3\']').attr('value', json['mask']);
			$('input[name=\'uploaded_file_source3\']').attr('value', json['link_href']);
		  }
		  var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="uploaded_file_source3" href="'+json['link_href']+'">View Document</a>';
		  $('#uploaded_file_source3').remove();
		  $('#button-other_document_3_new').append(previewHtml);
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});

$('#button-upload4').on('click', function() {
  $('#form-upload4').remove();
  
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload4" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload4 input[name=\'file\']').trigger('click');
  
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  
  timer = setInterval(function() {
	if ($('#form-upload4 input[name=\'file\']').val() != '') {
	  clearInterval(timer);   
	  
	  $.ajax({
		url: 'index.php?route=catalog/vendor/upload&token=<?php echo $token; ?>',
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-upload4')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload4').button('loading');
		},
		complete: function() {
		  $('#button-upload4').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
				
		  if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'filename4\']').attr('value', json['filename']);
			$('input[name=\'mask4\']').attr('value', json['mask']);
			$('input[name=\'uploaded_file_source4\']').attr('value', json['link_href']);
		  }
		  var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="uploaded_file_source4" href="'+json['link_href']+'">View Document</a>';
		  $('#uploaded_file_source4').remove();
		  $('#button-other_document_4_new').append(previewHtml);
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});

$('#button-upload5').on('click', function() {
  $('#form-upload5').remove();
  
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload5" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload5 input[name=\'file\']').trigger('click');
  
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  
  timer = setInterval(function() {
	if ($('#form-upload5 input[name=\'file\']').val() != '') {
	  clearInterval(timer);   
	  
	  $.ajax({
		url: 'index.php?route=catalog/vendor/upload&token=<?php echo $token; ?>',
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-upload5')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload5').button('loading');
		},
		complete: function() {
		  $('#button-upload5').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
				
		  if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'filename5\']').attr('value', json['filename']);
			$('input[name=\'mask5\']').attr('value', json['mask']);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});
var extra_field_1 = $('#extra_field_1').val();
var tab_index_2 = $('#tab_index_2').val();

function remove_folder_1(extra_field_1){
  $('.bank_row'+extra_field_1).remove();
}

var extra_field_2 = $('#extra_field_2').val();
var tab_index_1 = $('#tab_index_1').val();
function add_new_2() {
  html = '<div class="form-group contact_row' + extra_field_2 + '">';
	tab_index_1 ++;
	html += '<label class="col-sm-1 control-label" for="input-name'+extra_field_2+'" >Name '+extra_field_2+'</label>';
	html += '<div class="col-sm-3">';
	  html += '<input tabindex="'+tab_index_1+'" type="text" name="contact_datas['+extra_field_2+'][name]" value="" placeholder="Name" id="input-name'+extra_field_2+'" class="form-control" />';
	html += '</div>';
	tab_index_1 ++;
	html += '<label class="col-sm-1 control-label" for="input-designation'+extra_field_2+'" >Designation</label>';
	html += '<div class="col-sm-3">';
	  html += '<input tabindex="'+tab_index_1+'" type="text" name="contact_datas['+extra_field_2+'][designation]" value="" placeholder="Designtion" id="input-designtion'+extra_field_2+'" class="form-control" />'; 
	html += '</div>';
	tab_index_1 ++;
	html += '<label class="col-sm-1 control-label" for="input-phone_no'+extra_field_2+'" >Phone Number</label>';
	html += '<div class="col-sm-3">';
	  html += '<input tabindex="'+tab_index_1+'" type="text" name="contact_datas['+extra_field_2+'][phone_no]" value="" placeholder="Phone Number" id="input-phone_no'+extra_field_2+'" class="form-control" />';
	html += '</div>';
  html += '</div>';
  html += '<div class="form-group contact_row' + extra_field_2 + '">';
	tab_index_1 ++;
	html += '<label class="col-sm-1 control-label" for="input-mobile1'+extra_field_2+'" >Mobile 1</label>';
	html += '<div class="col-sm-3">';
	  html += '<input tabindex="'+tab_index_1+'" type="text" name="contact_datas['+extra_field_2+'][mobile1]" value="" placeholder="Mobile 1" id="input-mobile1'+extra_field_1+'" class="form-control" />';
	html += '</div>';
	tab_index_1 ++;
	html += '<label class="col-sm-1 control-label" for="input-mobile2'+extra_field_2+'" >Mobile 2</label>';
	html += '<div class="col-sm-3">';
	  html += '<input tabindex="'+tab_index_1+'" type="text" name="contact_datas['+extra_field_2+'][mobile2]" value="" placeholder="Mobile 2" id="input-mobile2'+extra_field_1+'" class="form-control" />';
	html += '</div>';
	tab_index_1 ++;
	html += '<label class="col-sm-1 control-label" for="input-email_id'+extra_field_2+'" >Email</label>';
	html += '<div class="col-sm-3">';
	  html += '<input tabindex="'+tab_index_1+'" type="email" name="contact_datas['+extra_field_2+'][email_id]" value="" placeholder="Email" id="input-email_id'+extra_field_1+'" class="form-control" />';
	html += '</div>';
	html += '<div style="margin-top: 1%;" class="pull-right col-sm-1">';
	html += '<button type="button" onclick="remove_folder_2('+extra_field_2+')" data-toggle="tooltip" class="btn btn-danger" data-original-title="Remove" id="remove_2'+extra_field_2+'" ><i class="fa fa-minus-circle"></i></button>';
	html += '</div>';
  html += '</div>';  
  $('.main_div_2').append(html);
  extra_field_2++;
}

function remove_folder_2(extra_field_2){
  $('.contact_row'+extra_field_2).remove();
}

//--></script></div> 
<?php echo $footer; ?>