<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="col-sm-2 panel-title"><i class="fa fa-pencil"></i> <?php echo 'View Trainer'; ?></h3>
                <?php if ($id) { ?>
                    <div style="padding-left: 40%;" class="">
                        <b><input style="font-family: cursive;color: #00a04d;font-size: 20px;border: none;background: #fcfcfc;width: 80%;text-transform: uppercase;" disabled="disabled" id="name1" value="<?php echo $trainer_name; ?>"></b>
                    <b>Trainer Code : <?php echo $trainer_code; ?> </b>
                </div>
                <?php } ?>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab">Trainer Basic Details</a></li>
                        <li><a href="#tab_trainer_contact_tax_details" data-toggle="tab">Trainer Contact & Tax Details</a></li>
                        <?php  if($id !=''){ ?>
                        <li><a href="#tab_owner_authority_to_trainer" data-toggle="tab">Owner Authority to trainer</a></li>
                        <li><a href="#tab_trainer_ban_details2" data-toggle="tab">Trainer Ban Details</a></li>
                        <li><a href="#tab_horse_incharge" data-toggle="tab">List of Horses In-charge</a></li>
                        <li><a href="#tab_staff" data-toggle="tab">List of Staff</a></li>
                        <li><a href="#tab_transaction" data-toggle="tab">Transaction</a></li>
                        <?php  } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                             <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-trainer-code">Trainer Code</label>
                                <div class="col-sm-3">
                                    <div style="margin-top: 8px;" class="col-sm-3"  >
                                        <?php echo $trainer_code_new; ?>
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label" for="racing_name">Profile Pic:</label>
                                <div class="col-sm-5">
                                    
                                    <?php if($img_path != "#") { //echo $img_path;exit; ?>
                                        <span id="profile_pic" class="col-sm-8" ><img src="<?php echo $img_path; ?>" height="100px" id="blah" alt=""  /></span>
                                    <?php } ?>
                                </div>

                                <label class="col-sm-2 control-label" for="input-horse" style="display: none;"><?php echo "Arrival Charges to be Paid:"; ?></label>
                                <div class="col-sm-3" style="display: none;">
                                    <input type="text" name="arrival_charges_to_be_paid" value="<?php echo $arrival_charges_to_be_paid; ?>" placeholder="<?php echo "Arrival Charges to be Paid"; ?>" id="arrival_charges_to_be_paid" class="form-control" tabindex="2" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-trainer-name">Trainer Racing Name:</label>
                                
                                <div style="margin-top: 8px;" class="col-sm-2"  >
                                     <?php echo $trainer_name; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="input-racing_name"><?php echo "Trainer Entry Name:"; ?></label>
                               
                                <div style="margin-top: 8px;" class="col-sm-2"  >
                                     <?php echo $racing_name; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="date_of_birth"><?php echo "Birth Date:"; ?></label>
                               
                                <div style="margin-top: 8px;" class="col-sm-2"  >
                                     <?php echo $date_of_birth; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">License Type(A/B):</label>
                                
                                <div style="margin-top: 8px;" class="col-sm-2"  >
                                     <?php echo $license_type; ?>
                                </div>
                                <?php if ($license_type == 'B') { ?>
                                    <label style="width: 215px;" class="col-sm-2 control-label" for="input-center_holding_license_type" class="center_holding_license_type"><?php echo "Center Holding License Type:" ?></label>
                                     <div style="margin-top: 8px;" class="col-sm-3"  >
                                         <?php echo $status; ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label" for="input-top"><?php echo "WITA Member:"; ?></label>
                                
                                <div style="margin-top: 8px;" class="col-sm-2"  >
                                     <?php echo $is_wita; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="date_of_license_issue"><?php echo "License Issued On (First Time)"; ?></label>
                                
                                <div style="margin-top: 8px;" class="col-sm-2"  >
                                     <?php echo $date_of_license_issue; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="date_of_license_issue2"><?php echo "License Renewal Date"; ?></label>
                                
                                <div style="margin-top: 8px;" class="col-sm-2"  >
                                     <?php echo $date_of_license_issue2; ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="input-private_trainer"><?php echo "Private Trainer:" ?></label>
                                
                                <div style="margin-top: 8px;" class="col-sm-3"  >
                                     <?php echo $private_trainer; ?>
                                </div>
                                <div class="col-sm-2 add-owner-div"  style="display:none;">
                                    <a id="add-ownrs" class="btn btn-primary"><i class="fa fa-plus"></i> Add Owner</a>
                                </div>
                                <div class="col-sm-3 owner_data-div">
                                    <?php  $cnt_total = 0;
                                     if($private_trainers_owner_data){ ?>
                                        <h3 style="color: black;font-weight: bold;">Owners</h3>
                                        <?php foreach($private_trainers_owner_data as $key => $value){ ?>
                                            <div class="form-group" id="remove_div_<?php echo $key ?>">
                                                <div style="margin-top: 8px;" >
                                                    <?php echo $value['owner_name']; ?>
                                                </div>
                                            </div>
                                        <?php $cnt_total++; } ?>
                                    <?php } ?>
                                </div>
                                <input type="hidden" value="<?php echo $cnt_total; ?>" class="owners_cnt" id="owners_cnt">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-file_number"><?php echo "File Number:"; ?></label>
                                
                                <div style="margin-top: 8px;" class="col-sm-3"  >
                                     <?php echo $file_number; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="assistant_trainer"><?php echo "Assistant Trainer:" ?></label>
                                
                                <div style="margin-top: 8px;" class="col-sm-3"  >
                                     <?php echo $assistant_trainer; ?>
                                </div>
                                <?php if ($assistant_trainer >= '1') { ?>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-assistant_trainer" id="assistant_trainer_one_label"><?php echo "Assistant Trainer 1:" ?></label>
                                        <div style="margin-top: 8px;" class="col-sm-3"  >
                                            <?php echo $assistant_trainer_1; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($assistant_trainer >= '2') { ?>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-assistant_trainer" id="assistant_trainer_one_label"><?php echo "Assistant Trainer 2:" ?></label>
                                        <div style="margin-top: 8px;" class="col-sm-3"  >
                                            <?php echo $assistant_trainer_2; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($assistant_trainer >= '3') { ?>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-assistant_trainer" id="assistant_trainer_one_label"><?php echo "Assistant Trainer 3:" ?></label>
                                        <div style="margin-top: 8px;" class="col-sm-3"  >
                                            <?php echo $assistant_trainer_3; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($assistant_trainer >= '4') { ?>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-assistant_trainer" id="assistant_trainer_one_label"><?php echo "Assistant Trainer 4:" ?></label>
                                        <div style="margin-top: 8px;" class="col-sm-3"  >
                                            <?php echo $assistant_trainer_4; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($assistant_trainer >= '5') { ?>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-assistant_trainer" id="assistant_trainer_one_label"><?php echo "Assistant Trainer 5:" ?></label>
                                        <div style="margin-top: 8px;" class="col-sm-3"  >
                                            <?php echo $assistant_trainer_5; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-group second_atnr1" style="display: none;">
                                <label class="col-sm-2 control-label" for="input-assistant_trainer" id="assistant_trainer_two_label" style="display: none;"><?php echo "Assistant Trainer 2:" ?></label>
                                <div class="col-sm-3 assistant_trainer_two" style="display: none;">
                                    <input type="text" name="assistant_trainer_2" value="<?php echo $assistant_trainer_2; ?>" placeholder="<?php echo "Assistant Trainer 2"; ?>" id="assistant_trainer_two" class="form-control" />
                                </div>
                                <label class="col-sm-2 control-label" for="input-assistant_trainer" id="assistant_trainer_three_label" style="display: none;"><?php echo "Assistant Trainer 3:" ?></label>
                                <div class="col-sm-3 assistant_trainer_three" style="display: none;">
                                    <input type="text" name="assistant_trainer_3" value="<?php echo $assistant_trainer_3; ?>"  placeholder="<?php echo "Assistant Trainer 3"; ?>" id="" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group third_atnr1" style="display: none;">
                                <label class="col-sm-2 control-label" for="input-assistant_trainer" id="assistant_trainer_four_label" style="display: none;"><?php echo "Assistant Trainer 4:" ?></label>
                                <div class="col-sm-3 assistant_trainer_four" style="display: none;">
                                    <input type="text" name="assistant_trainer_4" value="<?php echo $assistant_trainer_4; ?>"  placeholder="<?php echo "Assistant Trainer 4"; ?>" id="" class="form-control"/>
                                </div>
                                <label class="col-sm-2 control-label" for="input-assistant_trainer" id="assistant_trainer_five_label" style="display: none;"><?php echo "Assistant Trainer 5:" ?></label>
                                <div class="col-sm-3 assistant_trainer_five" style="display: none;">
                                    <input type="text" name="assistant_trainer_5" value="<?php echo $assistant_trainer_5; ?>"  placeholder="<?php echo "Assistant Trainer 5"; ?>" id="" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="select-isActive">Status</label>
                                <?php if ($isActive == '1') { ?>
                                    <div style="margin-top: 10px;" class="col-sm-3">
                                        In
                                    </div>
                                <?php } else { ?>
                                    <div style="margin-top: 10px;" class="col-sm-3">
                                        Exit
                                    </div>
                                <?php } ?>
                                 <label class="col-sm-2 control-label" for="remarks"><?php echo "Remark:" ?></label>
                                
                                <div style="margin-top: 8px;" class="col-sm-3"  >
                                     <?php echo $remarks; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="remarks"><?php echo "Last Update:" ?></label>
                                <div style="margin-top: 10px;" class="col-sm-10">
                                    <?php echo $last_upadate; ?> BY <?php echo $user; ?> 
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_trainer_contact_tax_details">
                                <label style="" class="col-sm-6 control-label alignLeft" for="input-address1"> Address 1</label>
                                <label class="col-sm-6 control-label alignLeft" for="input-address2">Address 2</label>
                            <div class="form-group">
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $address_1; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $address_2; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $address_3; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $address_4; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label style="" class="col-sm-3 control-label alignLeft" for="input-address2">Locality area or street 1</label>
                                    <label style="" class="col-sm-3 control-label alignLeft" for="input-address1"> City 1</label>
                                    <label style="" class="col-sm-3 control-label alignLeft" for="input-address2">Locality area or street 2</label>
                                    <label style="" class="col-sm-3 control-label alignLeft" for="input-address2">City 2</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $local_area_1; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $city_1; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $local_area_2; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $city_2; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label style="" class="col-sm-3 control-label alignLeft" for="input-address1"> Country 1</label>
                                <label class="col-sm-3 control-label alignLeft" for="input-address2">State 1</label>
                                <label class="col-sm-3 control-label alignLeft" for="input-address2">Country 2</label>
                                <label class="col-sm-3 control-label alignLeft" for="input-address2">State 2</label>
                                <div class="col-sm-3">
                                  <select disabled="disabled" style="color: black;border: white;background: white;-webkit-appearance: none;" name="country1" data-index="19" id="input-country" onchange="country(this);" class="">
                                    <option value="">Country</option>
                                    <?php foreach ($countries as $country) { ?>
                                    <?php if ($country['country_id'] == $country_1) { ?>
                                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>

                                <div class="col-sm-3">
                                  <select disabled="disabled" style="color: black;border: white;background: white;-webkit-appearance: none;" name="zone_id" data-index="20" id="zone_id" class="">
                                    <option value="">State</option>
                                    <?php foreach ($zones as $zone) { ?>
                                    <?php if ($zone['zone_id'] == $zone_id) { ?>
                                    <option value="<?php echo $zone['zone_id']; ?>" selected="selected"><?php echo $zone['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $zone_id; ?>"><?php echo $zone['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>


                                <div class="col-sm-3">
                                  <select disabled="disabled" style="color: black;border: white;background: white;-webkit-appearance: none;" name="country2" data-index="25" id="input-country2" onchange="countrys(this);" class="">
                                    <option value="">Country</option>
                                    <?php foreach ($countries as $country2) { ?>
                                    <?php if ($country2['country_id'] == $country_2) { ?>
                                    <option value="<?php echo $country2['country_id']; ?>" selected="selected"><?php echo $country2['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $country2['country_id']; ?>"><?php echo $country2['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>

                                <div class="col-sm-3">
                                  <select disabled="disabled" style="color: black;width: 100px;border: white;background: white;-webkit-appearance: none;" name="zone_id2" data-index="26" id="zone_id2" class="">
                                    <option value="0">State</option>
                                    <?php foreach ($zones2 as $zone2) { ?>
                                    <?php if ($zone2['zone_id'] == $zone_id2) { ?>
                                    <option value="<?php echo $zone2['zone_id']; ?>" selected="selected"><?php echo $zone2['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $zone2['zone_id']; ?>"><?php echo $zone2['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label style="" class="col-sm-6 control-label alignLeft" for="input-address1"> Pincode 1</label>
                                    <label class="col-sm-6 control-label alignLeft" for="input-address2">Pincode 2</label>
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $pincode_1; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $pincode_2; ?>
                                </div>
                            </div>
                            <div class="form-group">
                            <h4 style="margin-left: 15px;" >Contact Details</h4>
                                <label class="col-sm-2 control-label" for="phone_no">Phone No</label>
                                
                                <div style="margin-top: 8px;" class="col-sm-3"  >
                                     <?php echo $phone_no; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mobile_no1"> Mobile No 1</label>
                                
                                <div style="margin-top: 8px;" class="col-sm-4"  >
                                     <?php echo $mobile_no; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="altMobileNo">Alternate Mobile No</label>
                                
                                <div style="margin-top: 8px;" class="col-sm-4"  >
                                     <?php echo $alternate_mob_no; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="emailId"> Email id</label>
                                
                                <div style="margin-top: 8px;" class="col-sm-4"  >
                                     <?php echo $email_id; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="altEmailId">Alternate Email id</label>
                                
                                <div style="margin-top: 8px;" class="col-sm-4"  >
                                     <?php echo $alternate_email_id; ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                            <h4 style="margin-left: 15px;" >Tax Details</h4>
                                <label class="col-sm-2 control-label" for="gst_type"> GST Type</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $gst_type; ?>
                                </div>
                                <?php if($gst_type == 'R'){ ?>
                                    <label class="col-sm-2 control-label" id="gst_label" for="gst_no">GST No</label>
                                    <div style="margin-top: 10px;" class="col-sm-3">
                                        <?php echo $gst_no; ?>
                                    </div>

                                <?php } else { ?>
                                    <label id="gst_label" class="col-sm-2 control-label" for="gst_no">GST No</label>
                                    <div style="margin-top: 10px;" class="col-sm-3">
                                        <?php echo $gst_no; ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="pan_no">PAN Number</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $pan_no; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="prof_tax_no">Prof Tax No details</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $prof_tax_no; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="prof_tax_no">EPF No</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $epf_no; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden"  name="upload_hidden_id" id="upload_hidden_id" value="<?php echo $upload_hidden_id; ?>">
                                <table id="itrUpload" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr><td colspan="3" >ITR Upload</td></tr>
                                        <tr>
                                            <td class="text-left">Assesment Year</td>
                                            <td class="text-left">File Name</td>
                                        </tr>
                                    </thead>
                                    <tbody id="upload_itr">
                                        <?php //echo'<pre>';print_r($juploaditr);  ?>
                                            <?php foreach($uploaditr as $key => $result) { ?>
                                                <tr id="recurring-row<?php echo $key; ?>">
                                                    <td>
                                                        <div style="margin-top: 10px;" class="col-sm-3">
                                                            <?php echo $result['assesment_year']; ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div style="margin-top: 10px;" class="col-sm-3">
                                                            <?php echo $result['file']; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                        <?php } //exit; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane abc" id="tab_owner_authority_to_trainer">
                            <h3>Autority</h3>
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="text-center">Owner Name</td>
                                        <td class="text-center">Start Date</td>
                                        <td class="text-center">End Date</td>
                                        <td style="display: none;">SubAuthority</td>
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody id="authrity-table">
                                        <?php if(isset($authorityarray) ){  ?>
                                            <?php foreach($authorityarray as $key => $result) { ?>
                                                <tr class='par' id='par<?php echo $key ?>'>
                                                    <td>
                                                        <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="authorityarrays[<?php echo $key ?>][check_name]" id="club_'+auto_id+'"  value="<?php echo $result['check_name'] ?>">
                                                    </td>
                                                    <td class="text-right">
                                                        <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="authorityarrays[<?php echo $key ?>][start_date]" id="club_'+auto_id+'"  value="<?php echo $result['start_date'] ?>">
                                                    </td>
                                                    <td class="text-right">
                                                        <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="authorityarrays[<?php echo $key ?>][end_date]" id="club_'+auto_id+'"  value="<?php echo $result['end_date'] ?>">
                                                    </td>
                                                    <td class="text-center">
                                                       
                                                    </td>
                                            <?php } ?>
                                        <?php } ?>
                                </tbody>
                            </table>
                            <br>
                            <h3>Sub Autority</h3>
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="text-center">Owner Name</td>
                                        <td class="text-center">Start Date</td>
                                        <td class="text-center">End Date</td>
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody id="sub_authrity-table">
                                        <?php if(isset($Subauthorityarray) ){  ?>
                                            <?php foreach($Subauthorityarray as $key => $result) { ?>
                                                <tr class='par' id='para<?php echo $key ?>'>
                                                    <td>
                                                        <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="Subauthorityarray[<?php echo $key ?>][owner_name]" id="club_<?php echo $key ?>"  value="<?php echo $result['owner_name'] ?>">
                                                    </td>
                                                    <td class="text-right">
                                                        <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="Subauthorityarray[<?php echo $key ?>][start_date]" id="club_'+auto_id+'"  value="<?php echo $result['start_date'] ?>">
                                                    </td>
                                                    <td class="text-right">
                                                        <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="Subauthorityarray[<?php echo $key ?>][end_date]" id="club_'+auto_id+'"  value="<?php echo $result['end_date'] ?>">
                                                    </td>
                                                    <td class="text-center">
                                                        
                                                    </td>
                                            <?php } ?>
                                        <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab_trainer_ban_details2">
                            <div class="col-sm-11">
                                <h4>Trainer Ban Details</h4>
                            </div>
                           
                            <div class="form-group">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                          <td class="text-center"><?php echo "Club"; ?></td>
                                          <td class="text-center"><?php echo "Start Date"; ?></td>
                                          <td class="text-center"><?php echo "End Date"; ?></td>
                                          <td class="text-center"><?php echo "Start Date"; ?></td>
                                          <td class="text-center"><?php echo "End Date"; ?></td>
                                          <td class="text-center"><?php echo "Authority"; ?></td>
                                          <td class="text-center"><?php echo "Reason"; ?></td>
                                          <td class="text-center"><?php echo "Action"; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody id="bandeatilsbody">
                                        <?php if(isset($bandeatils)) { //echo'<pre>';print_r($bandeatils); exit; ?>
                                            <?php foreach($bandeatils as $bankey => $banvalue) { 
                                                if($banvalue['end_date1'] == '' || $banvalue['end_date1'] == '0000-00-00'){
                                                    $enddatebans = ''; 
                                                } else {
                                                    $enddatebans = date('d-m-Y', strtotime($banvalue['end_date1']));
                                                } 
                                                if($banvalue['start_date1'] == '' || $banvalue['start_date1'] == '0000-00-00'){
                                                    $startdatebans = '';
                                                } else {
                                                    $startdatebans = date('d-m-Y', strtotime($banvalue['start_date1']));
                                                }
                                                if($banvalue['end_date2'] == '' || $banvalue['end_date2'] == '0000-00-00'){
                                                    $enddatebans2 = ''; 
                                                } else {
                                                    $enddatebans2 = date('d-m-Y', strtotime($banvalue['end_date2']));
                                                } 
                                                if($banvalue['start_date2'] == '' || $banvalue['start_date2'] == '0000-00-00'){
                                                    $startdatebans2 = '';
                                                } else {
                                                    $startdatebans2 = date('d-m-Y', strtotime($banvalue['start_date2']));
                                                }
                                            ?>
                                                <tr id='bandetail_<?php echo $bankey ?>'>
                                                    <td class="text-left"><span id="clubs_<?php echo $bankey ?>" ><?php echo $banvalue['club'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][club]" id="club_<?php echo $bankey ?>"  value="<?php echo $banvalue['club'] ?>">
                                                    </td>
                                                    <td class="text-right"><span id="start_dateban_<?php echo $bankey ?>"><?php echo $startdatebans; ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][start_date1]" id="start_date_bans_<?php echo $bankey ?>"  value="<?php echo $startdatebans; ?>">
                                                    </td>
                                                    <td class="text-right"><span id="end_dateban_<?php echo $bankey ?>"><?php echo $enddatebans; ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][end_date1]" id="end_date_ban_<?php echo $bankey ?>"  value="<?php echo $enddatebans; ?>">
                                                    </td>
                                                    <td class="text-right"><span id="start_dateban2_<?php echo $bankey ?>"><?php echo $startdatebans2; ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][start_date2]" id="start_date_bans2_<?php echo $bankey ?>"  value="<?php echo $startdatebans2; ?>">
                                                    </td>
                                                     <td class="text-right"><span id="end_dateban2_<?php echo $bankey ?>"><?php echo $enddatebans2; ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][end_date2]" id="end_date_ban2_<?php echo $bankey ?>"  value="<?php echo $enddatebans2; ?>">
                                                    </td>
                                                    <td class="text-left"><span id="authorityban_<?php echo $bankey ?>" ><?php echo $banvalue['authority'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][authority]" id="authority_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['authority'] ?>">
                                                    </td>
                                                    <td class="text-left"><span  id="reasonban_<?php echo $bankey ?>"><?php echo $banvalue['reason'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][reason]" id="reason_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['reason'] ?>">
                                                        <!-- <span  id="amountban_<?php echo $bankey ?>"><?php echo $banvalue['amount'] ?></span> -->
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][amount]" id="amount_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['amount'] ?>">
                                                    </td>
                                                    <td class="text-center"> 
                                                        
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-11">
                                <h4>Trainer Ban History</h4>
                            </div>
                            <div class="form-group">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                          <td class="text-center" ><?php echo "Club"; ?></td>
                                          <td class="text-center" ><?php echo "Start Date"; ?></td>
                                          <td class="text-center" ><?php echo "End Date"; ?></td>
                                          <td class="text-center" ><?php echo "Authority"; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody id="banhistorybody">
                                        <?php if(isset($bandatas)) { //echo'<pre>';print_r($bandatas); exit(); ?>
                                            <?php foreach($bandatas as $bkey => $bvalue) { 
                                            if($bvalue['end_date1'] == '' || $bvalue['end_date1'] == '0000-00-00'){
                                                    $enddatebans = ''; 
                                                } else {
                                                    $enddatebans = date('d-m-Y', strtotime($bvalue['end_date1']));
                                                } 
                                                if($bvalue['start_date1'] == '' || $bvalue['start_date1'] == '0000-00-00'){
                                                    $startdatebans = '';
                                                } else {
                                                    $startdatebans = date('d-m-Y', strtotime($bvalue['start_date1']));
                                                }
                                                if($bvalue['end_date2'] == '' || $bvalue['end_date2'] == '0000-00-00'){
                                                    $enddatebans2 = ''; 
                                                } else {
                                                    $enddatebans2 = date('d-m-Y', strtotime($bvalue['end_date2']));
                                                } 
                                                if($bvalue['start_date2'] == '' || $bvalue['start_date2'] == '0000-00-00'){
                                                    $startdatebans2 = ''; 
                                                } else {
                                                    $startdatebans2 = date('d-m-Y', strtotime($bvalue['start_date2']));
                                                } ?>
                                                <tr id='banhistorybody<?php echo $bkey ?>'>
                                                    <td class="text-left" > 
                                                        <?php echo $bvalue['club'] ?>
                                                        <input type= "hidden" name="banhistorydatas[<?php echo $bkey ?>][history_club]" id="history_club_<?php echo $bkey ?>" value="<?php echo $bvalue['club'] ?>" >
                                                    </td>
                                                    <td class="text-right" >
                                                        <?php echo date('d-m-Y', strtotime($startdatebans)); ?>
                                                        <input type= "hidden" name="banhistorydatas[<?php echo $bkey ?>][history_startdate_ban]" id="history_startdate_ban_<?php echo $bkey ?>"  value="<?php echo date('d-m-Y', strtotime($startdatebans)); ?>" >
                                                    </td>
                                                    <td class="text-right" >
                                                        <?php echo date('d-m-Y', strtotime($enddatebans)); ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_enddate_ban]" id="history_enddate_ban_<?php echo $bkey ?>"  value="<?php echo date('d-m-Y', strtotime($enddatebans)); ?>" >
                                                    </td>
                                                    <td class="text-left" >
                                                        <?php echo $bvalue['authority']; ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_authority]" id="history_authority_<?php echo $bkey ?>"  value="<?php echo $bvalue['authority']; ?>" >
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_startdate_ban2]" id="history_startdate_ban2_<?php echo $bkey ?>"  value="<?php echo $startdatebans2; ?>" >

                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_enddate_ban2]" id="history_enddate_ban2_<?php echo $bkey ?>"  value="<?php echo $enddatebans2; ?>" >

                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_reason]" id="history_reason_<?php echo $bkey ?>"  value="<?php echo $bvalue['reason']; ?>" >

                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_amount]" id="history_amount_<?php echo $bkey ?>"  value="<?php echo $bvalue['amount']; ?>" >
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_horse_incharge">
                           
                            <div class="col-sm-12">
                                <h4>List of Horses In-charge</h4>
                            </div>
                            <div style="margin: 15px;" class="form-group">
                                <label class="pull-left control-label" for="Search">Search</label>
                                <div class="col-sm-2">
                                    <input id="myInput" type="text" placeholder="Search.." class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table id="myTable" class="tablesorter table-sort table-striped table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Horse Name</th>
                                                <th>Age</th>
                                                <th>Equipment Name</th>
                                                <th>Ban Name</th>
                                                <th>Race Records</th>
                                                <th>Shoe/Git Name</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($horseDatas)) { ?>
                                                <?php $i=1; ?>
                                                <?php foreach ($horseDatas as $hourseskey => $horseData) { ?>
                                                    <tr>
                                                        <td class="text-left"><?php echo $i++; ?></td>
                                                        <td class="text-left"><?php echo $horseData['official_name']; ?></td>
                                                        <td class="text-left"><?php echo $horseData['age']; ?></td>
                                                        <td class="text-left"><?php echo $horseData['horse_equipment_name']; ?></td>
                                                        <td class="text-left"><?php echo $horseData['ban']; ?></td>
                                                        <td class="text-left"></td>
                                                        <td class="text-left"><?php echo $horseData['shoe_name']; ?></td>

                                                        <input type= "hidden"  name="horses[<?php echo $hourseskey ?>][official_name]" value="<?php echo $horseData['official_name']; ?>">
                                                        <input type= "hidden"  name="horses[<?php echo $hourseskey ?>][date_of_charge]" value="<?php echo $horseData['date_of_charge']; ?>">
                                                        <input type= "hidden"  name="horses[<?php echo $hourseskey ?>][extra_narration]" value="<?php echo $horseData['extra_narration']; ?>">
                                                        <input type= "hidden"  name="horses[<?php echo $hourseskey ?>][sire_name]" value="<?php echo $horseData['sire_name']; ?>">
                                                        <input type= "hidden"  name="horses[<?php echo $hourseskey ?>][age]" value="<?php echo $horseData['age']; ?>-<?php echo $horseData['dam_name']; ?>">
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_staff">
                            <div class="col-sm-12">
                                <h4>List of Staff</h4>
                            </div>
                            <div class="form-group">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                          <td style="width: 5%;" class="text-center" ><?php echo "Sr No"; ?></td>
                                          <td class="text-center" ><?php echo "Staff Name"; ?></td>
                                          <td class="text-center" ><?php echo "Designation"; ?></td>
                                          <td class="text-center" ><?php echo "Adhar Number"; ?></td>
                                          <td style="width: 15%;" class="text-center" ><?php echo "Profile Pic"; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(isset($staffs)) { ?>
                                            <?php $i=1; ?>
                                            <?php foreach($staffs as $skey => $svalue) { ?>
                                                <tr>
                                                    <td class="text-left" ><?php echo $i++; ?></td>
                                                    <td class="text-left" ><?php echo $svalue['staff'] ?></td>
                                                    <td class="text-left" ><?php echo $svalue['designation'] ?></td>
                                                    <td class="text-left" ><?php echo $svalue['adhar_no'] ?></td>
                                                    <td style="text-align: center;" ><img style="height: 90px;" src="<?php echo $svalue['uploaded_file_source'] ?>"></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_transaction">
                            <div class="col-sm-11">
                                <h4>Transaction</h4>
                            </div>
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td>Sr No</td>
                                                <td>License Renewal date</td>
                                                <td>Amount</td>
                                                <td>Fee Paid Type</td>
                                                <td>License Start Date</td>
                                                <td>License End Date</td>
                                                <td>License Type</td>
                                                <td>Apprenties</td>
                                                <input type= "hidden" name="" id="trainer_idss"  value="<?php echo $trainer_idss ?>">
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($Trainers)) { ?>
                                                <?php $i=1; ?>
                                                <?php foreach ($Trainers as $result) { ?>
                                                    <tr>
                                                        <td class="text-left"><?php echo $i++; ?></td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo date('d-m-Y', strtotime($result['renewal_date'])) ?>">
                                                        </td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['amount'] ?>">
                                                        </td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['fees'] ?>">
                                                        </td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['license_start_date'] ?>">
                                                        </td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['license_end_date'] ?>">
                                                        </td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['license_type'] ?>">
                                                        </td>
                                                        <td class="text-center">
                                                            <?php if($result['apprenties'] == 'Yes'){ ?>
                                                                <i class="fa fa-check" aria-hidden="true" style="color: green;font-size: 20px;"></i> 
                                                            <?php } else { ?>
                                                                <i></i>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                            <tr id="hidden_transaction" style="display: none;" >
                                                <td class="text-left">10</td>
                                                <td>
                                                    <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="hidden_renewal_datess"  value="<?php echo $result['renewal_date'] ?>">
                                                </td>
                                                <td>
                                                    <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="hidden_renewal_amt"  value="<?php echo $result['renewal_date'] ?>">
                                                </td>
                                                <td>
                                                    <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="hidden_renewal_fee"  value="<?php echo $result['renewal_date'] ?>">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content" style="width: 146%;margin-left: -20%;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Owner Authority To Trainer</h4>
                            </div>
                            <div class="modal-body" style="margin-right: 24px;">
                                <form class="form-horizontal"  id="form-options">
                                    <input type="hidden" name="auth_id" value="" id="auth_id" class="form-control" />
                                    <input type="hidden" name="hidden_trainer_id"  value="<?php echo $id; ?>" id="hidden_trainer_id" class="form-control" />
                                
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="owner_name"><b style="color: red">*</b><?php echo "Owner Name:"; ?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="owner_name" value="" placeholder="<?php echo "Owner Name"; ?>" id="owner_name" class="form-control" />
                                            
                                            <span style="display: none;color: red;font-weight: bold;" id="error_owner_name" class="error"><?php echo 'Please Select Owner Name'; ?></span>
                                            <input type="hidden" name="hidden_owner_id" value="" id="hidden_owner_id" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="start_date"><?php echo "Start Date:"; ?></label>
                                        <div class="col-sm-3">
                                            <div class="input-group date input-start_date">
                                                <input type="text" name="start_date" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="start_date" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <span style="display: none;color: red;font-weight: bold;" id="error_start_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                        </div>

                                        <label id="l_end_date" style="display: none;" class="col-sm-2 control-label" for="end_date"><?php echo "End Date:"; ?></label>
                                        <div class="col-sm-3" id="h_end_date" style="display: none;">
                                            <div class="input-group date input-end_date">
                                                <input type="text" name="end_date" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="end_date" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <span style="display: none;color: red;font-weight: bold;" id="error_end_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                            <span style="display: none;color: red;font-weight: bold;" id="error_greater_start_date" class="error"><?php echo 'End Date must not be less than Start Date!'; ?></span>
                                        </div>
                                        <input type="hidden" name="hidden_end_date" value="" id="hidden_end_date" class="form-control" />
                                        <label class="col-sm-1 control-label" for="start_date">Is Indefinite:</label>
                                        <!-- <div>
                                            <input class="col-sm-1" type="checkbox" id="" onclick="checkboxs();" name="" value="">
                                        </div> -->
                                        <div class="checkbox" >
                                            <label>
                                                <input id="is_indefinites" type="hidden" name="is_indefinite" value="N" />
                                                <input id="is_indefinite" onclick="checkboxs();" type="checkbox" name="is_indefinite" value="Y" checked="checked";  class="form-control" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="monthly_maint_cost_checked">Monthly Maint Cost:</label>
                                        <div class="col-sm-3">
                                            <!-- <select id="monthly_maint_cost" class="form-control" name="monthly_maint_cost" tabindex="" >
                                                <?php foreach($owner_autority as $skey => $svalue){
                                                 ?>
                                                    <?php if($skey){ ?>
                                                        <option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select> -->

                                            <div class="checkbox" >
                                                <label>
                                                    <input id="monthly_maint_costs" type="hidden" name="monthly_maint_cost" value="N" />
                                                    <input id="monthly_maint_cost" type="checkbox" name="monthly_maint_cost" value="Y" checked="checked";  class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="Commission_to_tm_and_jockey">Commission To Tm & Jockey:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="Commission_to_tm_and_jockeys" type="hidden" name="Commission_to_tm_and_jockey" value="N" />
                                                    <input id="Commission_to_tm_and_jockey" type="checkbox" name="Commission_to_tm_and_jockey" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="syces">Syces: Diwali, Yearly Bonus:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="sycess" type="hidden" name="syces" value="N" />
                                                    <input id="syces" type="checkbox" name="syces" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="Payment_other_clubs">Payment to other Clubs:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="Payment_other_clubss" type="hidden" name="Payment_other_clubs" value="N" />
                                                    <input id="Payment_other_clubs" type="checkbox" name="Payment_other_clubs" value="Y"checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="water_charges">Water Charges:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="water_chargess" type="hidden" name="water_charges" value="N" />
                                                    <input id="water_charges" type="checkbox" name="water_charges" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="stable_rent">Stable Rent:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="stable_rents" type="hidden" name="stable_rent" value="N" />
                                                    <input id="stable_rent" type="checkbox" name="stable_rent" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="extra_oats">Extra Oats:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="extra_oatss" type="hidden" name="extra_oats" value="N" />
                                                    <input id="extra_oats" type="checkbox" name="extra_oats" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="private_doctors_bills">Private Doctors Vet Bills:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="private_doctors_billss" type="hidden" name="private_doctors_bills" value="N" />
                                                    <input id="private_doctors_bills" type="checkbox" name="private_doctors_bills" value="Y"checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="eia_test_charges">EIA Test Charges:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="eia_test_chargess" type="hidden" name="eia_test_charges" value="N" />
                                                    <input id="eia_test_charges" type="checkbox" name="eia_test_charges" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="extras">Extras:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="extrass" type="hidden" name="extras" value="N" />
                                                    <input id="extras" type="checkbox" name="extras" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="electricitty_charges">Electricitty Charges:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="electricitty_chargess" type="hidden" name="electricitty_charges" value="N" />
                                                    <input id="electricitty_charges" type="checkbox" name="electricitty_charges" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="buy">Buy:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="buys" type="hidden" name="buy" value="N" />
                                                    <input id="buy" type="checkbox" name="buy" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="sell">Sell:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="sells" type="hidden" name="sell" value="N" />
                                                    <input id="sell" type="checkbox" name="sell" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="any_other_payment">Any Other Payment</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="any_other_payments" type="hidden" name="any_other_payment" value="N" />
                                                    <input id="any_other_payment" type="checkbox" name="any_other_payment" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="appoint_sub_agent">Appoint Sub_agent:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="appoint_sub_agents" type="hidden" name="appoint_sub_agent" value="N" />
                                                    <input id="appoint_sub_agent" type="checkbox" name="appoint_sub_agent" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="remarks"><b style="color: red"></b>Remarks</label>
                                        <div class="col-sm-3">
                                            <input type="text" name="remarks" value="" placeholder="Remarks" id="remarkss" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-3">
                                        </div>
                                        <div style="">
                                            <button type="button" class="btn btn-primary" id = "ownersave" onclick="SaveAuthorityFunction();" style="margin-left: 230px;">Save</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left: 64px;">Close</button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="hidden_owner-save" id="hidden_owner-save" value="">
                                </form>
                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
                </div>
                <div id="sub_myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content" style="width: 146%;margin-left: -20%;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Sub Owner Authority To Trainer</h4>
                            </div>
                            <div class="modal-body" style="margin-right: 24px;">
                                <form class="form-horizontal"  id="sub-form-options">
                                    <input type="hidden" name="sub_autho_id" value="" id="sub_autho_id" class="form-control" />
                                    <input type="hidden" name="hidden_trainer_id"  value="<?php echo $id; ?>" id="hidden_trainer_id" class="form-control" />

                                     <input type="hidden" name="Sub_hidden_owner_save"  value="" id="Sub_hidden_owner_save" class="form-control" />
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="sub_owner_name"><b style="color: red">*</b><?php echo "Owner Name:"; ?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="sub_owner_name" value="" placeholder="<?php echo "Owner Name"; ?>" id="sub_owner_name" class="form-control" />
                                            
                                            <span style="display: none;color: red;font-weight: bold;" id="error_owner_name" class="error"><?php echo 'Please Select Owner Name'; ?></span>
                                            <input type="hidden" name="sub_hidden_owner_id" value="" id="sub_hidden_owner_id" class="form-control" />
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-sm-2 control-label" for="sub_trainer_name"><b style="color: red">*</b><?php echo "Trainer Name:"; ?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="sub_trainer_name" value="" placeholder="<?php echo "Trainer Name"; ?>" id="sub_trainer_name" class="form-control" />
                                            
                                            <span style="display: none;color: red;font-weight: bold;" id="error_trainer_name" class="error"><?php echo 'Please Select Owner Name'; ?></span>
                                            <input type="hidden" name="sub_hidden_trainer_id" value="" id="sub_hidden_trainer_id" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="sub_start_date"><?php echo "Start Date:"; ?></label>
                                        <div class="col-sm-3">
                                            <div class="input-group date input-sub_start_date">
                                                <input type="text" name="sub_start_date" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="sub_start_date" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <span style="display: none;color: red;font-weight: bold;" id="error_start_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                        </div>

                                        <label id="l_end_date" class="col-sm-2 control-label" for="sub_end_date"><?php echo "End Date:"; ?></label>
                                        <div class="col-sm-3" id="h_sub_end_date">
                                            <div class="input-group date input-sub_end_date">
                                                <input type="text" name="sub_end_date" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="sub_end_date" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <span style="display: none;color: red;font-weight: bold;" id="error_end_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                            <span style="display: none;color: red;font-weight: bold;" id="error_greater_start_date" class="error"><?php echo 'End Date must not be less than Start Date!'; ?></span>
                                        </div>
                                        <input type="hidden" name="hidden_end_date" value="" id="hidden_end_date" class="form-control" />
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="enter">Enter:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="enters" type="hidden" name="enter" value="N" />
                                                    <input id="enter" type="checkbox" name="enter" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="declare">Declare:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="declares" type="hidden" name="declare" value="N" />
                                                    <input id="declare" type="checkbox" name="declare" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="scratch">Scratch:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="scratchs" type="hidden" name="scratch" value="N" />
                                                    <input id="scratch" type="checkbox" name="scratch" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="pay_money">Pay money:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="pay_moneys" type="hidden" name="pay_money" value="N" />
                                                    <input id="pay_money" type="checkbox" name="pay_money" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="receive_money">Receive money:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="receive_moneys" type="hidden" name="receive_money" value="N" />
                                                    <input id="receive_money" type="checkbox" name="receive_money" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-3 control-label" for="sub_buy">Buy:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="sub_buys" type="hidden" name="sub_buy" value="N" />
                                                    <input id="sub_buy" type="checkbox" name="sub_buy" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="sub_sell">Sell:</label>
                                        <div class="col-sm-3">
                                            <div class="checkbox" >
                                                <label>
                                                    <input id="sub_sells" type="hidden" name="sub_sell" value="N" />
                                                    <input id="sub_sell" type="checkbox" name="sub_sell" value="Y" checked="checked"; class="form-control" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        
                                        <label class="col-sm-3 control-label" for="sub_remarks"><b style="color: red"></b>Remarks</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="sub_remarks" value="" placeholder="Remarks" id="sub_remarks" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-3">
                                        </div>
                                        <div style="">
                                            <button type="button" class="btn btn-primary" id ="sub_ownersave" onclick="SubSaveAuthorityFunction();" style="margin-left: 230px;">Save</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left: 64px;">Close</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
                </div>
            </div>
            

        </div>
    </div>
    <script type="text/javascript">
        $('.date').datetimepicker({
            pickTime: false
        });
    </script>

    <script type="text/javascript"><!--
        $('input[name=\'path\']').autocomplete({
            'source': function(request, response) {
                $.ajax({
                url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    json.unshift({
                        category_id: 0,
                        name: '<?php echo $text_none; ?>'
                     });

                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['category_id']
                            }
                        }));
                    }
                });
            },
            'select': function(item) {
                $('input[name=\'path\']').val(item['label']);
                $('input[name=\'parent_id\']').val(item['value']);
            }
        });
    </script>

    <script type="text/javascript"><!--
        $('input[name=\'filter\']').autocomplete({
            'source': function(request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item['name'],
                                value: item['filter_id']
                            }
                        }));
                    }
                });
            },
            'select': function(item) {
                $('input[name=\'filter\']').val('');

                $('#category-filter' + item['value']).remove();

                $('#category-filter').append('<div id="category-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="category_filter[]" value="' + item['value'] + '" /></div>');
            }
        });

        $('#category-filter').delegate('.fa-minus-circle', 'click', function() {
            $(this).parent().remove();
        });
    </script>
    
    <script type="text/javascript">
        var itrUpload = $('#upload_hidden_id').val();
        function ITR_Upload() {
            html  = '';
            html += '<tr id="recurring-row' + itrUpload + '">';
                /*html += '  <td class="left">';
                    html += '<label>'+itrUpload+'</label>';
                html += '  </td>';*/
                html += '  <td class="left">';
                    html += '  <input type="text" name="itrdatas[' + itrUpload + '][year]" value="" placeholder="Assesment Year" class="form-control" />';
                html += '  </td>';
                html += '  <td class="left">';
                    html += '  <input type="text" readonly id="image_' + itrUpload + '"  name="itrdatas[' + itrUpload + '][image]" value="" placeholder="choose file" class="form-control" />';
                    html += '  <input type="hidden" id="image_path_' + itrUpload + '" name="itrdatas[' + itrUpload + '][imagepath]" value=""  class="form-control" />';
                    html += '<button type="button" class="button-upload btn btn-default" style="margin-top: 5px;" id="button-upload_'+ itrUpload + '" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>';
                     html += '<span id="button-other_document_'+itrUpload+'"></span>';
                html += '  </td>';
                html += '  <td class="left">';
                    html += '    <a onclick="$(\'#recurring-row' + itrUpload + '\').remove()" data-toggle="tooltip" title="<?php echo "BTN Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
                html += '  </td>';
            html += '</tr>';

            $('#itrUpload tbody').append(html);
             itrUpload++;
        }
    </script>

    <script type="text/javascript">
        $(document).delegate('.button-upload', 'click', function() {
            idss = $(this).attr('id');
            s_id = idss.split('_');
            id = s_id[1];
            $('#form_upload_itr').remove();
            $('body').prepend('<form enctype="multipart/form-data" id="form_upload_itr" style="display: none;"><input type="file" name="file" /></form>');
            $('#form_upload_itr input[name=\'file\']').trigger('click');
            if (typeof timer != 'undefined') {
                    clearInterval(timer);
            }
            timer = setInterval(function() {
                if ($('#form_upload_itr input[name=\'file\']').val() != '') {
                    clearInterval(timer);   
                    id = id;
                    $.ajax({
                        url: 'index.php?route=catalog/trainer/upload&token=<?php echo $token; ?>'+'&id='+id,
                        type: 'post',   
                        dataType: 'json',
                        data: new FormData($('#form_upload_itr')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,   
                        beforeSend: function() {
                            $('#button-upload').button('loading');
                        },
                        complete: function() {
                            $('#button-upload').button('reset');
                        },  
                        success: function(json) {
                            if (json['error']) {
                                alert(json['error']);
                            }
                            if (json['success']) {
                                alert(json['success']);
                                console.log(json);
                                $('#image_'+json['id']).attr('value', json['filename']);
                                $('#image_path_'+json['id']).attr('value', json['filepath']);
                                 var previewHtml = '<a target="_blank" class = "btn btn-default" style="cursor: pointer;margin-left:5px;" id = "btn_view_'+json['id']+'" href="'+json['filepath']+'">View Document</a>';
                                $('#btn_view_'+json['id']).remove();
                                $('#button-other_document_'+json['id']).append(previewHtml);
                            }
                        },      
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            }, 500);
        });

    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                //SaveOwnershp();
            }
        });
        $( document ).ready(function() {
             $('#input-trainer_name').focus();
        });
        //trainer tab 
        $("#date_of_birth").keyup(function()  {
            if(this.value.length == 2 && parseInt($(this).val()) <= 31 ){
                console.log($(this).val());
                console.log(this.value.length);
                $('#error_date_of_birth').hide();
                $("#month_date_of_birth" ).focus();
                $('#month_date_of_birth').select();
            } else {
                $('#error_date_of_birth').show();
            }
        });
        $("#date_of_license_renewal").keyup(function()  {
            if(this.value.length == 2 && parseInt($(this).val()) <= 31 ){
                $('#error_date_of_license_renewal').hide();
                $( "#month_date_of_license_renewal" ).focus();
                $('#month_date_of_license_renewal').select();
            } else {
                $('#error_date_of_license_renewal').show();
            }
        });

        $(document).on('keydown', '.form-control', '.custom-control-input', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            console.log(name);
            console.log(class_name);
            console.log(id);
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'input-trainer_name'){
                    $('#input-trainer_code').focus();
                }
                if(id == 'input-trainer_code'){
                    $('#input-racing_name').focus();
                }
                if(id == 'input-racing_name'){
                    $('#date_of_birth').focus();
                }
                if(id == 'date_of_birth'){
                    $('#license_type').focus();
                }
                if(id == 'license_type'){
                    $('#is_wita').focus();
                }
                /*if(id == 'center_holding_license_type'){
                    $('#is_wita').focus();
                }*/
                if(id == 'is_wita'){
                    $('#date_of_license_issue').focus();
                }
                if(id == 'date_of_license_issue'){
                    $('#date_of_license_renewal').focus();
                }
                if(id == 'date_of_license_renewal'){
                    $('#input-private_trainer').focus();
                }
                if(id == 'input-private_trainer'){
                    $('#input-fees_paid_type').focus();
                }
                if(id == 'input-fees_paid_type'){
                    $('#file_number').focus();
                }
                if(id == 'file_number'){
                    $('#assistant_trainer').focus();
                }
                if(id == 'assistant_trainer'){
                    $('#assistant_trainer_one').focus();
                }
                if(id == 'assistant_trainer_one'){
                    $('#remarks').focus();
                }
            }
        });

      
        $( "#license_type" ).change(function(){
            var license_type_show =  $(this).val()
            if(license_type_show == 'B'){
                $('.center_holding_license_type').show();
            } else {
                $('.center_holding_license_type').hide();
            }
        });

         $( "#input-private_trainer" ).change(function(){
            var private_trainer_type =  $(this).val()
            if(private_trainer_type == 'Y'){
                $('.add-owner-div').show();
            } else {
                $('.add-owner-div').hide();
            }
        });

        $( document ).ready(function() {
             var license_type_show = $("#license_type").val()
            if(license_type_show == 'B'){
                $('.center_holding_license_type').show();
            } else {
                $('.center_holding_license_type').hide();
            }


             var assistant_trainer =  $('#assistant_trainer').val()
            //alert(assistant_trainer);
           if(assistant_trainer == '1'){
                if(assistant_trainer == '1'){
                    $('#assistant_trainer_one_label').show();
                    $('.first_atnr1').show();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide()

                } else {
                    $('#assistant_trainer_one_label').hide();
                    $('.first_atnr1').hide();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide()
                }
            } else if(assistant_trainer == '2'){

                if(assistant_trainer == '2'){
                    $('#assistant_trainer_one_label').show();
                    $('.first_atnr1').show();
                    $('#assistant_trainer_two_label').show();
                    $('.second_atnr1').show();
                    $('.assistant_trainer_two').show();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();


                } else {
                    $('#assistant_trainer_one_label').hide();
                    $('.first_atnr1').hide();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();
                }
            } else if(assistant_trainer == '3'){

                if(assistant_trainer == '3'){
                    $('#assistant_trainer_one_label').show();
                    $('.first_atnr1').show();
                    $('#assistant_trainer_two_label').show();
                    $('.second_atnr1').show();
                    $('.assistant_trainer_two').show();
                    $('#assistant_trainer_three_label').show();
                    $('.assistant_trainer_three').show();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();
                } else {
                    $('#assistant_trainer_one_label').hide();
                    $('.first_atnr1').hide();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();
                }
            } else if(assistant_trainer == '4'){
                if(assistant_trainer == '4'){
                    $('#assistant_trainer_one_label').show();
                    $('.first_atnr1').show();
                    $('#assistant_trainer_two_label').show();
                    $('.second_atnr1').show();
                    $('.assistant_trainer_two').show();
                    $('#assistant_trainer_three_label').show();
                    $('.assistant_trainer_three').show();
                    $('.third_atnr1').show();
                    $('#assistant_trainer_four_label').show();
                    $('.assistant_trainer_four').show();
                     $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();
                } else {
                    $('#assistant_trainer_one_label').hide();
                    $('.first_atnr1').hide();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();
                }
            } else if(assistant_trainer == '5'){
                if(assistant_trainer == '5'){
                    $('#assistant_trainer_one_label').show();
                    $('.first_atnr1').show();
                    $('#assistant_trainer_two_label').show();
                    $('.second_atnr1').show();
                    $('.assistant_trainer_two').show();
                    $('#assistant_trainer_three_label').show();
                    $('.assistant_trainer_three').show();
                    $('.third_atnr1').show();
                    $('#assistant_trainer_four_label').show();
                    $('.assistant_trainer_four').show();
                    $('#assistant_trainer_five_label').show();
                    $('.assistant_trainer_five').show();
                } else {
                    $('#assistant_trainer_one_label').hide();
                    $('.first_atnr1').hide();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide()
                }
            }
       });

        $( "#assistant_trainer" ).change(function(){
            var assistant_trainer =  $(this).val()
            //alert(assistant_trainer);
           if(assistant_trainer == '1'){
                if(assistant_trainer == '1'){
                    $('#assistant_trainer_one_label').show();
                    $('.first_atnr1').show();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide()

                } else {
                    $('#assistant_trainer_one_label').hide();
                    $('.first_atnr1').hide();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide()
                }
            } else if(assistant_trainer == '2'){

                if(assistant_trainer == '2'){
                    $('#assistant_trainer_one_label').show();
                    $('.first_atnr1').show();
                    $('#assistant_trainer_two_label').show();
                    $('.second_atnr1').show();
                    $('.assistant_trainer_two').show();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();


                } else {
                    $('#assistant_trainer_one_label').hide();
                    $('.first_atnr1').hide();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();
                }
            } else if(assistant_trainer == '3'){

                if(assistant_trainer == '3'){
                    $('#assistant_trainer_one_label').show();
                    $('.first_atnr1').show();
                    $('#assistant_trainer_two_label').show();
                    $('.second_atnr1').show();
                    $('.assistant_trainer_two').show();
                    $('#assistant_trainer_three_label').show();
                    $('.assistant_trainer_three').show();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();
                } else {
                    $('#assistant_trainer_one_label').hide();
                    $('.first_atnr1').hide();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();
                }
            } else if(assistant_trainer == '4'){
                if(assistant_trainer == '4'){
                    $('#assistant_trainer_one_label').show();
                    $('.first_atnr1').show();
                    $('#assistant_trainer_two_label').show();
                    $('.second_atnr1').show();
                    $('.assistant_trainer_two').show();
                    $('#assistant_trainer_three_label').show();
                    $('.assistant_trainer_three').show();
                    $('.third_atnr1').show();
                    $('#assistant_trainer_four_label').show();
                    $('.assistant_trainer_four').show();
                     $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();
                } else {
                    $('#assistant_trainer_one_label').hide();
                    $('.first_atnr1').hide();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide();
                }
            } else if(assistant_trainer == '5'){
                if(assistant_trainer == '5'){
                    $('#assistant_trainer_one_label').show();
                    $('.first_atnr1').show();
                    $('#assistant_trainer_two_label').show();
                    $('.second_atnr1').show();
                    $('.assistant_trainer_two').show();
                    $('#assistant_trainer_three_label').show();
                    $('.assistant_trainer_three').show();
                    $('.third_atnr1').show();
                    $('#assistant_trainer_four_label').show();
                    $('.assistant_trainer_four').show();
                    $('#assistant_trainer_five_label').show();
                    $('.assistant_trainer_five').show();
                } else {
                    $('#assistant_trainer_one_label').hide();
                    $('.first_atnr1').hide();
                    $('#assistant_trainer_two_label').hide();
                    $('.second_atnr1').hide();
                    $('.assistant_trainer_two').hide();
                    $('#assistant_trainer_three_label').hide();
                    $('.assistant_trainer_three').hide();
                    $('.third_atnr1').hide();
                    $('#assistant_trainer_four_label').hide();
                    $('.assistant_trainer_four').hide();
                    $('#assistant_trainer_five_label').hide();
                    $('.assistant_trainer_five').hide()
                }
            }
        });



        
    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
        $(document).on('keydown', '.form-control', '.radio-inline' , function(e) {
        var name = $(this).attr('name'); 
        var class_name = $(this).attr('class'); 
        var id = $(this).attr('id');
        console.log(name);
        console.log(class_name);
        console.log(id);
        var value = $(this).val();
        if(e.which == 13){
            if(id == 'phone_no'){
                $('#mobile_no1').focus();
            }
            if(id == 'mobile_no1'){
                $('#alternate_mob_no').focus();
            }
            if(id == 'alternate_mob_no'){
                $('#email_id').focus();
            }
            if(id == 'email_id'){
                $('#alternate_email_id').focus();
            }
            if(id == 'alternate_email_id'){
                $('#address1').focus();
            }
            if(id == 'address1'){
                $('#localArea1').focus();
            }
            if(id == 'address2'){
                $('#localArea2').focus();
            }
            if(id == 'localArea1'){
                $('#state_1').focus();
            }
            if(id == 'localArea2'){
                $('#state_2').focus();
            }
            if(id == 'state_1'){
                $('#city_1').focus();
            }
            if(id == 'city_1'){
                $('#pincode_1').focus();
            }
            if(id == 'state_2'){
                $('#city_2').focus();
            }
            if(id == 'city_2'){
                $('#pincode_2').focus();
            }
            if(id == 'pincode_1'){
                $('#country_1').focus();
            }
            if(id == 'country_1'){
                $('#address2').focus();
            }
            if(id == 'pincode_2'){
                $('#country_2').focus();
            }
            if(id == 'country_2'){
                $('#gst_type').focus();
            }
            if(id == 'gst_type'){
                $('#gst_no').focus();
            }
            if(id == 'gst_no'){
                $('#pan_no').focus();
            }
            if(id == 'pan_no'){
                $('#prof_tax_no').focus();
            }
            if(id == 'prof_tax_no'){
                $('#phone_no').focus();
            }
        }
    });
    </script>

    <script type="text/javascript">

        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
        $( document ).ready(function() {
            $('#owner_name').focus();
        });
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            console.log(name);
            console.log(class_name);
            console.log(id);
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'owner_name'){
                    $('#start_date').focus();
                }
                if(id == 'start_date'){
                    $('#end_date').focus();
                }
                if(id == 'end_date'){
                    $('#monthly_maint_cost').focus();
                }
                if(id == 'monthly_maint_cost'){
                    $('#Commission_to_tm_and_jockey').focus();
                }
                if(id == 'Commission_to_tm_and_jockey'){
                    $('#syces').focus();
                }
                if(id == 'syces'){
                    $('#Payment_other_clubs').focus();
                }
                if(id == 'Payment_other_clubs'){
                    $('#water_charges').focus();
                }
                if(id == 'water_charges'){
                    $('#stable_rent').focus();
                }
                if(id == 'stable_rent'){
                    $('#extra_oats').focus();
                }
                if(id == 'extra_oats'){
                    $('#private_doctors_bills').focus();
                }
                if(id == 'private_doctors_bills'){
                    $('#eia_test_charges').focus();
                }
                if(id == 'eia_test_charges'){
                    $('#extras').focus();
                }
                if(id == 'extras'){
                    $('#electricitty_charges').focus();
                }
                if(id == 'electricitty_charges'){
                    $('#buy').focus();
                }
                if(id == 'buy'){
                    $('#sell').focus();
                }
                if(id == 'sell'){
                    $('#any_other_payment').focus();
                }
                if(id == 'any_other_payment'){
                    $('#appoint_sub_agent').focus();
                }
                if(id == 'appoint_sub_agent'){
                    $('#ownersave').focus();
                }
            }
        });
    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
        $( document ).ready(function() {
             $('#club_name').focus();
        });
        $(document).on('keydown', '.form-control', '.radio-inline' , function(e) {
        var name = $(this).attr('name'); 
        var class_name = $(this).attr('class'); 
        var id = $(this).attr('id');
        console.log(name);
        console.log(class_name);
        console.log(id);
        var value = $(this).val();
        if(e.which == 13){
            if(id == 'club_name'){
                $('#day_start_date1').focus();
            }
            if(id == 'day_start_date1'){
                $('#month_start_date1').focus();
            }
            if(id == 'month_start_date1'){
                $('#year_start_date1').focus();
            }
            if(id == 'year_start_date1'){
                $('#day_end_date1').focus();
            }
            if(id == 'day_end_date1'){
                $('#month_end_date1').focus();
            }
            if(id == 'month_end_date1'){
                $('#year_end_date1').focus();
            }
            if(id == 'year_end_date1'){
                $('#day_start_date2').focus();
            }
            if(id == 'day_start_date2'){
                $('#month_start_date2').focus();
            }
            if(id == 'month_start_date2'){
                $('#year_start_date2').focus();
            }
            if(id == 'year_start_date2'){
                $('#day_end_date2').focus();
            }
            if(id == 'day_end_date2'){
                $('#month_end_date2').focus();
            }
            if(id == 'month_end_date2'){
                $('#year_end_date2').focus();
            }
            if(id == 'year_end_date2'){
                $('#amount').focus();
            }
            if(id == 'amount'){
                $('#authority').focus();
            }
            if(id == 'authority'){
                $('#reason').focus();
            }
            if(id == 'reason'){
                $('#ban_save_id').focus();
            }
        }
    });
    </script>

    <script type="text/javascript">
    	function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }
        function AuthorityFunction(auth_id,hidden_tr_id){
            $('#error_greater_start_date').hide();
            $('#error_owner_name').hide();
            $('#error_start_date').hide();
            $('#error_end_date').hide();
            $.ajax({
                type: "POST",
                url: 'index.php?route=catalog/trainer/edit1&token=<?php echo $token; ?>&auth_id_name=' + auth_id,
                dataType: 'json',
                success: function(json) { 
                	console.log(json);
                    $('#hidden_owner-save').val(hidden_tr_id);
                    var myidsss =   $('#hidden_owner-save').val();
                    $('#owner_name').val(json.check_name);
                    $('#auth_id').val(json.autho_id);
                    $('#hidden_owner_id').val(json.owner_id);
                    var s_date=json.start_date;
                    $('#start_date').val(s_date);
                    var e_date=json.end_date;
                    $('#end_date').val(e_date);

                    if (json.is_indefinite == 'N'){
                        $("#is_indefinite").prop("checked", false);
                    }else {
                        $('#is_indefinite').val(json.is_indefinite);
                    }

                    if (json.is_monthly_maint_cost == 'N'){
                        $("#monthly_maint_cost").prop("checked", false);
                    }else {
                        $('#monthly_maint_cost').val(json.is_monthly_maint_cost);
                    }
                    if (json.is_commission_to_trn_jock == 'N'){
                        $("#Commission_to_tm_and_jockey").prop("checked", false);
                    }else {
                        $('#Commission_to_tm_and_jockey').val(json.is_commission_to_trn_jock);
                    }
                    if (json.is_synces_bonus == 'N'){
                        $("#syces").prop("checked", false);
                    }else {
                        $('#syces').val(json.is_synces_bonus);
                    }
                    if (json.is_pay_othr_club == 'N'){
                        $("#Payment_other_clubs").prop("checked", false);
                    }else {
                        $('#Payment_other_clubs').val(json.is_pay_othr_club);
                    }
                    if (json.is_waiter_charge == 'N'){
                        $("#water_charges").prop("checked", false);
                    }else {
                        $('#water_charges').val(json.is_waiter_charge);
                    }
                    if (json.is_stable_rent == 'N'){
                        $("#stable_rent").prop("checked", false);
                    }else {
                        $('#stable_rent').val(json.is_stable_rent);
                    }
                    if (json.is_extra_oats == 'N'){
                        $("#extra_oats").prop("checked", false);
                    }else {
                        $('#extra_oats').val(json.is_extra_oats);
                    }
                    if (json.is_priv_doc_vet_bills == 'N'){
                        $("#private_doctors_bills").prop("checked", false);
                    }else {
                        $('#private_doctors_bills').val(json.is_priv_doc_vet_bills);
                    }
                    if (json.is_eia_test_charge == 'N'){
                        $("#eia_test_charges").prop("checked", false);
                    }else {
                        $('#eia_test_charges').val(json.is_eia_test_charge);
                    }
                    if (json.is_extras == 'N'){
                        $("#extras").prop("checked", false);
                    }else {
                        $('#extras').val(json.is_extras);
                    }
                    if (json.is_electricity_charge == 'N'){
                        $("#electricitty_charges").prop("checked", false);
                    }else {
                        $('#electricitty_charges').val(json.is_electricity_charge);
                    }
                    if (json.is_buy == 'N'){
                        $("#buy").prop("checked", false);
                    }else {
                        $('#buy').val(json.is_buy);
                    }
                    if (json.is_sell == 'N'){
                        $("#sell").prop("checked", false);
                    }else {
                        $('#sell').val(json.is_sell);
                    }
                    
                    $('#remarkss').val(json.remarks);
                    $('#any_other_payment').val(json.is_any_othr_payment);
                    $('#appoint_sub_agent').val(json.is_sub_authority);
                },
                error: function(){
                    alert("error");
                }
            });
            event.preventDefault();
        }
    </script>


    <script type="text/javascript">
        
        function SubAuthorityFunction(sub_autho_id,sub_hidden_tr_id){
            $('#error_greater_start_date').hide();
            $('#error_owner_name').hide();
            $('#error_start_date').hide();
            $('#error_end_date').hide();
            $.ajax({
                type: "POST",
                url: 'index.php?route=catalog/trainer/edit2&token=<?php echo $token; ?>&auth_id_name=' + sub_autho_id,
                dataType: 'json',
                success: function(json) { 
                    console.log(json);
                    $('#Sub_hidden_owner_save').val('');
                    $('#Sub_hidden_owner_save').val(sub_hidden_tr_id);
                    
                    $('#sub_owner_name').val(json.owner_name);
                    $('#sub_trainer_name').val(json.trainer_name);
                    $('#sub_autho_id').val(json.sub_autho_id);
                    $('#sub_hidden_owner_id').val(json.owner_id);
                    var s_date=json.start_date;
                    $('#sub_start_date').val(s_date);
                    var e_date=json.end_date;
                    $('#sub_end_date').val(e_date);

                    // if (json.is_indefinite == 'N'){
                    //     $("#is_indefinite").prop("checked", false);
                    // }else {
                    //     $('#is_indefinite').val(json.is_indefinite);
                    // }

                    if (json.enter == 'N'){
                        $("#enter").prop("checked", false);
                    }else {
                        $('#enter').val(json.enter);
                    }
                    if (json.declares == 'N'){
                        $("#declare").prop("checked", false);
                    }else {
                        $('#declare').val(json.declares);
                    }
                    if (json.scratch == 'N'){
                        $("#scratch").prop("checked", false);
                    }else {
                        $('#scratch').val(json.scratch);
                    }
                    if (json.pay_money == 'N'){
                        $("#pay_money").prop("checked", false);
                    }else {
                        $('#pay_money').val(json.pay_money);
                    }
                    if (json.receive_money == 'N'){
                        $("#receive_money").prop("checked", false);
                    }else {
                        $('#receive_money').val(json.receive_money);
                    }
                    if (json.sub_buy == 'N'){
                        $("#sub_buy").prop("checked", false);
                    }else {
                        $('#sub_buy').val(json.sub_buy);
                    }
                    if (json.sub_sell == 'N'){
                        $("#sub_sell").prop("checked", false);
                    }else {
                        $('#sub_sell').val(json.sub_sell);
                    }
                    
                    
                    $('#sub_remarks').val(json.sub_remarks);
                    
                },
                error: function(){
                    alert("error");
                }
            });
            event.preventDefault();
        }
    </script>

    <script type="text/javascript">
        function removetd(myid){
            $('#par'+myid+'').closest("tr").remove();
        }
        function BlankData(){
            $('#error_greater_start_date').hide();
            $('#error_owner_name').hide();
            $('#error_start_date').hide();
            $('#error_end_date').hide();
            $('#auth_id').val('');
            $('#owner_name').val('');
            $('#hidden_owner_id').val('');
            $('#start_date').val('');
            $('#end_date').val('');
            $('#monthly_maint_cost').val('Y');
            $('#Commission_to_tm_and_jockey').val('Y');
            $('#syces').val('Y');
            $('#Payment_other_clubs').val('Y');
            $('#water_charges').val('Y');
            $('#stable_rent').val('Y');
            $('#extra_oats').val('Y');
            $('#private_doctors_bills').val('Y');
            $('#eia_test_charges').val('Y');
            $('#extras').val('Y');
            $('#electricitty_charges').val('Y');
            $('#buy').val('Y');
            $('#sell').val('Y');
            $('#any_other_payment').val('Y');
            $('#appoint_sub_agent').val('Y');
        }

        function removetdd(myidd){
            console.log("myidd");
            console.log(myidd);

            $('#para'+myidd+'').closest("tr").remove();
        }
        function SubBlankData(){
            
            $('#error_greater_start_date').hide();
            $('#error_owner_name').hide();
            $('#error_start_date').hide();
            $('#error_end_date').hide();
            $('#sub_autho_id').val('');
            $('#sub_start_date').val('');
            $('#sub_end_date').val('');
            $('#sub_owner_name').val('');
            $('#sub_trainer_name').val('');
            $('#sub_hidden_owner_id').val('');
            $('#hidden_trainer_id').val('');
            $('#enter').val('Y');
            $('#declare').val('Y');
            $('#scratch').val('Y');
            $('#pay_money').val('Y');
            $('#receive_money').val('Y');
            $('#sub_buy').val('Y');
            $('#sub_sell').val('Y');
            $('#sub_remarks').val('');
        }
    </script>

    <script type="text/javascript">
        function SaveAuthorityFunction(){
            var owner_rqrd = $('#owner_name').val();
            var start_date_rqrd = $('#start_date').val();
            var end_date_rqrd = $('#end_date').val();
            if (end_date_rqrd != '') {

                var string_start_dates = start_date_rqrd.split('-');
                var s_dates  = string_start_dates[0];
                var s_months = string_start_dates[1];
                var s_years  = string_start_dates[2];

                var string_end_dates = end_date_rqrd.split('-');
                var e_dates  = string_end_dates[0];
                var e_months = string_end_dates[1];
                var e_years  = string_end_dates[2];

                if (s_years > e_years){
                    $('#error_greater_start_date_bans').show();
                    return false;
                } else if (e_months > s_months) {
                    if (s_dates > e_dates || e_dates > s_dates) {
                        $('#error_greater_start_date_bans').hide();
                    } 
                } else if(s_months > e_months) {
                    $('#error_greater_start_date_bans').show();
                    return false;
                } else if(s_months == e_months) {
                    if (s_dates > e_dates) {
                        $('#error_greater_start_date_bans').show();
                        return false;
                    }
                }
            }
            if(owner_rqrd != ''  ){
                $('#error_owner_name').hide();
            } else {
                $('#error_owner_name').show();
            }
            if(start_date_rqrd != '' ){
                $('#error_start_date').hide();
            } else {
                $('#error_start_date').show();
            }
            if(end_date_rqrd != '' ){
                $('#error_end_date').hide();
            } else {
                $('#error_end_date').show();
            }
            if(owner_rqrd == '' || start_date_rqrd == '' || end_date_rqrd == '') {
                return false;
            }else{
                var myid = $('#hidden_owner-save').val();
                var data =  $('#form-options').serialize();
                $.ajax({
                    method: "POST",
                    url:'index.php?route=catalog/trainer/tab_owner_authority_to_trainer_save&token=<?php echo $token; ?>',
                    data: data,
                    dataType: "json",
                    success: function (json1) {
                        console.log(json1);
                        if(json1.json.start_date == '01-01-1970'){
                            var start_date_final =  '';
                        } else {
                             var start_date_final =  json1.json.start_date;
                        }
                        if(json1.json.end_date == '01-01-1970'){
                            var end_date_final =  '';
                        } else {
                             var end_date_final =  json1.json.end_date;
                        }
                        html = '<tr id="par'+myid+'">';
                            html += '<td class="text-left"  >'+json1.json.owner_name+'';
                            html += '</td>';
                        
                            html += '<td class="text-right"  >'+start_date_final+'';
                            html += '</td>';
                        
                            html += '<td class="text-right"  >'+end_date_final+'';
                            html += '</td>';
                            html += '<td class="text-center"  >';
                                html +='<input type="hidden" id="hidden_new_id" value='+json1.new_id+'>';
                                html += '<button type="button" class="btn btn-primary" id="authoritybtn" data-toggle="modal" data-target="#myModal"  onclick="AuthorityFunction('+json1.new_id+','+myid+');"><i class="fa fa-pencil"></i></button>';
                            html += '</td>';

                        html += '</tr >';
                    $('#authrity-table').append(html);
                    myid++
                    $('#hidden_owner-save').val(myid);
                    }
                });
                $('#hidden_owner-save').val('');
                removetd(myid);
                $("#myModal").modal("toggle");
               
            }
        }
    </script>

    <script type="text/javascript">
        function SubSaveAuthorityFunction(){
            var owner_rqrd = $('#sub_owner_name').val();
            var start_date_rqrd = $('#sub_start_date').val();
            var end_date_rqrd = $('#sub_end_date').val();
            if (end_date_rqrd != '') {

                var string_start_dates = start_date_rqrd.split('-');
                var s_dates  = string_start_dates[0];
                var s_months = string_start_dates[1];
                var s_years  = string_start_dates[2];

                var string_end_dates = end_date_rqrd.split('-');
                var e_dates  = string_end_dates[0];
                var e_months = string_end_dates[1];
                var e_years  = string_end_dates[2];

                if (s_years > e_years){
                    $('#error_greater_start_date_bans').show();
                    return false;
                } else if (e_months > s_months) {
                    if (s_dates > e_dates || e_dates > s_dates) {
                        $('#error_greater_start_date_bans').hide();
                    } 
                } else if(s_months > e_months) {
                    $('#error_greater_start_date_bans').show();
                    return false;
                } else if(s_months == e_months) {
                    if (s_dates > e_dates) {
                        $('#error_greater_start_date_bans').show();
                        return false;
                    }
                }
            }
            if(owner_rqrd != ''  ){
                $('#error_owner_name').hide();
            } else {
                $('#error_owner_name').show();
            }
            if(start_date_rqrd != '' ){
                $('#error_start_date').hide();
            } else {
                $('#error_start_date').show();
            }
            if(end_date_rqrd != '' ){
                $('#error_end_date').hide();
            } else {
                $('#error_end_date').show();
            }
            if(owner_rqrd == '' || start_date_rqrd == '' || end_date_rqrd == '') {
                return false;
            } else {
                var myidd = $('#sub_hidden_owner_save').val();
                //console.log($('#sub_hidden_owner_save').length > 0);
                var data = $('#sub-form-options').serialize();
                //console.log(data.);
                $.ajax({
                    method: "POST",
                    url:'index.php?route=catalog/trainer/SubAuthority&token=<?php echo $token; ?>&data=' + data,
                    data: data,
                    dataType: "json",
                    success: function (json1) {
                        console.log("json1");
                        console.log(json1);
                        if(json1.json.start_date == '01-01-1970'){
                            var start_date_final =  '';
                        } else {
                             var start_date_final =  json1.json.sub_start_date;
                        }
                        if(json1.json.end_date == '01-01-1970'){
                            var end_date_final =  '';
                        } else {
                             var end_date_final =  json1.json.sub_end_date;
                        }
                        html = '<tr id="para'+myidd+'">';
                            html += '<td class="text-left"  >'+json1.json.sub_owner_name+'';
                            html += '</td>';
                            html += '<td class="text-right"  >'+start_date_final+'';
                            html += '</td>';
                        
                            html += '<td class="text-right"  >'+end_date_final+'';
                            html += '</td>';
                            html += '<td class="text-center"  >';
                                html +='<input type="hidden" id="sub_hidden_new_id" value='+json1.new_id+'>';
                                html += '<button type="button" class="btn btn-primary" id="subauthoritybtn" data-toggle="modal" data-target="#sub_myModal"  onclick="SubAuthorityFunction('+json1.new_id+','+myidd+');"><i class="fa fa-pencil"></i></button>';
                            html += '</td>';

                        html += '</tr >';
                        $('#sub_authrity-table').append(html);
                        myidd++
                        $('#sub_hidden_owner_save').val(json1.json.Sub_hidden_owner_save);
                        removetdd(json1.json.Sub_hidden_owner_save);
                        location.reload();
                        // setTimeout(abc, 3000);
                    }
                });
                //$('#sub_hidden_owner_save').val('');
                $("#sub_myModal").modal("toggle");
            }
        }

        // function abc() {
        //     $(window).load(function() {
        //         //$('#tab_owner_authority_to_trainer').tab('show');
        //         $(".tab-pane #tab_owner_authority_to_trainer").removeClass('active');
        //     });
        // }
    </script>

    <script type="text/javascript">
        $('#owner_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/trainer/autocompleteOwnername&token=<?php echo $token; ?>&owner_name=' +  encodeURIComponent(request),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.owner_name,
                                    value: item.owner_name,
                                    ownercode: item.owner_id
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(item) {
                 $('#hidden_owner_id').val(item.ownercode);
                $('#owner_name').val(item.value);
                $('#start_date').focus();
                return false;
            },
        });
    </script>

    <script type="text/javascript">
        $('#sub_owner_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/trainer/autocompleteOwnername&token=<?php echo $token; ?>&owner_name=' +  encodeURIComponent(request),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.owner_name,
                                    value: item.owner_name,
                                    ownercode: item.owner_id
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(item) {
                $('#sub_hidden_owner_id').val(item.ownercode);
                $('#sub_owner_name').val(item.value);
                $('#start_date').focus();
                return false;
            },
        });
    </script>

     <script type="text/javascript">
        $('#sub_trainer_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/trainer/autocompleteTrainername&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.trainer_name,
                                    value: item.trainer_name,
                                    trainercode: item.owner_id
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(item) {
                $('#sub_hidden_trainer_id').val(item.trainercode);
                $('#sub_trainer_name').val(item.value);
                $('#start_date').focus();
                return false;
            },
        });
    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                //SaveOwnershp();
            }
        });

        $('#date_start_date_ban').keyup(function(){
            var date_start_date_ban =  $('#date_start_date_ban').val();
            date_start_date_ban1 = date_start_date_ban.replace(/[^0-9-]+/i, '');
            $("#date_start_date_ban").val(date_start_date_ban1);
            var date_start_date_ban_again =  $('#date_start_date_ban').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(date_start_date_ban_again))) {
                $('#error_date_start_date_ban').show();
                return false;
            } else {
                $('#error_date_start_date_ban').hide();
            }
        });
        $('.input-date_start_date_ban').datetimepicker().on('dp.change', function (e) {  
            $('#error_date_start_date_ban').css('display','none');
        });

        $('#date_start_date_ban2').keyup(function(){
            var date_start_date_ban2 =  $('#date_start_date_ban2').val();
            date_start_date_ban21 = date_start_date_ban2.replace(/[^0-9-]+/i, '');
            $("#date_start_date_ban2").val(date_start_date_ban21);
            var date_start_date_ban2_again =  $('#date_start_date_ban2').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(date_start_date_ban2_again))) {
                $('#error_date_start_date_ban2').show();
                return false;
            } else {
                $('#error_date_start_date_ban2').hide();
            }
        });
        $('.input-date_start_date_ban2').datetimepicker().on('dp.change', function (e) {  
            $('#error_date_start_date_ban2').css('display','none');
        });

        $('#date_end_date_ban').keyup(function(){
            var date_end_date_ban =  $('#date_end_date_ban').val();
            date_end_date_ban1 = date_end_date_ban.replace(/[^0-9-]+/i, '');
            $("#date_end_date_ban").val(date_end_date_ban1);
            var date_end_date_ban_again =  $('#date_end_date_ban').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(date_end_date_ban_again))) {
                $('#error_date_end_date_ban').show();
                return false;
            } else {
                $('#error_date_end_date_ban').hide();
            }
        });
        $('.input-date_end_date_ban').datetimepicker().on('dp.change', function (e) {  
            $('#error_date_end_date_ban').css('display','none');
        });

        $('#date_end_date_ban2').keyup(function(){
            var date_end_date_ban2 =  $('#date_end_date_ban2').val();
            date_end_date_ban21 = date_end_date_ban2.replace(/[^0-9-]+/i, '');
            $("#date_end_date_ban2").val(date_end_date_ban21);
            var date_end_date_ban2_again =  $('#date_end_date_ban2').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(date_end_date_ban2_again))) {
                $('#error_date_end_date_ban2').show();
                return false;
            } else {
                $('#error_date_end_date_ban2').hide();
            }
        });
        $('.input-date_end_date_ban2').datetimepicker().on('dp.change', function (e) {  
            $('#error_date_end_date_ban2').css('display','none');
        });
        //Ban details tab
        function BanFunction(){
            var date_start_date_ban =  $( "#date_start_date_ban" ).val();
            var end_date_ban =  $( "#date_end_date_ban" ).val();
            var date_start_date_ban2 =  $( "#date_start_date_ban2" ).val();
            var end_date_ban2 =  $( "#date_end_date_ban2" ).val();
            var auto_id = $('#id_hidden_band').val();
            var id_hidden_BanFunction = $('#id_hidden_BanFunction').val();
            
            if (end_date_ban != '') {
                var string_start_date = date_start_date_ban.split('-');
                var s_date  = string_start_date[0];
                var s_month = string_start_date[1];
                var s_year  = string_start_date[2];

                var string_end_date = end_date_ban.split('-');
                var e_date  = string_end_date[0];
                var e_month = string_end_date[1];
                var e_year  = string_end_date[2];

                if (s_year > e_year){
                    $('#error_greater_start_date_ban').show();
                    return false;
                } else if (e_month > s_month) {
                    if (s_date > e_date || e_date > s_date) {
                        $('#error_greater_start_date_ban').hide();
                    } 
                } else if(s_month > e_month) {
                    $('#error_greater_start_date_ban').show();
                    return false;
                } else if(s_month == e_month) {
                    if (s_date > e_date) {
                        $('#error_greater_start_date_ban').show();
                        return false;
                    }
                }
            }

            if (end_date_ban2 != '') {
                var string_start_date2 = date_start_date_ban2.split('-');
                var s_date2  = string_start_date2[0];
                var s_month2 = string_start_date2[1];
                var s_year2  = string_start_date2[2];

                var string_end_date2 = end_date_ban2.split('-');
                var e_date2  = string_end_date2[0];
                var e_month2 = string_end_date2[1];
                var e_year2  = string_end_date2[2];

                if (s_year2 > e_year2){
                    $('#error_greater_start_date_ban2').show();
                    return false;
                } else if (e_month2 > s_month2) {
                    if (s_date2 > e_date2 || e_date2 > s_date2) {
                        $('#error_greater_start_date_ban2').hide();
                    } 
                } else if(s_month2 > e_month2) {
                    $('#error_greater_start_date_ban2').show();
                    return false;
                } else if(s_month2 == e_month2) {
                    if (s_date2 > e_date2) {
                        $('#error_greater_start_date_ban2').show();
                        return false;
                    }
                }
            }


            /*if(date_start_date_ban != ''){
                var date_start_date_bans = date_start_date_ban;
                $('#error_date_start_date_ban').hide();
            } else {
                $('#error_date_start_date_ban').show();
            }*/

            if( end_date_ban == ''){
                $('#error_date_end_date_ban').hide();
                var end_date_ban_ends = end_date_ban;
            } else {
                var end_date_ban_ends = end_date_ban;
            }

            if(date_start_date_ban2 == ''){
                $('#error_date_start_date_ban2').hide();
                var date_start_date_bans2 = date_start_date_ban2;
            } else {
                var date_start_date_bans2 = date_start_date_ban2;
            }
            
            if( end_date_ban2 == ''){
                $('#error_date_end_date_ban2').hide();
                var end_date_ban_ends2 = end_date_ban2;
            } else {
                var end_date_ban_ends2 = end_date_ban2;
            }

            var club = $('#input-club').val();
            if(club != null && club != ''){
                $('#error_club_ban').hide();
                clubs = club;
            } else {
                $('#error_club_ban').show();
            }
            var authority_ban = $('#authority_ban_details').val();
            if(authority_ban != '' && authority_ban !=  null){
                $('#error_authority_ban').hide();
                var authority_bans = authority_ban.replace(/\s\s+/g, ' ');
            } else {
                $('#error_authority_ban').show();
            }
            var reason_bans = $('#reason_ban').val();
            if(reason_bans != ''  ){
                $('#error_reason_ban').hide();
            } else {
                $('#error_reason_ban').show();
            }
            var amount_bans = $('#amount_ban').val();
            if(amount_bans != ''  ){
                $('#error_amount_ban').hide();
            } else {
                $('#error_amount_ban').show();
            }
            if (date_start_date_ban == ''){
                return false;
            } else {
                if(id_hidden_BanFunction == '0'){ 
                    var start_date_bans = date_start_date_ban; //getstart_dare
                    if(end_date_ban_ends == ''){
                        var end_date_bans = end_date_ban;
                    } else {
                        var end_date_bans = end_date_ban;
                    }

                    if(date_start_date_bans2 == ''){
                        var start_date_bans2 = date_start_date_ban2;  //getend days date_start_date_ban2 
                    } else {
                        var start_date_bans2 = date_start_date_ban2;
                    }

                    if(end_date_ban_ends2 == ''){
                        var end_date_bans2 = end_date_ban2;  //getend days
                    } else {
                        var end_date_bans2 = end_date_ban2;
                    }
                    html = '<tr id ="bandetail_'+auto_id+'">';
                        html += '<td class="text-left">';
                            html += '<span id="clubs_'+auto_id+'">'+clubs+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][club]" id="club_'+auto_id+'" value = '+clubs+'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="start_dateban_'+auto_id+'">'+start_date_bans+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][start_date1]" id="start_date_bans_'+auto_id+'" value = '+start_date_bans+'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="end_dateban_'+auto_id+'">'+end_date_bans+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][end_date1]" id="end_date_ban_'+auto_id+'" value = '+end_date_bans+'>';
                        html += '</td>';

                        html += '<td class="text-right">';
                            html += '<span id="start_dateban2_'+auto_id+'">'+start_date_bans2+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][start_date2]" id="start_date_bans2_'+auto_id+'" value = '+start_date_bans2+'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="end_dateban2_'+auto_id+'">'+end_date_bans2+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][end_date2]" id="end_date_ban2_'+auto_id+'" value = '+end_date_bans2+'>';
                        html += '</td>';

                        html += '<td class="text-right">';
                            html += '<span id="authorityban_'+auto_id+'">'+authority_bans+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][authority]" id="authority_ban_'+auto_id+'" value = \'' + authority_bans + '\'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="reasonban_'+auto_id+'">'+reason_bans+ '</span>';
                            html += '<input type= "hidden" name= "bandats['+auto_id+'][reason]" id="reason_ban_'+auto_id+'" value = \'' + reason_bans + '\'>';
                           /* html += '<span id="amountban_'+auto_id+'">'+amount_bans+ '</span>';*/
                            html += '<input type= "hidden" name= "bandats['+auto_id+'][amount]" id="amount_ban_'+auto_id+'" value = \'' + amount_bans + '\'>'; 
                        html += '</td>';
                       
                        html += '<td class="text-center">';
                            html += '<a onclick=updateban("'+auto_id+'") class="btn btn-primary"><i class="fa fa-pencil"></i></a>&nbsp';
                            html += '<a onclick=removeBanDetail(0,0,"bandetail_'+auto_id+'") class="btn btn-danger"><i class="fa fa-minus-circle"></i></a></td>'
                        html += '</tr >';
                    auto_id++;
                    $('#bandeatilsbody').append(html);
                    $('#id_hidden_band').val(auto_id);
                } else {
                    var old_start_date_ban = date_start_date_ban; //getstart_dare
                    if(end_date_ban_ends != ''){
                        var old_end_date_ban = end_date_ban_ends;  //getend days
                    } else {
                        var old_end_date_ban = '';
                    }
                    if(date_start_date_ban2 != ''){
                        var old_start_date_ban2 = date_start_date_ban2;  //getend days
                    } else {
                        var old_start_date_ban2 = '';
                    }
                    if(end_date_ban_ends2 != ''){
                        var old_end_date_ban2 = end_date_ban_ends2;  //getend days
                    } else {
                        var old_end_date_ban2 = '';
                    }
                    var idban_increment_id = $('#idban_increment_id').val();
                    $('#start_date_bans_'+idban_increment_id+'').val(old_start_date_ban);
                    $('#end_date_ban_'+idban_increment_id+'').val(old_end_date_ban);
                    $('#start_date_bans2_'+idban_increment_id+'').val(old_start_date_ban2);
                    $('#end_date_ban2_'+idban_increment_id+'').val(old_end_date_ban2);
                    $('#reason_ban_'+idban_increment_id+'').val(reason_bans);
                    $('#amount_ban_'+idban_increment_id+'').val(amount_bans);
                    $('#authority_ban_'+idban_increment_id+'').val(authority_bans);
                    $('#club_'+idban_increment_id+'').val(clubs);
                    $('#start_dateban_'+idban_increment_id+'').html(old_start_date_ban);
                    $('#end_dateban_'+idban_increment_id+'').html(old_end_date_ban);
                    $('#start_dateban2_'+idban_increment_id+'').html(old_start_date_ban2);
                    $('#end_dateban2_'+idban_increment_id+'').html(old_end_date_ban2);
                    $('#reasonban_'+idban_increment_id+'').html(reason_bans);
                    $('#amountban_'+idban_increment_id+'').html(amount_bans);
                    $('#authorityban_'+idban_increment_id+'').html(authority_bans);
                    $('#clubs_'+idban_increment_id+'').html(clubs);
                }
            }
            $("#myModal2").modal("toggle");
        }
        //blanked value for Ban details Tab
        function closeaddbuu1(){
            $('#input-club').val('');
            $('#reason_ban').val('');
            $('#amount_ban').val('');
            $('#authority_ban_details').val('');
            $('#id_hidden_BanFunction').val(0);
            $("#date_start_date_ban" ).val('');
            $("#date_end_date_ban" ).val('');
            $('#error_greater_start_date_ban').hide();
            $('#error_greater_start_date_ban').hide();
            $('#error_club_ban').hide();
            $('#error_authority_ban').hide();
            $('#error_date_start_date_ban').hide();
            $('#error_date_end_date_ban').hide();
            $('#error_reason_ban').hide();
            $('#error_amount_ban').hide();
            $("#date_start_date_ban2" ).val('');
            $("#date_end_date_ban2" ).val('');
            $('#error_date_start_date_ban2').hide();
            $('#error_date_end_date_ban2').hide();
        }
        $('#myModal2').on('shown.bs.modal', function () {
            $('#input-club').focus();
        }); 

        $('#date_of_birth').keyup(function(){
            $('#valierr_date_of_birth').hide();
            var date_of_birth_valid =  $('#date_of_birth').val();
            date_of_birth_valid1 = date_of_birth_valid.replace(/[^0-9-]+/i, '');
            $("#date_of_birth").val(date_of_birth_valid1);
            var date_of_birth_valid_again =  $('#date_of_birth').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(date_of_birth_valid_again))) {
                $('#error_date_of_birth').show();
                return false;
            } else {
                $('#error_date_of_birth').hide();
            }
        });
        $('.input-date_of_birth').datetimepicker().on('dp.change', function (e) {  
            $('#error_date_of_birth').css('display','none');
        });

        $('#date_of_license_renewal').keyup(function(){
            $('#valierr_date_of_license_renewal').hide();
            var date_of_license_renewal_valid =  $('#date_of_license_renewal').val();
            date_of_license_renewal_valid1 = date_of_license_renewal_valid.replace(/[^0-9-]+/i, '');
            $("#date_of_license_renewal").val(date_of_license_renewal_valid1);
            var date_of_license_renewal_valid_again =  $('#date_of_license_renewal').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(date_of_license_renewal_valid_again))) {
                $('#error_license_renewal_date').show();
                return false;
            } else {
                $('#error_license_renewal_date').hide();
            }
        });
        $('.input-date_of_license_renewal').datetimepicker().on('dp.change', function (e) {  
            $('#error_license_renewal_date').css('display','none');
        });

        $('#date_of_license_issue').keyup(function(){
            $('#valierr_license_issue_date').hide();
            var date_of_license_issue_valid =  $('#date_of_license_issue').val();
            date_of_license_issue_valid1 = date_of_license_issue_valid.replace(/[^0-9-]+/i, '');
            $("#date_of_license_issue").val(date_of_license_issue_valid1);
            var date_of_license_issue_valid_again =  $('#date_of_license_issue').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(date_of_license_issue_valid_again))) {
                $('#error_license_issue_date').show();
                return false;
            } else {
                $('#error_license_issue_date').hide();
            }
        });
        $('.input-date_of_license_issue').datetimepicker().on('dp.change', function (e) {  
            $('#error_license_issue_date').css('display','none');
        });

        $('#start_date').keyup(function(){
            var start_date =  $('#start_date').val();
            start_date1 = start_date.replace(/[^0-9-]+/i, '');
            $("#start_date").val(start_date1);
            var start_date_again =  $('#start_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(start_date_again))) {
                $('#error_start_date').show();
                return false;
            } else {
                $('#error_start_date').hide();
            }
        });
        $('.input-start_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_start_date').css('display','none');
        });

        $('#end_date').keyup(function(){
            var end_date =  $('#end_date').val();
            end_date1 = end_date.replace(/[^0-9-]+/i, '');
            $("#end_date").val(end_date1);
            var end_date_again =  $('#end_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(end_date_again))) {
                $('#error_end_date').show();
                return false;
            } else {
                $('#error_end_date').hide();
            }
        });
        $('.input-end_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_end_date').css('display','none');
        });


        //update function ban details tab
        function updateban(auto_id){
            $('#error_greater_start_date_ban').hide();
            $('#error_greater_start_date_ban2').hide();
            $('#error_club_ban').hide();
            $('#error_date_start_date_ban').hide();
            $('#error_date_end_date_ban').hide();
            $('#error_date_start_date_ban2').hide();
            $('#error_date_end_date_ban2').hide();
            $('#error_amount_ban').hide();
            $('#error_authority_ban').hide();
            $('#error_reason_ban').hide();
            $('#myModal2').modal('show');
            $('#idban_increment_id').val(auto_id);
            $('#id_hidden_BanFunction').val(1);
            var start_date_ban = $('#start_date_bans_'+auto_id+'').val();
            var end_date_ban = $('#end_date_ban_'+auto_id+'').val();
            var start_date_ban2 = $('#start_date_bans2_'+auto_id+'').val();
            var end_date_ban2 = $('#end_date_ban2_'+auto_id+'').val();
            var reason_ban = $('#reason_ban_'+auto_id+'').val();
            var amount_ban = $('#amount_ban_'+auto_id+'').val();
            var authority_ban = $('#authority_ban_'+auto_id+'').val();
            var club_ban = $('#club_'+auto_id+'').val();
            $("#date_start_date_ban").val(start_date_ban);
            $( "#date_end_date_ban" ).val(end_date_ban);
            $("#date_start_date_ban2").val(start_date_ban2);
            $( "#date_end_date_ban2" ).val(end_date_ban2);
            $('#authority_ban_details').val(authority_ban);
            $('#reason_ban').val(reason_ban);
            $('#amount_ban').val(amount_ban);
            $('#input-club').val(club_ban);
        }
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'input-club'){
                    $('#date_start_date_ban').focus();
                }
                if(id == 'date_start_date_ban'){
                    $('#date_end_date_ban').focus();
                }
                if(id == 'date_end_date_ban'){
                    $('#date_start_date_ban2').focus();
                }
                if(id == 'date_start_date_ban2'){
                    $('#date_end_date_ban2').focus();
                }
                if(id == 'date_end_date_ban2'){
                    $('#amount_ban').focus();
                }
                if(id == 'amount_ban'){
                    $('#authority_ban_details').focus();
                }
                if(id == 'authority_ban_details'){
                    $('#reason_ban').focus();
                }
                if(id == 'reason_ban'){
                    $('#ban_save_id').focus();
                }
            }
        });

        function removeBanDetail(trainer_id,ban_id,id_remove){
            if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
                if(trainer_id == '0' && ban_id == '0'){
                    alert('Deleted Record Sucessfully');
                    $('#'+id_remove+'').closest("tr").remove();
                    return false;
                }
                $.ajax({
                    url:'index.php?route=catalog/trainer/deletebandeatils&token=<?php echo $token; ?>'+'&trainer_id='+trainer_id+'&ban_id='+ban_id,
                    method: "POST",
                    dataType: 'json',
                    success: function(json)
                    {
                        $('#'+id_remove+'').closest("tr").remove();
                        alert(json['success']);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error deleting data');
                    }
                });
                return false;
            }
        }

        /*function gst_no(){
            $('#gst_no').show();
            var gst_type =  $( "#gst_type" ).val();
        }*/

        $( "#gst_type" ).click(function(){
            var gst_no =  $(this).val();
            if(gst_no == 'R'){
                $("#gst_label").show();
                $("#gst_no").show();
            } else {
                $("#gst_label").hide();
                $("#gst_no").hide();
            }
        });



        $('#gst_no').keyup(function(){
            gst_len = parseInt($("#gst_no").val().length);
           // console.log(gst_len);
            $('.gst_error').html('');
            if(gst_len > 15 || gst_len < 15){
                $('.gst_error').css("color", "red")
                $('.gst_error').append('GST No should be 15 digits!');

                //return false;
            } 

            if(gst_len == 15 ){
                $('.gst_error').html('');
            }   
        });

        $('#pan_no').keyup(function(){
            pan_len = parseInt($("#pan_no").val().length);
           // console.log(gst_len);
            $('.pan_error').html('');
            if(pan_len > 10 || pan_len < 10){
                $('.pan_error').css("color", "red")
                $('.pan_error').append('PAN No should be 10 digits!');

                //return false;
            } 

            if(pan_len == 10 ){
                $('.pan_error').html('');
            }   
        })


        function checkboxs(){
            var date = '31-12-2050';
            if (is_indefinite.checked == true){
                $('#end_date').val(date);
                l_end_date.style.display = "none";
                h_end_date.style.display = "none";
            } else {
                $('#end_date').val('');
                l_end_date.style.display = "block";
                h_end_date.style.display = "block";
            }
            //$('#end_date').val(date);
           // $('#hidden_end_date').val(date);
        }

        function transaction(){
            //alert('inn');
            var trainer_idss = $('#trainer_idss').val();
            var dates = $('#date_of_license_renewal').val();
            var amts = $('#input-license_amt').val();
            var feess = $('#input-fees_paid_type').val();

            $.ajax({
                url:'index.php?route=catalog/trainer/transaction&token=<?php echo $token; ?>'+'&dates='+dates+'&amts='+amts+'&feess='+feess+'&trainer_idss='+trainer_idss,
                method: "POST",
                dataType: 'json',
                success: function(json)
                {
                    if (json['success'] == '1') {
                        var date = $('#date_of_license_renewal').val();
                        var amt = $('#input-license_amt').val();
                        var fees = $('#input-fees_paid_type').val();
                        $('#hidden_renewal_datess').val(date);
                        $('#hidden_renewal_amt').val(amt);
                        $('#hidden_renewal_fee').val(fees);
                        $("#hidden_transaction").show();
                        location.reload();
                    }
                }
            });
        }

        function removes($id){
            var id = $id;
            $.ajax({
                url:'index.php?route=catalog/trainer/delete_transaction&token=<?php echo $token; ?>'+'&id='+id,
                method: "POST",
                dataType: 'json',
                success: function(json)
                {
                    if (json['success'] == '1') {
                        location.reload();
                    }
                }
            });
        }

        
    </script>
    <script type="text/javascript">
        function country(element, zone_id) {
            $.ajax({
                url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + element.value,
                dataType: 'json',
                beforeSend: function() {
                    $('select[name=\'country1\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    console.log(json['zone']);
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'country1[postcode]\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'country1[postcode]\']').parent().parent().removeClass('required');
                    }

                    html = '<option value="">Please Select</option>';

                    if (json['zone'] && json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                            if (json['zone'][i]['zone_id'] == zone_id) {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="0">None</option>';
                    }

                    $('select[name=\'zone_id\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }

        $('select[name$=\'[country_id]\']').trigger('change');


        function countrys(element, zone_id) {
            $.ajax({
                url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + element.value,
                dataType: 'json',
                beforeSend: function() {
                    $('select[name=\'country2\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    console.log(json['zone']);
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'country[postcode]\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'country[postcode]\']').parent().parent().removeClass('required');
                    }

                    html = '<option value="">Please Select</option>';

                    if (json['zone'] && json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                            if (json['zone'][i]['zone_id'] == zone_id) {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="0">None</option>';
                    }

                    $('select[name=\'zone_id2\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }

        $('select[name$=\'[country_id]\']').trigger('change');

    </script>

    <script type="text/javascript">

        $( document ).ready(function() {
            var private_trainer_type =  $('#input-private_trainer').val();
            if(private_trainer_type == 'Y'){
                $('.add-owner-div').show();
            } else {
                $('.add-owner-div').hide();
            }
        });

        $('#add-ownrs').click(function() {
            auto_id = $('.owners_cnt').val();
            id = parseInt(auto_id) + 1;
            html = '';
            html += '<div class="form-group" id="remove_div_'+id+'">';
                html += '<div class="col-sm-10">';
                    html += '<input type="text" name=private_trainers_owner_name['+id+'][owner_name] class="form-control trainer_owner_name" id="trainer_owner_id_'+id+'" />';
                    html += '<input type="hidden" name=private_trainers_owner_name['+id+'][owner_id] id="hidden_trainer_owner_id_'+id+'" />';
                html += '</div>';

                html += '<div class="col-sm-2">';
                    html += '<a onclick="rmvOwnerName('+id+')" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                html += '</div>';
            html += '</div>';
            

            $('.owner_data-div').append(html);
            $('.owners_cnt').val(id);
            $('#trainer_owner_id_'+id).focus();
        });




        $(document).on('keyup', '.trainer_owner_name', function(e) {
            $('.trainer_owner_name').autocomplete({
                'source': function(request, response) {
                    $.ajax({
                    url: 'index.php?route=catalog/trainer/autocompleteOwnername&token=<?php echo $token; ?>&owner_name=' +  encodeURIComponent(request),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item.owner_name,
                                value: item.owner_name,
                                ownercode: item.owner_id
                                }
                            }));
                        }
                    });
                },
                'select': function(item) {
                    idzz = $(this).attr('id');
                    idss = idzz.split('_');
                    $('#'+idzz).val(item['label']);
                    $('#hidden_trainer_owner_id_'+idss[3]).val(item['ownercode']);
                    return false;
                }
            });
        });


        function rmvOwnerName(id){
            $('#remove_div_'+id).remove();

        }

        $(document).on('input', '#input-trainer_name', function(){
            var name = $( "#input-trainer_name" ).val();
            $( "#name1" ).val(name);
        });


        $('#button-other_documents_1').on('click', function() {
$('#form-other_document_1').remove();
$('body').prepend('<form enctype="multipart/form-data" id="form-other_document_1" style="display: none;"><input type="file" name="file" /></form>');
$('#form-other_document_1 input[name=\'file\']').trigger('click');
if (typeof timer != 'undefined') {
    clearInterval(timer);
}
timer = setInterval(function() {
  if ($('#form-other_document_1 input[name=\'file\']').val() != '') {
    clearInterval(timer); 
    image_name = 'file_number';  
    $.ajax({ 
    url: 'index.php?route=catalog/trainer/upload_profile&token=<?php echo $token; ?>'+'&image_name='+image_name,
    type: 'post',   
    dataType: 'json',
    data: new FormData($('#form-other_document_1')[0]),
    cache: false,
    contentType: false,
    processData: false,   
    beforeSend: function() {
      $('#button-upload').button('loading');
    },
    complete: function() {
      $('#button-upload').button('reset');
    },  
    success: function(json) {
      if (json['error']) {
      alert(json['error']);
      }
      if (json['success']) {
      alert(json['success']);
      console.log(json);
      $('input[name=\'file_number_uploads\']').attr('value', json['filename']);
      $('input[name=\'uploaded_file_sources\']').attr('value', json['link_href']);
      d = new Date();
      $('#blah').remove();
      var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="uploaded_file_sources" href="'+json['link_href']+'">View Document</a>';
      var image_data = '<img src="'+json['link_href']+'" height="60" id="blah" alt=""  />'
      $('#uploaded_file_sources').remove();
       $('#profile_pic').append(image_data);
      }
    },      
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
    });
  }
  }, 500);
});

function readURL(input) {
    alert(input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(100)
                .height(80);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

    </script>

<link rel="stylesheet" href="view/stylesheet/theme.default.css">
<!-- <script type="text/javascript" src="view/javascript/jquery/jquery-latest.js"></script> -->
<script type="text/javascript" src="view/javascript/jquery/jquery.tablesorter.js"></script>

<!-- tablesorter widgets (optional) -->
<script type="text/javascript" src="view/javascript/jquery/jquery.tablesorter.widgets.js"></script>

<script type="text/javascript">
$(function() {
  $("#myTable").tablesorter();
});

$(function() {
  $("#myTable").tablesorter({ sortList: [[0,0], [1,0]] });
});

$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable >tbody >tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
</div>
<?php echo $footer; ?>gst_label gst_value