<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<!-- <button onclick="confirm('<?php echo 'Do You want to Save the Changes'; ?>') ? $('#form-manufacturer').submit() : false;" type="button" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button> -->
				<!-- <a href="<?php echo $previous; ?>" data-toggle="tooltip" title="<?php echo 'Previous Inward'; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a> -->
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1>Treatment Entry</h1>
			<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo 'Treatment Entry' ?></a></li>
			<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>
		<?php if ($error_name) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_name; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
		        	<div class="tab-content">
		        		<div class="tab-pane active" id="tab-general">
			        		<div class="form-group" >
		                        <label class="col-sm-2 control-label" ><b style="color: red;"> * </b> <?php echo "Clinic:"; ?></label>
		                        <div class="col-sm-3">
		                            <input type="text"  name="filter_parent_doctor" id="filter_parent_doctor" value="<?php echo $filter_parent_doctor; ?>" placeholder="Clinic Name" class="form-control" readonly/>
		                            <input type="hidden" name="filterParentId" id="filterParentId" value="<?php echo $filter_parent_doctor_id; ?>"  >
		                        </div>
		                        <label class="col-sm-2 control-label" for="input-trainer"><?php echo "Treatment No :"; ?></label>
		                        <div class="col-sm-3">
		                             <input type="text" name="treatment_number" value="<?php echo $input_isse_number; ?>" placeholder="Treatment No " id="input_treatment_number" class="form-control"  readonly="readonly"/>
		                        </div>
		                    </div>
		                     <div class="form-group" style="border-top: 0">
		                     	<label class="col-sm-2 control-label" id= "label_to_doc" ><?php echo "Doctor :"; ?></label>
		                        <div class="col-sm-3"  id="div_to_doc" >
		                              <input type="text" value="<?php echo $child_doctor_name ?>" class="form-control" readonly>
		                        </div>
		                        <label style="" class="col-sm-2 control-label" for="input-date"><?php echo 'Date OF Entry'; ?></label>
					            <div style="" class="col-sm-3">
					            	<input type="text" name="date" value="<?php echo $date; ?>" placeholder="<?php echo 'Date'; ?>" id="input-date" class="form-control date" readonly/>
					        	</div>
		                    </div>

		                    <div class="form-group" style="border-top: 0">
		                         <label class="col-sm-2 control-label lblhorse"><?php echo 'Horse Name'; ?></label>
		                        <div class="col-sm-3" >
		                            <input type="text" name="horse_name" id="horse_name" value="<?php echo $horse_name; ?>" placeholder="Horse Name" class="form-control" readonly>
                                    <input type="hidden" name="hidden_horse_name_id" id="hidden_horse_name_id" value="<?php echo $hidden_horse_name_id; ?>" >
                                   
		                        </div>
		                        <div class="col-sm-3" id="horse_div" style="display: none;">
					          	</div>
		                     	<label class="col-sm-2 control-label label_trainer" ><?php echo "Trainer:"; ?></label>
		                        <div class="col-sm-3 label_trainer">
		                            <input type="text" name="trainer_name" id="trainer_name" value="<?php echo $trainer_name; ?>" placeholder="Trainer Name" class="form-control" readonly>
                                    <input type="hidden" name="hidden_trainer_name_id" id="hidden_trainer_name_id" value="<?php echo $trainer_name_id; ?>" >
		                        </div>
		                        
		                    </div>

								<div class="form-group imp_div" style="<?php echo $sty ?>">
									<table id="tbladdMedicineTran" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
											  <td class="text-center">Code</td>
											  <td class="text-center">Medicine Name</td>
											  <td class="text-center" >Unit </td>
											  <td class="text-center">Quantity</td>
											  <td class="text-center">Purchase Price</td>
											  <td class="text-center">Value</td>
											  
											</tr>
										</thead>
										<tbody>
										<?php if($productraw_datas){ ?>
											<?php foreach ($productraw_datas as $key => $value) { ?>
												<tr >
													<td  class="text-left"><?php echo $value['medicine_code'] ?></td>

													<td  class="text-left"><?php echo $value['medicine_name'] ?>
														<br><i><?php echo $value['syringe_info'] ?></i>

													</td>

													<td  class="text-left"><?php echo $value['unit'] ?></td>

													<td  class="text-left"><?php echo $value['medicine_qty'] ?></td>

													<td  class="text-left"><?php echo $value['rate'] ?></td>

													<td  class="text-left"><?php echo $value['value'] ?></td>
												</tr>

											<?php } ?>
										<?php } else { ?>
										<tr>
											<td  colspan="10" class="text-center"><?php echo $text_no_results; ?></td>
										</tr>
										<?php } ?>
											
										</tbody>

										
									</table>
								</div>
							
							<div class="form-group" style="border-top: 0;">
		                        <label class="col-sm-2 control-label" for="input-trainer"><?php echo "Total Medicine:"; ?></label>
		                        <div class="col-sm-3">
		                             <input type="text" name="total_item"  placeholder="<?php echo "Total Medicine"; ?>" value="<?php echo $total_item; ?>" id="total_item" class="form-control"  readonly="readonly"/>
		                    	</div>
		                        <label style="" class="col-sm-1 control-label" for="input-date"><?php echo 'Total Qty'; ?></label>
					            <div style="" class="col-sm-2">
					            	<input type="text" name="total_qty" value="<?php echo $total_qty; ?>" placeholder="<?php echo 'Total Qty'; ?>" id="total_qty" class="form-control"  readonly="readonly"/>
					        	</div>
								<label class="col-sm-1 control-label" for="input-trainer"><?php echo "Total:"; ?></label>
		                        <div class="col-sm-3">
		                             <input type="text" name="total"  placeholder="<?php echo "Total"; ?>" value="<?php echo $total_amt; ?>" id="total" class="form-control"  readonly="readonly"/>
		                        </div>
		                    </div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY',
});

</script>

<?php echo $footer; ?>