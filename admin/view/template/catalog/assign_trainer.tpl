<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-category" data-toggle="tooltip" title="Update Trainer" class="btn btn-primary">Update Trainer</button>
			</div>
			<h1>Trainer To Staff</h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i>Trainer To Staff</h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category">
					<div style="display: none;" class="well">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="col-sm-4">
										<input type="text" name="filter_doctor_name" id="filter_doctor_name" value="" placeholder="Staff Name" class="form-control">
									</div>
									<div class="col-sm-4">
										<input type="text" name="filter_trainer_name" id="filter_trainer_name" value="" placeholder="Trainer Name" class="form-control">
									</div>
									<a onclick="filter();"  id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<td style="width: 5%;" class="text-left">Sr No</td>
										<td class="text-left"><?php echo "Staff Name"; ?></td>
										<td class="text-left"><?php echo "Designation"; ?></td>
										<td class="text-left"><?php echo "Trainer Name"; ?></td>
									</tr>
								</thead>
								<tbody>
									<?php if ($categories) { ?>
											<?php $i=1; ?>
											<?php foreach ($categories as $ckey => $category) { ?>
											<tr>
												<td class="text-left"><?php echo $i++; ?>
													<input tabindex="" type="hidden" name="trainer[<?php echo $ckey; ?>][staff_id]; ?>]" value="<?php echo $category['id']; ?>" id="" class="form-control" />
												</td>
												<td class="text-left"><?php echo $category['staff']; ?></td>
												<td class="text-left">
													<input tabindex="" type="text" name="trainer[<?php echo $ckey; ?>][designation]; ?>]" value="<?php echo $category['designation']; ?>" placeholder="Trainer Name" id="designation_<?php echo $ckey; ?>" class="form-control" />
													
												</td>
												<td class="text-left">
													<input tabindex="" type="text" name="trainer[<?php echo $ckey; ?>][trainer_name]; ?>]" value="<?php echo $category['trainer_name']; ?>" placeholder="Trainer Name" id="trainer_<?php echo $ckey; ?>" class="trainer form-control" />
													<input tabindex="" type="hidden" name="trainer[<?php echo $ckey; ?>][trainer_id]; ?>]" value="<?php echo $category['trainer_id']; ?>" id="trainerid_<?php echo $ckey; ?>" class="form-control" />
												</td>
											</tr>
											<?php } ?>
									<?php } else { ?>
									<tr>
										<td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$('.trainer').autocomplete({
	'source': function(request, response) {
	if (request) {
		var trainer_idss = $(this).attr("id");
		var trainer_ids = trainer_idss.split('_');
		var trainer_id = trainer_ids[1];
		$.ajax({
			url: 'index.php?route=catalog/trainer_staff/autocompleteparent&token=<?php echo $token; ?>&parent=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
			response($.map(json, function(item) {
				return {
				label: item['trainer_name'],
				id: item['id'],
				value: item['trainer_name'],
				}
			}));
			}
		});
	}
	},
	'select': function(item) {
	var trainer_idss = $(this).attr("id");
	var trainer_ids = trainer_idss.split('_');
	var trainer_id = trainer_ids[1];
	$('#trainer_'+trainer_id+'').val(item['label']);
	$('#trainer_'+trainer_id+'').val(item['value']);
	$('#trainerid_'+trainer_id+'').val(item['id']);
	$('.dropdown-menu').hide();
	}
});

function update_trainer(id) {
	var trainer_id = $("#trainer_id").val();
	var trainer_name = $("#input-parent").val();
	url = 'index.php?route=catalog/assign_trainer/update_trainer&token=<?php echo $token; ?>';
	if ((trainer_id) && (trainer_name)) {
		url += '&id=' + encodeURIComponent(id);
		url += '&trainer_id=' + encodeURIComponent(trainer_id);
		url += '&trainer_name=' + encodeURIComponent(trainer_name);
	}
	window.location.href = url;
}
</script>
<?php echo $footer; ?>