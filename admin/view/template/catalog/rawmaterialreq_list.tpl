<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header" >
		<div class="container-fluid">
			<?php if($user_group_id != 15){ ?>
				<div class="pull-right">
					<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> 
					<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Go To Dashboard" class="btn btn-primary">Go To Dashboard</a>
					<button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-rawmaterialreq').submit() : false;"><i class="fa fa-trash-o"></i></button>
				</div>
			<?php } ?>
			<h1>Indent</h1>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i>Indent</h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-rawmaterialreq">
					<div class="well">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div style="display: none;" class="col-sm-3">
										<input type="text" name="filter_rawmaterialreq" value="<?php echo $filter_rawmaterialreq; ?>" placeholder="<?php echo "Indent Req. No"; ?>" id="input-filter_rawmaterialreq" class="form-control" />
									</div>
									<div class="col-sm-3">
										<input type="text" name="filter_order_no" value="<?php echo $filter_order_no; ?>" placeholder="<?php echo "Indent Code"; ?>" id="input-filter_order_no" class="form-control" />
									</div>
									<div class="col-sm-3">
					                    <select name="filter_status" id="filter_status" class="form-control">
					                        <?php foreach ($status as $key => $tvalue) { ?>
					                            <?php if($key == $filter_status){ ?>
					                            	<option value="<?php echo $key ?>" selected="selected"><?php echo $tvalue?></option>
					                            <?php } else { ?>
					                            	<option value="<?php echo $key ?>"><?php echo $tvalue?></option>
					                            <?php } ?>
					                        <?php } ?>    
					                    </select>
					                </div>
									<div class="col-sm-3">
										<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<?php if($is_user == '0'){ ?>
										<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
										<?php } ?>
										<td class="text-left"><?php echo 'Indent Code:'; ?></td>
										<td class="text-left"><?php echo 'Date'; ?></td>
										<td class="text-left"><?php echo 'Approval'; ?></td>
										<td class="text-left"><?php echo 'Logs'; ?></td>
										<td class="text-right"><?php echo 'Action'; ?></td>
									</tr>
								</thead>
								<tbody>
									<?php if ($rawmaterialreqs) { ?>
										<?php $i = 1; ?>
										<?php foreach ($rawmaterialreqs as $rawmaterialreq) { ?>
										<tr>
											<?php if($is_user == '0'){ ?>
											<td style="display: none;" class="text-center"><?php if (in_array($rawmaterialreq['order_id'], $selected)) { ?>
												<input type="checkbox" name="selected[]" value="<?php echo $rawmaterialreq['order_id']; ?>" checked="checked" />
												<?php } else { ?>
												<input type="checkbox" name="selected[]" value="<?php echo $rawmaterialreq['order_id']; ?>" />
												<?php } ?>
											</td>
											<?php } ?>
											<td class="text-left"><?php echo $rawmaterialreq['order_no']; ?>
												<input type="hidden" name="count" class="count" value="<?php echo $i; ?>" />
												<input type="hidden" name="order_id-<?php echo $rawmaterialreq['order_id'] ?>" id="order_id-<?php echo $i ?>" value="<?php echo $rawmaterialreq['order_id']; ?>" />
											</td>
											<td class="text-left"><?php echo date('d-M-Y', strtotime($rawmaterialreq['date'])); ?></td>
											<?php if($rawmaterialreq['approval_status'] == 1){ ?>
												<td>Approved</td>
											<?php } elseif($rawmaterialreq['approval_status'] == 0) { ?>
												<td>Pending</td>
											<?php } else { ?>
												<td>Rejected</td>
											<?php } ?>
											<td class="text-left"><?php echo $rawmaterialreq['log_datas']; ?></td>
											<td class="text-right">
												<?php if($rawmaterialreq['approval_status'] == 1){ ?>
													<a href="<?php echo $rawmaterialreq['email']; ?>" data-toggle="tooltip" title="<?php echo 'Email'; ?>" class="btn btn-primary"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
													<a href="<?php echo $rawmaterialreq['print']; ?>" data-toggle="tooltip" title="<?php echo 'Print'; ?>" class="btn btn-primary"><i class="fa fa-print"></i></a>
												<?php } ?>
												<?php if($rawmaterialreq['approval_status'] == 0){ ?>
													<a href="<?php echo $rawmaterialreq['edit']; ?>" data-toggle="tooltip" title="<?php echo 'Edit'; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
												<?php } ?>
												<a href="<?php echo $rawmaterialreq['edit1']; ?>" data-toggle="tooltip" title="<?php echo 'View'; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
												<br>
												<div class="pull-right" >
													<textarea style="display: none; margin-top: 10px;" rows="3" placeholder="Reason" class="col-sm-8" id="reason_<?php echo $rawmaterialreq['order_id']; ?>"></textarea>
													<a onclick="rejected(<?php echo $rawmaterialreq['order_id']; ?>)" style="display: none; margin-top: 10px;margin-left: 10px;" data-toggle="tooltip" title="<?php echo 'Reject'; ?>" class="btn btn-danger col-sm-3" id="reject_<?php echo $rawmaterialreq['order_id']; ?>" >Reject</a>
												</div>
											</td>
										</tr>
										<?php $i ++; ?>
										<?php } ?>

									<?php } else { ?>
									<tr>
										<td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>
		        <div class="row">
		        	<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		        	<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		        </div>
	        </div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--

$('#button-filter').on('click', function() {
  var url = 'index.php?route=catalog/rawmaterialreq&token=<?php echo $token; ?>';

  var filter_rawmaterialreq = $('input[name=\'filter_rawmaterialreq\']').val();
 
  if (filter_rawmaterialreq) {
	var filter_order_id = $('input[name=\'filter_order_id\']').val();
	if (filter_order_id) {
	  url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}
	url += '&filter_rawmaterialreq=' + encodeURIComponent(filter_rawmaterialreq);
  
  }

  var filter_order_no = $('input[name=\'filter_order_no\']').val();
  if (filter_order_no) {
    url += '&filter_order_no=' + encodeURIComponent(filter_order_no);
  }

  var filter_status = $('select[name=\'filter_status\']').val();
  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

	var filter_productsort = $('select[name=\'filter_productsort\']').val();
	//alert(filter_productsort);

	if (filter_productsort) {
	  url += '&filter_productsort=' + encodeURIComponent(filter_productsort);
	}

  location = url;
});

$('input[name=\'filter_rawmaterialreq\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/rawmaterialreq/autocomplete&token=<?php echo $token; ?>&filter_order_id=' +  encodeURIComponent(request),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['order_id'],
			value: item['order_id']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter_rawmaterialreq\']').val(item['label']);
	$('input[name=\'filter_order_id\']').val(item['value']);
  }
});

//--></script>
<script type="text/javascript">

function reason_approve(order_id) {
	$('#reason_'+order_id+'').show();
	$('#reject_'+order_id+'').show();
}

function rejected(order_id) {
	var reason = $('#reason_'+order_id+'').val();
	url = 'index.php?route=catalog/rawmaterialreq/indent_dis_approval&token=<?php echo $token; ?>';
	if (reason != '') {
		url += '&reason=' + encodeURIComponent(reason);
		url += '&order_id=' + encodeURIComponent(order_id);
	} else {
		return false;
	}
	window.location.href = url;
}
</script>
<?php echo $footer; ?>