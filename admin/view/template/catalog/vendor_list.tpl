<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header" >
		<div class="container-fluid">
			<div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				<button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-vendor').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-vendor">
				<div class="well">
					<div class="row">
						<div class="col-sm-12">
							<div class="col-sm-4">
								<label class="control-label" for="input-name"></label>
								<input type="text" name="filter_vendor" value="<?php echo $filter_vendor; ?>" placeholder="<?php echo "Supplier Name"; ?>" id="input-filter_vendor" class="form-control" />
								<input type="hidden" name="filter_vendor_id" value="<?php echo $filter_vendor_id; ?>" id="input-filter_vendor_id" class="form-control" />
							</div>
							<div style="margin-top: 15px" class="col-sm-3">
								<select name="filter_status" id="filter_status" class="form-control">
										<?php foreach ($status as $key => $tvalue) { ?>
												<?php if($key == $filter_status){ ?>
													<option value="<?php echo $key ?>" selected="selected"><?php echo $tvalue?></option>
												<?php } else { ?>
													<option value="<?php echo $key ?>"><?php echo $tvalue?></option>
												<?php } ?>
										<?php } ?>    
								</select>
							</div>
							<button onclick="filter();" style="margin-top: 15px" type="button" id="button-filter" class="btn btn-primary " ><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
						</div>
					</div>
				</div>
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<?php if($is_user == '0'){ ?>
									<td style="display: none; width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
									<?php } ?>
									<td style="width: 5%;" class="text-left">Sr No</td>
									<td class="text-left">Supplier Code</td>
									<td class="text-left"><?php echo $column_name; ?></td>
									<td class="text-left">Status</td>
									<td class="text-left">Logs</td>
									<?php if($is_user == '0'){ ?>
									<td class="text-right"><?php echo $column_action; ?></td>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<?php if ($vendors) { ?>
								<?php $i=1; ?>
								<?php foreach ($vendors as $vendor) { ?>
								<tr>
									<?php if($is_user == '0'){ ?>
									<td style="display: none;" class="text-center"><?php if (in_array($vendor['vendor_id'], $selected)) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $vendor['vendor_id']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $vendor['vendor_id']; ?>" />
										<?php } ?></td>
									<?php } ?>
									<td class="text-left"><?php echo $i++; ?></td>
									<td class="text-left"><?php echo $vendor['code']; ?></td>
									<td class="text-left">
										<a data-toggle="modal" data-target="#myModal" style="<?php echo $vendor['anchor_styles'] ?>" class="common_class click_class-<?php echo $i; ?>"><?php echo $vendor['vendor_name']; ?></a>
										<input type="hidden" name="count" class="count" value="<?php echo $i; ?>" />
										<input type="hidden" name="vendor_id-<?php echo $vendor['vendor_id'] ?>" id="vendor_id-<?php echo $i ?>" value="<?php echo $vendor['vendor_id']; ?>" />
									</td>
									<td class="text-left"><?php echo $vendor['status']; ?></td>
									<td class="text-left"><?php echo $vendor['log_datas']; ?></td>
									<?php if($is_user == '0'){ ?>
										<td class="text-right">
											<a href="<?php echo $vendor['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
											<!-- <a href="<?php echo $vendor['export']; ?>" data-toggle="tooltip" title="<?php echo 'Export'; ?>" class="btn btn-primary">
											<i class="fa fa-print"></i></a> -->
										</td>
									<?php } ?>
								</tr>
								<?php $i ++; ?>
								<?php } ?>
								<?php } else { ?>
								<tr>
									<td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</form>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Vendor Overview</h4>
			</div>
			<div class="modal-body edit-content">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript"><!--

$('#myModal').on('show.bs.modal', function(e) {
		var $modal = $(this),
		esseyId = e.relatedTarget.id;
		idsss = e.relatedTarget.className;
		s_ids = idsss.split(' ');
		idss = s_ids[1];
		s_id = idss.split('-');
		id = s_id[1];
		vendor_id = $('#vendor_id-'+id).val();
		//console.log(productnew_id);
		//return false;
		$.ajax({
				cache: false,
				type: 'POST',
				url: 'index.php?route=catalog/vendor/getdata&token=<?php echo $token; ?>&filter_name_id=' +  encodeURIComponent(vendor_id),
				data: 'filter_name_id=' + vendor_id,
				success: function(data) {
						//console.log(data.html);
						$modal.find('.edit-content').html(data.html);
				}
		});
})

function filter() {
	var url = 'index.php?route=catalog/vendor&token=<?php echo $token; ?>';

	var filter_vendor = $('input[name=\'filter_vendor\']').val();
	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_vendor) {
		var filter_vendor_id = $('input[name=\'filter_vendor_id\']').val();
		if (filter_vendor_id) {
			url += '&filter_vendor_id=' + encodeURIComponent(filter_vendor_id);
		}
		url += '&filter_vendor=' + encodeURIComponent(filter_vendor);
	
	}

		if (filter_status) {
				url += '&filter_status=' + encodeURIComponent(filter_status);
		}

	var filter_vendorsort = $('select[name=\'filter_vendorsort\']').val();
	 // alert(filter_vendorsort);

		if (filter_vendorsort) {
			url += '&filter_vendorsort=' + encodeURIComponent(filter_vendorsort);
		}

	/*
	var filter_supplier = $('input[name=\'filter_supplier\']').val();

	if (filter_supplier) {
		var filter_supplier_id = $('input[name=\'filter_supplier_id\']').val();
		if (filter_supplier_id) {
			url += '&filter_supplier_id=' + encodeURIComponent(filter_supplier_id);
		}
		url += '&filter_supplier=' + encodeURIComponent(filter_supplier);
	}
	*/

	location = url;
}

$('input[name=\'filter_vendor\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/vendor/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['vendor_name'],
						value: item['vendor_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_vendor\']').val(item['label']);
		$('input[name=\'filter_vendor_id\']').val(item['value']);
		filter();
		return false;
	}
});

<?php if($is_user == '0'){ ?>
	var DELAY = 700, clicks = 0, timer = null;
	$(function(){
		$("a.common_class").on("click", function(e){
			//console.log(e.toElement.className);
			clicks++;  //count clicks
			if(clicks === 1) {
				timer = setTimeout(function() {
					
					//idsss = $('.common_class').attr('class');
					/*
					idsss = e.toElement.className;
					s_ids = idsss.split(' ');
					idss = s_ids[1];
					s_id = idss.split('-');
					id = s_id[1];
					vendor_id = $('#vendor_id-'+id).val();
					base_link = '<?php echo $base_link ?>';
					base_link = base_link.replace('&amp;', '&');
					final_url = base_link+'&vendor_id='+vendor_id;
					//alert(final_url);
					window.location.href = final_url;  //perform single-click action    
					*/
					clicks = 0;             //after action performed, reset counter
				}, DELAY);
			} else {
				clearTimeout(timer);    //prevent single-click action
				//idsss = $('.common_class').attr('class');
				idsss = e.toElement.className;
				s_ids = idsss.split(' ');
				idss = s_ids[1];
				s_id = idss.split('-');
				id = s_id[1];
				vendor_id = $('#vendor_id-'+id).val();
				base_link = '<?php echo $base_link_1 ?>';
				base_link = base_link.replace('&amp;', '&');
				final_url = base_link+'&vendor_id='+vendor_id;
				//alert(final_url);
				window.location.href = final_url;  //perform double-click action
				clicks = 0;             //after action performed, reset counter
			}
		}).on("dblclick", function(e){
			e.preventDefault();  //cancel system double-click event
		});
	});
<?php } else { ?>
	$('.common_class').css('cursor', 'default');
<?php } ?>




$(document).ready(function()               
		{
				// enter keyd
				$(document).bind('keypress', function(e) {
						if(e.keyCode==13){
								 $('#button-filter').trigger('click');
						 }
				});
		});

//--></script>
<script type="text/javascript">
$( document ).ready(function() {
		var refer = "<?php echo $refer; ?>";
		if(refer == '1') {
			close();
		}
});
</script>
<?php echo $footer; ?>