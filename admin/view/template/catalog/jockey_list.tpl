<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> 
            <!-- <a href="<?php echo $repair; ?>" data-toggle="tooltip" title="<?php echo $button_rebuild; ?>" class="btn btn-default"><i class="fa fa-refresh"></i></a> -->
            <button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
                    <div class="well" style="background-color: #ffffff;">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" name="jockey_name" id="jockey_name" value="<?php echo $filter_jockey_name?>" placeholder="Jockey Name" class="form-control">
                                        <input type="hidden" name="hidden_jockey_name_id" id="hidden_jockey_name_id" value="" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="jockey_code" id="jockey_code" value="<?php echo $filter_jockey_code?>" placeholder="Jockey Code" class="form-control">
                                        <input type="hidden" name="hidden_jockey_code_id" id="hidden_jockey_code_id" value="" >

                                    </div>
                                    <div class="col-sm-3">
                                        <select name="filter_status" id="filter_status" class="form-control">
                                            <?php foreach ($status as $key => $tvalue) { ?>
                                                <?php if($key == $filter_status){ ?>
                                                  <option value="<?php echo $key ?>" selected="selected"><?php echo $tvalue?></option>
                                                <?php } else { ?>
                                                  <option value="<?php echo $key ?>"><?php echo $tvalue?></option>
                                                <?php } ?>
                                            <?php } ?>    
                                        </select>
                                      </div>
                                    <div class="col-sm-1">
                                        <a onclick="filter()" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="display: none;" "width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                    
                                    <td class="text-center"><?php if ($sort == 'trainers.id') { ?>
                                        <a href="<?php echo $sr_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Sr No'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sr_no; ?>"><?php echo 'Sr No'; ?></a>
                                        <?php } ?>
                                    </td>

                                    <td class="text-center"><?php if ($sort == 'jockey_code') { ?>
                                        <a href="<?php echo $jockey_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Jockey Code'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $jockey_code; ?>"><?php echo 'Jockey Code'; ?></a>
                                        <?php } ?>
                                    </td>

                                    <td class="text-center"><?php if ($sort == 'jockey_name') { ?>
                                        <a href="<?php echo $jockey_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Jockey Name'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $jockey_name; ?>"><?php echo 'Jockey Name'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'jcd.mobile_no') { ?>
                                        <a href="<?php echo $mobile_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Mobile Number'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $mobile_no; ?>"><?php echo 'Mobile Number'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'jcd.email_id') { ?>
                                        <a href="<?php echo $email_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Email Id'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $email_id; ?>"><?php echo 'Email Id'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'jcd.pan_no') { ?>
                                        <a href="<?php echo $pan_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Pan No'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $pan_no; ?>"><?php echo 'Pan No'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'license_type') { ?>
                                        <a href="<?php echo $license_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'License type'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $license_type; ?>"><?php echo 'License type'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'origin') { ?>
                                        <a href="<?php echo $origin; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Indian/Foreign'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $origin; ?>"><?php echo 'Indian/Foreign'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'allowance_claiming') { ?>
                                        <a href="<?php echo $allowance_claiming; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Allowance'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $allowance_claiming; ?>"><?php echo 'Allowance'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'apprentice') { ?>
                                        <a href="<?php echo $apprentice; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Apprentice'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $apprentice; ?>"><?php echo 'Apprentice'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'age') { ?>
                                        <a href="<?php echo $age; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Age'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $age; ?>"><?php echo 'Age'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'ban') { ?>
                                        <a href="<?php echo $ban; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Ban'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $ban; ?>"><?php echo 'Ban'; ?></a>
                                        <?php } ?>
                                    </td>
                                     <td class="text-center"><?php if ($sort == 'ban') { ?>
                                        <a href="<?php echo $ban; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Ban Start Date'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $ban; ?>"><?php echo 'Ban Start Date'; ?></a>
                                        <?php } ?>
                                    </td>
                                     <td class="text-center"><?php if ($sort == 'ban') { ?>
                                        <a href="<?php echo $ban; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Ban End Date'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $ban; ?>"><?php echo 'Ban End Date'; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php if ($sort == 'Jockey Status') { ?>
                                        <a href="<?php echo $jockey_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Status'; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $jockey_status; ?>"><?php echo 'Status'; ?></a>
                                        <?php } ?>
                                    </td>
                                    
                                    <td style="padding-right: 0px;border-right: none;" class="text-right">Action</td>
                                    <td style="padding-right: 0px;border-left: none;" class="text-center"></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if ($categories) { ?>
                                <?php $i=1; ?>
                                <?php foreach ($categories as $category) { ?>
                                    <tr>
                                        <td style="display: none;" class="text-center"><?php if (in_array($category['jockey_id'], $selected)) { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $category['jockey_id']; ?>" checked="checked" />
                                            <?php } else { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $category['jockey_id']; ?>" />
                                            <?php } ?></td>
                                        <td class="text-left"><?php echo $i++; ?></td>
                                        <td class="text-left"><?php echo $category['jockey_code']; ?></td>
                                        <td class="text-left"><?php echo $category['jockey_name']; ?></td>
                                        <td class="text-left"><?php echo $category['mobile_no']; ?></td>
                                        <td class="text-left"><?php echo $category['email_id']; ?></td>
                                        <td class="text-left"><?php echo $category['pan_no']; ?></td>
                                        <td class="text-left"><?php echo $category['license_type']; ?></td>
                                        <td class="text-left"><?php echo $category['origin']; ?></td>
                                        <td class="text-left"><?php echo $category['allowance_claiming']; ?></td>
                                        <td class="text-left"><?php echo $category['apprentice']; ?></td>
                                        <td class="text-right"><?php echo $category['age']; ?></td>
                                        <td class="text-left"><?php echo $category['ban']; ?></td>
                                        <td class="text-left"><?php echo $category['start']; ?></td>
                                        <td class="text-left"><?php echo $category['end']; ?></td>
                                         <?php if ($category['jocky_status'] == '1') { ?>
                                            <td class="text-left">In</td>
                                            <?php } else { ?>
                                                <td class="text-left">Exit</td>
                                            <?php } ?>
                                        
                                        <td style="padding-right: 0px;border-right: none;">
                                            <a href="<?php echo $category['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                        </td>
                                        <td style="padding-left: 3px;border-left: none;">
                                            <a href="<?php echo $category['view']; ?>" data-toggle="tooltip" title="View" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                <tr>
                                    <td class="text-center" colspan="17"><?php echo $text_no_results; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script type="text/javascript">
        function filter() { 
            // alert('innnn');
            var filter_jockey_name =$('#jockey_name').val();
            var filter_jockey_code = $('#jockey_code').val();
             var filter_status = $('select[name=\'filter_status\']').val();
            if(filter_jockey_name !='' || filter_jockey_code !='' || filter_status != ''){
                //alert("abc");
                url = 'index.php?route=catalog/jockey&token=<?php echo $token; ?>';
                if (filter_jockey_name) {
                  url += '&filter_jockey_name=' + encodeURIComponent(filter_jockey_name);
                }

                if (filter_jockey_code) {
                    url += '&filter_jockey_code=' + encodeURIComponent(filter_jockey_code);
                } 
                 if (filter_status) {
                    url += '&filter_status=' + encodeURIComponent(filter_status);
                }
                window.location.href = url;
            } 
            else {
                // alert('Please select the filters');
                return false;
            }
            
        }

        $('#jockey_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#trainer_id').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/jockey/autocompleteJockey&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request), dataType: 'json',
                        success: function(json) {   
                            $('#trainer_codes_id').find('option').remove();
                            response($.map(json, function(item) {
                                return {
                                    label: item.trainer_name,
                                    value: item.trainer_name,
                                    trainer_id:item.trainer_id,
                                    trainer_codes:item.trainer_code
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(item) {
                console.log(item);
                $('#jockey_name').val(item.value);
                $('#trainer_id').val(item.trainer_id);
                $('.dropdown-menu').hide();
                filter();
                return false;
            },
        });
    </script>
<?php echo $footer; ?>