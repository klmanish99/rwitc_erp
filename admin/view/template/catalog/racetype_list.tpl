<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> <a style="display: none;" href="<?php echo $repair; ?>" data-toggle="tooltip" title="<?php echo $button_rebuild; ?>" class="btn btn-default"><i class="fa fa-refresh"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>




      <h1><?php echo $heading_title ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
        <div class="col-sm-12">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                      <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                      <!-- <td style="width: 5%;" class="text-left">Sr No</td> -->
                      <td class="text-left"><?php echo $column_racetype ?></td>
                      <td class="text-left"><?php echo $column_class ?></td>
                      <td class="text-right"><?php echo $column_action; ?></td>
                    </tr>
              </thead>
              <tbody>
                <?php if ($categories) { ?>
                        <?php $i=1; ?>
                        <?php foreach ($categories as $category) { ?>
                        <tr>
                          <td style="width: 1px;" class="text-center"><?php if (in_array($category['id'], $selected)) { ?>
                            <input type="checkbox" name="selected[]" value="<?php echo $category['id']; ?>" checked="checked" />
                            <?php } else { ?>
                            <input type="checkbox" name="selected[]" value="<?php echo $category['id']; ?>" />
                            <?php } ?></td>
                            <!-- <td class="text-left"><?php echo $i++; ?></td> -->
                            <td class="text-left"><?php echo $category['race_type']; ?></td>
                            <td class="text-left">
                                 <?php

                                  echo $category['race_class']; 


                                   ?> 
                             </td>
                            <td class="text-right"><a href="<?php echo $category['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                        </tr>
                        <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
                    </tr>
                    <?php } ?>
              </tbody>
            </table>
          </div>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
  function filter() { 
      var filter_owner_name = $('#filterOwnerName').val();
      var filter_owner_id = $('#filterOwnerId').val();
      //var filter_status = $('#filter_status').val();
      var filter_status = $('select[name=\'filter_status\']').val();
      //if(filter_owner_name != '' || filter_owner_id != ''){
        url = 'index.php?route=catalog/owner&token=<?php echo $token; ?>';
      if (filter_owner_name) {
        url += '&filter_owner_name=' + encodeURIComponent(filter_owner_name);
      }

      if (filter_owner_id) {
        url += '&filter_owner_id=' + encodeURIComponent(filter_owner_id);
      }

      if (filter_status) {
        url += '&filter_status=' + encodeURIComponent(filter_status);
      }

      window.location.href = url;
      /*} else {
      alert('Please select the filters');
      return false;
      }*/
  }

  $('#filterOwnerName').autocomplete({
    delay: 500,
    source: function(request, response) {
        $('#trainer_id').val('');
        if(request != ''){
            $.ajax({
                url: 'index.php?route=catalog/owner/autocompleteOwner&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {   
                    $('#trainer_codes_id').find('option').remove();
                    response($.map(json, function(item) {
                        return {
                            label: item.trainer_name,
                            value: item.trainer_name,
                            trainer_id:item.trainer_id,
                        }
                    }));
                }
            });
        }
    }, 
    select: function(item) {
        console.log(item);
        $('#filterOwnerName').val(item.value);
        $('#trainer_id').val(item.trainer_id);
        $('.dropdown-menu').hide();
        filter();
        return false;
    },
  });
</script>