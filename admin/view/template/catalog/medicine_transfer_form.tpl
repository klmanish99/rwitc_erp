<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button onclick="confirm('<?php echo 'Do You want to Save the Changes'; ?>') ? $('#form-manufacturer').submit() : false;" type="button" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<!-- <a href="<?php echo $previous; ?>" data-toggle="tooltip" title="<?php echo 'Previous Inward'; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a> -->
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>
		<?php if ($error_name) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_name; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>

		<?php if ($error_medicine_error) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_medicine_error; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>

		<?php if ($day_close_error) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $day_close_error; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>

		
		<div class="panel panel-default">
			
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
					
		        	<div class="tab-content">
		        		<div class="tab-pane active" id="tab-general">
			        		<div class="form-group" >
			        			<label class="col-sm-2 control-label" for="input-code"><b style="color: red;"></b> <?php echo "Doctor Code:"; ?></label>
		                        <div class="col-sm-2">
									<input type="hidden" value="<?php echo $user_log_grp_id; ?>" name="user_log_grp_id">
									<input type="hidden" value="<?php echo $user_log_id; ?>" name="user_log_id">
		                            <input type="text"  name="input-code" id="input-code" value="" placeholder="Code" class="form-control" />
		                            <br>
		                        </div>
		                        <label class="col-sm-2 control-label" for="input-horse"><b style="color: red;"> * </b> <?php echo "Clinic Name:"; ?></label>
		                        <div class="col-sm-2">
		                            <input type="text"  name="filter_parent_doctor" id="filter_parent_doctor" value="<?php echo $filter_parent_doctor ?>" placeholder="Clinic Name" class="form-control" />
		                            <input type="hidden" name="filterParentId" id="filterParentId" value="<?php echo $filter_parent_doctor_id ?>"  class="form-control">
		                            <br>
		                        </div>
		                        <label class="col-sm-2 control-label" for="input-trainer"><?php echo "Issue No :"; ?></label>
		                        <div class="col-sm-2">
		                             <input type="text" name="issue_number" value="<?php echo $input_isse_number ?>" placeholder="Issue No " id="input_isse_number" class="form-control"  readonly="readonly"/>
		                        </div>
		                    </div>
		                     <div class="form-group" style="border-top: 0;">
			                     	<label style="display: none;" class="col-sm-2 control-label" id= "label_to_doc" ><?php echo " Doctor Name :"; ?></label>
			                        <div style="display: none;" class="col-sm-2"  id="div_to_doc">
			                            <select name="to_doc" id="input_to_doc" class="form-control etr_evnt">
			                            	<?php foreach ($all_docs as $akey => $avalue) { 
			                            		if($avalue['id'] == $to_doc){
			                            		?>
			                            			<option value="<?php echo  $avalue['id'] ?>" selected="selected"> <?php echo $avalue['doctor_name'] ?> </option>
			                            		<?php } else { ?>
			                            			<option value="<?php echo  $avalue['id'] ?>"> <?php echo $avalue['doctor_name'] ?> </option>
			                            		<?php } ?>


		
			                            	<?php } ?>
						

			                            </select>
			                        </div>
		                        <label style="" class="col-sm-2 control-label" for="input-date"><?php echo ' Date :'; ?></label>
					            <div style="" class="col-sm-2">
					            	<input readonly="readonly" type="text" name="date" value="<?php echo $expire_date ?>" placeholder="<?php echo 'Date'; ?>" id="input-date" class="form-control date"/>
					        	</div>
		                    </div>

		            		<div class="form-group main_div">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-center" style="width: 10%;">Item Code</td>
										  <td class="text-center">Item Name</td>
										  <td class="text-center" >Expire Date </td>
										  <td class="text-center">Quantity</td>
										</tr>

									</thead>
									<tbody>
										<tr style="background: white;" >
											<td  class="text-left">
												<input type="text" name="input_item_code" value=""  id="input_item_code" class="form-control input-cls" />
											</td>
											<td class="text-left">
												<input type="text" name="item_name" value="" placeholder="<?php echo 'Item Name'; ?>" id="input_item_name" class="form-control input-cls" />
											</td>
											<td class="text-left">
												<input  type="text" name="input_exp_date" value="" placeholder="<?php echo 'Expire Date'; ?>" id="input_exp_date" class="form-control input-cls" readonly/>
											</td>
											<td class="text-left">
												<input  type="text" name="input_qty" value="" placeholder="Quantity" id="input_qty" class="form-control input-cls" />
													<input type="hidden" name="input_qty_hidden" value="" placeholder="Quantity" id="input_qty_hidden" class="form-control" />
													
											</td>

											<td style="display:none;">
												<input type="hidden" name="input_purchase_prise" value=""  id="input_purchase_prise" class="form-control" />
											</td>
											<td class="text-left" style="display:none;">
												<input   type="hidden" name="final_value" value="" placeholder="Value" id="input_value" class="form-control" readonly="readonly" />
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<?php if($productraw_datas){ 
								$sty = ""; 
							} else { 
								$sty = "display: none;";

							 } ?>
					      <div class="form-group imp_div" style="<?php echo $sty ?>">
								<table id="tbladdMedicineTran" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-center">Item Code</td>
										  <td class="text-center">Item Name</td>
										  <td class="text-center" >Expire Date </td>
										  <td class="text-center">Quantity</td>
										  <td class="text-center">Action</td>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($productraw_datas as $key => $value) { ?>
											<tr id="productraw_row<?php echo $key ?>">
												<td id="input_mt_qty_label<?php echo $key ?>" class="text-left"><?php echo $value['input_item_code'] ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][input_item_code]" value="<?php echo $value['input_item_code'] ?>"  class="form-control po_qty_class" />
												</td>

												<td id="input_mt_qty_label<?php echo $key ?>" class="text-left"><?php echo $value['item_name'] ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][item_name]" value="<?php echo $value['item_name'] ?>"  class="form-control po_qty_class" />
												</td>

												<td id="input_mt_qty_label<?php echo $key ?>" class="text-left"><?php echo $value['expiry_date'] ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][expiry_date]" value="<?php echo $value['expiry_date'] ?>"  class="form-control po_qty_class" />
												</td>

												<td id="input_mt_qty_label<?php echo $key ?>" class="text-right allqtyTotals"><?php echo $value['input_qty'] ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][input_qty]" value="<?php echo $value['input_qty'] ?>"  class="form-control po_qty_class" />
													
												</td>
												

												<td id="input_mt_qty_label<?php echo $key ?>" class="text-right allPurchaseTotals" style="display:none;">
													<input type="hidden"  value=""  class="form-control po_qty_class" />
												</td>'

												<td id="input_mt_qty_label<?php echo $key ?>" class="text-right allValueTotals" style="display:none;">
													<input type="hidden"  value=""  class="form-control allValueTotals_amt po_qty_class" />
												</td>'


												<td class="text-left"><button onclick="remove_reco(<?php echo $key ?>)" class="btn btn-danger" id="remove<?php echo $key ?>" ><i class="fa fa-minus-circle"></i></button></td>
											</tr>

										<?php } ?>
										
									</tbody>
									
								</table>
							</div>
							<div class="form-group" style="border-top: 0;">
		                        <label class="col-sm-2 control-label" for="input-trainer"><?php echo "Total Item:"; ?></label>
		                        <div class="col-sm-3">
		                             <input type="text" name="total_item"  placeholder="<?php echo "Total Item"; ?>" value="<?php echo $total_item; ?>" id="total_item" class="form-control"  readonly="readonly"/>
		                    	</div>
		                        <label style="" class="col-sm-1 control-label" for="input-date"><?php echo 'Total Qty'; ?></label>
					            <div style="" class="col-sm-2">
					            	<input type="text" name="total_qty" value="<?php echo $total_qty; ?>" placeholder="<?php echo 'Total Qty'; ?>" id="total_qty" class="form-control"  readonly="readonly"/>
					        	</div>
								<label class="col-sm-1 control-label" for="input-trainer"><?php echo "Total:"; ?></label>
		                        <div class="col-sm-3">
		                             <input type="text" name="total"  placeholder="<?php echo "Total"; ?>" value="<?php echo $total_amt; ?>" id="total" class="form-control"  readonly="readonly"/>
		                        </div>
		                    </div>
		                    <input type="hidden" id="last_owner_id" value="0" class="form-control last_owner_id" />
		                    <input type="hidden" id="selected_pre_owners" value="0" class="form-control selected_pre_owners" />
		                    <input type="hidden"  id="is_owners" class="is_owners">
		                     <div class="form-group " style="border-top: 0; height: 100%;display: flex;justify-content: center;align-items: center;" >
			                    <!-- <a id="save"  class="col-sm-2 btn btn-primary">Save</a> -->
			                    <button onclick="confirm('<?php echo 'Do You want to Save the Changes'; ?>') ? $('#form-manufacturer').submit() : false;" type="button" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary">Save</button>
			                </div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
// $('.date').datetimepicker({
//   pickTime: false,
//   format: 'DD-MM-YYYY',
// });

 	$('#filter_parent_doctor').autocomplete({
        delay: 500,
        source: function(request, response) {
            if(request != ''){
             $('#filterParentId').val('');
                $.ajax({
                    url: 'index.php?route=catalog/medicine_transfer/autocompleteParentDoc&token=<?php echo $token; ?>&filter_doctor_code=' +  encodeURIComponent(request.term),
                    dataType: 'json',
                    success: function(json) {   
                        response($.map(json, function(item) {
                            return {
                                label: item.doctor_name,
                                value: item.doctor_name,
                                horse_id:item.id,
                                parent_id:item.parent_id,
                                is_parent:item.is_parent,
                            }
                        }));
                    }
                });
            }
        }, 
        select: function(event, ui) {
            $('#filter_parent_doctor').val(ui.item.value);
            $('#filterParentId').val(ui.item.horse_id);
            $('.dropdown-menu').hide();
            console.log(ui.item);
            if ((ui.item.is_parent == 0) && (ui.item.parent_id != 0)) {
            	$("#label_to_doc").show();
            	$("#div_to_doc").show();
            } else {
            	$("#label_to_doc").hide();
            	$("#div_to_doc").hide();
            }
            if(ui.item.horse_id != ''){
	            $.ajax({
	                url: 'index.php?route=catalog/medicine_transfer/autocompleteChildDoc&token=<?php echo $token; ?>&parent_horse_id=' +  encodeURIComponent(ui.item.horse_id),
	                dataType: 'json',
	                cache: false,
	                success: function(json) {
					    $('#input_to_doc').find('option').remove();
					    $('#input_to_doc').append($("<option disabled='disabled' selected='true'></option>").attr("value", '').text('Please Select'));
					    if(json){
					    	if (json.length <= 0) {
					    		$("#label_to_doc").hide();
				            	$("#div_to_doc").hide();
					    	} else {
					    		$("#label_to_doc").show();
				            	$("#div_to_doc").show();
					    	}
						    $.each(json, function (i, item) {
						      $('#input_to_doc').append($('<option>', { 
						        value: item.id,
						        text : item. doctor_name 
						      }));
						    });
					    }
	                }
	            }); 
	        }
	        $('#input_item_code').focus();
	        return false;
        },
    });

	$('#input_item_name').autocomplete({
	      delay: 500,
	      source: function(request, response) {
	          if(request != ''){
          		//$('#item_code').html('');
          		$('#input_item_code').val('');
          		$('#input_batch').val('');
          		$('#input_exp_date').val('');
	       		$('#input_qty').val('');
	       		$('#input_qty_hidden').val('');
	       		$('#input_purchase_prise').val('');
	       		$('#input_value').val('');
	              $.ajax({
	                  url: 'index.php?route=catalog/medicine/autocomplete&token=<?php echo $token; ?>&filter_medicine_name=' +  encodeURIComponent(request.term), 
	                  dataType: 'json',
	                  success: function(json) { 
	                  console.log(json); 
	                      response($.map(json, function(item) {
	                          return {
	                              label: item.med_name,
	                              value: item.med_name,
	                              med_code: item.med_code,
	                              normal: item.normal,
	                              purchase_price: item.purchase_price,
	                              final_price: item.final_price,
	                              expiry_date : item.expiry_date,
	                              
	                          }
	                      }));
	                  }
	              });
	          	}
	      	}, 
	      select: function(event, ui) {
	      	//console.log(ui.item.expiry_date);
          	$('#input_item_name').val(ui.item.value);
	       	$('#input_exp_date').val(ui.item.expiry_date);
	       	$('#input_item_code').val(ui.item.med_code);
	       	$('#input_qty').val('');
	        $('#input_qty_hidden').val(parseInt(ui.item.normal));
	        $('#input_value').val(ui.item.final_price);
	       	$('#input_purchase_prise').val(ui.item.purchase_price);
          	$('.dropdown-menu').hide();
          	$('#input_qty').focus();
			$('#input_qty').select()
	          return false;
	      },
	  });

	   	$("#input_qty").keyup(function()  {

	   		var input_valid =  $('#input_qty').val() ;
			var input_valid1 = input_valid.replace(/[^0-9]+/i, '');
			$("#input_qty").val(input_valid1);

			var quantity =  $('#input_qty').val() || 0;
			var input_purchase_prise =  $('#input_purchase_prise').val();
			var final_value = quantity * input_purchase_prise;
			$('#input_value').val(final_value);
		});

	//enter button

	//document.getElementById("input_qty").onkeypress = function(event){
	$(document).on('keypress', '.input-cls', function(){
	    if (event.keyCode == 13 || event.which == 13){
	    	input_idss = $(this).attr("id");
	    	if(input_idss == 'input_item_code') {
	    		item_code = $(this).val();
		    	//console.log(item_code);
		    	//return false;
		    	if(item_code != ''){
	    		 	$.ajax({
	                	url: 'index.php?route=catalog/medicine_transfer/medicineCodeSearch&token=<?php echo $token; ?>&filter_medicine_code='+item_code, 
		                dataType: 'json',
		                success: function(json) {
		                  	if(json.success == 1){
			                  	console.log(json);
			                  	$('#input_item_name').val(json.med_name);
						       	$('#input_item_code').val(json.med_code);
						       	$('#input_qty').val('');
						        $('#input_qty_hidden').val(parseInt(json.normal));
						        $('#input_value').val(json.final_price);
						       	$('#input_purchase_prise').val(json.purchase_price);
						       	$('#input_exp_date').val(json.expiry_date);
					          	$('.dropdown-menu').hide();
					          	$('#input_qty').focus();
					          	$('#input_qty').select()
			                  	return false;

			                } else {
			                	alert("Medicine Code Invalid");
			                	return false;
			                }
		                } 
	                  
	              	});
	    		} else {
	    			$('#input_item_name').focus();
	    		}
	    	} 
	    	 if(input_idss == 'input_qty'){
	    	 	//alert('inn');
		    	var qty = $('#input_qty').val();
		    	var extra_field = $('#tbladdMedicineTran >tbody >tr').length;
		    	if (qty > 0) {
			    		$('.imp_div').show();
					    var input_item_name = $('#input_item_name').val();
					 	var input_item_code = $('#input_item_code').val();
						var expiry_date = $('#input_exp_date').val();

					  	var input_qty = $('#input_qty').val();
					   	var input_value = $('#input_value').val();
					    var input_purchase_prise =   $('#input_purchase_prise').val();
					    
						html = '';
						html += '<tr id="productraw_row' + extra_field + '">';
							html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">'+input_item_code+'';
								html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_item_code]" value="'+input_item_code+'"  class="form-control po_qty_class" />';
							html += '</td>';

							html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">'+input_item_name+'';
								html += '<input type="hidden" name="productraw_datas['+extra_field+'][item_name]" value="'+input_item_name+'"  class="form-control po_qty_class" />';
							html += '</td>';

							html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">' + expiry_date + '';
								html += '<input type="hidden" name="productraw_datas['+extra_field+'][expiry_date]" value="'+expiry_date+'"  class="form-control po_qty_class" />';
							html += '</td>';

							html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allqtyTotals">'+ input_qty +'';
								html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_qty]" value="'+input_qty+'"  class="form-control po_qty_class" />';
								
							html += '</td>';
							

							html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allPurchaseTotals" style="display:none;">';
								html += '<input type="hidden"  value="'+input_purchase_prise+'"  class="form-control po_qty_class" />';
							html += '</td>'

							html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allValueTotals" style="display:none;">';
								html += '<input type="hidden"  value="'+input_value+'"  class="form-control allValueTotals_amt po_qty_class" />';
							html += '</td>'


							html += '<td class="text-left"><button onclick="remove_reco('+extra_field+')" class="btn btn-danger" id="remove'+extra_field+'" ><i class="fa fa-minus-circle"></i></button></td>';
						html += '</tr>';
					  	$('#tbladdMedicineTran tbody').append(html);
					  	$('#input_item_name').val('');
						$('#item_code').html('');
				  		$('#input_item_code').val('');
				  		$('#input_batch').val('');
				   		$('#input_qty').val('');
				   		$('#input_qty_hidden').val('');
						$('#input_exp_date').val('');

				   		$('#input_purchase_prise').val('');
				   		$('#input_value').val('');
				   		 $('#input_value').attr('value', '');
						
					  	extra_field++;

					    $('#tbladdMedicineTran').each(function (i, elem) {
					     	var totalqty = 0;
					     	var totalvalue = 0;
						    $(elem).find("td.allqtyTotals").each(function(j, elem2) {
						       totalqty += parseFloat($(elem2).html());
						    });
					     // 	$(elem).find("td.allValueTotals").each(function(j, elem2) {
					    	// console.log($(elem2).html());
					     //   		totalvalue += parseFloat($(elem2).html());
					     // 	});

					     $('.allValueTotals_amt').each(function(i,j){
					     	totalvalue +=parseFloat($(j).val());
					     	console.log(totalvalue);
					     });


					     	
					     	var count_item = $('#tbladdMedicineTran >tbody >tr').length;
					      	$('#total_item').val('');
					      	$('#total').val('');
					      	$('#total_qty').val('');

					      	$('#total_item').attr('value', count_item);
					      	$('#total').attr('value', totalvalue);
					      	$('#total_qty').attr('value', totalqty);
					       	$('#total_item').val(count_item);
					      	$('#total').val(totalvalue);
					      	$('#total_qty').val(totalqty);
		            		$('#input_item_code').focus();
					   	});
		    	} else {
					alert('Please Enter Quantity!');
					return false;
				}
			}
	    }
	});

function remove_reco(extra_field){
	$('#productraw_row'+extra_field).remove();
	$('#tbladdMedicineTran').each(function (i, elem) {
     var totalqty = 0;
     var totalvalue = 0;
     $(elem).find("td.allqtyTotals").each(function(j, elem2) {
       totalqty += parseFloat($(elem2).html());
     });
    
     $(elem).find("td.allValueTotals").each(function(j, elem2) {
       totalvalue += parseFloat($(elem2).html());
     })
     var count_item = $('#tbladdMedicineTran >tbody >tr').length;
      $('#total_item').val('');
      $('#total').val('');
      $('#total_qty').val('');

      $('#total_item').attr('value', count_item);
      $('#total').attr('value', totalvalue);
      $('#total_qty').attr('value', totalqty);
       $('#total_item').val(count_item);
      $('#total').val(totalvalue);
      $('#total_qty').val(totalqty);

   });
}

$('#add_medicine').click(function(){
	var qty = $('#input_qty').val();
	var extra_field = $('#tbladdMedicineTran >tbody >tr').length;
	if (qty > 0) {
		 $('.imp_div').show();
	    var input_item_name = $('#input_item_name').val();
	 	var input_item_code = $('#input_item_code').val();
	  	var input_qty = $('#input_qty').val();
	   	var input_value = $('#input_value').val();
	    var input_purchase_prise =   $('#input_purchase_prise').val();
	    var input_batch = $('#input_batch').val();

		html = '';
		html += '<tr id="productraw_row' + extra_field + '">';
			html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">';
				html += '<input type="text" name="productraw_datas['+extra_field+'][input_item_code]" value="'+input_item_code+'"  class="form-control po_qty_class" />';
			html += '</td>';

			html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">'+input_item_name+'';
				html += '<input type="hidden" name="productraw_datas['+extra_field+'][item_name]" value="'+input_item_name+'"  class="form-control po_qty_class" />';
			html += '</td>'

			html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">' + input_batch + '';
				html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_batch]" value="'+input_batch+'"  class="form-control po_qty_class" />';
			html += '</td>'

			html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allqtyTotals">'+ input_qty +'';
				html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_qty]" value="'+input_qty+'"  class="form-control po_qty_class" />';
			html += '</td>'

			html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allPurchaseTotals">' + input_purchase_prise + '';
				html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_purchase_prise]" value="'+input_purchase_prise+'"  class="form-control po_qty_class" />';
			html += '</td>'

			html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allValueTotals">' + input_value + '';
				html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_value]" value="'+input_value+'"  class="form-control po_qty_class" />';
			html += '</td>'
			html += '<td class="text-left"><button onclick="remove_reco('+extra_field+')" class="btn btn-danger" id="remove'+extra_field+'" ><i class="fa fa-minus-circle"></i></button></td>';
		html += '</tr>';
	  	$('#tbladdMedicineTran tbody').append(html);
	  	$('#input_item_name').val('');
		$('#item_code').html('');
  		$('#input_item_code').val('');
  		$('#input_batch').val('');
   		$('#input_qty').val('');
   		$('#input_qty_hidden').val('');
   		$('#input_purchase_prise').val('');
   		$('#input_value').val('');
		
	  	extra_field++;

	    $('#tbladdMedicineTran').each(function (i, elem) {
	     var totalqty = 0;
	     var totalvalue = 0;
	     $(elem).find("td.allqtyTotals").each(function(j, elem2) {
	       totalqty += parseFloat($(elem2).html());
	     });
	    
	     $(elem).find("td.allValueTotals").each(function(j, elem2) {
	       totalvalue += parseFloat($(elem2).html());
	     })
	     var count_item = $('#tbladdMedicineTran >tbody >tr').length;
	      $('#total_item').val('');
	      $('#total').val('');
	      $('#total_qty').val('');

	      $('#total_item').attr('value', count_item);
	      $('#total').attr('value', totalvalue);
	      $('#total_qty').attr('value', totalqty);
	       $('#total_item').val(count_item);
	      $('#total').val(totalvalue);
	      $('#total_qty').val(totalqty);

	   });
	} else {
		alert('Please Enter Quantity!');
		return false;
	}
});




$("input, textarea, select, checkbox").keypress(function(event) {
	if (event.which == 13) {
		event.preventDefault();
	}
});

$(".etr_evnt").keypress(function(event) {
	if (event.which == 13) {
		ids = $(this).attr('id');
		//alert(ids);
		if(ids == 'input_to_doc'){
			$('#input_item_code').focus();
		}
	}
});

$(document).on('keypress', '#input-code', function(){
    if (event.keyCode == 13 || event.which == 13){
    	var code = $("#input-code").val();
    	if (code != '') {
	    	$.ajax({
            	url: 'index.php?route=catalog/medicine_transfer/clinic_search&token=<?php echo $token; ?>&code='+code, 
                dataType: 'json',
                success: function(json) {
                	console.log(json);
                	if (json.alert != '') {
                		alert(json.alert);
                		$('#label_to_doc').hide();
            			$('#div_to_doc').hide();
            			$('#filter_parent_doctor').val('');
            			$('#filterParentId').val('');
            			$('#input_to_doc').val('');
            			return false;
                	} else {
	                  	if(json.success == 1){
		                	console.log('inn');
		                	console.log(json);
		                  	$('#filter_parent_doctor').val(json.parent);
					       	$('#filterParentId').val(json.parent_id);
		            		$('#label_to_doc').show();
		            		$('#div_to_doc').show();
					       	$('#input_to_doc').find('option').remove();
					       	$.each(json.docs, function (i, item) {
						    	$('#input_to_doc').append($('<option>', { 
						        	value: item.doctor_id,
						        	text : item. doctor_name 
						    	}));
						    });
						    $('#input_to_doc').focus();
		                  	return false;
		                } else {
		                	$('#filter_parent_doctor').val(json.parent);
					       	$('#filterParentId').val(json.parent_id);
		            		$('#label_to_doc').show();
		            		$('#div_to_doc').show();
		            		if((json.success == 3) || (json.success == 4)){
		            			$('#label_to_doc').hide();
		            			$('#div_to_doc').hide();
		            		}
		            		$('#input_to_doc').find('option').remove();
						    $('#input_to_doc').append($("<option selected='selected'></option>").attr("value", ""+json.doctor_id+"").text(""+json.doctor_name+""));
		                	$('#input_item_code').focus();
		                	return false;
		                }
                	}
                } 
              
          	});
    	} else {
    		alert("Please Enter Code!");
    	}
    }
});


</script>

<?php echo $footer; ?>