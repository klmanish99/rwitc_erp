<?php echo $header; ?><?php echo $column_left;  ?>

<div id="content">
  	<div class="page-header">
		<div class="container-fluid">
		  	<div class="pull-right">
		  		<?php if ($group_id == '12') { ?>
			  		<a style="display: none;" href="<?php echo $import_horse; ?>"style="background-color: #00628B;border-color: #00628B;" type="button" class="btn btn-danger">Import Horse From ISB</a>
			  		<a style="display: none;" href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
					<button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>
				<?php } else { ?>
					<a href="<?php echo $import_horse; ?>"style="background-color: #00628B;border-color: #00628B;" type="button" class="btn btn-danger">Import Horse From ISB</a>
			  		<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				<?php } ?>
		  	</div>
	  		<h1><?php echo $heading_title; ?></h1>
		  	<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
		  	</ul>
		</div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
		  	<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		  	</div>
		  	<div class="panel-body">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
				  	<div class="well">
			            <div class="row">
			            	<div class="col-sm-12">
			                <div class="form-group">
			                  	<div class="col-sm-3">
			                   		<input type="text" name="filterHorseName" id="filterHorseName" value="<?php echo $filter_hourse_name; ?>" placeholder="Horse Name" class="form-control">
			                  	</div>
			                  	<div style="display: none;" class="col-sm-2">
			                   		<input type="text" name="filterHorseId" id="filterHorseId" value="<?php echo $filter_hourse_id; ?>" placeholder="Horse Code" class="form-control">
			                  	</div>
			                  	<div class="col-sm-3">
			                   		<input type="text" name="filterTrainerName" id="filterTrainerName" value="<?php echo $filter_trainer_name; ?>" placeholder="Trainer Name" class="form-control">
			                  	</div>
			                  	<div class="col-sm-3">
                                    <select name="filter_status" id="filter_status" class="form-control">
                                        <?php foreach ($status as $key => $tvalue) { ?>
                                            <?php if($key == $filter_status){ ?>
                                              <option value="<?php echo $key ?>" selected="selected"><?php echo $tvalue?></option>
                                            <?php } else { ?>
                                              <option value="<?php echo $key ?>"><?php echo $tvalue?></option>
                                            <?php } ?>
                                        <?php } ?>    
                                    </select>
                                </div>
			                	<a onclick="filter();"  id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
			                </div>
			                </div>
			            </div>
			        </div>
				  	<div class="table-responsive">
						<table class="table table-bordered table-hover">
					  		<thead>
								<tr>
								  	<td style="display: none;" "width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
								  	<td class="text-left">
										<?php if ($sort == 'horseseq') { ?>
	                                        <a href="<?php echo $horseseq; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Sr. No'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $horseseq; ?>"><?php echo 'Sr. No'; ?></a>
	                                    <?php } ?>	


								  	</td>
								 	<td style="display: none;" class="text-left">
								 		<?php if ($sort == 'horse_code') { ?>
	                                        <a href="<?php echo $horse_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Horse Code'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $horse_code; ?>"><?php echo 'Horse Code'; ?></a>
	                                    <?php } ?>	
								 	</td>
									<td class="text-left">
									 	<?php if ($sort == 'official_name') { ?>
	                                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Horse Name'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $sort_name; ?>"><?php echo 'Horse Name'; ?></a>
	                                    <?php } ?>
	                                </td>

	                                <td class="text-left">
									 	<?php if ($sort == 'sex') { ?>
	                                        <a href="<?php echo $sex; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Color/Sex'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $sex; ?>"><?php echo 'Color/Sex'; ?></a>
	                                    <?php } ?>
	                                </td>
								  	

								  	<td class="text-left">
									 	<?php if ($sort == 'age') { ?>
	                                        <a href="<?php echo $age; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Age'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $age; ?>"><?php echo 'Age'; ?></a>
	                                    <?php } ?>
	                                </td>

	                                <td class="text-left">
									 	<?php if ($sort == 'rating') { ?>
	                                        <a href="<?php echo $rating; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rating'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $rating; ?>"><?php echo 'Rating'; ?></a>
	                                    <?php } ?>
	                                </td>

	                                <td class="text-left">
									 	<?php if ($sort == 'official_name') { ?>
	                                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Equipment Name'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $sort_name; ?>"><?php echo 'Equipment Name'; ?></a>
	                                    <?php } ?>
	                                </td>

	                                <td class="text-left">
									 	<?php if ($sort == 'official_name') { ?>
	                                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Shoe/Bit Name'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $sort_name; ?>"><?php echo 'Shoe/Bit Name'; ?></a>
	                                    <?php } ?>
	                                </td>

	                                <td class="text-left">
									 	<?php if ($sort == 'official_name') { ?>
	                                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Ban'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $sort_name; ?>"><?php echo 'Ban'; ?></a>
	                                    <?php } ?>
	                                </td>

	                                <td class="text-left">
									 	<?php if ($sort == 'official_name') { ?>
	                                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Trainer Name'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $sort_name; ?>"><?php echo 'Trainer Name'; ?></a>
	                                    <?php } ?>
	                                </td>

	                                <td class="text-left">
									 	<?php if ($sort == 'status') { ?>
	                                        <a href="<?php echo $status; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Status'; ?></a>
	                                    <?php } else { ?>
	                                        <a href="<?php echo $status; ?>"><?php echo 'Status'; ?></a>
	                                    <?php } ?>
	                                </td>
								  	
								  	<td class="text-left"><?php echo $column_action; ?></td>
								</tr>
							</thead>
					  		<tbody>
								<?php if ($horseDatas) { ?>
									<?php $i=1; ?>
									<?php foreach ($horseDatas as $horseData) { ?>
										<tr>
								  			<td style="display: none;" class="text-center"><?php if (in_array($horseData['horseID'], $selected)) { ?>
											<input type="checkbox" name="selected[]" value="<?php echo $horseData['horseID']; ?>" checked="checked" />
												<?php } else { ?>
											<input type="checkbox" name="selected[]" value="<?php echo $horseData['horseID']; ?>" />
											<?php } ?></td>
											<td class="text-left"><?php echo $i++; ?></td>
											<td style="display: none;" class="text-left"><?php echo $horseData['horseID']; ?></td>
										  	<td style="text-transform: uppercase;" class="text-left"><?php echo $horseData['horseName']; ?></td>
										  	<td class="text-left"><?php echo $horseData['horseColor']; ?></td>
										  	<td class="text-left"><?php echo $horseData['horseAge']; ?></td>
										  	<td class="text-left"><?php echo $horseData['horseRating']; ?></td>
										  	<td class="text-left"><?php echo $horseData['equipment_name']; ?></td>
										  	<td class="text-left"><?php echo $horseData['shoe_name']; ?></td>
										  	<td class="text-left"><?php echo $horseData['ban']; ?></td>
										  	<td class="text-left"><?php echo $horseData['trainer_name']; ?></td>
										  	<?php  if($horseData['horseRegDate'] != '1970-01-01' && $horseData['horseRegDate'] != '0000-00-00'){
														$horeregdate = date('d-m-Y', strtotime($horseData['horseRegDate']));
													} else {
														$horeregdate  = '';
													} ?>
										  	<?php  $Status = ($horseData['horse_status'] == 1) ? 'In' : (($horseData['horse_status'] == 0) ? 'Exit' : 'Temp-Horse'); ?>
										  	<!-- <?php if ($horseData['horse_status'] == '1') { ?>
                                            <td class="text-left">Active</td>
                                            <?php } else { ?>
                                                <td class="text-left">In-Active</td>
                                            <?php } ?> -->
                                            <td class="text-left"><?php echo $Status; ?></td>
										  	<td style="width: 10%; " class="col-sm-2">
										  		<?php if ($group_id == '12') { ?>
											  		<a href="<?php echo $horseData['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a> 
										  		<?php } else{ ?>
										  			<a href="<?php echo $horseData['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>  <a href="<?php echo $horseData['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
										  		<?php } ?>
										  	</td>
										</tr>
									<?php } ?>
								<?php } else { ?>
									<tr>
									  <td class="text-center" colspan="13"><?php echo $text_no_results; ?></td>
									</tr>
								<?php } ?>
					  		</tbody>
						</table>
				  	</div>
				</form>
				<div class="row">
				  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
				  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
		  	</div>
		</div>
  	</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
	function filter() { 
		var filter_hourse_name = $('#filterHorseName').val();
  		var filter_hourse_id = $('#filterHorseId').val();
  		var filter_trainer_name = $('#filterTrainerName').val();
  		var filter_status = $('select[name=\'filter_status\']').val();
  		if(filter_hourse_name != '' || filter_hourse_id != '' || filter_trainer_name != '' || filter_status != ''){
  			url = 'index.php?route=catalog/horse&token=<?php echo $token; ?>';
			if (filter_hourse_name) {
			  url += '&filter_hourse_name=' + encodeURIComponent(filter_hourse_name);
			}

			if (filter_hourse_id) {
			  url += '&filter_hourse_id=' + encodeURIComponent(filter_hourse_id);
			}

			if (filter_trainer_name) {
				url += '&filter_trainer_name=' + encodeURIComponent(filter_trainer_name);
			} 
			if (filter_status) {
                url += '&filter_status=' + encodeURIComponent(filter_status);
            }
			window.location.href = url;
  		} else {
			alert('Please select the filters');
			return false;
  		}
	}

	$('#filterHorseName').autocomplete({
    delay: 500,
    source: function(request, response) {
        $('#trainer_id').val('');
        if(request != ''){
            $.ajax({
                url: 'index.php?route=catalog/horse/autocompleteHorse&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {   
                    $('#trainer_codes_id').find('option').remove();
                    response($.map(json, function(item) {
                        return {
                            label: item.trainer_name,
                            value: item.trainer_name,
                            trainer_id:item.trainer_id,
                        }
                    }));
                }
            });
        }
    }, 
    select: function(item) {
        console.log(item);
        $('#filterHorseName').val(item.value);
        $('#trainer_id').val(item.trainer_id);
        $('.dropdown-menu').hide();
        filter();
        return false;
    },
	});
	$('#filterHorseId').keydown(function(e) {
	  if (e.keyCode == 13) {
	    filter();
	  }
	});
	$('#filterTrainerName').keydown(function(e) {
	  if (e.keyCode == 13) {
	    filter();
	  }
	});
</script>