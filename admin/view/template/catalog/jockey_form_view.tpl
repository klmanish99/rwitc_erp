<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
                </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="col-sm-2 panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3></b>
                <?php if ($jockey_id) { ?>
                    <div style="padding-left: 40%;">
                        <b><label style="font-family: cursive;color: #00a04d;font-size: 20px;border: none;background: #fcfcfc;width: 80%;text-transform: uppercase;"><?php echo $jockey_name; ?></label></b>
                        <b>Jockey Code : <?php echo $jockey_code; ?> </b>
                    </div>
                <?php } else { ?>
                    <div style="padding-left: 34%;" class="">
                        <b><input style="border: none;background: #fcfcfc;width: 95%;" disabled="disabled" id="name1" value="" name=""></b>
                    </div>
                <?php } ?>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-contact_tax_details" data-toggle="tab"><?php echo $tab_contact_tax_details; ?></a></li>
                        <?php  if($jockey_id !=''){ ?>
                        <li style="display: none;"><a href="#tab-race_record" data-toggle="tab"><?php echo $tab_race_record; ?></a></li>

                        <li><a href="#tab-ban_details" data-toggle="tab"><?php echo $tab_ban_details; ?></a></li>
                        <li><a href="#tab_transaction" data-toggle="tab">Transaction</a></li>
                       <?php  } ?>

                    </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="jockey_code"><b style="color: red">*</b>Jockey code :</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $jockey_code_new; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="racing_name">Profile Pic:</label>
                                <div class="col-sm-5">
                                    <?php if($img_path != "#") { //echo $img_path;exit; ?>
                                        <span id="profile_pic" class="col-sm-8" ><img src="<?php echo $img_path; ?>" height="60" id="blah" alt=""  /></span>
                                    <?php } else { ?>
                                        <span id="profile_pic" class="col-sm-8" ><img src="<?php echo $unknown_pic; ?>" height="60" id="blah" alt=""  /></span>
                                    <?php } ?>
                                </div>

                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="racing_name"><?php echo "Racing Name :"; ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $racing_name; ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="jockey_name"><b style="color: red">*</b>Jockey Name</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $jockey_name; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="jai_member"><?php echo "JAI Member:"; ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $jaimember; ?>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="Fathers_name"><b style="color: red">*</b>Fathers Name</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $Fathers_name; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="educational_qualification"><b style="color: red">*</b>Educational Qualification</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $educational_qualification; ?>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="emergency_contact_person"><b style="color: red">*</b>Emergency Contact Person</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $emergency_contact_person; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="contact_no"><b style="color: red">*</b>Contact No</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $contact_no; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-date_of_birth"><?php echo "Birth Date"; ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $birth_date; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="origin"><?php echo "Origin:"; ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $origin; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-license_issued_date"><?php echo "License Issued On(First Time)"; ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $first_time_license_issued; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">License Type(A/B):</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $license_type; ?>
                                </div>
                                <div  id="center_holding_hide" style="display: none;">
                                    <label class="col-sm-2 control-label">Center Holding License Type A:</label>
                                    <div style="margin-top: 10px;" class="col-sm-3">
                                        <?php echo $center_holding_license_type_a; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="input-license_renewal"><?php echo "License Renewal date"; ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $license_renewal_date; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="license_end_date"><?php echo "License End date"; ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $license_end_date; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="ride_weight"><?php echo "Ride Weight:"; ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $ride_weight; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="input-file_number"><?php echo "File Number:"; ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $file_number; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="blood_group"><?php echo "Blood Group :" ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $blood_group; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="input-file"><?php echo "" ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $file_upload; ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="no_whip"><?php echo "No Whip:" ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $no_whip; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="apprentice"><?php echo "Apprentice:" ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $apprentice; ?>
                                </div>
                                <?php if ($apprentice == 'YES') { ?>
                                    <label style="display: none;" id="allowance_claiming_l" class="col-sm-1 control-label" for="allowance_claiming">Allowance claiming:</label>
                                    <div style="margin-top: 10px;" class="col-sm-3">
                                        <?php echo $allowance_claiming; ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="out_station_wins">Out Station Wins :</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $out_station_wins; ?>
                                </div>

                                <label hidden="hidden" class="col-sm-2 control-label" for="station_wins">Station Wins :</label>
                                <div  hidden="hidden" class="col-sm-2">
                                    <input type="text" name="station_wins" value="<?php echo $station_wins ; ?>" placeholder="<?php echo "Station Wins"; ?>" id="station_wins" class="form-control" onkeyup="total();" tabindex=""/>
                                </div>

                                <label class="col-sm-2 control-label" for="total_wins">Total Wins :</label>
                                <div style="margin-top: 10px;" class="col-sm-2">
                                    <?php echo $total_wins; ?>
                                </div>
                            </div>
                                <?php  $i=0;
                                if (isset($station_name) && $station_name != '') {
                                    foreach ($station_name as $key => $value) { ?>
                                        <div  class="form-group">
                                            <label class='col-sm-2 control-label'>Horse<?php echo $i ?>:</label>
                                            <div class="col-sm-4">
                                                <input style="border: none;" type="" value="<?php echo $value['out_station_name']; ?>">
                                            </div>
                                        </div>
                                        <?php $i++;
                                    }
                                } ?>
                            <div class="form-group" style="display: none;" id="div_master_trainer">
                                <label class="col-sm-2 control-label" for="master_trainer"><?php echo "Master Trainer :" ?></label>
                                <div style="margin-top: 10px;" class="col-sm-2">
                                    <?php echo $master_trainer; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="master_trainer"><?php echo "Trainer History :" ?></label>
                                <div style="margin-top: 10px;" class="col-sm-2">
                                    <?php echo $trainer_history; ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="input-jockey_returned_by"><?php echo "Jockey retain by:" ?></label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $jockey_returned_by; ?>
                                </div>
                                <div class="col-sm-3 owner_data-div">
                                    <?php  $cnt_total = 0;
                                     if($private_trainers_owner_data){
                                        foreach($private_trainers_owner_data as $key => $value){ ?>
                                            <div class="form-group">
                                                <div style="" class="col-sm-7">
                                                    <?php echo $value['owner_name']; ?>
                                                </div>
                                            </div>
                                        <?php $cnt_total++; }
                                    } ?>

                                </div>
                                <input type="hidden" value="<?php echo $cnt_total; ?>" class="owners_cnt" id="owners_cnt">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="select-isActive">Status</label>
                                <?php if ($isActive == '1') { ?>
                                    <div style="margin-top: 10px;" class="col-sm-3">
                                        In
                                    </div>
                                <?php } else { ?>
                                    <div style="margin-top: 10px;" class="col-sm-3">
                                        Exit
                                    </div>
                                <?php } ?>
                                <label class="col-sm-2 control-label" for="remarks"><?php echo "Remark:" ?></label>
                                <div style="margin-top: 10px;" class="col-sm-5">
                                    <?php echo $remarks; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="remarks"><?php echo "Last Update:" ?></label>
                                <div style="margin-top: 10px;" class="col-sm-10">
                                    <?php echo $last_upadate; ?> BY <?php echo $user; ?> 
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-contact_tax_details">
                            <div class="form-group">
                                <label style="" class="col-sm-6 control-label alignLeft" for="input-address1"> Address 1</label>
                                <label class="col-sm-6 control-label alignLeft" for="input-address2">Address 2</label>
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $address_1; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $address_2; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $address_3; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $address_4; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label style="" class="col-sm-3 control-label alignLeft" for="input-address2">Locality area or street 1</label>
                                <label style="" class="col-sm-3 control-label alignLeft" for="input-address1"> City 1</label>
                                <label style="" class="col-sm-3 control-label alignLeft" for="input-address2">Locality area or street 2</label>
                                <label style="" class="col-sm-3 control-label alignLeft" for="input-address2">City 2</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $local_area_1; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $city_1; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $local_area_2; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $city_2; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label style="" class="col-sm-3 control-label alignLeft" for="input-address1"> Country 1</label>
                                    <label class="col-sm-3 control-label alignLeft" for="input-address2">State 1</label>
                                    <label class="col-sm-3 control-label alignLeft" for="input-address2">Country 2</label>
                                    <label class="col-sm-3 control-label alignLeft" for="input-address2">State 2</label>
                                <div class="col-sm-3">
                                  <select disabled="disabled" style="color: black;border: white;background: white;-webkit-appearance: none;" name="country1" data-index="19" id="input-country" onchange="country(this);" class="">
                                    <option value="">Country</option>
                                    <?php foreach ($countries as $country) { ?>
                                    <?php if ($country['country_id'] == $country_1) { ?>
                                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>

                                <div class="col-sm-3">
                                  <select disabled="disabled" style="color: black;border: white;background: white;-webkit-appearance: none;" name="zone_id" data-index="20" id="zone_id" class="">
                                    <option value="">State</option>
                                    <?php foreach ($zones as $zone) { ?>
                                    <?php if ($zone['zone_id'] == $zone_id) { ?>
                                    <option value="<?php echo $zone['zone_id']; ?>" selected="selected"><?php echo $zone['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $zone_id; ?>"><?php echo $zone['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>


                                <div class="col-sm-3">
                                  <select disabled="disabled" style="color: black;border: white;background: white;-webkit-appearance: none;" name="country2" data-index="25" id="input-country2" onchange="countrys(this);" class="">
                                    <option value="">Country</option>
                                    <?php foreach ($countries as $country2) { ?>
                                    <?php if ($country2['country_id'] == $country_2) { ?>
                                    <option value="<?php echo $country2['country_id']; ?>" selected="selected"><?php echo $country2['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $country2['country_id']; ?>"><?php echo $country2['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>

                                <div class="col-sm-3">
                                  <select disabled="disabled" style="color: black;width: 100px;border: white;background: white;-webkit-appearance: none;" name="zone_id2" data-index="26" id="zone_id2" class="">
                                    <option value="0">State</option>
                                    <?php foreach ($zones2 as $zone2) { ?>
                                    <?php if ($zone2['zone_id'] == $zone_id2) { ?>
                                    <option value="<?php echo $zone2['zone_id']; ?>" selected="selected"><?php echo $zone2['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $zone2['zone_id']; ?>"><?php echo $zone2['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label style="" class="col-sm-6 control-label alignLeft" for="input-address1"> Pincode 1</label>
                                    <label class="col-sm-6 control-label alignLeft" for="input-address2">Pincode 2</label>
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $pincode_1; ?>
                                </div>
                                <div style="margin-top: 10px;" class="col-sm-6">
                                    <?php echo $pincode_2; ?>
                                </div>
                            </div>
                            <div class="form-group">
                            <h4 style="margin-left: 15px;" >Contact Details</h4>
                                <label class="col-sm-2 control-label" for="phone_no">Phone No</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $phone_no; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mobile_no1"> Mobile No 1</label>
                                <div style="margin-top: 10px;" class="col-sm-4">
                                    <?php echo $mobile_no; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="altMobileNo">Alternate Mobile No</label>
                                <div style="margin-top: 10px;" class="col-sm-4">
                                    <?php echo $alternate_mob_no; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="emailId"> Email id</label>
                                <div style="margin-top: 10px;" class="col-sm-4">
                                    <?php echo $email_id; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="altEmailId">Alternate Email id</label>
                                <div style="margin-top: 10px;" class="col-sm-4">
                                    <?php echo $alternate_email_id; ?>
                                </div>
                            </div>
                            <div class="form-group">
                            <h4 style="margin-left: 15px;" >Tax Details</h4>
                                <label class="col-sm-2 control-label" for="gst_type"> GST Type</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $gst_type; ?>
                                </div>
                                <?php if($gst_type == 'R'){ ?>
                                    <label class="col-sm-2 control-label" id="gst_label" for="gst_no">GST No</label>
                                    <div style="margin-top: 10px;" class="col-sm-3">
                                        <?php echo $gst_no; ?>
                                    </div>

                                <?php } else { ?>
                                    <label id="gst_label" class="col-sm-2 control-label" for="gst_no">GST No</label>
                                    <div style="margin-top: 10px;" class="col-sm-3">
                                        <?php echo $gst_no; ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="pan_no">PAN Number</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $pan_no; ?>
                                </div>
                                <label class="col-sm-2 control-label" for="prof_tax_no">Prof Tax No details</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $prof_tax_no; ?>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label class="col-sm-2 control-label" for="prof_tax_no">EPF No</label>
                                <div style="margin-top: 10px;" class="col-sm-3">
                                    <?php echo $epf_no; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden"  name="upload_hidden_id" id="upload_hidden_id" value="<?php echo $upload_hidden_id; ?>">
                                <table id="itrUpload" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr><td colspan="3" >ITR Upload</td></tr>
                                        <tr>
                                            <td class="text-left">Assesment Year</td>
                                            <td class="text-left">File Name</td>
                                        </tr>
                                    </thead>
                                    <tbody id="upload_itr">
                                        <?php //echo'<pre>';print_r($juploaditr);  ?>
                                            <?php foreach($juploaditr as $key => $result) { ?>
                                                <tr id="recurring-row<?php echo $key; ?>">
                                                    <td>
                                                        <div style="margin-top: 10px;" class="col-sm-3">
                                                            <?php echo $result['assesment_year']; ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div style="margin-top: 10px;" class="col-sm-3">
                                                            <?php echo $result['file']; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                        <?php } //exit; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-race_record">

                        </div>
                        <div class="tab-pane" id="tab-ban_details">
                            <div class="col-sm-11">
                                <h4>Jockey Ban Details</h4>
                            </div>
                            <div style="float: right;padding-bottom: 10px; display: none;">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2"  onclick="closeaddbuu1()"><i class="fa fa-plus-circle"></i></button>
                            </div>
                            <div class="col-sm-1" >
                                <input type="hidden" name="id_hidden_band" value="<?php echo $id_hidden_band ?>" id="id_hidden_band" class="form-control" />
                                <div id="myModal2" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content" style="width: 107%;" >
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Ban</h4>
                                            </div>
                                            <div class="modal-body" style="margin-left: 24px;margin-right: 24px;">
                                                <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-club"><b style="color: red">*</b><?php echo "Club:"; ?></label>
                                                    <div class="col-sm-8">
                                                        <!-- <input type="text" name="club" value="" placeholder="<?php echo "Club"; ?>" id="input-club" class="form-control" /> -->
                                                        <select  name="club" placeholder="Club" id="input-club" class="form-control">
                                                            <option value="" selected="selected" disabled="disabled" >Please Select</option>
                                                            <?php foreach ($jockey_clubs as $ckey => $cvalue) { ?>
                                                            <option value="<?php echo $cvalue; ?>" ><?php echo $cvalue ?></option>
                                                            <?php } ?>
                                                      </select>
                                                      <span style="display: none;color: red;font-weight: bold;" id="error_club_ban" class="error"><?php echo 'Please Select Club'; ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Start Date:"; ?></label>
                                                    <div class="col-sm-8">
                                                    <div class="input-group date">
                                                            <input type="text" name="date_start_date_ban" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_start_date_ban" class="form-control" />
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                            <span style="display: none;color: red;font-weight: bold;" id="error_date_start_date_ban" class="error"><?php echo 'Please Enter Valid Date'; ?></span>
                                                    </div>
                                                        <!-- <input type="Number" name="date_start_date_ban"  placeholder="DD" id="date_start_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_date_start_date_ban" class="error"><?php echo 'Please Enter Valid Date'; ?></span>
                                                    </div> -->
                                                        
                                                    <!-- <div class="col-sm-2">
                                                        <input type="Number" name="month_start_date_ban"  placeholder="MM" id="month_start_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_month_start_date_ban" class="error"><?php echo 'Please Enter Valid Month'; ?></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="Number" name="year_start_date_ban"  placeholder="YYYYY" id="year_start_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_year_start_date_ban" class="error"><?php echo 'Please Enter Valid Year'; ?></span>
                                                    </div> -->
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-horse"><?php echo "End Date:"; ?></label>
                                                    <div class="col-sm-8">
                                                         <div class="input-group date">
                                                            <input type="text" name="date_end_date_ban" value="" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="date_end_date_ban" class="form-control" />
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_greater_date_start_date_ban" class="error"><?php echo 'End Date must not be less than Start Date!'; ?></span>
                                                    </div>
                                                        <!-- <input type="Number" name="date_end_date_ban"  placeholder="DD" id="date_end_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_date_end_date_ban" class="error"><?php echo 'Please Enter Valid Date'; ?></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="Number" name="month_end_date_ban"  placeholder="MM" id="month_end_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_month_end_date_ban" class="error"><?php echo 'Please Enter Valid Month'; ?></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="Number" name="year_end_date_ban"  placeholder="YYYYY" id="year_end_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_year_end_date_ban" class="error"><?php echo 'Please Enter Valid Year'; ?></span>
                                                    </div> -->
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-horse"><?php echo "Start Date:"; ?></label>
                                                    <div class="col-sm-8">
                                                     <div class="input-group date">
                                                            <input type="text" name="date_start_date_ban2" value="" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="date_start_date_ban2" class="form-control" />
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                        <!-- <input type="Number" name="date_start_date_ban2"  placeholder="DD" id="date_start_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_date_start_date_ban2" class="error"><?php echo 'Please Enter Valid Date'; ?></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="Number" name="month_start_date_ban2"  placeholder="MM" id="month_start_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_month_start_date_ban2" class="error"><?php echo 'Please Enter Valid Month'; ?></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="Number" name="year_start_date_ban2"  placeholder="YYYYY" id="year_start_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_year_start_date_ban2" class="error"><?php echo 'Please Enter Valid Year'; ?></span>
                                                    </div> -->
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-horse"><?php echo "End Date:"; ?></label>
                                                    <div class="col-sm-8">
                                                    <div class="input-group date">
                                                            <input type="text" name="date_end_date_ban2" value="" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="date_end_date_ban2" class="form-control" />
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_greater_date_start_date_ban2" class="error"><?php echo 'End Date must not be less than Start Date!'; ?></span>
                                                    </div>
                                                        <!-- <input type="Number" name="date_end_date_ban2"  placeholder="DD" id="date_end_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_date_end_date_ban2" class="error"><?php echo 'Please Enter Valid Date'; ?></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="Number" name="month_end_date_ban2"  placeholder="MM" id="month_end_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_month_end_date_ban2" class="error"><?php echo 'Please Enter Valid Month'; ?></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="Number" name="year_end_date_ban2"  placeholder="YYYYY" id="year_end_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_year_end_date_ban2" class="error"><?php echo 'Please Enter Valid Year'; ?></span>
                                                    </div> -->
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="amount_ban"><?php echo "Amount:"; ?></label>
                                                    <div class="col-sm-8">
                                                        
                                                        <input type="number" name="amount_ban" value="" placeholder="<?php echo "Amount"; ?>" id="amount_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_amount_ban" class="error"><?php echo 'Please Enter Amount'; ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Authority:"; ?></label>
                                                    <div class="col-sm-8">
                                                        <!-- <input type="text" name="authority_ban_details" value="" placeholder="<?php echo "Authority"; ?>" id="authority_ban_details" class="form-control" /> -->
                                                        <select name="authority_ban_details" id="authority_ban_details" class="form-control">
                                                            <option value="" selected="selected" disabled="disabled" >Please Select</option>    
                                                            <?php foreach ($authorty_bans as $authorutykey => $authorutyvalue) { ?>
                                                            <option value="<?php echo $authorutyvalue; ?>" ><?php echo $authorutyvalue ?></option>
                                                            <?php } ?>
                                                      </select> 
                                                      <span style="display: none;color: red;font-weight: bold;" id="error_authority_ban" class="error"><?php echo 'Please Select Authority'; ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Reason:"; ?></label>
                                                    <div class="col-sm-8">
                                                        <textarea rows="2" name="reason_description_ban" placeholder="" id="reason_ban" class="form-control"></textarea>
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_reason_ban" class="error"><?php echo 'Please Enter Reason'; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="modal-footer">
                                            <input type="hidden" name="id_hidden_BanFunction" value="" id="id_hidden_BanFunction" class="form-control" />
                                            <input type="hidden" name="idban_increment_id" value="" id="idban_increment_id" class="form-control" />
                                            <button type="button" class="btn btn-primary" id = "ban_save_id" onclick="BanFunction()"  >Save</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                          <td class="text-center"><?php echo "Club"; ?></td>
                                          <td class="text-center"><?php echo "Start Date"; ?></td>
                                          <td class="text-center"><?php echo "End Date"; ?></td>
                                          <td class="text-center"><?php echo "Start Date"; ?></td>
                                          <td class="text-center"><?php echo "End Date"; ?></td>
                                          <td class="text-center"><?php echo "Authority"; ?></td>
                                          <td class="text-center"><?php echo "Reason"; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody id="bandeatilsbody">
                                        <?php if(isset($bandeatils)) { //echo'<pre>';print_r($bandeatils); exit; ?>
                                            <?php foreach($bandeatils as $bankey => $banvalue) { 
                                                if($banvalue['end_date1'] == '' || $banvalue['end_date1'] == '0000-00-00'){
                                                    $enddatebans = ''; 
                                                } else {
                                                    $enddatebans = date('d/m/Y', strtotime($banvalue['end_date1']));
                                                } 
                                                if($banvalue['start_date1'] == '' || $banvalue['start_date1'] == '0000-00-00'){
                                                    $startdatebans = '';
                                                } else {
                                                    $startdatebans = date('d/m/Y', strtotime($banvalue['start_date1']));
                                                }
                                                if($banvalue['end_date2'] == '' || $banvalue['end_date2'] == '0000-00-00'){
                                                    $enddatebans2 = ''; 
                                                } else {
                                                    $enddatebans2 = date('d/m/Y', strtotime($banvalue['end_date2']));
                                                } 
                                                if($banvalue['start_date2'] == '' || $banvalue['start_date2'] == '0000-00-00'){
                                                    $startdatebans2 = ''; 
                                                } else {
                                                    $startdatebans2 = date('d/m/Y', strtotime($banvalue['start_date2']));
                                                } 
                                                /*if($banvalue['start_date2'] == '' || $banvalue['start_date2'] == '0000-00-00'){
                                                    $startdatebans2 = '';       
                                                } else {
                                                    $startdatebans2 = date('d/m/Y', strtotime($banvalue['start_date2']));
                                                }*/
                                            ?>
                                                <tr id='bandetail_<?php echo $bankey ?>'>
                                                    <td class="text-left"><span id="clubs_<?php echo $bankey ?>" ><?php echo $banvalue['club'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][club]" id="club_<?php echo $bankey ?>"  value="<?php echo $banvalue['club'] ?>">
                                                    </td>
                                                    <td class="text-right"><span id="start_dateban_<?php echo $bankey ?>"><?php echo $startdatebans; ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][start_date1]" id="start_date_bans_<?php echo $bankey ?>"  value="<?php echo $startdatebans; ?>">
                                                    </td>
                                                    <td class="text-right"><span id="end_dateban_<?php echo $bankey ?>"><?php echo $enddatebans; ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][end_date1]" id="end_date_ban_<?php echo $bankey ?>"  value="<?php echo $enddatebans; ?>">
                                                    </td>
                                                    <td class="text-right"><span id="start_dateban2_<?php echo $bankey ?>"><?php echo $startdatebans2; ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][start_date2]" id="start_date_bans2_<?php echo $bankey ?>"  value="<?php echo $startdatebans2; ?>">
                                                    </td>
                                                     <td class="text-right"><span id="end_dateban2_<?php echo $bankey ?>"><?php echo $enddatebans2; ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][end_date2]" id="end_date_ban2_<?php echo $bankey ?>"  value="<?php echo $enddatebans2; ?>">
                                                    </td>
                                                    <td class="text-left"><span id="authorityban_<?php echo $bankey ?>" ><?php echo $banvalue['authority'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][authority]" id="authority_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['authority'] ?>">
                                                    </td>
                                                    <td class="text-left"><span  id="reasonban_<?php echo $bankey ?>"><?php echo $banvalue['reason'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][reason]" id="reason_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['reason'] ?>">
                                                        <!-- <span  id="amountban_<?php echo $bankey ?>"><?php echo $banvalue['amount'] ?></span> -->
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][amount]" id="amount_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['amount'] ?>">
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-11">
                                <h4>Jockey Ban History</h4>
                            </div>
                            <div class="form-group">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                          <td class="text-center" ><?php echo "Club"; ?></td>
                                          <td class="text-center" ><?php echo "Start Date"; ?></td>
                                          <td class="text-center" ><?php echo "End Date"; ?></td>
                                          <td class="text-center" ><?php echo "Authority"; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody id="banhistorybody">
                                        <?php if(isset($bandatas)) { //echo'<pre>';print_r($bandatas); exit(); ?>
                                            <?php foreach($bandatas as $bkey => $bvalue) { 
                                                if($bvalue['end_date1'] == '' || $bvalue['end_date1'] == '0000-00-00'){
                                                    $enddatebanshistory = ''; 
                                                } else {
                                                    $enddatebanshistory = date('d/m/Y', strtotime($bvalue['end_date1']));
                                                } 
                                                if($bvalue['start_date1'] == '' || $bvalue['start_date1'] == '0000-00-00'){
                                                    $startdatebanshistory = '';
                                                } else {
                                                    $startdatebanshistory = date('d/m/Y', strtotime($bvalue['start_date1']));
                                                }
                                                if($bvalue['end_date2'] == '' || $bvalue['end_date2'] == '0000-00-00'){
                                                    $enddatebanshistory2 = ''; 
                                                } else {
                                                    $enddatebanshistory2 = date('d/m/Y', strtotime($bvalue['end_date2']));
                                                }

                                                if($bvalue['start_date2'] == '' || $bvalue['start_date2'] == '0000-00-00'){
                                                    $startdatebanshistory2 = ''; 
                                                } else {
                                                    $startdatebanshistory2 = date('d/m/Y', strtotime($bvalue['start_date2']));
                                                }

                                               ?>
                                                <tr id='banhistorybody<?php echo $bkey ?>'>
                                                    <td class="text-left" > 
                                                        <?php echo $bvalue['club'] ?>
                                                        <input type= "hidden" name="banhistorydatas[<?php echo $bkey ?>][history_club]" id="history_club_<?php echo $bkey ?>" value="<?php echo $bvalue['club'] ?>" >
                                                    </td>
                                                    <td class="text-right" >
                                                        <?php echo date('d/m/Y', strtotime($bvalue['start_date1'])); ?>
                                                        <input type= "hidden" name="banhistorydatas[<?php echo $bkey ?>][history_startdate_ban]" id="history_startdate_ban_<?php echo $bkey ?>"  value="<?php echo date('d/m/Y', strtotime($startdatebanshistory)); ?>" >
                                                    </td>
                                                    <td class="text-right" >
                                                        <?php echo date('d/m/Y', strtotime($bvalue['end_date1'])); ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_enddate_ban]" id="history_enddate_ban_<?php echo $bkey ?>"  value="<?php echo date('d/m/Y', strtotime($enddatebanshistory)); ?>" >
                                                    </td>
                                                    <td class="text-left" >
                                                        <?php echo $bvalue['authority']; ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_authority]" id="history_authority_<?php echo $bkey ?>"  value="<?php echo $bvalue['authority']; ?>" >
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_startdate_ban2]" id="history_startdate_ban2_<?php echo $bkey ?>"  value="<?php echo $startdatebanshistory2; ?>" >

                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_enddate_ban2]" id="history_enddate_ban2_<?php echo $bkey ?>"  value="<?php echo $enddatebanshistory2; ?>" >

                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_reason]" id="history_reason_<?php echo $bkey ?>"  value="<?php echo $bvalue['reason']; ?>" >

                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_amount]" id="history_amount_<?php echo $bkey ?>"  value="<?php echo $bvalue['amount']; ?>" >
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="tab-pane" id="tab_transaction">
                            <div class="col-sm-11">
                                <h4>Transaction</h4>
                            </div>
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td>Sr No</td>
                                                <td>License Renewal date</td>
                                                <td>Amount</td>
                                                <td>Fee Paid Type</td>
                                                <td>License Start Date</td>
                                                <td>License End Date</td>
                                                <td>License Type</td>
                                                <td>Apprenties</td>
                                                <input type= "hidden" name="" id="trainer_idss"  value="<?php echo $trainer_idss ?>">
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($Trainers)) { ?>
                                                <?php $i=1; ?>
                                                <?php foreach ($Trainers as $result) { ?>
                                                    <tr>
                                                        <td class="text-left"><?php echo $i++; ?></td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo date('d-m-Y', strtotime($result['renewal_date'])) ?>">
                                                        </td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['amount'] ?>">
                                                        </td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['fees'] ?>">
                                                        </td>
                                                         <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo date('d-m-Y', strtotime($result['license_start_date'])) ?>">
                                                        </td>
                                                         <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo date('d-m-Y', strtotime($result['license_end_date'])) ?>">
                                                        </td>
                                                         <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['license_type'] ?>">
                                                        </td>
                                                        <td class="text-center">
                                                            <?php if($result['apprenties'] == 'Yes'){ ?>
                                                                <i class="fa fa-check" aria-hidden="true" style="color: green;font-size: 20px;"></i> 
                                                            <?php } else { ?>
                                                                <i></i>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                            <tr id="hidden_transaction" style="display: none;" >
                                                <td class="text-left">10</td>
                                                <td>
                                                    <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="hidden_renewal_datess"  value="<?php echo $result['renewal_date'] ?>">
                                                </td>
                                                <td>
                                                    <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="hidden_renewal_amt"  value="<?php echo $result['renewal_date'] ?>">
                                                </td>
                                                <td>
                                                    <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="hidden_renewal_fee"  value="<?php echo $result['renewal_date'] ?>">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var itrUpload = $('#upload_hidden_id').val();
        function ITR_Upload() {
            html  = '';
            html += '<tr id="recurring-row' + itrUpload + '">';
                /*html += '  <td class="left">';
                    html += '<label>'+itrUpload+'</label>';
                html += '  </td>';*/
                html += '  <td class="left">';
                    html += '  <input type="text" name="itrdatas[' + itrUpload + '][year]" value="" placeholder="Assesment Year" class="form-control" />';
                html += '  </td>';
                html += '  <td class="left">';
                    html += '  <input type="text" readonly id="image_' + itrUpload + '"  name="itrdatas[' + itrUpload + '][image]" value="" placeholder="choose file" class="form-control" />';
                    html += '  <input type="hidden" id="image_path_' + itrUpload + '" name="itrdatas[' + itrUpload + '][imagepath]" value=""  class="form-control" />';
                    html += '<button type="button" class="button-upload btn btn-default" id="button-upload_'+ itrUpload + '" data-loading-text="<?php echo 'Please Wait'; ?>" style="margin-top: 5px;"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>';
                    html += '<span id="button-other_document_'+itrUpload+'"></span>';
                html += '  </td>';
                html += '  <td class="left">';
                    html += '    <a onclick="$(\'#recurring-row' + itrUpload + '\').remove()" data-toggle="tooltip" title="<?php echo "BTN Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
                html += '  </td>';
            html += '</tr>';

            $('#itrUpload tbody').append(html);
             itrUpload++;
        }
    </script>

    <script type="text/javascript">
        $(document).delegate('.button-upload', 'click', function() {
            idss = $(this).attr('id');
            s_id = idss.split('_');
            id = s_id[1];
            $('#form_upload_itr').remove();
            $('body').prepend('<form enctype="multipart/form-data" id="form_upload_itr" style="display: none;"><input type="file" name="file" /></form>');
            $('#form_upload_itr input[name=\'file\']').trigger('click');
            if (typeof timer != 'undefined') {
                    clearInterval(timer);
            }
            timer = setInterval(function() {
                if ($('#form_upload_itr input[name=\'file\']').val() != '') {
                    clearInterval(timer);   
                    id = id;
                    $.ajax({
                        url: 'index.php?route=catalog/jockey/upload1&token=<?php echo $token; ?>'+'&jockey_id='+id,
                        type: 'post',   
                        dataType: 'json',
                        data: new FormData($('#form_upload_itr')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,   
                        beforeSend: function() {
                            $('#button-upload').button('loading');
                        },
                        complete: function() {
                            $('#button-upload').button('reset');
                        },  
                        success: function(json) {
                            if (json['error']) {
                                alert(json['error']);
                            }
                            if (json['success']) {
                                alert(json['success']);
                                console.log(json);
                                $('#image_'+json['id']).attr('value', json['filename']);
                                $('#image_path_'+json['id']).attr('value', json['filepath']);
                                d = new Date();
                            // var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="itrdatas[<?php echo $key ?>][imagepath]" href="'+json['link_href']+'">View Document</a>';
                            // $('#file_source').remove();
                            // $('#button-other_document_1_new_'+json['id']).html(previewHtml);
                            // }
                            var previewHtml = '<a target="_blank" class = "btn btn-default" style="cursor: pointer;margin-left:5px;" id = "btn_view_'+json['id']+'" href="'+json['filepath']+'">View Document</a>';
                                $('#btn_view_'+json['id']).remove();
                                $('#button-other_document_'+json['id']).append(previewHtml);}
                        },      
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            }, 500);
        });

    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });

        $( "#input-jockey_returned_by" ).change(function(){
            var jockey_returned_by =  $(this).val()
            if(jockey_returned_by == 'Y'){
                $('.add-owner-div').show();
            } else {
                $('.add-owner-div').hide();
            }
        });


        $('#license_type').change(function() {
        var select_b =  $('#license_type').val();
            if (select_b == 'B') {
                $('#center_holding_hide').show();
               
            } else {
                $('#center_holding_hide').hide();
            }
        });

        $( document ).ready(function() {
            var select_h =  $('#license_type').val();
            if (select_h == 'B') {
                $('#center_holding_hide').show();
                
               
            } else {
                $('#center_holding_hide').hide();
                
            }
        });

        $( document ).ready(function() {
            $('#jockey_code').focus();
        });
        $("#day_date_of_birth").keyup(function()  {
            if(this.value.length == 2 && parseInt($(this).val()) <= 31 ){
                $('#error_day_date_of_birth').hide();
                $( "#month_date_of_birth" ).focus();
            } else {
                $('#error_day_date_of_birth').show();
            }
        });
        // $("#month_date_of_birth").keyup(function()  {
        //      if(this.value.length == 2 && parseInt($(this).val()) <= 12){
        //         $('#error_month_date_of_birth').hide();
        //         $( "#year_date_of_birth" ).focus();
        //     } else {
        //         $('#error_month_date_of_birth').show();
        //     }
        // });
        // $("#year_date_of_birth").keyup(function()  {
        //      if(this.value.length == 4){
        //         $('#error_year_date_of_birth').hide();
        //         $( "#origin" ).focus();
        //      } else {
        //         $('#error_year_date_of_birth').show();
        //      }
        // });
        $("#day_date_of_license_renewal").keyup(function()  {
            if(this.value.length == 2 && parseInt($(this).val()) <= 31 ){
                $('#error_day_date_of_license_renewal').hide();
                $( "#month_date_of_license_renewal" ).focus();
            } else {
                $('#error_day_date_of_license_renewal').show();
            }
        });
        $("#month_date_of_license_renewal").keyup(function()  {
             if(this.value.length == 2 && parseInt($(this).val()) <= 12){
                $('#error_month_date_of_license_renewal').hide();
                $( "#year_date_of_license_renewal" ).focus();
            } else {
                $('#error_month_date_of_license_renewal').show();
            }
        });
        $("#year_date_of_license_renewal").keyup(function()  {
             if(this.value.length == 4){
                $('#error_year_date_of_license_renewal').hide();
                $( "#day_license_end_date").focus();
             } else {
                $('#error_year_date_of_license_renewal').show();
             }
        });
        $("#day_license_end_date").keyup(function()  {
            if(this.value.length == 2 && parseInt($(this).val()) <= 31 ){
                $('#error_day_license_end_date').hide();
                $( "#month_license_end_date" ).focus();
            } else {
                $('#error_day_license_end_date').show();
            }
        });
        $("#month_license_end_date").keyup(function()  {
             if(this.value.length == 2 && parseInt($(this).val()) <= 12){
                $('#error_month_license_end_date').hide();
                $( "#year_license_end_date" ).focus();
            } else {
                $('#error_month_license_end_date').show();
            }
        });
        $("#year_license_end_date").keyup(function()  {
             if(this.value.length == 4){
                $('#error_year_license_end_date').hide();
                $( "#fee_paid_type").focus();
             } else {
                $('#error_year_license_end_date').show();
             }
        });

        $("#day_license_issued_date").keyup(function()  {
            if(this.value.length == 2 && parseInt($(this).val()) <= 31 ){
                $('#error_day_license_issued_date').hide();
                $( "#month_license_issued_date" ).focus();
            } else {
                $('#error_day_license_issued_date').show();
            }
        });
        $("#month_license_issued_date").keyup(function()  {
             if(this.value.length == 2 && parseInt($(this).val()) <= 12){
                $('#error_month_license_issued_date').hide();
                $( "#year_license_issued_date" ).focus();
            } else {
                $('#error_month_license_issued_date').show();
            }
        });
        $("#year_license_issued_date").keyup(function()  {
             if(this.value.length == 4){
                $('#error_year_license_issued_date').hide();
                $( "#license_type").focus();
             } else {
                $('#error_year_license_issued_date').show();
             }
        });

        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            console.log(name);
            console.log(class_name);
            console.log(id);
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'jockey_code'){
                    $('#racing_name').focus();
                }
                if(id == 'racing_name'){
                    $('#jockey_name').focus();
                }
                if(id == 'jockey_name'){
                    $('#jaimember').focus();
                }
                if(id == 'jaimember'){
                    $('#day_date_of_birth').focus();
                }
                if(id == 'day_date_of_birth'){
                    $('#month_date_of_birth').focus();
                }
                if(id == 'month_date_of_birth'){
                    $('#year_date_of_birth').focus();
                }
                if(id == 'year_date_of_birth'){
                    $('#origin').focus();
                }
                if(id == 'origin'){
                    $('#day_license_issued_date').focus();
                }
                if(id == 'day_license_issued_date'){
                    $('#month_license_issued_date').focus();
                }
                if(id == 'month_license_issued_date'){
                    $('#year_license_issued_date').focus();
                }
                 if(id == 'year_license_issued_date'){
                    $('#license_type').focus();
                }
                if(id == 'license_type'){
                    if ($('#center_holding_hide').is(':visible')) {
                        $('#center_holding').focus();
                    } else {
                         $('#day_date_of_license_renewal').focus();
                    }
                }
                if(id == 'center_holding'){
                    $('#day_date_of_license_renewal').focus();
                }
                if(id == 'day_date_of_license_renewal'){
                    $('#month_date_of_license_renewal').focus();
                }
                if(id == 'month_date_of_license_renewal'){
                    $('#year_date_of_license_renewal').focus();
                }
                if(id == 'year_date_of_license_renewal'){
                    $('#day_license_end_date').focus();
                }
                if(id == 'day_license_end_date'){
                    $('#month_license_end_date').focus();
                }
                if(id == 'month_license_end_date'){
                    $('#year_license_end_date').focus();
                }
                if(id == 'year_license_end_date'){
                    $('#fee_paid_type').focus();
                }
                if(id == 'fee_paid_type'){
                    $('#fee_paid').focus();
                }
                if(id == 'fee_paid'){
                    $('#ride_weight').focus();
                }
                if(id == 'ride_weight'){
                    $('#file_number').focus();
                }
                if(id == 'file_number'){
                    $('#blood_group').focus();
                }
                if(id == 'blood_group'){
                    $('#button-file').focus();
                }
                if(id == 'button-file'){
                    $('#no_whip').focus();
                }

                if(id == 'no_whip'){
                    $('#apprentice').focus();
                }
                if(id == 'apprentice'){
                    $('#allowance_claiming').focus();
                }
                if(id == 'allowance_claiming'){
                    $('#total_wins').focus();
                }
                if(id == 'total_wins'){
                    $('#master_trainer').focus();
                }

                if(id == 'master_trainer'){
                    $('#remarks').focus();
                }
                if(id == 'remarks'){
                    $('#jockey_code').focus();
                }
            }
        });
        $('#button-file').on('click', function() {
        $('#form-other_file').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-other_file" style="display: none;"><input type="file" name="file" /></form>');
        $('#form-other_file input[name=\'file\']').trigger('click');
        if (typeof timer != 'undefined') {
              clearInterval(timer);
        }
        timer = setInterval(function() {
            if ($('#form-other_file input[name=\'file\']').val() != '') {
              clearInterval(timer); 
              image_name = 'jockey';  
              $.ajax({ 
                url: 'index.php?route=catalog/jockey/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
                type: 'post',   
                dataType: 'json',
                data: new FormData($('#form-other_file')[0]),
                cache: false,
                contentType: false,
                processData: false,   
                beforeSend: function() {
                  $('#button-upload').button('loading');
                },
                complete: function() {
                  $('#button-upload').button('reset');
                },  
                success: function(json) {
                  if (json['error']) {
                    alert(json['error']);
                  }
                  if (json['success']) {
                    alert(json['success']);
                    console.log(json);
                    $('input[name=\'file\']').attr('value', json['filename']);
                    $('input[name=\'file_source\']').attr('value', json['link_href']);
                    d = new Date();
                    var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="awbi_registration_file_source" href="'+json['link_href']+'">View Document</a>';
                    $('#file_source').remove();
                    $('#button-file_new').append(previewHtml);
                  }
                },      
                error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
              });
            }
          }, 500);
        });
    </script>
    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
        $(document).on('keydown', '.form-control', '.radio-inline' , function(e) {
        var name = $(this).attr('name'); 
        var class_name = $(this).attr('class'); 
        var id = $(this).attr('id');
        console.log(name);
        console.log(class_name);
        console.log(id);
        var value = $(this).val();
        if(e.which == 13){
            if(id == 'phone_no'){
                $('#mobile_no1').focus();
            }
            if(id == 'mobile_no1'){
                $('#alternate_mob_no').focus();
            }
            if(id == 'alternate_mob_no'){
                $('#email_id').focus();
            }
            if(id == 'email_id'){
                $('#alternate_email_id').focus();
            }
            if(id == 'alternate_email_id'){
                $('#address1').focus();
            }
            if(id == 'address1'){
                $('#localArea1').focus();
            }
            if(id == 'address2'){
                $('#localArea2').focus();
            }
            if(id == 'localArea1'){
                $('#state_1').focus();
            }
            if(id == 'localArea2'){
                $('#state_2').focus();
            }
            if(id == 'state_1'){
                $('#city_1').focus();
            }
            if(id == 'city_1'){
                $('#pincode_1').focus();
            }
            if(id == 'state_2'){
                $('#city_2').focus();
            }
            if(id == 'city_2'){
                $('#pincode_2').focus();
            }
            if(id == 'pincode_1'){
                $('#country_1').focus();
            }
            if(id == 'country_1'){
                $('#address2').focus();
            }
            if(id == 'pincode_2'){
                $('#country_2').focus();
            }
            if(id == 'country_2'){
                $('#gst_type').focus();
            }
            if(id == 'gst_type'){
                $('#gst_no').focus();
            }
            if(id == 'gst_no'){
                $('#pan_no').focus();
            }
            if(id == 'pan_no'){
                $('#prof_tax_no').focus();
            }
            if(id == 'prof_tax_no'){
                $('#phone_no').focus();
            }
        }
    });
    </script>
  
   
    <script type="text/javascript"><!--
        $('#language a:first').tab('show');
    </script>



     <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                //SaveOwnershp();
            }
        });
        $("#date_start_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 31 && parseInt($(this).val()) > 0  ){
                    $('#error_date_start_date_ban').hide();
                    $( "#month_start_date_ban" ).focus();
                } else {
                    $('#error_date_start_date_ban').show();
                }
            } else {
                e.preventDefault();
            }
        });
        $("#month_start_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)){
                if(this.value.length == 2 && parseInt($(this).val()) <= 12 && parseInt($(this).val()) > 0 ){
                    $('#error_month_start_date_ban').hide();
                    $( "#year_start_date_ban" ).focus();
                } else {
                    $('#error_month_start_date_ban').show();
                }
            }else {
                e.preventDefault();
            }
        });
        $("#year_start_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                 if(this.value.length == 4){
                    $('#error_year_start_date_ban').hide();
                    $( "#date_end_date_ban" ).focus();
                 } else {
                    $('#error_year_start_date_ban').show();
                 }
            } else {
                e.preventDefault();
            }
        });
        $("#date_end_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105)|| (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 31 && parseInt($(this).val()) > 0 ){
                    $('#error_date_end_date_ban').hide();
                    $( "#month_end_date_ban" ).focus();
                } else {
                    $('#error_date_end_date_ban').show();
                }
            } else {
                e.preventDefault();
            }
        });
        $("#month_end_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 12 && parseInt($(this).val()) > 0 ){
                    $('#error_month_end_date_ban').hide();
                    $( "#year_end_date_ban" ).focus();
                } else {
                    $('#error_month_end_date_ban').show();
                }
            } else {
                e.preventDefault();
            }
        });
        $("#year_end_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)){
                 if(this.value.length == 4){
                    $('#error_year_end_date_ban').hide();
                    $( "#date_start_date_ban2" ).focus();
                 } else {
                    $('#error_year_end_date_ban').show();
                 }
            } else {
                e.preventDefault();
            }
        });
        $("#date_start_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 31 && parseInt($(this).val()) > 0  ){
                    $('#error_date_start_date_ban2').hide();
                    $( "#month_start_date_ban2" ).focus();
                } else {
                    $('#error_date_start_date_ban2').show();
                }
            } else {
                e.preventDefault();
            }
        });
        $("#month_start_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)){
                if(this.value.length == 2 && parseInt($(this).val()) <= 12 && parseInt($(this).val()) > 0 ){
                    $('#error_month_start_date_ban2').hide();
                    $( "#year_start_date_ban2" ).focus();
                } else {
                    $('#error_month_start_date_ban2').show();
                }
            }else {
                e.preventDefault();
            }
        });
        $("#year_start_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                 if(this.value.length == 4){
                    $('#error_year_start_date_ban2').hide();
                    $( "#date_end_date_ban2" ).focus();
                 } else {
                    $('#error_year_start_date_ban2').show();
                 }
            } else {
                e.preventDefault();
            }
        });
        $("#date_end_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105)|| (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 31 && parseInt($(this).val()) > 0 ){
                    $('#error_date_end_date_ban2').hide();
                    $( "#month_end_date_ban2" ).focus();
                } else {
                    $('#error_date_end_date_ban2').show();
                }
            } else {
                e.preventDefault();
            }
        });
        $("#month_end_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 12 && parseInt($(this).val()) > 0 ){
                    $('#error_month_end_date_ban2').hide();
                    $( "#year_end_date_ban2" ).focus();
                } else {
                    $('#error_month_end_date_ban2').show();
                }
            } else {
                e.preventDefault();
            }

        });
        $("#year_end_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)){
                 if(this.value.length == 4){
                    $('#error_year_end_date_ban2').hide();
                    $( "#amount_ban" ).focus();
                 } else {
                    $('#error_year_end_date_ban2').show();
                 }
            } else {
                e.preventDefault();
            }
        });
        // Ban details tab
        function BanFunction(){
            var date_start_date_ban =  $( "#date_start_date_ban" ).val();
            var end_date_ban =  $( "#date_end_date_ban" ).val();
            var date_start_date_ban2 =  $( "#date_start_date_ban2" ).val();
            var end_date_ban2 =  $( "#date_end_date_ban2" ).val();
            var auto_id = $('#id_hidden_band').val();
            var id_hidden_BanFunction = $('#id_hidden_BanFunction').val();
            
            if (end_date_ban != '') {

                var string_start_date = date_start_date_ban.split('-');
                var s_date  = string_start_date[0];
                var s_month = string_start_date[1];
                var s_year  = string_start_date[2];

                var string_end_date = end_date_ban.split('-');
                var e_date  = string_end_date[0];
                var e_month = string_end_date[1];
                var e_year  = string_end_date[2];

                if (s_year > e_year){
                    $('#error_greater_date_start_date_ban').show();
                    return false;
                } else if (e_month > s_month) {
                    if (s_date > e_date || e_date > s_date) {
                        $('#error_greater_date_start_date_ban').hide();
                    } 
                } else if(s_month > e_month) {
                    $('#error_greater_date_start_date_ban').show();
                    return false;
                } else if(s_month == e_month) {
                    if (s_date > e_date) {
                        $('#error_greater_date_start_date_ban').show();
                        return false;
                    }
                }
            }

            if (end_date_ban2 != '') {
                
                var string_start_date2 = date_start_date_ban2.split('-');
                var s_date2  = string_start_date2[0];
                var s_month2 = string_start_date2[1];
                var s_year2  = string_start_date2[2];

                var string_end_date2 = end_date_ban2.split('-');
                var e_date2  = string_end_date2[0];
                var e_month2 = string_end_date2[1];
                var e_year2  = string_end_date2[2];

                if (s_year2 > e_year2){
                    $('#error_greater_date_start_date_ban2').show();
                    return false;
                } else if (e_month2 > s_month2) {
                    if (s_date2 > e_date2 || e_date2 > s_date2) {
                        $('#error_greater_date_start_date_ban2').hide();
                    } 
                } else if(s_month2 > e_month2) {
                    $('#error_greater_date_start_date_ban2').show();
                    return false;
                } else if(s_month2 == e_month2) {
                    if (s_date2 > e_date2) {
                        $('#error_greater_date_start_date_ban2').show();
                        return false;
                    }
                }
            }

            if(date_start_date_ban != ''){
                $('#error_date_start_date_ban').hide();
                var date_start_date_bans = date_start_date_ban;
            } else {
                $('#error_date_start_date_ban').show();
            }

            if( end_date_ban != ''){
                $('#error_date_end_date_ban').hide();
                var end_date_ban_ends = end_date_ban;
            } else {
                $('#error_date_end_date_ban').show();
            }

            if(date_start_date_ban2 != ''){
                $('#error_date_start_date_ban2').hide();
                var date_start_date_bans2 = date_start_date_ban2;
            } else {
                $('#error_date_start_date_ban2').show();
            }

            if( end_date_ban2 != ''){
                $('#error_date_end_date_ban2').hide();
                var end_date_ban_ends2 = end_date_ban2;
            } else {
                $('#error_date_end_date_ban2').show();
            }

            var club = $('#input-club').val();
            if(club != null && club != ''){
                $('#error_club_ban').hide();
                clubs = club;
            } else {
                $('#error_club_ban').show();
            }
            var authority_ban = $('#authority_ban_details').val();
            if(authority_ban != '' && authority_ban !=  null){
                $('#error_authority_ban').hide();
                var authority_bans = authority_ban.replace(/\s\s+/g, ' ');
            } else {
                $('#error_authority_ban').show();
            }
            var reason_bans = $('#reason_ban').val();
            if(reason_bans != ''  ){
                $('#error_reason_ban').hide();
            } else {
                $('#error_reason_ban').show();
            }
            var amount_bans = $('#amount_ban').val();
            if(amount_bans != ''  ){
                $('#error_amount_ban').hide();
            } else {
                $('#error_amount_ban').show();
            }
            if (date_start_date_ban == ''){
                return false;
            } else {
                if(id_hidden_BanFunction == '0'){ 
                    var start_date_bans = date_start_date_ban; //getstart_dare 
                    if(end_date_ban_ends != ''){
                        var end_date_bans = end_date_ban;  //getend days
                    } else {
                        var end_date_bans = '';
                    }

                    if(date_start_date_bans2 != ''){
                        var start_date_bans2 = date_start_date_ban2;  //getend days

                    } else {
                        var start_date_bans2 = '';
                    }
                    if(end_date_ban_ends2 != ''){
                        var end_date_bans2 = end_date_ban2;  //getend days
                    } else {
                        var end_date_bans2 = '';
                    }
                    html = '<tr id ="bandetail_'+auto_id+'">';
                        html += '<td class="text-left">';
                            html += '<span id="clubs_'+auto_id+'">'+clubs+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][club]" id="club_'+auto_id+'" value = '+clubs+'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="start_dateban_'+auto_id+'">'+start_date_bans+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][start_date1]" id="start_date_bans_'+auto_id+'" value = '+start_date_bans+'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="end_dateban_'+auto_id+'">'+end_date_bans+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][end_date1]" id="end_date_ban_'+auto_id+'" value = '+end_date_bans+'>';
                        html += '</td>';

                        html += '<td class="text-right">';
                            html += '<span id="start_dateban2_'+auto_id+'">'+start_date_bans2+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][start_date2]" id="start_date_bans2_'+auto_id+'" value = '+start_date_bans2+'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="end_dateban2_'+auto_id+'">'+end_date_bans2+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][end_date2]" id="end_date_ban2_'+auto_id+'" value = '+end_date_bans2+'>';
                        html += '</td>';

                        html += '<td class="text-right">';
                            html += '<span id="authorityban_'+auto_id+'">'+authority_bans+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][authority]" id="authority_ban_'+auto_id+'" value = \'' + authority_bans + '\'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="reasonban_'+auto_id+'">'+reason_bans+ '</span>';
                            html += '<input type= "hidden" name= "bandats['+auto_id+'][reason]" id="reason_ban_'+auto_id+'" value = \'' + reason_bans + '\'>';
                           /* html += '<span id="amountban_'+auto_id+'">'+amount_bans+ '</span>';*/
                            html += '<input type= "hidden" name= "bandats['+auto_id+'][amount]" id="amount_ban_'+auto_id+'" value = \'' + amount_bans + '\'>'; 
                        html += '</td>';
                       
                        html += '<td class="text-center">';
                            html += '<a onclick=updateban("'+auto_id+'") class="btn btn-primary"><i class="fa fa-pencil"></i></a>&nbsp';
                            html += '<a onclick=removeBanDetail(0,0,"bandetail_'+auto_id+'") class="btn btn-danger"><i class="fa fa-minus-circle"></i></a></td>'
                        html += '</tr >';
                    auto_id++;
                    $('#bandeatilsbody').append(html);
                    $('#id_hidden_band').val(auto_id);
                } else {
                    var old_start_date_ban = date_start_date_bans; //getstart_dare 
                    if(end_date_ban_ends != ''){
                        var old_end_date_ban = end_date_ban_ends;  //getend days
                    } else {
                        var old_end_date_ban = '';
                    }
                    if(date_start_date_ban2 != ''){
                        var old_start_date_ban2 = date_start_date_ban2;  //getend days
                    } else {
                        var old_start_date_ban2 = '';
                    }
                    if(end_date_ban_ends2 != ''){
                        var old_end_date_ban2 = end_date_ban_ends2;  //getend days
                    } else {
                        var old_end_date_ban2 = '';
                    }
                    var idban_increment_id = $('#idban_increment_id').val();
                    $('#start_date_bans_'+idban_increment_id+'').val(old_start_date_ban);
                    $('#end_date_ban_'+idban_increment_id+'').val(old_end_date_ban);
                    $('#start_date_bans2_'+idban_increment_id+'').val(old_start_date_ban2);
                    $('#end_date_ban2_'+idban_increment_id+'').val(old_end_date_ban2);
                    $('#reason_ban_'+idban_increment_id+'').val(reason_bans);
                    $('#amount_ban_'+idban_increment_id+'').val(amount_bans);
                    $('#authority_ban_'+idban_increment_id+'').val(authority_bans);
                    $('#club_'+idban_increment_id+'').val(clubs);
                    $('#start_dateban_'+idban_increment_id+'').html(old_start_date_ban);
                    $('#end_dateban_'+idban_increment_id+'').html(old_end_date_ban);
                    $('#start_dateban2_'+idban_increment_id+'').html(old_start_date_ban2);
                    $('#end_dateban2_'+idban_increment_id+'').html(old_end_date_ban2);
                    $('#reasonban_'+idban_increment_id+'').html(reason_bans);
                    $('#amountban_'+idban_increment_id+'').html(amount_bans);
                    $('#authorityban_'+idban_increment_id+'').html(authority_bans);
                    $('#clubs_'+idban_increment_id+'').html(clubs);
                }
            }
            $("#myModal2").modal("toggle");
        }
        //blanked value for Ban details Tab
        function closeaddbuu1(){
            $('#input-club').val('');
            $('#reason_ban').val('');
            $('#amount_ban').val('');
            $('#authority_ban_details').val('');
            $('#id_hidden_BanFunction').val(0);
            $("#date_start_date_ban" ).val('');
            $("#date_end_date_ban" ).val('');
            $('#error_greater_date_start_date_ban').hide();
            $('#error_greater_date_start_date_ban2').hide();
            $('#error_club_ban').hide();
            $('#error_authority_ban').hide();
            $('#error_date_start_date_ban').hide();
            $('#error_date_end_date_ban').hide();
            $('#error_reason_ban').hide();
            $('#error_amount_ban').hide();
            $("#date_start_date_ban2" ).val('');
            $("#date_end_date_ban2" ).val('');
            $('#error_date_start_date_ban2').hide();
            $('#error_date_end_date_ban2').hide();
        }
        $('#myModal2').on('shown.bs.modal', function () {
            $('#input-club').focus();
        }); 
        //update function ban details tab
        function updateban(auto_id){
            $('#error_greater_date_start_date_ban').hide();
            $('#error_greater_date_start_date_ban2').hide();
            $('#myModal2').modal('show');
            $('#idban_increment_id').val(auto_id);
            $('#id_hidden_BanFunction').val(1);
            var start_date_ban = $('#start_date_bans_'+auto_id+'').val();
            var end_date_ban = $('#end_date_ban_'+auto_id+'').val();
            var start_date_ban2 = $('#start_date_bans2_'+auto_id+'').val();
            var end_date_ban2 = $('#end_date_ban2_'+auto_id+'').val();
            var reason_ban = $('#reason_ban_'+auto_id+'').val();
            var amount_ban = $('#amount_ban_'+auto_id+'').val();
            var authority_ban = $('#authority_ban_'+auto_id+'').val();
            var club_ban = $('#club_'+auto_id+'').val();
            $("#date_start_date_ban").val(start_date_ban);
            $( "#date_end_date_ban" ).val(end_date_ban);
            $("#date_start_date_ban2").val(start_date_ban2);
            $( "#date_end_date_ban2" ).val(end_date_ban2);
            $('#authority_ban_details').val(authority_ban);
            $('#reason_ban').val(reason_ban);
            $('#amount_ban').val(amount_ban);
            $('#input-club').val(club_ban);
        }
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'input-club'){
                    $('#date_start_date_ban').focus();
                }
                if(id == 'date_start_date_ban'){
                    $('#date_end_date_ban').focus();
                }
                if(id == 'date_end_date_ban'){
                    $('#date_start_date_ban2').focus();
                }
                if(id == 'date_start_date_ban2'){
                    $('#date_end_date_ban2').focus();
                }
                if(id == 'date_end_date_ban2'){
                    $('#amount_ban').focus();
                }
                if(id == 'amount_ban'){
                    $('#authority_ban_details').focus();
                }
                if(id == 'authority_ban_details'){
                    $('#reason_ban').focus();
                }
                if(id == 'reason_ban'){
                    $('#ban_save_id').focus();
                }
            }
        });

        function removeBanDetail(jockey_id,ban_id,id_remove){
            if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
                if(jockey_id == '0' && ban_id == '0'){
                    alert('Delete Record Sucessfully');
                    $('#'+id_remove+'').closest("tr").remove();
                    return false;
                }
                $.ajax({
                    url:'index.php?route=catalog/jockey/deletebandeatils&token=<?php echo $token; ?>'+'&jockey_id='+jockey_id+'&ban_id='+ban_id,
                    method: "POST",
                    dataType: 'json',
                    success: function(json)
                    {
                        $('#'+id_remove+'').closest("tr").remove();
                        alert(json['success']);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error deleting data');
                    }
                });
                return false;
            }
        }

        $( "#gst_type" ).click(function(){
            var gst_no =  $(this).val();
            if(gst_no == 'R'){
                $("#gst_label").show();
                $("#gst_no").show();
            } else {
                $("#gst_label").hide();
                $("#gst_no").hide();
            }
        });



        $('#gst_no').keyup(function(){
            gst_len = parseInt($("#gst_no").val().length);
           // console.log(gst_len);
            $('.gst_error').html('');
            if(gst_len > 15 || gst_len < 15){
                $('.gst_error').css("color", "red")
                $('.gst_error').append('GST No should be 15 digits!');

                //return false;
            } 

            if(gst_len == 15 ){
                $('.gst_error').html('');
            }   
        });

        $('#pan_no').keyup(function(){
            pan_len = parseInt($("#pan_no").val().length);
           // console.log(gst_len);
            $('.pan_error').html('');
            if(pan_len > 10 || pan_len < 10){
                $('.pan_error').css("color", "red")
                $('.pan_error').append('PAN No should be 10 digits!');

                //return false;
            } 

            if(pan_len == 10 ){
                $('.pan_error').html('');
            }   
        })

        function transaction(){
            //alert('inn');
            var trainer_idss = $('#trainer_idss').val();
            var dates = $('#date_of_license_renewal').val();
            var amts = $('#input-license_amt').val();
            var feess = $('#input-fees_paid_type').val();

            $.ajax({
                url:'index.php?route=catalog/jockey/transaction&token=<?php echo $token; ?>'+'&dates='+dates+'&amts='+amts+'&feess='+feess+'&trainer_idss='+trainer_idss,
                method: "POST",
                dataType: 'json',
                success: function(json)
                {
                    if (json['success'] == '1') {
                        var date = $('#date_of_license_renewal').val();
                        var amt = $('#input-license_amt').val();
                        var fees = $('#input-fees_paid_type').val();
                        $('#hidden_renewal_datess').val(date);
                        $('#hidden_renewal_amt').val(amt);
                        $('#hidden_renewal_fee').val(fees);
                        $("#hidden_transaction").show();
                        location.reload();
                    }
                }
            });
        }

        function removes($id){
            var id = $id;
            $.ajax({
                url:'index.php?route=catalog/jockey/delete_transaction&token=<?php echo $token; ?>'+'&id='+id,
                method: "POST",
                dataType: 'json',
                success: function(json)
                {
                    if (json['success'] == '1') {
                        location.reload();
                    }
                }
            });
        }
    </script>
    <script type="text/javascript">
        function country(element, zone_id) {
            $.ajax({
                url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + element.value,
                dataType: 'json',
                beforeSend: function() {
                    $('select[name=\'country1\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    console.log(json['zone']);
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'country1[postcode]\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'country1[postcode]\']').parent().parent().removeClass('required');
                    }

                    html = '<option value="">Please Select</option>';

                    if (json['zone'] && json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                            if (json['zone'][i]['zone_id'] == zone_id) {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="0">None</option>';
                    }

                    $('select[name=\'zone_id\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }

        $('select[name$=\'[country_id]\']').trigger('change');


        function countrys(element, zone_id) {
            $.ajax({
                url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + element.value,
                dataType: 'json',
                beforeSend: function() {
                    $('select[name=\'country2\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    console.log(json['zone']);
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'country[postcode]\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'country[postcode]\']').parent().parent().removeClass('required');
                    }

                    html = '<option value="">Please Select</option>';

                    if (json['zone'] && json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                            if (json['zone'][i]['zone_id'] == zone_id) {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="0">None</option>';
                    }

                    $('select[name=\'zone_id2\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }

        $('select[name$=\'[country_id]\']').trigger('change');

    </script>
</div>
<script type="text/javascript">
$( document ).ready(function() {
var apprentice =  $('#apprentice').val();
//alert(assistant_trainer);div_master_trainer
    if(apprentice == 'Yes'){
        $('#allowance_claiming_l').show();
        $('#allowance_claiming_div').show();
        $('#div_master_trainer').show();
    } else {
        $('#allowance_claiming_l').hide();
        $('#allowance_claiming_div').hide();
        $('#div_master_trainer').hide();
    }
});
$( "#apprentice" ).change(function(){
var apprentice =  $(this).val()
console.log(apprentice);
    if(apprentice == 'Yes'){
        $('#allowance_claiming_l').show();
        $('#allowance_claiming_div').show();
        $('#div_master_trainer').show();
    } else {
        $('#allowance_claiming_l').hide();
        $('#allowance_claiming_div').hide();
        $('#div_master_trainer').hide();
    }
});

$('.date').datetimepicker({
    pickTime: false
});

$('.time').datetimepicker({
    pickDate: false
});

$('.datetime').datetimepicker({
    pickDate: true,
    pickTime: true
});

</script>
<script type="text/javascript">
    function total(){
        var out_station_wins =  parseInt($('#out_station_wins').val()) || 0;
        var station_wins =  parseInt($('#station_wins').val()) || 0;

        var total_wins =(out_station_wins + station_wins);

        $('#total_wins').val('');
        $('#total_wins').val(total_wins);

    }
</script>
<script language="javascript">
function changeIt(count){
    var i;

    $('.mydivs').remove();
    for(i=0;i<count;i++) {
        my_div.innerHTML = my_div.innerHTML +"<div class='mydivs'><label class='col-sm-2 control-label'>Horse"+i+":</label><div class='col-sm-2'><input class='form-control' type='text' name='out_station_name["+i+"][name]</div></div>'>";

    }
}
</script>

<script type="text/javascript">
$('#master_trainer').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/jockey/autocompletetrainer&token=<?php echo $token; ?>&trainer=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['vendor_name'],
            id: item['vendor_id'],
            value: item['vendor_name'],
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('#master_trainer').val(item['label']);
    $('#master_trainer').val(item['value']);
    $('.dropdown-menu').hide();
  }
});
</script>

<script type="text/javascript">
$( document ).ready(function() {
    var jockey_returned_by_type =  $('#input-jockey_returned_by').val();
    if(jockey_returned_by_type == 'Y'){
        $('.add-owner-div').show();
    } else {
        $('.add-owner-div').hide();
    }
});

$('#add-ownrs').click(function() {
    auto_id = $('.owners_cnt').val();
    id = parseInt(auto_id) + 1;
    html = '';
    html += '<div class="form-group" id="remove_div_'+id+'">';
        html += '<div class="col-sm-10">';
            html += '<input type="text" name=private_trainers_owner_name['+id+'][owner_name] class="form-control trainer_owner_name" id="trainer_owner_id_'+id+'" />';
            html += '<input type="hidden" name=private_trainers_owner_name['+id+'][owner_id] id="hidden_trainer_owner_id_'+id+'" />';
        html += '</div>';

        html += '<div class="col-sm-2">';
            html += '<a onclick="rmvOwnerName('+id+')" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
        html += '</div>';
    html += '</div>';
    

    $('.owner_data-div').append(html);
    $('.owners_cnt').val(id);
    $('#trainer_owner_id_'+id).focus();
});

$(document).on('keyup', '.trainer_owner_name', function(e) {
    $('.trainer_owner_name').autocomplete({
        'source': function(request, response) {
            $.ajax({
            url: 'index.php?route=catalog/jockey/autocompleteOwnername&token=<?php echo $token; ?>&owner_name=' +  encodeURIComponent(request),
            dataType: 'json',
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.owner_name,
                        value: item.owner_name,
                        ownercode: item.owner_id
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            idzz = $(this).attr('id');
            idss = idzz.split('_');
            $('#'+idzz).val(item['label']);
            $('#hidden_trainer_owner_id_'+idss[3]).val(item['ownercode']);
            return false;
        }
    });
});

function rmvOwnerName(id){
    $('#remove_div_'+id).remove();

}
</script>

<script type="text/javascript">
    $('#button-other_document_1').on('click', function() {
$('#form-other_document_1').remove();
$('body').prepend('<form enctype="multipart/form-data" id="form-other_document_1" style="display: none;"><input type="file" name="file" /></form>');
$('#form-other_document_1 input[name=\'file\']').trigger('click');
if (typeof timer != 'undefined') {
    clearInterval(timer);
}
timer = setInterval(function() {
  if ($('#form-other_document_1 input[name=\'file\']').val() != '') {
    clearInterval(timer); 
    image_name = 'file_number';  
    $.ajax({ 
    url: 'index.php?route=catalog/jockey/upload_profile&token=<?php echo $token; ?>'+'&image_name='+image_name,
    type: 'post',   
    dataType: 'json',
    data: new FormData($('#form-other_document_1')[0]),
    cache: false,
    contentType: false,
    processData: false,   
    beforeSend: function() {
      $('#button-upload').button('loading');
    },
    complete: function() {
      $('#button-upload').button('reset');
    },  
    success: function(json) {
      if (json['error']) {
      alert(json['error']);
      }
      if (json['success']) {
      alert(json['success']);
      console.log(json);
      $('input[name=\'file_number_upload\']').attr('value', json['filename']);
      $('input[name=\'uploaded_file_source\']').attr('value', json['link_href']);
      d = new Date();
      $('#blah').remove();
      var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="uploaded_file_source" href="'+json['link_href']+'">View Document</a>';
      var image_data = '<img src="'+json['link_href']+'" height="60" id="blah" alt=""  />'
      $('#uploaded_file_source').remove();
       $('#profile_pic').append(image_data);
      }
    },      
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
    });
  }
  }, 500);
});

$(document).on('input', '#jockey_name', function(){
    var name = $( "#jockey_name" ).val();
    $( "#name1" ).val(name);
});


function readURL(input) {
    alert(input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(100)
                .height(80);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
<?php echo $footer; ?>