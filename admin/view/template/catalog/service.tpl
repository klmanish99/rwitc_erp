<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab-general">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="med_type"><?php echo $entry_name; ?></label>
								<div class="col-sm-4">
									<input type="hidden" value="<?php echo $user_log_grp_id; ?>" name="user_log_grp_id">
									<input type="hidden" value="<?php echo $user_log_id; ?>" name="user_log_id">
									<input type="text" name="med_type" value="<?php echo $med_type; ?>" placeholder="<?php echo $entry_name; ?>" id="med_type" class="form-control" tabindex="1"/>
									<?php if (isset($valierr_med_type)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_med_type; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="descp"><?php echo $entry_descp; ?></label>
								<div class="col-sm-4">
									<input type="text" name="descp" value="<?php echo $descp; ?>" placeholder="<?php echo $entry_descp; ?>" id="descp" class="form-control" tabindex="2"/>
									<?php if (isset($valierr_med_descp)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_med_descp; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="limit1">Limit 1</label>
								<div class="col-sm-4">
									<input type="text" name="limit1" value="<?php echo $limit1; ?>" placeholder="Limit 1" id="limit1" class="form-control" tabindex="3"/>
								</div>
								<label class="col-sm-2 control-label" for="limit2">Limit 2</label>
								<div class="col-sm-4">
									<input type="text" name="limit2" value="<?php echo $limit2; ?>" placeholder="Limit 2" id="limit2" class="form-control" tabindex="4"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="sc">Service Charge</label>
								<div class="col-sm-4">
									<input type="text" name="sc" value="<?php echo $sc; ?>" placeholder="Service Charge" id="sc" class="form-control" tabindex="5"/>
								</div>
								<label class="col-sm-2 control-label" for="select-isActive">Status</label>
								<div class="col-sm-3">
										<select name="isActive" id="select-isActive" class="form-control" tabindex="5">
												<?php foreach ($Active as $key => $value) { ?>
												<?php if ($value == $isActive) { ?>
													<option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
												<?php } else { ?>
													<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
												<?php } ?>
												<?php } ?>
										</select>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</script>
	
	<script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script>
<script type="text/javascript">
		$(document).on('keypress','input,select, textarea', function (e) {
//$('input,select').on('keypress', function (e) {
		if (e.which == 13) {
				e.preventDefault();
				var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
				if (!$next.length) {
						$next = $('[tabIndex=1]');
				}
				$next.focus();
		}
});
</script>
</div>
<?php echo $footer; ?>