<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $treatment_entry; ?>" data-toggle="tooltip" title="<?php echo 'Treatment Entry'; ?>" class="btn btn-warning"><i class="fa fa-reply"></i>Treatment Entry</a>
				<button onclick="confirm('<?php echo 'Do You want to Save the Changes'; ?>') ? $('#form-manufacturer').submit() : false;" type="button" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
				
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning || isset($medicine_error)) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?> <?php echo isset($medicine_error) ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>
		<?php if ($day_close_error) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $day_close_error; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
		        	<div class="tab-content">
		        		<div class="tab-pane active" id="tab-general">
			        		<div class="form-group" >
			        			<label class="col-sm-2 control-label" for="input-code"><b style="color: red;"></b> <?php echo "Doctor Code:"; ?></label>
		                        <div class="col-sm-2">
									<input type="hidden" value="" id="input-req" class="form-control" />
									<input type="hidden" value="<?php echo $user_log_grp_id; ?>" name="user_log_grp_id">
									<input type="hidden" value="<?php echo $user_log_id; ?>" name="user_log_id">
		                            <input type="text"  name="input-code" id="input-code" value="" placeholder="Code" class="form-control" />
		                            <br>
		                        </div>
		                        <label class="col-sm-2 control-label" ><b style="color: red;"> * </b> <?php echo "Clinic:"; ?></label>
		                        <div class="col-sm-2">
		                            <input type="text"  name="filter_parent_doctor" id="filter_parent_doctor" value="<?php echo $filter_parent_doctor; ?>" placeholder="Clinic Name" class="form-control" />
		                            <input type="hidden" name="filterParentId" id="filterParentId" value="<?php echo $filter_parent_doctor_id; ?>"  >
		                            <?php if (isset($valierr_clinic)) { ?><span class="alert-danger"><?php echo $valierr_clinic; ?></span><?php } ?>
		                        </div>
		                        <label class="col-sm-2 control-label" for="input-trainer"><?php echo "Treatment No :"; ?></label>
		                        <div class="col-sm-2">
		                             <input type="text" name="treatment_number" value="<?php echo $input_isse_number; ?>" placeholder="Treatment No " id="input_treatment_number" class="form-control"  readonly="readonly"/>
		                        </div>
		                    </div>
		                     <div class="form-group" style="border-top: 0">
		                     	<?php 

		                     		$styl = ($to_doc != '') ? "" : "display: none;";

		                     	?>
		                     	<label class="col-sm-2 control-label" id= "label_to_doc" style="display: none;"><?php echo "Doctor :"; ?></label>
		                        <div class="col-sm-2"  id="div_to_doc" style="display: none;">
		                              <select name="to_doc" id="input_to_doc" class="form-control">
		                              	<?php foreach ($all_docs as $akey => $avalue) { 
		                            		if($avalue['id'] == $to_doc){
		                            		?>
		                            			<option value="<?php echo  $avalue['id'] ?>" selected="selected"> <?php echo $avalue['doctor_name'] ?> </option>
		                            		<?php } else { ?>
		                            			<option value="<?php echo  $avalue['id'] ?>"> <?php echo $avalue['doctor_name'] ?> </option>
		                            		<?php } ?>
		                            	<?php } ?>
		                            </select>
		                            <?php if (isset($valierr_doctor)) { ?><span class="alert-danger"><?php echo $valierr_doctor; ?></span><?php } ?>
		                        </div>
		                        <label class="col-sm-2 control-label" for="input-date"><?php echo 'Date OF Entry'; ?></label>
					            <div style="" class="col-sm-2">
					            	<input type="text" name="date" value="<?php echo date('d-m-Y'); ?>" placeholder="<?php echo 'Date'; ?>" id="input-date" class="form-control date"  readonly="readonly"/>
					        	</div>
		                    </div>

		                    <div class="form-group" style="border-top: 0">
		                     	<label class="col-sm-2 control-label"><?php echo "Trainer:"; ?></label>
		                        <div class="col-sm-3" >
		                            <input type="text" name="trainer_name" id="trainer_name" value="<?php echo $trainer_name; ?>" placeholder="Trainer Name" class="form-control">
                                    <input type="hidden" name="hidden_trainer_name_id" id="hidden_trainer_name_id" value="<?php echo $trainer_name_id; ?>" >
                                    <b><span style="display: none;color: red;" id="valid_supplier" class="errors">Please Enter Valid Trainer!</span></b>
                                    <?php if (isset($valierr_trainer)) { ?><span class="alert-danger"><?php echo $valierr_trainer; ?></span><?php } ?>
		                        </div>
		                        <?php if($horse_datas){
		                        	$cls = "col-sm-4";
		                        	$st = "";
		                        } else {
		                        	$cls = "col-sm-3";
		                        	$st = "display: none;";
		                        }

		                         ?>
		                         <label class="col-sm-2 control-label lblhorse" style=" display: none;"><?php echo 'Horse Name'; ?></label>
		                        <div class="<?php echo $cls; ?>" id="horse_div">
		                        	<label class="col-sm-2 control-label" style="<?php echo $st ?>">Horse Name</label>
						            <div class="col-sm-10" style="<?php echo $st ?>">
						              	<div class="well well-sm" style="height: 150px; overflow: auto;">
						                <?php foreach ($horse_datas as $permission => $pvalue) { ?>
						                <div class="checkbox">
						                  <label>
						                    
						                    <input type="checkbox" name="horse_datas[<?php echo $permission ?>][horse_id]" value="<?php echo $pvalue['horse_id']; ?>" />
						                    <?php echo $pvalue['horse_name']; ?>
						                    <input type="hidden" name="horse_datas[<?php echo $permission ?>][horse_name]" value="<?php echo $pvalue['horse_name']; ?>" />
						                    
						                    
						                  </label>
						                </div>
						                <?php } ?>
						              </div>
						              <input onclick="$(this).parent().find(':checkbox').prop('checked', true);" type="checkbox" name="" id="" >
						              <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo "Select All" ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo "Unselect All" ?></a>
						          	</div>
		                        	<?php if (isset($valierr_horse)) { ?><span class="alert-danger"><?php echo $valierr_horse; ?></span><?php } ?>
					          	</div>
		                        
		                    </div>

		            		<div class="form-group main_div" style="border-top: 0">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-center">Code</td>
										  <td class="text-center">Medicine Name</td>
										  <td style="width: 10%;"  class="text-center">Quantity</td>
										  <td style="width: 5%;" class="text-center" >Unit  </td>
										  <td style="width: 10%;" class="text-center">Rate</td>
										  <td style="width: 15%;" class="text-center">Value</td>
										</tr>

									</thead>
									<tbody>
										<tr style="background: white;" >
											<td style="width: 120px;" class="text-left">
												<input type="text" name="input_item_code" value=""  id="input_item_code" placeholder="<?php echo 'Code'; ?>" class="form-control" />
											</td>
											<td class="text-left">
												<input type="text" name="item_name" value="" placeholder="<?php echo 'Medicine Name'; ?>" id="input_item_name" class="form-control automedi" />
												<input type="hidden" name="input_med_id" value=""  id="input_hidden_medi_id" class="form-control" />
												<br>
												<div class="col-sm-12 syng_div" style="display: none;">
													
													<!-- <select name="syringe_select" id="syringe_select" class="form-control">
						                              	<option value="" >Please Select</option>
						                            </select> -->
						                            <span id="select1" style="display: none;">
							                            <input type="checkbox" name="syringe_select1" id="syringe_select1" >
														<label id="current_syringe1" style="margin-left: 10px;"></label><br>
						                            </span>
						                            <span id="select2" style="display: none;">
							                            <input type="checkbox" name="syringe_select2" id="syringe_select2" >
														<label id="current_syringe2" style="margin-left: 10px;"></label><br>
						                            </span>
						                            <span id="select3" style="display: none;">
							                            <input type="checkbox" name="syringe_select3" id="syringe_select3" >
														<label id="current_syringe3" style="margin-left: 10px;"></label><br>
						                            </span>

						                            <span id="select4" style="display: none;">
							                            <input type="checkbox" name="syringe_select4" id="syringe_select4" >
														<label id="current_syringe4" style="margin-left: 10px;"></label><br>
						                            </span>

						                            <span id="select5" style="display: none;">
							                            <input type="checkbox" name="syringe_select5" id="syringe_select5" >
														<label id="current_syringe5" style="margin-left: 10px;"></label><br>
						                            </span>
						                            <span id="select6" style="display: none;">
							                            <input type="checkbox" name="syringe_select6" id="syringe_select6" >
														<label id="current_syringe6" style="margin-left: 10px;"></label><br>
													</span>
													<span id="select7" style="display: none;">
							                            <input type="checkbox" name="syringe_select7" id="syringe_select7" >
														<label id="current_syringe7" style="margin-left: 10px;"></label>
													</span>
							                        
						                        </div>

											</td>
											<td class="text-left">
												<input  type="text" name="input_qty" value="" placeholder="Quantity" id="input_qty" class="form-control" />
												<input type="hidden" name="input_qty_hidden" value="" placeholder="Quantity" id="input_qty_hidden" class="form-control" />
												<input type="hidden" name="med_type" value=""  id="med_type"  />

											</td>
											<td class="text-left">
												<span id="input_unit"></span>
												<input  type="hidden" name="input_unit" value="" placeholder="<?php echo 'Unit'; ?>" id="input_unit" class="form-control" />
											</td>
											<td class="text-left" id="autopacking">
												<input   type="text" name="input_purchase_prise" value="" placeholder="Purchase Price" id="input_purchase_prise" class="form-control" readonly="readonly"/>
											</td>
											<td class="text-left">
												<input   type="text" name="final_value" value="" placeholder="Value" id="input_value" class="form-control" readonly="readonly" />
											</td>
											
										</tr>
									</tbody>
								</table>
							</div>
							<?php if($productraw_datas){ 
								$sty = ""; 
							} else { 
								$sty = "display: none;";

							 } ?>
					      <div class="form-group imp_div" style="<?php echo $sty ?>">
								<table id="tbladdMedicineTran" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-center">Code</td>
										  <td class="text-center">Medicine Name</td>
										  <td class="text-center" >Unit </td>
										  <td class="text-center">Quantity</td>
										  <td class="text-center">Purchase Price</td>
										  <td class="text-center">Value</td>
										  <td class="text-center">Action</td>
										</tr>
									</thead>
									<tbody id="test">
										<?php foreach ($productraw_datas as $key => $value) { ?>
											<tr id="productraw_row<?php echo $key ?>">
												<td id="input_mt_qty_label<?php echo $key ?>" class="text-left"><?php echo $value['input_item_code'] ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][input_item_code]" value="<?php echo $value['input_item_code'] ?>"  class="form-control po_qty_class" />
												</td>

												<td id="input_mt_qty_label<?php echo $key ?>" class="text-left"><?php echo $value['item_name'] ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][item_name]" value="<?php echo $value['item_name'] ?>"  class="form-control po_qty_class" />

													<?php if($value['syringe_one'] != ''){ ?>
														<i><?php echo $value['syringe_one'].' Syringe With 2- Niddles' ?></i><br>
													<?php } ?>
													<?php if($value['syringe_two'] != ''){ ?>
														<i><?php echo $value['syringe_two'].' Syringe With 2- Niddles' ?></i><br>
													<?php } ?>
													<?php if($value['syringe_three'] != ''){ ?>
														<i><?php echo $value['syringe_three'].' Syringe With 2- Niddles' ?></i><br>
													<?php } ?>
													<?php if($value['syringe_four'] != ''){ ?>
														<i><?php echo $value['syringe_four'].' Syringe With 2- Niddles' ?></i><br>
													<?php } ?>
													<?php if($value['syringe_five'] != ''){ ?>
														<i><?php echo $value['syringe_five'].' Syringe With 2- Niddles' ?></i><br>
													<?php } ?>
													<?php if($value['syringe_six'] != ''){ ?>
														<i><?php echo $value['syringe_six'].' Syringe With 2- Niddles' ?></i><br>
													<?php } ?>
													<?php if($value['syringe_seven'] != ''){ ?>
														<i><?php echo $value['syringe_seven'].' Syringe With 2- Niddles' ?></i><br>
													<?php } ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][syringe_one]" value="<?php echo $value['syringe_one'] ?>"  class="form-control po_qty_class" />
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][syringe_two]" value="<?php echo $value['syringe_two'] ?>"  class="form-control po_qty_class" />
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][syringe_three]" value="<?php echo $value['syringe_three'] ?>"  class="form-control po_qty_class" />
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][syringe_four]" value="<?php echo $value['syringe_four'] ?>"  class="form-control po_qty_class" />
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][syringe_five]" value="<?php echo $value['syringe_five'] ?>"  class="form-control po_qty_class" />
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][syringe_six]" value="<?php echo $value['syringe_six'] ?>"  class="form-control po_qty_class" />
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][syringe_seven]" value="<?php echo $value['syringe_seven'] ?>"  class="form-control po_qty_class" />

												</td>

												<td id="input_mt_qty_label<?php echo $key ?>" class="text-left"><?php echo $value['input_unit'] ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][input_unit]" value="<?php echo $value['input_unit'] ?>"  class="form-control po_qty_class" />
												</td>

												<td id="input_mt_qty_label<?php echo $key ?>" class="text-right allqtyTotals"><?php echo $value['input_qty'] ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][input_qty]" value="<?php echo $value['input_qty'] ?>"  class="form-control po_qty_class" />
												</td>

												<td id="input_mt_qty_label<?php echo $key ?>" class="text-right allPurchaseTotals" ><?php echo $value['input_purchase_prise'] ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][input_purchase_prise]" value="<?php echo $value['input_purchase_prise'] ?>"  class="form-control po_qty_class" />
												</td>

												<td id="input_mt_qty_label<?php echo $key ?>" class="text-right allValueTotals"><?php echo $value['input_value'] ?>
													<input type="hidden" name="productraw_datas[<?php echo $key ?>][input_value]" value="<?php echo $value['input_value'] ?>"  class="form-control po_qty_class" />
												</td>

												<td class="text-left"><button onclick="remove_reco(<?php echo $key ?>)" class="btn btn-danger" id="remove<?php echo $key ?>" ><i class="fa fa-minus-circle"></i></button></td>
											</tr>

										<?php } ?>
										
									</tbody>
									
								</table>
							</div>
							<div class="form-group" style="border-top: 0;">
		                        <label class="col-sm-2 control-label" for="input-trainer"><?php echo "Total Medicine:"; ?></label>
		                        <div class="col-sm-3">
		                             <input type="text" name="total_item"  placeholder="<?php echo "Total Medicine"; ?>" value="0" id="total_item" class="form-control"  readonly="readonly"/>
		                    	</div>
		                        <label style="" class="col-sm-1 control-label" for="input-date"><?php echo 'Total Qty'; ?></label>
					            <div style="" class="col-sm-2">
					            	<input type="text" name="total_qty" value="0" placeholder="<?php echo 'Total Qty'; ?>" id="total_qty" class="form-control"  readonly="readonly"/>
					        	</div>
								<label class="col-sm-1 control-label" for="input-trainer"><?php echo "Total:"; ?></label>
		                        <div class="col-sm-3">
		                             <input type="text" name="total"  placeholder="<?php echo "Total"; ?>" value="0" id="total" class="form-control"  readonly="readonly"/>
		                        </div>
		                    </div>
		                    <input type="hidden" id="last_owner_id" value="0" class="form-control last_owner_id" />
		                    <input type="hidden" id="selected_pre_owners" value="0" class="form-control selected_pre_owners" />
		                    <input type="hidden"  id="is_owners" class="is_owners">
		                     <div class="form-group " style="border-top: 0; height: 100%;display: flex;justify-content: center;align-items: center;" >
			                    <button onclick="confirm('<?php echo 'Do You want to Save the Changes'; ?>') ? $('#form-manufacturer').submit() : false;" type="button" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary">Save</button>
			                </div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY',
});

$(document).ready(function(){
	$('#input-code').focus();
});



 $('#filter_parent_doctor').autocomplete({
        delay: 500,
        source: function(request, response) {
            if(request != ''){
             $('#filterParentId').val('');
                $.ajax({
                    url: 'index.php?route=catalog/treatment_entry/autocompleteParentDoc&token=<?php echo $token; ?>&filter_doctor_code=' +  encodeURIComponent(request.term),
                    dataType: 'json',
                    success: function(json) {   
                        response($.map(json, function(item) {
                            return {
                                label: item.doctor_name,
                                value: item.doctor_name,
                                doctor_id:item.id,
                                is_parent:item.is_parent,
                                parent_id:item.parent_id,
                                doc_code : item.doctor_code,
                            }
                        }));
                    }
                });
            }
        }, 
        select: function(event, ui) {
            $('#filter_parent_doctor').val(ui.item.value);
            $('#filterParentId').val(ui.item.doctor_id);
            $("#input-code").val(ui.item.doc_code);
            $('.dropdown-menu').hide();
            if ((ui.item.is_parent == 0) && (ui.item.parent_id != 0)) {
            	$("#label_to_doc").show();
            	$("#div_to_doc").show();
            } else {
            	$("#label_to_doc").hide();
            	$("#div_to_doc").hide();
            }

            if(ui.item.doctor_id != ''){
            $.ajax({
                    url: 'index.php?route=catalog/treatment_entry/autocompleteChildDoc&token=<?php echo $token; ?>&parent_doc_id=' +  encodeURIComponent(ui.item.doctor_id),
                    dataType: 'json',
                    cache: false,
                    success: function(json) {
					    $('#input_to_doc').find('option').remove();
					     $('#input_to_doc').append($("<option disabled='disabled' selected='true'></option>").attr("value", '').text('Please Select'));
					    if(json){
					    	if (json.length <= 0) {
					    		$("#label_to_doc").hide();
				            	$("#div_to_doc").hide();
					    	} else {
					    		$("#label_to_doc").show();
				            	$("#div_to_doc").show();
					    	}
						    $.each(json, function (i, item) {
						      $('#input_to_doc').append($('<option>', { 
						        value: item.id,
						        text : item. doctor_name 
						      }));
						    });
						    $('#input_to_doc').focus();
					    }
                       
                    }
                }); 
            }

            
            return false;
        },
    });

	 $('#input_item_name').autocomplete({
	      delay: 500,
	      source: function(request, response) {
	          if(request != ''){
          		$('#input_item_code').val('');
	       		$('#input_qty').val('');
	       		$('#input_qty_hidden').val('');
	       		$('#input_purchase_prise').val('');
	       		$('#input_value').val('');
	       		$('#input_unit').val('');
	         	$('#input_unit').html('');

	              $.ajax({
	                  url: 'index.php?route=catalog/medicine/autocomplete&token=<?php echo $token; ?>&filter_medicine_name=' +  encodeURIComponent(request.term), 
	                  dataType: 'json',
	                  success: function(json) { 
	                      response($.map(json, function(item) {
	                          return {
	                              label: item.med_name,
	                              value: item.med_name,
	                              med_code: item.med_code,
	                              normal: item.normal,
	                              purchase_price: item.purchase_price,
	                              final_price: item.final_price,
	                              store_unit: item.store_unit,
	                               med_type : item.med_type
	                              
	                          }
	                      }));
	                  }
	              });
	          }
	      }, 
	      select: function(event, ui) {
          $('#input_item_name').val(ui.item.value);
	       $('#input_item_code').val(ui.item.med_code);
	       $('#input_qty').val(parseInt(ui.item.normal));
	        $('#input_qty_hidden').val(parseInt(ui.item.normal));
	        $('#input_value').val(ui.item.final_price);
	       $('#input_purchase_prise').val(ui.item.purchase_price);
	        $('#input_unit').val(ui.item.store_unit);
	         $('#input_unit').html('');
	          $('#input_unit').html(ui.item.store_unit);
	        $('#med_type').val(ui.item.med_type);
	        if(ui.item.med_type == 'INJ'){
	        	checkSyringe(ui.item.normal)
	        }

	           $('#input_qty').focus();
          $('.dropdown-menu').hide();
	          return false;
	      },
	  });

	  $('#input_item_code').autocomplete({
	      delay: 500,
	      source: function(request, response) {
	          if(request != ''){
	       		$('#input_qty').val('');
	       		$('#input_qty_hidden').val('');
	       		$('#input_purchase_prise').val('');
	       		$('#input_value').val('');
	       		$('#input_unit').val('');
	         	$('#input_unit').html('');

	              $.ajax({
	                  url: 'index.php?route=catalog/medicine/autocomplete&token=<?php echo $token; ?>&filter_item_code=' +  encodeURIComponent(request.term), 
	                  dataType: 'json',
	                  success: function(json) { 
	                      response($.map(json, function(item) {
	                          return {
	                              label: item.med_code,
	                              value: item.med_code,
	                              med_name: item.med_name,
	                              normal: item.normal,
	                              purchase_price: item.purchase_price,
	                              final_price: item.final_price,
	                              store_unit: item.store_unit,
	                              med_type : item.med_type,
	                              
	                          }
	                      }));
	                  }
	              });
	          } 
	      }, 
	      select: function(event, ui) {
          $('#input_item_name').val(ui.item.med_name);
	       $('#input_item_code').val(ui.item.value);
	       $('#input_qty').val(parseInt(ui.item.normal));
	        $('#input_qty_hidden').val(parseInt(ui.item.normal));
	        $('#input_value').val(ui.item.final_price);
	       $('#input_purchase_prise').val(ui.item.purchase_price);
	        $('#input_unit').val(ui.item.store_unit);
	         $('#input_unit').html('');
	          $('#input_unit').html(ui.item.store_unit);
	        $('#med_type').val(ui.item.med_type);
	        if(ui.item.med_type == 'INJ'){
	        	checkSyringe(ui.item.normal)
	        }

	           $('#input_qty').focus();
          $('.dropdown-menu').hide();
	          return false;
	      },
	  });

	   $("#input_qty").keyup(function()  {

   		var input_valid =  $('#input_qty').val() ;
		var input_valid1 = input_valid.replace(/[^0-9]+/i, '');
		$("#input_qty").val(input_valid1);
		original_qty = $("#input_qty_hidden").val();
		
		var quantity =  $('#input_qty').val() || 0;
		var input_purchase_prise =  $('#input_purchase_prise').val();
		oneml_price = parseFloat(input_purchase_prise) * 1 / parseFloat(original_qty);
		var final_value = quantity * oneml_price;

		$('#input_value').val((final_value.toFixed(2)));

	    medtype = $('#med_type').val();
	    if(medtype == 'INJ' && parseInt(quantity) > 0){
	    	checkSyringe(quantity);
	    }
	});


	   function checkSyringe(qty){
		med_qty = parseInt(qty);
		//alert(med_qty + ' idss ' + id);
		
		$('#current_syringe1').html('');
		$('#current_syringe2').html('');
		$('#current_syringe3').html('');
		$('#current_syringe4').html('');
		$('#current_syringe5').html('');
		$('#current_syringe6').html('');
		$('#current_syringe7').html('');

		$('#select1').hide();
		$('#select2').hide();
		$('#select3').hide();
		$('#select4').hide();
		$('#select5').hide();
		$('#select6').hide();
		$('#select7').hide();

		$('#syringe_select1').prop("checked", false);
		$('#syringe_select2').prop("checked", false);
		$('#syringe_select3').prop("checked", false);
		$('#syringe_select4').prop("checked", false);
		$('#syringe_select5').prop("checked", false);
		$('#syringe_select6').prop("checked", false);
		$('#syringe_select7').prop("checked", false);

		if(med_qty > 0 && med_qty <= 5){
			$('#select1').show();
			$('#syringe_select1').prop("checked", true);
			$('#current_syringe1').append('5ml - Syringe with 2 Needle ');
		} else if(med_qty > 5 && med_qty <= 10) {
			$('#select1').show();
			$('#syringe_select1').prop("checked", true);
			$('#current_syringe1').append('10ml - Syringe with 2 Needle ');
		}  else if(med_qty > 10 && med_qty <= 15) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);

			$('#current_syringe1').append('10ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('5ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');

		} else if(med_qty > 15 && med_qty <= 20) {
			$('#select1').show();
			$('#syringe_select1').prop("checked", true);
			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
		} else if(med_qty > 20 && med_qty <= 25) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('5ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('10ml - Syringe with 2 Needle ');
		} else if(med_qty > 25 && med_qty <= 30) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('10ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
		} else if(med_qty > 30 && med_qty <= 35) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();

			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('10ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('5ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('20ml - Syringe with 2 Needle ');

		} else if(med_qty > 35 && med_qty <= 40) {
			$('#select1').show();
			$('#select2').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			
		} else if(med_qty > 40 && med_qty <= 45) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('5ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('10ml - Syringe with 2 Needle ');
			
		}  else if(med_qty > 45 && med_qty <= 50) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('10ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('5ml - Syringe with 2 Needle ');
			
		}  else if(med_qty > 50 && med_qty <= 55) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			$('#select5').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			$('#syringe_select4').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('10ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('5ml - Syringe with 2 Needle ');
			$('#current_syringe5').append('10ml - Syringe with 2 Needle ');
			
		} else if(med_qty > 55 && med_qty <= 60) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			
			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
			
		} else if(med_qty > 60 && med_qty <= 65) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			$('#select5').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			$('#syringe_select4').prop("checked", true);
			
			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('5ml - Syringe with 2 Needle ');
			$('#current_syringe5').append('10ml - Syringe with 2 Needle ');

			
		}  else if(med_qty > 65 && med_qty <= 70) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			$('#syringe_select4').prop("checked", true);
			
			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('10ml - Syringe with 2 Needle ');
		}  else if(med_qty > 70 && med_qty <= 75) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			$('#select5').show();
			$('#select6').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			$('#syringe_select4').prop("checked", true);
			$('#syringe_select5').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('10ml - Syringe with 2 Needle ');
			$('#current_syringe5').append('5ml - Syringe with 2 Needle ');
			$('#current_syringe6').append('20ml - Syringe with 2 Needle ');


		} else if(med_qty > 75 && med_qty <= 80) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			$('#syringe_select4').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('20ml - Syringe with 2 Needle ');
		} else if(med_qty > 80 && med_qty <= 85) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			$('#select5').show();
			$('#select6').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			$('#syringe_select4').prop("checked", true);
			$('#syringe_select5').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe5').append('5ml - Syringe with 2 Needle ');
			$('#current_syringe6').append('10ml - Syringe with 2 Needle ');

		} else if(med_qty > 85 && med_qty <= 90) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			$('#select5').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			$('#syringe_select4').prop("checked", true);
			$('#syringe_select5').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe5').append('10ml - Syringe with 2 Needle ');

		} else if(med_qty > 90 && med_qty <= 95) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			$('#select5').show();
			$('#select6').show();
			$('#select7').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			$('#syringe_select4').prop("checked", true);
			$('#syringe_select5').prop("checked", true);
			$('#syringe_select6').prop("checked", true);

			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe5').append('10ml - Syringe with 2 Needle ');
			$('#current_syringe6').append('5ml - Syringe with 2 Needle ');
			$('#current_syringe7').append('10ml - Syringe with 2 Needle ');

		}  else if(med_qty > 90 && med_qty <= 95) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			$('#select5').show();
			$('#select6').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			$('#syringe_select4').prop("checked", true);
			$('#syringe_select5').prop("checked", true);
			$('#syringe_select6').prop("checked", true);
			
			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe5').append('10ml - Syringe with 2 Needle ');
			$('#current_syringe6').append('5ml - Syringe with 2 Needle ');
			$('#current_syringe7').append('20ml - Syringe with 2 Needle ');

			
		}  else if(med_qty > 95 && med_qty <= 100) {
			$('#select1').show();
			$('#select2').show();
			$('#select3').show();
			$('#select4').show();
			$('#select5').show();
			
			$('#syringe_select1').prop("checked", true);
			$('#syringe_select2').prop("checked", true);
			$('#syringe_select3').prop("checked", true);
			$('#syringe_select4').prop("checked", true);
			$('#syringe_select5').prop("checked", true);
			
			$('#current_syringe1').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe2').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe3').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe4').append('20ml - Syringe with 2 Needle ');
			$('#current_syringe5').append('20ml - Syringe with 2 Needle ');
		}
		$('.syng_div').show();
	}


	//enter button

	document.getElementById("input_qty").onkeypress = function(event){
	    if (event.keyCode == 13 || event.which == 13){
	    	var qty = $('#input_qty').val();
	    	var extra_field = $('#tbladdMedicineTran >tbody >tr').length;
	    	if (qty != '') {
	    		 $('.imp_div').show();
			    var input_item_name = $('#input_item_name').val();
			 	var input_item_code = $('#input_item_code').val();
			  	var input_qty = $('#input_qty').val();
			   	var input_value = $('#input_value').val();
			    var input_purchase_prise =   $('#input_purchase_prise').val();
			    var input_unit = $('#input_unit').val();
			    var med_type = $('#med_type').val();
			    sy1 = $('#syringe_select1').val();
				sy2 = $('#syringe_select2').val();
				sy3 = $('#syringe_select3').val();
				sy4 = $('#syringe_select4').val();
				sy5 = $('#syringe_select5').val();
				sy6 = $('#syringe_select6').val();

				sy1_val = ($('#syringe_select1').is(':checked')) ? ($('#current_syringe1').text() != '') ? $('#current_syringe1').text().split('-') : 0 : 0;
				sy2_val = ($('#syringe_select2').is(':checked')) ? ($('#current_syringe2').text() != '') ? $('#current_syringe2').text().split('-') : 0 : 0;
				sy3_val = ($('#syringe_select3').is(':checked')) ? ($('#current_syringe3').text() != '') ? $('#current_syringe3').text().split('-') : 0 : 0;
				sy4_val = ($('#syringe_select4').is(':checked')) ? ($('#current_syringe4').text() != '') ? $('#current_syringe4').text().split('-') : 0 : 0;
				sy5_val = ($('#syringe_select5').is(':checked')) ? ($('#current_syringe5').text() != '') ? $('#current_syringe5').text().split('-') : 0 : 0;
				sy6_val = ($('#syringe_select6').is(':checked')) ? ($('#current_syringe6').text() != '') ? $('#current_syringe6').text().split('-') : 0 : 0;
				sy7_val = ($('#syringe_select7').is(':checked')) ? ($('#current_syringe7').text() != '') ? $('#current_syringe7').text().split('-') : 0 : 0;

				final_sry1 = (sy1_val != 0) ? sy1_val[0] : "";
				final_sry2 = (sy2_val != 0) ? sy2_val[0] : "";
				final_sry3 = (sy3_val != 0) ? sy3_val[0] : "";
				final_sry4 = (sy4_val != 0) ? sy4_val[0] : "";
				final_sry5 = (sy5_val != 0) ? sy5_val[0] : "";
				final_sry6 = (sy6_val != 0) ? sy6_val[0] : "";
				final_sry7 = (sy7_val != 0) ? sy7_val[0] : "";


				html = '';
				html += '<tr id="productraw_row' + extra_field + '">';
					html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">'+input_item_code;
						html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_item_code]" value="'+input_item_code+'"  class="form-control po_qty_class" />';
					html += '</td>';

					html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">'+input_item_name+'<br />';
						html += '<input type="hidden" name="productraw_datas['+extra_field+'][item_name]" value="'+input_item_name+'"  class="form-control po_qty_class" />';
						html +=  '<span style="font-style: italic;" id="lbl1_syringe_'+extra_field+'"> '+ sy1_val +' </span><br>';
						if(sy2_val != 0){
							html +=  '<span style="font-style: italic;" id="lbl2_syringe_'+extra_field+'"> '+ sy2_val +' </span><br>';
						}
						if(sy3_val != 0){
							html +=  '<span style="font-style: italic;" id="lbl3_syringe_'+extra_field+'"> '+ sy3_val +' </span><br>';
						}

						if(sy4_val != 0){
							html +=  '<span style="font-style: italic;" id="lbl4_syringe_'+extra_field+'"> '+ sy4_val +' </span><br>';
						}
						if(sy5_val != 0){
							html +=  '<span style="font-style: italic;" id="lbl5_syringe_'+extra_field+'"> '+ sy5_val +' </span><br>';
						}
						if(sy6_val != 0){
							html +=  '<span style="font-style: italic;" id="lbl6_syringe_'+extra_field+'"> '+ sy6_val +' </span><br>';
						}
						if(sy7_val != 0){
							html +=  '<span style="font-style: italic;" id="lbl7_syringe_'+extra_field+'"> '+ sy7_val +' </span><br>';
						}

						html += '<input type="hidden" id="syringe1_'+extra_field+'" name="productraw_datas['+extra_field+'][syringe_one]" value="'+ final_sry1 +'"  class="form-control po_qty_class" />';
						html += '<input type="hidden" id="syringe2_'+extra_field+'" name="productraw_datas['+extra_field+'][syringe_two]" value="'+ final_sry2 +'"  class="form-control po_qty_class" />';
						html += '<input type="hidden" id="syringe3_'+extra_field+'" name="productraw_datas['+extra_field+'][syringe_three]" value="'+ final_sry3 +'"  class="form-control po_qty_class" />';
						html += '<input type="hidden" id="syringe4_'+extra_field+'" name="productraw_datas['+extra_field+'][syringe_four]" value="'+ final_sry4 +'"  class="form-control po_qty_class" />';
						html += '<input type="hidden" id="syringe5_'+extra_field+'" name="productraw_datas['+extra_field+'][syringe_five]" value="'+ final_sry5 +'"  class="form-control po_qty_class" />';
						html += '<input type="hidden" id="syringe6_'+extra_field+'" name="productraw_datas['+extra_field+'][syringe_six]" value="'+ final_sry6 +'"  class="form-control po_qty_class" />';
						html += '<input type="hidden" id="syringe7_'+extra_field+'" name="productraw_datas['+extra_field+'][syringe_seven]" value="'+ final_sry7 +'"  class="form-control po_qty_class" />';
					html += '</td>';

					html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">' + input_unit + '';
						html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_unit]" value="'+input_unit+'"  class="form-control po_qty_class" />';
					html += '</td>';

					html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allqtyTotals">'+ input_qty +'';
						html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_qty]" value="'+input_qty+'"  class="form-control po_qty_class" />';
					html += '</td>';

					html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allPurchaseTotals">' + input_purchase_prise + '';
						html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_purchase_prise]" value="'+input_purchase_prise+'"  class="form-control po_qty_class" />';
					html += '</td>';

					html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allValueTotals">' + input_value + '';
						html += '<input type="hidden" name="productraw_datas['+extra_field+'][input_value]" value="'+input_value+'"  class="form-control po_qty_class" />';
					html += '</td>';

					html += '<td id="input_mt_qty_label'+extra_field+'" class="text-center">';
						html += '<a  class="btn btn-danger" onclick="remove_reco('+extra_field+')"><i class="fa fa-minus-circle"></i></a>';
					html += '</td>';

					
				html += '</tr>';
				console.log(html);
			  	$('#test').append(html);
			  	if(med_type == 'INJ'){
		        	checkSyringe(input_qty, extra_field);
			    }
			  	$('#input_item_name').val('');
		  		$('#input_item_code').val('');
		  		$('#input_batch').val('');
		   		$('#input_qty').val('');
		   		$('#input_qty_hidden').val('');
		   		$('#input_purchase_prise').val('');
		   		$('#input_value').val('');
		   		 $('#input_value').attr('value', '');
		   		 $('#input_unit').val('');
		   		 $('#input_unit').attr('value', '');
		   		 $('#input_unit').html('');
	         
				
			  	extra_field++;

			    $('#tbladdMedicineTran').each(function (i, elem) {
			     var totalqty = 0;
			     var totalvalue = 0;
			     $(elem).find("td.allqtyTotals").each(function(j, elem2) {
			       totalqty += parseFloat($(elem2).html());
			     });
			    
			     $(elem).find("td.allValueTotals").each(function(j, elem2) {
			       totalvalue += parseFloat($(elem2).html());
			     })
			     var count_item = $('#tbladdMedicineTran >tbody >tr').length;
			      $('#total_item').val('');
			      $('#total').val('');
			      $('#total_qty').val('');

			      $('#total_item').attr('value', count_item);
			      $('#total').attr('value', totalvalue.toFixed(2));
			      $('#total_qty').attr('value', totalqty);
			       $('#total_item').val(count_item);
			      $('#total').val(totalvalue.toFixed(2));
			      $('#total_qty').val(totalqty);
			       $('#input_item_code').focus();
			   });
	    	} else {
				alert('Please Enter Quantity!');
				return false;
				$('#input_qty').focus();
			}
			$('#current_syringe1').html('');
			$('#current_syringe2').html('');
			$('#current_syringe3').html('');
			$('#current_syringe4').html('');
			$('#current_syringe5').html('');
			$('#current_syringe6').html('');
			$('#current_syringe7').html('');

			$('#select1').hide();
			$('#select2').hide();
			$('#select3').hide();
			$('#select4').hide();
			$('#select5').hide();
			$('#select6').hide();
			$('#select7').hide();

			$('#syringe_select1').prop("checked", false);
			$('#syringe_select2').prop("checked", false);
			$('#syringe_select3').prop("checked", false);
			$('#syringe_select4').prop("checked", false);
			$('#syringe_select5').prop("checked", false);
			$('#syringe_select6').prop("checked", false);
			$('#syringe_select7').prop("checked", false);

	    }
	};

function remove_reco(extra_field){
	$('#productraw_row'+extra_field).remove();
	$('#tbladdMedicineTran').each(function (i, elem) {
     var totalqty = 0;
     var totalvalue = 0;
     $(elem).find("td.allqtyTotals").each(function(j, elem2) {
       totalqty += parseFloat($(elem2).html());
     });
    
     $(elem).find("td.allValueTotals").each(function(j, elem2) {
       totalvalue += parseFloat($(elem2).html());
     })
     var count_item = $('#tbladdMedicineTran >tbody >tr').length;
      $('#total_item').val('');
      $('#total').val('');
      $('#total_qty').val('');

      $('#total_item').attr('value', count_item);
      $('#total').attr('value', totalvalue.toFixed(2));
      $('#total_qty').attr('value', totalqty);
       $('#total_item').val(count_item);
      $('#total').val(totalvalue.toFixed(2));
      $('#total_qty').val(totalqty);

   });
}

$('#trainer_name').autocomplete({
    delay: 500,
    source: function(request, response) {
        $('#trainer_id').val('');
        $('#input-req').val(0);
        if(request != ''){
            $.ajax({
                url: 'index.php?route=catalog/trainer/autocompleteTrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {   
                    $('#trainer_codes_id').find('option').remove();
                    response($.map(json, function(item) {
                        return {
                            label: item.trainer_name,
                            value: item.trainer_name,
                            trainer_id:item.trainer_id,
                            trainer_codes:item.trainer_code
                        }
                    }));
                }
            });
        }
    }, 
    select: function(event, ui) {
    	console.log('trainer idddd '+ ui.item.trainer_id)
        $('#trainer_name').val(ui.item.value);
        $('#hidden_trainer_name_id').val(ui.item.trainer_id);
        $('#input-req').val(1);
        if(ui.item.horse_id != ''){
            $.ajax({
                url: 'index.php?route=catalog/treatment_entry/autocompletehorse&token=<?php echo $token; ?>&trainer_id=' +  encodeURIComponent(ui.item.trainer_id),
                dataType: 'json',
                cache: false,
                success: function(json) {
                	console.log(json);
                    if(json && json.length > 0){
                    	$('.lblhorse').show(); 
                		$('.removecheckdiv').remove(); 
                		$("#horse_div").empty()
						 $('#horse_div').append('<div class="well well-sm removecheckdiv" style="height: 150px; overflow: auto;"></div>');
	                    $.each(json, function (i, item) {
	                      $('.removecheckdiv').append('<div class="checkbox"><label><input type="checkbox" id="checkeds" class="checkeds" name="horse_datas['+i+'][horse_id]" value='+item.horse_id +'><input type="hidden" name="horse_datas['+i+'][horse_name]" value='+item.horse_name +'> '+item.horse_name+'</label></div>');
	                    });
	                    onclickss = "$(this).parent().find(':checkbox').prop('checked',true)";
	                    onclickss1 = "$(this).parent().find(':checkbox').prop('checked',false)";
	                    //$('#horse_div').append('<input onclick="$(this).parent().find(':checkbox').prop('checked', true);" type="checkbox" >');
	                    $('#horse_div').append('<div class="checkbox"><label><input type="checkbox" id="link" class="link" />'+' Select All/Unselect All</label></div>');
	                    //$('#horse_div').append('<a onclick='+onclickss+' class="link">Select All</a> / <a onclick='+onclickss1+' class="link">Unselect All</a>');
	                    
                   } else {
                    	$('#horse_div').append('<span style = "color:red;">Please Assign Horse</span>');
                    }
                   
                }
            }); 
        }
        $('.dropdown-menu').hide();
        $('#input_item_code').focus();
      
        return false;
    },
});

$(document).on('change', '#link', function(){ 
	console.log($(this).prop('checked') );
    if ($(this).prop('checked') == true) {
        $('.checkeds').prop('checked', true);
    } else {
        $('.checkeds').prop('checked', false);
    }
});

$(document).on('keydown', '#input_item_code, #input_item_name, #input_to_doc, #filter_parent_doctor', function(e) {
	if(e.which == 13){
		//alert('inn');
	  	var class_name = $(this).attr('class'); 
	  	var id = $(this).attr('id');
	  	var value = $(this).val();
	  	
		if(value == '' && id == 'input_item_code'){
			$('#input_item_name').focus();
		}

		if(value == '' && id == 'input_item_name'){
			alert("Please Select Medicine Name OR Medicine Code !");
			return false;
		}

		if(id == 'input_to_doc'){
			$('#trainer_name').focus();
		}

		if(id == 'filter_parent_doctor' && value == ''){
			//$('#trainer_name').focus();
			alert("Please Enter Clinic Name OR Clinic Code !");
			return false;
		}

	}
});

$(document).on('keypress', '#input-code', function(){
    if (event.keyCode == 13 || event.which == 13){
    	var code = $("#input-code").val();
    	if (code != '') {
	    	$.ajax({
            	url: 'index.php?route=catalog/treatment_entry/clinic_search&token=<?php echo $token; ?>&code='+code, 
                dataType: 'json',
                success: function(json) {
                	if (json.alert != '') {
                		$('#label_to_doc').hide();
            			$('#div_to_doc').hide();
            			$('#filter_parent_doctor').val('');
            			$('#filterParentId').val('');
            			$('#input_to_doc').val('');
    					$('#trainer_name').focus();

            			return false;
                	} else {
	                  	if(json.success == 1){
		                  	$('#filter_parent_doctor').val(json.parent);
					       	$('#filterParentId').val(json.parent_id);
		            		$('#label_to_doc').show();
		            		$('#div_to_doc').show();
					       	$('#input_to_doc').find('option').remove();
					       	$.each(json.docs, function (i, item) {
						    	$('#input_to_doc').append($('<option>', { 
						        	value: item.doctor_id,
						        	text : item. doctor_name 
						    	}));
						    });
    						$('#trainer_name').focus();

		                  	return false;
		                } else {
		                	$('#filter_parent_doctor').val(json.parent);
					       	$('#filterParentId').val(json.parent_id);
		            		$('#label_to_doc').show();
		            		$('#div_to_doc').show();
		            		if((json.success == 3) || (json.success == 4)){
		            			$('#label_to_doc').hide();
		            			$('#div_to_doc').hide();
		            		}
		            		$('#input_to_doc').find('option').remove();
						    $('#input_to_doc').append($("<option selected='selected'></option>").attr("value", ""+json.doctor_id+"").text(""+json.doctor_name+""));
    						$('#trainer_name').focus();
		                	
		                	return false;
		                }
                	}
                } 
          	});
    	} else {
    		$('#filter_parent_doctor').focus();
    		//alert("Please Enter Code!");
    	}
    }
});

$('.form-control').on('keydown', function(e) { 
    if (e.keyCode == 9) {
        $(this).focus();
       e.preventDefault();
    }
});

$('#input_item_code').click(function () {
	var reqs_by = $("#input-req").val();
	if ((reqs_by == '') || (reqs_by == 0)) {
		//alert("Please Enter Valid Supplier!");
		$('#valid_supplier').show();
		$('#trainer_name').focus();
	} else {
		$('#valid_supplier').hide();
	}
});

$('#input_item_name').click(function () {
	var reqs_by = $("#input-req").val();
	if ((reqs_by == '') || (reqs_by == 0)) {
		//alert("Please Enter Valid Supplier!");
		$('#valid_supplier').show();
		$('#trainer_name').focus();
	} else {
		$('#valid_supplier').hide();
	}
});

$('#input_qty').click(function () {
	var reqs_by = $("#input-req").val();
	if ((reqs_by == '') || (reqs_by == 0)) {
		//alert("Please Enter Valid Supplier!");
		$('#valid_supplier').show();
		$('#trainer_name').focus();
	} else {
		$('#valid_supplier').hide();
	}
});

</script>

<?php echo $footer; ?>