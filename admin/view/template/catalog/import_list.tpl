<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header" >
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $import_tally; ?>" data-toggle="tooltip" title="<?php echo 'Import Tally' ?>" class="btn btn-primary">Import Tally</a>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Go To Dashboard" class="btn btn-primary">Go To Dashboard</a>
        <button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-vendor').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo 'PO List'; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo 'PO List'; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
                  <div class="form-group">
                    <div class="col-sm-3" >
                        <input type="text" name="filter_po_no" value="<?php echo $filter_po_no; ?>" placeholder="<?php echo "PO No"; ?>" id="input-filter_po_no" class="form-control" />
                    </div>
                    <div class="col-sm-3" >
                        <input type="text" name="filter_item" value="<?php echo $filter_item; ?>" placeholder="<?php echo "Item"; ?>" id="input-filter_item" class="form-control" />
                    </div>
                    <div class="col-sm-2" >
                        <div class="input-group date">
                            <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-2" >
                      <select name="filter_status" id="filter_status" class="form-control">
                          <?php foreach ($status as $key => $tvalue) { ?>
                              <?php if($key == $filter_status){ ?>
                                  <option value="<?php echo $key ?>" selected="selected"><?php echo $tvalue?></option>
                              <?php } else { ?>
                              <option value="<?php echo $key ?>"><?php echo $tvalue?></option>
                              <?php } ?>
                          <?php } ?>    
                      </select>
                    </div>
                    <div class="col-sm-2" >
                        <button onclick="filter();" style="" type="button" id="button-filter" class="btn btn-primary " ><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
                    </div>
                  </div>
          </div>
        </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 5%;" class="text-left">Sr No</td>
                  <td class="text-left">Date</td>
                  <td class="text-left">User</td>
                  <td class="text-left">Po Number</td>
                  <td class="text-left">Item</td>
                  <td class="text-left">Quantity</td>
                  <td class="text-left">Price</td>
                  <td class="text-left">Indent No</td>
                  <td class="text-left">Status</td>
                </tr>
              </thead>
              <tbody>
                <?php if ($import_lists) { ?>
                <?php $i=1; ?>
                <?php foreach ($import_lists as $import_list) { ?>
                <tr>
                    <td class="text-left"><?php echo $i++; ?></td>
                    <td class="text-left"><?php echo date('d-m-Y', strtotime($import_list['date'])); ?></td>
                    <td class="text-left"><?php echo $import_list['user_name']; ?></td>
                    <td class="text-left"><?php echo $import_list['po_no']; ?></td>
                    <td class="text-left"><?php echo $import_list['item']; ?></td>
                    <td class="text-left"><?php echo $import_list['qty']; ?></td>
                    <td class="text-left"><?php echo $import_list['price']; ?></td>
                    <td class="text-left"><?php echo $import_list['indent_no']; ?></td>
                    <td class="text-left"><?php echo $import_list['status']; ?></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
</script>

<script type="text/javascript">
function filter() {
  var url = 'index.php?route=catalog/import_list&token=<?php echo $token; ?>';

  var filter_po_no = $('input[name=\'filter_po_no\']').val();

  if (filter_po_no) {
    url += '&filter_po_no=' + encodeURIComponent(filter_po_no);
  
  }

  var filter_item = $('input[name=\'filter_item\']').val();

  if (filter_item) {
    url += '&filter_item=' + encodeURIComponent(filter_item);
  
  }

  var filter_date = $('input[name=\'filter_date\']').val();

  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  
  }

  var filter_status = $('select[name=\'filter_status\']').val();
  
  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  location = url;
}

$('#input-filter_po_no').autocomplete({
  'source': function(request, response) {
    if(request != ''){
        $.ajax({
          url: 'index.php?route=catalog/import_list/autocomplete&token=<?php echo $token; ?>&filter_po_no=' +  encodeURIComponent(request),
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              return {
                label: item['po_no'],
                value: item['po_no']
              }
            }));
          }
        });
    }
  },
  'select': function(item) {
    $('input[name=\'filter_po_no\']').val(item['label']);
    filter();
    return false;
  }
});
</script>

<?php echo $footer; ?>