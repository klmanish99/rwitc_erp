<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> <a style="display: none;" href="<?php echo $repair; ?>" data-toggle="tooltip" title="<?php echo $button_rebuild; ?>" class="btn btn-default"><i class="fa fa-refresh"></i></a>
        <button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
          <div class="well">
            <div class="row">

              <div class="col-sm-12">
                <div class="form-group">
                  <div class="col-sm-4">
                    <input type="text" name="filterOwnerName" id="filterOwnerName" value="<?php echo $filter_owner_name; ?>" placeholder="Owner Name" class="form-control">
                  </div>
                  <div class="col-sm-4">
                    <input type="text" name="filterOwnerId" id="filterOwnerId" value="<?php echo $filter_owner_id; ?>" placeholder="Owner Code" class="form-control">
                  </div>
                  <div class="col-sm-3">
                    <select name="filter_status" id="filter_status" class="form-control">
                        <?php foreach ($status as $key => $tvalue) { ?>
                            <?php if($key == $filter_status){ ?>
                              <option value="<?php echo $key ?>" selected="selected"><?php echo $tvalue?></option>
                            <?php } else { ?>
                              <option value="<?php echo $key ?>"><?php echo $tvalue?></option>
                            <?php } ?>
                        <?php } ?>    
                    </select>
                  </div>
                  <a onclick="filter();"  id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
                </div>
              </div>
            </div>
          </div>
        <div class="col-sm-12">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="display: none;" "width: 1px;" class="text-center"><input style="display: none;" type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  
                 <!--  <td >Owner Code</td> -->
                  <td class="text-center"><?php if ($sort == 'sr_no') { ?>
                      <a href="<?php echo $sr_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Sr No'; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $sr_no; ?>"><?php echo 'Sr No'; ?></a>
                      <?php } ?>
                  </td>
                  <td class="text-center"><?php if ($sort == 'owner_name') { ?>
                      <a href="<?php echo $owner_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Owner Name'; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $owner_name; ?>"><?php echo 'Owner Name'; ?></a>
                      <?php } ?>
                  </td>
                  <td class="text-center"><?php if ($sort == 'type_of_ownership') { ?>
                      <a href="<?php echo $type_of_ownership; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Ownership Type'; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $type_of_ownership; ?>"><?php echo 'Ownership Type'; ?></a>
                      <?php } ?>
                  </td>
                  <td class="text-center"><?php if ($sort == 'mobile_no') { ?>
                      <a href="<?php echo $mobile_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Mobile Number'; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $mobile_no; ?>"><?php echo 'Mobile Number'; ?></a>
                      <?php } ?>
                  </td>
                  <td class="text-center"><?php if ($sort == 'email_id') { ?>
                      <a href="<?php echo $email_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Email Id'; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $email_id; ?>"><?php echo 'Email Id'; ?></a>
                      <?php } ?>
                  </td>
                  <td class="text-center"><?php if ($sort == 'pan_no') { ?>
                      <a href="<?php echo $pan_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Pan Number'; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $pan_no; ?>"><?php echo 'Pan Number'; ?></a>
                      <?php } ?>
                  </td>
                 <td class="text-center"><?php if ($sort == 'gst_no') { ?>
                      <a href="<?php echo $gst_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'GST Number'; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $gst_no; ?>"><?php echo 'GST Number'; ?></a>
                      <?php } ?>
                  </td>
                  <td class="text-center"><?php if ($sort == 'status') { ?>
                      <a href="<?php echo $statuses; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Status'; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $statuses; ?>"><?php echo 'Status'; ?></a>
                      <?php } ?>
                  </td>
                  <td class="text-center"><?php if ($sort == 'no_of_horse') { ?>
                      <a href="<?php echo $no_of_horse; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'No Of Horse'; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $no_of_horse; ?>"><?php echo 'No Of Horse'; ?></a>
                      <?php } ?>
                  </td>
                  <td class="text-center"><?php if ($sort == 'ban') { ?>
                      <a href="<?php echo $ban; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'UFL/BTF'; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $ban; ?>"><?php echo 'UFL/BTF'; ?></a>
                      <?php } ?>
                  </td>
                  <!-- <td>Approval Date</td> -->
                  <td>Action</td>
                </tr>
              </thead>
              <tbody>
                <?php
                if (isset($ownersDatas)) { ?>
                  <?php $i=1; ?>
                  <?php foreach ($ownersDatas as $ownersData) { ?>
                  <tr>
                      <td style="display: none;" class="text-center"><?php if (in_array($ownersData['ownerId'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $ownersData['ownerId']; ?>" checked="checked" />
                      <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $ownersData['ownerId']; ?>" />
                    <?php } ?></td>
                    <td class="text-left"><?php echo $i++; ?></td>
                  <!--  <td class="text-left"><?php echo $ownersData['ownerId']; ?></td> -->
                      <td class="text-left"><?php echo $ownersData['ownerName']; ?></td>
                      <td class="text-left"><?php echo $ownersData['ownershipType']; ?></td>
                      <td class="text-left"><?php echo $ownersData['mobileNo']; ?></td>
                      <td class="text-left"><?php echo $ownersData['emailId']; ?></td>
                      <td class="text-left"><?php echo $ownersData['panNumber']; ?></td>
                      <td class="text-left"><?php echo $ownersData['gst_type']; ?></td>
                      <?php if ($ownersData['ownerStatus'] == '1') { ?>
                        <td class="text-left">In</td>
                      <?php } else { ?>
                        <td class="text-left">Exit</td>
                      <?php } ?>
                      <td class="text-left"><?php echo $ownersData['count']; ?></td>
                      <!-- <td class="text-left"><?php echo $ownersData['approvalDate']; ?></td> -->
                       <td class="text-left"><?php echo $ownersData['ban_amount']; ?></td> 
                      <td style="width: 10%; " class="col-sm-2">
                        <a href="<?php echo $ownersData['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo $ownersData['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                      </td>
                  </tr>
                <?php } ?>
                <?php } ?>
              </tbody>
            </table>
          </div>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
  function filter() { 
      var filter_owner_name = $('#filterOwnerName').val();
      var filter_owner_id = $('#filterOwnerId').val();
      //var filter_status = $('#filter_status').val();
      var filter_status = $('select[name=\'filter_status\']').val();
      //if(filter_owner_name != '' || filter_owner_id != ''){
        url = 'index.php?route=catalog/owner&token=<?php echo $token; ?>';
      if (filter_owner_name) {
        url += '&filter_owner_name=' + encodeURIComponent(filter_owner_name);
      }

      if (filter_owner_id) {
        url += '&filter_owner_id=' + encodeURIComponent(filter_owner_id);
      }

      if (filter_status) {
        url += '&filter_status=' + encodeURIComponent(filter_status);
      }

      window.location.href = url;
      /*} else {
      alert('Please select the filters');
      return false;
      }*/
  }

  $('#filterOwnerName').autocomplete({
    delay: 500,
    source: function(request, response) {
        $('#trainer_id').val('');
        if(request != ''){
            $.ajax({
                url: 'index.php?route=catalog/owner/autocompleteOwner&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {   
                    $('#trainer_codes_id').find('option').remove();
                    response($.map(json, function(item) {
                        return {
                            label: item.trainer_name,
                            value: item.trainer_name,
                            trainer_id:item.trainer_id,
                        }
                    }));
                }
            });
        }
    }, 
    select: function(item) {
        console.log(item);
        $('#filterOwnerName').val(item.value);
        $('#trainer_id').val(item.trainer_id);
        $('.dropdown-menu').hide();
        filter();
        return false;
    },
  });
</script>