<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> <!-- <a href="<?php echo $repair; ?>" data-toggle="tooltip" title="<?php echo $button_rebuild; ?>" class="btn btn-default"><i class="fa fa-refresh"></i></a> -->
        <button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
          <div class="well">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <div class="col-sm-4">
                    <input type="text" name="filter_medicine_type" id="filter_medicine_type" value="" placeholder="Medicine Type" class="form-control">
                  </div>
                  <!-- <div class="col-sm-4">
                    <input type="text" name="filterMedicineId" id="filterMedicineId" value="" placeholder="Medicine Code" class="form-control">
                  </div> -->
                  <div class="col-sm-3">
                    <select name="filter_status" id="filter_status" class="form-control">
                        <?php foreach ($status as $key => $tvalue) { ?>
                            <?php if($key == $filter_status){ ?>
                              <option value="<?php echo $key ?>" selected="selected"><?php echo $tvalue?></option>
                            <?php } else { ?>
                              <option value="<?php echo $key ?>"><?php echo $tvalue?></option>
                            <?php } ?>
                        <?php } ?>    
                    </select>
                  </div>
                  <a onclick="filter();"  id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="display: none;" "width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td style="width: 5%;" class="text-left">Sr No</td>
                  <td class="text-left"><?php echo "Medicine Type"; ?></td>
                  <td class="text-left"><?php echo "Description"; ?></td>
                  <td class="text-left"><?php echo "Status"; ?></td>
                  <td class="text-left"><?php echo "Logs"; ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($categories) { ?>
                    <?php $i=1; ?>
                    <?php foreach ($categories as $category) { ?>
                    <tr>
                      <td style="display: none;" class="text-center"><?php if (in_array($category['equipment_id'], $selected)) { ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $category['equipment_id']; ?>" checked="checked" />
                        <?php } else { ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $category['equipment_id']; ?>" />
                        <?php } ?></td>
                        <td class="text-left"><?php echo $i++; ?></td>
                        <td class="text-left"><?php echo $category['med_type']; ?></td>
                        <td class="text-left"><?php echo $category['descp']; ?></td>
                        <td class="text-left"><?php echo $category['status']; ?></td>
                          <td class="text-left"><?php echo $category['log_datas']; ?></td>
                        <td class="text-right"><a href="<?php echo $category['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                    </tr>
                    <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
      </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function filter() { 
      var filter_medicine_type = $('#filter_medicine_type').val();
      var filter_status = $('select[name=\'filter_status\']').val();

        url = 'index.php?route=catalog/service&token=<?php echo $token; ?>';
      if (filter_medicine_type) {
        url += '&filter_medicine_type=' + encodeURIComponent(filter_medicine_type);
      }

      if (filter_status) {
        url += '&filter_status=' + encodeURIComponent(filter_status);
      }

      window.location.href = url;
      /*} else {
      alert('Please select the filters');
      return false;
      }*/
    }

    $('#filter_medicine_type').autocomplete({
      delay: 500,
      source: function(request, response) {
          if(request != ''){
              $.ajax({
                  url: 'index.php?route=catalog/service/autocomplete&token=<?php echo $token; ?>&filter_medicine_type=' +  encodeURIComponent(request), dataType: 'json',
                  success: function(json) {   
                      response($.map(json, function(item) {
                          return {
                              label: item.med_type,
                              value: item.med_type,
                          }
                      }));
                  }
              });
          }
      }, 
      select: function(item) {
          console.log(item);
          $('#filter_medicine_type').val(item.value);
          $('.dropdown-menu').hide();
          filter();
          return false;
      },
  });
</script>
<?php echo $footer; ?>