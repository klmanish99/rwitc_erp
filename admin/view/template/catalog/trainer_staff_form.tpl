<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab-general">
							
							<div class="form-group">
								<label class="col-sm-2 control-label" for="staff_name"><?php echo 'Staff Name'; ?></label>
								<div class="col-sm-4">
									<input type="text" name="staff_name" value="<?php echo $staff_name; ?>" placeholder="<?php echo 'Staff Name'; ?>" id="staff_name" class="form-control" tabindex="2"/>
									<?php if (isset($valierr_staff_name)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_staff_name; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="racing_name">Profile Pic:</label>
								<div class="col-sm-3">
										<input style="display: none;" readonly="readonly" onchange="readURL(this);" type="text" name="file_number_upload" value="<?php echo $file_number_upload; ?>" placeholder="Upload File Number" id="input-other_document_1" class="form-control" />
										<input type="hidden" name="uploaded_file_source" value="<?php echo $uploaded_file_source; ?>" id="input-other_document_1_source" class="form-control" />
										<span class="input-group-btn pull-left" style="padding-right: 65px;">
												<button type="button" id="button-other_document_1" style = "border-radius: 3px;" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-default"><i class="fa fa-upload"></i></button>
												<?php if($uploaded_file_source != ''){ ?>
														<a target="_blank" class = "btn btn-default" style=" display: none;cursor: pointer;margin-left:10px; border-radius: 3px;" id="uploaded_file_source" href="<?php echo $uploaded_file_source; ?>">View Profile</a>
												<?php } ?>
										</span>
										<?php if($img_path != "#") { //echo $img_path;exit; ?>
												<span id="profile_pic" class="col-sm-8" ><img src="<?php echo $img_path; ?>" height="60" id="blah" alt=""  /></span>
										<?php } else { ?>
												<span id="profile_pic" class="col-sm-8" ><img src="<?php echo $unknown_pic; ?>" height="60" id="blah" alt=""  /></span>
										<?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="doctor_name"><?php echo 'Adhar Number'; ?></label>
								<div class="col-sm-4">
									<input type="text" name="adhar_number" value="<?php echo $adhar_number; ?>" placeholder="<?php echo 'Adhar Number'; ?>" id="adhar_number" class="form-control" tabindex="1"/>
									<?php if (isset($valierr_adhar_number)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_adhar_number; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="doctor_name"><?php echo 'Designation'; ?></label>
								<div class="col-sm-4">
									<input type="text" name="designation" value="<?php echo $designation; ?>" placeholder="<?php echo 'Designation'; ?>" id="designation" class="form-control" tabindex="1"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="ambulance_no">Address</label>
								<div class="col-sm-4">
									<input type="text" name="address" value="<?php echo $address; ?>" placeholder="Address" id="address" class="form-control" tabindex="3"/>
								</div>
								<label class="col-sm-2 control-label" for="ambulance_no">Blood Group</label>
								<div class="col-sm-4">
									<input type="text" name="blood_group" value="<?php echo $blood_group; ?>" placeholder="Blood Group" id="blood_group" class="form-control" tabindex="3"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-parent">Trainer Name</label>
								<div class="col-sm-4">
									<input readonly="readonly" tabindex="" type="text" name="trainer_name" value="<?php echo $trainer_name; ?>" placeholder="Trainer Name" id="input-parent" class="form-control" />
									<input tabindex="" type="hidden" name="trainer_id" value="<?php echo $trainer_id; ?>"id="trainer_id" placeholder="Trainer Id" class="form-control" />
									<?php if (isset($valierr_trainer_name)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_trainer_name; ?></span><?php } ?>
								</div>
							</div>
						 
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</script>
	
	<script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script>
<script type="text/javascript">
		$(document).on('keypress','input,select, textarea', function (e) {
//$('input,select').on('keypress', function (e) {
		if (e.which == 13) {
				e.preventDefault();
				var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
				if (!$next.length) {
						$next = $('[tabIndex=1]');
				}
				$next.focus();
		}
});
</script>
<script type="text/javascript">
$('#input-parent').autocomplete({
	'source': function(request, response) {
	$.ajax({
		url: 'index.php?route=catalog/trainer_staff/autocompleteparent&token=<?php echo $token; ?>&parent=' +  encodeURIComponent(request),
		dataType: 'json',
		success: function(json) {
		response($.map(json, function(item) {
			return {
			label: item['trainer_name'],
			id: item['id'],
			value: item['trainer_name'],
			}
		}));
		}
	});
	},
	'select': function(item) {
	$('#input-parent').val(item['label']);
	$('#input-parent').val(item['value']);
	$('#trainer_id').val(item['id']);
	$('.dropdown-menu').hide();
	}
});
</script>

<script type="text/javascript">
$('#button-other_document_1').on('click', function() {
	$('#form-other_document_1').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form-other_document_1" style="display: none;"><input type="file" name="file" /></form>');
	$('#form-other_document_1 input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
			clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form-other_document_1 input[name=\'file\']').val() != '') {
			clearInterval(timer); 
			image_name = 'file_number';  
			$.ajax({ 
			url: 'index.php?route=catalog/jockey/upload_profile&token=<?php echo $token; ?>'+'&image_name='+image_name,
			type: 'post',   
			dataType: 'json',
			data: new FormData($('#form-other_document_1')[0]),
			cache: false,
			contentType: false,
			processData: false,   
			beforeSend: function() {
				$('#button-upload').button('loading');
			},
			complete: function() {
				$('#button-upload').button('reset');
			},  
			success: function(json) {
				if (json['error']) {
				alert(json['error']);
				}
				if (json['success']) {
				alert(json['success']);
				console.log(json);
				$('input[name=\'file_number_upload\']').attr('value', json['filename']);
				$('input[name=\'uploaded_file_source\']').attr('value', json['link_href']);
				d = new Date();
				$('#blah').remove();
				var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="uploaded_file_source" href="'+json['link_href']+'">View Document</a>';
				var image_data = '<img src="'+json['link_href']+'" height="60" id="blah" alt=""  />'
				$('#uploaded_file_source').remove();
				 $('#profile_pic').append(image_data);
				}
			},      
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
			});
		}
		}, 500);
	});

	// $(document).on('input', '#jockey_name', function(){
	//     var name = $( "#jockey_name" ).val();
	//     $( "#name1" ).val(name);
	// });


	function readURL(input) {
			alert(input);
			if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
							$('#blah')
									.attr('src', e.target.result)
									.width(100)
									.height(80);
					};

					reader.readAsDataURL(input.files[0]);
			}
	}
</script>
</div>
<?php echo $footer; ?>