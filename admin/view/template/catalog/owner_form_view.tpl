<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
	<div class="container-fluid">
	  <div class="pull-right">
		
		<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
	  <h1><?php echo $heading_title ?></h1>
	  <ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	  </ul>
	</div>
  </div>
  <div class="container-fluid">
	<?php if ($error_warning) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>

	<?php if ($error_linkedOwner) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_linkedOwner; ?>
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>

	<?php if ($error_partner_owner) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_partner_owner; ?>
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>

	<?php if ($error_gst_number) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_gst_number; ?>
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>
	
	<?php if ($error_sharedColor) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_sharedColor; ?>
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>

	<div class="panel panel-default">
	  <div class="panel-heading">
		<h3 class="col-sm-5  panel-title"><i class="fa fa-pencil"></i> <?php echo 'View Owner'; ?> Code: <?php echo $owner_code; ?></h3>
		<?php if (isset($ownerId)) { ?>
			<div style="padding-left: 40%;" class="">
				<b><input style="border: none;background: #fcfcfc;width: 95%;" disabled="disabled" id="name1" value="<?php echo $owner_name; ?>"></b>
			</div>
		<?php } else { ?>
			<div style="padding-left: 40%;" class="">
				<b><input style="border: none;background: #fcfcfc;width: 95%;" disabled="disabled" id="name1" value="" name=""></b>
			</div>
		<?php } ?>
	  </div>
	  <div class="panel-body">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
		  <ul class="nav nav-tabs">
			<li class="<?php echo $defalult_style;?>"><a data-index="1" tabindex="1" href="#tabOwnerBasicDetails" data-toggle="tab">Owner Basic Details</a></li>
			<li><a data-index="2" tabindex="2" href="#tabOwnerContactTaxDetails" data-toggle="tab">Owner Contact & Tax Details</a></li>
			<li><a data-index="3" tabindex="3" href="#tabOwnerColorReg" data-toggle="tab">Owner Color Registration</a></li>
			<li class="<?php echo $active_style;?>"><a data-index="4" tabindex="4" href="#tabHorsesWithOwner" data-toggle="tab">Horses With Owner</a></li>
			<li><a data-index="5" tabindex="5" href="#tab_BTF_UFL_Ban" data-toggle="tab">BTF/UFL Ban</a></li>
		  </ul>
		  <div class="tab-content">
			<div class="tab-pane <?php echo $defalult_style;?>" id="tabOwnerBasicDetails">
			<div class="tab-content">

				<div class="form-group ">
                    <label class="col-sm-2 control-label" for="owner_code"><b style="color: red">*</b>Owner code :</label>
                    <div class="col-sm-3">
                        <input disabled="disabled" type="text" name="owner_code" value="<?php echo $owner_code; ?>" placeholder="<?php echo "Owner Code"  ;?>" id="owner_code" class="form-control" tabindex="1"/>
                        <input type="hidden" name="hidden_owner_code" value="<?php echo $owner_code; ?>" id="owner_code" class="form-control"/>
                        <?php if (isset($valierr_code)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_code; ?></span><?php } ?>
                    </div>
                    <label class="col-sm-2 control-label" for="racing_name">Profile Pic:</label>
                    <div class="col-sm-5">
                        
                        <?php if($img_path != "#") { //echo $img_path;exit; ?>
                            <span id="profile_pic" class="col-sm-8" ><img src="<?php echo $img_path; ?>" height="60" id="blah" alt=""  /></span>
                        <?php } else { ?>
                            <span id="profile_pic" class="col-sm-8" ><img src="<?php echo $unknown_pic; ?>" height="60" id="blah" alt=""  /></span>
                        <?php } ?>
                    </div>
                </div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="select-ownerNamePrefix"><b class="rqrd">*</b>Owner Name</label>
					
					<div style="margin-top: 8px;" class="col-sm-2"  >
	                    <?php echo $owner_name_prefix; ?> <?php echo $owner_name; ?>
	                </div>
				  	<label class="col-sm-2 control-label" for="select-isTrainer">is Trainer?</label>
					
					<div style="margin-top: 8px;" class="col-sm-2"  >
	                    <?php echo $is_trainer; ?>
	                </div>
				  </div>

				  <div class="form-group">
					<label class="col-sm-2 control-label" for="select-approved_status">Approved Status</label>

					<div style="margin-top: 8px;" class="col-sm-2"  >
	                    <?php echo $approved_status; ?>
	                </div>
					<?php if ($approved_status == 'Yes') { ?>
						<label id="approval_dates" class="col-sm-2 control-label" for="input-dateApproval"><b class="rqrd">*</b> Approval Date</label>
						
						<div style="margin-top: 8px;" class="col-sm-2"  >
	                    	<?php echo $approval_date; ?>
	                	</div>
					<?php } else { ?>
						<label style="display: none;" id="approval_dates" class="col-sm-2 control-label" for="input-dateApproval"><b class="rqrd">*</b> Approval Date</label>
						
						<div style="display: none; margin-top: 8px;" class="col-sm-2"  >
	                    	<?php echo $approval_date; ?>
	                	</div>
					<?php } ?>

					<label class="col-sm-2 control-label" for="input-raceCardName">Race Card Name</label>
					
					<div style=" margin-top: 8px;" class="col-sm-2"  >
                    	<?php echo $race_card_name; ?>
                	</div>
                  </div>
                  <div class="form-group">
					<label class="col-sm-2 control-label" for="input-raceCardName">WIRHOA</label>
					
	                <div style=" margin-top: 8px;" class="col-sm-2"  >
                    	<?php echo $wirhoa; ?>
                	</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-2 control-label" for="select-ownershipType">Type of Ownership</label>
					<!-- <div class="col-sm-2">
						<select name="ownershipType" id="select-ownershipType" class="form-control" data-index="7">
							<?php foreach ($ownershipType as $key => $value) { ?>
							<?php if ($value == $type_of_ownership ) { ?>
							<option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
							<?php } else { ?>
							<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
							<?php } ?>
							<?php } ?>
						</select>
						<?php if (isset($valierr_ownershipType)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_ownershipType; ?></span><?php } ?>
					</div> -->

					<div style=" margin-top: 8px;" class="col-sm-2"  >
                    	<?php echo $type_of_ownership; ?>
                	</div>
				  </div>

				  <!-- Partnership owners starts here -->
				  <div id="partnershipOwnerDiv" class="form-group">
					  <table id="tblPartners" class="table table-striped table-bordered table-hover">
						  <thead>
							<tr><td colspan="6" >Add Partners</td></tr>
							<tr>
							  <td style="width: 5%;" class="text-left">Sr No</td>
							  <td class="text-center">Partner Name</td>
							  <td class="text-center">File Upload</td>
							  <td class="text-center">Pan Card</td>
							  <td class="text-center">Is Member</td>
							  
							</tr>
						  </thead>
						  <tbody>
							<?php $partners_row = 0; ?>
							<?php if (isset($partnersData_query)) { ?>
								<?php foreach ($partnersData_query as $partnersquery_data) {?>
									<tr id="partners_row<?php echo $partners_row; ?>">
										<td class="text-left">
											<label><?php echo $partners_row+1; ?></label>
										</td>
										<td class="text-left">
											<input type="text" name="partnersData[<?php echo $partners_row+1; ?>][partner_name]" value="<?php echo $partnersquery_data['partner_name']; ?>" placeholder="Partner Name" id="partnerName_<?php echo $partners_row+1; ?>" class="form-control ownerName_partner" />
											<input type="hidden" name="partnersData[<?php echo $partners_row+1; ?>][partner_id]" value="<?php echo $partnersquery_data['partner_id']; ?>" placeholder="Partner Id" id="partnerId_<?php echo $partners_row+1; ?>" class="form-control" />
										</td>
										<td class="text-left">
											<input type="text" readonly id="partner_image_<?php echo $partners_row+1 ?>" name="partnersData[<?php echo $partners_row+1 ?>][file]"  value="<?php  echo $partnersquery_data['file'] ;?>" placeholder="choose file" class="form-control" />
											<input type="hidden" id="partner_image_path_<?php echo $partners_row+1 ?>" name="partnersData[<?php echo $partners_row+1 ?>][file_path]" value="<?php  echo $partnersquery_data['file_path'] ;?>"  class="form-control" />
											<button type="button" class="button-uploads btn btn-default" style="margin-top: 5px;" id="button-upload_<?php echo $partners_row+1 ?>" data-loading-text="<?php echo 'Please Wait'; ?>"><i class="fa fa-upload"></i> <?php echo 'Upload File'; ?></button>
											 <span id="button-other_document_1_new<?php echo $partners_row+1 ?>"></span>
											 <?php if($partnersquery_data['file_path'] != ''){ ?>
												<span id="partnersquery_data_<?php echo $partners_row+1 ?>">
													<a target="_blank" class = "btn btn-default" style="cursor: pointer;margin-left:10px; border-radius: 3px;margin-top: 5px;"  href="<?php  echo $partnersquery_data['file_path'] ;?>">View Document</a>
												</span>
											<?php } ?>
										</td>
										<td class="text-left">
											<input type="text" name="partnersData[<?php echo $partners_row+1; ?>][pan_no]" value="" placeholder="Pan Number" id="panNo_<?php echo $partners_row+1; ?>" class="form-control" />
											<button type="button" class="check button-set btn btn-default" style="margin-top: 5px;" id="button-check_<?php echo $partners_row+1 ?>" >Check</button>
										</td>
										<td class="text-left">
											<select name="partnersData[<?php echo $partners_row+1; ?>][mem]" id="select-mem" class="form-control">
												<option value="0">Select</option>
												<?php foreach ($memArray as $key => $value) { ?>
												<?php if ($value == $partnersquery_data['member']) { ?>
												<option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
												<?php } else { ?>
												<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
												<?php } ?>
												<?php } ?>
											</select>
										</td>
									  <td class="text-left"></td>
									</tr>
									<?php $partners_row++; ?>
								<?php } ?>
							<?php } ?>
						  </tbody>
						  <tfoot>
						  </tfoot>
						</table>
				  </div>
				  <!-- Partnership owners ends here -->

					<div class="form-group">
						<label class="col-sm-2 control-label" for="select-committeMember">Committe Member</label>
						
						<div style=" margin-top: 8px;" class="col-sm-2"  >
	                    	<?php echo $cummitte_member; ?>
	                	</div>
						<label class="col-sm-2 control-label" for="select-member">Member</label>
						
						<div style=" margin-top: 8px;" class="col-sm-2"  >
	                    	<?php echo $member; ?>
	                	</div>

						<label class="col-sm-2 control-label" for="select-isLinkedOwner">Linked Owners</label>
						
						<div style=" margin-top: 8px;" class="col-sm-2"  >
	                    	<?php echo $linked_owners; ?>
	                	</div>
				  	</div>

				  <?php if ($linked_owners == 'Yes') { ?>
						<div id="linkedOwnerDiv" class="form-group">
				   <?php } else { ?>
						<div style="display: none;" id="linkedOwnerDiv" class="form-group">
				   <?php } ?>
					  <table id="linkedOwner" class="table table-striped table-bordered table-hover">
						  <thead>
							<tr><td colspan="4" >Add Linked Owners</td></tr>
							<tr>
							  <td style="width: 5%;" class="text-left">Sr No</td>
							  <td class="text-right">Owner Name</td>
							  <td class="text-right">Relationship</td>
							  <td class="text-left"></td>
							</tr>
						  </thead>
						  <tbody>
							<?php $recurring_row = 0; ?>
							<?php if (isset($linked_owner_query)) { ?>
								<?php foreach ($linked_owner_query as $linked_owner_data) {?>
									<tr id="recurring-row<?php echo $recurring_row; ?>">
										<td class="text-left">
											<label><?php echo $recurring_row+1; ?></label>
										</td>
										<td class="text-left">
											<input type="text" name="linkedOwnerName[<?php echo $recurring_row+1; ?>][owner_name]" value="<?php echo $linked_owner_data['owner_name']; ?>" placeholder="Owner Name" id="ownerName_<?php echo $recurring_row+1; ?>" class="form-control ownerName" />
											<input type="hidden" name="linkedOwnerName[<?php echo $recurring_row+1; ?>][linked_owner_id]" value="<?php echo $linked_owner_data['linked_owner_id']; ?>" placeholder="Owner Id" id="ownerId_<?php echo $recurring_row+1; ?>" class="form-control" />
										</td>
										<td class="text-left">
											<select name="linkedOwnerName[<?php echo $recurring_row+1; ?>][relationship]" id="select-relationship_<?php echo $recurring_row+1; ?>" class="relationship form-control">
												<option value="0">Select</option>
												<?php foreach ($relationshipArray as $key => $value) { ?>
												<?php if ($value == $linked_owner_data['relationship']) { ?>
												<option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
												<?php } else { ?>
												<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
												<?php } ?>
												<?php } ?>
											</select>
											<input type="text" name="linkedOwnerName[<?php echo $recurring_row+1; ?>][relationships]" placeholder="Relationship" id="relationship_<?php echo $recurring_row+1; ?>" class="form-control" />
										</td>
									  <td class="text-left"></td>
									</tr>
									<?php $recurring_row++; ?>
								<?php } ?>
							<?php } ?>
						  </tbody>
						  <tfoot>
						  </tfoot>
						</table>
				  </div>

				  <div class="form-group">
						<label class="col-sm-2 control-label" for="input-fileNumber">File Number</label>
						
						<div style=" margin-top: 8px;" class="col-sm-2"  >
	                    	<?php echo $file_number; ?>
	                	</div>
						<div class="col-sm-6">
							<input readonly="readonly" type="text" name="file_number_upload" value="<?php echo $file_number_upload; ?>" placeholder="Upload File Number" id="input-other_document_1" class="form-control" />
							<input type="hidden" name="uploaded_file_source" value="<?php echo $uploaded_file_source; ?>" id="input-other_document_1_source" class="form-control" />
							
						</div>                
				  </div>

				  <div class="form-group">
						<label class="col-sm-2 control-label" for="input-remark">Remark</label>
						
						<div style=" margin-top: 8px;" class="col-sm-10"  >
	                    	<?php echo $remark; ?>
	                	</div>
				  </div>
				  <div class="form-group">
						<label class="col-sm-2 control-label" for="select-isActive">Status</label>
						
						<div style=" margin-top: 8px;" class="col-sm-10"  >
	                    	<?php echo $isActive; ?>
	                	</div>
				  </div>
				  <div class="form-group">
	                    <label class="col-sm-2 control-label" for="remarks"><?php echo "Last Update:" ?></label>
	                    <div style="margin-top: 10px;" class="col-sm-10">
	                        <?php echo $last_upadate; ?> BY <?php echo $user; ?> 
	                    </div>
	              </div>
			  </div>
			</div>

			<div class="tab-pane" id="tabOwnerContactTaxDetails">
				<div class="form-group">
                        <label style="" class="col-sm-6 control-label alignLeft" for="input-address1"> Address 1</label>
                        <label class="col-sm-6 control-label alignLeft" for="input-address2">Address 2</label>
                    <div style="" class="col-sm-6">
                        <?php echo $address_1; ?>
                    </div>
                    <div style="" class="col-sm-6">
                        <?php echo $address_2; ?>
                    </div>
                    <div style="" class="col-sm-6">
                        <?php echo $address_3; ?>
                    </div>
                    <div style="" class="col-sm-6">
                        <?php echo $address_4; ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <label style="padding-left: 30px" class="col-sm-3 control-label alignLeft" for="input-address2">Locality area or street 1</label>
                        <label style="" class="col-sm-3 control-label alignLeft" for="input-address1"> City 1</label>
                        <label style="" class="col-sm-3 control-label alignLeft" for="input-address2">Locality area or street 2</label>
                        <label style="" class="col-sm-3 control-label alignLeft" for="input-address2">City 2</label>
                    </div>
                    <div style="" class="col-sm-3">
                        <?php echo $local_area_1; ?>
                    </div>
                    <div style="" class="col-sm-3">
                        <?php echo $city_1; ?>
                    </div>
                    <div style="" class="col-sm-3">
                        <?php echo $local_area_2; ?>
                    </div>
                    <div style="" class="col-sm-3">
                        <?php echo $city_2; ?>
                    </div>
                </div>
                <div class="form-group">
                        <label style="" class="col-sm-3 control-label alignLeft" for="input-address1"> Country 1</label>
                        <label class="col-sm-3 control-label alignLeft" for="input-address2">State 1</label>
                        <label class="col-sm-3 control-label alignLeft" for="input-address2">Country 2</label>
                        <label class="col-sm-3 control-label alignLeft" for="input-address2">State 2</label>
                    <div class="col-sm-3">
                      <select disabled="disabled" style="color: black;border: white;background: white;-webkit-appearance: none;" name="country1" data-index="19" id="input-country" onchange="country(this);" class="">
                        <option value="">Country</option>
                        <?php foreach ($countries as $country) { ?>
                        <?php if ($country['country_id'] == $country_1) { ?>
                        <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>
                    </div>

                    <div class="col-sm-3">
                      <select disabled="disabled" style="color: black;border: white;background: white;-webkit-appearance: none;" name="zone_id" data-index="20" id="zone_id" class="">
                        <option value="">State</option>
                        <?php foreach ($zones as $zone) { ?>
                        <?php if ($zone['zone_id'] == $zone_id) { ?>
                        <option value="<?php echo $zone['zone_id']; ?>" selected="selected"><?php echo $zone['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $zone_id; ?>"><?php echo $zone['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>
                    </div>


                    <div class="col-sm-3">
                      <select disabled="disabled" style="color: black;border: white;background: white;-webkit-appearance: none;" name="country2" data-index="25" id="input-country2" onchange="countrys(this);" class="">
                        <option value="">Country</option>
                        <?php foreach ($countries as $country2) { ?>
                        <?php if ($country2['country_id'] == $country_2) { ?>
                        <option value="<?php echo $country2['country_id']; ?>" selected="selected"><?php echo $country2['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $country2['country_id']; ?>"><?php echo $country2['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>
                    </div>

                    <div class="col-sm-3">
                      <select disabled="disabled" style="color: black;width: 100px;border: white;background: white;-webkit-appearance: none;" name="zone_id2" data-index="26" id="zone_id2" class="">
                        <option value="0">State</option>
                        <?php foreach ($zones2 as $zone2) { ?>
                        <?php if ($zone2['zone_id'] == $zone_id2) { ?>
                        <option value="<?php echo $zone2['zone_id']; ?>" selected="selected"><?php echo $zone2['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $zone2['zone_id']; ?>"><?php echo $zone2['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                        <label style="" class="col-sm-6 control-label alignLeft" for="input-address1"> Pincode 1</label>
                        <label class="col-sm-6 control-label alignLeft" for="input-address2">Pincode 2</label>
                    <div style="margin-top: 10px;" class="col-sm-6">
                        <?php echo $pincode_1; ?>
                    </div>
                    <div style="margin-top: 10px;" class="col-sm-6">
                        <?php echo $pincode_2; ?>
                    </div>
                </div>
				<div class="form-group">
					<h4 style="padding-left: 15px;">Contact Details</h4>
					<label class="col-sm-2 control-label" for="input-phoneNo">Phone No</label>
					
					<div style=" margin-top: 8px;" class="col-sm-4"  >
                    	<?php echo $phone_no; ?>
                	</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-mobileNo1"> Mobile No 1</label>
					
					<div style=" margin-top: 8px;" class="col-sm-4"  >
                    	<?php echo $mobile_no_1; ?>
                	</div>
					<label class="col-sm-2 control-label" for="input-altMobileNo">Alternate Mobile No</label>
					
					<div style=" margin-top: 8px;" class="col-sm-4"  >
                    	<?php echo $alt_mobile_no; ?>
                	</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-emailId"> Email id</label>
					
					<div style=" margin-top: 8px;" class="col-sm-4"  >
                    	<?php echo $email_id; ?>
                	</div>
					<label class="col-sm-2 control-label" for="input-altEmailId">Alternate Email id</label>
					
					<div style=" margin-top: 8px;" class="col-sm-4"  >
                    	<?php echo $alt_email_id; ?>
                	</div>
				</div>

				<div class="form-group">
				<h4 style="padding-left: 15px;">Tax Details</h4>
					<label class="col-sm-2 control-label" for="select-gst">GST</label>
					
					<div style="margin-top: 10px;" class="col-sm-3">
                        <?php echo $gst_type; ?>
                    </div>

					<label class="col-sm-2 control-label" for="input-gstNo">GST No</label>
					<div class="col-sm-4">
						<?php if ($gst_type == 'Registered') { ?>
							<div style="margin-top: 10px;" class="col-sm-3">
	                            <?php echo $gst_no; ?>
	                        </div>
						<?php } else { ?>
							<div style="margin-top: 10px;" class="col-sm-3">
	                            <?php echo $gst_no; ?>
	                        </div>
						<?php } ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-panNumber">PAN Number</label>
					
					<div style="margin-top: 10px;" class="col-sm-3">
                        <?php echo $pan_number; ?>
                    </div>

					<label class="col-sm-2 control-label" for="input-profTaxNoDetails">Prof Tax No details</label>
					
					<div style="margin-top: 10px;" class="col-sm-3">
                        <?php echo $prof_tax_no_details; ?>
                    </div>
				</div>

				<div class="form-group">
                    <input type="hidden"  name="upload_hidden_id" id="upload_hidden_id" value="<?php echo $upload_hidden_id; ?>">
                    <table id="itrUpload" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr><td colspan="3" >ITR Upload</td></tr>
                            <tr>
                                <td class="text-left">Assesment Year</td>
                                <td class="text-left">File Name</td>
                            </tr>
                        </thead>
                        <tbody id="upload_itr">
                            <?php //echo'<pre>';print_r($juploaditr);  ?>
                                <?php foreach($itr_owner_query as $key => $result) { ?>
                                    <tr id="recurring-row<?php echo $key; ?>">
                                        <td>
                                            <div style="margin-top: 10px;" class="col-sm-3">
                                                <?php echo $result['assesment_year']; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="margin-top: 10px;" class="col-sm-3">
                                                <?php echo $result['file']; ?>
                                            </div>
                                        </td>
                                    </tr>
                            <?php } //exit; ?>
                        </tbody>
                    </table>
                </div>
			</div>

			<div class="tab-pane " id="tabOwnerColorReg">
					<div class="form-group">
						<table id="tbl_add_Color" class="table table-striped table-bordered table-hover">
						  <thead>
							<tr><td colspan="4" >Add color</td></tr>
							<tr>
							  <td class="text-left">Color #</td>
							  <td class="text-left">Color Name</td>
							  <td class="text-left"></td>
							</tr>
						  </thead>
						  <tbody>
							<?php $color = 0; ?>
							<?php if (isset($colorQuery)) { ?>
								<?php foreach ($colorQuery as $colorDatas) { ?>
									<tr id="color_recurring-row<?php echo $color; ?>">
										<td class="text-left">
											<label>Color <?php echo $color+1; ?></label>
										</td>
										<td class="text-left">
											<input readonly="readonly" type="text" name="add_color[<?php echo $color+1 ?>][color]" id="add_color_<?php echo $color+1 ?>" value="<?php echo $colorDatas['color']; ?>" placeholder="Color Name" class="form-control colors" autocomplete="off" />
										</td>
									  <td class="text-left"></td>
									</tr>
								<?php $color++; ?>
								<?php } ?>
							<?php } ?>
						  </tbody>
						  <tfoot>
						  </tfoot>
						</table>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="startDate">Start Date</label>
							
						<div style="margin-top: 10px;" class="col-sm-3">
                            <?php echo $start_date; ?>
                        </div>
						<label class="col-sm-2 control-label" for="endDate">End Date</label>
							
						<div style="margin-top: 10px;" class="col-sm-3">
                            <?php echo $end_date; ?>
                        </div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="select-season">Season</label>
						
						<div style="margin-top: 10px;" class="col-sm-3">
                            <?php echo $season; ?>
                        </div>

						<label class="col-sm-2 control-label" for="select-allowShareColor">Allow to Share Color</label>
						
						<div style="margin-top: 10px;" class="col-sm-3">
                            <?php echo $allow_Share_Color; ?>
                        </div>
					</div>

					<?php if ($allow_Share_Color == 'Yes') { ?>
						<div id="shareColorDiv" class="form-group">
					<?php } else { ?>
						<div style="display: none;" id="shareColorDiv" class="form-group">
					<?php } ?>
						<table id="addOwnertoShareColor" class="table table-striped table-bordered table-hover">
						  <thead>
							<tr><td colspan="4" >Add Owner to share color</td></tr>
							<tr>
							  <td class="text-left">Sr. No</td>
							  <td class="text-left">Owner Name</td>
							  <td class="text-left"></i></button></td>
							</tr>
						  </thead>
						  <tbody>
							<?php $shareColorIncr = 0; ?>
							<?php if (isset($owner_shared_color)) { ?>
								<?php foreach ($owner_shared_color as $sharedColorDatas) { ?>
									<tr id="share_color_recurring-row<?php echo $color; ?>">
										<td class="text-left">
											<label><?php echo $shareColorIncr+1; ?></label>
										</td>
										<td class="text-left">
											<input readonly="readonly" type="text" name="owner_shared_color[<?php echo $shareColorIncr+1 ?>][shared_owner_name]" id="shareOwnerName_<?php echo $shareColorIncr+1; ?>" value="<?php echo $sharedColorDatas['shared_owner_name']; ?>" placeholder="Owner Name" class="form-control shareOwner" autocomplete="off" />
											<input type="hidden" name="owner_shared_color[<?php echo $shareColorIncr+1 ?>][shared_owner_id]" id="shareOwnerId_<?php echo $shareColorIncr+1; ?>" value="<?php echo $sharedColorDatas['shared_owner_id']; ?>" placeholder="Owner Id" class="form-control" autocomplete="off" />
										</td>
									  <td class="text-left"></td>
									</tr>
								<?php $shareColorIncr++; ?>
								<?php } ?>
							<?php } ?>
						  </tbody>
						  <tfoot>
						  </tfoot>
						</table>
					</div>


					<div class="form-group">
						<table class="table table-striped table-bordered table-hover">
						  <thead>
							<tr><td colspan="4" >Owner Color History</td></tr>
							<tr>
							  <td class="text-center">Color</td>
							  <td class="text-center">Start Date</td>
							  <td class="text-center">End Date</td>
							</tr>
						  </thead>
							<tbody>
							<?php 
							if (isset($colorHistoryQuery)) {
								foreach ($colorHistoryQuery as $key => $value) { 
									$s_date = '';
									$e_date = '';
									$dates = explode('_', $key);
									$startDate = $dates[0];
									$endDate = $dates[1];
									foreach ($value as $akey => $avalue) { ?>
										<tr>
											<td><?php echo $avalue['color']; ?>
											<br>
											<?php
											if($avalue['own_name'] != ''){
												echo '('.$avalue['own_name'].')';
											}
											?>
											
											
										
										</td>
											
											<?php if($startDate != $s_date){ ?>
											<td rowspan="<?php echo count($value); ?>" ><?php echo date('d-m-Y', strtotime($startDate)); ?></td>
											<?php } ?>
											
											<?php if($endDate != $e_date){ ?>
											<td rowspan="<?php echo count($value); ?>" ><?php echo date('d-m-Y', strtotime($endDate)); ?></td>
											<?php } ?>
										</tr>
										<?php
										$s_date = $startDate; 
										$e_date = $endDate;
									} 
								} 
							} ?>
							</tbody>
						</table>               
					</div>
			</div>

			<div class="tab-pane <?php echo $active_style;?>" id="tabHorsesWithOwner">
				<input type= "hidden"  name="owner_ids" id= "hourse_owner_id_filter" value="<?php echo $owner_ids; ?>">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="totalNoOfHorses">Total No Of Horses</label>
					
					<div style="margin-top: 10px;" class="col-sm-3">
                        <?php echo $totalNoOfHorses; ?>
                    </div>

					<label class="col-sm-2 control-label" for="onDate">As on Date</label>
					<div class="col-sm-2">
						<div class="input-group date">
							<input type="text" name="onDate" id="onDate" value="<?php echo $date_filer ?>" placeholder="Date" data-date-format="YYYY-MM-DD" class="form-control" />
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
						</div>
					</div>
					<div class="col-sm-1">
						<a onclick="ownerfilter()" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
					</div>
					
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="totalNoOfHorses">Search</label>
					<div class="col-sm-2">
						<input id="myInput" type="text" placeholder="Search.." class="form-control">
					</div>
				</div>
				<div class="form-group">
					<table  id="myTable" class=" tablesorter table table-striped table-bordered table-hover table-sort">
						  <thead>
							<tr>
							  <th class="text-center table-sort">Sr No</th>
							  <th class="text-center table-sort">Horse Name</th>
							  <th class="text-center table-sort">Horse Color/Sex</th>
							  <th class="text-center table-sort">Horse Age</th>
							  <th class="text-center table-sort">Owner Percentage</th>
							  <th class="text-center table-sort">Trainer Name</th>
							</tr>
						  </thead>

						  <tbody>
							<?php if ($horsewithowner_info) { ?>
								<?php $i=1; ?>
								<?php foreach ($horsewithowner_info as  $hourseownerkey => $hourseownervalue) { ?>
								<tr>
									<td class="text-center"> <?php echo  $i++; ?></td>
									
									<td class="text-center"><?php echo $hourseownervalue['official_name']; ?></td>
									<td class="text-center"><?php echo $hourseownervalue['color']; ?> <?php echo $hourseownervalue['sex'];  ?></td>
									<td class="text-center"><?php echo $hourseownervalue['age']; ?></td>
									<td class="text-center"><?php echo $hourseownervalue['owner_percentage']; ?></td>
									<td class="text-center"><?php echo $hourseownervalue['trainer_name']; ?></td>
									<input type= "hidden"  name="horsewithowner_info[<?php echo $hourseownerkey ?>][official_name]" value="<?php echo $hourseownervalue['official_name']; ?>">
									<input type= "hidden"  name="horsewithowner_info[<?php echo $hourseownerkey ?>][color]" value="<?php echo $hourseownervalue['color']; ?>">
									<input type= "hidden"  name="horsewithowner_info[<?php echo $hourseownerkey ?>][sex]" value="<?php echo $hourseownervalue['sex']; ?>">
									<input type= "hidden"  name="horsewithowner_info[<?php echo $hourseownerkey ?>][age]" value="<?php echo $hourseownervalue['age']; ?>">
									<input type= "hidden"  name="horsewithowner_info[<?php echo $hourseownerkey ?>][owner_percentage]" value="<?php echo $hourseownervalue['owner_percentage']; ?>">
									<input type= "hidden"  name="horsewithowner_info[<?php echo $hourseownerkey ?>][trainer_name]" value="<?php echo $hourseownervalue['trainer_name']; ?>">
								</tr>
								<?php } ?>
								<?php } else { ?>
								<tr>
									<td class="text-center" colspan="7">No Result Found</td>
								</tr>
								<?php } ?>
						  </tbody>
						</table> 
				</div>
			</div>

			<div class="tab-pane " id="tab_BTF_UFL_Ban">
			  <div style="padding: 0px;" class="form-group">
				<div class="col-sm-11">
				  <h4>BTF/UFL Ban</h4>
				</div>
				<div>
				  <button style="display: none;" type="button" id='btnAddBan' class="btn btn-primary" data-toggle="modal" data-target="#myModal"  onclick="clearModel()"><i class="fa fa-plus-circle"></i></button>
				</div>
			  </div>
			  <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Ban</h4>
					  </div>
					  <div class="modal-body">
						<input type="hidden" name="isEdit" value="" placeholder="" id="isEdit" class="form-control" />
						<div class="form-group">
						  <label class="col-sm-3 control-label" for="select-club"><b style="color: red">*</b>Club:</label>
						  <div class="col-sm-8">
							<select  name="club" placeholder="Club" id="select-club" data-index="40" class="form-control">
								<option value="" selected="selected" disabled="disabled" >Please Select</option>
								<?php foreach ($clubs as $key => $value) { ?>
								<option value="<?php echo $value; ?>" ><?php echo $value ?></option>
								<?php } ?>
							</select>
							<span style="display: none;" id="error_ban_club" class="error">Please Select Club.</span>
						  </div>
						</div>
					  
						<div class="form-group">
						  <label class="col-sm-3 control-label" for="select-banType"><b style="color: red">*</b>Ban Type:</label>
						  <div class="col-sm-8">
							<select  name="ban_type" placeholder="Ban Type" id="select-banType" data-index="41" class="form-control">
								<option value="" selected="selected" disabled="disabled" >Please Select</option>
								<?php foreach ($banType as $key => $value) { ?>
								<option value="<?php echo $value; ?>" ><?php echo $value ?></option>
								<?php } ?>
							</select>
							<span style="display: none;" id="error_ban_type" class="error">Please Select Ban Type.</span>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-sm-3 control-label" for="input-banStartDate"><b class="rqrd">*</b> Start Date</label>
						  <div class="col-sm-8">
							<div class="input-group date input-banStartDate">
							  <input type="text" name="ban_start_date" value="" data-index="42" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="input-banStartDate" class="form-control" />
							  <span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
							  </span>
							</div>
							<span style="display: none;" id="error_start_date" class="error">Start Date can not be blank.</span>
							<span style="display: none;" id="error_start_date_2" class="error">Invalid Start Date.</span>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-sm-3 control-label" for="input-banEndDate"><b class="rqrd">*</b> End Date</label>
						  <div class="col-sm-8">
							<div class="input-group date input-banEndDate">
							  <input type="text" name="ban_end_date" value="" data-index="43" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="input-banEndDate" class="form-control" />
							  <span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
							  </span>
							</div>
							<span style="display: none;" id="error_end_date" class="error">End Date can not be blank.</span>
							<span style="display: none;" id="error_end_date_2" class="error">Invalid End Date.</span>
							<span style="display: none;color: red;font-weight: bold;" id="error_greater_start_date" class="error"><?php echo 'End Date must not be less than Start Date!'; ?></span>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-sm-3 control-label" for="input-banAmount"><b style="color: red">*</b>Amount</label>
						  <div class="col-sm-8">
							  <input type="Number" name="ban_amount"  placeholder="Amount" data-index="44" id="input-banAmount" class="form-control" />
							  <span style="display: none;" id="error_ban_amount" class="error">Please enter amount.</span>
						  </div>
						</div>

					  </div>
					  <!-- Model Footer -->
					  <div class="modal-footer">
						  <!-- <input type="text" name="id_hidden_BanFunction" value="" id="id_hidden_BanFunction" class="form-control" />
						  <input type="text" name="idban_increment_id" value="" id="idban_increment_id" class="form-control" /> -->
						  <button type="button" class="btn btn-primary" id = "ban_save_id" onclick="saveBan()"  >Save</button>
						  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					  </div>

				  </div>
			  </div>
			  <div class="form-group">
				<table class="table table-striped table-bordered table-hover">
				  <thead>
					<tr>
					  <td class="text-center">Club</td>
					  <td class="text-center">Ban Type</td>
					  <td class="text-center">Start Date</td>
					  <td class="text-center">End Date</td>
					  <td class="text-center">Amount</td>
					  <td class="text-center">Action</td>
					</tr>
				  </thead>
				  <tbody id="baned_table_body">
					<?php $ban_row_incr = 0; ?>
					<?php if ($owner_ban) { ?>
						<?php foreach ($owner_ban as $key => $value) { ?>
						  <tr id="ban_row_<?php echo $ban_row_incr; ?>">
							
							<td class="left">
							  <span id="tdBanClub_<?php echo $ban_row_incr; ?>"><?php echo $value['club']; ?></span>
							  <input type="hidden" name="ownerBan[<?php echo $ban_row_incr; ?>][club]" value="<?php echo $value['club']; ?>" id="inputClub_<?php echo $ban_row_incr; ?>" class="form-control" />
							</td>

							<td class="left">
							  <span id="tdBanType_<?php echo $ban_row_incr; ?>"><?php echo $value['ban_type']; ?></span>
							  <input type="hidden" name="ownerBan[<?php echo $ban_row_incr; ?>][ban_type]" value="<?php echo $value['ban_type']; ?>" id="inputBanType_<?php echo $ban_row_incr; ?>" class="form-control" />
							</td>
						  
							<td class="left">
							  <span id="tdBanStartDate_<?php echo $ban_row_incr;?>"><?php echo date('d-m-Y', strtotime($value['start_date'])); ?></span>
							  <input type="hidden" name="ownerBan[<?php echo $ban_row_incr; ?>][start_date]" value="<?php echo date('d-m-Y', strtotime($value['start_date'])); ?>" id="inputBanStartDate_<?php echo $ban_row_incr; ?>" class="form-control" />
							</td>

							<td class="left">
							  <span id="tdBanEndDate_<?php echo $ban_row_incr; ?>"><?php echo date('d-m-Y', strtotime($value['end_date'])); ?></span>
							  <input type="hidden" name="ownerBan[<?php echo $ban_row_incr; ?>][end_date]" value="<?php echo date('d-m-Y', strtotime($value['end_date'])); ?>" id="inputBanEndDate_<?php echo $ban_row_incr; ?>" class="form-control" />
							</td>

							<td class="left">
							  <span id="tdBanAmount_<?php echo $ban_row_incr; ?>"><?php echo $value['amount']; ?></span>
							  <input type="hidden" name="ownerBan[<?php echo $ban_row_incr; ?>][amount]" value="<?php echo $value['amount']; ?>" id="inputBanAmount_<?php echo $ban_row_incr; ?>" class="form-control" />
							</td>

							<td class="text-center">
							  <a onclick="editBan(<?php echo $ban_row_incr; ?>);" title="<?php echo "Edit"; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							  <a onclick="$('#ban_row_<?php echo $ban_row_incr; ?>').remove()" data-toggle="tooltip" title="<?php echo "Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
							</td>

						  </tr>
						  <?php $ban_row_incr++; ?>
					   <?php } ?>
					<?php } else { ?>
					  <tr><td colspan="6" class="text-center emptyRows">No BAN to display</td></tr>
					<?php } ?>
				  </tbody>
				</table>
			  </div>

			  <h4>BTF/UFL Ban History</h4>
			  <div class="form-group">
				<table class="table table-striped table-bordered table-hover">
				  <thead>
					<tr>
					  <td class="text-center">Club</td>
					  <td class="text-center">Ban Type</td>
					  <td class="text-center">Start Date</td>
					  <td class="text-center">End Date</td>
					  <td class="text-center">Amount</td>
					</tr>
				  </thead>
				  <tbody id="baned_history">
					<?php if (isset($owner_ban_history)) { ?>
					  <?php if ($owner_ban_history) { ?>
						<?php foreach ($owner_ban_history as $key => $value) { ?>
						  <tr>
							<td class="text-left"><?php echo $value['club']; ?></td>
							<td class="text-left"><?php echo $value['ban_type']; ?></td>
							<td class="text-left"><?php echo $value['start_date']; ?></td>
							<td class="text-left"><?php echo $value['end_date']; ?></td>
							<td class="text-left"><?php echo $value['amount']; ?></td>
						  </tr>
					   <?php } ?>
					  <?php } else { ?>
						<tr><td colspan="6" class="text-center emptyRows">No BAN History to display</td></tr>
					  <?php } ?>
					<?php } ?>
				  </tbody>
				</table>
			  </div>

			</div>
			
		  </div>
		</form>
	  </div>
	</div>
  </div>
<?php if (isset($ownerId)) {
	$o_Id = $ownerId;
} else {
	$o_Id = 0;
} ?>
<script type="text/javascript">
  var ban_row_incr = <?php echo $ban_row_incr; ?>

  function saveBan(){
	var strng = $('#isEdit').val();
	var isEdit = strng.split('_');
	var type = isEdit[0];
	var id = isEdit[1];

	if (type == 'edit') {
	  //Edit Ban data
	  var ban_club = $('#select-club').val();
	  var ban_type = $('#select-banType').val();
	  var ban_start_date = $('#input-banStartDate').val();
	  var ban_end_date = $('#input-banEndDate').val();
	  var ban_amount = $('#input-banAmount').val();
	  if (ban_end_date != '') {
			
			var string_start_date_ban = ban_start_date.split('-');
			var s_ban_date  = string_start_date_ban[0];
			var s_ban_month = string_start_date_ban[1];
			var s_ban_year  = string_start_date_ban[2];

			var string_end_date_ban = ban_end_date.split('-');
			var e_ban_date  = string_end_date_ban[0];
			var e_ban_month = string_end_date_ban[1];
			var e_ban_year  = string_end_date_ban[2];

			if (s_ban_year > e_ban_year){
				$('#error_greater_start_date').show();
				return false;
			} else if (e_ban_month > s_ban_month) {
				if (s_ban_date > e_ban_date || e_ban_date > s_ban_date) {
					$('#error_greater_start_date').hide();
				} 
			} else if(s_ban_month > e_ban_month) {
				$('#error_greater_start_date').show();
				return false;
			} else if(s_ban_month == e_ban_month) {
				if (s_ban_date > e_ban_date) {
					$('#error_greater_start_date').show();
					return false;
				}
			}
		}

	  if(ban_start_date != '' || ban_end_date  != ''){
		var string_start_date = ban_start_date.split('-');
		var s_date  = string_start_date[0];
		var s_month = string_start_date[1];
		var s_year  = string_start_date[2];
		
		var string_end_date = ban_end_date.split('-');
		var e_date  = string_end_date[0];
		var e_month = string_end_date[1];
		var e_year  = string_end_date[2];
		var letters = /^[A-Za-z_]+$/;

		if (s_date < 1 || s_date > 31 || s_month < 1 || s_month > 12 || s_year < 1980 || e_date < 1 || e_date > 31 || e_month < 1 || e_month > 12 || e_year < 1980 || ban_start_date.match(letters) || ban_end_date.match(letters)) {
		  if ((s_date < 1 || s_date > 31 || s_month < 1 || s_month > 12 || s_year < 1980 || ban_start_date.match(letters)) && (ban_start_date != '')) {
			$('#error_start_date_2').css('display','block');
		  }
		  else {
			$('#error_start_date_2').css('display','none');
		  }
		  if ((e_date < 1 || e_date > 31 || e_month < 1 || e_month > 12 || e_year < 1980 || ban_end_date.match(letters)) && (ban_end_date != '')) {
			$('#error_end_date_2').css('display','block');
		  }
		  else {
			$('#error_end_date_2').css('display','none');
		  }
		  return false;
		}
		else{
		  $('#error_start_date_2').css('display','none');
		  $('#error_end_date_2').css('display','none');
		}
	  }
	  if (ban_club == null || ban_type == null || ban_start_date == '' || ban_end_date == '' || ban_amount == '') {
		if (ban_club == null) {
		  $('#error_ban_club').css('display','block');
		}
		if (ban_type == null) {
		  $('#error_ban_type').css('display','block');
		}
		if (ban_start_date == '') {
		  $('#error_start_date').css('display','block');
		  $('#error_start_date_2').css('display','none');
		}
		if (ban_end_date == '') {
		  $('#error_end_date').css('display','block');
		  $('#error_end_date_2').css('display','none');
		}
		if (ban_amount == '') {
		  $('#error_ban_amount').css('display','block');
		}
		return false;
	  } else {
		$('#inputClub_'+ id +'').val(ban_club);
		$('#inputBanType_'+ id +'').val(ban_type);
		$('#inputBanStartDate_'+ id +'').val(ban_start_date);
		$('#inputBanEndDate_'+ id +'').val(ban_end_date);
		$('#inputBanAmount_'+ id +'').val(ban_amount);

		$('#tdBanClub_'+ id +'').html(ban_club);   
		$('#tdBanType_'+ id +'').html(ban_type);
		$('#tdBanStartDate_'+ id +'').html(ban_start_date);
		$('#tdBanEndDate_'+ id +'').html(ban_end_date);
		$('#tdBanAmount_'+ id +'').html(ban_amount);
	  }
	}
	else {
	  //Add New Ban data
	  var ban_club = $('#select-club').val();
	  var ban_type = $('#select-banType').val();
	  var ban_start_date = $('#input-banStartDate').val();
	  var ban_end_date = $('#input-banEndDate').val();
	  var ban_amount = $('#input-banAmount').val();
	  if (ban_end_date != '') {
			
			var string_start_date_ban = ban_start_date.split('-');
			var s_ban_date  = string_start_date_ban[0];
			var s_ban_month = string_start_date_ban[1];
			var s_ban_year  = string_start_date_ban[2];

			var string_end_date_ban = ban_end_date.split('-');
			var e_ban_date  = string_end_date_ban[0];
			var e_ban_month = string_end_date_ban[1];
			var e_ban_year  = string_end_date_ban[2];

			if (s_ban_year > e_ban_year){
				$('#error_greater_start_date').show();
				return false;
			} else if (e_ban_month > s_ban_month) {
				if (s_ban_date > e_ban_date || e_ban_date > s_ban_date) {
					$('#error_greater_start_date').hide();
				} 
			} else if(s_ban_month > e_ban_month) {
				$('#error_greater_start_date').show();
				return false;
			} else if(s_ban_month == e_ban_month) {
				if (s_ban_date > e_ban_date) {
					$('#error_greater_start_date').show();
					return false;
				}
			}
		}
		
	  if(ban_start_date != '' || ban_end_date  != ''){
		var string_start_date = ban_start_date.split('-');
		var s_date  = string_start_date[0];
		var s_month = string_start_date[1];
		var s_year  = string_start_date[2];
		
		var string_end_date = ban_end_date.split('-');
		var e_date  = string_end_date[0];
		var e_month = string_end_date[1];
		var e_year  = string_end_date[2];
		var letters = /^[A-Za-z_]+$/;

		if (s_date < 1 || s_date > 31 || s_month < 1 || s_month > 12 || s_year < 1980 || e_date < 1 || e_date > 31 || e_month < 1 || e_month > 12 || e_year < 1980 || ban_start_date.match(letters) || ban_end_date.match(letters)) {
		  if ((s_date < 1 || s_date > 31 || s_month < 1 || s_month > 12 || s_year < 1980 || ban_start_date.match(letters)) && (ban_start_date != '')) {
			$('#error_start_date_2').css('display','block');
		  }
		  else {
			$('#error_start_date_2').css('display','none');
		  }
		  if ((e_date < 1 || e_date > 31 || e_month < 1 || e_month > 12 || e_year < 1980 || ban_end_date.match(letters)) && (ban_end_date != '')) {
			$('#error_end_date_2').css('display','block');
		  }
		  else {
			$('#error_end_date_2').css('display','none');
		  }
		  return false;
		}
		else{
		  $('#error_start_date_2').css('display','none');
		  $('#error_end_date_2').css('display','none');
		}
	  }
	  if (ban_club == null || ban_type == null || ban_start_date == '' || ban_end_date == '' || ban_amount == '') {
		if (ban_club == null) {
		  $('#error_ban_club').css('display','block');
		}
		if (ban_type == null) {
		  $('#error_ban_type').css('display','block');
		}
		if (ban_start_date == '') {
		  $('#error_start_date').css('display','block');
		  $('#error_start_date_2').css('display','none');
		}
		if (ban_end_date == '') {
		  $('#error_end_date').css('display','block');
		  $('#error_end_date_2').css('display','none');
		}
		if (ban_amount == '') {
		  $('#error_ban_amount').css('display','block');
		}
		return false;
	  } else {
		ban_row_incr++;
		html  = '';
		html += '<tr id="ban_row_' + ban_row_incr + '">';

		html += '<td class="left">';
		html += '<span id="tdBanClub_'+ban_row_incr+'">'+ ban_club + '</span>';
		html += '<input type="hidden" name="ownerBan[' + ban_row_incr + '][club]" value="'+ ban_club +'" id="inputClub_'+ ban_row_incr + '" class="form-control" />';
		html += '</td>';

		html += '<td class="left">';
		html += '<span id="tdBanType_'+ban_row_incr+'">'+ ban_type + '</span>';
		html += '<input type="hidden" name="ownerBan[' + ban_row_incr + '][ban_type]" value="'+ ban_type +'" id="inputBanType_'+ ban_row_incr + '" class="form-control" />';
		html += '</td>';

		html += '<td class="left">';
		html += '<span id="tdBanStartDate_'+ban_row_incr+'">'+ ban_start_date + '</span>';
		html += '<input type="hidden" name="ownerBan[' + ban_row_incr + '][start_date]" value="'+ ban_start_date +'" id="inputBanStartDate_'+ ban_row_incr + '" class="form-control" />';
		html += '</td>';

		html += '<td class="left">';
		html += '<span id="tdBanEndDate_'+ban_row_incr+'">'+ ban_end_date + '</span>';
		html += '<input type="hidden" name="ownerBan[' + ban_row_incr + '][end_date]" value="'+ ban_end_date +'" id="inputBanEndDate_'+ ban_row_incr + '" class="form-control" />';
		html += '</td>';

		html += '<td class="left">';
		html += '<span id="tdBanAmount_'+ban_row_incr+'">'+ ban_amount + '</span>';
		html += '<input type="hidden" name="ownerBan[' + ban_row_incr + '][amount]" value="'+ ban_amount +'" id="inputBanAmount_'+ ban_row_incr + '" class="form-control" />';
		html += '</td>';

		html += '<td class="text-center">';
		html += '<a onclick="editBan(' + ban_row_incr + ');" title="<?php echo "Edit"; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>';
		html += '<a onclick="$(\'#ban_row_' + ban_row_incr + '\').remove()" style="margin-left: 3px;" data-toggle="tooltip" title="<?php echo "Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
		html += '</td>';

		html += '</tr>';
		$('#baned_table_body .emptyRows').css('display','none');
		$('#baned_table_body').append(html);        
	  }
	}
	  $("#myModal").modal("toggle");
  }

  function clearModel(){
	$('#select-club').val('');
	$('#select-banType').val('');
	$('#input-banStartDate').val('');
	$('#input-banEndDate').val('');
	$('#input-banAmount').val('');
	$('#isEdit').val('');

	$('#error_ban_club').css('display','none');
	$('#error_ban_type').css('display','none');
	$('#error_start_date').css('display','none');
	$('#error_end_date').css('display','none');
	$('#error_ban_amount').css('display','none');
	$('#error_start_date_2').css('display','none');
	$('#error_end_date_2').css('display','none');
  }

  $('#myModal').on('shown.bs.modal', function () {
	$('#select-club').focus();
  });

  $('#input-banAmount').on('keydown', function (event) {
	if (event.which == 13) {
	  $('#ban_save_id').focus();
	}
  });

  function editBan(id){
	$('#error_greater_start_date').hide();
	$('#myModal').modal('show');

	$('#error_ban_club').css('display','none');
	$('#error_ban_type').css('display','none');
	$('#error_start_date').css('display','none');
	$('#error_end_date').css('display','none');
	$('#error_ban_amount').css('display','none');
	$('#error_start_date_2').css('display','none');
	$('#error_end_date_2').css('display','none');

	var club = $('#inputClub_'+ id +'').val();
	var banType = $('#inputBanType_'+ id +'').val();
	var banStartDate = $('#inputBanStartDate_'+ id +'').val();
	var banEndDate = $('#inputBanEndDate_'+ id +'').val();
	var banAmount = $('#inputBanAmount_'+ id +'').val();

	$('#select-club').val(club);
	$('#select-banType').val(banType);
	$('#input-banStartDate').val(banStartDate);
	$('#input-banEndDate').val(banEndDate);
	$('#input-banAmount').val(banAmount);
	
	$('#isEdit').val('edit_'+id+'');
  }

  //validation ban model starts here
	var isError = 0;
  //1-Club Error
  $('#select-club').on('focusout',function(){
	var value = $(this).val();
	if (value == null) {
	  $('#error_ban_club').css('display','block');
	}
	else{
	  $('#error_ban_club').css('display','none');
	}
  });

  $('#select-club').on('change',function(){
	$('#error_ban_club').css('display','none');
  });

  //2-Ban Type Error
  $('#select-banType').on('focusout',function(){
	var value = $(this).val();
	if (value == null) {
	  $('#error_ban_type').css('display','block');
	}
	else{
	  $('#error_ban_type').css('display','none');
	}
  });

  $('#select-banType').on('change',function(){
	$('#error_ban_type').css('display','none');
  });

  //3-Start Date Error
  $('#input-banStartDate').on('focusout',function(){
	var value = $(this).val();
	if (value == '') {
	  $('#error_start_date').css('display','block');
	  $('#error_start_date_2').css('display','none');
	}
	else{
	  $('#error_start_date').css('display','none');
	}
  });

  /*$('.input-banStartDate').datetimepicker().on('dp.change', function (e) {  
	$('#error_start_date').css('display','none');
  });*/

  //4-End Date Error
  $('#input-banEndDate').on('focusout',function(){
	var value = $(this).val();
	if (value == '') {
	  $('#error_end_date').css('display','block');
	  $('#error_end_date_2').css('display','none');
	}
	else{
	  $('#error_end_date').css('display','none');
	}
  });

  /*$('.input-banEndDate').datetimepicker().on('dp.change', function (e) {  
	$('#error_end_date').css('display','none');
  });*/

  //5-Ban Amount Error
  $('#input-banAmount').on('focusout',function(){
	var value = $(this).val();
	if (value == '') {
	  $('#error_ban_amount').css('display','block');
	}
	else{
	  $('#error_ban_amount').css('display','none');
	}
  });

  $('#input-banAmount').on('keyup',function(event){
	var value = $(this).val();
	  if (event.which != 13) {
	  if (value.length != 0) {
		$('#error_ban_amount').css('display','none');
	  }
	  else{
		$('#error_ban_amount').css('display','block');
	  }
	}
  });

  /*if (empty) {
	  $('#ban_save_id').attr('disabled', 'disabled');
  } else {
	  $('#ban_save_id').removeAttr('disabled');
  }*/


  //validation ban model ends here
</script>

<script type="text/javascript"><!--

$("#select-isLinkedOwner").change(function(){
	$(this).find("option:selected").each(function(){
		var optionValue = $(this).attr("value");
		if (optionValue == 'Yes') {
			$("#linkedOwnerDiv").show();
		}
		else{
			$("#linkedOwnerDiv").hide();
		}
	});
});

$("#select-gst").change(function(){
	$(this).find("option:selected").each(function(){
		var optionValue = $(this).attr("value");
		if (optionValue == 'Unregistered') {
			$("#input-gstNo").attr("disabled", "disabled");
		}
		else{
			$("#input-gstNo").removeAttr("disabled");
		}
	});
});

$('#input-panNumber').keyup(function(){
            pan_len = parseInt($("#input-panNumber").val().length);
           // console.log(gst_len);
            $('.pan_error').html('');
            if(pan_len > 10 || pan_len < 10){
                $('.pan_error').css("color", "red")
                $('.pan_error').append('PAN No should be 10 digits!');

                //return false;
            } 

            if(pan_len == 10 ){
                $('.pan_error').html('');
            }   
        })

$("#select-allowShareColor").change(function(){
	$(this).find("option:selected").each(function(){
		var optionValue = $(this).attr("value");
		if (optionValue == 'Yes') {
			$("#shareColorDiv").show();
		}
		else{
			$("#shareColorDiv").hide();
		}
	});
});

var recurring_row = <?php echo $recurring_row; ?>;
var ownerId = <?php echo $o_Id; ?>;

function addLinkedOwner() {
	recurring_row++;
	html  = '';
	html += '<tr id="recurring-row' + recurring_row + '">';
	html += '  <td class="left">';
	html += '<label>'+recurring_row+'</label>';
	html += '  </td>';
	html += '  <td class="left">';
	html += '  <input type="text" name="linkedOwnerName[' + recurring_row + '][owner_name]" value="" placeholder="Owner name" id="ownerName_'+ recurring_row + '" class="form-control ownerName" autocomplete="off" />';
	html += '  <input type="hidden" name="linkedOwnerName[' + recurring_row + '][linked_owner_id]" value="" placeholder="Owner Id" id="ownerId_'+ recurring_row + '" class="form-control" />';
	html += '  </td>';
	html += '  <td class="left">';
	html += '  <select name="linkedOwnerName[' + recurring_row + '][relationship]" id="select-relationship_'+ recurring_row + '" class="relationship form-control">';
	html += '  <option value="0" selected="selected" >Select</option>';
	html += '  <?php foreach ($relationshipArray as $key => $value) { ?>';
	html += '  <option value="<?php echo $value; ?>"><?php echo $value; ?></option>';
	html += '  <?php } ?>';
	html += '  </select>';
	html += '  <input type="text" name="linkedOwnerName[' + recurring_row + '][relationships]" value="" placeholder="Relationship" id="relationship_'+ recurring_row + '" class="form-control"/>';
	html += '  </td>';
	html += '  <td class="left">';
	html += '    <a onclick="$(\'#recurring-row' + recurring_row + '\').remove()" data-toggle="tooltip" title="<?php echo "Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
	html += '  </td>';
	html += '</tr>';

	$('#linkedOwner tbody').append(html);
	//linked owner autocomplete
	$('.ownerName').autocomplete({

		'source': function(request, response) {
			var strng = $(this).attr('id');
			var s_id = strng.split('_');
			$('input[name=\'linkedOwnerName[' + s_id[1] + '][linked_owner_id]\']').val('');
			$.ajax({
				url: 'index.php?route=catalog/owner/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request)+'&owner_Id=' + encodeURIComponent(ownerId),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['owner_name'],
							value: item['owner_id'],
						}
					}));
				}
			});
		},
		'select': function(item) {
			var strng = $(this).attr('id');
			var s_id = strng.split('_');
			$('input[name=\'linkedOwnerName[' + s_id[1] + '][owner_name]\']').val(item['label']);
			$('input[name=\'linkedOwnerName[' + s_id[1] + '][linked_owner_id]\']').val(item['value']);
		}
	});
}

//linked owner autocomplete start
$('.ownerName').autocomplete({
	'source': function(request, response) {
		var strng = $(this).attr('id');
		var s_id = strng.split('_');
		$('input[name=\'linkedOwnerName[' + s_id[1] + '][linked_owner_id]\']').val('');
		$.ajax({
			url: 'index.php?route=catalog/owner/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request)+'&owner_Id=' + encodeURIComponent(ownerId),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['owner_name'],
						value: item['owner_id'],
					}
				}));
			}
		});
	},
	'select': function(item) {
		var strng = $(this).attr('id');
		var s_id = strng.split('_');
		$('input[name=\'linkedOwnerName[' + s_id[1] + '][owner_name]\']').val(item['label']);
		$('input[name=\'linkedOwnerName[' + s_id[1] + '][linked_owner_id]\']').val(item['value']);
	}
});
//linked owner autocomplete end

//Add partners start
var partners_row = <?php echo $partners_row; ?>;
function addPartners(){
	partners_row++;
	html  = '';
	html += '<tr id="partners_row' + partners_row + '">';
	html += '  <td class="left">';
	html += '<label>'+partners_row+'</label>';
	html += '  </td>';

	html += '<td class="left">';
	html += '<input type="text" name="partnersData[' + partners_row + '][partner_name]" value="" placeholder="Partner Name" id="partnerName_' + partners_row + '" class="form-control ownerName_partner" />';
	html += '<input type="hidden" name="partnersData[' + partners_row + '][partner_id]" value="" placeholder="Partner Id" id="partnerId_<?php echo $partners_row+1; ?>" class="form-control" />';
	html += '</td>';

	html += '<td class="left">';
	html += '<input type="text" readonly id="partner_image_' + partners_row + '" name="partnersData[' + partners_row + '][file]"  value="" placeholder="choose file" class="form-control" />';
	html += '<input type="hidden" id="partner_image_path_' + partners_row + '" name="partnersData[' + partners_row + '][file_path]" value=""  class="form-control" />';
	html += '<button type="button" class="button-uploads btn btn-default" style="margin-top: 5px;" id="button-upload_' + partners_row + '" data-loading-text="<?php echo 'Please Wait'; ?>"><i class="fa fa-upload"></i> <?php echo 'Upload File'; ?></button>';
	html += '<span id="button-other_document_1_new' + partners_row + '"></span>';
	html += '</td>';

	html += '<td class="left">';
	html += '<input type="text" name="partnersData[' + partners_row + '][pan_no]" value="" placeholder="Pan Number" id="panNo_' + partners_row + '" class="form-control" />';
	html += '<button type="button" style="margin-top: 5px;" id="button-check_<?php echo $partners_row+1 ?>" class="button-set btn btn-default check" >Check</button>';
	html += '</td>';

	html += '  <td class="left">';
	html += '  <select name="partnersData[' + partners_row + '][mem]" id="select-mem" class="form-control">';
	html += '  <option value="0" disabled="disabled" selected="selected" >Select</option>';
	html += '  <?php foreach ($memArray as $key => $value) { ?>';
	html += '  <option value="<?php echo $value; ?>"><?php echo $value; ?></option>';
	html += '  <?php } ?>';
	html += '  </select>';
	html += '  </td>';


	html += '<td class="left">';
	html += '<a onclick="$(\'#partners_row' + partners_row + '\').remove()" data-toggle="tooltip" title="<?php echo "Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
	html += '</td>';
	
	html += '</tr>';
	$('#tblPartners tbody').append(html);

	 $('.ownerName_partner').autocomplete({
		'source': function(request, response) {
			var strng = $(this).attr('id');
			var s_id = strng.split('_');
			$('input[name=\'partnersData[' + s_id[1] + '][partner_id]\']').val('');
			$.ajax({
				url: 'index.php?route=catalog/owner/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request)+'&owner_Id=' + encodeURIComponent(ownerId),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['owner_name'],
							value: item['owner_id'],
						}
					}));
				}
			});
		},
		'select': function(item) {
			var strng = $(this).attr('id');
			var s_id = strng.split('_');
			$('input[name=\'partnersData[' + s_id[1] + '][partner_name]\']').val(item['label']);
			$('input[name=\'partnersData[' + s_id[1] + '][partner_id]\']').val(item['value']);
		}
	});

	$( ".check" ).click(function(){
		idss = $(this).attr('id');
		s_id = idss.split('_');
		id = s_id[1];
		pan = $('#panNo_'+ id +'').val();
		
		$.ajax({
		  url: 'index.php?route=catalog/owner/getMemberdata&token=<?php echo $token; ?>'+'&pan='+pan,
		  dataType: 'json',
			success: function(json) {
				if (json['exist_status'] == 1) {
					alert(json['name']+"\n"+json['date_of_birth']+"\n"+json['exist_status']);
				} else {
					alert('Member Not Found!');
				}
				/*if (json['error']) {
					alert(json['error']);
				}*/
			},  
		});
	});
}

$('.ownerName_partner').autocomplete({
	'source': function(request, response) {
		var strng = $(this).attr('id');
		var s_id = strng.split('_');
		$('input[name=\'partnersData[' + s_id[1] + '][partner_id]\']').val('');
		$.ajax({
			url: 'index.php?route=catalog/owner/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request)+'&owner_Id=' + encodeURIComponent(ownerId),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['owner_name'],
						value: item['owner_id'],
					}
				}));
			}
		});
	},
	'select': function(item) {
		var strng = $(this).attr('id');
		var s_id = strng.split('_');
		$('input[name=\'partnersData[' + s_id[1] + '][partner_name]\']').val(item['label']);
		$('input[name=\'partnersData[' + s_id[1] + '][partner_id]\']').val(item['value']);
	}
});

$(document).delegate('.button-uploads', 'click', function() {
	idss = $(this).attr('id');
	s_id = idss.split('_');
	id = s_id[1];
	$('#form_upload_partner').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form_upload_partner" style="display: none;"><input type="file" name="file" /></form>');
	$('#form_upload_partner input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
			clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form_upload_partner input[name=\'file\']').val() != '') {
			clearInterval(timer);   
			id = id;
			$.ajax({
				url: 'index.php?route=catalog/owner/upload_partner&token=<?php echo $token; ?>'+'&id='+id,
				type: 'post',   
				dataType: 'json',
				data: new FormData($('#form_upload_partner')[0]),
				cache: false,
				contentType: false,
				processData: false,   
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},  
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}
					if (json['success']) {
						alert(json['success']);
						console.log(json);
						$('#partner_image_'+json['id']).attr('value', json['filename']);
						$('#partner_image_path_'+json['id']).attr('value', json['filepath']);
						var previewHtml = '<a target="_blank" class = "btn btn-default partnersquery_data[' + json['id'] + ']" style="cursor: pointer;margin-left:10px; border-radius: 3px;margin-top: 5px;" id="partnersquery_data[' + json['id'] + ']" href="'+json['filepath']+'">View Document</a>';
						$('#partnersquery_data_'+json['id']).hide();
						$('#button-other_document_1_new'+json['id']).html('');
						$('#button-other_document_1_new'+json['id']).append(previewHtml);
					}
				},      
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//Add partners ends


var shareColorIncr = <?php echo $shareColorIncr; ?>;
function addOwnertoShareColor() {
	shareColorIncr++;
	html  = '';
	html += '<tr id="share_color_recurring-row' + shareColorIncr + '">';
	html += '  <td class="left">';
	html += '<label>'+shareColorIncr+'</label>';
	html += '  </td>';
	html += '  <td class="left">';
	html += '  <input type="text" name="owner_shared_color[' + shareColorIncr + '][shared_owner_name]" value="" placeholder="Owner Name" id="shareOwnerName_'+ shareColorIncr + '" class="form-control shareOwner" autocomplete="off" />';
	html += '  <input type="hidden" name="owner_shared_color[' + shareColorIncr + '][shared_owner_id]" value="" placeholder="Owner Id" id="ownerId_'+ shareColorIncr + '" class="form-control" />';
	html += '  </td>';
	html += '  <td class="left">';
	html += '    <a onclick="$(\'#share_color_recurring-row' + shareColorIncr + '\').remove()" data-toggle="tooltip" title="<?php echo "BTN Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
	html += '  </td>';
	html += '</tr>';

	$('#addOwnertoShareColor tbody').append(html);

	$('.shareOwner').autocomplete({

		'source': function(request, response) {
			var strng = $(this).attr('id');
			var s_id = strng.split('_');
			$('input[name=\'owner_shared_color[' + s_id[1] + '][shared_owner_id]\']').val('');
			$.ajax({
				url: 'index.php?route=catalog/owner/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request)+'&owner_Id=' + encodeURIComponent(ownerId),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['owner_name'],
							value: item['owner_id'],
						}
					}));
				}
			});
		},
		'select': function(item) {
			var strng = $(this).attr('id');
			var s_id = strng.split('_');
			$('input[name=\'owner_shared_color[' + s_id[1] + '][shared_owner_name]\']').val(item['label']);
			$('input[name=\'owner_shared_color[' + s_id[1] + '][shared_owner_id]\']').val(item['value']);
		}
	});
}

//share owner autocomplete start
$('.shareOwner').autocomplete({

	'source': function(request, response) {
		var strng = $(this).attr('id');
		var s_id = strng.split('_');
		$('input[name=\'owner_shared_color[' + s_id[1] + '][shared_owner_id]\']').val('');
		$.ajax({
			url: 'index.php?route=catalog/owner/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request)+'&owner_Id=' + encodeURIComponent(ownerId),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['owner_name'],
						value: item['owner_id'],
					}
				}));
			}
		});
	},
	'select': function(item) {
		var strng = $(this).attr('id');
		var s_id = strng.split('_');
		$('input[name=\'owner_shared_color[' + s_id[1] + '][shared_owner_name]\']').val(item['label']);
		$('input[name=\'owner_shared_color[' + s_id[1] + '][shared_owner_id]\']').val(item['value']);
	}
});
//share owner autocomplete end

var itrUpload = <?php echo $itrUpload; ?>;
function ITR_Upload() {
	itrUpload++;
	html  = '';
	html += '<tr id="itr_recurring-row' + itrUpload + '">';
	html += '<td class="left">';
	html += '<label>'+itrUpload+'</label>';
	html += '</td>';
	html += '<td class="left">';
	html += '<input type="text" name="itrDatas[' + itrUpload + '][assessment_year]" value="" placeholder="Assesment Year" class="form-control" />';
	html += '</td>';
	html += '<td class="left">';
	html += '<input type="text" readonly name="itrDatas[' + itrUpload + '][file]"     id="image_' + itrUpload + '" value="" placeholder="choose file" class="form-control" />';
	html += '<input type="hidden"          name="itrDatas[' + itrUpload + '][file_path][imagepath]" id="image_path_' + itrUpload + '" value=""  class="form-control" />';
	html += '<button type="button" class="button-upload btn btn-default" style="margin-top: 5px;" id="button-upload_'+ itrUpload + '" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>';
	html += '<span id="button-other_document_'+itrUpload+'"></span>';
	html += '</td>';
	html += '<td class="left">';
	html += '<a onclick="$(\'#itr_recurring-row' + itrUpload + '\').remove()" data-toggle="tooltip" title="<?php echo "BTN Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
	html += '</td>';
	html += '</tr>';

	$('#itrUpload tbody').append(html);
}

$(document).delegate('.button-upload', 'click', function() {
	idss = $(this).attr('id');
	s_id = idss.split('_');
	id = s_id[1];
	$('#form_upload_itr').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form_upload_itr" style="display: none;"><input type="file" name="file" /></form>');
	$('#form_upload_itr input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
			clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form_upload_itr input[name=\'file\']').val() != '') {
			clearInterval(timer);   
			id = id;
			$.ajax({
				url: 'index.php?route=catalog/owner/upload_itr&token=<?php echo $token; ?>'+'&id='+id,
				type: 'post',   
				dataType: 'json',
				data: new FormData($('#form_upload_itr')[0]),
				cache: false,
				contentType: false,
				processData: false,   
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},  
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}
					if (json['success']) {
						alert(json['success']);
						console.log(json);
						$('#image_'+json['id']).attr('value', json['filename']);
						$('#image_path_'+json['id']).attr('value', json['filepath']);
						var previewHtml = '<a target="_blank" class = "btn btn-default" style="cursor: pointer;margin-left:5px;" id = "btn_view_'+json['id']+'" href="'+json['filepath']+'">View Document</a>';
                        $('#btn_view_'+json['id']).remove();
                        $('#button-other_document_'+json['id']).append(previewHtml);
					}
				},      
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});

var color = <?php echo $color; ?>;
function addColor() {
	color++;
	html  = '';
	html += '<tr id="addColor_recurring-row' + color + '">';
	html += '  <td class="left">';
	html += '<label>Color '+color+'</label>';
	html += '  </td>';
	html += '  <td class="left">';
	html += '  <input type="text" name="add_color[' + color + '][color]" id="add_color_' + color + '" value="" placeholder="Color Name" class="form-control colors" autocomplete="off" />';
	html += '  </td>';
	html += '  <td class="left">';
	html += '    <a onclick="$(\'#addColor_recurring-row' + color + '\').remove()" data-toggle="tooltip" title="<?php echo "BTN Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
	html += '  </td>';
	html += '</tr>';

	$('#tbl_add_Color tbody').append(html);

	$('.colors').autocomplete({

		'source': function(request, response) {
			var strng = $(this).attr('id');
			var s_id = strng.split('_');
			$('input[name=\'add_color[' + s_id[1] + '][color]\']').val('');
			$.ajax({
				url: 'index.php?route=catalog/owner/colorauto&token=<?php echo $token; ?>&filter_color=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['color'],
							value: item['color'],
						}
					}));
				}
			});
		},
		'select': function(item) {
			var strng = $(this).attr('id');
			var s_id = strng.split('_');
			$('input[name=\'add_color[' + s_id[2] + '][color]\']').val(item['label']);
			$('input[name=\'add_color[' + s_id[2] + '][color]\']').val(item['value']);
		}
	});
}


$('input[name=\'path\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
	  dataType: 'json',
	  success: function(json) {
		json.unshift({
		  category_id: 0,
		  name: '<?php echo $text_none; ?>'
		});

		response($.map(json, function(item) {
		  return {
			label: item['name'],
			value: item['category_id']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'path\']').val(item['label']);
	$('input[name=\'parent_id\']').val(item['value']);
  }
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['name'],
			value: item['filter_id']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter\']').val('');

	$('#category-filter' + item['value']).remove();

	$('#category-filter').append('<div id="category-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="category_filter[]" value="' + item['value'] + '" /></div>');
  }
});

$('#category-filter').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript">
$(document).ready(function(){
	$('[data-index= 1]').focus();
});
$('#form-category').on('keydown','input', function (event) {
	if (event.which == 13) {
		event.preventDefault();
		var $this = $(event.target);
		var index = parseFloat($this.attr('data-index'));
		$('[data-index="' + (index + 1).toString() + '"]').focus();
	}
});

$('#form-category').on('keydown','select', function (event) {
	if (event.which == 13) {
		event.preventDefault();
		var $this = $(event.target);
		var index = parseFloat($this.attr('data-index'));
		//console.log(index);
		$('[data-index="' + (index + 1).toString() + '"]').focus();
	}
});

$('#button-other_document_1').on('click', function() {
	$('#form-other_document_1').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form-other_document_1" style="display: none;"><input type="file" name="file" /></form>');
	$('#form-other_document_1 input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
		clearInterval(timer);
	}
	timer = setInterval(function() {
	  if ($('#form-other_document_1 input[name=\'file\']').val() != '') {
		clearInterval(timer); 
		image_name = 'file_number';  
		$.ajax({ 
		url: 'index.php?route=catalog/owner/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-other_document_1')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
		  alert(json['error']);
		  }
		  if (json['success']) {
		  alert(json['success']);
		  console.log(json);
		  $('input[name=\'file_number_upload\']').attr('value', json['filename']);
		  $('input[name=\'uploaded_file_source\']').attr('value', json['link_href']);
		  d = new Date();
		  var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="uploaded_file_source" href="'+json['link_href']+'">View Document</a>';
		  $('#uploaded_file_source').remove();
		  $('#button-other_document_1_new').append(previewHtml);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
		});
	  }
	  }, 500);
	});

/*$('#form-category').on('keydown','.nav li a', function (event) {      
	if (event.which == 9) {
		event.preventDefault();
		var $this = $(event.target);
		var index = parseFloat($this.attr('data-index'));
		var a = $('[data-index="' + (index + 1).toString() + '"]');
		var dataIndex = a.selector;
		var id = $(dataIndex).attr('href');
		console.log(id);
		$('#tabOwnerContactTaxDetails').addClass('active');
	}
});*/

$( document ).ready(function() {
	var selectownershipType = $("#select-ownershipType").val();
	if(selectownershipType == 'Individual'){
		$('#partnershipOwnerDiv').hide();
	} else {
		$('#partnershipOwnerDiv').show();
	}

});

$( "#select-ownershipType" ).click(function(){
	var selectownershipType = $("#select-ownershipType").val();
	if(selectownershipType == 'Individual'){
		$('#partnershipOwnerDiv').hide();
	} else {
		$('#partnershipOwnerDiv').show();
	}
});

 function ownerfilter() { 
	var onDate = $('#onDate').val();
	/*$('#forth_list').addClass('active');
	$('#first_list')..removeAttr('class');
	$('#tabHorsesWithOwner').addClass('active');
	$('#tabOwnerBasicDetails').removeAttr('class');*/
	var hourse_owner_id_filter = $('#hourse_owner_id_filter').val();
   // if(onDate != ''){
		url = 'index.php?route=catalog/owner/edit&token=<?php echo $token; ?>';
		if (onDate) {
		  url += '&date_filer=' + encodeURIComponent(onDate);
		}

		 if (hourse_owner_id_filter) {
			url += '&ownerId=' + encodeURIComponent(hourse_owner_id_filter);
		}
		window.location.href = url;
   // } 
}

$( document ).ready(function() {
	 $( "#owner_code" ).focus();
 });




/*$(document).delegate( ".relationship" ).change(function(){
	idss = $(this).attr('id');
	alert(idss);
	s_id = idss.split('_');
	id = s_id[1];
	relation = $('#select-relationship_'+ id +'').val();
	if (relation == 'Other') {
		$('#relationship_'+ id +'').show();
	} else {
		$('#relationship_'+ id +'').hide();
	}

});*/

function country(element, zone_id) {
	$.ajax({
		url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + element.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country1\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			console.log(json['zone']);
			if (json['postcode_required'] == '1') {
				$('input[name=\'country1[postcode]\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'country1[postcode]\']').parent().parent().removeClass('required');
			}

			html = '<option value="">Please Select</option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == zone_id) {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0">None</option>';
			}

			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
}

$('select[name$=\'[country_id]\']').trigger('change');


function countrys(element, zone_id) {
	$.ajax({
		url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + element.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country2\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			console.log(json['zone']);
			if (json['postcode_required'] == '1') {
				$('input[name=\'country[postcode]\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'country[postcode]\']').parent().parent().removeClass('required');
			}

			html = '<option value="">Please Select</option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == zone_id) {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0">None</option>';
			}

			$('select[name=\'zone_id2\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
}

$('select[name$=\'[country_id]\']').trigger('change');


</script>

<script type="text/javascript">
$("#select-approved_status").change(function(){
	$(this).find("option:selected").each(function(){
		var optionValue = $(this).attr("value");
		if (optionValue == 'Yes') {
			$("#approval_dates").show();
			$("#approval_datess").show();
		}
		else{
			$("#approval_dates").hide();
			$("#approval_datess").hide();
		}
	});
});
</script>
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script></div>

<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>  -->
<!-- <script type="text/javascript" src="view/javascript/jquery/tablesort.js"></script> 
<link rel="stylesheet" type="text/css" href="view/stylesheet/tablesort.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="view/stylesheet/styles.css">
 -->
<!-- <script type="text/javascript">
$(function () {
	$('table.table-sort').tablesort();
});
</script> -->

<!-- <script type="text/javascript">

$(function () {

$('table.table-sort').tablesort();

});

</script> -->



<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
	$('th').click(function(){
    var table = $(this).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
    this.asc = !this.asc
    if (!this.asc){rows = rows.reverse()}
    for (var i = 0; i < rows.length; i++){table.append(rows[i])}
})
function comparer(index) {
    return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
    }
}
function getCellValue(row, index){ return $(row).children('td').eq(index).text() }
</script> -->
<link rel="stylesheet" href="view/stylesheet/theme.default.css">
<!-- <script type="text/javascript" src="view/javascript/jquery/jquery-latest.js"></script> -->
<script type="text/javascript" src="view/javascript/jquery/jquery.tablesorter.js"></script>

<!-- tablesorter widgets (optional) -->
<script type="text/javascript" src="view/javascript/jquery/jquery.tablesorter.widgets.js"></script>

<script type="text/javascript">
$(function() {
  $("#myTable").tablesorter();
});

$(function() {
  $("#myTable").tablesorter({ sortList: [[0,0], [1,0]] });
});
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable >tbody >tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

$('#button-other_documents_1').on('click', function() {
$('#form-other_document_1').remove();
$('body').prepend('<form enctype="multipart/form-data" id="form-other_document_1" style="display: none;"><input type="file" name="file" /></form>');
$('#form-other_document_1 input[name=\'file\']').trigger('click');
if (typeof timer != 'undefined') {
    clearInterval(timer);
}
timer = setInterval(function() {
  if ($('#form-other_document_1 input[name=\'file\']').val() != '') {
    clearInterval(timer); 
    image_name = 'file_number';  
    $.ajax({ 
    url: 'index.php?route=catalog/owner/upload_profile&token=<?php echo $token; ?>'+'&image_name='+image_name,
    type: 'post',   
    dataType: 'json',
    data: new FormData($('#form-other_document_1')[0]),
    cache: false,
    contentType: false,
    processData: false,   
    beforeSend: function() {
      $('#button-upload').button('loading');
    },
    complete: function() {
      $('#button-upload').button('reset');
    },  
    success: function(json) {
      if (json['error']) {
      alert(json['error']);
      }
      if (json['success']) {
      alert(json['success']);
      console.log(json);
      $('input[name=\'file_number_uploads\']').attr('value', json['filename']);
      $('input[name=\'uploaded_file_sources\']').attr('value', json['link_href']);
      d = new Date();
      $('#blah').remove();
      var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="uploaded_file_sources" href="'+json['link_href']+'">View Document</a>';
      var image_data = '<img src="'+json['link_href']+'" height="60" id="blah" alt=""  />'
      $('#uploaded_file_sources').remove();
       $('#profile_pic').append(image_data);
      }
    },      
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
    });
  }
  }, 500);
});

function readURL(input) {
    alert(input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(100)
                .height(80);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('input', '#input-owner_name', function(){
	var name = $( "#input-owner_name" ).val();
	$( "#name1" ).val(name);
});
</script>

<?php echo $footer; ?>