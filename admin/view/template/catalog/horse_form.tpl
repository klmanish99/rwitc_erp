<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-horse" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<link type="text/css" href="view/stylesheet/myform.css" rel="stylesheet" media="screen" />
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading" style="padding: 5px 15px;">
				<h3 class="col-sm-2 panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
                <?php if (isset($horse_id)) { ?>
                    <div style="padding-left: 40%;">
                        <b><label style="font-family: cursive;color: #00a04d;font-size: 20px;border: none;background: #fcfcfc;width: 80%;text-transform: uppercase;"><?php echo $horse_name; ?></label></b>
                        <b>Horse Code : <?php echo $horse_code; ?> </b>
                    </div>
                <?php } else { ?>
                    <div style="padding-left: 40%;" class="">
                        <b><input style="border: none;background: #fcfcfc;width: 95%;" disabled="disabled" id="name1" value="" name=""></b>
                    </div>
                <?php } ?>
            </div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-horse" class="form-horizontal">
					<ul class="nav nav-tabs" id="myTab">
						<li id="basic_tab_class" class="active"><a href="#tab-horse_basic" data-toggle="tab"><?php echo "Horse Basic Details"; ?></a></li>
						<li id="trainer_tab_class"><a href="#tab-trainer" data-toggle="tab"><?php echo "Trainer Details"; ?></a></li>
						<?php if($horse_id != '') { ?> 
							<li id="ownership_tab_class"><a href="#tab-ownership" data-toggle="tab"><?php echo "Ownership/Lease"; ?></a></li>
							<li><a href="#tab-BanDetails" data-toggle="tab"><?php echo "Ban Details"; ?></a></li>
							<li><a href="#tab-Change-Equipment" data-toggle="tab"><?php echo "Change Equipment"; ?></a></li>
							<li><a href="#tab-Shoeing-and-bits" data-toggle="tab"><?php echo "Shoeing and Bits"; ?></a></li>
							<li><a href="#tab-Stakes-Earned-Outstation" data-toggle="tab"><?php echo "Stakes Earned/Outstation"; ?></a></li>
						<?php } ?>
					</ul>
					<div class="tab-content">
						<div id="myhorseisbModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" style="">
							<div class="modal-dialog" style="width: 50%;height: 500px;">
							<!-- Modal content-->
								<div class="modal-content" >
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Horse</h4>
									</div>
									<div class="modal-body">
										<input type="text" name="horse_isb_name" value="" placeholder="Horse Name" id="input-horse_isb" class="form-control" tabindex="2"   />
										<input type="hidden" name="horse_isb_id" value="" />
										<div class="form-group">
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<td class="text-center"><?php echo "YROFFLNG"; ?></td>
													  <td class="text-center"><?php echo "SireName"; ?></td>
													  <td class="text-center"><?php echo "Sirenat"; ?></td>
													  <td class="text-center"><?php echo "Marename"; ?></td>
													  <td class="text-center"><?php echo "Marenat"; ?></td>
													  <td class="text-center"><?php echo "Color"; ?></td>
													  <td class="text-center"><?php echo "Sex"; ?></td>
													  <td class="text-center"><?php echo "Studname"; ?></td>
													</tr>
												</thead>
												<tbody >
													<tr id="horse_isb_table_body" style="display: none;">
													 <td class="text-center"><span id="horse_isb_year"></span></td>
													  <td style="text-transform: uppercase;" class="text-center"><span id="horse_isb_horse_name"></span></td>
													  <td class="text-center"><span id="horse_isb_country"></span></td>
													  <td class="text-center"><span id="horse_isb_mother_name"></span></td>
													  <td class="text-center"><span id="horse_isb_mother_country_name"></span></td>
													  <td class="text-center"><span id="horse_isb_color"></span></td>
													  <td class="text-center"><span id="horse_isb_sex"></span></td>
													  <td class="text-center"><span id="horse_isb_breeder"></span></td>
													  <input type="hidden" value="" id="hidden_horse_isb_id" class="form-control"/>
													  <input type="hidden" value="" id="horse_isb_micro_chip_one" class="form-control"/>
													  <input type="hidden" value="" id="horse_isb_sire_name" class="form-control"/>
													  <input type="hidden" value="" id="horse_isb_micro_chip_two" class="form-control"/>
													  <input type="hidden" value="" id="horse_isb_foal_date" class="form-control"/>
													  <input type="hidden" value="" id="horse_isb_id_by_brand" class="form-control"/>
													  <input type="hidden" value="" id="horse_input_passport" class="form-control"/>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-primary"  onclick="importhorseisb()"  >Save</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								  </div>
								</div>
							</div>
						</div>

						<div id="mynewhorseModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" style="">
							<div class="modal-dialog" style="width: 50%;height: 500px;">
							<!-- Modal content-->
								<div class="modal-content" >
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Horse</h4>
									</div>
									<div class="modal-body">
										<input type="text" name="horse_new_name" value="" placeholder="Horse Name" id="input-new_horse" class="form-control" tabindex="2"   />
									</div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-primary" onclick="importnewhorse()" >Save</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								  </div>
								</div>
							</div>
						</div>

						<div class="tab-pane active" id="tab-horse_basic">
							<input type="hidden" name="hidden_isb_horse" value="<?php echo $hidden_isb_horse; ;?>" id="hidden_isb_horse" class="form-control"/>
							<div class="form-group" hidden="hidden">
                                <label class="col-sm-2 control-label" for="horse_code"><b style="color: red">*</b>Horse code :</label>
                                <div class="col-sm-3">
                                    <input disabled="disabled" type="text" name="horse_code" value="<?php echo $horse_code; ?>" placeholder="<?php echo "Horse Code"  ;?>" id="horse_code" class="form-control" tabindex="1"/>
                                    <input type="hidden" name="hidden_horse_code" value="<?php echo $horse_code; ?>" id="horse_code" class="form-control"/>

                                    <?php if (isset($valierr_code)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_code; ?></span><?php } ?>
                                </div>

                             </div>
							<div class="form-group" >
								<div class="row">
								<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Horse Name :"; ?></label>
								<?php if($official_name_change == '1') { ?> 
									<div class="col-sm-2" >
										<input style="text-transform: uppercase;" type="text" name="horse_name" value="<?php echo $horse_name; ?>" placeholder="Horse Name" id="input-horse" class="form-control" tabindex="2" readonly="readonly"   />
										<input type="hidden" name="horse_id" value="<?php echo $horse_id; ?>" />
										<!-- <input type="hidden" name="horse_name" value="<?php echo $horse_name; ?>" placeholder="Horse Name" id="input-change_horse" class="form-control" tabindex="1"/> -->
										<?php if (isset($valierr_horse_name)) { ?><span class="errors"><?php echo $valierr_horse_name; ?></span><?php } ?>
									</div>
								<?php } else { ?>
									<div class="col-sm-2" >
										<input style="text-transform: uppercase;" type="text" name="horse_name" value="<?php echo $horse_name; ?>" placeholder="Horse Name" id="input-horse" class="form-control" tabindex="2" readonly="readonly"/>
										<input type="hidden" name="horse_id" value="<?php echo $horse_id; ?>" />
										<!-- <input type="hidden" name="horse_name" value="<?php echo $horse_name; ?>" placeholder="Horse Name" id="input-change_horse" class="form-control" tabindex="1"/> -->
										<?php if (isset($valierr_horse_name)) { ?><span class="errors"><?php echo $valierr_horse_name; ?></span><?php } ?>
									</div>
								<?php } ?>
								<label hidden="hidden" class="col-sm-2 control-label" for="input-top"><?php echo "Official Name Change"; ?></label>
								<div hidden="hidden" class="col-sm-2">
									<?php if($official_name_change == '1') { ?>
									  	<div class="checkbox" >
											<label>
												<input type="hidden" name="official_name_change" value="0" />
											  	<input type="checkbox" onclick="return false" name="official_name_change" value="1"  id="official_name_change" <?php if ($official_name_change == 1){ echo 'checked="checked"'; } ?> class="form-control" tabindex="2" />
											</label>
										</div>
									<?php } else { ?>
										<div class="checkbox" >
											<label>
												<input type="hidden" name="official_name_change" value="0" />
											  	<input type="checkbox" name="official_name_change" value="1"  id="official_name_change" <?php if ($official_name_change == 1){ echo 'checked="checked"'; } ?> class="form-control" tabindex="2"/>
											</label>
										</div>
									<?php } ?>
								</div>
								<?php if($name_registration == 'Change Of Name') { ?>
								<div>
									<label class="col-sm-2 control-label" for="input-horse_change"><b style="color: red">*</b><?php echo "Horse Late Name :"; ?></label>
									<div class="col-sm-2" >
										<input type="text" name="changehorse_name" value="<?php echo $changehorse_name; ?>" placeholder="Change Horse Name" id="change_horse_name" class="form-control" tabindex="" readonly="readonly" />
										<?php if (isset($valierr_change_horse_name)) { ?><span class="errors"><?php echo $valierr_change_horse_name; ?></span><?php } ?> 
									</div>
									<label class="col-sm-2 control-label" for="input-date-available"><?php echo "Change Horse Date:"; ?></label>
									<?php if($official_name_change == '1') { ?>
										<div class="col-sm-2">
											<div class="input-group input-change_horse_dates">
												<input type="text" name="change_horse_dates" value="<?php echo $change_horse_dates; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="change_horse_date" class="form-control" readonly="readonly" tabindex="2"  />
											</div>
											<?php if (isset($valierr_change_horse_date)) { ?><span class="errors" id="error_change_horse_date_post"><?php echo $valierr_change_horse_date; ?></span><?php } ?>
											<span style="display: none;color: red;font-weight: bold;" id="error_change_horse_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
										</div>
									<?php } else { ?>
										<div class="col-sm-2">
											<div class="input-group input-change_horse_dates">
												<input type="text" name="change_horse_dates" value="<?php echo $change_horse_dates; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="change_horse_date" class="form-control" tabindex="3" />
												<span class="input-group-btn">
													<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<?php if (isset($valierr_change_horse_date)) { ?><span class="errors" id="error_change_horse_date_post"><?php echo $valierr_change_horse_date; ?></span><?php } ?>
											<span style="display: none;color: red;font-weight: bold;" id="error_change_horse_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
										</div>
									<?php } ?>
								</div>
								<?php } ?>
								</div>
								<?php if($horse_id == '') { ?> 
									<div class="row">
										<label class="col-sm-2 control-label" for="input-horse"></label>
										<div style="margin-top: 10px;" class="col-sm-6">
												<a  class="btn-default isbhorse" data-toggle="modal" data-target="#myhorseisbModal" style="padding: 6px;cursor: pointer;background: #1e91cf;color: white;font-weight: bold;line-height: 25px;border: 1px solid;border-color: #115376;border-radius: 3px;" onclick="blankisb_horse()">SBAI Horse</a>
												<a  class="btn-default" data-toggle="modal" data-target="#mynewhorseModal" style="padding: 6px;cursor: pointer;background: #1e91cf;color: white;font-weight: bold;line-height: 25px;border: 1px solid;border-color: #115376;border-radius: 3px;" >Create Your Own Horse</a>
										</div>
									</div>
								<?php } ?>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-rating"><?php echo "Rating:"; ?></label>
								<div class="col-sm-2">
									<input type="number" name="rating" value="<?php echo $rating; ?>" placeholder="<?php echo "Rating"; ?>" id="input-rating" class="form-control"  tabindex="17" readonly="readonly/">
								</div>
								<label class="col-sm-2 control-label" for="input-sire"><b style="color: red">*</b><?php echo "Sire/ Nat"; ?></label>
								<div class="col-sm-2">
									<input type="text" name="sire" value="<?php echo $sire; ?>" placeholder="<?php echo "Sire/ Nat"; ?>" id="input-sire" class="form-control" tabindex="7"/>
									<?php if (isset($valierr_sire)) { ?><span class="errors"><?php echo $valierr_sire; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-dam_nat"><b style="color: red">*</b><?php echo "Dam/ Nat:"; ?></label>
								<div class="col-sm-2">
									<input type="text" name="dam" value="<?php echo $dam; ?>" placeholder="<?php echo "Dam/ Nat"; ?>" id="input-dam_nat" class="form-control" tabindex="8"/>
									<?php if (isset($valierr_dam)) { ?><span class="errors"><?php echo $valierr_dam; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-color"><b style="color: red">*</b><?php echo "Color:"; ?></label>
								<div class="col-sm-2">
									<input type="text" name="color" value="<?php echo $color; ?>" placeholder="<?php echo "Color"; ?>" id="input-color" class="form-control"  tabindex="13"/>
									<?php if (isset($valierr_color)) { ?><span class="errors"><?php echo $valierr_color; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-origin"><b style="color: red">*</b><?php echo "Origin:"; ?></label>
								<div class="col-sm-2">
									<select name="origin" id="input_origin" class="form-control"  tabindex="14">
										<option value="" >Please Select</option>
										<?php foreach ($origins as $originkey => $originvalue) { ?>
										<?php if ($originkey == $origin) { ?>
										<option value="<?php echo $originvalue; ?>" selected="selected"><?php echo $originvalue; ?></option>
										<?php } else { ?>
										<option value="<?php echo $originvalue; ?>" ><?php echo $originvalue; ?></option>
										<?php } ?>
										<?php } ?>
								  </select>
								<?php if (isset($valierr_origin)) { ?><span class="errors"><?php echo $valierr_origin; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-sex"><b style="color: red">*</b><?php echo "Sex:"; ?></label>
								<div class="col-sm-2">
								<select name="sex" id="input_sex" class="form-control"  tabindex="15">
									<option value="" >Please Select</option>
									<?php foreach ($sexs as $sexkey =>  $sexvalue) { ?>
									<?php if ($sexkey == $sex) { ?>
									<option value="<?php echo $sexkey ?>" selected="selected"><?php echo $sexvalue; ?></option>
									<?php } else { ?>
									<option value="<?php echo $sexkey; ?>"><?php echo $sexvalue; ?></option>
									<?php } ?>
									<?php } ?>
								 </select>
								<?php if (isset($valierr_sex)) { ?><span class="errors"><?php echo $valierr_sex; ?></span><?php } ?>
								</div>
							</div>
							<div style="display: none;" id="date_sex_date_div" class="form-group">
								<label style="display: none;" class="col-sm-2 control-label" id="l_date_sex_date" for="input-sex"><?php echo "Castration Date:"; ?></label>
								<div class="col-sm-2">
									<div  class="input-group date input-date_sex_date">
										<input type="text" name="castration_date" value="<?php echo $castration_date ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="castration_date" class="form-control" tabindex="12" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-date-available"><?php echo "Registration Date:"; ?></label>
								<div class="col-sm-2">
									<div class="input-group date input-registeration_date">
										<input type="text" name="registeration_date" value="<?php echo $registeration_date; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="registeration_date" class="form-control" tabindex="5"  readonly="readonly"/>
										<span class="input-group-btn" >
											<button class="btn btn-default" type="button" style="display: none;"><i class="fa fa-calendar" ></i></button>
										</span>
									</div>
									<?php if (isset($valierr_registeration_date)) { ?><span class="errors" id="error_registeration_date_post"><?php echo $valierr_registeration_date; ?></span><?php } ?>
									<span style="display: none;color: red;font-weight: bold;" id="error_registeration_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
								</div>
								<label class="col-sm-2 control-label" for="input-registration_auth"><b style="color: red">*</b><?php echo "Registration Auth:"; ?></label>
								<div class="col-sm-2">
									<select name="registerarton_authentication" id="registerarton_authentication_id" class="form-control" tabindex="6">
										<option value="" >Please Select</option>
										<?php foreach ($regiteration_authenticaton_datas as $reg_aut_key => $reg_aut_value) { ?>
										<?php if ($reg_aut_value == $registerarton_authentication) { ?>
										<option value="<?php echo $reg_aut_value; ?>" selected="selected"><?php echo $reg_aut_value; ?></option>
										<?php } else { ?>
										<option value="<?php echo $reg_aut_value; ?>"><?php echo $reg_aut_value; ?></option>
										<?php } ?>
										<?php } ?>
								  	</select>
									<?php if (isset($valierr_registerarton_authentication)) { ?><span class="errors"><?php echo $valierr_registerarton_authentication; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-passport"><?php echo "Passport/Life Number:"; ?></label>
								<div class="col-sm-2">
									<input type="text" name="passport_no" value="<?php echo $passport_no; ?>" placeholder="<?php echo "Passport/Life Number"; ?>" id="input-passport" class="form-control"  tabindex="4" />
									<input type="hidden" name="passport_no_hidden" value="<?php echo $passport_no; ?>" />
									<?php if (isset($valierr_passport_no)) { ?><span class="errors"><?php echo $valierr_passport_no; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-micro_chip1"><?php echo "Micro Chip no 1:"; ?></label>
								<div class="col-sm-2">
									<input type="text" name="chip_no_one" value="<?php echo $chip_no_one; ?>" placeholder="<?php echo "Micro Chip no 1"; ?>" id="input-micro_chip1" class="form-control" tabindex="9"/>
									<?php if (isset($valierr_chip_no_one)) { ?><span class="errors"><?php echo $valierr_chip_no_one; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-micro_chip_2"><?php echo "Micro Chip no 2:"; ?></label>
								<div class="col-sm-2">
									<input type="text" name="chip_no_two" value="<?php echo $chip_no_two; ?>" placeholder="<?php echo "Micro Chip no 2"; ?>" id="input-micro_chip_2" class="form-control" tabindex="10" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Foal Date:"; ?></label>
								<div class="col-sm-3">
									<div class="input-group date input-date_foal_date">
										<input type="text" name="date_foal_date" value="<?php echo $date_foal_date; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_foal_date" class="form-control" tabindex="12" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<span style="display: none;color: red;font-weight: bold;" id="error_date_foal_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
									<?php if (isset($valierr_foal_date)) { ?><span class="errors" id="error_date_foal_date_post"><?php echo $valierr_foal_date; ?></span><?php } ?>
								</div>
								<label class="col-sm-1 control-label" for="input-horse"><?php echo "Age:"; ?></label>
								<div class="col-sm-1">
									<input style="color: black;font-size: 15px;font-weight: 600;" type="Number" name="age" value="<?php echo $age ?>" placeholder="Age" id="input-age" class="form-control"   readonly="readonly"  />
									<?php if (isset($valierr_age)) { ?><span class="errors"><?php echo $valierr_age; ?></span><?php } ?>
								</div>
								
							</div>

							
							

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-stud_farm"><?php echo "Stud Farm:"; ?></label>
								<div class="col-sm-3">
									<textarea rows="3" name="stud_form"  placeholder="Stud Farm" id="input-stud_farm" class="form-control" tabindex="16"><?php echo $stud_form;?></textarea>
									<?php if (isset($valierr_stud_form)) { ?><span class="errors"><?php echo $valierr_stud_form; ?></span><?php } ?>
								</div>
								<label class="col-sm-1 control-label" for="input-breeder"><?php echo "Breeder:"; ?></label>
								<div class="col-sm-3">
									<textarea rows="3" name="breeder"  placeholder="Breeder" id="input-breeder" class="form-control" tabindex="18"><?php echo $breeder;?></textarea>
									<?php if (isset($valierr_breeder)) { ?><span class="errors"><?php echo $valierr_breeder; ?></span><?php } ?>
								</div>
								<label class="col-sm-1 control-label" for="input-saddle_no"><?php echo "Saddle No:"; ?></label>
								<div class="col-sm-2">
									<input type="text" name="saddle_no" value="<?php echo $saddle_no; ?>" placeholder="<?php echo "Saddle No"; ?>" id="input-saddle_no" class="form-control"  tabindex="19" />
									<?php if (isset($valierr_saddle_no)) { ?><span class="errors"><?php echo $valierr_saddle_no; ?></span><?php } ?>
								</div>
								
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "St Stall Certificate:"; ?></label>
								<div class="col-sm-2">
									<div class="input-group date input-date_std_stall_certificate">
										<input type="text" name="date_std_stall_certificate" value="<?php echo $date_std_stall_certificate; ?>" data-index="21" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_std_stall_certificate" class="form-control" tabindex="22" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<?php if (isset($valierr_std_stall_certificate_date)) { ?><span class="errors" id="error_date_std_stall_certificate_post"><?php echo $valierr_std_stall_certificate_date; ?></span><?php } ?>
									<span style="display: none;color: red;font-weight: bold;" id="error_date_std_stall_certificate" class="error"><?php echo 'Please Select Valid Date'; ?></span>
								</div>
								<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "St Stall Remark:"; ?></label>
								<div class="col-sm-3">
									<input type="text" name="std_stall_remarks" value="<?php echo $std_stall_remarks; ?>" placeholder="<?php echo "St Stall Remark"; ?>" id="input-stall_certificate" class="form-control" tabindex="24" />
									<?php if (isset($valierr_std_stall_remarks)) { ?><span class="errors"><?php echo $valierr_std_stall_remarks; ?></span><?php } ?>
								</div>
							</div>

							<div class="form-group">
								
								<label class="col-sm-2 control-label" for="input-awbi"><?php echo "AWBI Registration No:"; ?></label>
								<div class="col-sm-2">
									<input type="text" name="awbi_registration_no" value="<?php echo $awbi_registration_no; ?>" placeholder="<?php echo "AWBI Registration No:"; ?>" id="input-awbi" class="form-control" tabindex="21"/>
									<?php if (isset($valierr_awbi_registration_no)) { ?><span class="errors"><?php echo $valierr_awbi_registration_no; ?></span><?php } ?>
								</div>
								<label class="col-sm-2 control-label" for="input-octroi"><?php echo "Octroi Details:"; ?></label>
								<div class="col-sm-2">
									<input type="text" name="octroi_details" value="<?php echo $octroi_details; ?>" placeholder="<?php echo "Octroi Details"; ?>" id="input-octroi" class="form-control"  tabindex="20" />
									<?php if (isset($valierr_octroi_details)) { ?><span class="errors"><?php echo $valierr_octroi_details; ?></span><?php } ?>
								</div>
							</div>
							<?php if ($license_type == 'A') { ?>
								<div class="form-group">
	                                <div class="arrival_charges">
										<label class="col-sm-2 control-label" for="input-horse"><?php echo "Arrival Charges to be Paid:"; ?></label>
										<div class="col-sm-2">
											<input type="number" name="arrival_charges_to_be_paid" value="<?php echo $arrival_charges_to_be_paid; ?>" id = "arrival_charges_to_be_paid" placeholder="<?php echo "Arrival Charges to be Paid"; ?>"  class="form-control"  tabindex="11" readonly="readonly" />
											<?php if (isset($valierr_arrival_charges_to_be_paid)) { ?><span class="errors"><?php echo $valierr_arrival_charges_to_be_paid; ?></span><?php } ?>
										</div>
									</div>
								</div>
									<?php } elseif ($license_type == 'B') { ?>
									<div class="form-group">
		                                <div class="one_time_levy">
											<label class="col-sm-2 control-label" for="input-horse"><?php echo "One Time Levy Charge:"; ?></label>
											<div class="col-sm-3">
												<input type="number" name="one_time_lavy" value="<?php echo $one_time_lavy; ?>" id = "one_time_lavy" placeholder="<?php echo "One Time Levy Charge"; ?>"  class="form-control"  tabindex="11" readonly="readonly"/>
											</div>
										</div>
									</div>
							<?php } ?>
							<div style="display: none;" class="form-group">
								<label class="col-sm-2 control-label" for="input-file"><?php echo "" ?></label>
								<div class="col-sm-3"  >
									<input readonly="readonly" type="text" name="awbi_registration_file" value="<?php echo $awbi_registration_file; ?>" placeholder="Awbi Registration File" id="input-other_document_1" class="form-control"  />
									<input type="hidden" name="awbi_registration_file_source" value="<?php echo $awbi_registration_file_source; ?>" id="input-other_document_1_source" class="form-control" />
									<span class="input-group-btn" style="padding-top: 2%;">
			  							<button type="button" id="button-other_document_1" style = "border-radius: 3px;" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary" tabindex="20"><i class="fa fa-upload"></i> <?php echo 'Upload Document'; ?></button>
										<span id="button-other_document_1_new"></span>
										<?php if($awbi_registration_file_source != ''){ ?>
											<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="awbi_registration_file_source" href="<?php echo $awbi_registration_file_source; ?>">View Document</a>
										<?php } ?>
									</span>
								</div>
							</div>
							<div class="form-group">
								
								<label class="col-sm-2 control-label" for="input-id_by_brand"><?php echo "ID By Brand:"; ?></label>
								<div class="col-sm-3">
									<textarea rows="2" name="Id_by_brand"  placeholder="ID By Brand" id="input-id_by_brand" class="form-control" tabindex="23"><?php echo $Id_by_brand;?></textarea>
									<?php if (isset($valierr_Id_by_brand)) { ?><span class="errors"><?php echo $valierr_Id_by_brand; ?></span><?php } ?>
								</div>
							</div>
							<div class="form-group">
                                <label class="col-sm-2 control-label" for="select-isActive">Status</label>
                                <div class="col-sm-3">
                                    <input type="hidden" name="hidden_active" value="<?php echo $isActive1; ?>">
                                    <select name="isActive" id="select-isActive" class="form-control" data-index="">
                                        <?php foreach ($Active as $key => $value) { ?>
                                        <?php if ($key == $isActive) { ?>
                                          <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                          <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                <?php if (isset($error_status)) { ?><span class="errors" style="color: #ff0000;"><?php echo $error_status; ?></span><?php } ?>
                            	</div>
                                <label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Remarks:"; ?></label>
								<div class="col-sm-5">
									<textarea cols="100" name="horse_remarks" placeholder="Remarks" id = "horse_remarks"  class="form-control" tabindex="10"><?php echo $horse_remarks; ?></textarea>
									<?php if (isset($remarks_errors)) { ?><span class="errors"><?php echo $remarks_errors; ?></span><?php } ?>
								</div>
                            </div>
                            <div class="form-group">
						<div style="margin-left: 50%">
							<button type="button" class="btn btn-primary" id="general_save" >Save</button>
						</div>
					</div>
						</div>
						<div class="tab-pane" id="tab-trainer">
							<form id="trainer_form">
								<h3  style="color: black;font-weight: bold;text-align: center;" >Current Trainer</h3>
								<hr style="width: 165px;">
								<div class="form-group">
										<label style="width: 130px;" class="col-sm-2 control-label" for="input-trainer_code"><b style="color: red"></b><?php echo "Trainer Code:"; ?></label>
										<div style="width: 115px;" class="col-sm-2">
											<input type="text" name="trainer_codes_id" value="<?php echo $trainer_codes_id; ?>" placeholder="<?php echo "Trainer Code"; ?>" id="" class="form-control"  readonly/>
										</div>
										<input type="hidden" id="tariner_horse_id" value="<?php echo $horse_id; ?>" />
										<label style="width: 130px;" class="col-sm-2 control-label" for="input-trainer_name"><b style="color: red"></b><?php echo "Trainer Name:"; ?></label>
										<div class="col-sm-2">
											<input readonly="readonly" type="text" name="trainer_name"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $trainer_name; ?>" id="" class="form-control" />
											<input type="hidden" name="trainer_id"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $trainer_id; ?>" id="trainer_id" class="form-control" />
											<input type="hidden" id="horse_to_trainer_id" name="horse_to_trainer_id"   value="<?php echo $horse_to_trainer_id; ?>"  class="form-control" />
										</div>
										<label style="width: 130px" class="col-sm-2 control-label" for="input-date_charge_trainer"><?php echo "Date Of Charge:"; ?></label>
										<div class="col-sm-2">
											<input type="text" name="" value="<?php echo $date_charge_trainer; ?>" placeholder="<?php echo "Date Of Charge"; ?>" id="" class="form-control"  readonly/>
										</div>
										<label style="width: 100px" class="col-sm-2 control-label" for="input-date_charge_trainer"><?php echo "Narration:"; ?></label>
										<div class="col-sm-2">
											<input type="text" name="" value="<?php echo $extra_narrration_trainer; ?>" id="" class="form-control"  readonly/>
											<!-- <a style="font-size: 12px;font-weight: bold;color: black;" title="<?php echo $extra_narrration_trainer; ?>"><i style="font-size: 17px;color: black;" class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Please hover for narration.</a> -->
										</div>
								</div>
								<div class="form-group">
									<h3  style="color: black;font-weight: bold;text-align: center;" >New Trainer</h3>
								<hr style="width: 165px;">
									<div style="padding-top: 20px;" class="row" >
										<label class="col-sm-2 control-label" for="input-trainer_code"><b style="color: red"></b><?php echo "Trainer Code:"; ?></label>
										<div class="col-sm-3">
											<input type="text" name="trainer_codes_id" value="<?php echo $new_trainer_code; ?>" placeholder="<?php echo "Trainer Code"; ?>" id="trainer_codes_id" class="form-control"/>
										</div>
										<label class="col-sm-2 control-label" for="input-new_trainer_name"><b style="color: red"></b><?php echo "Trainer Name:"; ?></label>
										<div class="col-sm-3">
											<input type="text" name="trainer_name"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $new_trainer_name; ?>" id="input-trainer_name" class="form-control" />
											<?php if (isset($valierr_trainer_name)) { ?><span class="errors"><?php echo $valierr_trainer_name; ?></span><?php } ?>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-date_of_charge"><b style="color: red">*</b><?php echo "Date Of Charge:"; ?></label>
									<div class="col-sm-3">
										<div class="input-group date input-date_charge_trainer">
											<input type="text" name="date_charge_trainer" value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_charge_trainer" class="form-control" />
											<span class="input-group-btn">
												<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
										<span style="display: none;color: red;font-weight: bold;" id="error_date_charge_trainer" class="error"><?php echo 'Please Select Valid Date'; ?></span>
										<?php if (isset($valierr_date_charge_trainer)) { ?><span class="errors" id="error_date_charge_trainer_post"><?php echo $valierr_date_charge_trainer; ?></span><?php } ?>
									</div>
									<label class="col-sm-2 control-label" for="input-trainer_name"><b style="color: red">*</b><?php echo "Arrival Time:"; ?></label>
									<div class="col-sm-3">
										<select name="arrival_time_trainers" id="arrival_time_trainer" class="form-control">
											<?php foreach ($arrival_time_trainer as $arrival_time_value) { ?>
												<?php if ($arrival_time_value == $arrival_time_trainers) { ?> 
													<option value="<?php echo $arrival_time_value; ?>"  selected="selected" ><?php echo $arrival_time_value; ?></option>
										    	<?php } else { ?> 
													<option value="<?php echo $arrival_time_value; ?>"><?php echo $arrival_time_value ?></option>
											 	<?php } ?> 
											<?php } ?>
									 	</select>
										<?php if (isset($valierr_arrival_time_trainer)) { ?><span class="errors"><?php echo $valierr_arrival_time_trainer; ?></span><?php } ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Extra Narration:"; ?></label>
									<div class="col-sm-8">
										<textarea rows="4" name="extra_narrration_trainer"  id ="extra_narrration_trainer" placeholder=""  class="form-control"></textarea>
										<?php if (isset($valierr_extra_narrration_trainer)) { ?><span class="errors"><?php echo $valierr_extra_narrration_trainer; ?></span><?php } ?>
									</div>
								</div>
								<div class="form-group">
									
					                <label class="col-sm-2 control-label provisional_label_class" for="input-top" ><?php echo "Provisional:"; ?></label>
									<div class="col-sm-3 provisional_checkbx_class">
										<?php if($provisional == '1') { ?>
										  	<div class="checkbox" >
												<label>
													<input type="hidden" name="provisional" id="provisional" value="false">
												  	<input type="checkbox"  name="provisionals" value="1"  id="provisionals" onclick="prov()" checked="checked" class="form-control" tabindex="5" />
												</label>
											</div>
										<?php } else { ?>
											<div class="checkbox" >
												<label>
													<input type="hidden" name="provisional" id="provisional" value="">
												  	<input type="checkbox" name="provisionals" value="1"  id="provisionals" onclick="prov()" class="form-control" tabindex="5"/>
												</label>
											</div>
										<?php } ?>
									</div>
									<label style="display: none;" class="col-sm-2 control-label undertaking_charge_label_class" for="input-top" ><?php echo "Undertaking Charge:"; ?></label>
										<div style="display: none;" class="col-sm-3 undertaking_charge_checkbx_class" >
											<?php if($undertaking_charge == '1') { ?>
											  	<div class="checkbox" >
													<label>
													  	<input type="checkbox"  name="undertaking_charge" value="1"  id="undertaking_charge"  checked="checked" class="form-control" tabindex="5" />
													</label>
												</div>
											<?php } else { ?>
												<div class="checkbox" >
													<label>
													  	<input type="checkbox" name="undertaking_charge" value=""  id="undertaking_charge" class="form-control" tabindex="5"/>
													</label>
												</div>
											<?php } ?>
											<?php if (isset($valierr_undertaking_Charge)) { ?><span class="errors"><?php echo $valierr_undertaking_Charge; ?></span><?php } ?>
										</div>
					            </div>

								<?php if(($horse_id != '' ) && $date_charge_trainer != '' &&  $date_charge_trainer != '0000-00-00' ) { ?> 
									<div style="display: none;" class="form-group">
										<label class="col-sm-2 control-label" for="input-horse"><?php echo "Left Charge:"; ?></label>
										<div class="col-sm-3">
											<div class="input-group date input-date_left_trainer">
												<input type="text" name="date_left_trainer" value="<?php echo $date_left_trainer; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_left_trainer" class="form-control" />
												<span class="input-group-btn">
													<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<span style="display: none;color: red;font-weight: bold;" id="error_date_left_trainer" class="error"><?php echo 'Please Select Valid Date'; ?></span>
											<?php if (isset($valierr_date_left_trainer)) { ?><span class="errors" id="error_date_left_trainer_post"><?php echo $valierr_date_left_trainer; ?></span><?php } ?>
										</div>
										<!-- <label class="col-sm-2 control-label undertaking_charge_label_class" for="input-top" ><?php echo "Undertaking Charge:"; ?></label>
										<div class="col-sm-3 undertaking_charge_checkbx_class" >
											<?php if($undertaking_charge == '1') { ?>
											  	<div class="checkbox" >
													<label>
													  	<input type="checkbox"  name="undertaking_charge" value="1"  id="undertaking_charge"  checked="checked" class="form-control" tabindex="5" />
													</label>
												</div>
											<?php } else { ?>
												<div class="checkbox" >
													<label>
													  	<input type="checkbox" name="undertaking_charge" value="1"  id="undertaking_charge" class="form-control" tabindex="5"/>
													</label>
												</div>
											<?php } ?>
											<?php if (isset($valierr_undertaking_Charge)) { ?><span class="errors"><?php echo $valierr_undertaking_Charge; ?></span><?php } ?>
										</div> -->
									</div>
									<div style="display: none;" class="form-group">
										<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Extra Narration 2:"; ?></label>
										<div class="col-sm-8">
											<textarea rows="4" name="extra_narrration_two_trainer"  id = "extra_narrration_two_trainer" placeholder=""  class="form-control"><?php echo $extra_narrration_two_trainer; ?></textarea>
											<?php if (isset($valierr_extra_narrration_two_trainer)) { ?><span class="errors"><?php echo $valierr_extra_narrration_two_trainer; ?></span><?php } ?>
										</div>
									</div>

									</form>
									
								<?php } ?>
								<?php if($horse_id != '' ) { ?>
									<div class="form-group">
										<div style="margin-left: 50%">
											<button type="button" class="btn btn-primary" id="trainer_charge" onclick="TrainerCharge()">Save</button>
										</div>
									</div>
								<?php } ?>
							<div class="col-sm-11">
								<h4>Trainer History</h4>
							</div>
							<div class="col-sm-1">
								<div id="myModaltrainer" class="modal fade" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Edit / Change Trainer Details</h4>
											</div>
											<div class="modal-body">
												<div class="form-group">
												<label class="col-sm-2 control-label" for="input-club"><?php echo "Date Of Charge:"; ?></label>
							                    	<div class="col-sm-4">
							                            <div class="input-group date">
															<input type="text"  value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="u_date_of_charge" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
							                    	</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Left Charge:"; ?></label>
													<div class="col-sm-4">
														<div class="input-group date">
															<input type="text"  value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="u_left_date" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
													</div>
												</div>

												 <div class="form-group">
													<label class="col-sm-2 control-label" for="input-full_form">Extra Narration</label>
													<div class="col-sm-10">
														<input type="text" name="u_extra_narration"  placeholder="Extra Narration" id="u_extra_narration" class="form-control" />
														<input type="hidden" name="u_horse_trainer_id" id="u_horse_trainer_id" class="form-control" />
													</div>
												</div>
											</div>
										  	<div class="modal-footer">
												<button type="button" class="btn btn-primary"  id="UpdateTrainerfunc_id" onclick="UpdateTrainerfunc()"  >Update</button>
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  	</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" >
								<table id="tbcharge" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-center"><?php echo "Trainer Name"; ?></td>
										  <td class="text-center"><?php echo "Date Of Charge"; ?></td>
										  <td class="text-center"><?php echo "Left Charge"; ?></td>
										  <td class="text-center"><?php echo "Extra Narration"; ?></td>
										  <td class="text-center"><?php echo "Action"; ?></td>
										</tr>
									</thead>
									<tbody>

										<?php if($trainer_info_all) { //echo'<pre>';print_r($trainer_info_all[0]);exit; ?>
											<?php foreach($trainer_info_all as $trainerkey => $trainervalue) {  
												if($trainervalue['date_of_charge'] == '' || $trainervalue['date_of_charge'] == '0000-00-00'){
													$date_charge_trainer = ''; 
												} else {
													$date_charge_trainer = date('d-m-Y', strtotime($trainervalue['date_of_charge']));
												} 
												if($trainervalue['left_date_of_charge'] == '' || $trainervalue['left_date_of_charge'] == '0000-00-00'){
													$left_charge_trainer = '';
												} else {
													$left_charge_trainer = date('d-m-Y', strtotime($trainervalue['left_date_of_charge']));
												} ?>
												<tr>
													<td class="text-center"><?php echo $trainervalue['trainer_name']; ?></td>
				                                    <td class="text-center"><?php echo $date_charge_trainer; ?></td>
				                                    <td class="text-center"><?php echo $left_charge_trainer; ?></td>
				                                    <td class="text-center"><?php echo $trainervalue['extra_narration'] ?></td>
				                                    <td>
				                                    	<?php if($trainerkey == 0) { ?>
				                                    		<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModaltrainer" onclick='updatetrainer(<?php echo $trainervalue['horse_to_trainer_id'] ?>);'><i class="fa fa-pencil"></i></a>
				                                    	<?php } ?>
				                                    </td>
				                                    <input type= "hidden"  name="trainer_info_history[<?php echo $trainerkey ?>][trainer_name]" value="<?php echo $trainervalue['trainer_name']; ?>">
				                                    <!-- <input type= "hidden"  name="trainer_info_history[<?php echo $trainerkey ?>][date_of_charge]" value="<?php echo $date_charge_trainer; ?>"> -->
				                                    <input type= "hidden"  name="trainer_info_history[<?php echo $trainerkey ?>][left_date_of_charge]" value="<?php echo $left_charge_trainer; ?>">
												</tr>
											<?php }  ?>
                   	 					<?php } else { ?>
		                                <tr>
		                                    <td class="text-center" colspan="5">No Result Found</td>
		                                </tr>
		                                <?php } ?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="tab-ownership">
							<div class="form-group">
								
		  						<div style= "display: flex;justify-content: center;">
		  						<a  href="<?php echo $change_owner_link; ?>"style="background-color: #00628B;border-color: #00628B;" type="button" class="btn btn-danger">Change Owner</a>
		  						</div>
			  					
								<div class="table-responsive">
									<h4> <b>Current Ownership :</b> </h4>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
											  <td class="text-center;" style="vertical-align: middle;"><?php echo "Name"; ?></td>
											  <td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Period"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Share"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Color"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Contingency"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Ownership"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Cancel"; ?></td>
											</tr>
										</thead>
										<tbody id = "ownershipdeatilsbody">
											<?php if($owners_olddatas) { //echo'<pre>';print_r($owners_olddatas); exit(); ?>
												<?php foreach($owners_olddatas as $okey => $result) { ?>
													<tr>
														<td style="text-align: left;">
															<?php if($result['partner_status'] == 1 ) { ?>
																<span style="cursor:pointer"><a id="partners_<?php echo $result['to_owner_id'] ?>"  class="partners" ><?php echo $result['to_owner'] ?></a></span><br>
															<?php } else { ?>
																<span><?php echo $result['to_owner'] ?></span><br>
															<?php } ?>

														</td>

														<td style="text-align: center;">
															<?php
																$provisional_ownership  = ($result['provisional_ownership'] == 'Yes') ? 'Provisional Ownership' : "";

															if($result['ownership_type'] == 'Lease'){ ?>
																<span style="font-size:12px";><?php echo  $provisional_ownership ?> L </span>
															<?php } else if($result['ownership_type'] == 'Sub Lease'){ ?>
																<span style="font-size:12px";> <?php echo  $provisional_ownership ?> SL </span>
															<?php } else {	?>
																<span style="font-size:12px";> <?php echo  $provisional_ownership ?> O </span>
															<?php } ?>
														</td>

														<td style="text-align: left;">
															<?php
																$from_date = date('d-m-Y', strtotime($result['date_of_ownership']));
																//$to_date = date('d-m-Y', strtotime($result['end_date_of_ownership']));
																$to_date  = ($result['end_date_of_ownership'] != '0000-00-00' && $result['end_date_of_ownership'] != '') ? date('d-m-Y',strtotime($result['end_date_of_ownership'])) : "";

															if($result['ownership_type'] == 'Lease'){ ?>
																<span style="font-size:12px";>  <?php echo  $from_date. '- ' .$to_date ?> </span>
															<?php } else if($result['ownership_type'] == 'Sub Lease'){ ?>
																<span style="font-size:12px";>  <?php echo  $from_date. '- ' .$to_date ?> </span>
															<?php } else {	?>
																<!-- <span style="font-size:12px";>  <?php echo $from_date ?> </span> -->
																<?php if($to_date != '') { ?>
																	<span style="font-size:12px";>  <?php echo  $from_date. ' - ' .$to_date ?> </span>
																<?php } else {	?>
																	<span style="font-size:12px";>  <?php echo  $from_date ?> </span>
																<?php } ?>

															<?php } ?>
														</td>



														<td class="r_<?php echo $results ?>" style="text-align: right;">
															<span><?php echo $result['owner_percentage'].'%' ?></span>
															
														</td>
														
														<td class="text-left" style="text-align: center;width: 10%;" >
															<?php if($result['owner_color'] == 'Yes' && $result['last_color'] == $result['horse_to_owner_id']) { ?>
																<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>
															<?php } elseif($result['owner_color'] == 'join') { ?>
																<i class="fa fa-link" aria-hidden="true" style="color: green;font-size:1.4em;"></i>
															<?php } ?>
														</td>

														<td style="text-align: center;">
															<?php 
																$contengency = ($result['contengency'] == 1) ? "Yes" : "N/A";
																$cont_percentage = ($result['cont_percentage'] != 0) ? $result['cont_percentage'] : "";
																$cont_amount = ($result['cont_amount'] != 0) ? $result['cont_amount'] : "0";
																$win_gross_stake = ($result['win_gross_stake'] == 1) ? "Yes" : "N/A";
																$win_net_stake = ($result['win_net_stake'] == 1) ? "Yes" : "N/A";
																$cont_place = ($result['cont_place'] == 1) ? "Yes" : "N/A";
																$millionover = ($result['millionover'] == 1) ? "Yes" : "N/A";
																$millionover_amt = ($result['millionover_amt'] != 0) ? $result['millionover_amt'] : "0";
																$grade1 = ($result['grade1'] == 1) ? "Yes" : "N/A";
																$grade2 = ($result['grade2'] == 1) ? "Yes" : "N/A";
																$grade3 = ($result['grade3'] == 1) ? "Yes" : "N/A";
															?>
															
					                                        <span><a class="modalopen" id="details_<?php echo $result['horse_to_owner_id']?>" style="cursor: pointer;">
					                                        <?php if($contengency == 'Yes'){
					                                         		echo $cont_percentage.'% / Rs.'.$cont_amount ?>
					                                         	</a></span><br>
					                                        <?php } ?>
					                                        
					                                        
					                                    
					                                    </td>
					                                    <td style="text-align: left;">
					                                    	<?php if(isset($result['parent_owner_name']) && $result['ownership_type'] != 'Sale') {
					                                    		if($result['ownership_type'] == 'Lease'){ ?>
						                                    		<span><?php echo $result['parent_owner_name']  ?> ( In the case of Lease )</span>
						                                    	<?php } else { ?>
						                                    		<span><?php echo $result['sub_parent_owner_name']  ?> ( In the case of Lease )</span> => <span><?php echo $result['parent_owner_name']  ?> ( In the case of Sub-Lease )</span>
						                                    		<!-- <span><?php echo $result['parent_owner_name']  ?> ( In the case of Sub-Lease )</span> -->
						                                    	<?php }  ?>
															<?php }  ?>
					                                    </td>
														
					                                    <td style="text-align: left;">
															<?php if(($result['cancel_lease_status'] != '') && ($result['cancel_contigencey'] == '')){ ?>
																<span><?php echo $result['ownership_type'].' '.$result['cancel_lease_status']  ?> </span>
															<?php } else if(($result['cancel_contigencey'] != '')&& ($result['cancel_lease_status'] == '')) { ?>
																<span><?php echo $result['cancel_contigencey'];  ?> </span>
															<?php } else if(($result['cancel_lease_status'] != '') && ($result['cancel_contigencey'] != '')) {  ?>
																<span><?php echo $result['ownership_type'].' '.$result['cancel_lease_status'].','.' '.$result['cancel_contigencey'] ?> </span>
															<?php }  ?>
														</td>
													</tr>
												<?php } ?>
                   	 						<?php } ?>
										</tbody>
									</table>
								</div>
							</div>

							<div class="form-group">
								<div class="table-responsive">
									<h4> <b>Provisional Ownership :</b> </h4>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
											  <td class="text-center;" style="vertical-align: middle;"><?php echo "Name"; ?></td>
											  <td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Period"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Share"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Color"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Contingency"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Ownership"; ?></td>
											</tr>
										</thead>
										<tbody id = "ownershipdeatilsbody">
											<?php if($owners_provisional) { //echo'<pre>';print_r($owners_provisional); exit(); ?>
												<?php foreach($owners_provisional as $okey => $result) { ?>
													<tr>
														<td style="text-align: left;">
															<?php if($result['partner_status'] == 1 ) { ?>
																<span style="cursor:pointer"><a id="partners_<?php echo $result['to_owner_id'] ?>"  class="partners" ><?php echo $result['to_owner'] ?></a></span><br>
															<?php } else { ?>
																<span><?php echo $result['to_owner'] ?></span><br>
															<?php } ?>

														</td>

														<td style="text-align: center;">
															<?php
																$provisional_ownership  = ($result['provisional_ownership'] == 'Yes') ? 'Provisional Ownership' : "";

															if($result['ownership_type'] == 'Lease'){ ?>
																<span style="font-size:12px";><?php echo  $provisional_ownership ?> L </span>
															<?php } else if($result['ownership_type'] == 'Sub Lease'){ ?>
																<span style="font-size:12px";> <?php echo  $provisional_ownership ?> SL </span>
															<?php } else {	?>
																<span style="font-size:12px";> <?php echo  $provisional_ownership ?> O </span>
															<?php } ?>
														</td>

														<td style="text-align: left;">
															<?php
																$from_date = date('d-m-Y', strtotime($result['date_of_ownership']));
																if ($result['end_date_of_ownership'] != '0000-00-00') {
																	$to_date = date('d-m-Y', strtotime($result['end_date_of_ownership']));
																} else{
																	$to_date = '0000-00-00';
																}

															if($result['ownership_type'] == 'Lease'){ ?>
																<span style="font-size:12px";>  <?php echo  $from_date. '- ' .$to_date ?> </span>
															<?php } else if($result['ownership_type'] == 'Sub Lease'){ ?>
																<span style="font-size:12px";>  <?php echo  $from_date. '- ' .$to_date ?> </span>
															<?php } else {	?>
																<span style="font-size:12px";>  <?php echo $from_date. ' - ' .$to_date ?> </span>

															<?php } ?>
														</td>



														<td class="r_<?php echo $results ?>" style="text-align: right;">
															<span><?php echo $result['owner_percentage'].'%' ?></span>
															
														</td>
														
														<td class="text-left" style="text-align: center;width: 10%;" >
															<?php if($result['owner_color'] == 'Yes' && $result['last_color'] == $result['horse_to_owner_id']) { ?>
																<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>
															<?php } ?>
														</td>

														<td style="text-align: center;">
															<?php 
																$contengency = ($result['contengency'] == 1) ? "Yes" : "N/A";
																$cont_percentage = ($result['cont_percentage'] != 0) ? $result['cont_percentage'] : "";
																$cont_amount = ($result['cont_amount'] != 0) ? $result['cont_amount'] : "0";
																$win_gross_stake = ($result['win_gross_stake'] == 1) ? "Yes" : "N/A";
																$win_net_stake = ($result['win_net_stake'] == 1) ? "Yes" : "N/A";
																$cont_place = ($result['cont_place'] == 1) ? "Yes" : "N/A";
																$millionover = ($result['millionover'] == 1) ? "Yes" : "N/A";
																$millionover_amt = ($result['millionover_amt'] != 0) ? $result['millionover_amt'] : "0";
																$grade1 = ($result['grade1'] == 1) ? "Yes" : "N/A";
																$grade2 = ($result['grade2'] == 1) ? "Yes" : "N/A";
																$grade3 = ($result['grade3'] == 1) ? "Yes" : "N/A";
															?>
															
					                                        <span><a class="modalopen" id="details_<?php echo $result['horse_to_owner_id']?>" style="cursor: pointer;">
					                                        <?php if($contengency == 'Yes'){
					                                         		echo $cont_percentage.'% / Rs.'.$cont_amount ?>
					                                         	</a></span><br>
					                                        <?php } ?>
					                                        
					                                        
					                                    
					                                    </td>
					                                    <td style="text-align: left;">
					                                    	<?php if(isset($result['parent_owner_name']) && $result['ownership_type'] != 'Sale') {
					                                    		if($result['ownership_type'] == 'Lease'){ ?>
						                                    		<span><?php echo $result['parent_owner_name']  ?> ( In the case of Lease )</span>
						                                    	<?php } else { ?>
						                                    		<span><?php echo $result['sub_parent_owner_name']  ?> ( In the case of Lease )</span> => <span><?php echo $result['parent_owner_name']  ?> ( In the case of Sub-Lease )</span>
						                                    		<!-- <span><?php echo $result['parent_owner_name']  ?> ( In the case of Sub-Lease )</span> -->
						                                    	<?php }  ?>
															<?php }  ?>
					                                    </td>
								
													</tr>
												<?php } ?>
                   	 						<?php } ?>
										</tbody>
									</table>
								</div>
							</div>

							<div class="form-group">
								<div class="table-responsive">
									<h4> <b> Ownership History :</b> </h4>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Name"; ?></td>
											  <td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Period"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Share"; ?></td>
											  <td class="text-center" style="display: none; vertical-align: middle;"><?php echo "Color"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Contingency"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Ownership"; ?></td>
											  <td class="text-center" style="vertical-align: middle;"><?php echo "Cancel"; ?></td>
											</tr>
										</thead>
										<tbody id = "ownershipdeatilsbody">
											<?php if($ownerhistory) { //echo'<pre>';print_r($ownerhistory); exit();
											function random_color_part() {
											   // return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
											     return str_pad( dechex( mt_rand( 0, 127 ) ), 2, '0', STR_PAD_LEFT);
											}

											function random_color() {
											    return random_color_part() . random_color_part() . random_color_part();
											}

											 ?>
												<?php 
												$colors = '';
												$batch_id = 0;
												foreach($ownerhistory as $okey => $result) { 
													if($result['batch_id'] != $batch_id){
														$color = '#'.random_color(); ?>
														<tr style="background-color: <?php echo $color; ?>">
															<td  colspan="7"></td>
														</tr>

														
													<?php } 
													$batch_id = $result['batch_id'];
													
													?>
														
													<tr >
														<td style="text-align: left;">
															<?php if($result['partner_status'] == 1 ) { ?>
																<span style="cursor:pointer;"><a id="partners_<?php echo $result['to_owner_id'] ?>"  class="partners" ><?php echo $result['to_owner'] ?></a></span><br>
															<?php } else { ?>
																<span ><?php echo $result['to_owner'] ?></span><br>
															<?php } ?>
														</td>

														<td style="text-align: center;">
															
															<?php
																
															$provisional_ownership  = ($result['provisional_ownership'] == 'Yes') ? 'Provisional Ownership' : "";

															if($result['ownership_type'] == 'Lease'){ ?>
																<span style="font-size:12px";>  <?php echo  $provisional_ownership." L"  ?> </span>
															<?php } else if($result['ownership_type'] == 'Sub Lease'){ ?>
																<span style="font-size:12px";>  <?php echo  $provisional_ownership." SL"  ?> </span>
															<?php } else {	?>
																<span style="font-size:12px";>  <?php echo  $provisional_ownership." O"  ?> </span>
															<?php } ?>
														</td>

														<td style="text-align: left;">
															
															<?php
																$from_date = date('d-m-Y', strtotime($result['date_of_ownership']));
																//$to_date = date('d-m-Y', strtotime($result['end_date_of_ownership']));
																$to_date  = ($result['end_date_of_ownership'] != '0000-00-00' && $result['end_date_of_ownership'] != '') ? date('d-m-Y',strtotime($result['end_date_of_ownership'])) : "";

															if($result['ownership_type'] == 'Lease'){ ?>
																<span style="font-size:12px";>  <?php echo $from_date. ' - ' .$to_date  ?> </span>
															<?php } else if($result['ownership_type'] == 'Sub Lease'){ ?>
																<span style="font-size:12px";>  <?php echo  $from_date. ' - ' .$to_date  ?> </span>
															<?php } else {	?>
																<!-- <span style="font-size:12px";>  <?php echo  $from_date. ' - ' .$to_date ?> </span> -->
																<?php if($to_date != '') { ?>
																	<span style="font-size:12px";>  <?php echo  $from_date. ' - ' .$to_date ?> </span>
																<?php } else {	?>
																	<span style="font-size:12px";>  <?php echo  $from_date ?> </span>
																<?php } ?>

															<?php } ?>
														</td>

	
				

														<td class="r_<?php echo $results ?>" style="text-align: right;">
															<span><?php echo $result['owner_percentage'].'%' ?></span>
															
														</td>
														
														<td style="display: none;" class="text-left" style="text-align: center;width: 10%;" >
															<?php if($result['owner_color'] == 'Yes') { ?>
																<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>
															<?php } ?>
														</td>

														<td style="text-align: center;">
															<?php 
																$contengency = ($result['contengency'] == 1) ? "Yes" : "N/A";
																$cont_percentage = ($result['cont_percentage'] != 0) ? $result['cont_percentage'] : "";
																$cont_amount = ($result['cont_amount'] != 0) ? $result['cont_amount'] : "0";
															?>
															
					                                        <span><a class="modalopen" id="details_<?php echo $result['horse_to_owner_id']?>" style="cursor: pointer;">
					                                        <?php if($contengency == 'Yes'){
					                                         		echo $cont_percentage.'% / Rs.'.$cont_amount ;
					                                         	?>
					                                        	
					                                         	</a></span><br>
					                                        <?php  } ?>
					                                    </td>
					                                    <td style="text-align: left;">
					                                    	<?php if(isset($result['parent_owner_name']) && $result['ownership_type'] != 'Sale') {
					                                    		if($result['ownership_type'] == 'Lease'){ ?>
						                                    		<span><?php echo $result['parent_owner_name']  ?> ( In the case of Lease )</span>
						                                    	<?php } else { ?>
						                                    		<span><?php echo $result['parent_owner_name']  ?> ( In the case of Sub-Lease )</span>
						                                    	<?php }  ?>
															<?php }  ?>
					                                    </td>
					                                    <td style="text-align: left;">
															<?php if(($result['cancel_lease_status'] != '') && ($result['cancel_contigencey'] == '')){ ?>
																<span><?php echo $result['ownership_type'].' '.$result['cancel_lease_status']  ?> </span>
															<?php } else if(($result['cancel_contigencey'] != '')&& ($result['cancel_lease_status'] == '')) { ?>
																<span><?php echo $result['cancel_contigencey'];  ?> </span>
															<?php } else if(($result['cancel_lease_status'] != '') && ($result['cancel_contigencey'] != '')) {  ?>
																<span><?php echo $result['ownership_type'].' '.$result['cancel_lease_status'].','.' '.$result['cancel_contigencey'] ?> </span>
															<?php }  ?>
														</td>
								
													</tr>
												<?php } ?>
                   	 						<?php } ?>
										</tbody>
									</table>
								</div>
							</div>

							<div class="modal fade" id="contModal" role="dialog">
						        <div class="modal-dialog">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button type="button" class="close" data-dismiss="modal">&times;</button>
						                    <h4 class="modal-title">Contingency Details</h4>
						                </div>
						                <div class="modal-body" style="height: 240px;">
						                    
						                </div>
						                <div class="modal-footer">
						                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						                </div>
						            </div>
						        </div>
						    </div>

						    <div class="mod" >
                       
                    		</div>

						</div> <!-- closed-ownership -->
						<div class="tab-pane" id="tab-BanDetails">
							<div class="col-sm-11">
								<h4>Ban Details</h4>
							</div>
							<div style="float: right;padding-bottom: 10px;">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"  onclick="closeaddbuu1()"><i class="fa fa-plus-circle"></i></button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="id_hidden_band" value="<?php echo $id_hidden_band ?>" id="id_hidden_band" class="form-control" />

								<div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Ban</h4>
											</div>
											<div class="modal-body">
											<form id="ban_data">
												<div class="form-group">
												<label class="col-sm-2 control-label" for="input-club"><b style="color: red">*</b><?php echo "Club:"; ?></label>
													<div class="col-sm-8">
														<select  name="club" placeholder="Club" id="input-club" class="form-control">
															<option value="" selected="selected" disabled="disabled" >Please Select</option>
															<?php foreach ($clubs as $ckey => $cvalue) { ?>
															<option value="<?php echo $cvalue; ?>" ><?php echo $cvalue ?></option>
															<?php } ?>
													  </select>
													  <span style="display: none;color: red;font-weight: bold;" id="error_club_ban" class="error"><?php echo 'Please Select Club'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Authority:"; ?></label>
													<div class="col-sm-8">
														<select name="authority_ban" id="authority_ban_details" class="form-control">
															<option value="" selected="selected" disabled="disabled" >Please Select</option>	
															<?php foreach ($authorty_bans as $authorutykey => $authorutyvalue) { ?>
															<option value="<?php echo $authorutyvalue; ?>" ><?php echo $authorutyvalue ?></option>
															<?php } ?>
													  </select>
													  <span style="display: none;color: red;font-weight: bold;" id="error_authority_ban" class="error"><?php echo 'Please Select Authority'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Start Date:"; ?></label>
													<div class="col-sm-8">
														<div class="input-group date input-start_date_ban">
															<input type="text" name="date_start_date_ban" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="start_date_ban" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_date_start_date_ban" class="error"><?php echo 'Please Select Valid Date'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><?php echo "End Date:"; ?></label>
													<div class="col-sm-8">
														<div class="input-group date input-end_date_ban">
															<input type="text" name="date_end_date_ban" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="end_date_ban" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_date_end_date_ban" class="error"><?php echo 'Please Select Valid Date'; ?></span>
														<span style="display: none;color: red;font-weight: bold;" id="error_greater_start_date" class="error"><?php echo 'End Date must not be less than Start Date!'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Reason:"; ?></label>
													<div class="col-sm-8">
														<textarea rows="2" name="reason_description_ban" placeholder="" id="reason_ban" class="form-control"></textarea>
													  	<span style="display: none;color: red;font-weight: bold;" id="error_reason_ban" class="error"><?php echo 'Please Enter Reason'; ?></span>
													</div>
												</div>
											</form>
											</div>
										  <div class="modal-footer">
											<input type="hidden" name="id_hidden_BanFunction" value="" id="id_hidden_BanFunction" class="form-control" />
											<input type="hidden" name="idban_increment_id" value="" id="idban_increment_id" class="form-control" />
											<input type="hidden" name="hidden_horse_id" value="" id="hidden_horse_id" class="form-control" />
											<input type="hidden" name="hidden_horse_ban_id" value="" id="hidden_horse_ban_id" class="form-control" />
											<button type="button" class="btn btn-primary" id = "ban_save_id" onclick="BanFunction()"  >Save</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  </div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">

									<thead>
										<tr>
										  <td class="text-left"><?php echo "Club"; ?></td>
										  <td class="text-left"><?php echo "Start Date"; ?></td>
										  <td class="text-left"><?php echo "End Date"; ?></td>
										  <td class="text-left"><?php echo "Authority"; ?></td>
										  <td class="text-left"><?php echo "Reason"; ?></td>
										  <td class="text-left"><?php echo "Action"; ?></td>
										</tr>
									</thead>

									<tbody id="bandeatilsbody">
										<?php if($bandeatils) { //echo "<pre>";print_r($bandeatils);exit; ?>
											<?php foreach($bandeatils as $bankey => $banvalue) { 
												if($banvalue['startdate_ban'] == '' || $banvalue['startdate_ban'] == '0000-00-00'){
													$startdatebans = ''; 
												} else {
													$startdatebans = date('d-m-Y', strtotime($banvalue['startdate_ban']));
												} 
												if($banvalue['enddate_ban'] == '' || $banvalue['enddate_ban'] == '0000-00-00'){
													$enddatebans = '';
												} else {
													$enddatebans = date('d-m-Y', strtotime($banvalue['enddate_ban']));
												}
											?>
												<tr id='bandetail_<?php echo $bankey ?>'>
													<td><span id="clubs_<?php echo $bankey ?>" ><?php echo $banvalue['club_ban'] ?></span>
														<input type= "hidden"  name="bandats[<?php echo $bankey ?>][club_ban]" id="club_<?php echo $bankey ?>"  value="<?php echo $banvalue['club_ban'] ?>">
													</td>
													<td><span id="start_dateban_<?php echo $bankey ?>"><?php echo $startdatebans; ?></span>
														<input type= "hidden"  name="bandats[<?php echo $bankey ?>][startdate_ban]" id="start_date_bans_<?php echo $bankey ?>"  value="<?php echo $startdatebans; ?>">
													</td>
													<td><span id="end_dateban_<?php echo $bankey ?>"><?php echo $enddatebans; ?></span>
														<input type= "hidden"  name="bandats[<?php echo $bankey ?>][enddate_ban]" id="end_date_ban_<?php echo $bankey ?>"  value="<?php echo $enddatebans; ?>">
													</td>
													<td><span id="authorityban_<?php echo $bankey ?>" ><?php echo $banvalue['authority'] ?></span>
														<input type= "hidden"  name="bandats[<?php echo $bankey ?>][authority]" id="authority_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['authority'] ?>">
													</td>
													<td><span  id="reasonban_<?php echo $bankey ?>"><?php echo $banvalue['reason_ban'] ?></span>
														<input type= "hidden"  name="bandats[<?php echo $bankey ?>][reason_ban]" id="reason_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['reason_ban'] ?>">
													</td>
													<td> 
														<a onclick='updateban(<?php echo $bankey ?>);' class="btn btn-primary"><i class="fa fa-pencil"></i></a>
														<!-- <a onclick="removeBanDetail(<?php echo $banvalue['horse_id'] ?>,<?php echo $banvalue['horse_ban_id'] ?>,'bandetail_<?php echo $bankey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a> -->
													</td>
													<input type= "hidden"  name="bandats[<?php echo $bankey ?>][horse_ban_id]"  id= "hidden_increment_ban_id_<?php echo $bankey ?>"  value="<?php echo $banvalue['horse_ban_id'] ?>">
													<input type= "hidden"  name= "bandats[<?php echo $bankey ?>][horse_id]"  value ="<?php echo $banvalue['horse_id']?>" >
												</tr>
											<?php }  ?>
                   	 					<?php } ?>
									</tbody>
								</table>
							</div>
							<div class="col-sm-11">
								<h4>Ban History</h4>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-left" ><?php echo "Club"; ?></td>
										  <td class="text-left" ><?php echo "Start Date"; ?></td>
										  <td class="text-left" ><?php echo "End Date"; ?></td>
										  <td class="text-left" ><?php echo "Authority"; ?></td>
										  <td class="text-left" ><?php echo "Reason"; ?></td>
										   <td class="text-left" ><?php echo "Action"; ?></td>
										</tr>
									</thead>
									<tbody id="banhistorybody">
										<?php if($bandatas) { //echo'<pre>';print_r($bandatas); exit(); ?>
											<?php foreach($bandatas as $bkey => $bvalue) { ?>
												<tr id='banhistorybody<?php echo $bkey ?>'>
													<td class="text-left" >	
														<?php echo $bvalue['club_ban'] ?>
														<input type= "hidden" name="banhistorydatas[<?php echo $bkey ?>][club_ban]" id="history_club_<?php echo $bkey ?>"  value="<?php echo $bvalue['club_ban'] ?>" >
													</td>
													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($bvalue['startdate_ban'])); ?>
														<input type= "hidden" name="banhistorydatas[<?php echo $bkey ?>][startdate_ban]" id="history_startdate_ban_<?php echo $bkey ?>"  value="<?php echo date('d-m-Y', strtotime($bvalue['startdate_ban'])); ?>" >
													</td>
													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($bvalue['enddate_ban'])); ?>
														<input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][enddate_ban]" id="history_enddate_ban_<?php echo $bkey ?>"  value="<?php echo date('d-m-Y', strtotime($bvalue['enddate_ban'])); ?>" >
													</td>
													<td class="text-left" >
														<?php echo $bvalue['authority']; ?>
														<input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][authority]" id="history_authority_<?php echo $bkey ?>"  value="<?php echo $bvalue['authority']; ?>" >
													</td>
													<td class="text-left" >
														<?php echo $bvalue['reason_ban']; ?>
														<input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][reason_ban]" id="history_reason_ban_<?php echo $bkey ?>"  value="<?php echo $bvalue['reason_ban']; ?>" >
													</td>
													<td class="text-left">
														<!-- <a onclick="removeBanDetail(<?php echo $bvalue['horse_id'] ?>,<?php echo $bvalue['horse_ban_id'] ?>,'banhistorybody<?php echo $bkey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a> -->
													</td>
													<input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][horse_ban_id]"  value="<?php echo $bvalue['horse_ban_id']; ?>">
													<input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][horse_id]" value="<?php echo $bvalue['horse_id']; ?>">
												</tr>
											<?php } ?>
										<?php } else { ?>
                      						<tr><td colspan="6" class="text-center emptyRows">No BAN To Display</td></tr>
                   	 					<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="tab-Change-Equipment">
							<div style="padding-bottom: 10px;" class="col-sm-12">
								<h4 class="pull-left">Current Equipment</h4>
								<button type="button" class="pull-right btn btn-primary" data-toggle="modal" data-target="#myModal1"  onclick="closeaddequpment()">Add Equipment</button>
							</div>
							<div class="col-sm-1">

								<div id="myModal1" class="modal fade" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<div class="row" >
													<div class="col-sm-6">
														<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
														<h4 class="modal-title">Current Equipment</h4>
													</div>
													<div class="col-sm-6 text-right">
														<button type="button" class="btn btn-primary" id ="equipemnt_save"onclick="EquipemntFunction()" >Save</button>
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
											</div>
											<div class="modal-body">
												<div class="form-group">
												<label class="col-sm-3 control-label" for="input-club"><?php echo "Equipment:"; ?></label>
													<div class="col-sm-9">
														<input type="text" name="equipment"  placeholder="<?php echo "Equipment"; ?>" id="equipment" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_equipement" class="error"><?php echo 'Please Enter Equipment'; ?></span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-sm-3 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Start Date:"; ?></label>
													<div class="col-sm-8">
														<div class="input-group date input-date_choose_date">
															<input type="text" name="change_horse_date" value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_choose_date" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_date_choose_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label" >Status</label>
													<div class="col-sm-9 radio-inline">
														<input type="radio" class="custom-control-input" id="defaultChecked" name="eqipment_checked" value="1" checked>
  														<label class="custom-control-label" for="defaultUnchecked">On</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<span style="display: none;color: red;font-weight: bold;" id="error_equipement_on_off" class="error"><?php echo 'Please select on/off'; ?></span>
													</div>
													
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label" >Reason</label>
													<div class="col-sm-9">
														<input type="text" name="equ_reason" placeholder="<?php echo "Reason"; ?>" id="equ-reason" class="form-control" />
													</div>
												</div>

												<div class="pre_equipment_assign_datas">
													
												</div>

											</div>
										  <div class="modal-footer">
											<button type="button" class="btn btn-primary" id ="equipemnt_save"onclick="EquipemntFunction()" >Save</button>
											<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 -->
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">

									<thead>
										<tr>
										  <td class="text-center col-sm-4"><?php echo "Equipment"; ?></td>
										  <td class="text-center col-sm-4"><?php echo "Date"; ?></td>
										  <td class="text-center col-sm-2"><?php echo "Status"; ?></td>
										  <td class="text-center col-sm-4"><?php echo "Action"; ?></td>

										</tr>
									</thead>
									<tbody id="equipmentdeatilsbody">

										<?php
										$auto_ids = 0;
										  if($equipment_datas) { //echo'<pre>';print_r($bandatas); exit(); ?>
											<?php foreach($equipment_datas as $eqkey => $eqvalue) { 
												$equ_status = ($eqvalue['equipment_status'] == 1) ? "On" : "Off";
												?>
												<tr id="equipment_datas_row<?php echo $eqkey ?>'">
													<td class="text-left" >
														<a class="chng_equ" id="equname_<?php echo $eqvalue['horse_equipments_id'] ?>" style="cursor: pointer;"><?php echo $eqvalue['equipment_name'] ?></a>
													</td>
													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($eqvalue['equipment_date'])); ?>
													</td>
													<td class="text-left" ><?php echo $equ_status ?></td>
													<td class="text-center" >
														<a class="chng_equ" id="chng_<?php echo $eqvalue['horse_equipments_id'] ?>" style="cursor: pointer;font-size:2em;font-weight: 700"><i  class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
													</td>
												</tr>
												
											<?php
												$auto_ids = $eqkey;
											 } ?>
                   	 					<?php } //echo'<pre>'; print_r( $auto_ids);exit; ?>
									</tbody>
									
									<input type="hidden" name="id_hidden_eqipment" value="<?php echo $auto_ids ?>" id="id_hidden_eqipment" class="form-control" />
							
								</table>
							</div>
							<div class="modal_equip">
								
							</div>
							<div class="col-sm-11">
								<h4>History of Equipment Change</h4>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-left col-sm-4"><?php echo "Equipment"; ?></td>
										  <td class="text-left col-sm-3"><?php echo "Start Date"; ?></td>
										  <td class="text-left col-sm-3"><?php echo "End Date"; ?></td>
										  <td class="text-left col-sm-3"><?php echo "Status"; ?></td>
										</tr>
									</thead>
									<tbody >
										<?php if($equipmentdatas) { //echo'<pre>';print_r($bandatas); exit(); ?>
											<?php foreach($equipmentdatas as $ekey => $evalue) { ?>
												
												<tr id="equipmentdatasrow<?php echo $ekey ?>'">
													<td class="text-left" >
														<?php echo $evalue['equipment_name'] ?>
													</td>
													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($evalue['equipment_date'])); ?>
													</td>

													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($evalue['equipment_end_date'])); ?>
													</td>
													<?php if($evalue['equipment_status'] == 0) { ?>
														<td class="text-left" ><?php echo 'Off' ?></td>
													<?php } else{ ?>
														<td class="text-left" ><?php echo 'On' ?></td>
													<?php } ?>
												</tr>
											<?php } ?>
										<?php } else { ?>
                      						<tr><td colspan="4" class="text-center emptyRows">No Equipment To Display</td></tr>
                   	 					<?php } ?> 
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="tab-Shoeing-and-bits">
							<div class="pull-left">
								<h4>Current Shoe</h4>
							</div>
							<div style="float: right;padding-bottom: 10px;">
								<button id="add_shoes" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2" onclick="closeaddshoe()">Add / Change Shoe</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="id_hidden_shoe" value="1" id="id_hidden_shoe" class="form-control" />
								<div id="myModal2" class="modal fade" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Add / Change Shoe</h4>
											</div>
											<div class="modal-body">
												<div class="form-group">
												<label class="col-sm-2 control-label" for="input-club"><?php echo "Type:"; ?></label>
				                                	<div class="col-sm-3">
				                                        <select name="type_shoe" id="select-type_shoe" class="form-control" data-index="">
				                                            <?php foreach ($Type as $key => $value) { ?>
				                                            <?php if ($key == $type_shoe) { ?>
				                                              <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
				                                            <?php } else { ?>
				                                              <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
				                                            <?php } ?>
				                                            <?php } ?>
				                                        </select>
				                                	</div>
												</div>
												
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Description:"; ?></label>
													<div class="col-sm-10">
														<input type="text" name="description_shoe"  placeholder="<?php echo "Description"; ?>" id="description_shoe" class="form-control" />
														<input type="hidden" name="description_shoe_id"   id="description_shoe_id" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_shoe_description" class="error"><?php echo 'Please Enter Description'; ?></span>
													</div>
												</div>

												 <div class="form-group">
													<label class="col-sm-2 control-label" for="input-full_form">Full Form</label>
													<div class="col-sm-10">
														<input type="text" name="full_form"  placeholder="Full Form" id="full_form" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_fullform" class="error"><?php echo 'Please Enter Full Form'; ?></span>
													</div>
												</div>


												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo " Date:"; ?></label>
													<div class="col-sm-4">
														<div class="input-group date input-date_choose_datess">
															<input type="text"  value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_choose_datess" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_date_choose_datess" class="error"><?php echo 'Please Select Valid Date'; ?></span>
													</div>
												</div>
												
											</div>
										  	<div class="modal-footer">
												<button type="button" class="btn btn-primary"  id="shoeFunction_id_1" onclick="ShoeFunction()"  >Save</button>
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  	</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="id_hidden_shoe" value="1" id="id_hidden_shoe" class="form-control" />
								<div id="myModal3" class="modal fade" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Edit / Change Shoe</h4>
											</div>
											<div class="modal-body">
												<div class="form-group">
												<label class="col-sm-2 control-label" for="input-club"><?php echo "Type:"; ?></label>
				                                	<div class="col-sm-3">
				                                        <select name="type_shoe" id="u_select-type_shoe" class="form-control" data-index="">
				                                            <?php foreach ($Type as $key => $value) { ?>
				                                            <?php if ($key == $type_shoe) { ?>
				                                              <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
				                                            <?php } else { ?>
				                                              <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
				                                            <?php } ?>
				                                            <?php } ?>
				                                        </select>
				                                	</div>
												</div>
												
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Description:"; ?></label>
													<div class="col-sm-10">
														<input type="text" name="description_shoe"  placeholder="<?php echo "Description"; ?>" id="u_description_shoe" class="form-control" />
														<input type="hidden" name="description_shoe_id"   id="u_description_shoe_id" class="form-control" />
														<input type="hidden" id="u_horse_shoeing_id" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_shoe_description" class="error"><?php echo 'Please Enter Description'; ?></span>
													</div>
												</div>

												 <div class="form-group">
													<label class="col-sm-2 control-label" for="input-full_form">Full Form</label>
													<div class="col-sm-10">
														<input type="text" name="full_form"  placeholder="Full Form" id="u_full_form" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_fullform" class="error"><?php echo 'Please Enter Full Form'; ?></span>
													</div>
												</div>


												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo " Date:"; ?></label>
													<div class="col-sm-4">
														<div class="input-group date input-date_choose_datess">
															<input type="text"  value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="u_date_choose_datess" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_date_choose_datess" class="error"><?php echo 'Please Select Valid Date'; ?></span>
													</div>
												</div>
											</div>
										  	<div class="modal-footer">
												<button type="button" class="btn btn-primary"  id="shoeFunction_id" onclick="UpdateShoeFunction()"  >Update</button>
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  	</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<td class="text-left"><?php echo "Shoe"; ?></td>
											<td class="text-left"><?php echo "Description"; ?></td>
											<td class="text-left"><?php echo "Full Form"; ?></td>
											<td class="text-left"><?php echo "Date"; ?></td>
											<td class="text-left"><?php echo "Action"; ?></td>
											
										</tr>
									</thead>
									 <tbody id = "shoedeatilsbody">
									 	<?php if($new_shoe_dats) { ?>
											<?php foreach($new_shoe_dats as $shoekey => $shoevalue) { ?>
												<tr id = 'shoeing_current<?php echo $shoekey ?>'>
													<td class="text-left"><?php echo $shoevalue['type']?></td>
													<td class="text-left"><?php echo $shoevalue['shoe_description']?></td>
													<td class="text-left"><?php echo $shoevalue['full_form']?></td>
													<td class="text-left"><?php echo date('d-m-Y', strtotime($shoevalue['shoe_start_date'])) ?></td>
													<td>
														<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal3" onclick='updateshoe(<?php echo $shoevalue['horse_shoeing_id'] ?>);'><i class="fa fa-pencil"></i></a>
													</td>
												<tr>
											<?php } ?>
               	 						<?php } ?> 
									</tbody>
								</table>
							</div>
							<div class="pull-left">
								<h4>Shoeing History</h4>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<td class="text-left"><?php echo "Shoe"; ?></td>
											<td class="text-left"><?php echo "Description"; ?></td>
											<td class="text-left"><?php echo "Full Form"; ?></td>
											<td class="text-left"><?php echo "Start Date"; ?></td>
											<td class="text-left"><?php echo "End Date"; ?></td>

											
										</tr>
									</thead>
									 <tbody>
								 		<?php if($history_shoe) { ?>
											<?php foreach($history_shoe as $historykey => $historyvalue) { ?>
												<tr id = 'shoeing_history<?php echo $historykey ?>'>
													<td class="text-left"><?php echo $historyvalue['type']?></td>
													<td class="text-left"><?php echo $historyvalue['shoe_description']?></td>
													<td class="text-left"><?php echo $historyvalue['full_form']?></td>
													<td class="text-left"><?php echo date('d-m-Y', strtotime($historyvalue['shoe_start_date'])) ?></td>

													<td class="text-left"><?php echo date('d-m-Y', strtotime($historyvalue['shoe_end_date'])) ?></td>
												<tr>
											<?php } ?>
										<?php } else { ?>
              								<tr><td colspan="8" class="text-center emptyRows">No Shoeing and Bits To Display</td></tr>
           	 							<?php } ?> 
									</tbody>
								</table>
							</div>

							<div class="pull-left">
								<h4>Current Bit</h4>
							</div>
							<div style="float: right;padding-bottom: 10px;">
								<button id="add_bit" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalBit" >Add / Change Bit</button>
							</div>
							<div id="myModalBit" class="modal fade" role="dialog">
								<div class="modal-dialog">
								<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Add / Change Bit</h4>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="bits_name"><?php echo "Description:"; ?></label>
												<div class="col-sm-10">
													<input type="text" name="bits_name"  placeholder="<?php echo "Bit"; ?>" id="bits_name" class="form-control" />
													<input type="hidden" name="bit_id"  id="bit_id" class="form-control" />
													<span style="display: none;color: red;font-weight: bold;" id="error_bit" class="error"><?php echo 'Please select Bit'; ?></span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="description_bits"><?php echo "Full Form:"; ?></label>
												<div class="col-sm-10">
													<input type="text" name="description_bits"  placeholder="<?php echo "Full Form"; ?>" id="description_bits" class="form-control" />
													<span style="display: none;color: red;font-weight: bold;" id="error_descp" class="error"><?php echo 'Please select Description'; ?></span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="bit_date"><b style="color: red">*</b><?php echo " Date:"; ?></label>
												<div class="col-sm-4">
													<div class="input-group date bit_date">
														<input type="text"  value="<?php echo date("d-m-Y");?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="bit_date" class="form-control" />
														<span class="input-group-btn">
															<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
														</span>

													</div>
													<span style="display: none;color: red;font-weight: bold;" id="error_bit_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
												</div>
											</div>
											
										</div>
									  	<div class="modal-footer">
											<button type="button" class="btn btn-primary" onclick="BitFunction()"  >Save</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									  	</div>
									</div>
								</div>
							</div>

							<div id="myModalBit1" class="modal fade" role="dialog">
								<div class="modal-dialog">
								<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Edit / Change Bit</h4>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="bits_name"><?php echo "Description:"; ?></label>
												<div class="col-sm-10">
													<input type="text" name="bits_name"  placeholder="<?php echo "Bit"; ?>" id="u_bits_name" class="form-control" />
													<input type="hidden" name="bit_id"  id="u_bit_id" class="form-control" />
													<input type="hidden" name="auto_id"  id="u_auto_id" class="form-control" />
													<span style="display: none;color: red;font-weight: bold;" id="error_bit" class="error"><?php echo 'Please select Bit'; ?></span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="description_bits"><?php echo "Full Form:"; ?></label>
												<div class="col-sm-10">
													<input type="text" name="description_bits"  placeholder="<?php echo "Full Form"; ?>" id="u_description_bits" class="form-control" />
													<span style="display: none;color: red;font-weight: bold;" id="error_descp" class="error"><?php echo 'Please select Description'; ?></span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="bit_date"><b style="color: red">*</b><?php echo " Date:"; ?></label>
												<div class="col-sm-4">
													<div class="input-group date bit_date">
														<input type="text"  value="<?php echo date("d-m-Y");?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="u_bit_date" class="form-control" />
														<span class="input-group-btn">
															<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
														</span>

													</div>
													<span style="display: none;color: red;font-weight: bold;" id="error_bit_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
												</div>
											</div>
											
										</div>
									  	<div class="modal-footer">
											<button type="button" class="btn btn-primary" onclick="UpdateBitFunction()"  >Update</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									  	</div>
									</div>
								</div>
							</div>

							<div class="form-group main_div">
								<table id="tblBit" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-left">Description</td>
										  <td class="text-left">Full Form</td>
										  <td class="text-left">Date</td>
										  <td class="text-left"><?php echo "Action"; ?></td>
										</tr>
									</thead>
									<tbody>
										<?php $extra_field = 1; ?>
										<?php $tab_index_1 = 7; ?>
										<?php if($current_bit_datass){ //echo "<pre>";print_r($current_bit_datass);exit; ?>
											<?php foreach($current_bit_datass as $ckey => $cvalue){ ?>
											<tr id="productrawcurrent_row<?php echo $ckey; ?>">
												<td class="text-left">
													<?php echo $cvalue['short_name']; ?>
												</td>
												<td class="text-left">
													<?php echo $cvalue['long_name']; ?>
												</td>
												<td class="text-left">
													<?php echo $cvalue['start_date']; ?>
												</td>
												<td>
													<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalBit1" onclick='updatebit(<?php echo $cvalue['id'] ?>);'><i class="fa fa-pencil"></i></a>
												</td>
											</tr>
											<?php } ?>
										<?php } ?>
		

										
										<input type="hidden" id="extra_field" name="extra_field" value="<?php echo $extra_field; ?>" />
										<input type="hidden" id="tab_index_1" name="tab_index_1" value="<?php echo $tab_index_1; ?>" />
									</tbody>
								</table>
							</div>

							<div class="col-sm-11">
								<h4>Bit History</h4>
							</div>
							<div class="form-group main_div">
								<table id="" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-left">Description</td>
										  <td class="text-left">Full Form</td>
										  <td class="text-left">Start Date</td>
										  <td class="text-left">End Date</td>
										</tr>
									</thead>
									<tbody>
										<?php if($bit_datas){ ?>
											<?php foreach($bit_datas as $ckey => $cvalue){ ?>
											<tr id="productraw_row<?php echo $extra_field; ?>">
												<td class="text-left">
													<?php echo $cvalue['short_name']; ?>
												</td>
												<td class="text-left">
													<?php echo $cvalue['long_name']; ?>
												</td>
												<td class="text-left">
													<?php echo $cvalue['start_date']; ?>
												</td>
												<td class="text-left">
													<?php echo $cvalue['end_date']; ?>
												</td>
											</tr>
											<?php } ?>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="tab-Stakes-Earned-Outstation">
							<div class="col-sm-11">
								<h4>Stakes Earned at RWITC</h4>
							</div>
							<div class="col-sm-1">
								<div id="myModal4" class="modal fade" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Stakes Outstation</h4>
											</div>
											<div class="modal-body">
												<div class="form-group">
												<label class="col-sm-2 control-label" for="input-club"><?php echo "Venue:"; ?></label>
													<div class="col-sm-8">
														<select name="stacked_earneds_vanue" name="stacked_earneds_vanue" placeholder="Venue" id="stacked_earneds_vanue" class="form-control">
															<option value="" >Please Select</option>	
															<?php foreach ($venus as $vkey => $vvalue) { ?>
															<option value="<?php echo $vvalue; ?>"><?php echo $vvalue ?></option>
															<?php } ?>
													 	 </select>
														<span style="display: none;color: red;font-weight: bold;" id="error_stacked_earneds_vanue" class="error"><?php echo 'Please Select Venue'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Season:"; ?></label>
													<div class="col-sm-10">
														<input type="text" name="season_name"  placeholder="Season" id="season_name" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_season" class="error"><?php echo 'Please Enter Season'; ?></span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><?php echo "Race No:"; ?></label>
													<div class="col-sm-10">
														<input type="Number" name="race_no"  placeholder="Race No" id="race_no" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_race_no" class="error"><?php echo 'Please Enter Race No'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><?php echo "Stakes:"; ?></label>
													<div class="col-sm-10">
														<input type="Number" name="stacks_id"  placeholder="Stakes" id="stacks_id" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_stacks_id" class="error"><?php echo 'Please Enter Stakes'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><?php echo "Race Date:"; ?></label>
													<div class="col-sm-8">
														<div class="input-group date input-race_date">
															<input type="text" name="" value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="race_date" class="form-control" />
															<span class="input-group-btn">
																<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<span style="display: none;color: red;font-weight: bold;" id="error_race_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Grade"; ?></label>
													<div class="col-sm-5">
														<select name="grade_stack_id" id="grade_stack_id" class="form-control">
															<option value="" >Please Select</option>	
															<?php foreach ($grads as $gkey => $gvalue) { ?>
																<option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
															<?php } ?>
													  	</select>
														<span style="display: none;color: red;font-weight: bold;" id="error_grade_stack_id" class="error"><?php echo 'Please Select Grade'; ?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-horse"><?php echo "Placing:"; ?></label>
													<div class="col-sm-10">
														<input type="Number" name="placing_id"  placeholder="Placing" id="placing_id" class="form-control" />
														<span style="display: none;color: red;font-weight: bold;" id="error_placing_id" class="error"><?php echo 'Please Enter Place'; ?></span>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-primary" id= "Stackedoutstationsave_id" onclick="StackedoutstationFunction()">Save</button>
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-left"><?php echo "Venue"; ?></td>
										  <td class="text-left"><?php echo "Season"; ?></td>
										  <td class="text-left"><?php echo "Race No"; ?></td>
										  <td class="text-left"><?php echo "Placing"; ?></td>
										  <td class="text-left"><?php echo "Stakes"; ?></td>
										  <td class="text-left"><?php echo "Date"; ?></td>
										  <td class="text-left"><?php echo "Grade"; ?></td>
										</tr>
									</thead>
								</table>
							</div>
							<div style="float: right;padding-bottom: 10px;">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal4" onclick="closestackesearned()"><i class="fa fa-plus-circle"></i></button>
								<input type="hidden" name="stacked_hidden_outstation_id" value="<?php  echo $stacked_hidden_outstation_id ?>" id="stacked_hidden_outstation_id" class="form-control" />
							</div>
							<div class="col-sm-11">
								<h4>Stakes Earned At Outstation</h4>
							</div>
							<div class="form-group">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-left"><?php echo "Venue"; ?></td>
										  <td class="text-left"><?php echo "Season"; ?></td>
										  <td class="text-left"><?php echo "Race No"; ?></td>
										  <td class="text-left"><?php echo "Placing"; ?></td>
										  <td class="text-left"><?php echo "Stakes"; ?></td>
										  <td class="text-left"><?php echo "Date"; ?></td>
										  <td class="text-left"><?php echo "Grade"; ?></td>
										   <td class="text-left"><?php echo "Action"; ?></td>
										</tr>
									</thead>
									<tbody id="stackedoutstationbody">
									 	<?php $sumof_stacks = 0 ;?>
									 	<?php if(isset($stackoutstation_datas)) {  ?>
											<?php foreach($stackoutstation_datas as $stackkey => $stackvalue) { ?>
												<?php  $sumof_stacks = $sumof_stacks +  $stackvalue['stack_id'] ?>
												<tr id='stackedoutstationrow<?php echo $stackkey ?>'>
													<td class="text-left" >	
														<?php echo $stackvalue['stack_venue'] ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][stack_venue]" value="<?php echo $stackvalue['stack_venue'] ?>" >
													</td>
													<td class="text-left" >
														<?php echo $stackvalue['season_name'] ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][season_name]"  value="<?php echo $stackvalue['season_name'] ?>" >
													</td>
													<td class="text-left" >
														<?php echo $stackvalue['stack_race_no']; ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][stack_race_no]" value="<?php echo $stackvalue['stack_race_no']; ?> " >
													</td>
													<td class="text-left" >
														<?php echo $stackvalue['stack_placing_no']; ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][stack_placing_no]" value="<?php echo $stackvalue['stack_placing_no']; ?>" >
													</td>
													<td class="text-left" >
														<?php echo $stackvalue['stack_id']; ?>
														<input type= "hidden" class="stacks_out_id" name= "stackesdatas[<?php echo $stackkey ?>][stack_id]"  value="<?php echo $stackvalue['stack_id']; ?>" >
													</td>
													<td class="text-left" >
														<?php echo date('d-m-Y', strtotime($stackvalue['stack_race_date'])); ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][stack_race_date]"  value="<?php echo date('d-m-Y', strtotime($stackvalue['stack_race_date'])); ?>" >
													</td>
													<td class="text-left" >
														<?php echo $stackvalue['grade_stack_id']; ?>
														<input type= "hidden" name= "stackesdatas[<?php echo $stackkey ?>][grade_stack_id]" value="<?php echo $stackvalue['grade_stack_id']; ?>" >
													</td>
													<td class="text-left">
														<a onclick="removeStackOtDetail(<?php echo $stackvalue['horse_id'] ?>,<?php echo $stackvalue['horse_stackoutstation_id'] ?>,'stackedoutstationrow<?php echo $stackkey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
													</td>
													<input type= "hidden"  name="stackesdatas[<?php echo $stackkey ?>][horse_stackoutstation_id]"   value="<?php echo $stackvalue['horse_stackoutstation_id'] ?>">
													<input type= "hidden"  name="stackesdatas[<?php echo $stackkey ?>][horse_id]"   value="<?php echo $stackvalue['horse_id'] ?>">
												</tr>
											<?php } ?>
           	 							<?php } ?> 
									</tbody>
								</table>
								<center style = 'font-weight: bold;'>Total Stakes ear..<span id = "stacks_dats" ><?php echo $sumof_stacks ?></span></center>
							</div>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
	<script type="text/javascript"><!--
		show_owner_tab = '<?php echo $show_owner_tab; ?>';
		if(show_owner_tab == 1){
			$('#basic_tab_class').removeClass('active');
			$('#ownership_tab_class').addClass('active');
			$('#tab-horse_basic').removeClass('active');
			$('#tab-ownership').addClass('active');
			$('#frm_owner_id').focus();
		}

		$(document).ready(function(){
			$('.date').datetimepicker({
				pickTime: false
			});
		});

		
		$(document).on('focus',".input-equ_start_date , .input-equ_end_date", function(){
		    $(this).datetimepicker({
				pickTime: false
			});
		});



		$('#myTab a').click(function(e) {
		  e.preventDefault();
		  $(this).tab('show');
		});

		// store the currently selected tab in the hash value
		$("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
		  var id = $(e.target).attr("href").substr(1);
		  window.location.hash = id;
		});

		// on load of the page: switch to the currently selected tab
		var hash = window.location.hash;
		$('#myTab a[href="' + hash + '"]').tab('show');

	</script>
	<script type="text/javascript">
	    $("#owner_percentage_id").keyup(function()  {
	      	var owner_percentage_valid =  $('#owner_percentage_id').val();
	      	var owner_percentage_valid1 = owner_percentage_valid.replace(/[^A-Z0-9.]+/i, '');
	      	$("#owner_percentage_id").val(owner_percentage_valid1);
	      	if(parseInt($(this).val()) >= 0  && parseInt($(this).val()) <= 100){
	      		$('#error_owner_percentage').hide();
	      	} else {
	   			$('#error_owner_percentage').show();
	   		} 
	    });

	    var timer = null;
		$('#date_ownership').keyup(function(){
			$('#error_datawonership_same').hide();
			$('#error_end_date_ownership_same').hide();
			$('#error_end_date_ownership_greater').hide();
	       	clearTimeout(timer); 
	        var date_ownership_valid =  $('#date_ownership').val();
	       	date_ownership_valid1 = date_ownership_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_ownership").val(date_ownership_valid1);
	       	timer = setTimeout(dateOwnership, 800)
		});

		function dateOwnership() {
		   var date_ownership_valid =  $('#date_ownership').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_ownership_valid))) {
				$('#error_datawonership').show();
			    return false;
			} else {
				$('#error_datawonership').hide();
			}
		}

		$('.input-date_ownership').datetimepicker().on('dp.change', function (e) {  
		    $('#error_datawonership').css('display','none');
		    $('#error_datawonership_same').hide();
			$('#error_end_date_ownership_same').hide();
			$('#error_end_date_ownership_greater').hide();
		});

		var timer = null;
		$('#end_date_ownership').keyup(function(){
			$('#error_datawonership_same').hide();
			$('#error_end_date_ownership_same').hide();
			$('#error_end_date_ownership_greater').hide();
	       	clearTimeout(timer); 
	        var end_date_ownership_valid =  $('#end_date_ownership').val();
	       	end_date_ownership_valid1 = end_date_ownership_valid.replace(/[^0-9-]+/i, '');
	       	$("#end_date_ownership").val(end_date_ownership_valid1);
	       	timer = setTimeout(end_date_ownership, 800)
		});

		function end_date_ownership() {
		   var end_date_ownership_valid =  $('#end_date_ownership').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(end_date_ownership_valid))) {
				$('#error_end_date_ownership').show();
			    return false;
			} else {
				$('#error_end_date_ownership').hide();
			}
		}

		$('.input-end_date_ownership').datetimepicker().on('dp.change', function (e) {  
		    $('#error_end_date_ownership').css('display','none');
		    $('#error_datawonership_same').hide();
			$('#error_end_date_ownership_same').hide();
			$('#error_end_date_ownership_greater').hide();
		});

	   			 /*For Ownership and laese tab */
		function SaveOwnershp(){
			var date_ownership =  $( "#date_ownership" ).val();
			var end_date_ownership =  $( "#end_date_ownership" ).val();
			var ownership_remarks_checked = $('#ownership_remarks').val();
			var frm_owner_id_checked = $('#frm_owner_id').val();
			var to_owner_id_checked = $('#to_owner_id').val();
			var owner_percentage_id_checked_length= $('#owner_percentage_id').val();
			//var owner_percentage_id_checked_length= $('#owner_percentage_id').val().length;
			var ownership_type_id_ckecked = $('#ownership_type_id').val();
			var frm_owner_hidden_checked = $('#frm_owner_id_hidden').val();
			var to_owner_hidden_checked = $('#to_owner_id_hidden').val();




			if(date_ownership != ''){
				$('#error_datawonership').hide();
				var date_ownerships = date_ownership;
			} else {
				$('#error_datawonership').show();
			}

			if(frm_owner_id_checked != ''){
				$('#error_frm_owner').hide();
				frm_owner_id = frm_owner_id_checked;
				if((frm_owner_hidden_checked != '') && (frm_owner_hidden_checked != undefined)){
					$('#error_frm_owner_hidden').hide();
					frm_owner_hidden_id = frm_owner_hidden_checked;
				} else {
					$('#error_frm_owner_hidden').show();
				}
			} else {
				$('#error_frm_owner_hidden').hide();
				$('#error_frm_owner').show();
			}

			if(to_owner_id_checked != ''){
				$('#error_to_owner').hide();
				to_owner_id = to_owner_id_checked;
				if(to_owner_hidden_checked != '' && to_owner_hidden_checked != undefined){
					$('#error_to_owner_hidden').hide();
					to_owner_hidden_id = to_owner_hidden_checked;
				} else {
					$('#error_to_owner_hidden').show();
				}
			} else {
				$('#error_to_owner_hidden').hide();
				$('#error_to_owner').show();
			}

			//console.log(owner_percentage_id_checked_length);

			if((owner_percentage_id_checked_length == '') || (owner_percentage_id_checked_length <= 0 ||  owner_percentage_id_checked_length > 100)){
				console.log(owner_percentage_id_checked_length);
				console.log('eroor');
				$('#error_owner_percentage').show();
			} else {
				$('#error_owner_percentage').hide();
				owner_percentage_id_checked = owner_percentage_id_checked_length;
			}


			if(ownership_type_id_ckecked != null && ownership_type_id_ckecked != ''){
				$('#error_ownership_type').hide();
				ownership_type_ids = ownership_type_id_ckecked ;
				if(ownership_type_id_ckecked == 'Lease' || ownership_type_id_ckecked == 'Lease with Contingency' ){
					if(end_date_ownership != ''){
						$('#error_end_date_ownership').hide();
						var end_date_ownerships = end_date_ownership;
					} else {
						$('#error_end_date_ownership').show();
					}
				} else {
					if(end_date_ownership == ''){
						$('#error_end_date_ownership').hide();
						var end_date_ownerships = end_date_ownership;
					} 
				}
			} else{
				$('#error_ownership_type').show();
			}

			if(ownership_remarks_checked != ''){
				$('#error_ownership_remarks').hide();
				ownership_remarks = ownership_remarks_checked;
			} else {
				$('#error_ownership_remarks').show();
			}

			inerror = 0;
			if(ownership_type_id_ckecked == 'Lease' || ownership_type_id_ckecked == 'Lease with Contingency'){
				if(date_ownership != '' || end_date_ownership != ''){
					var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
					if (!(date_regex.test(date_ownership))) {
						$('#error_datawonership').show();
						 inerror = 1;
					} else {
						$('#error_datawonership').hide();
					}
					if (!(date_regex.test(end_date_ownership))) {
						$('#error_end_date_ownership').show();
						 inerror = 1;
					} else {
						$('#error_end_date_ownership').hide();
					}
					if(date_regex.test(date_ownership) == true){
						var end_date_ownership1 = end_date_ownership.split('-');
				        var end_day  = end_date_ownership1[0];
				        var end_month = end_date_ownership1[1];
				        var end_year  = end_date_ownership1[2];
				        var end_date = end_year + "-" + end_month + "-" + end_day;
					    var end_date_final   = new Date(end_date);

					    var start_date_ownership1 = date_ownership.split('-');
				        var start_day  = start_date_ownership1[0];
				        var start_month = start_date_ownership1[1];
				        var start_year  = start_date_ownership1[2];
				        var start_date = start_year + "-" + start_month + "-" + start_day;
						var start_date_final = new Date(start_date);
					    
						if(end_date_final < start_date_final){
							$('#error_end_date_ownership_greater').show();
							$('#error_datawonership_same').hide();
							$('#error_end_date_ownership_same').hide();
							inerror = 1;
						} else if(start_date_final == end_date_final){
							$('#error_datawonership_same').show();
							$('#error_end_date_ownership_same').show();
							$('#error_end_date_ownership_greater').hide();
							inerror = 1;
						} else {
							$('#error_datawonership_same').hide();
							$('#error_end_date_ownership_same').hide();
							$('#error_end_date_ownership_greater').hide();
						}
					}
				}
				
				if( date_ownership == '' || end_date_ownership == '' ||  frm_owner_id_checked == '' || to_owner_id_checked == '' || owner_percentage_id_checked_length == ''  || ownership_type_id_ckecked == null ||  ownership_type_id_ckecked == '' || ownership_type_id_ckecked == null ||ownership_type_id_ckecked == '' || ownership_remarks_checked  == '' || to_owner_hidden_checked == 0 || frm_owner_hidden_checked == 0 || to_owner_hidden_checked == '' || frm_owner_hidden_checked == '' || to_owner_hidden_checked == undefined  || (owner_percentage_id_checked_length <= 0 || owner_percentage_id_checked_length > 100)){
					return false;
				}else if(inerror == 1){
					return false;
				}
			} else {
				if(date_ownership != ''){
					var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
					if (!(date_regex.test(date_ownership))) {
						$('#error_datawonership').show();
						 inerror = 1;
					} else {
						$('#error_datawonership').hide();
					}
					if(date_ownership != '' && end_date_ownership != ''){
						if (!(date_regex.test(end_date_ownership))) {
							$('#error_end_date_ownership').show();
							 inerror = 1;
						} else {
							$('#error_end_date_ownership').hide();
						}
						if(date_regex.test(date_ownership) == true){
							var end_date_ownership1 = end_date_ownership.split('-');
					        var end_day  = end_date_ownership1[0];
					        var end_month = end_date_ownership1[1];
					        var end_year  = end_date_ownership1[2];
					        var end_date = end_year + "-" + end_month + "-" + end_day;
						    var end_date_final   = new Date(end_date);

						    var start_date_ownership1 = date_ownership.split('-');
					        var start_day  = start_date_ownership1[0];
					        var start_month = start_date_ownership1[1];
					        var start_year  = start_date_ownership1[2];
					        var start_date = start_year + "-" + start_month + "-" + start_day;
							var start_date_final = new Date(start_date);
							if(end_date_final < start_date_final){
								$('#error_end_date_ownership_greater').show();
								$('#error_datawonership_same').hide();
								$('#error_end_date_ownership_same').hide();
								inerror = 1;
							} else if(start_date_final == end_date_final){
								$('#error_datawonership_same').show();
								$('#error_end_date_ownership_same').show();
								$('#error_end_date_ownership_greater').hide();
								inerror = 1;
							} else {
								$('#error_datawonership_same').hide();
								$('#error_end_date_ownership_same').hide();
								$('#error_end_date_ownership_greater').hide();
							}
						}
					}

				}
				if( date_ownership == '' || frm_owner_id_checked == '' || to_owner_id_checked == '' || owner_percentage_id_checked_length == ''  || ownership_type_id_ckecked == null ||  ownership_type_id_ckecked == '' || ownership_type_id_ckecked == null ||ownership_type_id_ckecked == '' || ownership_remarks_checked  == '' || to_owner_hidden_checked == 0 || frm_owner_hidden_checked == 0 || to_owner_hidden_checked == '' || frm_owner_hidden_checked == '' || to_owner_hidden_checked == undefined  || (owner_percentage_id_checked_length <= 0 || owner_percentage_id_checked_length > 100)){
					return false;
				} else if(inerror == 1){
					return false;
				}
			}


			var ownership_date_id = date_ownerships;
			if(end_date_ownership != '' ){
				var ownership_date_end_id = end_date_ownership;
			} else {
				var ownership_date_end_id = '';
			}
			
			var auto_id = $('#ownership_hidden_id').val();
			var frm_owner_id_hidden = $('#frm_owner_id_hidden').val();
			var to_owner_id_hidden = $('#to_owner_id_hidden').val();
			
			var iscolor_checked = $('input[name=\'color_owner_jk_id\']').is(":checked");
			if(iscolor_checked == false){
				var iscolors_checked = 'No';
				$('input[name=\'color_owner_jk_id\']').val(iscolors_checked);
			} else {
				var iscolors_checked = 'Yes';
				$('input[name=\'color_owner_jk_id\']').val(iscolors_checked);
			}
			var id_hidden_saveownership = $('#id_hidden_saveownership').val();
			 in_ajax = 0;
			$.ajax({
                method: "POST",
                url:'index.php?route=catalog/horse/owner_status&token=<?php echo $token; ?>',
                data: $('#tab-ownership input[type=\'hidden\'], #tab-ownership select , #tab-ownership textarea ,#tab-ownership input[type=\'checkbox\'],#tab-ownership input[type=\'text\'],#tab-ownership input[type=\'Number\']'),
                dataType: "json",
		       success: function(json)
		       {
		       	console.log(json);
		           if(json.success == 1){
						/*if(id_hidden_saveownership != '0'){ 
							html = '<tr id = "tr_'+auto_id+'">';
								html += '<td class="text-left frm_ownername_'+auto_id+'">';
									html += '<span id="from_owner_historys_'+auto_id+'">' + frm_owner_id + '</span>';
									html += '<input type= "hidden"  name= "ownerdatas['+auto_id+'][from_owner]" id="from_owner_history_'+auto_id+'" value = \'' + frm_owner_id + '\'>';
									html += '<input type= "hidden"  name= "ownerdatas['+auto_id+'][from_owner_id]"  id="from_owner_id_history_'+auto_id+'" value = \'' + frm_owner_id_hidden + '\'>';
								html += '</td>';

								html += '<td class="text-left to_ownername_'+auto_id+'" >';
									html += '<span id="to_owner_historys_'+auto_id+'">' + to_owner_id + '</span>';
									html += '<input type= "hidden"   name= "ownerdatas['+auto_id+'][to_owner]"  id="to_owner_history_'+auto_id+'" value = \'' + to_owner_id + '\'>';
									html += '<input type= "hidden"  name= "ownerdatas['+auto_id+'][to_owner_id]"  id="to_owner_idhistory_'+auto_id+'" value = \'' + to_owner_id_hidden + '\'>';
								html += '</td>';

								html += '<td class="text-left owner_percentagename_'+auto_id+'" >';
									html += '<span id="owner_percentage_historys_'+auto_id+'">' +owner_percentage_id+ '</span>';
									html += '<input type= "hidden"   name= "ownerdatas['+auto_id+'][owner_percentage]" id="owner_percentage_history_'+auto_id+'" value = '+owner_percentage_id+'>';
								html += '</td>';

								html += '<td class="text-left ownership_typename_'+auto_id+'" >';
									html += '<span id="ownership_type_historys_'+auto_id+'">' + ownership_type_ids + '</span>';
									html += '<input type= "hidden"   name= "ownerdatas['+auto_id+'][ownership_type]" id="ownership_type_history_'+auto_id+'"  value = \'' + ownership_type_ids + '\'>';
								html += '</td>';

								html += '<td class="text-left ownership_date_name'+auto_id+'" >';
									html += '<span id="date_of_ownership_historys_'+auto_id+'">' +ownership_date_id+ '</span>';
									html += '<input type= "hidden"   name= "ownerdatas['+auto_id+'][date_of_ownership]" id="date_of_ownership_history_'+auto_id+'"  value = '+ownership_date_id+'>';
								html += '</td>';

								html += '<td class="text-left ownership_date_end_'+auto_id+'">';
									html += '<span id="end_date_of_ownership_historys_'+auto_id+'">'+ownership_date_end_id+ '</span>';
									html += '<input type= "hidden"   name= "ownerdatas['+auto_id+'][end_date_of_ownership]"  id="end_date_of_ownership_history_'+auto_id+'" value = '+ownership_date_end_id+'>';
								html += '</td>';

								html += '<td class="ownership_remarks_name_'+auto_id+'">' 
									html += '<span id="remark_historys_'+auto_id+'">'+ ownership_remarks + '</span>';
									html += '<input type= "hidden"  name= "ownerdatas['+auto_id+'][remark_horse_to_owner]"  id="remark_history_'+auto_id+'" value = \'' + ownership_remarks + '\'>'; 
								html += '</td>';

								html += '<td class="text-left iscolors_'+auto_id+'">';
									html += '<span id="color_owner_historys_'+auto_id+'">'+iscolors_checked+ '</span>';
									html += '<input type= "hidden"  name= "ownerdatas['+auto_id+'][owner_color]" id="color_owner_history_'+auto_id+'" value = '+iscolors_checked+'>';
								html += '</td>';
								html += '<td>';
									html += '<a onclick=updateownerhistory("'+auto_id+'") class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>';
							html += '</tr >';
							auto_id++;


							$('#ownershipdeatilsbody').append(html);
							$('#ownership_hidden_id').val(auto_id);
							$('#to_owner_id').val('');
							$('#owner_percentage_id').val('');
							$('#ownership_type_id').val('');
							$('#ownership_date_id').val('');
							$('#ownership_date_end_id').val('');
							$('#ownership_remarks').val('');
							$('#color_owner_jk_id').attr('checked', false);
							$('#frm_owner_id').val('');
							$( "#date_ownership" ).val('');
							$( "#month_ownership" ).val('');
							$( "#year_ownership" ).val('');
							$( "#end_date_ownership" ).val('');
							$( "#end_month_ownership" ).val('');
							$( "#end_year_ownership" ).val('');
							$( "#frm_owner_id_hidden" ).val('');
							$( "#to_owner_id_hidden" ).val('');
							$("#frm_owner_id").focus();
						} else { 
							var old_ownership_date_id = date_ownerships;
							if(end_date_ownership != ''){
								var old_ownership_date_end_id = end_date_ownership;
							} else {
								var old_ownership_date_end_id = '';
							}
							var idowner_old_increment_id = $('#idowner_old_increment_id').val();
							$('#date_of_ownership_history_'+idowner_old_increment_id+'').val(old_ownership_date_id);
							$('#end_date_of_ownership_history_'+idowner_old_increment_id+'').val(old_ownership_date_end_id);
							$('#from_owner_history_'+idowner_old_increment_id+'').val(frm_owner_id);
							$('#from_owner_id_history_'+idowner_old_increment_id+'').val(frm_owner_id_hidden);
							$('#to_owner_history_'+idowner_old_increment_id+'').val(to_owner_id);
							$('#to_owner_idhistory_'+idowner_old_increment_id+'').val(to_owner_id_hidden);
							$('#owner_percentage_history_'+idowner_old_increment_id+'').val(owner_percentage_id);
							$('#ownership_type_history_'+idowner_old_increment_id+'').val(ownership_type_ids);
							$('#remark_history_'+idowner_old_increment_id+'').val(ownership_remarks);
							$('#color_owner_history_'+idowner_old_increment_id+'').val(iscolors_checked);

							$('#date_of_ownership_historys_'+idowner_old_increment_id+'').html(old_ownership_date_id);
							$('#end_date_of_ownership_historys_'+idowner_old_increment_id+'').html(old_ownership_date_end_id);
							$('#from_owner_historys_'+idowner_old_increment_id+'').html(frm_owner_id);
							$('#to_owner_historys_'+idowner_old_increment_id+'').html(to_owner_id);
							$('#owner_percentage_historys_'+idowner_old_increment_id+'').html(owner_percentage_id);
							$('#ownership_type_historys_'+idowner_old_increment_id+'').html(ownership_type_ids);
							$('#remark_historys_'+idowner_old_increment_id+'').html(ownership_remarks);
							$('#color_owner_historys_'+idowner_old_increment_id+'').html(iscolors_checked);
							
							$('#to_owner_id').val('');
							$('#owner_percentage_id').val('');
							$('#ownership_type_id').val('');
							$('#ownership_remarks').val('');
							$('#color_owner_jk_id').attr('checked', false);
							$('#frm_owner_id').val('');
							$( "#date_ownership" ).val('');
							$( "#end_date_ownership" ).val('');
							$( "#frm_owner_id_hidden" ).val('');
							$( "#to_owner_id_hidden" ).val('');
							$('#id_hidden_saveownership').val('');
						}*/
					location.reload(true);
		           } else if(json.warning == 1) {
		           		alert('Please Enter Correct Percentage');
		           } else {
		           		alert('something wrong')
		           }
		       }

	   		 });
			console.log(in_ajax);
			
	


		}

		function updateownerhistory(auto_id){
			$('#id_hidden_saveownership').val(0);
			$('#idowner_old_increment_id').val(auto_id);
			var from_owner_owner = $('#from_owner_history_'+auto_id+'').val();
			var from_owner_id_owner = $('#from_owner_id_history_'+auto_id+'').val();
			var to_owner_owner = $('#to_owner_history_'+auto_id+'').val();
			var to_owner_id_owner = $('#to_owner_idhistory_'+auto_id+'').val();
			var start_date_owner = $('#date_of_ownership_history_'+auto_id+'').val();
			var end_date_owner = $('#end_date_of_ownership_history_'+auto_id+'').val();
			var ownership_type_owner = $('#ownership_type_history_'+auto_id+'').val();
			var percentage_owner = $('#owner_percentage_history_'+auto_id+'').val();
			var remark_owner = $('#remark_history_'+auto_id+'').val();
			var color_owner_owner = $('#color_owner_history_'+auto_id+'').val();

			/*console.log(from_owner_owner);
			$('#tab-ownership input[type=\'text\']').val('');*/

			

			$('#to_owner_id').val(to_owner_owner);
			$('#owner_percentage_id').val(percentage_owner);
			$('#ownership_type_id').val(ownership_type_owner);
			$('#ownership_remarks').val(remark_owner);
			if(color_owner_owner == 'Yes'){
				$('#color_owner_jk_id').prop('checked', true);
				$('#color_owner_jk_id').val(color_owner_owner);
			} else{
				$('#color_owner_jk_id').attr('checked', false);
				$('#color_owner_jk_id').val(color_owner_owner);
			}
			$('#frm_owner_id').val(from_owner_owner);
			$('#date_ownership').val(start_date_owner);
			$('#end_date_ownership').val(end_date_owner);
			$('#frm_owner_id_hidden').val(from_owner_id_owner);
			$('#to_owner_id_hidden').val(to_owner_id_owner);
		}


		$(document).on('keydown', '.form-control', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'owner_percentage_id'){
					$('#ownership_type_id').focus();
				}
				if(id == 'ownership_type_id'){
					$('#date_ownership').focus();
				}
				if(id == 'date_ownership'){
					$('#end_date_ownership').focus();
				}
				if(id == 'end_date_ownership'){
					$('#ownership_remarks').focus();
				}
				if(id == 'ownership_remarks'){
					$('#color_owner_jk_id').focus();
				}
				if(id == 'color_owner_jk_id'){
					$('#savefunctionownership').focus();
				}
				if(id == 'frm_owner_id' && value == '' && $('.frm_ownername_1').length > 0 ){
					var conn = confirm('Do You want to Save');				
					if(conn != false){
						$('#form-horse').submit();
					} else {
					}
				}
			}
		});

		$( "#ownership_type_id" ).select(function() {
			$('#date_ownership').focus();
		});
		
		$( document ).ready(function() {
			$("#horse_code" ).focus();
		});

		$("input, textarea, select, checkbox").keypress(function(event) {
			if (event.which == 13) {
				event.preventDefault();
			}
		});

		$('.from_owner').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
				if(request != ''){
		  			$('#frm_owner_id_hidden').val('');
					$.ajax({
				  		url: 'index.php?route=catalog/horse/autocompleteFromOwner&token=<?php echo $token; ?>&from_owner_name=' +  encodeURIComponent(request),
				  		dataType: 'json',
				  		success: function(json) {   
							response($.map(json, function(item) {
					  			return {
									label: item.fromowner_name,
									value: item.fromowner_code
					  			}
							}));
				 	 	}
					});
				}
		  	}, 
		  	select: function(item) {
				$('#frm_owner_id').val(item.label);
				$('#frm_owner_id_hidden').val(item.value);
				$('.dropdown-menu').hide();
				$('#to_owner_id').focus();
				return false;
		  	},
		});

		$('#to_owner_id').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
				if(request != ''){
					$('#to_owner_id_hidden').val('');
					$.ajax({
				  		url: 'index.php?route=catalog/horse/autocompleteToOwner&token=<?php echo $token; ?>&to_owner_name=' +  encodeURIComponent(request),
				  		dataType: 'json',
				  		success: function(json) {   
							response($.map(json, function(item) {
						  		return {
									label: item.toowner_name,
									value: item.toowner_code
						  		}
							}));
				  		}
					});
				}
			}, 
			select: function(item) {
				$('#to_owner_id').val(item.label);
				$('#to_owner_id_hidden').val(item.value);
				$('.dropdown-menu').hide();
				$('#owner_percentage_id').focus();
				return false;
			},
		});
	</script>

	<script type="text/javascript">
		$("input, textarea, select, checkbox").keypress(function(event) {
			if (event.which == 13) {
				event.preventDefault();
				//SaveOwnershp();
			}
		});
		$('#start_date_ban').keyup(function(){
	        var start_date_ban_valid =  $('#start_date_ban').val();
	       	start_date_ban_valid1 = start_date_ban_valid.replace(/[^0-9-]+/i, '');
	       	$("#start_date_ban").val(start_date_ban_valid1);
	       	var start_date_ban_valid_again =  $('#start_date_ban').val();
		   	var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(start_date_ban_valid_again))) {
				$('#error_date_start_date_ban').show();
			    return false;
			} else {
				$('#error_date_start_date_ban').hide();
			}
		});
		$('.input-start_date_ban').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_start_date_ban').css('display','none');
		});

		$('#end_date_ban').keyup(function(){
	        var end_date_ban_valid =  $('#end_date_ban').val();
	       	end_date_ban_valid1 = end_date_ban_valid.replace(/[^0-9-]+/i, '');
	       	$("#end_date_ban").val(end_date_ban_valid1);
	       	var end_date_ban_valid_again =  $('#end_date_ban').val();
		   	var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(end_date_ban_valid))) {
				$('#error_date_end_date_ban').show();
			    return false;
			} else {
				$('#error_date_end_date_ban').hide();
			}
		});
		$('.input-end_date_ban').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_end_date_ban').css('display','none');
		});

	    //Ban details tab
		function BanFunction(){
			var date_start_date_ban =  $("#start_date_ban" ).val();
			var date_end_date_ban = $( "#end_date_ban" ).val();
			if (date_end_date_ban != '') {
	            
	            var string_start_date = date_start_date_ban.split('-');
                var s_date  = string_start_date[0];
                var s_month = string_start_date[1];
                var s_year  = string_start_date[2];

                var string_end_date = date_end_date_ban.split('-');
                var e_date  = string_end_date[0];
                var e_month = string_end_date[1];
                var e_year  = string_end_date[2];

                if (s_year > e_year){
                    $('#error_greater_start_date').show();
                    return false;
                } else if (e_month > s_month) {
                    if (s_date > e_date || e_date > s_date) {
                        $('#error_greater_start_date').hide();
                    } 
                } else if(s_month > e_month) {
                    $('#error_greater_start_date').show();
                    return false;
                } else if(s_month == e_month) {
                    if (s_date > e_date) {
                        $('#error_greater_start_date').show();
                        return false;
                    }
                }
	        }
			var auto_id = $('#id_hidden_band').val();
			var id_hidden_BanFunction = $('#id_hidden_BanFunction').val();

			if( date_start_date_ban != ''){
				$('#error_date_start_date_ban').hide();
			} else {
				$('#error_date_start_date_ban').show();
			}

			var club = $('#input-club').val();

			if(club != null && club != ''){
				$('#error_club_ban').hide();
				clubs = club;
			} else {
				$('#error_club_ban').show();
			}
			
			var authority_ban = $('#authority_ban_details').val();
			if(authority_ban != '' && authority_ban !=  null){
				$('#error_authority_ban').hide();
				var authority_bans = authority_ban.replace(/\s\s+/g, ' ');
			} else {
				$('#error_authority_ban').show();
			}

			var reason_bans = $('#reason_ban').val();
			if(reason_bans != ''  ){
				$('#error_reason_ban').hide();
			} else {
				$('#error_reason_ban').show();
			}
			inerror = 0;
			if (date_start_date_ban != '' || date_end_date_ban != '' || date_start_date_ban == ''){
				if(date_start_date_ban != ''){
					var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
					if (!(date_regex.test(date_start_date_ban))) {
						$('#error_date_start_date_ban').show();
						 inerror = 1;
					} else {
						$('#error_date_start_date_ban').hide();
					}
					if (date_start_date_ban != '' && date_end_date_ban == '') {
						if (authority_ban == 'BLEEDER 1' || authority_ban == 'BLEEDER 2' || authority_ban == 'BLEEDER 3' && date_end_date_ban == '') {
							$('#error_date_end_date_ban').show();
							inerror = 1;
						}
					}
					if(date_start_date_ban != '' && date_end_date_ban != ''){
						if (!(date_regex.test(date_end_date_ban))) {
							$('#error_date_end_date_ban').show();
							 inerror = 1;
						} else {
							$('#error_date_end_date_ban').hide();
						}
					}
				} 
				// else if(authority_ban == 'BLEEDER 1' && date_end_date_ban == '' && date_start_date_ban != ''){
				// 	alert('innn');
				// 	$('#error_date_end_date_ban').show();
				// 	inerror = 1;
				// }
				if (date_start_date_ban == '' || club == null || club == '' || authority_ban == '' || authority_ban == null || reason_bans == '' ){
					return false;
				} else if(inerror == 1){
					return false;
				}
			} 
			date_start_date_ban == ''
				if(id_hidden_BanFunction == '0'){ 
					html = '<tr id ="bandetail_'+auto_id+'">';
						html += '<td>';
							html += '<span id="clubs_'+auto_id+'">'+clubs+ '</span>';
							html += '<input type= "hidden"  name= "bandats['+auto_id+'][club_ban]" id="club_'+auto_id+'" value = '+clubs+'>';
						html += '</td>';
						html += '<td>';
							html += '<span id="start_dateban_'+auto_id+'">'+date_start_date_ban+ '</span>';
							html += '<input type= "hidden"  name= "bandats['+auto_id+'][startdate_ban]" id="start_date_bans_'+auto_id+'" value = '+date_start_date_ban+'>';
						html += '</td>';
						html += '<td>';
							html += '<span id="end_dateban_'+auto_id+'">'+date_end_date_ban+ '</span>';
							html += '<input type= "hidden"  name= "bandats['+auto_id+'][enddate_ban]" id="end_date_ban_'+auto_id+'" value = '+date_end_date_ban+'>';
						html += '</td>';
						html += '<td>';
							html += '<span id="authorityban_'+auto_id+'">'+authority_bans+ '</span>';
							html += '<input type= "hidden"  name= "bandats['+auto_id+'][authority]" id="authority_ban_'+auto_id+'" value = \'' + authority_bans + '\'>';
						html += '</td>';
						html += '<td>';
							html += '<span id="reasonban_'+auto_id+'">'+reason_bans+ '</span>';
							html += '<input type= "hidden" name= "bandats['+auto_id+'][reason_ban]" id="reason_ban_'+auto_id+'" value = \'' + reason_bans + '\'>'; 
						html += '</td>';
						html += '<td>';
							html += '<a onclick=updateban("'+auto_id+'") class="btn btn-primary"><i class="fa fa-pencil"></i></a>&nbsp';
							// html += '<a onclick="removeBanDetail(0,0,"bandetail_'+auto_id+'")" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a></td>';
							html += '<input type= "hidden" name= "bandats['+auto_id+'][horse_id]"  value ="0" >';
							html += '<input type= "hidden" name= "bandats['+auto_id+'][horse_ban_id]" value ="0"  >';
						html += '</tr >';
					auto_id++;
					$('#bandeatilsbody').append(html);
					$('#id_hidden_band').val(auto_id);
				} else {
					var idban_increment_id = $('#idban_increment_id').val();
					$('#start_date_bans_'+idban_increment_id+'').val(date_start_date_ban);
					$('#end_date_ban_'+idban_increment_id+'').val(date_end_date_ban);
					$('#reason_ban_'+idban_increment_id+'').val(reason_bans);
					$('#authority_ban_'+idban_increment_id+'').val(authority_bans);
					$('#club_'+idban_increment_id+'').val(clubs);

					$('#start_dateban_'+idban_increment_id+'').html(date_start_date_ban);
					$('#end_dateban_'+idban_increment_id+'').html(date_end_date_ban);
					$('#reasonban_'+idban_increment_id+'').html(reason_bans);
					$('#authorityban_'+idban_increment_id+'').html(authority_bans);
					$('#clubs_'+idban_increment_id+'').html(clubs);
				}
			$("#myModal").modal("toggle");

			horse_id = '<?php echo $horse_id ?>';	
			// var ban_datas = $('#ban_data').serialize();
			// alert(ban_datas);
			$.ajax({
				method : "POST",
	            url: 'index.php?route=catalog/horse/addNewBan&token=<?php echo $token; ?>&horse_id='+horse_id,
	            dataType: 'json',
	            data : {
	            	club : club,
	            	authority_ban : authority_ban,
	            	date_start_date_ban : date_start_date_ban,
	            	date_end_date_ban : date_end_date_ban,
	            	reason_bans : reason_bans,
	            	idban_increment_id : idban_increment_id,
	            },
	            success: function(json) { 
	            	if (json.alert != '' ) {
	            		alert(json.alert);
	            		location.reload(true);
	            		return false;
	            	}else{
	            		location.reload(true);
	            	}
	            }
	        });
		}

		//blanked value for Ban details Tab
		function closeaddbuu1(){
			$('#input-club').val('');
			$('#reason_ban').val('');
			$('#authority_ban_details').val('');
			$('#id_hidden_BanFunction').val(0);
			var today = new Date();
			var dd = String(today.getDate()).padStart(2, '0');
			var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
			var yyyy = today.getFullYear();
    		var day_auto_choose_dates_shoe = dd + "-" + mm + "-" + yyyy;
			$( "#start_date_ban" ).val(day_auto_choose_dates_shoe);
			$( "#end_date_ban" ).val();
			$('#error_greater_start_date').hide();
			$('#error_club_ban').hide();
			$('#error_authority_ban').hide();
			$('#error_date_start_date_ban').hide();
			$('#error_date_end_date_ban').hide();
			$('#error_reason_ban').hide();
		}
		$('#myModal').on('shown.bs.modal', function () {
		    $('#input-club').focus();
		}); 

		//update function ban details tab
		function updateban(auto_id){
			//alert(auto_id);
			$('#error_greater_start_date').hide();
			$('#error_club_ban').hide();
			$('#error_authority_ban').hide();
			$('#error_date_start_date_ban').hide();
			$('#error_date_end_date_ban').hide();
			$('#error_reason_ban').hide();
			$('#myModal').modal('show');
			$('#id_hidden_BanFunction').val(1);
			var start_date_ban = $('#start_date_bans_'+auto_id+'').val();
			var end_date_ban = $('#end_date_ban_'+auto_id+'').val();
			var reason_ban = $('#reason_ban_'+auto_id+'').val();
			var authority_ban = $('#authority_ban_'+auto_id+'').val();
			var club_ban = $('#club_'+auto_id+'').val();
			var increment_ban_id = $('#hidden_increment_ban_id_'+auto_id+'').val();
			$("#start_date_ban").val(start_date_ban);
        	$("#end_date_ban").val(end_date_ban);
			$('#authority_ban_details').val(authority_ban);
			$('#reason_ban').val(reason_ban);
			$('#input-club').val(club_ban);

			horse_id = '<?php echo $horse_id ?>';	
			$.ajax({
				method : "POST",
	            url: 'index.php?route=catalog/horse/UpdateBanData&token=<?php echo $token; ?>&horse_id='+horse_id,
	            dataType: 'json',
	            data : {
	            	club_ban : club_ban,
	            	authority_ban : authority_ban,
	            	start_date_ban : start_date_ban,
	            	end_date_ban : end_date_ban,
	            	reason_ban : reason_ban,
	            	increment_ban_id : increment_ban_id,
	            },
	            success: function(json) { 
	            	console.log(json);
	            	$('#idban_increment_id').val(json.id);
	            }
	        });
		}

		$(document).on('keydown', '.form-control', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'input-club'){
					$('#authority_ban_details').focus();
				}
				if(id == 'authority_ban_details'){
					$('#start_date_ban').focus();
				}
				if(id == 'start_date_ban'){
					$('#end_date_ban').focus();
				}
				if(id == 'end_date_ban'){
					$('#reason_ban').focus();
				}
				if(id == 'reason_ban'){
					$('#ban_save_id').focus();
				}
			}
		});

		function removeBanDetail(hourse_id,hourse_ban_id,id_remove){
			if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
			 	if(hourse_id == '0' && hourse_ban_id == '0'){
					alert('Delete Record Sucessfully');
					$('#'+id_remove+'').closest("tr").remove();
					return false;
				}

		        $.ajax({
                	url:'index.php?route=catalog/horse/deletebandeatils&token=<?php echo $token; ?>'+'&hourse_id='+hourse_id+'&hourse_ban_id='+hourse_ban_id,
		           	method: "POST",
		           	dataType: 'json',
		            success: function(json)
		            {
						$('#'+id_remove+'').closest("tr").remove();
						alert(json['success']);
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Error deleting data');
		            }
		        });
		         return false;
     		}
		}
	</script>

	<script type="text/javascript">
		$('#myModal1').on('shown.bs.modal', function () {
    		$('#equipment').focus();
		})
		//blanke value for Change Equipment
		function closeaddequpment(){
			$('#equipment').val('');
			var today = new Date();
			var dd = String(today.getDate()).padStart(2, '0');
			var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
			var yyyy = today.getFullYear();
    		var day_auto_choose_dates = dd + "-" + mm + "-" + yyyy;
    		$('#date_choose_date').val(day_auto_choose_dates);
			$('#defaultChecked').prop('checked', true);
			$('#error_equipement').hide();
			$('#error_date_choose_date').hide();
    		$('#error_equipement_on_off').hide();
    		$('.pre_equipment_assign_datas').html('');
		}

		$('#date_choose_date').keyup(function(){
	        var date_choose_date_valid =  $('#date_choose_date').val();
	       	date_choose_date_valid1 = date_choose_date_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_choose_date").val(date_choose_date_valid1);
	       	var date_choose_date_valid_again =  $('#date_choose_date').val();
		   	var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_choose_date_valid_again))) {
				$('#error_date_choose_date').show();
			    return false;
			} else {
				$('#error_date_choose_date').hide();
			}
		});
		$('.input-date_choose_date').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_choose_date').css('display','none');
		});


		// Change equipement tab
		function EquipemntFunction(){
			//alert('innn');
			var auto_id = $('#id_hidden_eqipment').val();
				auto_id = parseInt(auto_id) + 1;
			

			var iseqipment_checked = $('input[name=\'eqipment_checked\']:checked').val();
			var date_choose_date =  $( "#date_choose_date" ).val();

			var equipments = $('#equipment').val();
			var equ_reason = $('#equ-reason').val();
			//alert(equ_reason);
			if(date_choose_date != ''){
				$('#error_date_choose_date').hide();
				var date_choose_dates = date_choose_date;
			} else {
				$('#error_date_choose_date').show();
			}

			if(equipments != ''){
				$('#error_equipement').hide();
			} else {
				$('#error_equipement').show();
			}

			if(iseqipment_checked != undefined){
				$('#error_equipement_on_off').hide();
			} else {
				$('#error_equipement_on_off').show();
			}

			inerror = 0;
			//if(date_choose_date != '' || date_choose_date == ''){
			var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_choose_date))) {
				$('#error_date_choose_date').show();
				 inerror = 1;
			} else {
				$('#error_date_choose_date').hide();
			}

			if (date_choose_date == '' || iseqipment_checked == undefined || equipments == ''){
				return false;
			} else if(inerror == 1){
				return false;
			}
			
			horse_id = '<?php echo $horse_id ?>';
			check_equip = $("#equipment").val();
			$.ajax({
				method : "POST",
	            url: 'index.php?route=catalog/horse/checkEquip&token=<?php echo $token; ?>&check_equip='+check_equip+'&horse_id='+horse_id,
	            dataType: 'json',
	            success: function(json) {
	            	if (json.alert != '' ) {
	            		alert(json.alert);
	            		return false;
	            	}
	            	if(json.duplicate == 1){
	            	 	alert("This Equipment Is Already In Used! Please Select Another One");
	            	 	return false;
	            	} else {
	            		$.ajax({
							method : "POST",
				            url: 'index.php?route=catalog/horse/addNewEquipment&token=<?php echo $token; ?>&horse_id='+horse_id,
				            dataType: 'json',
				            data : {
				            	name : equipments,
				            	start_date : date_choose_dates,
				            	equipment_status : '1',
				            	reason : equ_reason,
				            },
				            success: function(json) {
				            	if(json.status == 1){
				            	 	$('#myModal1').modal('hide');
				            	 	location.reload(true);
				            	} else {
				            	 	alert("Please Select Date to Update");
				            	 	return false;
				            	}
				            }
				        });
	            	}
	            }
	        });
		}

		function updateEquipmentData(equ_id){
			name = $('#equipment_'+equ_id).val();
			start_date = $('#equ_start_date_'+equ_id).val();
			end_date = $('#equ_end_date_'+equ_id).val();
	        $.ajax({
	            url: 'index.php?route=catalog/horse/updateEquipment&token=<?php echo $token; ?>&equ_id='+equ_id+'&end_date='+end_date,
	            dataType: 'json',
	            success: function(json) { 
	            	 console.log(json.status);
	            	 if(json.status == 1){
	            	 	$('#equ_data_'+equ_id).modal('hide');
	            	 	location.reload(true);
	            	 } else {
	            	 	alert("Please Select End Date to Update");
	            	 	return false;
	            	 }
	            }
	        });
		}

		$(document).on('keydown', '.form-control','.custom-control-input', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'equipment'){
					$('#date_choose_date').focus();
				}
				if(id == 'date_choose_date'){
					$('#defaultChecked').focus();
				}
				if(id == 'defaultChecked'){
					$('#equipemnt_save').focus();
				}
			}
		});

		$(document).on('keydown', '.custom-control-input', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			console.log(name);
			console.log(class_name);
			//console.log(id);
			if(class_name == 'custom-control-input'){
				$('#equipemnt_save').focus();
			}
		});

		$('#equipment').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
				if(request != ''){
					$.ajax({
				  		url: 'index.php?route=catalog/horse/autocompleteEquipement&token=<?php echo $token; ?>&equipment_name=' +  encodeURIComponent(request),
				  		dataType: 'json',
				  		success: function(json) {   
							response($.map(json, function(item) {
					  			return {
									label: item.equipment_name,
									value: item.equipment_name
					  			}
							}));
				 	 	}
					});
				}
		  	}, 
		  	select: function(item) {
		  		//equipment validation 
   				horse_id = '<?php echo $horse_id ?>';
			        $.ajax({
			            url: 'index.php?route=catalog/horse/getEquipmentName&token=<?php echo $token; ?>&equ_name='+item.value+'&horse_id='+horse_id,
			            dataType: 'json',
			            success: function(json) { 
			            	console.log("Innnn Equipment check name"); 
			            	console.log(json);
			            	if((json.isequipment == 1 && item.label == 'BLINKERS') || (json.isequipment == 1 && item.label == 'VISOR')){
			            		$('#equipment').val('');
			            		alert('This is Not Available');
			            		$('#equipment').focus();
			            		return false;
			            	} else {
			            		$('#equipment').val(item.label);
			            	 	$('.pre_equipment_assign_datas').html('');
			            	 	$('.pre_equipment_assign_datas').append(json.html);
								$('.dropdown-menu').hide();
								$('#date_choose_date').focus();
								return false;
			            	}
			            	if((json.eyesheild == 1 && item.label == 'EYE SHEILD (LEFT)') || (json.eyesheild == 1 && item.label == 'EYE SHEILD (RIGHT)')){
			            		$('#equipment').val('');
			            		alert('This is Not Available');
			            		$('#equipment').focus();
			            		return false;
			            	}
			            	if(json.status == 0){
			            	 	$('#equipment').val(item.label);
			            	 	$('.pre_equipment_assign_datas').html('');
			            	 	$('.pre_equipment_assign_datas').append(json.html);
								$('.dropdown-menu').hide();
								$('#date_choose_date').focus();
								return false;
			            	 } else {
			            	 	$('#equipment').val('');
			            	 	alert("This Equipment already on");
			            	 	$('#equipment').focus();
			            	 	return false;
			            	 }

			                // $('#equ_data_'+equ_id[1]).remove();
			                // //$('#tableid_'+equ_id[1]).html('')
			                // $('.modal_equip').append(json);
			                // $('#equ_data_'+equ_id[1]).modal('show'); 
			            }
			        });
			   


				
		  	},
		});
	</script>

	<script type="text/javascript">
	    /* Closed value for Shoeing and bits tab*/
		function closeaddshoe(){
			$('#type_shoe').val('');
			$('#shoe_name').val('');
			$('#description_shoe').val('');
			$('#bit_shoe').val('');
			$('#bit_description').val('');
			$('#select-type_shoe').focus();
			$('.always_selcted').prop('checked', true);
    		var today = new Date();
			var dd = String(today.getDate()).padStart(2, '0');
			var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
			var yyyy = today.getFullYear();
    		var day_auto_choose_dates_shoe = dd + "-" + mm + "-" + yyyy;
    		$('#date_choose_datess').val(day_auto_choose_dates_shoe);
    		$('#error_type_shoeing').hide();
    		$('#error_shoe').hide();
    		$('#error_shoe_description').hide();
    		$('#error_bit_shoe').hide();
    		$('#error_bit_shoe_description').hide();
    		$('#error_shoie_on_off').hide();
    		$('#error_date_choose_datess').hide();

		}
		$('#myModal2').on('shown.bs.modal', function () {
		    $('#select-type_shoe').focus();
		}); 

		$('#date_choose_datess').keyup(function(){
	        var date_choose_datess_valid =  $('#date_choose_datess').val();
	       	date_choose_datess_valid1 = date_choose_datess_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_choose_datess").val(date_choose_datess_valid1);
	       	var date_choose_datess_valid_again =  $('#date_choose_datess').val();
		   	var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_choose_datess_valid_again))) {
				$('#error_date_choose_datess').show();
			    return false;
			} else {
				$('#error_date_choose_datess').hide();
			}
		});
		$('.input-date_choose_datess').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_choose_datess').css('display','none');
		});

		/* For Shoeing and bits tab*/
		function ShoeFunction(){
			var auto_id = $('#id_hidden_shoe').val();
			var isshoe_checked = $('input[name=\'shoe_checkeds\']:checked').val();
			var date_choose_datess =  $( "#date_choose_datess" ).val();

			if(date_choose_datess != ''){
				$('#error_date_choose_datess').hide();
				var date_choose_datesss = date_choose_datess;
			} else {
				$('#error_date_choose_datess').show();
			}

			var type_shoe = $('#select-type_shoe').val();

			if(type_shoe != ''  ){
				$('#error_type_shoeing').hide();
			} else {
				$('#error_type_shoeing').show();
			}

			var description_shoe = $('#description_shoe').val();
			var shoe_description_of_id = $('#description_shoe_id').val();

			var shoe_fullform = $('#full_form').val();


			if(description_shoe != ''  ){
				$('#error_shoe_description').hide();
			} else {
				$('#error_shoe_description').show();
			}
			
			inerror = 0;
			
			var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_choose_datess))) {
				$('#error_date_choose_datess').show();
				 inerror = 1;
			} else {
				$('#error_date_choose_datess').hide();
			}

			if (date_choose_datess == '' || type_shoe == '' || description_shoe == '' ){
				return false;
			} else if(inerror == 1){
				return false;
			}
			
			
			// console.log(date_choose_datess);
			// return false;

			horse_id = '<?php echo $horse_id ?>';	
			$.ajax({
				method : "POST",
	            url: 'index.php?route=catalog/horse/addNewShoes&token=<?php echo $token; ?>&horse_id='+horse_id,
	            dataType: 'json',
	            data : {
	            	type_of_shoe : type_shoe,
				 	fullform : shoe_fullform,
	            	description_of_shoe : description_shoe,
	            	start_date : date_choose_datesss,
	            	shoe_description_id: shoe_description_of_id

	            },
	            success: function(json) { 
	            	if(json.shoeName == 1){
	            	 	$('#myModal2').modal('hide');
	            	 	location.reload(true);
	            	} else {
	            	 	alert("Please Enter Valid Description");
	            	 	return false;
	            	}

	            	if(json.status == 1){
	            	 	$('#myModal2').modal('hide');
	            	 	location.reload(true);
	            	} else {
	            	 	alert("Please Select Date to Update");
	            	 	return false;
	            	}
	            }
	        });


			$("#add_shoes").hide();
			//$("#myModal2").modal("toggle")
		}

		$(document).on('keydown', '.form-control','.custom-control-input', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'select-type_shoe'){
					$('#shoe_name').focus();
				}
				if(id == 'shoe_name'){
					$('#description_shoe').focus();
				}
				if(id == 'description_shoe'){
					$('#bit_shoe').focus();
				}
				if(id == 'bit_shoe'){
					$('#bit_description').focus();
				}
				if(id == 'bit_description'){
					$('#date_choose_datess').focus();
				}
				if(id == 'date_choose_datess'){
					$('#shoe_checkeds').focus();
				}
			}
		});

		$(document).on('keydown', '.custom-control-input', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(id == 'shoe_checkeds'){
				$('#shoeFunction_id').focus();
			}
		});

		function removeShoeingDetail(hourse_id,hourse_shoeing_id,id_remove){
			console.log(hourse_id);
			console.log(hourse_shoeing_id);
			console.log(id_remove);
			if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
			 	if(hourse_id == '0' && hourse_shoeing_id == '0'){
					alert('Delete Record Sucessfully');
					$('#'+id_remove+'').closest("tr").remove();
					return false;
				}

		        $.ajax({
                	url:'index.php?route=catalog/horse/deleteShoeDeatils&token=<?php echo $token; ?>'+'&hourse_id='+hourse_id+'&hourse_shoeing_id='+hourse_shoeing_id,
		           	method: "POST",
		           	dataType: 'json',
		            success: function(json)
		            {
						$('#'+id_remove+'').closest("tr").remove();
						alert(json['success']);
						$('#add_shoes').show();
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Error deleting data');
		            }
		        });
		         return false;
     		}
		}
	</script>

	<script type="text/javascript">
	    // blanked value for stacked outstation tab 
		function closestackesearned(){
			$('#stacked_earneds_vanue').val('');
			$('#season_name').val('');
			$('#race_no').val('');
			$('#stacks_id').val('');
			$('#race_date').val('');
			$('#grade_stack_id').val('');
			$('#placing_id').val('');
			$('#error_race_date').hide();
			$('#error_stacked_earneds_vanue').hide();
			$('#error_season').hide();
			$('#error_race_no').hide();
			$('#error_stacks_id').hide();
			$('#error_placing_id').hide();
			$('#error_grade_stack_id').hide()

		}
		$('#myModal4').on('shown.bs.modal',function (){
			$('#stacked_earneds_vanue').focus();
		});

		$('#race_date').keyup(function(){
	        var race_date_valid =  $('#race_date').val();
	       	race_date_valid1 = race_date_valid.replace(/[^0-9-]+/i, '');
	       	$("#race_date").val(race_date_valid1);
	       	var race_date_valid_again =  $('#race_date').val();
		   	var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(race_date_valid_again))) {
				$('#error_race_date').show();
			    return false;
			} else {
				$('#error_race_date').hide();
			}
		});
		$('.input-race_date').datetimepicker().on('dp.change', function (e) {  
		    $('#error_race_date').css('display','none');
		});

		//stacked outstation tab 
		function StackedoutstationFunction(){
			var auto_id = $('#stacked_hidden_outstation_id').val();
			var race_date =  $( "#race_date" ).val();
			if(race_date != ''){
				$('#error_race_date').hide();
				var race_dates = race_date;
			} else {
				$('#error_race_date').show();
			}

			var stacked_earneds_vanues = $('#stacked_earneds_vanue').val();
			if (stacked_earneds_vanues != null && stacked_earneds_vanues != ''){
				$('#error_stacked_earneds_vanue').hide();
				var stacked_earneds_vanue = stacked_earneds_vanues;
			} else {
				$('#error_stacked_earneds_vanue').show();
			}

			var season_name = $('#season_name').val();
			if (season_name  != ''){
				$('#error_season').hide();
				var season_names = season_name;
			} else {
				$('#error_season').show();
			}

			var race_no = $('#race_no').val();
			if (race_no  != ''){
				$('#error_race_no').hide();
				var race_nos = race_no;
			} else {
				$('#error_race_no').show();
			}

			var stacks_id = $('#stacks_id').val();
			if (stacks_id  != ''){
				$('#error_stacks_id').hide();
				var stacks_ids = stacks_id;
			} else {
				$('#error_stacks_id').show();
			}

			var placing_id = $('#placing_id').val();
			if (placing_id  != ''){
				$('#error_placing_id').hide();
				var placing_ids = placing_id;
			} else {
				$('#error_placing_id').show();
			}

			var grade_stack_id = $('#grade_stack_id').val();
			if (grade_stack_id != null && grade_stack_id != ''){
				$('#error_grade_stack_id').hide();
				var grade_stack_ids = grade_stack_id.replace(/\s\s+/g, ' ');
			} else {
				$('#error_grade_stack_id').show();
			}

			inerror = 0;
			if(race_date != '' || race_date == ''){
				var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
				if (!(date_regex.test(race_date))) {
					$('#error_race_date').show();
					 inerror = 1;
				} else {
					$('#error_race_date').hide();
				}

				if (race_date == '' || stacked_earneds_vanues == '' || stacked_earneds_vanues == null  || season_name == '' || race_no == '' || stacks_id == '' || placing_id == '' || grade_stack_id == null  || grade_stack_id == ''){
					return false;
				}else if(inerror == 1){
					return false;
				}
			}

			html = '<tr id = "stackedoutstationrow'+auto_id+'">';
				html += '<td class="text-left" name= "stacked_earneds_vanuename_'+auto_id+'">'+stacked_earneds_vanue+'';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][stack_venue]"  value = '+stacked_earneds_vanue+'>';

				html += '<td class="text-left" name= "season_name'+auto_id+'" >'+season_names+'';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][season_name]"  value = \'' +season_names + '\'>';

				html += '<td class="text-left" name= "race_no'+auto_id+'" >'+race_nos+'';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][stack_race_no]"  value = '+race_nos+'>';

				html += '<td class="text-left" name= "placing_no'+placing_id+'" >'+placing_ids+'';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][stack_placing_no]"  value = '+placing_ids+'>';

				html += '<td class="text-left " name= "stacks_id'+auto_id+'" >'+stacks_ids+'';
				html += '</td>';
				html += '<input type= "hidden" class="stacks_out_id" name= "stackesdatas['+auto_id+'][stack_id]" id="stacks_id'+auto_id+'" value = '+stacks_ids+'>';
				
				html += '<td class="text-left" name= "race_date'+auto_id+'" >'+race_dates+'';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][stack_race_date]"  value = '+race_dates+'>';

				html += '<td class="text-left" name= "grade_stack_id'+auto_id+'" >'+ grade_stack_ids + '';
				html += '</td>';
				html += '<input type= "hidden"  name= "stackesdatas['+auto_id+'][grade_stack_id]"  value = \'' + grade_stack_ids + '\'>';
				html += '<td>';
				html += '<a onclick=removeStackOtDetail(0,0,"stackedoutstationrow'+auto_id+'") class="btn btn-danger"><i class="fa fa-minus-circle"></i></a></td>'; 
				html += '<input type= "hidden" name= "stackesdatas['+auto_id+'][horse_id]"  value ="0" >';
				html += '<input type= "hidden" name= "stackesdatas['+auto_id+'][horse_stackoutstation_id]" value ="0"  >';
			html += '</tr >';
			
			auto_id++;
			$('#stackedoutstationbody').append(html);
			$('#stacked_hidden_outstation_id').val(auto_id);
			var sum = 0;
			var from_db =  $('#stacks_dats').html();
			$('.stacks_out_id').each(function() {
				sum = parseInt(sum) + parseInt($(this).val());
			});
			$('#stacks_dats').html('');
			$('#stacks_dats').append(sum);
			$("#myModal4").modal("toggle");
		}

		$(document).on('keydown', '.form-control', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'stacked_earneds_vanue'){
					$('#season_name').focus();
				}
				if(id == 'season_name'){
					$('#race_no').focus();
				}
				if(id == 'race_no'){
					$('#stacks_id').focus();
				}
				if(id == 'stacks_id'){
					$('#race_date').focus();
				}
				if(id == 'race_date'){
					$('#grade_stack_id').focus();
				}
				if(id == 'grade_stack_id'){
					$('#placing_id').focus();
				}
				if(id == 'placing_id'){
					$('#Stackedoutstationsave_id').focus();
				}
			}
		});
		function removeStackOtDetail(hourse_id,hourse_stack_id,id_remove){
			console.log(hourse_id);
			console.log(hourse_stack_id);
			console.log(id_remove);
			if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
			 	if(hourse_id == '0' && hourse_stack_id == '0'){
			 		$('#'+id_remove+'').closest("tr").remove();
					var sum = 0;
					var from_db =  $('#stacks_dats').html();
					$('.stacks_out_id').each(function() {
						sum = parseInt(sum) + parseInt($(this).val());
					});
					$('#stacks_dats').html('');
					$('#stacks_dats').append(sum);
					alert('Delete Record Sucessfully');
					return false;
				}

		        $.ajax({
                	url:'index.php?route=catalog/horse/deleteStackDeatils&token=<?php echo $token; ?>'+'&hourse_id='+hourse_id+'&hourse_stack_id='+hourse_stack_id,
		           	method: "POST",
		           	dataType: 'json',
		            success: function(json)
		            {
						$('#'+id_remove+'').closest("tr").remove();
						var sum = 0;
						var from_db =  $('#stacks_dats').html();
						$('.stacks_out_id').each(function() {
							sum = parseInt(sum) + parseInt($(this).val());
						});
						$('#stacks_dats').html('');
						$('#stacks_dats').append(sum);
						alert(json['success']);
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Error deleting data');
		            }
		        });
		         return false;
     		}
		}
	</script>
	<script type="text/javascript">
		//trainer tab 
	    $('#input-trainer_name').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
		  		//$('#trainer_id').val('');
				if(request != ''){
					$.ajax({
				  		url: 'index.php?route=catalog/horse/autocompleteTrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
				  		dataType: 'json',
				  		success: function(json) {   
				  			$('#trainer_codes_id').find('option').remove();
							response($.map(json, function(item) {
					  			return {
									label: item.trainer_name,
									value: item.trainer_name,
									trainer_id:item.trainer_id,
									trainer_codes:item.trainer_code,
									license_type: item.license_type,
					  			}
							}));
				 	 	}
					});
				}
		  	}, 
		  	select: function(item) {
		  		console.log(item);

				$('#input-trainer_name').val(item.value);
				$('#trainer_id').val(item.trainer_id);
				$('#trainer_codes_id').val(item.trainer_codes);
				$('.dropdown-menu').hide();
				$('#date_charge_trainer').focus();
				if(item.license_type == ''){
                    $('.undertaking_charge_label_class').show();
                    $('.undertaking_charge_checkbx_class').show();
                } else {
                    $('.undertaking_charge_label_class').hide();
                    $('.undertaking_charge_checkbx_class').hide();
                }
				return false;
		  	},
		});

		$('#trainer_codes_id').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
		  		//$('#trainer_id').val('');
		  		//alert(request);
				if(request != ''){
					$.ajax({
				  		url: 'index.php?route=catalog/horse/autocompleteTrainercode&token=<?php echo $token; ?>&trainer_code=' +  encodeURIComponent(request),
				  		dataType: 'json',
				  		success: function(json) {   
				  			$('#trainer_codes_id').find('option').remove();
							response($.map(json, function(item) {
					  			return {
									label: item.trainer_code,
									value: item.trainer_code,
									trainer_id:item.trainer_id,
									trainer_codes:item.trainer_code,
									trainer_name:item.trainer_name,
					  			}
							}));
				 	 	}
					});
				}
		  	}, 
		  	select: function(item) {
		  		console.log(item);
				$('#input-trainer_name').val(item.trainer_name);
				$('#trainer_id').val(item.trainer_id);
				$('#trainer_codes_id').val(item.trainer_codes);
				$('.dropdown-menu').hide();
				$('#date_charge_trainer').focus();
				return false;
		  	},
		});

	    $(document).on('keydown', '.form-control', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
	  		//alert(value);
			if(e.which == 13){
				console.log(name);
				console.log(class_name);
				console.log(id);
				if(id == 'input-trainer_name'){
					$('#date_charge_trainer').focus();
				}
				if(id == 'date_charge_trainer' &&  value != ''){
					$('#arrival_time_trainer').focus();
				}
				if(id == 'arrival_time_trainer'){
					$('#extra_narrration_trainer').focus();
				}
				if(id == 'extra_narrration_trainer'){
					$('#date_left_trainer').focus();
				}
				if(id == 'date_left_trainer'){
					$('#extra_narrration_two_trainer').focus();
				}
				if(id == 'extra_narrration_two_trainer'){
					$('#input-trainer_name').focus();
				}
			}
		});
	</script>
	<script type="text/javascript">
		//hourse tab
		var iscolor_checked = $('input[name=\'official_name_change\']:checked').val();
		if (iscolor_checked == 1) {
	    $('#hourse_names_changes').show();
	    } else {
	    	$('#hourse_names_changes').hide();
	    }

		$('#official_name_change').change(function() {
    	if ($(this).is(':checked')) {
		    $('#hourse_names_changes').show();
		    } else {
		    	$('#hourse_names_changes').hide();
		    }
			var abc =  $('#input-horse').val();
			$("#change_horse_name").val(abc);
		});

		var timer = null;
		$('#registeration_date').keyup(function(){
	       	clearTimeout(timer); 
	       $('#error_registeration_date_post').hide();
	        var registeration_date_valid =  $('#registeration_date').val();
	       	registeration_date_valid1 = registeration_date_valid.replace(/[^0-9-]+/i, '');
	       	$("#registeration_date").val(registeration_date_valid1);
	       	timer = setTimeout(checkregi, 800)
		});

		function checkregi() {
		   var registeration_date_valid =  $('#registeration_date').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(registeration_date_valid))) {
				$('#error_registeration_date').show();
			    return false;
			} else {
				$('#error_registeration_date').hide();
			}
		}

		$('.input-registeration_date').datetimepicker().on('dp.change', function (e) {  
		    $('#error_registeration_date').css('display','none');
	      	$('#error_registeration_date_post').hide();
		});

		var timer = null;
		$('#date_left_trainer').keyup(function(){
	       	clearTimeout(timer); 
	       	$('#error_date_left_trainer_post').hide();
	        var date_left_trainer_valid =  $('#date_left_trainer').val();
	       	date_left_trainer_valid1 = date_left_trainer_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_left_trainer").val(date_left_trainer_valid1);
	       	timer = setTimeout(checklefttrainer, 800)
		});

		function checklefttrainer() {
		   var date_left_trainer_valid =  $('#date_left_trainer').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_left_trainer_valid))) {
				$('#error_date_left_trainer').show();
			    return false;
			} else {
				$('#error_date_left_trainer').hide();
			}
		}

		$('.input-date_left_trainer').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_left_trainer').css('display','none');
		    $('#error_date_left_trainer_post').css('display','none');
		});

		var timer = null;
		$('#date_charge_trainer').keyup(function(){
	       	clearTimeout(timer); 
	       	$('#error_date_charge_trainer_post').hide();
	        var date_charge_trainer_valid =  $('#date_charge_trainer').val();
	       	date_charge_trainer_valid1 = date_charge_trainer_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_charge_trainer").val(date_charge_trainer_valid1);
	       	timer = setTimeout(checkchargetrainer, 800)
		});

		function checkchargetrainer() {
		   var date_charge_trainer_valid =  $('#date_charge_trainer').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_charge_trainer_valid))) {
				$('#error_date_charge_trainer').show();
			    return false;
			} else {
				$('#error_date_charge_trainer').hide();
			}
		}

		$('.input-date_charge_trainer').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_charge_trainer').css('display','none');
		    $('#error_date_charge_trainer_post').css('display','none');
		});

		var timer = null;
		$('#date_std_stall_certificate').keyup(function(){
	       	clearTimeout(timer); 
	       	$('#error_date_std_stall_certificate_post').hide();
	        var date_std_stall_certificate_valid =  $('#date_std_stall_certificate').val();
	       	date_std_stall_certificate_valid1 = date_std_stall_certificate_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_std_stall_certificate").val(date_std_stall_certificate_valid1);
	       	timer = setTimeout(stallcertificate, 800)
		});

		function stallcertificate() {
		   var date_std_stall_certificate_valid =  $('#date_std_stall_certificate').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_std_stall_certificate_valid))) {
				$('#error_date_std_stall_certificate').show();
			    return false;
			} else {
				$('#error_date_std_stall_certificate').hide();
			}
		}

		$('.input-date_std_stall_certificate').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_std_stall_certificate').css('display','none');
		    $('#error_date_std_stall_certificate_post').css('display','none');
		});

		var timer = null;
		$('#date_foal_date').keyup(function(){
	       	clearTimeout(timer); 
	       	$('#error_date_foal_date_post').hide();
	        var date_foal_date_valid =  $('#date_foal_date').val();
	       	date_foal_date_valid1 = date_foal_date_valid.replace(/[^0-9-]+/i, '');
	       	$("#date_foal_date").val(date_foal_date_valid1);
	       	foaldate();
		});

		function foaldate() {
		   var date_foal_date_valid =  $('#date_foal_date').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(date_foal_date_valid))) {
				$('#error_date_foal_date').show();
			    return false;
			} else {
					$('#error_date_foal_date').hide();
					if(date_foal_date_valid != ''){

		    		var date_foal_date_for_age1 = date_foal_date_valid.split('-');
			        var s_date  = date_foal_date_for_age1[0];
			        var s_month = date_foal_date_for_age1[1];
			        var s_year  = date_foal_date_for_age1[2];

			        var start = s_year + "-" + s_month + "-" + s_date;

					var current_year = new Date().getFullYear()
					//alert(s_year);
				    var age   = current_year - s_year ;
				    //alert(age);
					$('#input-age').val(age);
					$('#input-age').attr('value', age);
				}
			}
		}



		$('.input-date_foal_date').datetimepicker().on('dp.change', function (e) {  
		    $('#error_date_foal_date').css('display','none');
		    $('#error_date_foal_date_post').css('display','none');

		    var date_foal_date_for_age =  $('#date_foal_date').val();
			if(date_foal_date_for_age != ''){
				var today = new Date();
				var dd = String(today.getDate()).padStart(2, '0');
				var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
				var yyyy = today.getFullYear();
	    		var current_date = yyyy + "-" + mm + "-" + dd;

	    		var date_foal_date_for_age1 = date_foal_date_for_age.split('-');
		        var s_date  = date_foal_date_for_age1[0];
		        var s_month = date_foal_date_for_age1[1];
		        var s_year  = date_foal_date_for_age1[2];

		        var start = s_year + "-" + s_month + "-" + s_date;

				var starts = new Date(start);
			    var end   = new Date(current_date);

			    //diff  = new Date(end - starts);
			    //days  = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
			    days = yyyy - s_year;
			    //alert(days);
				$('#input-age').val(days);
				$('#input-age').attr('value', days);
			}

		    
		});
		var timer = null;
		$('#change_horse_date').keyup(function(){
	       	clearTimeout(timer); 
	       	$('#error_change_horse_date_post').hide();
	        var change_horse_date_valid =  $('#change_horse_date').val();
	       	change_horse_date_valid1 = change_horse_date_valid.replace(/[^0-9-]+/i, '');
	       	$("#change_horse_date").val(change_horse_date_valid1);
	       	timer = setTimeout(changehorse, 800)
		});

		function changehorse() {
		   var change_horse_date_valid =  $('#change_horse_date').val();
		   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(change_horse_date_valid))) {
				$('#error_change_horse_date').show();
			    return false;
			} else {
				$('#error_change_horse_date').hide();
			}
		}

		$('.input-change_horse_dates').datetimepicker().on('dp.change', function (e) {  
		    $('#error_change_horse_date').css('display','none');
		    $('#error_change_horse_date_post').css('display','none');
		});

		$('.input-date_left_trainer').datetimepicker().on('dp.change', function (e) {  
			$('.undertaking_charge_label_class').show();
			$('.undertaking_charge_checkbx_class').show();
		});

		$( document ).ready(function() {
            var date_left_trainer =  $('#date_left_trainer').val();
            if(date_left_trainer != ''){
            	$('.undertaking_charge_label_class').show();
				$('.undertaking_charge_checkbx_class').show();
            }
         });


		$(document).on('keydown', '.form-control', '.form-group', function(e) {
		var name = $(this).attr('name'); 
	  	var class_name = $(this).attr('class'); 
	  	var id = $(this).attr('id');
	  	var value = $(this).val();
			if(e.which == 13){

				if(id == 'horse_code'){
					$('#input-horse').focus();
				}
				if(id == 'input-horse'){
					$('#official_name_change').focus();
				}

				if(id == 'official_name_change'){
					if ($('#hourse_names_changes').is(':visible')) {
						$('#change_horse_name').focus();
					} else {
						 $('#input-passport').focus();
					}
				}

				if(id == 'change_horse_name'){
					$('#change_horse_date').focus();
				}

				if(id == 'change_horse_date'){
					$('#input-passport').focus();
				}

				if(id == 'input-passport'){
					$('#registeration_date').focus();
				}

				if(id == 'registeration_date'){
					$('#registerarton_authentication_id').focus();
				}

				if(id == 'registerarton_authentication_id'){
					$('#input-sire').focus();
				}

				if(id == 'input-sire'){
					$('#input-dam_nat').focus();
				}

				if(id == 'input-dam_nat'){
					$('#input-micro_chip1').focus();
				}

				if(id == 'input-micro_chip1'){
					$('#input-micro_chip_2').focus();
				}

				if(id == 'input-micro_chip_2'){
					$('#arrival_charges_to_be_paid').focus();
				}

				if(id == 'arrival_charges_to_be_paid'){
					$('#date_foal_date').focus();
				}

				if(id == 'date_foal_date'){
					$('#input-color').focus();
				}
				
				if(id == 'input-color'){
					$('#input_origin').focus();
				}
				if(id == 'input_origin'){
					$('#input_sex').focus();
				}

				if(id == 'input_sex'){
					$('#input-stud_farm').focus();
				}

				if(id == 'input-stud_farm'){
					$('#input-rating').focus();
				}

				if(id == 'input-rating'){
					$('#input-breeder').focus();
				}

				if(id == 'input-breeder'){
					$('#input-saddle_no').focus();
				}

				if(id == 'input-saddle_no'){
					$('#input-octroi').focus();
				}

				if(id == 'input-octroi'){
					$('#input-awbi').focus();
				}

				if(id == 'input-awbi'){
					$('#date_std_stall_certificate').focus();
				}

				// if(id == 'awbi_registration_file'){
				// 	$('#date_std_stall_certificate').focus();
				// }

				if(id == 'date_std_stall_certificate'){
					$('#input-id_by_brand').focus();
				}

				

				if(id == 'input-id_by_brand'){
					$('#input-stall_certificate').focus();
				}

				if(id == 'input-stall_certificate'){
					$('#horse_remarks').focus();
				}

				if(id == 'horse_remarks'){
					$('#select-isActive').focus();
				}

				if(id == 'select-isActive'){
					$('#horse_code').focus();
				}
			}
		});

		// $( document ).ready(function() {
  //           var license_type_show =  $("#license_type").val()
  //           if(license_type_show == 'B'){
  //               $('.one_time_levy').show();
  //           } else {
  //               $('.one_time_levy').hide();
  //           }
  //       });

  //       $( "#license_type" ).change(function(){
  //           var license_type_show =  $(this).val()
  //           if(license_type_show == 'B'){
  //               $('.one_time_levy').show();
  //           } else {
  //               $('.one_time_levy').hide();
  //           }
  //       });
        
  //       $( document ).ready(function() {
  //           var license_type_show =  $("#license_type").val()
  //           if(license_type_show == 'A'){
  //               $('.arrival_charges').show();
  //           } else {
  //               $('.arrival_charges').hide();
  //           }
  //       });

  //       $( "#license_type" ).change(function(){
  //           var license_type_show =  $(this).val()
  //           if(license_type_show == 'A'){
  //               $('.arrival_charges').show();
  //           } else {
  //               $('.arrival_charges').hide();
  //           }
  //       });

		$( document ).ready(function() {
             var input_sex =  $('#input_sex').val()
            //alert(assistant_trainer);
                if(input_sex == 'g'){
                    $('#l_date_sex_date').show();
                    $('#date_sex_date_div').show();
                } else if(input_sex == 'r'){
                	$('#l_date_sex_date').show();
                    $('#date_sex_date_div').show();
                } else {
                    $('#l_date_sex_date').hide();
                    $('#date_sex_date_div').hide();
                }
       });
		$( "#input_sex" ).change(function(){
            var input_sex =  $(this).val()
           //console.log(input_sex);
                if(input_sex == 'g'){
                    $('#l_date_sex_date').show();
                    $('#date_sex_date_div').show();
                } else if(input_sex == 'r'){   console.log(input_sex);
                	$('#l_date_sex_date').show();
                    $('#date_sex_date_div').show();
                } else {
                    $('#l_date_sex_date').hide();
                    $('#date_sex_date_div').hide();
                }
        });



		$(document).on('keydown', '.form-control','.custom-control-input', function(e) {
			var name = $(this).attr('name'); 
	  		var class_name = $(this).attr('class'); 
	  		var id = $(this).attr('id');
	  		var value = $(this).val();
			if(e.which == 13){
				if(id == 'frm_owner_id'){
					$('#to_owner_id').focus();
				}
				if(id == 'to_owner_id'){
					$('#owner_percentage_id').focus();
				}
				if(id == 'owner_percentage_id'){
					$('#ownership_type_id').focus();
				}
				
			}
		});

		$('#button-other_document_1').on('click', function() {
		$('#form-other_document_1').remove();
		$('body').prepend('<form enctype="multipart/form-data" id="form-other_document_1" style="display: none;"><input type="file" name="file" /></form>');
		$('#form-other_document_1 input[name=\'file\']').trigger('click');
		if (typeof timer != 'undefined') {
			  clearInterval(timer);
		}
		timer = setInterval(function() {
			if ($('#form-other_document_1 input[name=\'file\']').val() != '') {
			  clearInterval(timer); 
			  image_name = 'Awbi_Registeration_file';  
			  $.ajax({ 
				url: 'index.php?route=catalog/horse/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
				type: 'post',   
				dataType: 'json',
				data: new FormData($('#form-other_document_1')[0]),
				cache: false,
				contentType: false,
				processData: false,   
				beforeSend: function() {
				  $('#button-upload').button('loading');
				},
				complete: function() {
				  $('#button-upload').button('reset');
				},  
				success: function(json) {
				  if (json['error']) {
					alert(json['error']);
				  }
				  if (json['success']) {
					alert(json['success']);
					console.log(json);
					$('input[name=\'awbi_registration_file\']').attr('value', json['filename']);
					$('input[name=\'awbi_registration_file_source\']').attr('value', json['link_href']);
					d = new Date();
					var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="awbi_registration_file_source" href="'+json['link_href']+'">View Document</a>';
					$('#awbi_registration_file_source').remove();
					$('#button-other_document_1_new').append(previewHtml);
				  }
				},      
				error: function(xhr, ajaxOptions, thrownError) {
				  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			  });
			}
		  }, 500);
		});

		$('#input-horse_isb').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
				if(request != ''){
					$('#horse_isb_id').val('');
					$.ajax({
				  		url: 'index.php?route=catalog/horse/getHorseIsb&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request),
				  		dataType: 'json',
				  		success: function(json) {  
				  		console.log(json);	 
							response($.map(json, function(item) {
						  		return {
									label: item.horse_name,
									value: item.hore_isb_id,
									year_horse: item.YROFFLNG,
									country: item.country,
									mother: item.mother,
									mother_country:item.mother_country,
									horse_color:item.horse_color,
									horse_gender:item.horse_gender,
									breeder_name:item.STUDNAME,
									horse_foal_date:item.FOALDATE,
									hourse_micro_chip_one:item.MICROCHIP1,
									hourse_micro_chip_two:item.MICROCHIP2,
									hourse_sire_name:item.SIRENAME,
									LIFENUMBER:item.LIFENUMBER,
									BRAND1:item.BRAND1
						  		}
							}));
				  		}
					});
				}
			}, 
			select: function(item) {
				
				$('#horse_isb_table_body').show();
				$('#horse_isb_horse_name').html('');
				$('#hidden_horse_isb_id').val('');
				$('#horse_isb_year').html('');
				$('#horse_isb_breeder').html('');
				$('#horse_isb_country').html('');
				$('#horse_isb_mother_name').html('');
				$('#horse_isb_mother_country_name').html('');
				$('#horse_isb_color').html('');
				$('#horse_isb_sex').html('');
				$('#horse_isb_foal_date').val('');
				$('#horse_isb_micro_chip_one').val('');
				$('#hourse_micro_chip_two').val('');
				$('#horse_isb_sire_name').val('');
				$('#horse_isb_id_by_brand').val('');
				$('#horse_input_passport').val('');



				$('#horse_isb_horse_name').html(item.label);
				$('#hidden_horse_isb_id').val(item.value);
				$('#horse_isb_year').html(item.year_horse);
				$('#horse_isb_breeder').html(item.breeder_name);
				$('#horse_isb_country').html(item.country);
				$('#horse_isb_mother_name').html(item.mother);
				$('#horse_isb_mother_country_name').html(item.mother_country);
				$('#horse_isb_color').html(item.horse_color);
				$('#horse_isb_sex').html(item.horse_gender);
				$('#horse_isb_foal_date').val(item.horse_foal_date);
				$('#horse_isb_micro_chip_one').val(item.hourse_micro_chip_one);
				$('#hourse_micro_chip_two').val(item.hourse_micro_chip_two);
				$('#horse_isb_sire_name').val(item.hourse_sire_name);
				$('#horse_isb_id_by_brand').val(item.BRAND1);
				$('#horse_input_passport').val(item.LIFENUMBER);
				$('#input-horse_isb').val(item.label);
				$('.dropdown-menu').hide();
				return false;
			},
		});

		function importhorseisb (){
			var hidden_horse_isb_id = $( "#hidden_horse_isb_id" ).val();
			if (hidden_horse_isb_id != '') {
				var horse_isb_horse_name = $('#horse_isb_horse_name').html();
				$('#input-horse').val('');
				$('#input-horse').val(horse_isb_horse_name);
				var horse_isb_color = $('#horse_isb_color').html();
				$('#input-color').val('');
				$('#input-color').val(horse_isb_color);
				var horse_isb_sex = $('#horse_isb_sex').html();
				$('#input_sex').val('');
				$('#input_sex').val(horse_isb_sex);
				var horse_isb_foal_date = $('#horse_isb_foal_date').val();
				$('#date_foal_date').val('');
				$('#date_foal_date').val(horse_isb_foal_date);
				var horse_isb_micro_chip_one = $('#horse_isb_micro_chip_one').val();
				
				$('#input-micro_chip1').val('');
				$('#input-micro_chip1').val(horse_isb_micro_chip_one);
				var hourse_micro_chip_two = $('#hourse_micro_chip_two').val();
				
				$('#input-micro_chip2').val('');
				$('#input-micro_chip_2').val(hourse_micro_chip_two);

				var horse_isb_mother_name = $('#horse_isb_mother_name').html();
				$('#input-dam_nat').val('');
				$('#input-dam_nat').val(horse_isb_mother_name);
				var horse_isb_breeder = $('#horse_isb_breeder').html();
				$('#input-breeder').val('');
				$('#input-breeder').val(horse_isb_breeder);

				var horse_isb_sire_name = $('#horse_isb_sire_name').val();
				
				$('#input-sire').val('');
				$('#input-sire').val(horse_isb_sire_name);

				var horse_isb_id_by_brand = $('#horse_isb_id_by_brand').val();
				
				$('#input-id_by_brand').val('');
				$('#input-id_by_brand').val(horse_isb_id_by_brand);

				var horse_input_passport = $('#horse_input_passport').val();
				
				$('#input-passport').val('');
				$('#input-passport').val(horse_input_passport);

				$('#hidden_isb_horse').val('');
				$('#hidden_isb_horse').val(hidden_horse_isb_id);
				foaldate();

				/* $.ajax({
	            	url:'index.php?route=catalog/horse/UpdateStatusIsbHorse&token=<?php echo $token; ?>'+'&hidden_horse_isb_id='+hidden_horse_isb_id,
		           	method: "POST",
		           	dataType: 'json',
		            success: function(json)
		            {
						$('#isbhorse_bttn').hide();
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Error Updating date data');
		            }
		        });*/
			}
			$("#myhorseisbModal").modal("toggle");
		}

		function blankisb_horse(){
			$('#horse_isb_horse_name').html('');
			$('#hidden_horse_isb_id').val('');
			$('#horse_isb_year').html('');
			$('#horse_isb_breeder').html('');
			$('#horse_isb_country').html('');
			$('#horse_isb_mother_name').html('');
			$('#horse_isb_mother_country_name').html('');
			$('#horse_isb_color').html('');
			$('#horse_isb_sex').html('');
			$('#horse_isb_foal_date').val('');
			$('#horse_isb_micro_chip_one').val('');
			$('#hourse_micro_chip_two').val('');
			$('#horse_isb_sire_name').val('');
			$('#horse_isb_id_by_brand').val('');
			$('#horse_input_passport').val('');
		}


	</script>
<script type="text/javascript">
		
$('#description_shoe').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		if(request != ''){
			$.ajax({
		  		url: 'index.php?route=catalog/horse/getShoes&token=<?php echo $token; ?>&shoe_name=' +  encodeURIComponent(request),
		  		dataType: 'json',
		  		success: function(json) {
		  			console.log('inn');
		  			console.log(json);
					response($.map(json, function(item) {
			  			return {
							label: item.shoe_name,
							value: item.shoe_name,
							shoe_id: item.shoe_id,
							descp: item.shoe_details
			  			}
					}));
		 	 	}
			});
		}
  	}, 
  	select: function(item) {
		$('#description_shoe').val(item.label);
		$('#description_shoe').val(item.value);
		$('#description_shoe_id').val(item.shoe_id);
		$('#full_form').val(item.descp);
		$('.dropdown-menu').hide();
		//$('#to_owner_id').focus();
		return false;
  	},
});

$('#u_description_shoe').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		if(request != ''){
			$.ajax({
		  		url: 'index.php?route=catalog/horse/getShoes&token=<?php echo $token; ?>&shoe_name=' +  encodeURIComponent(request),
		  		dataType: 'json',
		  		success: function(json) {
		  			console.log('inn');
		  			console.log(json);
					response($.map(json, function(item) {
			  			return {
							label: item.shoe_name,
							value: item.shoe_name,
							shoe_id: item.shoe_id,
							descp: item.shoe_details
			  			}
					}));
		 	 	}
			});
		}
  	}, 
  	select: function(item) {
		$('#u_description_shoe').val(item.label);
		$('#u_description_shoe').val(item.value);
		$('#u_description_shoe_id').val(item.shoe_id);
		$('#u_full_form').val(item.descp);
		$('.dropdown-menu').hide();
		//$('#to_owner_id').focus();
		return false;
  	},
});

/*function BitFunction() {

}*/

	var extra_field = $('#extra_field').val();
	var tab_index_1 = $('#tab_index_1').val();
	function BitFunction(){
		var bits_name = $("#bits_name").val();
		var bit_id = $("#bit_id").val();
		var bit_date = $("#bit_date").val();
		var description_bits = $("#description_bits").val();


		inerror = 0;
		
			var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
			if (!(date_regex.test(bit_date))) {
				$('#error_bit_date').show();
				 inerror = 1;
			} else {
				$('#error_bit_date').hide();
			}

			if (bit_date == '' || bits_name == '' || description_bits == '' ){
				return false;
			} else if(inerror == 1){
				return false;
			}
	
			
		

 		horse_id = '<?php echo $horse_id ?>';	
		$.ajax({
			method : "POST",
            url: 'index.php?route=catalog/horse/addNewBit&token=<?php echo $token; ?>&horse_id='+horse_id,
            dataType: 'json',
            data : {
            	bit_name : bits_name,
            	bit_id : bit_id,
			 	description_bit : description_bits,
            	start_date : bit_date
            },
            success: function(json) { 
            	if(json.bitName == 1){
            	 	$("#add_bit").hide();
            	 	$('#myModalBit').modal('hide');
            	 	location.reload(true);
            	} else {
            	 	alert("Please Enter Valid Description");
            	 	return false;
            	}

            	if(json.status == 1){
            	 	$("#add_bit").hide();
            	 	$('#myModalBit').modal('hide');
            	 	location.reload(true);
            	} else {
            	 	alert("Please Select Date to Update");
            	 	return false;
            	}


            }
        });
	}


function remove_folder(extra_field){
  $('#productraw_row'+extra_field).remove();
  $("#add_bit").show();

}

$('#bits_name').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		if(request != ''){
			$.ajax({
		  		url: 'index.php?route=catalog/horse/getBits&token=<?php echo $token; ?>&bits_name=' +  encodeURIComponent(request),
		  		dataType: 'json',
		  		success: function(json) {
		  			console.log('inn');
		  			console.log(json);
					response($.map(json, function(item) {
			  			return {
							label: item.short_name,
							value: item.short_name,
							descp: item.long_name,
							bit_id: item.bit_id

			  			}
					}));
		 	 	}
			});
		}
  	}, 
  	select: function(item) {
		$('#bits_name').val(item.label);
		$('#bits_name').val(item.value);
		$('#bit_id').val(item.bit_id);

		$('#description_bits').val(item.descp);
		$('.dropdown-menu').hide();
		//$('#to_owner_id').focus();
		return false;
  	},
});

$('#u_bits_name').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		if(request != ''){
			$.ajax({
		  		url: 'index.php?route=catalog/horse/getBits&token=<?php echo $token; ?>&bits_name=' +  encodeURIComponent(request),
		  		dataType: 'json',
		  		success: function(json) {
		  			console.log('inn');
		  			console.log(json);
					response($.map(json, function(item) {
			  			return {
							label: item.short_name,
							value: item.short_name,
							descp: item.long_name,
							bit_id: item.bit_id

			  			}
					}));
		 	 	}
			});
		}
  	}, 
  	select: function(item) {
		$('#u_bits_name').val(item.label);
		$('#u_bits_name').val(item.value);
		$('#u_bit_id').val(item.bit_id);
		$('#u_description_bits').val(item.descp);
		$('.dropdown-menu').hide();
		//$('#to_owner_id').focus();
		return false;
  	},
});

$(document).on('input', '#input-horse', function(){
    var name = $( "#input-horse" ).val();
    $( "#name1" ).val(name);
});


//contingency modal 


	$(document).on("click",".modalopen",function() {
	    //alert('inn');
	    console.log($(this).attr('id'));
	    horse_owner_idssss = $(this).attr('id');
	    horse_owner_id = horse_owner_idssss.split('_');

	    $.ajax({
	        url: 'index.php?route=transaction/ownership_shift_module/getContaingencyData&token=<?php echo $token; ?>&horse_owner_id='+horse_owner_id[1],
	        dataType: 'json',
	        success: function(json) {   
	            $('.modal-body').html('');
	            $('.modal-body').append(json);
	    		$('#contModal').modal('show'); 
	        }
	    });
	});


 // owner Partners Detail Popup

    $(document).on("click",".partners",function() {
        //alert('inn');
        console.log($(this).attr('id'));
        parent_owner_idss = $(this).attr('id');
        parent_owner_id = parent_owner_idss.split('_');

        horse_id = '<?php echo $horse_id ?>';

        $.ajax({
            url: 'index.php?route=catalog/horse/getPartnersData&token=<?php echo $token; ?>&parent_owner_id='+parent_owner_id[1]+'&horse_id='+horse_id,
            dataType: 'json',
            success: function(json) {   
                $('#partners_data_'+parent_owner_id[1]).remove();
                //$('#tableid_'+parent_owner_id[1]).html('')
                $('.mod').append(json);
                $('#partners_data_'+parent_owner_id[1]).modal('show'); 
            }
        });
    });


    //equipment chnange popup 

    $(document).on("click",".chng_equ",function() {
        equ_idss = $(this).attr('id');
        equ_id = equ_idss.split('_');
        horse_id = '<?php echo $horse_id ?>';

        $.ajax({
            url: 'index.php?route=catalog/horse/getEquipmentData&token=<?php echo $token; ?>&equ_id='+equ_id[1]+'&horse_id='+horse_id,
            dataType: 'json',
            success: function(json) {   
                $('#equ_data_'+equ_id[1]).remove();
                $('.modal_equip').append(json);
                $('#equ_data_'+equ_id[1]).modal('show'); 
            }
        });
    });



    
    
</script>
<script type="text/javascript">

	$('#general_save').on('click',function(){
		var date_foal_date = $('#date_foal_date').val();
		var sire = $('#input-sire').val();
		var dam_nat = $('#input-dam_nat').val();
		var color = $('#input-color').val();
		var origin = $('#input_origin').val();
		var sex = $('#input_sex').val();
		var registerarton_authentication = $('#registerarton_authentication_id').val();
		if (sire == '') {
			alert('Please Enter Sire')
			return false;
		}
		if (dam_nat == '') {
			alert('Please Enter Dam')
			return false;
		}
		if (color == '') {
			alert('Please Enter Color')
			return false;
		}
		if (origin == '') {
			alert('Please Enter Origin')
			return false;
		}
		if (sex == '') {
			alert('Please Enter Sex')
			return false;
		}
		if (registerarton_authentication == '') {
			alert('Please Enter Registration Auth')
			return false;
		}
		if (date_foal_date == '') {
			alert('Please Enter Foal Date')
			return false;
		}
		horse_id = '<?php echo $horse_id ?>';
		$.ajax({
			type: "POST",
		    url: 'index.php?route=catalog/horse/saveGeneralDatas&token=<?php echo $token; ?>&horse_id='+horse_id,
		    dataType: 'json',
		    data: $('#form-horse').serialize(),
	        success: function(json) {
	        	if(json.success == 1){
            	 	window.location ='index.php?route=catalog/horse&token=<?php echo $token; ?>';
            	}
				//details();
	        }
		});
	});

	function prov() {
		var prov = document.getElementById("provisionals").checked;
		$('#provisional').val(prov);
	}

	// function unde() {
	// 	var unde = document.getElementById("undertaking_charge").checked;
	// 	$('#undertaking_charges').val(unde);
	// }


	function TrainerCharge(){
		var horse_to_trainer_id = $('#horse_to_trainer_id').val();
		var horse_id = $('#tariner_horse_id').val();
		var code = $('#trainer_codes_id').val(); 
		var trainer_name = $('#input-trainer_name').val();
		var trainer_id = $('#trainer_id').val();
		var charge_trainer = $('#date_charge_trainer').val();
		var arrival_time = $('#arrival_time_trainer').val();
		var extra_narrration = $('#extra_narrration_trainer').val();
		var left_trainer = $('#date_left_trainer').val();
		//var undertaking_charge = $('#undertaking_charge').val();
		var extra_narrration_two = $('#extra_narrration_two_trainer').val();
		var provisionals = $('#provisional').val();
		if (provisionals == 'true') {
			provisional = 1;
		} else {
			provisional = 0;
		}
		//var undertaking_charge =  $('#undertaking_charge').prop('checked', true);
		if ($('#undertaking_charge').prop('checked')) {
			var undertaking_charge = 1;
		} else{
			var undertaking_charge = 0;
		}
		
		//alert(undertaking_charge);

		//alert(name);
		//console.log(form);
		if (trainer_id != '') {
			if (charge_trainer != '') {
				$.ajax({
					type: "POST",
				    url: 'index.php?route=catalog/horse/TrainerDetails&token=<?php echo $token; ?>&horse_id='+horse_id,
				    dataType: 'json',
				    data: {
				    	horse_to_trainer_id: horse_to_trainer_id,
				    	horse_id: horse_id,
				    	code: code,
				    	trainer_name: trainer_name,
				    	trainer_id: trainer_id,
				    	charge_trainer: charge_trainer,
				    	arrival_time: arrival_time,
				    	extra_narrration: extra_narrration,
				    	undertaking_charge: undertaking_charge,
				    	extra_narrration_two: extra_narrration_two,
				    	left_trainer: left_trainer,
				    	provisional: provisional,
				    	extra_narrration_two: extra_narrration_two,
				    },
			        success: function(json) {
			        	console.log(json.alert);
			        	if (json.alert != '' ) {
		            		alert('Please Enter Valid Trainer');
		            		return false;
		            	} else {
							$('#tbcharge tbody').append(json.html);
							location.reload(true);
		            	}
						//details();
			        }
				});
			} else {
				alert("Please Select Date Of Charge!");
				return false;
			}
		} else {
			alert("Please Select Trainer!");
			return false;
		}
	}

	function importnewhorse() {
		var horse_name = $("#input-new_horse").val();
		$("#input-horse").val(horse_name);
		$("#mynewhorseModal").modal("toggle");
	}
</script>
<script type="text/javascript">
	function updateshoe(horse_shoeing_id){
		$.ajax({
			type: "POST",
		    url: 'index.php?route=catalog/horse/UpdateShoesData&token=<?php echo $token; ?>&shoeing_id='+horse_shoeing_id,
		    dataType: 'json',
		    //data: $('#form-horse').serialize(),
	        success: function(json) {
	        	console.log(json);
	        	$('#u_select-type_shoe').val(json.type);
	        	$('#u_description_shoe').val(json.shoe_description);
	        	$('#u_description_shoe_id').val(json.shoe_description_id); 
	        	$('#u_full_form').val(json.full_form);
	        	$('#u_date_choose_datess').val(json.shoe_start_date);
	        	$('#u_horse_shoeing_id').val(json.horse_shoeing_id);

	        	// if(json.success == 1){
          //   	 	window.location ='index.php?route=catalog/horse&token=<?php echo $token; ?>';
          //   	}
				//details();
	        }
		});

	}

	function UpdateShoeFunction(){
		var type_shoe = $('#u_select-type_shoe').val();
		var shoe_fullform = $('#u_full_form').val();
		var description_shoe = $('#u_description_shoe').val();
		var date_choose_datesss = $('#u_date_choose_datess').val();
		var shoe_description_of_id = $('#u_description_shoe_id').val(); 
		var horse_shoeing_id = $('#u_horse_shoeing_id').val();

		$.ajax({
			method : "POST",
            url: 'index.php?route=catalog/horse/UpdateShoes&token=<?php echo $token; ?>&horse_shoeing_id='+horse_shoeing_id,
            dataType: 'json',
            data : {
            	type_of_shoe : type_shoe,
			 	fullform : shoe_fullform,
            	description_of_shoe : description_shoe,
            	start_date : date_choose_datesss,
            	shoe_description_id: shoe_description_of_id

            },
            success: function(json) { 
            	if(json.shoeName == 1){
            	 	$('#myModal2').modal('hide');
            	 	location.reload(true);
            	} else {
            	 	alert("Please Enter Valid Description");
            	 	return false;
            	}

            	if(json.status == 1){
            	 	$('#myModal2').modal('hide');
            	 	location.reload(true);
            	} else {
            	 	alert("Please Select Date to Update");
            	 	return false;
            	}
            }
        });
	}
</script>

<script type="text/javascript">
	function updatebit(bit_id){
		//alert(bit_id);
		$.ajax({
			type: "POST",
		    url: 'index.php?route=catalog/horse/UpdateBitData&token=<?php echo $token; ?>&bit_id='+bit_id,
		    dataType: 'json',
		    //data: $('#form-horse').serialize(),
	        success: function(json) {
	        	console.log(json);
	        	$('#u_bits_name').val(json.short_name);
	        	$('#u_description_bits').val(json.long_name);
	        	$('#u_bit_id').val(json.bit_id); 
	        	$('#u_bit_date').val(json.start_date);
	        	$('#u_auto_id').val(json.id);
	        }
		});

	}

	function UpdateBitFunction(){
		var short_name = $('#u_bits_name').val();
		var long_name = $('#u_description_bits').val();
		var bit_id =$('#u_bit_id').val(); 
		var start_date = $('#u_bit_date').val();
		var auto_id = $('#u_auto_id').val();

		$.ajax({
			method : "POST",
            url: 'index.php?route=catalog/horse/UpdateBits&token=<?php echo $token; ?>&auto_id='+auto_id,
            dataType: 'json',
            data : {
            	short_name : short_name,
			 	long_name : long_name,
            	bit_id : bit_id,
            	start_date : start_date,
            },
            success: function(json) { 
            	if(json.bitName == 1){
            	 	$('#myModalBit1').modal('hide');
            	 	location.reload(true);
            	} else {
            	 	alert("Please Enter Valid Description");
            	 	return false;
            	}

            	if(json.status == 1){
            	 	$('#myModalBit1').modal('hide');
            	 	location.reload(true);
            	} else {
            	 	alert("Please Select Date to Update");
            	 	return false;
            	}
            }
        });
	}
</script>

<script type="text/javascript">
	function updatetrainer(horse_trainer_id){
		//alert(horse_trainer_id);
		$.ajax({
			type: "POST",
		    url: 'index.php?route=catalog/horse/UpdateTrainerData&token=<?php echo $token; ?>&horse_trainer_id='+horse_trainer_id,
		    dataType: 'json',
		    //data: $('#form-horse').serialize(),
	        success: function(json) {
	        	console.log(json);
	        	$('#u_date_of_charge').val(json.date_of_charge);
	        	$('#u_left_date').val(json.left_date_of_charge);
	        	$('#u_extra_narration').val(json.extra_narration); 
	        	$('#u_horse_trainer_id').val(json.horse_to_trainer_id);
	        }
		});

	}

	function UpdateTrainerfunc(){
		var date_of_charge = $('#u_date_of_charge').val();
		var left_date_of_charge = $('#u_left_date').val();
		var extra_narration =$('#u_extra_narration').val(); 
		var horse_to_trainer_id = $('#u_horse_trainer_id').val();

		$.ajax({
			method : "POST",
            url: 'index.php?route=catalog/horse/Updatetrainers&token=<?php echo $token; ?>&horse_to_trainer_id='+horse_to_trainer_id,
            dataType: 'json',
            data : {
            	date_of_charge : date_of_charge,
			 	left_date_of_charge : left_date_of_charge,
            	extra_narration : extra_narration,
            },
            success: function(json) { 
            	if(json.status == 1){
            	 	$('#myModaltrainer').modal('hide');
            	 	location.reload(true);
            	} else {
            	 	alert("Please Select Date to Update");
            	 	return false;
            	}
            }
        });
	}
</script>

<?php echo $footer; ?>