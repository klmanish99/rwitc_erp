<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
			<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			<?php if($user_group_id == 15 && $approval_status == 0){ ?>
				<a href="<?php echo $approval; ?>" data-toggle="tooltip" title="<?php echo 'Approve'; ?>" class="btn btn-primary"><i class="fa fa-check"></i></a>
				<a onclick="reason_approve()" data-toggle="tooltip" title="<?php echo 'Reject'; ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
			<?php } ?></div>
			<div class="pull-right" >
				<textarea style="display: none; " rows="3" placeholder="Reason" class="col-sm-8" id="reason"></textarea>
				<a onclick="rejected()" style="display: none; margin-left: 10px;" data-toggle="tooltip" title="<?php echo 'Reject'; ?>" class="btn btn-danger col-sm-3" id="reject" >Reject</a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo 'View Inward'; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-po_number"><?php echo 'Inward Code : '; ?></label>
						<div class="col-sm-2">
							<input type="hidden" name="order_id" value="<?php echo $order_id; ?>" id="input-order_id" class="form-control" />
							<input disabled="disabled" type="text" value="<?php echo $order_no; ?>" class="form-control" />
						</div>
						<label class="col-sm-2 control-label" for="input-mname"><?php echo 'Date : '; ?></label>
						<div class="col-sm-2">
							<input disabled="disabled" type="text" value="<?php echo date('d-M-Y', strtotime($date)); ?>" class="form-control" />
						</div>
						<label class="col-sm-2 control-label" for="input-requested_by"><?php echo 'Supplier:'; ?></label>
						<div class="col-sm-2 sup_div">
							<input disabled="disabled" type="text" value="<?php echo $supplier; ?>" class="form-control" />
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="col-sm-2 control-label" for="input-mname"><?php echo 'Narration:'; ?></label>
						<div class="col-sm-2 sup_div">
							<input disabled="disabled" type="text" value="<?php echo $narration; ?>" class="form-control" />
						</div>
						<label class="col-sm-2 control-label" for="input-mname"><?php echo 'Indent Req. No:'; ?></label>
						<div class="col-sm-2 sup_div">
							<input disabled="disabled" type="text" value="<?php echo $order_id; ?>" class="form-control" />
						</div>
					</div>	 -->
					<div class="form-group">
						<table class="table table-bordered table-hover">
								<thead>
									<tr>
									 	<td class="text-center" style="width: 10%" >Medicine Code</td>
									 	<td class="text-center">Medicine Name</td>
									 	<td class="text-center">Po Data</td>
									 	<td class="text-center">Po Qty</td>
									 	<td class="text-center">Pending Po Qty</td>
									 	<td class="text-center">Packing Type</td>
									 	<td class="text-center">Quantity</td>
									 	<td class="text-center">Convert Qty</td>
									 	<td class="text-center">Rate Rs</td>
									 	<td class="text-center">Value Rs</td>
									 	<td class="text-center">GST Rate %</td>
									 	<td class="text-center">GST Value Rs</td>
									 	<td class="text-center">Total Rs</td>
									 	<td class="text-center">Expiry Date</td>
									 	<td class="text-center">Batch No</td>
									</tr>
								</thead>
								 	<?php $extra_field_row = 0; ?>
								 	<?php 
									$total_quantity = 0;
									$total_total = 0;
								 	?>
								 	<?php foreach ($productraw_datas as $pkey => $productraw_data) { ?>
								<tbody>  
									<tr>
										<td style="text-align: center;">
										 	<?php echo $productraw_data['product_id']; ?>
										</td>
										<td style="text-align: left;">
										 	<?php echo $productraw_data['productraw_name']; ?>
										</td>
										<td style="text-align: left;">
										 	<?php echo $productraw_data['po_no']; ?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['po_qty']; ?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['reduce_po_qty']; ?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['packing'];?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['quantity']; ?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['ordered_quantity']; ?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['purchase_price']; ?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['value']; ?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['gst_rate']; ?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['gst_value']; ?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['total']; ?>
										</td>
										<td style="text-align: left;">
										 	<?php echo $productraw_data['ex_date']; ?>
										</td>
										<td style="text-align: right;">
										 	<?php echo $productraw_data['batch_no']; ?>
										</td>
									</tr>
								</tbody>
								<?php } ?>
							<?php $extra_field_row++; ?>
							<?php  ?>
							<input type="hidden" id="extra_field_row" name="extra_field_row" value="<?php echo $extra_field_row; ?>" />
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
var extra_field_row = $('#extra_field_row').val();
function addproduct(data) {
//function addproduct(name, id, price, manufacturer_id, manufacturer_name, type, due_on, weight_kg_per_length, product_link) {
  html  = '<tbody id="po_contents_row' + extra_field_row + '" class="po_contents_row">';
	html += '<tr>'; 
	  html += '<td class="left" style=""><a target="_blank" href='+data.product_href+'>'+data.label+'</a>';
		html += '<input type="hidden" name="productraw_datas[' + extra_field_row + '][productraw_name]" value="'+data.label+'"  />';
		html += '<input type="hidden" name="productraw_datas[' + extra_field_row + '][product_href]" value="'+data.product_href+'"  />';
		html += '<input type="hidden" name="productraw_datas[' + extra_field_row + '][product_id]" id="product_id-'+extra_field_row+'" value="'+data.value+'"  />';
		html += '<input type="hidden" name="productraw_datas[' + extra_field_row + '][manufacturer_id]" value="'+data.manufacturer_id+'" />';
		html += '<input type="hidden" name="productraw_datas[' + extra_field_row + '][manufacturer_name]" value="'+data.manufacturer_name+'" />';
		html += '<input type="hidden" name="productraw_datas[' + extra_field_row + '][type]" id="type-'+extra_field_row+'" value="'+data.type+'" />';
		html += '<input type="hidden" name="productraw_datas[' + extra_field_row + '][po_number]" id="po_number-'+extra_field_row+'" value="0" />';
		html += '<input type="hidden" name="productraw_datas[' + extra_field_row + '][is_new]" id="is_new-'+extra_field_row+'" value="1" />';
		html += '<input type="hidden" name="productraw_datas[' + extra_field_row + '][date_added]" id="date_added-'+extra_field_row+'" value="'+data.date_added+'" />';
		html += '<input type="hidden" name="productraw_datas[' + extra_field_row + '][base_price]" id="base_price-'+extra_field_row+'" value="'+data.base_price+'" />';
		html += '</td>';
		html += '<td class="right" style="text-align:left;">'+data.description+'</td>';
		html += '<td class="right" style="text-align:right;">'+data.base_price_dis+'</td>';
		html += '<td class="right" style="text-align:right;"><input type="text" class="search_po_quantity" id="quantity-'+extra_field_row+'" name="productraw_datas[' + extra_field_row + '][quantity]" value="'+data.quantity+'" size="10"/> '+data.type+'</td>';
		html += '<td class="right" style="text-align:right;"><input readonly="readonly" id="total_dis-'+extra_field_row+'" type="text" name="productraw_datas[' + extra_field_row + '][total_dis]" value="'+data.total_dis+'" size="10" /><input id="total-'+extra_field_row+'" type="hidden" name="productraw_datas[' + extra_field_row + '][total]" value="'+data.total+'" /></td>';
		html += '<td class="left" style="text-align:left;"><a style="cursor: pointer;" onclick="remove_folder('+extra_field_row+')" class="button"><span><?php echo "Remove"; ?></span></a></td>';
		html += '</tr>';  
		html += '</tbody>';
  $('#po_content tfoot').before(html);
  //ownerautocomplete(extra_field_row);
  extra_field_row++;
  $('.date').datetimepicker({
	pickTime: false,
	format: 'DD-MM-YYYY'
  });
  $('#input-product').focus();
}

function remove_folder(extra_field_row){
  //$('#medicine_contents_row'+extra_field_row).remove();
  $('#po_contents_row'+extra_field_row).remove();
  updatetotals();
}

$('#po_content').on('keyup', '.search_po_quantity', function(ev){
  $('.dropdown-menu').hide();
  idss = $(this).attr('id');
  s_id = idss.split('-');
  quantity = $('#quantity-'+s_id[1]).val();
  price = $('#base_price-'+s_id[1]).val();
  total = price * quantity;
  $('#total_dis-'+s_id[1]).attr('value', total.toFixed(2));
  $('#total-'+s_id[1]).attr('value', total.toFixed(2));
  updatetotals();
});

// Category


function close_button(){
  $('.dropdown-menu').hide();
  $('.search_po_quantity').focus();
}

$('input[name=\'mname\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/vendor/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['vendor_name'],
			value: item['vendor_id'],
			state_data: item['state_data'],
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'mname\']').val(item.label);
	$('input[name=\'mname_id\']').val(item.value);

	state_datas = item.state_data;
	//html = '';
	$('#input-state').find('option').remove();
	$.each(state_datas, function (i, item) {
	  $('#input-state').append($('<option>', { 
		  value: item,
		  text : item 
	  }));
	});
	$("#input-state option[value='all']").attr("selected", "selected");
	return false;
  }
});

$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY'
  
});

function updatetotals(){
  total_quantity = 0;
  total_total = 0;
  for(i=0;i<$('.po_contents_row').length;i++){
	if($('#po_contents_row'+i).length){
	  $quan = parseFloat($('#quantity-'+i).val());
	  total_quantity = total_quantity + $quan;

	  $tot = parseFloat($('#total-'+i).val());
	  total_total = total_total + $tot;
	}
  }
  $('#total_quantity').html(total_quantity);
  $('#total_total').html(total_total);
}

function reason_approve() {
	$('#reason').show();
	$('#reject').show();
}

function rejected() {
	var reason = $('#reason').val();
	var order_id = $('#input-order_id').val();
	url = 'index.php?route=catalog/inward/inward_dis_approval&token=<?php echo $token; ?>';
	if (reason != '') {
		url += '&reason=' + encodeURIComponent(reason);
		url += '&order_id=' + encodeURIComponent(order_id);
	} else {
		return false;
	}
	window.location.href = url;
}


//--></script>
<?php echo $footer; ?>