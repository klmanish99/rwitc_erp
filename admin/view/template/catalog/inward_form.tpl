<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button onclick="checks() ? $('#form-manufacturer').submit() : false;" type="button" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a onclick="prev_inw()" data-toggle="tooltip" title="<?php echo 'Previous Inward'; ?>" class="btn btn-primary">Previous Inward</a>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
				<?php if($user_group_id == 15 && $approval_status == 0){ ?>
					<a href="<?php echo $approval; ?>" data-toggle="tooltip" title="<?php echo 'Approve'; ?>" class="btn btn-primary"><i class="fa fa-check"></i></a>
					<a onclick="reason_approve()" data-toggle="tooltip" title="<?php echo 'Reject'; ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
				<?php } ?>
			</div>
			<div class="pull-right" >
				<textarea style="display: none; " rows="3" placeholder="Reason" class="col-sm-8" id="reason"></textarea>
				<a onclick="rejected()" style="display: none; margin-left: 10px;" data-toggle="tooltip" title="<?php echo 'Reject'; ?>" class="btn btn-danger col-sm-3" id="reject" >Reject</a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i>Add Inward</h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
					<ul class="nav nav-tabs">
		            	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
		        	</ul>
		        	<div class="tab-content">
		        		<div class="tab-pane active" id="tab-general">
		        			<div class="form-group">
								<label class="col-sm-2 control-label" for="input-order_no">Inward Code</label>
								<div class="col-sm-2">
									<input type="hidden" value="<?php echo $user_log_grp_id; ?>" name="user_log_grp_id">
									<input type="hidden" value="<?php echo $user_log_id; ?>" name="user_log_id">
									<input type="hidden" value="" id="input-added" class="form-control" />
									<input type="hidden" value="" id="input-checks" class="form-control" />
									<input type="hidden" value="" id="input-datas" class="form-control" />
									<input type="hidden" name="order_id" value="<?php echo $order_id; ?>" id="input-order_id" class="form-control" />
									<input disabled="disabled" tabindex="-1" type="text" name="order_no" value="<?php echo $order_no; ?>" placeholder="Inward Code" id="input-order_no" class="form-control" />
									<input type="hidden" name="hidden_order_no" value="<?php echo $order_no; ?>" placeholder="Inward Code" id="input-order_no" class="form-control" />
								</div>
								<label class="col-sm-2 control-label" for="input-order_no">Supplier</label>
								<div class="col-sm-2">
									<input tabindex="-1" type="text" name="supplier" value="<?php echo $supplier; ?>" placeholder="Supplier" id="input-supplier" class="form-control" />
									<?php if (isset($error_valierr_supplier_name)) { ?><span class="errors" style="color: #ff0000;"><?php echo $error_valierr_supplier_name; ?></span><?php } ?>
								</div>
					        	<label style="display: none;" class="col-sm-2 control-label" for="input-date"><?php echo ' Date'; ?></label>
					            <div style="display: none;" class="col-sm-3">
					            	<input type="hidden" name="date" value="<?php echo $date; ?>" id="input-date" class="form-control"/>
					            	<input type="text" name="date" value="<?php echo $date; ?>" placeholder="<?php echo 'Date'; ?>" id="input-date" class="form-control date"/>
					        	</div>
							</div>
							
							<div class="form-group main_div">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-center">Code</td>
										  <td class="text-center">Medicine Name</td>
										  <td class="text-center" style="width: 215px;">PO no</td>
										  <td class="text-center">Quantity</td>
										  <td class="text-center">Rate Rs</td>
										  <td class="text-center">Expiry Date</td>
										  <td class="text-center">Batch No</td>
									</tr>

									</thead>
									<tbody>
										<tr style="background: white;" >
											<td style="width: 120px;" class="text-left">
												<input  type="text" name="productnew_code" value="" placeholder="<?php echo 'Code'; ?>" id="input-productnew_code" class="form-control" />
											</td>
											<td class="text-left">
												<input type="text" name="productnew" value="" placeholder="<?php echo 'Medicine Name'; ?>" id="input-productnew" class="form-control" />
												<input type="hidden" name="med_id" value="" placeholder="<?php echo 'Medicine Id'; ?>" id="input-med_id" class="form-control" />
											</td>
											<td class="text-left">
												<select name="po_data" id="select-po_data" class="form-control">
							                        <option value="" selected="selected"></option>
							                    </select>
											</td>
											<td class="text-right">
												<input type="text" name="quantity" value="" placeholder="<?php echo 'Qty'; ?>" id="input-quantity" class="form-control" />
												<input style="outline: none; border: none; background: white;" readonly="readonly" type="hidden" name="packing" value="" placeholder="" id="input-packing" class="" />
												<input type="hidden" name="volume" id="hidden-volume" class="form-control" />
												<input style="border: none; background: white;" readonly="readonly" type="hidden" name="unit" value="" placeholder="" id="input-unit" class="" />
												<input style="border: none;" type="hidden" readonly="readonly" name="ordered_quantitys" value="" id="input-ordered_quantitys" class="" />
												<input style="background: white;" readonly="readonly" type="hidden" name="po_qty" value="" placeholder="<?php echo 'Po Qty'; ?>" id="input-po_qty" class="form-control" />
												<input style="background: white;" readonly="readonly" type="hidden" value="" placeholder="<?php echo 'Reduce Po Qty'; ?>" id="hidden-reduce_po_qty" class="form-control" />
												<input style="border: none;outline: none;cursor: pointer;" type="hidden" readonly="readonly" name="gst_rate" value="" id="input-gst_rate" class="" />
												<input style="border: none;outline: none;cursor: pointer;" type="hidden" readonly="readonly" name="gst_value" value="" id="input-gst_value" class="" />
												<input style="border: none;outline: none;cursor: pointer;" type="hidden" readonly="readonly" name="total" value="" id="input-total" class="" />
												<input type="hidden" name="totals" value="0" id="input-totals" class="totals form-control" />
												<input type="hidden" name="vals" value="<?php echo $value_total; ?>" id="input-vals" class="form-control" />
												<input type="hidden" name="gst_val" value="<?php echo $gst_value_total; ?>" id="input-gst_val" class="form-control" />
												<input type="hidden" name="prev_rate" value="<?php echo $all_total; ?>" id="input-prev_rate" class="form-control" />
												<input style="border: none;outline: none;cursor: pointer;" type="hidden" readonly="readonly" name="purchase_value" value="" id="input-purchase_value" class="" />
											</td>
											<td class="text-right">
												<input style="background: white;" type="number" name="purchase_price" value="" placeholder="<?php echo 'Rate'; ?>" id="input-purchase_price" class="form-control" />
											</td>
											<td style="width: 150px;" class="text-left">
												<input  type="date" name="expiry_date" value="" placeholder="<?php echo 'Expiry Date'; ?>" id="input-expiry_date" class="form-control" />
												<span id="error_expiry_date" style="display: none;" >Please Select Valid Date!</span>
											</td>
											<td style="width: 150px;" class="text-left">
												<input type="text"  name="batch_no" value="" placeholder="Batch No" id="input-batch_no" class="form-control batch_no"/>
											</td>
										</tr>
										<thead>
											<tr>
											  <td class="text-center">Pending PO Qty</td>
											  <td class="text-center">Packing Type</td>
											  <td class="text-center">Converted Qty</td>
											  <td class="text-center">Previous Rate</td>
											  <td class="text-center">Value Rs</td>
											  <td class="text-center">GST Rate %</td>
											  <td class="text-center">GST Value Rs</td>
											  <td class="text-center">Total Rs</td>
											</tr>
										</thead>
										<tr style="background: white;">
											<td class="text-left" style="text-align: right;">
												<input style="text-align: right;border: none; width: 95px;background: white;" readonly="readonly" type="text" name="reduce_po_qty" value="" placeholder="" id="input-reduce_po_qty" class="" />
											</td>
											<td class="text-left" id="autopacking"></td>
											<td class="text-left">
												<input style="text-align: left;width: 80px;border: none;" type="text" readonly="readonly" name="ordered_quantity" value="" id="input-ordered_quantity" class="" />
											</td>
											<td class="text-left" id="auto_prev_rate" ></td>
											<td class="text-right">
												<input style="text-align: right;border: none;width: 70px;background: white;" readonly="readonly" type="text" name="Value" value="" placeholder="" id="input-Values" class="" />
											</td>
											<td class="text-right" id="auto_gst_rate" ></td>
											<td class="text-right" id="auto_gst_value" ></td>
											<td class="text-right" id="auto_total" ></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="form-group main_div">
								<table id="tblPartners" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
										  <td class="text-center">Code</td>
										  <td class="text-center">Medicine Name</td>
										  <td class="text-center">PO No</td>
										  <td class="text-center">PO Qty</td>
										  <td class="text-center">Pending PO Qty</td>
										  <td class="text-center">Packing Type</td>
										  <td class="text-center">Quantity</td>
										  <td class="text-center">Convert Qty</td>
										  <td class="text-center">Rate Rs</td>
										  <td class="text-center">Value Rs</td>
										  <td class="text-center">GST Rate %</td>
										  <td class="text-center">GST Value Rs</td>
										  <td class="text-center">Total Rs</td>
										  <td class="text-center">Expiry Date</td>
										  <td class="text-center">Batch No</td>
										  <td class="text-center">Action</td>
										</tr>
									</thead>
									<tbody>
										<?php $extra_field = 1; ?>
										<?php $tab_index_1 = 7; ?>
										<?php if($productraw_datas){ ?>
											<?php foreach($productraw_datas as $ckey => $cvalue){ ?>
											<tr id="productraw_row<?php echo $extra_field; ?>">
												<td class="text-left">
													<?php echo $cvalue['productraw_code']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][productraw_code]" value="<?php echo $cvalue['productraw_code']; ?>" id="input-productraw_code<?php echo $extra_field; ?>" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['productraw_name']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][productraw_name]" value="<?php echo $cvalue['productraw_name']; ?>" placeholder="<?php echo 'Product Name'; ?>" id="input-productraw_name<?php echo $extra_field; ?>" class="form-control" />
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][productraw_id]" value="<?php echo $cvalue['productraw_id']; ?>" id="input-productraw_id<?php echo $extra_field; ?>" class="form-control" />
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][med_id]" value="<?php echo $cvalue['med_id']; ?>" id="input-med_id<?php echo $extra_field; ?>" class="form-control" />
												</td>
												<?php $tab_index_1 ++; ?>
												<td class="text-left">
													<?php echo $cvalue['po_data']; ?>
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][po_data]" value="<?php echo $cvalue['po_data']; ?>" placeholder="<?php echo 'Po Data'; ?>" id="input-po_data_<?php echo $extra_field; ?>" class="form-control po_data_class" />
													<!-- <select name="productraw_datas[<?php echo $extra_field ?>][po_data]" id="select-po_data_<?php echo $extra_field; ?>" class="form-control po_data_class"  data-index="3">
								                          <option value="<?php echo $cvalue['po_data']; ?>" selected="selected"></option>
								                    </select> -->
												</td>
												<td class="text-left">
													<?php echo $cvalue['po_qty']; ?>
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][po_qty]" value="<?php echo $cvalue['po_qty']; ?>" placeholder="<?php echo 'PO qty'; ?>" id="input-po_qty_<?php echo $extra_field; ?>" class="form-control po_qty_class" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['reduce_po_qty']; ?>
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][reduce_po_qty]" value="<?php echo $cvalue['reduce_po_qty']; ?>" placeholder="<?php echo 'Reduce PO qty'; ?>" id="input-reduce_po_qty_<?php echo $extra_field; ?>" class="form-control reduce_po_qty_class" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['packing']; ?> 
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][packing]" value="<?php echo $cvalue['packings']; ?>" placeholder="<?php echo 'packings'; ?>" id="input-packing_<?php echo $extra_field; ?>" class="form-control packing_class" />
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][volume]" value="<?php echo $cvalue['volume']; ?>" placeholder="<?php echo 'volume'; ?>" id="hidden-volume_<?php echo $extra_field; ?>" class="form-control volume_class" />
												</td>
												
												<td class="text-left">
													<?php echo $cvalue['quantity']; ?> 
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][quantity]" value="<?php echo $cvalue['quantity']; ?>" placeholder="<?php echo 'Quantity'; ?>" id="input-quantity_<?php echo $extra_field; ?>" class="form-control quantity_class" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['ordered_quantity']; ?> 
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][ordered_quantity]" value="<?php echo $cvalue['ordered_quantity']; ?>" placeholder="<?php echo 'ordered_quantity'; ?>" id="input-ordered_quantity_<?php echo $extra_field; ?>" class="form-control ordered_quantity_class" />
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][unit]" value="<?php echo $cvalue['unit']; ?>" placeholder="<?php echo 'unit'; ?>" id="input-unit_<?php echo $extra_field; ?>" class="form-control unit_class" />
													<!-- <input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][ordered_quantitys]" value="<?php echo $cvalue['ordered_quantitys']; ?>" placeholder="<?php echo 'ordered_quantitys'; ?>" id="input-ordered_quantitys_<?php echo $extra_field; ?>" class="form-control ordered_quantitys_class" /> -->
												</td>
												<td class="text-right">
													<?php echo $cvalue['purchase_price']; ?> 
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][purchase_price]" value="<?php echo $cvalue['purchase_price']; ?>" placeholder="<?php echo 'Purchase Price'; ?>" id="input-purchase_price_<?php echo $extra_field; ?>" class="form-control purchase_price_class" />
												</td>
												<td class="text-right">
													<?php echo $cvalue['value']; ?> 
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][value]" value="<?php echo $cvalue['value']; ?>" placeholder="<?php echo 'value'; ?>" id="input-value_<?php echo $extra_field; ?>" class="form-control value_class" />
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][value]" value="<?php echo $cvalue['value']; ?>" placeholder="<?php echo 'value'; ?>" id="input-gst_values<?php echo $extra_field; ?>" class="form-control gst_rate_class" />
													
												</td>
												<td class="text-right">
													<?php echo $cvalue['gst_rate']; ?> 
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][gst_rate]" value="<?php echo $cvalue['gst_rate']; ?>" placeholder="<?php echo 'gst_rate'; ?>" id="input-gst_rate_<?php echo $extra_field; ?>" class="form-control gst_rate_class" />
													
												</td>
												<td class="text-right">
													<?php echo $cvalue['gst_value']; ?> 
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][gst_value]" value="<?php echo $cvalue['gst_value']; ?>" placeholder="<?php echo 'gst_value'; ?>" id="input-gst_value_<?php echo $extra_field; ?>" class="form-control gst_value_class" />
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][gst_value]" value="<?php echo $cvalue['gst_value']; ?>" placeholder="<?php echo 'Purchase Price'; ?>" id="input-values<?php echo $extra_field; ?>" class="form-control purchase_price_class" />
												</td>
												<td class="text-right">
													<?php echo $cvalue['total']; ?> 
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][total]" value="<?php echo $cvalue['total']; ?>" placeholder="<?php echo 'total'; ?>" id="input-total_<?php echo $extra_field; ?>" class="form-control total_class" />
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][total]" value="<?php echo $cvalue['total']; ?>" id="input-totals<?php echo $extra_field; ?>" class="form-control purchase_price_class" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['expiry_date']; ?> 
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][expiry_date]" value="<?php echo $cvalue['expiry_date']; ?>" placeholder="<?php echo 'Expiry Date'; ?>" id="input-expiry_date_<?php echo $extra_field; ?>" class="form-control expiry_date_class" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['batch_no']; ?> 
													<input tabindex="-1" type="hidden" name="productraw_datas[<?php echo $extra_field ?>][batch_no]" value="<?php echo $cvalue['batch_no']; ?>" placeholder="<?php echo 'Batch No'; ?>" id="input-batch_no_<?php echo $extra_field; ?>" class="form-control batch_no_class" />
												</td>

												<td class="text-left">
													<button onclick="remove_folder('<?php echo $extra_field; ?>')" class="btn btn-danger" id="remove<?php echo $extra_field; ?>" ><i class="fa fa-minus-circle"></i></button>
												</td>
											</tr>
											<?php $extra_field ++; ?>
											<?php } ?>
										<?php } ?>
										<input type="hidden" id="extra_field" name="extra_field" value="<?php echo $extra_field; ?>" />
										<input type="hidden" id="tab_index_1" name="tab_index_1" value="<?php echo $tab_index_1; ?>" />
									</tbody>
									<thead>
										<tr>
										  <td></td>
										  <td></td>
										  <td></td>
										  <td></td>
										  <td></td>
										  <td></td>
										  <td></td>
										  <td></td>
										  <td class="text-center">Grand Total</td>
										  <td class="text-right"><input style="text-align: right;width: 65px;border: none;outline: none;cursor: pointer;" readonly="readonly" type="text" name="final_val" value="<?php echo $value_total; ?>" id="input-final_val"/></td>
										  <td></td>
										  <td class="text-right"><input style="text-align: right;width: 65px;border: none;outline: none;cursor: pointer;" readonly="readonly" type="text" name="gst_val" value="<?php echo $gst_value_total; ?>" id="input-final_gst_val"/></td>
										  <td class="text-right"><input style="text-align: right;width: 65px;border: none;outline: none;cursor: pointer;" readonly="readonly" type="text" name="total" value="<?php echo $all_total; ?>" id="input-final_total"/></td>
										  <td></td>
										  <td></td>
										  <td></td>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<script type="text/javascript">
$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY',
});

$(document).ready(function() {
    $('#input-supplier').focus();
});

  $("#input-quantity").keyup(function()  {
  	var input_valid =  $('#input-quantity').val() ;
  	var input_valid1 = input_valid.replace(/[^0-9]+/i, '');
  	$("#input-quantity").val(input_valid1);
  	var price =  $('#input-purchase_price').val();
  	var reduce_po_qty =  $('#hidden-reduce_po_qty').val() ;
  	var value = input_valid1 * price;
  
  	var reduce = parseFloat(reduce_po_qty) - Number(input_valid1); //reduce_po_qty - input_valid1;

  	//console.log(value);
  	$('#input-Value').val('');
  	//alert(value.toFixed(2));
	$('#input-Values').val(value.toFixed(2));
	$('#input-reduce_po_qty').val(reduce || '');

	var unit =  $('#input-unit').val() ;
  	var qty = $('#input-quantity').val() ;
  	var packing_type = $('#input-packing').val();
	if ((unit == 'litre') || (unit == 'gm')) {
		final_qty = (qty.replace(/[^0-9]+/i, '')) * 1000;
	} else {
		final_qty = qty.replace(/[^0-9]+/i, '');
	}
  	var volume = $('#hidden-volume').val();
  	//alert(final_qty);
  	//alert(volume);
  	var order_qty = final_qty * volume;
  	var units = $('#input-unit').val();
  	if ((units == 'litre') || (units == 'ml')) {
  		final_unit = 'ML';
  	} else if ((units == 'gm') || (units == 'Kg')) {
  		final_unit = 'KG';
  	} else {
  		final_unit = '';
  	}

  	$('#input-ordered_quantity').val('');
	$('#input-ordered_quantity').val(order_qty+' '+final_unit);

	$('#input-ordered_quantitys').val('');
	$('#input-ordered_quantitys').val(order_qty);
  	
  	$('#input-purchase_value').val('');
	$('#input-purchase_value').val(value.toFixed(2));
	$('#input-values').val('');
	$('#input-values').val(value+' Rs / '+packing_type);
	var value1 = $('#input-purchase_value').val();
  	var gst = $('#input-gst_rate').val();
  	var gst_rate = (value1 * gst)/100;
	$('#input-gst_value').val('');
	$('#input-gst_value').val(gst_rate);
	var total = parseFloat(value1) + Number(gst_rate);
	$('#input-total').val(total);

	$('#auto_order').html('');
	auto_order ='';
	auto_order +='<label style="font-weight: 400;" class="text-left">'+order_qty+' '+final_unit+'</label>';
	$('#auto_order').append(auto_order);

	$('#input-values').html('');
	auto_purchase_value ='';
	auto_purchase_value +='<label style="font-weight: 400;" class="text-left">'+' '+value1+'</label>';
	$('#input-values').append(auto_purchase_value);

	$('#auto_gst_rate').html('');
	auto_gst_rate ='';
	auto_gst_rate +='<label style="font-weight: 400;" class="text-left">'+gst+''+'</label>';
	$('#auto_gst_rate').append(auto_gst_rate);

	$('#auto_gst_value').html('');
	auto_gst_value ='';
	auto_gst_value +='<label style="font-weight: 400;" class="text-left">'+' '+gst_rate+'</label>';
	$('#auto_gst_value').append(auto_gst_value);

	$('#auto_total').html('');
	auto_total ='';
	auto_total +='<label style="font-weight: 400;" class="text-left">'+' '+total+'</label>';
	$('#auto_total').append(auto_total);
});

$("#input-purchase_price").keyup(function()  {
  	var input_valid =  $('#input-purchase_price').val() ;
  	//var input_valid1 = input_valid.replace(/[^0-9]+/i, '');
  	$("#input-purchase_price").val(input_valid);
  	var qty =  $('#input-quantity').val();
  	var value = input_valid * qty;
  	
  	//console.log(value);
  	$('#input-Value').val('');
	$('#input-Values').val(value.toFixed(2));

	var unit =  $('#input-unit').val() ;
  	var qty = $('#input-quantity').val() ;
  	var packing_type = $('#input-packing').val();
	if ((unit == 'litre') || (unit == 'gm')) {
		final_qty = (qty.replace(/[^0-9]+/i, '')) * 1000;
	} else {
		final_qty = qty.replace(/[^0-9]+/i, '');
	}
  	var volume = $('#hidden-volume').val();
  	//alert(final_qty);
  	//alert(volume);
  	var order_qty = final_qty * volume;
  	var units = $('#input-unit').val();
  	if ((units == 'litre') || (units == 'ml')) {
  		final_unit = 'ML';
  	} else if ((units == 'gm') || (units == 'Kg')) {
  		final_unit = 'KG';
  	} else {
  		final_unit = '';
  	}

  	$('#input-ordered_quantity').val('');
	$('#input-ordered_quantity').val(order_qty+' '+final_unit);

	$('#input-ordered_quantitys').val('');
	$('#input-ordered_quantitys').val(order_qty);
  	
  	$('#input-purchase_value').val('');
	$('#input-purchase_value').val(value.toFixed(2));
	$('#input-values').val('');
	$('#input-values').val(value+' Rs / '+packing_type);
	var value1 = $('#input-purchase_value').val();
  	var gst = $('#input-gst_rate').val();
  	var gst_rate = (value1 * gst)/100;
	$('#input-gst_value').val('');
	$('#input-gst_value').val(gst_rate);
	var total = parseFloat(value1) + Number(gst_rate);
	$('#input-total').val(total);

	$('#auto_order').html('');
	auto_order ='';
	auto_order +='<label style="font-weight: 400;" class="text-left">'+order_qty+' '+final_unit+'</label>';
	$('#auto_order').append(auto_order);

	$('#input-values').html('');
	auto_purchase_value ='';
	auto_purchase_value +='<label style="font-weight: 400;" class="text-left">'+' '+value1+'</label>';
	$('#input-values').append(auto_purchase_value);

	$('#auto_gst_rate').html('');
	auto_gst_rate ='';
	auto_gst_rate +='<label style="font-weight: 400;" class="text-left">'+gst+''+'</label>';
	$('#auto_gst_rate').append(auto_gst_rate);

	$('#auto_gst_value').html('');
	auto_gst_value ='';
	auto_gst_value +='<label style="font-weight: 400;" class="text-left">'+' '+gst_rate+'</label>';
	$('#auto_gst_value').append(auto_gst_value);

	$('#auto_total').html('');
	auto_total ='';
	auto_total +='<label style="font-weight: 400;" class="text-left">'+' '+total+'</label>';
	$('#auto_total').append(auto_total);
	
});


$(document).on('keypress','input,select,textarea', function (e) {
//$('input,select').on('keypress', function (e) {
	if (e.which == 13) {
		e.preventDefault();
		var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
		if (!$next.length) {
			$next = $('[tabIndex=1]');
		}
		$next.focus();
	}
});

var extra_field = $('#extra_field').val();
var tab_index_1 = $('#tab_index_1').val();
function add_productraw(item){
	$('#input-datas').val(1);
	$('#input-added').val(1);
		html = '';
	  html += '<tr id="productraw_row' + extra_field + '">';
	  	html += '<td id="input-productraw_code_label'+extra_field+'" class="text-left">';
			
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][productraw_code]" value="" id="input-productraw_codes'+extra_field+'" class="form-control" />';
		html += '</td>';

		html += '<td id="input-productraw_name_label'+extra_field+'" class="text-left">';
			
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][productraw_name]" value="'+item['id']+'" id="input-productraw_names'+extra_field+'" class="form-control" />';
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][med_id]" value="" id="input-med_id'+extra_field+'" class="form-control" />';
		html += '</td>';
		
		tab_index_1 ++;
		html += '<td id="input-select-po_data_label'+extra_field+'" class="text-left">';
			
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][po_data]" value="'+item['po_data']+'" id="select-po_datas_'+extra_field+'" class="form-control po_data_class" />';
		html += '</td>';

		html += '<td id="input-po_qty_label'+extra_field+'" class="text-right">';
			
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][po_qty]" value="'+item['po_qty']+'" id="input-po_qtys_'+extra_field+'" class="form-control po_qty_class" />';
		html += '</td>';

		html += '<td id="input-reduce_po_qty_label'+extra_field+'" class="text-right">';
			
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][reduce_po_qty]" value="'+item['reduce_po_qty']+'" id="input-reduce_po_qtys_'+extra_field+'" class="form-control reduce_po_qty_class" />';
		html += '</td>';

		html += '<td id="input-packing_label'+extra_field+'" class="text-left">';
			
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][packing]" value="'+item['packing']+'" id="input-packings_'+extra_field+'" class="form-control packing_class" />';
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][volume]" value="'+item['volume']+'" id="input-volumes_'+extra_field+'" class="form-control volume_class" />';
		html += '</td>';

		html += '<td id="input-quantity_label'+extra_field+'" class="text-right">';
			
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][quantity]" value="'+item['quantity']+'" id="input-quantitys_'+extra_field+'" class="form-control quantity_class" />';
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][unit]" value="'+item['unit']+'" id="input-units_'+extra_field+'" class="form-control unit_class" />';

		html += '<td id="input-ordered_quantity_label'+extra_field+'" class="text-left">';
			
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][ordered_quantity]" value="'+item['ordered_quantity']+'" id="input-ordered_quantitys_'+extra_field+'" class="form-control ordered_quantity_class" />';
		html += '</td>';
		
		html += '<td id="input-purchase_price_label'+extra_field+'"  class="text-left">';
			
			html += '<input tabindex="-1" type="hidden" name="productraw_datas['+extra_field+'][purchase_price]" value="" id="input-purchase_prices_'+extra_field+'" class="form-control purchase_price_class" />'; 
		html += '</td>';

		html += '<td id="input-value_label'+extra_field+'" class="text-right">';
			
			html += '<input tabindex="-1" type="hidden" name="productraw_datas['+extra_field+'][value]" value="" id="input-values_'+extra_field+'" class="form-control value_class" />'; 
		html += '</td>';

		html += '<td id="input-gst_rate_label'+extra_field+'" class="text-right">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][gst_rate]" value="" id="input-gst_rates_'+extra_field+'" class="form-control" />';
		html += '</td>';
		
		html += '<td id="input-gst_value_label'+extra_field+'" class="text-right">';
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][gst_value]" value="" id="input-gst_values'+extra_field+'" class="form-control" />';
		html += '</td>';
		
		html += '<td id="input-total_label'+extra_field+'" class="text-right">';
			html += '<input type="hidden" name="productraw_datas['+extra_field+'][total]" value="" id="input-totals'+extra_field+'" class="form-control" />';
		html += '</td>';



		html += '<td id="input-expiry_date_label'+extra_field+'" class="text-left">';
			
			html += '<input tabindex="-1" type="hidden" name="productraw_datas['+extra_field+'][expiry_date]" value="" id="input-expiry_dates_'+extra_field+'" class="form-control expiry_date_class" />'; 
		html += '</td>';

		html += '<td id="input-batch_no_label'+extra_field+'" class="text-left">';
			
			html += '<input tabindex="-1" type="hidden" name="productraw_datas['+extra_field+'][batch_no]" value="" id="input-batch_nos_'+extra_field+'" class="form-control batch_no_class" />'; 
		html += '</td>';

		html += '<td class="text-left"><button onclick="remove_folder('+extra_field+')" class="btn btn-danger" id="remove'+extra_field+'" ><i class="fa fa-minus-circle"></i></button></td>';
	  html += '</tr>';
	  $('#tblPartners tbody').append(html);
	  	
	  	var po_data = $("#select-po_data").val();
	  	var po_qty = $("#input-po_qty").val();
	  	var reduce_po_qty = $("#input-reduce_po_qty").val();
		var packing = $("#input-packing").val();
		var volume = $("#hidden-volume").val();
	  	var price = $("#input-purchase_price").val();
	  	var qty = $('#input-quantity').val();
	  	var unit = $('#input-unit').val();
		var code = $("#input-productnew_code").val();
		var name = $("#input-productnew").val();
		var med_id = $("#input-med_id").val();
		var ordered_quantity = $("#input-ordered_quantity").val();
		var ordered_quantitys = $("#input-ordered_quantitys").val();
		var value = price * qty;
		var gst_rate = $("#input-gst_rate").val();
		var gst_value = $("#input-gst_value").val();
		var total = $("#input-total").val();
		var expiry_date = $("#input-expiry_date").val();
		var batch_no = $("#input-batch_no").val();
		//alert(qty);
		//alert(code);
		//alert(name);
		$('#input-value_'+extra_field+'').val('');
		$('#input-Value').val('');
		//$('#input-Values').val(value);
		//$('#input-Values').val(value);

		$('#input-value_'+extra_field+'').val(value.toFixed(2));
		$('#input-values_'+extra_field+'').val(value.toFixed(2));
		$('#input-purchase_price_'+extra_field+'').val(price);
		$('#input-purchase_prices_'+extra_field+'').val(price);
		$('#select-po_data_'+extra_field+'').val(po_data);
		$('#select-po_datas_'+extra_field+'').val(po_data);
		$('#input-po_qty_'+extra_field+'').val(po_qty);
		$('#input-po_qtys_'+extra_field+'').val(po_qty);

		$('#input-gst_rates_'+extra_field+'').val(gst_rate);
		$('#input-gst_values_'+extra_field+'').val(gst_value);
		$('#input-gst_values'+extra_field+'').val(gst_value);
		$('#input-totals'+extra_field+'').val(total);

		$('#input-reduce_po_qty_'+extra_field+'').val(reduce_po_qty);
		$('#input-reduce_po_qtys_'+extra_field+'').val(reduce_po_qty);
		$('#input-quantity_'+extra_field+'').val(qty);
		$('#input-quantitys_'+extra_field+'').val(qty);
		$('#input-unit_'+extra_field+'').val(unit);
		$('#input-units_'+extra_field+'').val(unit);
		$('#input-ordered_quantity_'+extra_field+'').val(ordered_quantity);
		$('#input-ordered_quantitys_'+extra_field+'').val(ordered_quantitys);
		$('#input-productraw_code'+extra_field+'').val(code);
		$('#input-productraw_codes'+extra_field+'').val(code);
		$('#input-productraw_name'+extra_field+'').val(name);
		$('#input-med_id'+extra_field+'').val(med_id);
		$('#input-productraw_names'+extra_field+'').val(name);
		$('#input-expiry_date_'+extra_field+'').val(expiry_date);
		$('#input-expiry_dates_'+extra_field+'').val(expiry_date);
		$('#input-batch_no_'+extra_field+'').val(batch_no);
		$('#input-batch_nos_'+extra_field+'').val(batch_no);
		$('#input-packing_'+extra_field+'').val(packing);
		$('#input-packings_'+extra_field+'').val(packing);
		$('#input-volume_'+extra_field+'').val(volume);
		$('#input-volumes_'+extra_field+'').val(volume);

		productraw_code_label ='';
		productraw_code_label +='<label style="text-transform: uppercase;font-weight: 400;" class="text-left">'+code+'</label>';
		$('#input-productraw_code_label'+extra_field+'').append(productraw_code_label);

		productraw_name_label ='';
		productraw_name_label +='<label style="font-weight: 400;" class="text-left">'+name+'</label>';
		$('#input-productraw_name_label'+extra_field+'').append(productraw_name_label);

		select_po_data_label ='';
		select_po_data_label +='<label style="font-weight: 400;" class="text-left">'+po_data+'</label>';
		$('#input-select-po_data_label'+extra_field+'').append(select_po_data_label);

		packing_label ='';
		packing_label +='<label style="font-weight: 400;" class="text-left">'+packing+' '+volume+' '+unit+'</label>';
		$('#input-packing_label'+extra_field+'').append(packing_label);

		po_qty_label ='';
		po_qty_label +='<label style="font-weight: 400;" class="text-right">'+po_qty+'</label>';
		$('#input-po_qty_label'+extra_field+'').append(po_qty_label);

		reduce_po_qty_label ='';
		reduce_po_qty_label +='<label style="font-weight: 400;" class="text-right">'+reduce_po_qty+'</label>';
		$('#input-reduce_po_qty_label'+extra_field+'').append(reduce_po_qty_label);

		quantity_label ='';
		quantity_label +='<label style="font-weight: 400;" class="text-right">'+qty+'</label>';
		$('#input-quantity_label'+extra_field+'').append(quantity_label);

		label_ordered_quantity ='';
		label_ordered_quantity +='<label style="font-weight: 400;" class="text-right">'+ordered_quantity+'</label>';
		$('#input-ordered_quantity_label'+extra_field+'').append(label_ordered_quantity);

		// purchase_price_label ='';
		// purchase_price_label +='<label style="font-weight: 400;" class="text-left">'+price+'</label>';
		// $('#input-purchase_price_label'+extra_field+'').append(purchase_price_label);

		gst_rate_label ='';
		gst_rate_label +='<label style="font-weight: 400;" class="text-right">'+gst_rate+''+'</label>';
		$('#input-gst_rate_label'+extra_field+'').append(gst_rate_label);

		gst_value_label ='';
		gst_value_label +='<label style="font-weight: 400;" class="text-right">'+' '+gst_value+'</label>';
		$('#input-gst_value_label'+extra_field+'').append(gst_value_label);

		purchase_price_label ='';
		purchase_price_label +='<label style="font-weight: 400;" class="text-left">'+' '+price+' '+packing+'</label>';
		$('#input-purchase_price_label'+extra_field+'').append(purchase_price_label);

		value_label ='';
		value_label +='<label style="font-weight: 400;" class="text-right">'+value.toFixed(2)+'</label>';
		$('#input-value_label'+extra_field+'').append(value_label);

		expiry_date_label ='';
		expiry_date_label +='<label style="font-weight: 400;" class="text-left">'+expiry_date+'</label>';
		$('#input-expiry_date_label'+extra_field+'').append(expiry_date_label);

		batch_no_label ='';
		batch_no_label +='<label style="font-weight: 400;" class="text-left">'+batch_no+'</label>';
		$('#input-batch_no_label'+extra_field+'').append(batch_no_label);

		total_label ='';
		total_label +='<label style="font-weight: 400;" class="text-left">'+' '+total+'</label>';
		$('#input-total_label'+extra_field+'').append(total_label);

		$('#input-productnew_code').val('');
		$('#input-productnew').val('');
		$('#input-med_id').val('');
		$("#select-po_data").find('option').remove();
		$("#input-po_qty").val('');
		$("#input-reduce_po_qty").val('');
		$('#input-quantity').val('');
		$('#input-unit').val('');
		$('#input-packing').val('');
		$('#hidden-volume').val('');
		$('#input-ordered_quantity').val('');
		$('#input-purchase_price').val('');
		$("#input-expiry_date").val('');
		$("#input-batch_no").val('');
		$("#input-Values").val('');
		$('#input-gst_rate').val('');
		$('#input-gst_value').val('');
		$('#autopacking').html('');
		$('#auto_gst_rate').html('');
		$('#auto_gst_value').html('');
		$('#auto_total').html('');
		$('#auto_prev_rate').html('');
		//$('#input-productnew').focus();
	  extra_field++;

}

function remove_folder(extra_field){
	var final_total = $('#input-final_total').val();
	var purchase_price = $('#input-totals'+extra_field+'').val();
	var final_totals = parseFloat(final_total) - Number(purchase_price);
	$('#input-final_total').val(final_totals.toFixed(2));
	$('#input-totals').val(final_totals.toFixed(2));
	
	var final_val = $('#input-final_gst_val').val();
	var purchase_val = $('#input-values'+extra_field+'').val();
	var final_vals = parseFloat(final_val) - Number(purchase_val);
	$('#input-final_gst_val').val(final_vals.toFixed(2));
	$('#input-vals').val(final_vals.toFixed(2));
	

	var final_gst_val = $('#input-final_val').val();
	var gst_purchase_val = $('#input-gst_values'+extra_field+'').val();
	var final_gst_val = parseInt(final_gst_val) - parseInt(gst_purchase_val);
	$('#input-final_val').val(final_gst_val.toFixed(2));
	$('#input-gst_val').val(final_gst_val.toFixed(2));

	$('#productraw_row'+extra_field).remove();
}

// function remove_folder(extra_field){
//   $('#productraw_row'+extra_field).remove();
// }

document.getElementById("input-productnew_code").onkeypress = function(event){
	//alert(qty);
	//return false;
    if (event.keyCode == 13 || event.which == 13){
    	var code = $('#input-productnew_code').val();
    	//alert(code);
    	if (code != '') {
	    	var med_code = $('#input-productnew_code').val();
	        $.ajax({
				url: 'index.php?route=catalog/inward/autocomplete_raw_code&token=<?php echo $token; ?>'+'&med_code='+med_code,
				type: 'post', 
				dataType: 'json',
				success: function(json) {

					//console.log('inn');
					console.log(json.length);
					if (json.length != 0) {

						var code = $('#input-productnew_code').val();
						if (code != '0') {
							$('#input-productnew').val(json.med_name);
							$('#input-med_id').val(json.id);
							$('#input-quantity').val(json.quantity);
							$('#input-gst_rate').val(json.gst_rate);
							$('#input-gst_rates').val(json.gst_rate+'%');
							$('#input-purchase_price').val(json.price);
							$('#input-prev_rate').val(json.prev_rate);
							$('#input-unit').val(json.unit);
							$('#input-packing').val(json.packing);
							$('#hidden-volume').val(json.volume);
							$('#autopacking').html('');
							$('#auto_purchase_price').html('');
							$('#auto_prev_rate').html('');

							packing ='';
							packing +='<label style="font-weight: 400;" class="text-right">'+json.packing+' '+json.volume+' '+json.unit+'</label>';
							$('#autopacking').append(packing);
							auto_prev_rate ='';
							auto_prev_rate +='<label style="font-weight: 400;" class="text-right">'+'Rs '+json.prev_rate+'</label>';
							$('#auto_prev_rate').append(auto_prev_rate);
							auto_purchase_price ='';
							auto_purchase_price +='<label style="font-weight: 400;" class="text-right">'+' '+json.price+' / '+json.packing+'</label>';
							$('#input-purchase_price').append(auto_purchase_price);
								var med_name = $('#input-productnew').val();
								$('#select-po_data').focus();
								//alert(med_name);
							$.ajax({
								url: 'index.php?route=catalog/inward/PoDatas&token=<?php echo $token; ?>'+'&med_name='+med_name,
								type: 'post', 
								dataType: 'json',
								success: function(json) {
									console.log(json);
									//$('#select-po_data').val(json.po_no);
									$('#input-po_qty').val(json.qty);
									$('#select-po_data').find('option').remove();
									if (json != '') {
										$.each(json, function (i, item) {
										    console.log(item.qty);
										    $('#select-po_data').append($('<option>', { 
										        value: item.po_no,
										        text : item.po_no
										    }));
									    });
									} else{
										$('#select-po_data').append($('<option>', { 
									        value: '',
									        text : ''
									    }));
									}
								    
							    	document.getElementById("select-po_data").onkeypress = function(event){
							    		if (event.keyCode == 13 || event.which == 13){
								    		po_num =  $('#select-po_data').val();
							    			if (po_num != '') {
								    			$('#input-quantity').focus();
							    			}
							    		}
							    	};
									document.getElementById("input-quantity").onkeypress = function(event){
									    if (event.keyCode == 13 || event.which == 13){
									    	var qty = $('#input-quantity').val();
									    	if (qty != '') {
									    		if (qty > 0) {

										    		$('#input-purchase_price').focus();
										    		document.getElementById("input-purchase_price").onkeypress = function(event){
											    		if (event.keyCode == 13 || event.which == 13){
											    			var purchase_price = $('#input-purchase_price').val();
											    			if (purchase_price == '') {
											    				alert('Please Enter Rate!');
											    				return false;
											    			} else if (purchase_price <= 0) {
											    				alert('Please Enter Rate!');
											    				return false;
											    			} else {
												    			$('#input-expiry_date').focus();
											    			}
											    			document.getElementById("input-expiry_date").onkeypress = function(event){
													    		if (event.keyCode == 13 || event.which == 13){
													    			var expiry_date = $('#input-expiry_date').val();
											    					var expiry_dates = expiry_date.split('-');
											    					var dd = expiry_dates[2];
																	var mm = expiry_dates[1];
																	var yyyy = expiry_dates[0];
														    		var expiry_date = dd + "-" + mm + "-" + yyyy;
											    					var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
											    					if (!(date_regex.test(expiry_date))) {
																		$('#error_expiry_date').show();
																		return false;
																	} else {
																		$('#error_expiry_date').hide();
														    			$('#input-batch_no').focus();
																	}
														    		document.getElementById("input-batch_no").onkeypress = function(event){
													    				if (event.keyCode == 13 || event.which == 13){
													    					var po_no = $('#select-po_data').val();
													    					var qutys = $('#input-quantity').val();
													    					var pratesz = $('#input-purchase_price').val();
													    					if (po_no == '') {
													    						alert("Please Select Po No!");
													    					} else if (qutys <= 0) {
													    						alert("Please Enter Valid Quantity!");
													    					} else if (pratesz <= 0) {
													    						alert("Please Enter Valid Rate!");
													    					} else {
														    					var expiry_date_val = $('#input-expiry_date').val();
														    					var expiry_dates = expiry_date_val.split('-');
														    					var dd = expiry_dates[2];
																				var mm = expiry_dates[1];
																				var yyyy = expiry_dates[0];
																	    		var expiry_date = dd + "-" + mm + "-" + yyyy;
														    					var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
														    					if (!(date_regex.test(expiry_date))) {
																					$('#error_expiry_date').show();
																					return false;
																				} else {
																					$('#error_expiry_date').hide();
																				}
														    					var final_total = $('#input-totals').val();
																				var tots = $('#input-total').val();
																				var final_totals = parseFloat(final_total) + Number(tots);
																				$('#input-totals').val(final_totals);
																				$('#input-final_total').val(final_totals);

																				var final_val = $('#input-vals').val();
																				var vals = $('#input-Values').val();
																				var final_vals = parseFloat(final_val) + Number(vals);
																				$('#input-vals').val(final_vals);
																				$('#input-final_val').val(final_vals.toFixed(2));

																				var final_gst_val = $('#input-gst_val').val();
																				var gst_val = $('#input-gst_value').val();
																				var final_gst_vals = parseFloat(final_gst_val) + Number(gst_val);
																				$('#input-gst_val').val(final_gst_vals);
																				$('#input-final_gst_val').val(final_gst_vals);


																    			var batch_no = $('#input-batch_no').val();
																    			$('#input-productnew_code').focus();
																        		add_productraw(json)
																        	}
															        	}
															        };
															    }
									    					
											    			}
											        	}
											        };
									    		} else {
													alert('Please Enter Quantity!');
													return false;
												}
									    	} else {
												alert('Please Enter Quantity!');
												return false;
											}
									    }
									};
								}
							});

						} else {
							alert('This Medicine Is Not Available!');
							return false;
						}
					} else {
						alert('This Medicine Is Not Available!');
						return false;
					}
				}
			});
    	} else {
    		$('#input-productnew').focus();
    	}
    }
};

function getpoNo(po){
	console.log('innnnnn');
	console.log(po);
	$.ajax({
		url: 'index.php?route=catalog/inward/PoQty&token=<?php echo $token; ?>'+'&po='+po,
		type: 'post', 
		dataType: 'json',
		success: function(json) {
			console.log(json);
			$('#input-po_qty').val(json.qty);
			$('#input-reduce_po_qty').val(json.reduce_po_qty);
			$('#hidden-reduce_po_qty').val(json.reduce_po_qty);


		}
	});
}
function add_productraws(json){
	$('#input-datas').val(1);
	$('#input-added').val(1);
	//alert('innnnn');
	console.log('inn');
	console.log(json);
	html = '';
  html += '<tr id="productraw_row' + extra_field + '">';
  	html += '<td id="input-productraw_code_label'+extra_field+'" style="background: white;" class="text-left">';
		
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][productraw_code]" value="" id="input-productraw_codes'+extra_field+'" class="form-control" />';
	html += '</td>';

	html += '<td  id="input-productraw_name_label'+extra_field+'" style="background: white;" class="text-left">';
		
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][productraw_name]" value="" id="input-productraw_names'+extra_field+'" class="form-control" />';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][med_id]" value="" id="input-med_id'+extra_field+'" class="form-control" />';
	html += '</td>';
	
	tab_index_1 ++;
	html += '<td id="input-select-po_data_label'+extra_field+'"  style="background: white;" class="text-left">';
		
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][po_data]" value="" id="select-po_datas_'+extra_field+'" class="form-control po_data_class" />';
	html += '</td>';



	html += '<td  id="input-po_qty_label'+extra_field+'" style="background: white;" class="text-right">';
		
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][po_qty]" value="" id="input-po_qtys_'+extra_field+'" class="form-control po_qty_class" />';
	html += '</td>';

	html += '<td id="input-reduce_po_qty_label'+extra_field+'"  style="background: white;" class="text-right">';
		
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][reduce_po_qty]" value="" id="input-reduce_po_qtys_'+extra_field+'" class="form-control reduce_po_qty_class" />';
	html += '</td>';

	html += '<td id="input-packing_label'+extra_field+'"  style="background: white;" class="text-left">';
			
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][packing]" value="" id="input-packings_'+extra_field+'" class="form-control packing_class" />';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][volume]" value="" id="input-volumes_'+extra_field+'" class="form-control volume_class" />';
	html += '</td>';

	html += '<td id="input-quantity_label'+extra_field+'" style="background: white;" class="text-right">';
		
		html += '<input tabindex="-1" type="hidden" name="productraw_datas['+extra_field+'][quantity]" value="" id="input-quantitys_'+extra_field+'" class="form-control quantity_class" />'; 
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][unit]" value="" id="input-units_'+extra_field+'" class="form-control unit_class" />';
	html += '</td>';


	html += '<td id="input-ordered_quantity_label'+extra_field+'"  style="background: white;" class="text-right">';
		
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][ordered_quantity]" value="" id="input-ordered_quantitys_'+extra_field+'" class="form-control ordered_quantity_class" />';
	html += '</td>';

	html += '<td id="input-purchase_price_label'+extra_field+'" style="background: white;" class="text-left">';
		
		html += '<input tabindex="-1" type="hidden" name="productraw_datas['+extra_field+'][purchase_price]" value="" id="input-purchase_prices_'+extra_field+'" class="form-control purchase_price_class" />'; 
	html += '</td>';

	html += '<td id="input-value_label'+extra_field+'" style="background: white;" class="text-right">';
		
		html += '<input tabindex="-1" type="hidden" name="productraw_datas['+extra_field+'][value]" value="" id="input-values_'+extra_field+'" class="form-control value_class" />'; 
	html += '</td>';

	html += '<td id="input-gst_rate_label'+extra_field+'"  style="background: white;" class="text-right">';
	html += '<input type="hidden" name="productraw_datas['+extra_field+'][gst_rate]" value="" id="input-gst_rates_'+extra_field+'" class="form-control" />';
	html += '</td>';
	
	html += '<td id="input-gst_value_label'+extra_field+'"  style="background: white;" class="text-right">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][gst_value]" value="" id="input-gst_values'+extra_field+'" class="form-control" />';
	html += '</td>';
	
	html += '<td id="input-total_label'+extra_field+'"  style="background: white;" class="text-right">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][total]" value="" id="input-totals'+extra_field+'" class="form-control" />';
	html += '</td>';

	html += '<td id="input-expiry_date_label'+extra_field+'" style="background: white;" class="text-left">';
		
		html += '<input tabindex="-1" type="hidden" name="productraw_datas['+extra_field+'][expiry_date]" value="" id="input-expiry_dates_'+extra_field+'" class="form-control expiry_date_class" />'; 
	html += '</td>';

	html += '<td id="input-batch_no_label'+extra_field+'" style="background: white;" class="text-left">';
		
		html += '<input tabindex="-1" type="hidden" name="productraw_datas['+extra_field+'][batch_no]" value="" id="input-batch_nos_'+extra_field+'" class="form-control batch_no_class" />'; 
	html += '</td>';
	
	html += '<td class="text-left"><button onclick="remove_folder('+extra_field+')" class="btn btn-danger" id="remove'+extra_field+'" ><i class="fa fa-minus-circle"></i></button></td>';
  html += '</tr>';
  $('#tblPartners tbody').append(html);
  	var po_data = $("#select-po_data").val();
  	var po_qty = $("#input-po_qty").val();
  	var reduce_po_qty = $("#input-reduce_po_qty").val();
  	var packing = $("#input-packing").val();
	var volume = $("#hidden-volume").val();
  	var price = $("#input-purchase_price").val();
  	var qty = $('#input-quantity').val();
  	var unit = $('#input-unit').val();
  	var value = price * qty;
  	var gst_rate = $("#input-gst_rate").val();
	var gst_value = $("#input-gst_value").val();
	var total = $("#input-total").val();
	var code = $("#input-productnew_code").val();
	var name = $("#input-productnew").val();
	var med_id = $("#input-med_id").val();
	var ordered_quantity = $("#input-ordered_quantity").val();
	var ordered_quantitys = $("#input-ordered_quantitys").val();
	var expiry_date = $("#input-expiry_date").val();
	var batch_no = $("#input-batch_no").val();
	$('#input-purchase_price_'+extra_field+'').val(price);
	$('#input-purchase_prices_'+extra_field+'').val(price);
	$('#select-po_data_'+extra_field+'').val(po_data);
	$('#select-po_datas_'+extra_field+'').val(po_data);
	$('#input-po_qty_'+extra_field+'').val(po_qty);
	$('#input-po_qtys_'+extra_field+'').val(po_qty);
	$('#input-reduce_po_qty_'+extra_field+'').val(reduce_po_qty);
	$('#input-reduce_po_qtys_'+extra_field+'').val(reduce_po_qty);
	$('#input-quantity_'+extra_field+'').val(qty);
	$('#input-quantitys_'+extra_field+'').val(qty);
	$('#input-unit_'+extra_field+'').val(unit);
	$('#input-units_'+extra_field+'').val(unit);
	$('#input-productraw_code'+extra_field+'').val(code);
	$('#input-productraw_codes'+extra_field+'').val(code);
	$('#input-productraw_name'+extra_field+'').val(name);
	$('#input-productraw_names'+extra_field+'').val(name);
	$('#input-med_id'+extra_field+'').val(med_id);
	$('#input-value_'+extra_field+'').val(value.toFixed(2));
	$('#input-values_'+extra_field+'').val(value.toFixed(2));
	$('#input-expiry_date_'+extra_field+'').val(expiry_date);
	$('#input-expiry_dates_'+extra_field+'').val(expiry_date);
	$('#input-batch_no_'+extra_field+'').val(batch_no);
	$('#input-batch_nos_'+extra_field+'').val(batch_no);
	$('#input-packing_'+extra_field+'').val(packing);
	$('#input-packings_'+extra_field+'').val(packing);
	$('#input-volume_'+extra_field+'').val(volume);
	$('#input-volumes_'+extra_field+'').val(volume);
	$('#input-ordered_quantity_'+extra_field+'').val(ordered_quantity);
	$('#input-ordered_quantitys_'+extra_field+'').val(ordered_quantitys);

	$('#input-gst_rates_'+extra_field+'').val(gst_rate);
	$('#input-gst_values_'+extra_field+'').val(gst_value);
	$('#input-gst_values'+extra_field+'').val(gst_value);
	$('#input-totals'+extra_field+'').val(total);

	productraw_code_label ='';
	productraw_code_label +='<label style="font-weight: 400;" class="text-left">'+code+'</label>';
	$('#input-productraw_code_label'+extra_field+'').append(productraw_code_label);

	productraw_name_label ='';
	productraw_name_label +='<label style="font-weight: 400;" class="text-left">'+name+'</label>';
	$('#input-productraw_name_label'+extra_field+'').append(productraw_name_label);

	select_po_data_label ='';
	select_po_data_label +='<label style="font-weight: 400;" class="text-left">'+po_data+'</label>';
	$('#input-select-po_data_label'+extra_field+'').append(select_po_data_label);

	packing_label ='';
	packing_label +='<label style="font-weight: 400;" class="text-left">'+packing+' '+volume+' '+unit+'</label>';
	$('#input-packing_label'+extra_field+'').append(packing_label);

	po_qty_label ='';
	po_qty_label +='<label style="font-weight: 400;" class="text-right">'+po_qty+'</label>';
	$('#input-po_qty_label'+extra_field+'').append(po_qty_label);

	reduce_po_qty_label ='';
	reduce_po_qty_label +='<label style="font-weight: 400;" class="text-right">'+reduce_po_qty+'</label>';
	$('#input-reduce_po_qty_label'+extra_field+'').append(reduce_po_qty_label);

	quantity_label ='';
	quantity_label +='<label style="font-weight: 400;" class="text-right">'+qty+'</label>';
	$('#input-quantity_label'+extra_field+'').append(quantity_label);

	label_ordered_quantity ='';
	label_ordered_quantity +='<label style="font-weight: 400;" class="text-right">'+ordered_quantity+'</label>';
	$('#input-ordered_quantity_label'+extra_field+'').append(label_ordered_quantity);

	gst_rate_label ='';
	gst_rate_label +='<label style="font-weight: 400;" class="text-right">'+gst_rate+''+'</label>';
	$('#input-gst_rate_label'+extra_field+'').append(gst_rate_label);

	gst_value_label ='';
	gst_value_label +='<label style="font-weight: 400;" class="text-right">'+' '+gst_value+'</label>';
	$('#input-gst_value_label'+extra_field+'').append(gst_value_label);

	purchase_price_label ='';
	purchase_price_label +='<label style="font-weight: 400;" class="text-left">'+' '+price+' '+packing+'</label>';
	$('#input-purchase_price_label'+extra_field+'').append(purchase_price_label);

	value_label ='';
	value_label +='<label style="font-weight: 400;" class="text-right">'+value.toFixed(2)+'</label>';
	$('#input-value_label'+extra_field+'').append(value_label);

	expiry_date_label ='';
	expiry_date_label +='<label style="font-weight: 400;" class="text-left">'+expiry_date+'</label>';
	$('#input-expiry_date_label'+extra_field+'').append(expiry_date_label);

	batch_no_label ='';
	batch_no_label +='<label style="font-weight: 400;" class="text-left">'+batch_no+'</label>';
	$('#input-batch_no_label'+extra_field+'').append(batch_no_label);

	total_label ='';
	total_label +='<label style="font-weight: 400;" class="text-right">'+' '+total+'</label>';
	$('#input-total_label'+extra_field+'').append(total_label);

	
	$('#input-productnew_code').val('');
	$('#input-productnew').val('');
	$('#input-med_id').val('');
	$("#select-po_data").find('option').remove();
	$("#input-po_qty").val('');
	$("#input-reduce_po_qty").val('');
	$('#input-quantity').val('');
	$('#input-unit').val('');
	$('#input-packing').val('');
	$('#hidden-volume').val('');
	$('#input-ordered_quantity').val('');
	$('#input-purchase_price').val('');
	$("#input-expiry_date").val('');
	$("#input-batch_no").val('');
	$("#input-Values").val('');
	$('#autopacking').html('');
	$('#input-gst_rate').val('');
	$('#input-gst_value').val('');
	$('#auto_gst_rate').html('');
	$('#auto_gst_value').html('');
	$('#auto_total').html('');
	$('#auto_prev_rate').html('');
	//$('#input-productnew').focus();
  extra_field++;
}

$(document).on('keydown', '.form-control', '.custom-control-input', function(e) {
    var name = $(this).attr('name'); 
    var class_name = $(this).attr('class'); 
    var id = $(this).attr('id');
    console.log(name);
    console.log(class_name);
    console.log(id);
    var value = $(this).val();
    if(e.which == 13){
        if(id == 'input-date'){
            $('#input-indent_no').focus();
        }
        if(id == 'input-indent_no'){
            $('#input-productnew_code').focus();
        }
    }
});


$('input[name=\'salesorder_category\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['name'],
			value: item['category_id']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'salesorder_category\']').val(item['label']);
	$('input[name=\'salesorder_category_id\']').val(item['value']);
  }
});


$(document).on('focusout','.quantity_class', function (e) {

  idss = $(this).attr('id');
  s_id = idss.split('_');
  id = s_id[1];
  price = $('#input-price'+id).val();
  quantity = $(this).val();
  total = price * quantity;
  total = total.toFixed(2);
  $('#total'+id).text(total);
  $('#input-total'+id).val(total);

  grand_total = 0;
  $('.quantity_class').each(function( index ) {
	idss1 = $(this).attr('id');
	s_id1 = idss1.split('_');
	id1 = s_id1[1];
	price1 = $('#input-price'+id1).val();
	quantity1 = $('#input-quantity_'+id1).val();
	total1 = price1 * quantity1;
	total1 = total1.toFixed(2);
	grand_total = parseFloat(grand_total) + parseFloat(total1);
  });
  grand_total = grand_total.toFixed(2);
  $('#costprice').text(grand_total);
  $('#input-cost-price').val(grand_total);
});
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$('#input-supplier').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/inward/autocompletesupplier&token=<?php echo $token; ?>&supplier=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['vendor_name'],
			id: item['vendor_id'],
			value: item['vendor_name'],
		  }
		}));
	  }
	});
  },
  'select': function(item, ui) {
  	$('#input-supplier').val(ui.item.label);
	$('#input-supplier').val(ui.item.value);
	$('#input-checks').val(1);
	$('.dropdown-menu').hide();
	$('#input-productnew_code').focus();
  }

});
	
$('#input-productnew').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/inward/autocomplete_raw&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['med_name'],
			id: item['id'],
			med_code: item['med_code'],
			value: item['med_name'],
			prev_rate: item['prev_rate'],
		    quantity: item['quantity'],
		    price: item['price'],
		    store_unit: item['store_unit'],
		    unit: item['unit'],
		    packing: item['packing'],
		    volume: item['volume'],
		    gst_rate: item['gst_rate'],
		  }
		}));
	  }
	});
  },
  'select': function(item, ui) {

	$('#input-productnew_code').val(ui.item.med_code);
	$('#input-med_id').val(ui.item.id);
	$('#input-productnew').val(ui.item.value);
	$('#input-quantity').val(ui.item.quantity);
	$('#input-purchase_price').val(ui.item.price);
	$('#input-unit').val(ui.item.unit);
	$('#input-packing').val(ui.item.packing);
	$('#hidden-volume').val(ui.item.volume);
	$('#autopacking').html('');
	$('#input-gst_rate').val(ui.item.gst_rate);
	$('#input-gst_rates').val(ui.item.gst_rate+'%');
	$('#auto_purchase_price').html('');

	packing ='';
	packing +='<label style="font-weight: 400;" class="text-left">'+ui.item.packing+' '+ui.item.volume+' '+ui.item.unit+'</label>';
	$('#autopacking').append(packing);
	auto_prev_rate ='';
	auto_prev_rate +='<label style="font-weight: 400;" class="text-right">'+'Rs '+ui.item.prev_rate+'</label>';
	$('#auto_prev_rate').append(auto_prev_rate);
	auto_purchase_price ='';
	auto_purchase_price +='<label style="font-weight: 400;" class="text-left">'+' '+ui.item.price+' / '+ui.item.packing+'</label>';
	$('#input-purchase_price').append(auto_purchase_price);
	var med_name = $('#input-productnew').val();
	$('#select-po_data').focus();
	//alert(med_name);
	$.ajax({
		url: 'index.php?route=catalog/inward/PoDatas&token=<?php echo $token; ?>'+'&med_name='+med_name,
		type: 'post', 
		dataType: 'json',
		success: function(json) {
			console.log(json);
			/*$('#select-po_data').val(json.po_no);
			$('#input-po_qty').val(json.qty);*/
			$('#select-po_data').find('option').remove();
			if (json != '') {
				$.each(json, function (i, item) {
				    console.log(item.qty);
				    $('#select-po_data').append($('<option>', { 
				        value: item.po_no,
				        text : item.po_no
				    }));
			    });
			} else{
				$('#select-po_data').append($('<option>', { 
			        value: '',
			        text : ''
			    }));
			}
		    
	    	document.getElementById("select-po_data").onkeypress = function(event){
	    		if (event.keyCode == 13 || event.which == 13){
	    			po_num =  $('#select-po_data').val();
	    	// 		if (json.length == 0) {
	    	// 			alert('Please Enter Po Number!');
						// return false;
	    	// 		} else{
	    				$('#input-quantity').focus();
	    			// }
	    		}
	    	};
			document.getElementById("input-quantity").onkeypress = function(event){
			    if (event.keyCode == 13 || event.which == 13){
			    	var qty = $('#input-quantity').val();
			    	if (qty != '') {
			    		if (qty > 0) {
				    		$('#input-purchase_price').focus();
				    		document.getElementById("input-purchase_price").onkeypress = function(event){
					    		if (event.keyCode == 13 || event.which == 13){
					    			var purchase_price = $('#input-purchase_price').val();
					    			if (purchase_price == '') {
					    				alert('Please Enter Rate!');
					    				return false;
					    			} else if (purchase_price <= 0) {
					    				alert('Please Enter Rate!');
					    				return false;
					    			} else {
						    			$('#input-expiry_date').focus();
					    			}
					    			document.getElementById("input-expiry_date").onkeypress = function(event){
							    		if (event.keyCode == 13 || event.which == 13){
							    			var expiry_date = $('#input-expiry_date').val();
					    					var expiry_dates = expiry_date.split('-');
					    					var dd = expiry_dates[2];
											var mm = expiry_dates[1];
											var yyyy = expiry_dates[0];
								    		var expiry_date = dd + "-" + mm + "-" + yyyy;
					    					var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
					    					if (!(date_regex.test(expiry_date))) {
												$('#error_expiry_date').show();
												return false;
											} else {
												$('#error_expiry_date').hide();
								    			$('#input-batch_no').focus();
											}
								    		document.getElementById("input-batch_no").onkeypress = function(event){
							    				if (event.keyCode == 13 || event.which == 13){
							    					var po_no = $('#select-po_data').val();
							    					var qutys = $('#input-quantity').val();
							    					var pratesz = $('#input-purchase_price').val();
							    					if (po_no == '') {
							    						alert("Please Select Po No!");
							    					} else if (qutys <= 0) {
							    						alert("Please Enter Valid Quantity!");
							    					} else if (pratesz <= 0) {
							    						alert("Please Enter Valid Rate!");
							    					} else {
								    					var final_total = $('#input-totals').val();
														var tots = $('#input-total').val();
														var final_totals = parseFloat(final_total) + Number(tots);
														$('#input-totals').val(final_totals);
														$('#input-final_total').val(final_totals);

														var final_val = $('#input-vals').val();
														var vals = $('#input-Values').val();
														var final_vals = parseFloat(final_val) + Number(vals);
														$('#input-vals').val(final_vals);
														$('#input-final_val').val(final_vals.toFixed(2));

														var final_gst_val = $('#input-gst_val').val();
														var gst_val = $('#input-gst_value').val();
														var final_gst_vals = parseFloat(final_gst_val) + Number(gst_val);
														$('#input-gst_val').val(final_gst_vals);
														$('#input-final_gst_val').val(final_gst_vals);


										    			var batch_no = $('#input-batch_no').val();
										    			$('#input-productnew_code').focus();
										        		add_productraws(json)
							    					}
									        	}
									        };
									    }
			    					
					    			}
					        	}
					        };
			    		}else {
							alert('Please Enter Quantity!');
							return false;
						}
			    	} else {
						alert('Please Enter Quantity!');
						return false;
					}
			    }
			};
			
		}
	});
  }
});
$("#select-po_data").keypress(function (e) {
	if (e.which == 13) {
		var po = $("#select-po_data").val();
		getpoNo(po)
	}
});

function reason_approve() {
	$('#reason').show();
	$('#reject').show();
}

function rejected() {
	var reason = $('#reason').val();
	var order_id = $('#input-order_id').val();
	url = 'index.php?route=catalog/inward/inward_dis_approval&token=<?php echo $token; ?>';
	if (reason != '') {
		url += '&reason=' + encodeURIComponent(reason);
		url += '&order_id=' + encodeURIComponent(order_id);
	} else {
		return false;
	}
	window.location.href = url;
}

function prev_inw() {
	var datas = $("#input-datas").val();
	if (datas == 1) {
		var check = confirm('Warning: Please Save This Inward! \n Click Ok To Discard This Inward');
		if (check == true) {
			url = 'index.php?route=catalog/inward&token=<?php echo $token; ?>';
			window.location.href = url;
		} else {
			return false;
		}
	} else {
		url = 'index.php?route=catalog/inward&token=<?php echo $token; ?>';
		window.location.href = url;
	}
}

function checks() {
	var supp = $("#input-supplier").val();
	var checks = $("#input-checks").val();
	var order_id = $("#input-order_id").val();
	var added = $('#input-added').val();
	if (order_id == '') {
		if (supp == '') {
			alert("Please Enter Valid Supplier!");
		} else if (checks == '') {
			alert("Please Enter Valid Supplier!");
		} else if (added == '') {
			alert("Please Enter Atleast One Inward!");
		} else {
			document.getElementById("form-manufacturer").submit();
		}
	} else {
		document.getElementById("form-manufacturer").submit();
	}
}

$('.form-control').on('keydown', function(e) { 
    if (e.keyCode == 9) {
        $(this).focus();
       e.preventDefault();
    }
});

// document.getElementById("input-batch_no").onkeypress = function(event){
// if (event.keyCode == 13 || event.which == 13){
// }


</script>

<?php echo $footer; ?>