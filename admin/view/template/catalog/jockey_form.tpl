<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
                </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="col-sm-2 panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3></b>
                <?php if ($jockey_id) { ?>
                    <div style="padding-left: 40%;">
                        <b><label style="font-family: cursive;color: #00a04d;font-size: 20px;border: none;background: #fcfcfc;width: 80%;text-transform: uppercase;"><?php echo $jockey_name; ?></label></b>
                        <b>Jockey Code : <?php echo $jockey_code; ?> </b>
                    </div>
                <?php } else { ?>
                    <div style="padding-left: 34%;" class="">
                        <b><input style="border: none;background: #fcfcfc;width: 95%;" disabled="disabled" id="name1" value="" name=""></b>
                    </div>
                <?php } ?>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-contact_tax_details" data-toggle="tab"><?php echo $tab_contact_tax_details; ?></a></li>
                        <?php  if($jockey_id !=''){ ?>
                        <li style="display: none;"><a href="#tab-race_record" data-toggle="tab"><?php echo $tab_race_record; ?></a></li>

                        <li><a href="#tab-ban_details" data-toggle="tab"><?php echo $tab_ban_details; ?></a></li>
                        <li><a href="#tab_transaction" data-toggle="tab">Transaction</a></li>
                       <?php  } ?>

                    </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <div class="form-group ">
                                <label style="display: none;" class="col-sm-2 control-label" for="jockey_code"><b style="color: red">*</b>Jockey code :</label>
                                <div style="display: none;" class="col-sm-3">
                                    <input type="hidden" name="jockey_id" value="<?php echo $jockey_id;  ?>">
                                    <input disabled="disabled" type="text" name="jockey_code" value="<?php echo $jockey_code; ?>" placeholder="<?php echo "Jockey Code"  ;?>" id="jockey_code" class="form-control" tabindex="1"/>
                                    <input type="hidden" name="hidden_jockey_code" value="<?php echo $jockey_code; ?>" id="jockey_code" class="form-control"/>
                                    <?php if (isset($valierr_jockey_code)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_jockey_code; ?></span><?php } ?>
                                </div>
                                <label class="col-sm-2 control-label" for="input-jockey_code_new">Jockey Code</label>
                                <div class="col-sm-3">
                                    <input type="text" name="jockey_code_new" value="<?php echo $jockey_code_new; ?>" placeholder="<?php echo "Jockey Code"; ?>" id="input-jockey_code_new" class="form-control" tabindex="1"/>
                                    <?php if (isset($valierr_jockey_code)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_jockey_code; ?></span><?php } ?>
                                </div>
                                <label class="col-sm-2 control-label" for="racing_name">Profile Pic:</label>
                                <div class="col-sm-5">
                                    <input style="display: none;" readonly="readonly" onchange="readURL(this);" type="text" name="file_number_upload" value="<?php echo $file_number_upload; ?>" placeholder="Upload File Number" id="input-other_document_1" class="form-control" />
                                    <input type="hidden" name="uploaded_file_source" value="<?php echo $uploaded_file_source; ?>" id="input-other_document_1_source" class="form-control" />
                                    <span class="input-group-btn pull-left" style="padding-right: 65px;">
                                        <button type="button" id="button-other_document_1" style = "border-radius: 3px;" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-default"><i class="fa fa-upload"></i></button>
                                        <?php if($uploaded_file_source != ''){ ?>
                                            <a target="_blank" class = "btn btn-default" style=" display: none;cursor: pointer;margin-left:10px; border-radius: 3px;" id="uploaded_file_source" href="<?php echo $uploaded_file_source; ?>">View Profile</a>
                                        <?php } ?>
                                    </span>
                                    <?php if($img_path != "#") { //echo $img_path;exit; ?>
                                        <span id="profile_pic" class="col-sm-8" ><img src="<?php echo $img_path; ?>" height="60" id="blah" alt=""  /></span>
                                    <?php } else { ?>
                                        <span id="profile_pic" class="col-sm-8" ><img src="<?php echo $unknown_pic; ?>" height="60" id="blah" alt=""  /></span>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="racing_name"><?php echo "Racing Name :"; ?></label>
                                <div class="col-sm-3">
                                    <input type="text" name="racing_name" value="<?php echo $racing_name;  ?>" placeholder="<?php echo "Racing Name "; ?>" id="racing_name" class="form-control" tabindex="2" />
                                    
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="jockey_name"><b style="color: red">*</b>Jockey Name</label>
                                <div class="col-sm-3">
                                    <input type="text" name="jockey_name" value="<?php echo $jockey_name; ?>" placeholder="<?php echo "Jockey Name"; ?>" id="jockey_name" class="form-control" tabindex="3"/>
                                    <?php if (isset($valierr_jockey_name)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_jockey_name; ?></span><?php } ?>
                                </div>
                                <label class="col-sm-2 control-label" for="jai_member"><?php echo "JAI Member:"; ?></label>
                                <div class="col-sm-3">
                                    <select name="jaimember" id="jaimember" class="form-control"  tabindex="">
                                        <?php foreach ($jaimembers as $jaimemberkey => $jaimembervalue) { ?>
                                        <?php if ($jaimemberkey == $jaimember) { ?>
                                        <option value="<?php echo $jaimembervalue; ?>" selected="selected"><?php echo $jaimembervalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $jaimembervalue; ?>"><?php echo $jaimembervalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="Fathers_name">Fathers Name</label>
                                <div class="col-sm-3">
                                    <input type="text" name="Fathers_name" value="<?php echo $Fathers_name; ?>" placeholder="<?php echo "Fathers Name"; ?>" id="Fathers_name" class="form-control" tabindex="3"/>
                                    
                                </div>
                                <label class="col-sm-2 control-label" for="educational_qualification">Educational Qualification</label>
                                <div class="col-sm-3">
                                    <input type="text" name="educational_qualification" value="<?php echo $educational_qualification; ?>" placeholder="<?php echo "Educational Qualification"; ?>" id="educational_qualification" class="form-control" tabindex="3"/>
                                    
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="emergency_contact_person">Emergency Contact Person</label>
                                <div class="col-sm-3">
                                    <input type="text" name="emergency_contact_person" value="<?php echo $emergency_contact_person; ?>" placeholder="<?php echo "Emergency Contact Person"; ?>" id="emergency_contact_person" class="form-control" tabindex="3"/>
                                    
                                </div>
                                <label class="col-sm-2 control-label" for="contact_no">Contact No</label>
                                <div class="col-sm-3">
                                    <input type="number" name="contact_no" value="<?php echo $contact_no; ?>" placeholder="<?php echo "Contact No"; ?>" id="contact_no" class="form-control" tabindex="3"/>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-date_of_birth"><?php echo "Birth Date"; ?></label>
                                <div class="col-sm-3"> 
                                    <div class="input-group date input-date_of_birth">
                                        <input type="text" name="date_of_birth" value="<?php echo $birth_date; ?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-date_of_birth" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" id="date-btn" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                        <input type="hidden" name="age" value="<?php echo $age; ?>" id="input-age" class="form-control" />
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label" for="origin"><?php echo "Origin:"; ?></label>
                                <div class="col-sm-3">
                                    <select name="origin" id="origin" class="form-control"  tabindex="">
                                        <?php foreach ($origins as $originkey => $originvalue) { ?>
                                        <?php if ($originkey == $origin) { ?>
                                        <option value="<?php echo $originvalue; ?>" selected="selected"><?php echo $originvalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $originvalue; ?>"><?php echo $originvalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-license_issued_date"><?php echo "License Issued On(First Time)"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date">
                                        <input type="text" name="license_issued_date" value="<?php echo $first_time_license_issued; ?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-license_issued_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>     
                                </div>
                            </div>
                            <?php $sty_css = ($jockId != 0) ? 'disabled="disabled"': ""; ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">License Type(A/B):</label>
                                <div class="col-sm-3 ">
                                    <select <?php echo $sty_css ?> id="license_type"  class="form-control" name="license_type" value="<?php  ?>" tabindex="" >
                                            <?php foreach($license_type1 as $skey => $svalue){
                                             ?>
                                                <?php if($skey ==$license_type){ ?>
                                                    <option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                    </select>
                                </div>
                                <div  id="center_holding_hide" style="display: none;">
                                    <label class="col-sm-2 control-label">Center Holding License Type A:</label>
                                    <div class="col-sm-3 ">
                                        <select id="center_holding_license_type_a"  class="form-control" name="center_holding_license_type_a" tabindex="" >
                                                <?php foreach($center_holding1 as $skey => $svalue){
                                                 ?>
                                                    <?php if($skey == $center_holding_license_type_a){ ?>
                                                        <option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                        </select>
                                    </div>
                                
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="input-license_renewal"><?php echo "License Renewal date"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date">
                                        <input type="text" name="license_renewal" value="<?php echo $license_renewal_date; ?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-license_renewal" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label" for="license_end_date"><?php echo "License End date"; ?></label>
                                        <div class="col-sm-3">
                                            <div class="input-group date">
                                                <input type="text" name="license_end_date" value="<?php echo $license_end_date; ?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-license_end_date" class="form-control" />
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <?php if (isset($valierr_license_renewal)) { ?><span class="errors"><?php echo $valierr_license_renewal; ?></span><?php } ?>
                                        </div>
                                <!-- <div class="col-sm-1">
                                    <input type="Number" name="month_license_end_date" value="<?php echo $month_license_end_date;   ?>"  placeholder="MM" id="month_license_end_date" class="form-control" tabindex="10"/>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_month_license_end_date" class="error"><?php echo 'Please Enter Valid Month'; ?></span>
                                </div>
                                <div class="col-sm-1">
                                    <input type="Number" name="year_license_end_date"  value="<?php echo $year_license_end_date;  ?>" placeholder="YYYY" id="year_license_end_date" class="form-control" tabindex="11" />
                                    <span style="display: none;color: red;font-weight: bold;" id="error_year_license_end_date" class="error"><?php echo 'Please Enter Valid Year'; ?></span>
                                </div> -->
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="ride_weight"><?php echo "Ride Weight:"; ?></label>
                                <div class="col-sm-3">
                                    <input type="text" name="ride_weight" value="<?php echo $ride_weight;  ?>" placeholder="<?php echo "Ride Weight"; ?>" id="ride_weight" class="form-control" tabindex="14"/>
                                </div>
                                <label class="col-sm-2 control-label" for="input-file_number"><?php echo "File Number:"; ?></label>
                                <div class="col-sm-3">
                                    <input type="text" name="file_number" value="<?php echo $file_number;  ?>" placeholder="<?php echo "File Number"; ?>" id="file_number" class="form-control" tabindex="14"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="blood_group"><?php echo "Blood Group :" ?></label>
                                <div class="col-sm-3">
                                  <select name="blood_group" id="blood_group"  class="form-control" tabindex="">
                                    <option value="<?php  ?>">Please Select</option>
                                    <?php foreach ($bloodgroups as $bloodgroupkey => $bloodgroupvalue) { ?>
                                        <?php if ($bloodgroupkey == $blood_group) { ?>
                                        <option value="<?php echo $bloodgroupvalue; ?>" selected="selected"><?php echo $bloodgroupvalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $bloodgroupvalue; ?>"><?php echo $bloodgroupvalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                  </select>
                                </div>
                                <label class="col-sm-2 control-label" for="input-file"><?php echo "" ?></label>
                                <div class="col-sm-3"  >
                                    <input readonly="readonly" type="text" name="file" value="<?php echo $file_upload;  ?>" placeholder=" File" id="file" class="form-control" />
                                    <input type="hidden" name="file_source" value="<?php  ?>" id="file_source" class="form-control" />
                                    <span class="input-group-btn" style="padding-top: 2%;">
                                        <button type="button" id="button-file" style = "border-radius: 3px;" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo 'Upload Document'; ?></button>
                                        <span id="button-file_new"></span>
                                       <!--  <?php if($file_source != ''){ ?>
                                            <a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="awbi_registration_file_source" href="<?php echo $awbi_registration_file_source; ?>">View Document</a>
                                        <?php } ?> -->
                                    </span>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="no_whip"><?php echo "No Whip:" ?></label>
                                <div class="col-sm-3">
                                  <select name="no_whip" id="no_whip" class="form-control" tabindex="">
                                    <option value="<?php  ?>">Please Select</option>
                                    <?php foreach ($typess as $nowhipkey => $nowhipvalue) { ?>
                                        <?php if ($nowhipvalue == $no_whip) { ?>
                                        <option value="<?php echo $nowhipvalue; ?>" selected="selected"><?php echo $nowhipvalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $nowhipvalue; ?>"><?php echo $nowhipvalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                  </select>
                                </div>
                                <label class="col-sm-2 control-label" for="apprentice"><?php echo "Apprentice:" ?></label>
                                <div class="col-sm-2">
                                    <!-- <input type="text" name="allowance_claiming" value="<?php echo $apprentice   ; ?>" placeholder="<?php echo "Total Wins"; ?>" id="allowance_claiming" class="form-control" tabindex=""/>
                                     -->
                                  <select name="apprentice" id="apprentice" class="form-control" tabindex="">
                                    <option value="<?php  ?>">Please Select</option>
                                    <?php foreach ($typess as $key => $value) { ?>
                                        <?php if ($value == $apprentice) { ?>
                                        <option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                  </select>
                                </div>
                                <label style="display: none;" id="allowance_claiming_l" class="col-sm-1 control-label" for="allowance_claiming">Allowance claiming:</label>
                                <div style="display: none;" id="allowance_claiming_div" class="col-sm-2">
                                    <!-- <input type="text" name="allowance_claiming" value="<?php echo $allowance_claiming   ; ?>" placeholder="<?php echo "Total Wins"; ?>" id="allowance_claiming" class="form-control" tabindex=""/> -->
                                    <select name="allowance_claiming" id="allowance_claiming" class="form-control" tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($allowance_claiming1 as $key => $value) { ?>
                                            <?php if ($value == $allowance_claiming) { ?>
                                                <option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                      </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="out_station_wins">Out Station Wins :</label>
                                <div class="col-sm-3">
                                    <input type="text" name="out_station_wins" value="<?php echo $out_station_wins ; ?>" placeholder="<?php echo "Out Station Wins"; ?>" id="out_station_wins" onblur="changeIt(this.value)" class="form-control" onkeyup="total();" tabindex=""/>
                                     
                                </div>

                                <label hidden="hidden" class="col-sm-2 control-label" for="station_wins">Station Wins :</label>
                                <div  hidden="hidden" class="col-sm-2">
                                    <input type="text" name="station_wins" value="<?php echo $station_wins ; ?>" placeholder="<?php echo "Station Wins"; ?>" id="station_wins" class="form-control" onkeyup="total();" tabindex=""/>
                                </div>

                                <label class="col-sm-2 control-label" for="total_wins">Total Wins :</label>
                                <div class="col-sm-2">
                                    <input type="text" readonly="readonly" name="total_wins" value="<?php echo $total_wins ; ?>" placeholder="<?php echo "Total Wins"; ?>" id="total_wins" class="form-control" tabindex=""/>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div id="my_div"></div>
                                 <?php  $i=0;
                                 if (isset($station_name) && $station_name != '') {
                                    foreach ($station_name as $key => $value) { ?>
                                        <div class='mydivs'>
                                            <label class='col-sm-2 control-label'>Horse<?php echo $i ?>:</label>
                                            <div class="col-sm-2">
                                                <input class='form-control' value="<?php echo $value['out_station_name'] ?>" type='text' name='out_station_name[<?php echo $i; ?>][name]'>
                                            </div>
                                        </div>
                                    <?php $i++;
                                    }
                                 } ?>
                            </div>
                            <div style="display: none;" id="div_master_trainer">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="master_trainer"><?php echo "Master Trainer :" ?></label>
                                    <div class="col-sm-2">
                                      <!-- <select style="display: none;" name="master_trainer" id="master_trainer" class="form-control" tabindex="">
                                        <option value="">Please Select</option>
                                        <?php foreach ($typess as $tkey => $tvalue) { ?>
                                            <?php if ($tvalue == $master_trainer) { ?>
                                            <option value="<?php echo $tvalue; ?>" selected="selected"><?php echo $tvalue; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $tvalue; ?>"><?php echo $tvalue; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                      </select> -->
                                      <input class='form-control' value="<?php echo $master_trainer; ?>" type='text' name='master_trainer' id="master_trainer" placeholder="<?php echo "Master Trainer" ?>" />

                                      <input class='form-control' value="<?php echo $hidden_master_trainer; ?>" type='hidden' name='hidden_master_trainer' id="hidden_master_trainer"  />
                                    </div>

                                    <label class="col-sm-2 control-label" for="input-date_of_birth"><?php echo "Master Trainer Start Date"; ?></label>
                                    <div class="col-sm-2"> 
                                        <div class="input-group date">
                                            <input type="text" name="master_trainer_s_date" value="<?php echo $master_trainer_s_date; ?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-master_trainer_s_date" class="form-control" />
                                            <span class="input-group-btn">
                                            <button class="btn btn-default" id="date-btn" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>

                                    <label class="col-sm-2 control-label" for="input-master_trainer_e_date"><?php echo "Master Trainer End Date"; ?></label>
                                    <div class="col-sm-2"> 
                                        <div class="input-group date">
                                            <input type="text" name="master_trainer_e_date" value="<?php echo $master_trainer_e_date; ?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-master_trainer_e_date" class="form-control" />
                                            <span class="input-group-btn">
                                            <button class="btn btn-default" id="date-btn" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="form-group ">
                                   <label class="col-sm-2 control-label" for="master_trainer"><?php echo "Trainer History :" ?></label>
                                    <div class="col-sm-2">
                                      <textarea readonly="readonly" class='form-control'><?php echo $trainer_history; ?></textarea>
                                    </div> 
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="input-jockey_returned_by"><?php echo "Jockey retain by:" ?></label>
                                <div class="col-sm-3">
                                <select id="input-jockey_returned_by" class="form-control" name="jockey_returned_by" value="<?php echo $jockey_returned_by; ?>" tabindex="12">
                                    <option value="0" disabled="disabled" >Please Select</option>
                                        <?php foreach($jockey_option as $tkey => $tvalue){ ?>
                                            <?php if($tkey == $jockey_returned_by){ ?>
                                                <option value="<?php echo $tkey ?>" selected="selected"><?php echo $tvalue; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $tkey ?>"><?php echo $tvalue ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                </select>
                                </div>
                                <div class="col-sm-2 add-owner-div"  style="display:none;">
                                    <a id="add-ownrs" class="btn btn-primary"><i class="fa fa-plus"></i> Add Owner</a>
                                </div>
                                <div class="col-sm-3 owner_data-div">
                                    <?php  $cnt_total = 0;
                                     if($private_trainers_owner_data){
                                        foreach($private_trainers_owner_data as $key => $value){ ?>
                                            <div class="form-group" id="remove_div_<?php echo $key ?>">
                                                <div class="col-sm-10">
                                                    <input type="text" name=private_trainers_owner_name[<?php echo $key ?>][owner_name] value="<?php echo $value['owner_name'] ?>" class="form-control trainer_owner_name" id="trainer_owner_id_<?php echo $key ?>" />
                                                    <input type="hidden" name=private_trainers_owner_name[<?php echo $key ?>][owner_id] value="<?php echo $value['owner_id'] ?>" id="hidden_trainer_owner_id_<?php echo $key ?>" />
                                                </div>

                                                <div class="col-sm-2">
                                                    <a onclick="rmvOwnerName('<?php echo $key ?>')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </div>
                                        <?php $cnt_total++; }
                                    } ?>

                                </div>
                                <input type="hidden" value="<?php echo $cnt_total; ?>" class="owners_cnt" id="owners_cnt">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="select-isActive">Status</label>
                                <div class="col-sm-3">
                                    <input type="hidden" name="hidden_active" value="<?php echo $isActive1; ?>">
                                    <select name="isActive" id="select-isActive" class="form-control" data-index="">
                                        <?php foreach ($Active as $key => $value) { ?>
                                        <?php if ($key == $isActive) { ?>
                                          <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                          <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                <?php if (isset($error_status)) { ?><span class="errors" style="color: #ff0000;"><?php echo $error_status; ?></span><?php } ?>
                                </div>
                                <label class="col-sm-2 control-label" for="remarks"><?php echo "Remark:" ?></label>
                                <div class="col-sm-5">
                                  <textarea name="remarks" id="remarks"cols="100" class="form-control" tabindex="16"><?php echo $remarks ?></textarea>
                                <?php if (isset($remarks_errors)) { ?><span class="errors" style="color: #ff0000;"><?php echo $remarks_errors; ?></span><?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-contact_tax_details">
                            <label style="padding-bottom: 30px;padding-left: 0px;" class="col-sm-6 control-label alignLeft" for="input-address1"> Address 1</label>
                            <label style="padding-bottom: 30px;" class="col-sm-6 control-label alignLeft" for="input-address2">Address 2</label>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <input type="text" name="address1" data-index="23" id="input-address1" value="<?php echo $address_1; ?>" placeholder="Address" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" name="address2" data-index="29" id="input-address2" value="<?php echo $address_2; ?>" placeholder="Address" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <input type="text" name="address_3" data-index="23" id="input-address_3" value="<?php echo $address_3; ?>" placeholder="Address" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" name="address_4" data-index="29" id="input-address_4" value="<?php echo $address_4; ?>" placeholder="Address" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <input type="text" name="localArea1" data-index="22" id="input-localArea1" value="<?php echo $local_area_1; ?>" placeholder="Locality area or street" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="city1" data-index="21" id="input-city1" value="<?php echo $city_1; ?>" placeholder="City" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="localArea2" data-index="28" id="input-localArea2" value="<?php echo $local_area_2; ?>" placeholder="Locality area or street" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="city2" data-index="27" id="input-city2" value="<?php echo $city_2; ?>" placeholder="City" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                  <select name="country1" data-index="19" id="input-country" onchange="country(this);" class="form-control">
                                    <option value="">Country</option>
                                    <?php foreach ($countries as $country) { ?>
                                    <?php if ($country['country_id'] == $country_1) { ?>
                                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>

                                <div class="col-sm-3">
                                  <select name="zone_id" data-index="20" id="zone_id" class="form-control">
                                    <option value="">State</option>
                                    <?php foreach ($zones as $zone) { ?>
                                    <?php if ($zone['zone_id'] == $zone_id) { ?>
                                    <option value="<?php echo $zone['zone_id']; ?>" selected="selected"><?php echo $zone['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $zone_id; ?>"><?php echo $zone['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>

                                <div class="col-sm-3">
                                  <select name="country2" data-index="25" id="input-country2" onchange="countrys(this);" class="form-control">
                                    <option value="">Country</option>
                                    <?php foreach ($countries as $country2) { ?>
                                    <?php if ($country2['country_id'] == $country_2) { ?>
                                    <option value="<?php echo $country2['country_id']; ?>" selected="selected"><?php echo $country2['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $country2['country_id']; ?>"><?php echo $country2['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>

                                <div class="col-sm-3">
                                  <select name="zone_id2" data-index="26" id="zone_id2" class="form-control">
                                    <option value="0">State</option>
                                    <?php foreach ($zones2 as $zone2) { ?>
                                    <?php if ($zone2['zone_id'] == $zone_id2) { ?>
                                    <option value="<?php echo $zone2['zone_id']; ?>" selected="selected"><?php echo $zone2['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $zone2['zone_id']; ?>"><?php echo $zone2['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                  </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-6">
                                    <input type="Number" name="pincode1" data-index="24" id="pincode1" value="<?php echo $pincode_1; ?>" placeholder="Pincode" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <input type="Number" name="pincode2" data-index="30" id="pincode2" value="<?php echo $pincode_2; ?>" placeholder="Pincode" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <h4 style="padding-left: 20px;">Contact Details</h4>
                                <label class="col-sm-2 control-label" for="phone_no">Phone No</label>
                                <div class="col-sm-4">
                                    <input type="number" maxlength="10" name="phone_no" id="phone_no" value="<?php echo $phone_no; ?>" placeholder="Phone No" class="form-control" tabindex="1">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mobile_no1"> Mobile No 1</label>
                                <div class="col-sm-4">
                                    <input type="number" maxlength="10" name="mobile_no1" id="mobile_no1" value="<?php echo $mobile_no; ?>" placeholder="Mobile No" class="form-control" tabindex="2">
                                </div>
                                <label class="col-sm-2 control-label" for="altMobileNo">Alternate Mobile No</label>
                                <div class="col-sm-4">
                                    <input type="number" maxlength="10" name="alternate_mob_no" id="alternate_mob_no" value="<?php echo $alternate_mob_no; ?>" placeholder="Alternate Mobile No" class="form-control" tabindex="3">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="emailId"> Email id</label>
                                <div class="col-sm-4">
                                    <input type="email" name="email_id" id="email_id" value="<?php echo $email_id ?>" placeholder="Email id" class="form-control" tabindex="4">
                                </div>
                                <label class="col-sm-2 control-label" for="altEmailId">Alternate Email id</label>
                                <div class="col-sm-4">
                                    <input type="email" name="alternate_email_id" id="alternate_email_id" value="<?php echo $alternate_email_id; ?>" placeholder="Alternate Email id" class="form-control" tabindex="5">
                                </div>
                            </div>
                            <div class="form-group">
                                <h4 style="padding-left: 20px;">Tax Details</h4>
                                <label class="col-sm-2 control-label" for="gst_type"> GST Type</label>
                                <div class="col-sm-4">
                                    <select id="gst_type" class="form-control" name="gst_type" value="<?php echo $gst_type; ?>" tabindex="19" >
                                            <option value="">Please Select</option>
                                            <?php foreach($gst_type1 as $skey => $svalue){
                                             ?>
                                                <?php if($skey == $gst_type){ ?>
                                                    <option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                    </select>
                                </div>
                                <?php if($gst_type == 'R'){ ?>
                                    <label class="col-sm-2 control-label" id="gst_label" for="gst_no">GST No</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="gst_no" id="gst_no" value="<?php echo $gst_no; ?>" placeholder="GST No" class="form-control" tabindex="19">
                                        <span class="gst_error"></span>
                                <?php if (isset($gst_errors)) { ?><span class="errors" style="color: #ff0000;"><?php echo $gst_errors; ?></span><?php } ?>

                                    </div>

                                <?php } else { ?>
                                    <label  id="gst_label" class="col-sm-2 control-label" for="gst_no">GST No</label>
                                    <div class="col-sm-4">
                                        <input  type="text" name="gst_no" id="gst_no" placeholder="GST No" class="form-control" tabindex="19">
                                        <span class="gst_error"></span>
                                <?php if (isset($gst_errors)) { ?><span class="errors" style="color: #ff0000;"><?php echo $gst_errors; ?></span><?php } ?>

                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="pan_no">PAN Number</label>
                                <div class="col-sm-4">
                                    <input type="text" name="pan_no" id="pan_no" value="<?php echo $pan_no; ?>" placeholder="PAN Number" class="form-control" tabindex="21">
                                    <span class="pan_error"></span>
                                    <?php if (isset($pan_errors)) { ?><span class="errors" style="color: #ff0000;"><?php echo $pan_errors; ?></span><?php } ?>
                                </div>
                                <label class="col-sm-2 control-label" for="prof_tax_no">Prof Tax No details</label>
                                <div class="col-sm-4">
                                    <input type="text" name="prof_tax_no" id="prof_tax_no" value="<?php echo $prof_tax_no; ?>" placeholder="Prof Tax No details" class="form-control" tabindex="22">
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label class="col-sm-2 control-label" for="epf_no">EPF No</label>
                                <div class="col-sm-4">
                                    <input type="text" name="epf_no" id="epf_no" value="<?php echo $epf_no; ?>" placeholder="EPF No" class="form-control" tabindex="21">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden"  name="upload_hidden_id" id="upload_hidden_id" value="<?php echo $upload_hidden_id; ?>">
                                <table id="itrUpload" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr><td colspan="4" >ITR Upload</td></tr>
                                        <tr>
                                            <td class="text-right">Assesment Year</td>
                                            <td class="text-right">File Name</td>
                                            <?php if ($group_id == '1' || $group_id == '11') { ?>
                                                <td class="text-left"><button type="button" onclick="ITR_Upload()" data-toggle="tooltip" title="<?php echo "add"; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody id="upload_itr">
                                        <?php //echo'<pre>';print_r($juploaditr);  ?>
                                            <?php foreach($juploaditr as $key => $result) { ?>
                                                <tr id="recurring-row<?php echo $key; ?>">
                                                    <td>
                                                        <input type="text" id="year_<?php echo $key ?>"  name="itrdatas[<?php echo $key ?>][year]" value="<?php  echo $result['assesment_year'] ;?>" placeholder="Assesment Year" class="form-control" />
                                                        <input type="hidden" id="hidden_id_<?php echo $key ?>"  name="itrdatas[<?php echo $key ?>][hidden_id_]" value="<?php  echo $result['jockey_itr_id'] ;?>"  class="form-control" />
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly id="image_<?php echo $key ?>" name="itrdatas[<?php echo $key ?>][image]"  value="<?php  echo $result['file'] ;?>" placeholder="choose file" class="form-control" />
                                                        <input type="hidden" id="image_path_<?php echo $key ?>" name="itrdatas[<?php echo $key ?>][imagepath]" value="<?php  echo $result['file_path'] ;?>"  class="form-control" />
                                                        <?php if ($group_id == '1' || $group_id == '11') { ?>
                                                        <button type="button" class="button-upload btn btn-default" id="button-upload_<?php echo $key ?>" data-loading-text="<?php echo 'Please Wait'; ?>"  style="margin-top: 5px;"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
                                                        <?php } ?>
                                                        <span id="button-other_document_<?php echo $key ?>"></span>
                                                        <?php if($result['file_path'] != ''){ ?>
                                                            <span id="btn_view_<?php echo $key ?>">
                                                                <a target="_blank" class = "btn btn-default" style="cursor: pointer;margin-left:10px; border-radius: 3px;" id="itrdatas[<?php echo $key ?>][imagepath]" href="<?php  echo $result['file_path'] ;?>">View Document</a>
                                                            </span>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                        <?php } //exit; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-race_record">

                        </div>
                        <div class="tab-pane" id="tab-ban_details">
                            <div class="col-sm-11">
                                <h4>Jockey Ban Details</h4>
                            </div>
                            <div style="float: right;padding-bottom: 10px;">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2"  onclick="closeaddbuu1()"><i class="fa fa-plus-circle"></i></button>
                            </div>
                            <div class="col-sm-1" >
                                <input type="hidden" name="id_hidden_band" value="<?php echo $id_hidden_band ?>" id="id_hidden_band" class="form-control" />
                                <div id="myModal2" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content" style="width: 107%;" >
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Ban</h4>
                                            </div>
                                            <div class="modal-body" style="margin-left: 24px;margin-right: 24px;">
                                                <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-club"><b style="color: red">*</b><?php echo "Club:"; ?></label>
                                                    <div class="col-sm-8">
                                                        <!-- <input type="text" name="club" value="" placeholder="<?php echo "Club"; ?>" id="input-club" class="form-control" /> -->
                                                        <select  name="club" placeholder="Club" id="input-club" class="form-control">
                                                            <option value="" selected="selected" disabled="disabled" >Please Select</option>
                                                            <?php foreach ($jockey_clubs as $ckey => $cvalue) { ?>
                                                            <option value="<?php echo $cvalue; ?>" ><?php echo $cvalue ?></option>
                                                            <?php } ?>
                                                      </select>
                                                      <span style="display: none;color: red;font-weight: bold;" id="error_club_ban" class="error"><?php echo 'Please Select Club'; ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Start Date:"; ?></label>
                                                    <div class="col-sm-8">
                                                    <div class="input-group date">
                                                            <input type="text" name="date_start_date_ban" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_start_date_ban" class="form-control" />
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                            <span style="display: none;color: red;font-weight: bold;" id="error_date_start_date_ban" class="error"><?php echo 'Please Enter Valid Date'; ?></span>
                                                    </div>
                                                        <!-- <input type="Number" name="date_start_date_ban"  placeholder="DD" id="date_start_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_date_start_date_ban" class="error"><?php echo 'Please Enter Valid Date'; ?></span>
                                                    </div> -->
                                                        
                                                    <!-- <div class="col-sm-2">
                                                        <input type="Number" name="month_start_date_ban"  placeholder="MM" id="month_start_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_month_start_date_ban" class="error"><?php echo 'Please Enter Valid Month'; ?></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="Number" name="year_start_date_ban"  placeholder="YYYYY" id="year_start_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_year_start_date_ban" class="error"><?php echo 'Please Enter Valid Year'; ?></span>
                                                    </div> -->
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-horse"><?php echo "End Date:"; ?></label>
                                                    <div class="col-sm-8">
                                                         <div class="input-group date">
                                                            <input type="text" name="date_end_date_ban" value="" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="date_end_date_ban" class="form-control" />
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_greater_date_start_date_ban" class="error"><?php echo 'End Date must not be less than Start Date!'; ?></span>
                                                    </div>
                                                        <!-- <input type="Number" name="date_end_date_ban"  placeholder="DD" id="date_end_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_date_end_date_ban" class="error"><?php echo 'Please Enter Valid Date'; ?></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="Number" name="month_end_date_ban"  placeholder="MM" id="month_end_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_month_end_date_ban" class="error"><?php echo 'Please Enter Valid Month'; ?></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="Number" name="year_end_date_ban"  placeholder="YYYYY" id="year_end_date_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_year_end_date_ban" class="error"><?php echo 'Please Enter Valid Year'; ?></span>
                                                    </div> -->
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-horse"><?php echo "Start Date:"; ?></label>
                                                    <div class="col-sm-8">
                                                     <div class="input-group date">
                                                            <input type="text" name="date_start_date_ban2" value="" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="date_start_date_ban2" class="form-control" />
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                        <!-- <input type="Number" name="date_start_date_ban2"  placeholder="DD" id="date_start_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_date_start_date_ban2" class="error"><?php echo 'Please Enter Valid Date'; ?></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="Number" name="month_start_date_ban2"  placeholder="MM" id="month_start_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_month_start_date_ban2" class="error"><?php echo 'Please Enter Valid Month'; ?></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="Number" name="year_start_date_ban2"  placeholder="YYYYY" id="year_start_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_year_start_date_ban2" class="error"><?php echo 'Please Enter Valid Year'; ?></span>
                                                    </div> -->
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-horse"><?php echo "End Date:"; ?></label>
                                                    <div class="col-sm-8">
                                                    <div class="input-group date">
                                                            <input type="text" name="date_end_date_ban2" value="" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="date_end_date_ban2" class="form-control" />
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_greater_date_start_date_ban2" class="error"><?php echo 'End Date must not be less than Start Date!'; ?></span>
                                                    </div>
                                                        <!-- <input type="Number" name="date_end_date_ban2"  placeholder="DD" id="date_end_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_date_end_date_ban2" class="error"><?php echo 'Please Enter Valid Date'; ?></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="Number" name="month_end_date_ban2"  placeholder="MM" id="month_end_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_month_end_date_ban2" class="error"><?php echo 'Please Enter Valid Month'; ?></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="Number" name="year_end_date_ban2"  placeholder="YYYYY" id="year_end_date_ban2" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_year_end_date_ban2" class="error"><?php echo 'Please Enter Valid Year'; ?></span>
                                                    </div> -->
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="amount_ban"><?php echo "Amount:"; ?></label>
                                                    <div class="col-sm-8">
                                                        
                                                        <input type="number" name="amount_ban" value="" placeholder="<?php echo "Amount"; ?>" id="amount_ban" class="form-control" />
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_amount_ban" class="error"><?php echo 'Please Enter Amount'; ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-trainer_code"><?php echo "Authority:"; ?></label>
                                                    <div class="col-sm-8">
                                                        <!-- <input type="text" name="authority_ban_details" value="" placeholder="<?php echo "Authority"; ?>" id="authority_ban_details" class="form-control" /> -->
                                                        <select name="authority_ban_details" id="authority_ban_details" class="form-control">
                                                            <option value="" selected="selected" disabled="disabled" >Please Select</option>    
                                                            <?php foreach ($authorty_bans as $authorutykey => $authorutyvalue) { ?>
                                                            <option value="<?php echo $authorutyvalue; ?>" ><?php echo $authorutyvalue ?></option>
                                                            <?php } ?>
                                                      </select> 
                                                      <span style="display: none;color: red;font-weight: bold;" id="error_authority_ban" class="error"><?php echo 'Please Select Authority'; ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-stall_certificate"><?php echo "Reason:"; ?></label>
                                                    <div class="col-sm-8">
                                                        <textarea rows="2" name="reason_description_ban" placeholder="" id="reason_ban" class="form-control"></textarea>
                                                        <span style="display: none;color: red;font-weight: bold;" id="error_reason_ban" class="error"><?php echo 'Please Enter Reason'; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="modal-footer">
                                            <input type="hidden" name="id_hidden_BanFunction" value="" id="id_hidden_BanFunction" class="form-control" />
                                            <input type="hidden" name="idban_increment_id" value="" id="idban_increment_id" class="form-control" />
                                            <button type="button" class="btn btn-primary" id = "ban_save_id" onclick="BanFunction()"  >Save</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                          <td class="text-center"><?php echo "Club"; ?></td>
                                          <td class="text-center"><?php echo "Authority"; ?></td>
                                          <td class="text-center"><?php echo "Horse Name"; ?></td>
                                          <td class="text-center"><?php echo "Race No"; ?></td>
                                          <td class="text-center"><?php echo "Race Date"; ?></td>
                                          <td class="text-center"><?php echo "Start Date"; ?></td>
                                          <td class="text-center"><?php echo "End Date"; ?></td>
                                          <td class="text-center"><?php echo "Offences"; ?></td>
                                          <td class="text-center"><?php echo "Fine"; ?></td>
                                          <td class="text-center"><?php echo "Reason"; ?></td>
                                          <td class="text-center"><?php echo "Action"; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody id="bandeatilsbody">
                                        <?php if(isset($bandeatils)) { //echo'<pre>';print_r($bandeatils); exit; ?>
                                            <?php foreach($bandeatils as $bankey => $banvalue) { 
                                                if($banvalue['end_date1'] == '' || $banvalue['end_date1'] == '0000-00-00'){
                                                    $enddatebans = ''; 
                                                } else {
                                                    $enddatebans = date('d/m/Y', strtotime($banvalue['end_date1']));
                                                } 
                                                if($banvalue['start_date1'] == '' || $banvalue['start_date1'] == '0000-00-00'){
                                                    $startdatebans = '';
                                                } else {
                                                    $startdatebans = date('d/m/Y', strtotime($banvalue['start_date1']));
                                                }
                                                if($banvalue['end_date2'] == '' || $banvalue['end_date2'] == '0000-00-00'){
                                                    $enddatebans2 = ''; 
                                                } else {
                                                    $enddatebans2 = date('d/m/Y', strtotime($banvalue['end_date2']));
                                                } 
                                                if($banvalue['start_date2'] == '' || $banvalue['start_date2'] == '0000-00-00'){
                                                    $startdatebans2 = ''; 
                                                } else {
                                                    $startdatebans2 = date('d/m/Y', strtotime($banvalue['start_date2']));
                                                } 
                                                /*if($banvalue['start_date2'] == '' || $banvalue['start_date2'] == '0000-00-00'){
                                                    $startdatebans2 = '';       
                                                } else {
                                                    $startdatebans2 = date('d/m/Y', strtotime($banvalue['start_date2']));
                                                }*/
                                            ?>
                                                <tr id='bandetail_<?php echo $bankey ?>'>
                                                    <td class="text-left"><span id="clubs_<?php echo $bankey ?>" ><?php echo $banvalue['club'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][club]" id="club_<?php echo $bankey ?>"  value="<?php echo $banvalue['club'] ?>">
                                                    </td>
                                                    <td class="text-left"><span id="authorityban_<?php echo $bankey ?>" ><?php echo $banvalue['authority'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][authority]" id="authority_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['authority'] ?>">
                                                    </td>

                                                    <td class="text-left"><span id="horsename_<?php echo $bankey ?>" ><?php echo $banvalue['horse_name'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][horse_name]" id="horsename_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['horse_name'] ?>">
                                                    </td>
                                                    <td class="text-left"><span id="raceno_<?php echo $bankey ?>" ><?php echo $banvalue['race_no'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][race_no]" id="race_no_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['race_no'] ?>">
                                                    </td>
                                                    <td class="text-left"><span id="racedate_<?php echo $bankey ?>" ><?php echo $banvalue['race_date'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][race_date]" id="race_date_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['race_date'] ?>">
                                                    </td>

                                                    <td class="text-left"><span id="start_dateban_<?php echo $bankey ?>"><?php echo $startdatebans; ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][start_date1]" id="start_date_bans_<?php echo $bankey ?>"  value="<?php echo $startdatebans; ?>">
                                                    </td>
                                                    <td class="text-left"><span id="end_dateban_<?php echo $bankey ?>"><?php echo $enddatebans; ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][end_date1]" id="end_date_ban_<?php echo $bankey ?>"  value="<?php echo $enddatebans; ?>">
                                                    </td>
                                                    <td class="text-left"><span id="offences_<?php echo $bankey ?>"><?php echo $banvalue['offences'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][offences]" id="offences_<?php echo $bankey ?>"  value="<?php echo $offences; ?>">
                                                    </td>
                                                    <td class="text-right"><span id="amount_<?php echo $bankey ?>"><?php echo $banvalue['amount'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][amount]" id="amount_<?php echo $bankey ?>"  value="<?php echo $amount; ?>">
                                                    </td>
                                                    
                                                    <td class="text-left"><span  id="reasonban_<?php echo $bankey ?>"><?php echo $banvalue['reason'] ?></span>
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][reason]" id="reason_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['reason'] ?>">
                                                        <!-- <span  id="amountban_<?php echo $bankey ?>"><?php echo $banvalue['amount'] ?></span> -->
                                                        <input type= "hidden"  name="bandats[<?php echo $bankey ?>][amount]" id="amount_ban_<?php echo $bankey ?>"  value="<?php echo $banvalue['amount'] ?>">
                                                    </td>
                                                    <td class="text-center"> 
                                                        <a  onclick='updateban(<?php echo $bankey ?>);' class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                                        <!-- <a onclick="removeBanDetail(<?php echo $banvalue['jockey_id'] ?>,<?php echo $banvalue['ban_id'] ?>,'bandetail_<?php echo $bankey ?>' )" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a> -->
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-11">
                                <h4>Jockey Ban History</h4>
                            </div>
                            <div class="form-group">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                          <td class="text-center"><?php echo "Club"; ?></td>
                                          <td class="text-center"><?php echo "Authority"; ?></td>
                                          <td class="text-center"><?php echo "Horse Name"; ?></td>
                                          <td class="text-center"><?php echo "Race No"; ?></td>
                                          <td class="text-center"><?php echo "Race Date"; ?></td>
                                          <td class="text-center"><?php echo "Start Date"; ?></td>
                                          <td class="text-center"><?php echo "End Date"; ?></td>
                                          <td class="text-center"><?php echo "Offences"; ?></td>
                                          <td class="text-center"><?php echo "Fine"; ?></td>
                                          <td class="text-center"><?php echo "Reason"; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody id="banhistorybody">
                                        <?php if(isset($bandatas)) { //echo'<pre>';print_r($bandatas); exit(); ?>
                                            <?php foreach($bandatas as $bkey => $bvalue) { 
                                                if($bvalue['end_date1'] == '' || $bvalue['end_date1'] == '0000-00-00'){
                                                    $enddatebanshistory = ''; 
                                                } else {
                                                    $enddatebanshistory = date('d/m/Y', strtotime($bvalue['end_date1']));
                                                } 
                                                if($bvalue['start_date1'] == '' || $bvalue['start_date1'] == '0000-00-00'){
                                                    $startdatebanshistory = '';
                                                } else {
                                                    $startdatebanshistory = date('d/m/Y', strtotime($bvalue['start_date1']));
                                                }
                                                if($bvalue['end_date2'] == '' || $bvalue['end_date2'] == '0000-00-00'){
                                                    $enddatebanshistory2 = ''; 
                                                } else {
                                                    $enddatebanshistory2 = date('d/m/Y', strtotime($bvalue['end_date2']));
                                                }

                                                if($bvalue['start_date2'] == '' || $bvalue['start_date2'] == '0000-00-00'){
                                                    $startdatebanshistory2 = ''; 
                                                } else {
                                                    $startdatebanshistory2 = date('d/m/Y', strtotime($bvalue['start_date2']));
                                                }

                                               ?>
                                                <tr id='banhistorybody<?php echo $bkey ?>'>
                                                    <td class="text-left" > 
                                                        <?php echo $bvalue['club'] ?>
                                                        <input type= "hidden" name="banhistorydatas[<?php echo $bkey ?>][history_club]" id="history_club_<?php echo $bkey ?>" value="<?php echo $bvalue['club'] ?>" >
                                                    </td>
                                                    <td class="text-left" >
                                                        <?php echo $bvalue['authority']; ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_authority]" id="history_authority_<?php echo $bkey ?>"  value="<?php echo $bvalue['authority']; ?>" >
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_startdate_ban2]" id="history_startdate_ban2_<?php echo $bkey ?>"  value="<?php echo $startdatebanshistory2; ?>" >

                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_enddate_ban2]" id="history_enddate_ban2_<?php echo $bkey ?>"  value="<?php echo $enddatebanshistory2; ?>" >

                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_reason]" id="history_reason_<?php echo $bkey ?>"  value="<?php echo $bvalue['reason']; ?>" >

                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_amount]" id="history_amount_<?php echo $bkey ?>"  value="<?php echo $bvalue['amount']; ?>" >
                                                    </td>
                                                    <td class="text-left"><?php echo $bvalue['horse_name'] ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][horse_name]" id="horse_name_<?php echo $bkey ?>"  value="<?php echo $horse_name; ?>">
                                                    </td>
                                                    <td class="text-right"><?php echo $bvalue['race_no'] ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][race_no]" id="race_no_<?php echo $bkey ?>"  value="<?php echo $race_no; ?>">
                                                    </td>
                                                    
                                                    <td class="text-left"><?php echo $bvalue['race_date'] ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][race_date]" id="race_date_<?php echo $bkey ?>"  value="<?php echo $bvalue['race_date'] ?>">
                                                    </td>
                                                    <td class="text-right" >
                                                        <?php echo date('d/m/Y', strtotime($bvalue['start_date1'])); ?>
                                                        <input type= "hidden" name="banhistorydatas[<?php echo $bkey ?>][history_startdate_ban]" id="history_startdate_ban_<?php echo $bkey ?>"  value="<?php echo date('d/m/Y', strtotime($startdatebanshistory)); ?>" >
                                                    </td>
                                                    <td class="text-right" >
                                                        <?php echo date('d/m/Y', strtotime($bvalue['end_date1'])); ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][history_enddate_ban]" id="history_enddate_ban_<?php echo $bkey ?>"  value="<?php echo date('d/m/Y', strtotime($enddatebanshistory)); ?>" >
                                                    </td>


                                                    <td class="text-left"><?php echo $bvalue['offences'] ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][offences]" id="offences_<?php echo $bkey ?>"  value="<?php echo $offences; ?>">
                                                    </td>
                                                    <td class="text-right"><?php echo $bvalue['amount'] ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][amount]" id="amount_<?php echo $bkey ?>"  value="<?php echo $amount; ?>">
                                                    </td>
                                                    
                                                    <td class="text-left"><?php echo $bvalue['reason'] ?>
                                                        <input type= "hidden"  name="banhistorydatas[<?php echo $bkey ?>][reason]" id="reason_<?php echo $bkey ?>"  value="<?php echo $bvalue['reason'] ?>">
                                                        <!-- <span  id="amountban_<?php echo $bkey ?>"><?php echo $bvalue['amount'] ?></span> -->
                                                    </td>


                                                    
                                                    
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="tab-pane" id="tab_transaction">
                            <div class="col-sm-5">
                                <h4>Transaction</h4>
                            </div>
                            <div class="form-group">
                                <a target="_blank" href="<?php echo $jockey_license; ?>" data-toggle="tooltip" title="<?php echo 'Jockey License' ?>" class="btn btn-primary">Jockey License</a>
                            </div>
                            <!-- <div class="form-group ">
                                <label class="col-sm-2 control-label" for="input-license_renewal"><?php echo "License Renewal date"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date">
                                        <input type="text" name="license_renewal" value="" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="date_of_license_renewal" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label" for="input-license_amt"><?php echo "Fee paid (in Rs):"; ?></label>
                                <div class="col-sm-3">
                                   <input type="text" name="fees_paid" value="" placeholder="<?php echo "Fee paid (in Rs)"; ?>" id="input-license_amt" class="form-control" tabindex="13" />
                                </div>
                                <button type="button" class="btn btn-primary" id = "" onclick="transaction();" style="">Save</button>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="input-fees_paid_type"><?php echo "Fee Paid Type:" ?></label>
                                <div class="col-sm-3">
                                <select id="input-fees_paid_type" class="form-control" name="fees_paid_type" value="<?php echo $fees_paid_type; ?>" tabindex="12">
                                    <option value="0" disabled="disabled" >Please Select</option>
                                        <?php foreach($fees as $fkey => $fvalue){ ?>
                                            <?php if($fkey == $fees_paid_type){ ?>
                                                <option value="<?php echo $fkey ?>" selected="selected"><?php echo $fvalue; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $fkey ?>"><?php echo $fvalue ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                </select>
                                </div>
                            </div> -->
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td>Sr No</td>
                                                <td>License Renewal date</td>
                                                <td>Amount</td>
                                                <td>Fee Paid Type</td>
                                                <td>License Start Date</td>
                                                <td>License End Date</td>
                                                <td>License Type</td>
                                                <td>Apprenties</td>
                                                <td>Action</td>
                                                <input type= "hidden" name="" id="trainer_idss"  value="<?php echo $trainer_idss ?>">
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($Trainers)) { ?>
                                                <?php $i=1; ?>
                                                <?php foreach ($Trainers as $result) { ?>
                                                    <tr>
                                                        <td class="text-left"><?php echo $i++; ?></td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo date('d-m-Y', strtotime($result['renewal_date'])) ?>">
                                                        </td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['amount'] ?>">
                                                        </td>
                                                        <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['fees'] ?>">
                                                        </td>
                                                         <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo date('d-m-Y', strtotime($result['license_start_date'])) ?>">
                                                        </td>
                                                         <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo date('d-m-Y', strtotime($result['license_end_date'])) ?>">
                                                        </td>
                                                         <td>
                                                            <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="renewal_datess"  value="<?php echo $result['license_type'] ?>">
                                                        </td>
                                                        <td class="text-center">
                                                            <?php if($result['apprenties'] == 'Yes'){ ?>
                                                                <i class="fa fa-check" aria-hidden="true" style="color: green;font-size: 20px;"></i> 
                                                            <?php } else { ?>
                                                                <i></i>
                                                            <?php } ?>
                                                        </td>
                                                        <td>
                                                            <a onclick="removes(<?php echo $result['id'] ?>);" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                            <tr id="hidden_transaction" style="display: none;" >
                                                <td class="text-left">10</td>
                                                <td>
                                                    <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="hidden_renewal_datess"  value="<?php echo $result['renewal_date'] ?>">
                                                </td>
                                                <td>
                                                    <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="hidden_renewal_amt"  value="<?php echo $result['renewal_date'] ?>">
                                                </td>
                                                <td>
                                                    <input type= "text" style="border: none;border-color: transparent;background-color: transparent;outline: none;" name="" id="hidden_renewal_fee"  value="<?php echo $result['renewal_date'] ?>">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var itrUpload = $('#upload_hidden_id').val();
        function ITR_Upload() {
            html  = '';
            html += '<tr id="recurring-row' + itrUpload + '">';
                /*html += '  <td class="left">';
                    html += '<label>'+itrUpload+'</label>';
                html += '  </td>';*/
                html += '  <td class="left">';
                    html += '  <input type="text" name="itrdatas[' + itrUpload + '][year]" value="" placeholder="Assesment Year" class="form-control" />';
                html += '  </td>';
                html += '  <td class="left">';
                    html += '  <input type="text" readonly id="image_' + itrUpload + '"  name="itrdatas[' + itrUpload + '][image]" value="" placeholder="choose file" class="form-control" />';
                    html += '  <input type="hidden" id="image_path_' + itrUpload + '" name="itrdatas[' + itrUpload + '][imagepath]" value=""  class="form-control" />';
                    html += '<button type="button" class="button-upload btn btn-default" id="button-upload_'+ itrUpload + '" data-loading-text="<?php echo 'Please Wait'; ?>" style="margin-top: 5px;"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>';
                    html += '<span id="button-other_document_'+itrUpload+'"></span>';
                html += '  </td>';
                html += '  <td class="left">';
                    html += '    <a onclick="$(\'#recurring-row' + itrUpload + '\').remove()" data-toggle="tooltip" title="<?php echo "BTN Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
                html += '  </td>';
            html += '</tr>';

            $('#itrUpload tbody').append(html);
             itrUpload++;
        }
    </script>

    <script type="text/javascript">
        $(document).delegate('.button-upload', 'click', function() {
            idss = $(this).attr('id');
            s_id = idss.split('_');
            id = s_id[1];
            $('#form_upload_itr').remove();
            $('body').prepend('<form enctype="multipart/form-data" id="form_upload_itr" style="display: none;"><input type="file" name="file" /></form>');
            $('#form_upload_itr input[name=\'file\']').trigger('click');
            if (typeof timer != 'undefined') {
                    clearInterval(timer);
            }
            timer = setInterval(function() {
                if ($('#form_upload_itr input[name=\'file\']').val() != '') {
                    clearInterval(timer);   
                    id = id;
                    $.ajax({
                        url: 'index.php?route=catalog/jockey/upload1&token=<?php echo $token; ?>'+'&jockey_id='+id,
                        type: 'post',   
                        dataType: 'json',
                        data: new FormData($('#form_upload_itr')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,   
                        beforeSend: function() {
                            $('#button-upload').button('loading');
                        },
                        complete: function() {
                            $('#button-upload').button('reset');
                        },  
                        success: function(json) {
                            if (json['error']) {
                                alert(json['error']);
                            }
                            if (json['success']) {
                                alert(json['success']);
                                console.log(json);
                                $('#image_'+json['id']).attr('value', json['filename']);
                                $('#image_path_'+json['id']).attr('value', json['filepath']);
                                d = new Date();
                            // var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="itrdatas[<?php echo $key ?>][imagepath]" href="'+json['link_href']+'">View Document</a>';
                            // $('#file_source').remove();
                            // $('#button-other_document_1_new_'+json['id']).html(previewHtml);
                            // }
                            var previewHtml = '<a target="_blank" class = "btn btn-default" style="cursor: pointer;margin-left:5px;" id = "btn_view_'+json['id']+'" href="'+json['filepath']+'">View Document</a>';
                                $('#btn_view_'+json['id']).remove();
                                $('#button-other_document_'+json['id']).append(previewHtml);}
                        },      
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            }, 500);
        });

    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });

        $( "#input-jockey_returned_by" ).change(function(){
            var jockey_returned_by =  $(this).val()
            if(jockey_returned_by == 'Y'){
                $('.add-owner-div').show();
            } else {
                $('.add-owner-div').hide();
            }
        });


        $('#license_type').change(function() {
        var select_b =  $('#license_type').val();
            if (select_b == 'B') {
                $('#center_holding_hide').show();
               
            } else {
                $('#center_holding_hide').hide();
            }
        });

        $( document ).ready(function() {
            var select_h =  $('#license_type').val();
            if (select_h == 'B') {
                $('#center_holding_hide').show();
                
               
            } else {
                $('#center_holding_hide').hide();
                
            }
        });

        $( document ).ready(function() {
            $('#jockey_code').focus();
        });
        $("#day_date_of_birth").keyup(function()  {
            if(this.value.length == 2 && parseInt($(this).val()) <= 31 ){
                $('#error_day_date_of_birth').hide();
                $( "#month_date_of_birth" ).focus();
            } else {
                $('#error_day_date_of_birth').show();
            }
        });
        // $("#month_date_of_birth").keyup(function()  {
        //      if(this.value.length == 2 && parseInt($(this).val()) <= 12){
        //         $('#error_month_date_of_birth').hide();
        //         $( "#year_date_of_birth" ).focus();
        //     } else {
        //         $('#error_month_date_of_birth').show();
        //     }
        // });
        // $("#year_date_of_birth").keyup(function()  {
        //      if(this.value.length == 4){
        //         $('#error_year_date_of_birth').hide();
        //         $( "#origin" ).focus();
        //      } else {
        //         $('#error_year_date_of_birth').show();
        //      }
        // });
        $("#day_date_of_license_renewal").keyup(function()  {
            if(this.value.length == 2 && parseInt($(this).val()) <= 31 ){
                $('#error_day_date_of_license_renewal').hide();
                $( "#month_date_of_license_renewal" ).focus();
            } else {
                $('#error_day_date_of_license_renewal').show();
            }
        });
        $("#month_date_of_license_renewal").keyup(function()  {
             if(this.value.length == 2 && parseInt($(this).val()) <= 12){
                $('#error_month_date_of_license_renewal').hide();
                $( "#year_date_of_license_renewal" ).focus();
            } else {
                $('#error_month_date_of_license_renewal').show();
            }
        });
        $("#year_date_of_license_renewal").keyup(function()  {
             if(this.value.length == 4){
                $('#error_year_date_of_license_renewal').hide();
                $( "#day_license_end_date").focus();
             } else {
                $('#error_year_date_of_license_renewal').show();
             }
        });
        $("#day_license_end_date").keyup(function()  {
            if(this.value.length == 2 && parseInt($(this).val()) <= 31 ){
                $('#error_day_license_end_date').hide();
                $( "#month_license_end_date" ).focus();
            } else {
                $('#error_day_license_end_date').show();
            }
        });
        $("#month_license_end_date").keyup(function()  {
             if(this.value.length == 2 && parseInt($(this).val()) <= 12){
                $('#error_month_license_end_date').hide();
                $( "#year_license_end_date" ).focus();
            } else {
                $('#error_month_license_end_date').show();
            }
        });
        $("#year_license_end_date").keyup(function()  {
             if(this.value.length == 4){
                $('#error_year_license_end_date').hide();
                $( "#fee_paid_type").focus();
             } else {
                $('#error_year_license_end_date').show();
             }
        });

        $("#day_license_issued_date").keyup(function()  {
            if(this.value.length == 2 && parseInt($(this).val()) <= 31 ){
                $('#error_day_license_issued_date').hide();
                $( "#month_license_issued_date" ).focus();
            } else {
                $('#error_day_license_issued_date').show();
            }
        });
        $("#month_license_issued_date").keyup(function()  {
             if(this.value.length == 2 && parseInt($(this).val()) <= 12){
                $('#error_month_license_issued_date').hide();
                $( "#year_license_issued_date" ).focus();
            } else {
                $('#error_month_license_issued_date').show();
            }
        });
        $("#year_license_issued_date").keyup(function()  {
             if(this.value.length == 4){
                $('#error_year_license_issued_date').hide();
                $( "#license_type").focus();
             } else {
                $('#error_year_license_issued_date').show();
             }
        });

        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            console.log(name);
            console.log(class_name);
            console.log(id);
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'jockey_code'){
                    $('#racing_name').focus();
                }
                if(id == 'racing_name'){
                    $('#jockey_name').focus();
                }
                if(id == 'jockey_name'){
                    $('#jaimember').focus();
                }
                if(id == 'jaimember'){
                    $('#day_date_of_birth').focus();
                }
                if(id == 'day_date_of_birth'){
                    $('#month_date_of_birth').focus();
                }
                if(id == 'month_date_of_birth'){
                    $('#year_date_of_birth').focus();
                }
                if(id == 'year_date_of_birth'){
                    $('#origin').focus();
                }
                if(id == 'origin'){
                    $('#day_license_issued_date').focus();
                }
                if(id == 'day_license_issued_date'){
                    $('#month_license_issued_date').focus();
                }
                if(id == 'month_license_issued_date'){
                    $('#year_license_issued_date').focus();
                }
                 if(id == 'year_license_issued_date'){
                    $('#license_type').focus();
                }
                if(id == 'license_type'){
                    if ($('#center_holding_hide').is(':visible')) {
                        $('#center_holding').focus();
                    } else {
                         $('#day_date_of_license_renewal').focus();
                    }
                }
                if(id == 'center_holding'){
                    $('#day_date_of_license_renewal').focus();
                }
                if(id == 'day_date_of_license_renewal'){
                    $('#month_date_of_license_renewal').focus();
                }
                if(id == 'month_date_of_license_renewal'){
                    $('#year_date_of_license_renewal').focus();
                }
                if(id == 'year_date_of_license_renewal'){
                    $('#day_license_end_date').focus();
                }
                if(id == 'day_license_end_date'){
                    $('#month_license_end_date').focus();
                }
                if(id == 'month_license_end_date'){
                    $('#year_license_end_date').focus();
                }
                if(id == 'year_license_end_date'){
                    $('#fee_paid_type').focus();
                }
                if(id == 'fee_paid_type'){
                    $('#fee_paid').focus();
                }
                if(id == 'fee_paid'){
                    $('#ride_weight').focus();
                }
                if(id == 'ride_weight'){
                    $('#file_number').focus();
                }
                if(id == 'file_number'){
                    $('#blood_group').focus();
                }
                if(id == 'blood_group'){
                    $('#button-file').focus();
                }
                if(id == 'button-file'){
                    $('#no_whip').focus();
                }

                if(id == 'no_whip'){
                    $('#apprentice').focus();
                }
                if(id == 'apprentice'){
                    $('#allowance_claiming').focus();
                }
                if(id == 'allowance_claiming'){
                    $('#total_wins').focus();
                }
                if(id == 'total_wins'){
                    $('#master_trainer').focus();
                }

                if(id == 'master_trainer'){
                    $('#remarks').focus();
                }
                if(id == 'remarks'){
                    $('#jockey_code').focus();
                }
            }
        });
        $('#button-file').on('click', function() {
        $('#form-other_file').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-other_file" style="display: none;"><input type="file" name="file" /></form>');
        $('#form-other_file input[name=\'file\']').trigger('click');
        if (typeof timer != 'undefined') {
              clearInterval(timer);
        }
        timer = setInterval(function() {
            if ($('#form-other_file input[name=\'file\']').val() != '') {
              clearInterval(timer); 
              image_name = 'jockey';  
              $.ajax({ 
                url: 'index.php?route=catalog/jockey/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
                type: 'post',   
                dataType: 'json',
                data: new FormData($('#form-other_file')[0]),
                cache: false,
                contentType: false,
                processData: false,   
                beforeSend: function() {
                  $('#button-upload').button('loading');
                },
                complete: function() {
                  $('#button-upload').button('reset');
                },  
                success: function(json) {
                  if (json['error']) {
                    alert(json['error']);
                  }
                  if (json['success']) {
                    alert(json['success']);
                    console.log(json);
                    $('input[name=\'file\']').attr('value', json['filename']);
                    $('input[name=\'file_source\']').attr('value', json['link_href']);
                    d = new Date();
                    var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="awbi_registration_file_source" href="'+json['link_href']+'">View Document</a>';
                    $('#file_source').remove();
                    $('#button-file_new').append(previewHtml);
                  }
                },      
                error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
              });
            }
          }, 500);
        });
    </script>
    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
        $(document).on('keydown', '.form-control', '.radio-inline' , function(e) {
        var name = $(this).attr('name'); 
        var class_name = $(this).attr('class'); 
        var id = $(this).attr('id');
        console.log(name);
        console.log(class_name);
        console.log(id);
        var value = $(this).val();
        if(e.which == 13){
            if(id == 'input-address1'){
                $('#input-address_3').focus();
            }
            if(id == 'input-address_3'){
                $('#input-localArea1').focus();
            }
            if(id == 'input-localArea1'){
                $('#input-city1').focus();
            }
            if(id == 'input-city1'){
                $('#input-country').focus();
            }
            if(id == 'input-country'){
                $('#zone_id').focus();
            }
            if(id == 'zone_id'){
                $('#pincode1').focus();
            }
            if(id == 'pincode1'){
                $('#input-address2').focus();
            }
            if(id == 'input-address2'){
                $('#input-address_4').focus();
            }
            if(id == 'input-address_4'){
                $('#input-localArea2').focus();
            }
            if(id == 'input-localArea2'){
                $('#input-city2').focus();
            }
            if(id == 'input-city2'){
                $('#input-country2').focus();
            }
            if(id == 'input-country2'){
                $('#zone_id2').focus();
            }
            if(id == 'zone_id2'){
                $('#pincode2').focus();
            }
            if(id == 'pincode2'){
                $('#phone_no').focus();
            }
            if(id == 'phone_no'){
                $('#mobile_no1').focus();
            }
            if(id == 'mobile_no1'){
                $('#email_id').focus();
            }
            if(id == 'email_id'){
                $('#alternate_mob_no').focus();
            }
            if(id == 'alternate_mob_no'){
                $('#alternate_email_id').focus();
            }
            if(id == 'alternate_email_id'){
                $('#gst_type').focus();
            }
            if(id == 'gst_type'){
                $('#gst_no').focus();
            }
            if(id == 'gst_no'){
                $('#pan_no').focus();
            }
            if(id == 'pan_no'){
                $('#prof_tax_no').focus();
            }
            if(id == '#prof_tax_no'){
                $('#phone_no').focus();
            }
        }
    });
    </script>
  
   
    <script type="text/javascript"><!--
        $('#language a:first').tab('show');
    </script>



     <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                //SaveOwnershp();
            }
        });
        $("#date_start_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 31 && parseInt($(this).val()) > 0  ){
                    $('#error_date_start_date_ban').hide();
                    $( "#month_start_date_ban" ).focus();
                } else {
                    $('#error_date_start_date_ban').show();
                }
            } else {
                e.preventDefault();
            }
        });
        $("#month_start_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)){
                if(this.value.length == 2 && parseInt($(this).val()) <= 12 && parseInt($(this).val()) > 0 ){
                    $('#error_month_start_date_ban').hide();
                    $( "#year_start_date_ban" ).focus();
                } else {
                    $('#error_month_start_date_ban').show();
                }
            }else {
                e.preventDefault();
            }
        });
        $("#year_start_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                 if(this.value.length == 4){
                    $('#error_year_start_date_ban').hide();
                    $( "#date_end_date_ban" ).focus();
                 } else {
                    $('#error_year_start_date_ban').show();
                 }
            } else {
                e.preventDefault();
            }
        });
        $("#date_end_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105)|| (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 31 && parseInt($(this).val()) > 0 ){
                    $('#error_date_end_date_ban').hide();
                    $( "#month_end_date_ban" ).focus();
                } else {
                    $('#error_date_end_date_ban').show();
                }
            } else {
                e.preventDefault();
            }
        });
        $("#month_end_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 12 && parseInt($(this).val()) > 0 ){
                    $('#error_month_end_date_ban').hide();
                    $( "#year_end_date_ban" ).focus();
                } else {
                    $('#error_month_end_date_ban').show();
                }
            } else {
                e.preventDefault();
            }
        });
        $("#year_end_date_ban").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)){
                 if(this.value.length == 4){
                    $('#error_year_end_date_ban').hide();
                    $( "#date_start_date_ban2" ).focus();
                 } else {
                    $('#error_year_end_date_ban').show();
                 }
            } else {
                e.preventDefault();
            }
        });
        $("#date_start_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 31 && parseInt($(this).val()) > 0  ){
                    $('#error_date_start_date_ban2').hide();
                    $( "#month_start_date_ban2" ).focus();
                } else {
                    $('#error_date_start_date_ban2').show();
                }
            } else {
                e.preventDefault();
            }
        });
        $("#month_start_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)){
                if(this.value.length == 2 && parseInt($(this).val()) <= 12 && parseInt($(this).val()) > 0 ){
                    $('#error_month_start_date_ban2').hide();
                    $( "#year_start_date_ban2" ).focus();
                } else {
                    $('#error_month_start_date_ban2').show();
                }
            }else {
                e.preventDefault();
            }
        });
        $("#year_start_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                 if(this.value.length == 4){
                    $('#error_year_start_date_ban2').hide();
                    $( "#date_end_date_ban2" ).focus();
                 } else {
                    $('#error_year_start_date_ban2').show();
                 }
            } else {
                e.preventDefault();
            }
        });
        $("#date_end_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105)|| (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 31 && parseInt($(this).val()) > 0 ){
                    $('#error_date_end_date_ban2').hide();
                    $( "#month_end_date_ban2" ).focus();
                } else {
                    $('#error_date_end_date_ban2').show();
                }
            } else {
                e.preventDefault();
            }
        });
        $("#month_end_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) {
                if(this.value.length == 2 && parseInt($(this).val()) <= 12 && parseInt($(this).val()) > 0 ){
                    $('#error_month_end_date_ban2').hide();
                    $( "#year_end_date_ban2" ).focus();
                } else {
                    $('#error_month_end_date_ban2').show();
                }
            } else {
                e.preventDefault();
            }

        });
        $("#year_end_date_ban2").keyup(function(e)  {
            if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)){
                 if(this.value.length == 4){
                    $('#error_year_end_date_ban2').hide();
                    $( "#amount_ban" ).focus();
                 } else {
                    $('#error_year_end_date_ban2').show();
                 }
            } else {
                e.preventDefault();
            }
        });
        // Ban details tab
        function BanFunction(){
            var date_start_date_ban =  $( "#date_start_date_ban" ).val();
            var end_date_ban =  $( "#date_end_date_ban" ).val();
            var date_start_date_ban2 =  $( "#date_start_date_ban2" ).val();
            var end_date_ban2 =  $( "#date_end_date_ban2" ).val();
            var auto_id = $('#id_hidden_band').val();
            var id_hidden_BanFunction = $('#id_hidden_BanFunction').val();
            
            if (end_date_ban != '') {

                var string_start_date = date_start_date_ban.split('-');
                var s_date  = string_start_date[0];
                var s_month = string_start_date[1];
                var s_year  = string_start_date[2];

                var string_end_date = end_date_ban.split('-');
                var e_date  = string_end_date[0];
                var e_month = string_end_date[1];
                var e_year  = string_end_date[2];

                if (s_year > e_year){
                    $('#error_greater_date_start_date_ban').show();
                    return false;
                } else if (e_month > s_month) {
                    if (s_date > e_date || e_date > s_date) {
                        $('#error_greater_date_start_date_ban').hide();
                    } 
                } else if(s_month > e_month) {
                    $('#error_greater_date_start_date_ban').show();
                    return false;
                } else if(s_month == e_month) {
                    if (s_date > e_date) {
                        $('#error_greater_date_start_date_ban').show();
                        return false;
                    }
                }
            }

            if (end_date_ban2 != '') {
                
                var string_start_date2 = date_start_date_ban2.split('-');
                var s_date2  = string_start_date2[0];
                var s_month2 = string_start_date2[1];
                var s_year2  = string_start_date2[2];

                var string_end_date2 = end_date_ban2.split('-');
                var e_date2  = string_end_date2[0];
                var e_month2 = string_end_date2[1];
                var e_year2  = string_end_date2[2];

                if (s_year2 > e_year2){
                    $('#error_greater_date_start_date_ban2').show();
                    return false;
                } else if (e_month2 > s_month2) {
                    if (s_date2 > e_date2 || e_date2 > s_date2) {
                        $('#error_greater_date_start_date_ban2').hide();
                    } 
                } else if(s_month2 > e_month2) {
                    $('#error_greater_date_start_date_ban2').show();
                    return false;
                } else if(s_month2 == e_month2) {
                    if (s_date2 > e_date2) {
                        $('#error_greater_date_start_date_ban2').show();
                        return false;
                    }
                }
            }

            if(date_start_date_ban != ''){
                $('#error_date_start_date_ban').hide();
                var date_start_date_bans = date_start_date_ban;
            } else {
                $('#error_date_start_date_ban').show();
            }

            if( end_date_ban != ''){
                $('#error_date_end_date_ban').hide();
                var end_date_ban_ends = end_date_ban;
            } else {
                $('#error_date_end_date_ban').show();
            }

            if(date_start_date_ban2 != ''){
                $('#error_date_start_date_ban2').hide();
                var date_start_date_bans2 = date_start_date_ban2;
            } else {
                $('#error_date_start_date_ban2').show();
            }

            if( end_date_ban2 != ''){
                $('#error_date_end_date_ban2').hide();
                var end_date_ban_ends2 = end_date_ban2;
            } else {
                $('#error_date_end_date_ban2').show();
            }

            var club = $('#input-club').val();
            if(club != null && club != ''){
                $('#error_club_ban').hide();
                clubs = club;
            } else {
                $('#error_club_ban').show();
            }
            var authority_ban = $('#authority_ban_details').val();
            if(authority_ban != '' && authority_ban !=  null){
                $('#error_authority_ban').hide();
                var authority_bans = authority_ban.replace(/\s\s+/g, ' ');
            } else {
                $('#error_authority_ban').show();
            }
            var reason_bans = $('#reason_ban').val();
            if(reason_bans != ''  ){
                $('#error_reason_ban').hide();
            } else {
                $('#error_reason_ban').show();
            }
            var amount_bans = $('#amount_ban').val();
            if(amount_bans != ''  ){
                $('#error_amount_ban').hide();
            } else {
                $('#error_amount_ban').show();
            }
            if (date_start_date_ban == ''){
                return false;
            } else {
                if(id_hidden_BanFunction == '0'){ 
                    var start_date_bans = date_start_date_ban; //getstart_dare 
                    if(end_date_ban_ends != ''){
                        var end_date_bans = end_date_ban;  //getend days
                    } else {
                        var end_date_bans = '';
                    }

                    if(date_start_date_bans2 != ''){
                        var start_date_bans2 = date_start_date_ban2;  //getend days

                    } else {
                        var start_date_bans2 = '';
                    }
                    if(end_date_ban_ends2 != ''){
                        var end_date_bans2 = end_date_ban2;  //getend days
                    } else {
                        var end_date_bans2 = '';
                    }
                    html = '<tr id ="bandetail_'+auto_id+'">';
                        html += '<td class="text-left">';
                            html += '<span id="clubs_'+auto_id+'">'+clubs+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][club]" id="club_'+auto_id+'" value = '+clubs+'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="start_dateban_'+auto_id+'">'+start_date_bans+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][start_date1]" id="start_date_bans_'+auto_id+'" value = '+start_date_bans+'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="end_dateban_'+auto_id+'">'+end_date_bans+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][end_date1]" id="end_date_ban_'+auto_id+'" value = '+end_date_bans+'>';
                        html += '</td>';

                        html += '<td class="text-right">';
                            html += '<span id="start_dateban2_'+auto_id+'">'+start_date_bans2+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][start_date2]" id="start_date_bans2_'+auto_id+'" value = '+start_date_bans2+'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="end_dateban2_'+auto_id+'">'+end_date_bans2+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][end_date2]" id="end_date_ban2_'+auto_id+'" value = '+end_date_bans2+'>';
                        html += '</td>';

                        html += '<td class="text-right">';
                            html += '<span id="authorityban_'+auto_id+'">'+authority_bans+ '</span>';
                            html += '<input type= "hidden"  name= "bandats['+auto_id+'][authority]" id="authority_ban_'+auto_id+'" value = \'' + authority_bans + '\'>';
                        html += '</td>';
                        html += '<td class="text-right">';
                            html += '<span id="reasonban_'+auto_id+'">'+reason_bans+ '</span>';
                            html += '<input type= "hidden" name= "bandats['+auto_id+'][reason]" id="reason_ban_'+auto_id+'" value = \'' + reason_bans + '\'>';
                           /* html += '<span id="amountban_'+auto_id+'">'+amount_bans+ '</span>';*/
                            html += '<input type= "hidden" name= "bandats['+auto_id+'][amount]" id="amount_ban_'+auto_id+'" value = \'' + amount_bans + '\'>'; 
                        html += '</td>';
                       
                        html += '<td class="text-center">';
                            html += '<a onclick=updateban("'+auto_id+'") class="btn btn-primary"><i class="fa fa-pencil"></i></a>&nbsp';
                            // html += '<a onclick=removeBanDetail(0,0,"bandetail_'+auto_id+'") class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
                            html += ' </td>';
                        html += '</tr >';
                    auto_id++;
                    $('#bandeatilsbody').append(html);
                    $('#id_hidden_band').val(auto_id);
                } else {
                    var old_start_date_ban = date_start_date_bans; //getstart_dare 
                    if(end_date_ban_ends != ''){
                        var old_end_date_ban = end_date_ban_ends;  //getend days
                    } else {
                        var old_end_date_ban = '';
                    }
                    if(date_start_date_ban2 != ''){
                        var old_start_date_ban2 = date_start_date_ban2;  //getend days
                    } else {
                        var old_start_date_ban2 = '';
                    }
                    if(end_date_ban_ends2 != ''){
                        var old_end_date_ban2 = end_date_ban_ends2;  //getend days
                    } else {
                        var old_end_date_ban2 = '';
                    }
                    var idban_increment_id = $('#idban_increment_id').val();
                    $('#start_date_bans_'+idban_increment_id+'').val(old_start_date_ban);
                    $('#end_date_ban_'+idban_increment_id+'').val(old_end_date_ban);
                    $('#start_date_bans2_'+idban_increment_id+'').val(old_start_date_ban2);
                    $('#end_date_ban2_'+idban_increment_id+'').val(old_end_date_ban2);
                    $('#reason_ban_'+idban_increment_id+'').val(reason_bans);
                    $('#amount_ban_'+idban_increment_id+'').val(amount_bans);
                    $('#authority_ban_'+idban_increment_id+'').val(authority_bans);
                    $('#club_'+idban_increment_id+'').val(clubs);
                    $('#start_dateban_'+idban_increment_id+'').html(old_start_date_ban);
                    $('#end_dateban_'+idban_increment_id+'').html(old_end_date_ban);
                    $('#start_dateban2_'+idban_increment_id+'').html(old_start_date_ban2);
                    $('#end_dateban2_'+idban_increment_id+'').html(old_end_date_ban2);
                    $('#reasonban_'+idban_increment_id+'').html(reason_bans);
                    $('#amountban_'+idban_increment_id+'').html(amount_bans);
                    $('#authorityban_'+idban_increment_id+'').html(authority_bans);
                    $('#clubs_'+idban_increment_id+'').html(clubs);
                }
            }
            $("#myModal2").modal("toggle");
        }
        //blanked value for Ban details Tab
        function closeaddbuu1(){
            $('#input-club').val('');
            $('#reason_ban').val('');
            $('#amount_ban').val('');
            $('#authority_ban_details').val('');
            $('#id_hidden_BanFunction').val(0);
            $("#date_start_date_ban" ).val('');
            $("#date_end_date_ban" ).val('');
            $('#error_greater_date_start_date_ban').hide();
            $('#error_greater_date_start_date_ban2').hide();
            $('#error_club_ban').hide();
            $('#error_authority_ban').hide();
            $('#error_date_start_date_ban').hide();
            $('#error_date_end_date_ban').hide();
            $('#error_reason_ban').hide();
            $('#error_amount_ban').hide();
            $("#date_start_date_ban2" ).val('');
            $("#date_end_date_ban2" ).val('');
            $('#error_date_start_date_ban2').hide();
            $('#error_date_end_date_ban2').hide();
        }
        $('#myModal2').on('shown.bs.modal', function () {
            $('#input-club').focus();
        }); 
        //update function ban details tab
        function updateban(auto_id){
            $('#error_greater_date_start_date_ban').hide();
            $('#error_greater_date_start_date_ban2').hide();
            $('#myModal2').modal('show');
            $('#idban_increment_id').val(auto_id);
            $('#id_hidden_BanFunction').val(1);
            var start_date_ban = $('#start_date_bans_'+auto_id+'').val();
            var end_date_ban = $('#end_date_ban_'+auto_id+'').val();
            var start_date_ban2 = $('#start_date_bans2_'+auto_id+'').val();
            var end_date_ban2 = $('#end_date_ban2_'+auto_id+'').val();
            var reason_ban = $('#reason_ban_'+auto_id+'').val();
            var amount_ban = $('#amount_ban_'+auto_id+'').val();
            var authority_ban = $('#authority_ban_'+auto_id+'').val();
            var club_ban = $('#club_'+auto_id+'').val();
            $("#date_start_date_ban").val(start_date_ban);
            $( "#date_end_date_ban" ).val(end_date_ban);
            $("#date_start_date_ban2").val(start_date_ban2);
            $( "#date_end_date_ban2" ).val(end_date_ban2);
            $('#authority_ban_details').val(authority_ban);
            $('#reason_ban').val(reason_ban);
            $('#amount_ban').val(amount_ban);
            $('#input-club').val(club_ban);
        }
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'input-club'){
                    $('#date_start_date_ban').focus();
                }
                if(id == 'date_start_date_ban'){
                    $('#date_end_date_ban').focus();
                }
                if(id == 'date_end_date_ban'){
                    $('#date_start_date_ban2').focus();
                }
                if(id == 'date_start_date_ban2'){
                    $('#date_end_date_ban2').focus();
                }
                if(id == 'date_end_date_ban2'){
                    $('#amount_ban').focus();
                }
                if(id == 'amount_ban'){
                    $('#authority_ban_details').focus();
                }
                if(id == 'authority_ban_details'){
                    $('#reason_ban').focus();
                }
                if(id == 'reason_ban'){
                    $('#ban_save_id').focus();
                }
            }
        });

        function removeBanDetail(jockey_id,ban_id,id_remove){
            if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
                if(jockey_id == '0' && ban_id == '0'){
                    alert('Delete Record Sucessfully');
                    $('#'+id_remove+'').closest("tr").remove();
                    return false;
                }
                $.ajax({
                    url:'index.php?route=catalog/jockey/deletebandeatils&token=<?php echo $token; ?>'+'&jockey_id='+jockey_id+'&ban_id='+ban_id,
                    method: "POST",
                    dataType: 'json',
                    success: function(json)
                    {
                        $('#'+id_remove+'').closest("tr").remove();
                        alert(json['success']);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error deleting data');
                    }
                });
                return false;
            }
        }

        $( "#gst_type" ).click(function(){
            var gst_no =  $(this).val();
            if(gst_no == 'R'){
                $("#gst_label").show();
                $("#gst_no").show();
            } else {
                $("#gst_label").hide();
                $("#gst_no").hide();
            }
        });



        $('#gst_no').keyup(function(){
            gst_len = parseInt($("#gst_no").val().length);
           // console.log(gst_len);
            $('.gst_error').html('');
            if(gst_len > 15 || gst_len < 15){
                $('.gst_error').css("color", "red")
                $('.gst_error').append('GST No should be 15 digits!');

                //return false;
            } 

            if(gst_len == 15 ){
                $('.gst_error').html('');
            }   
        });

        $('#pan_no').keyup(function(){
            pan_len = parseInt($("#pan_no").val().length);
           // console.log(gst_len);
            $('.pan_error').html('');
            if(pan_len > 10 || pan_len < 10){
                $('.pan_error').css("color", "red")
                $('.pan_error').append('PAN No should be 10 digits!');

                //return false;
            } 

            if(pan_len == 10 ){
                $('.pan_error').html('');
            }   
        })

        function transaction(){
            //alert('inn');
            var trainer_idss = $('#trainer_idss').val();
            var dates = $('#date_of_license_renewal').val();
            var amts = $('#input-license_amt').val();
            var feess = $('#input-fees_paid_type').val();

            $.ajax({
                url:'index.php?route=catalog/jockey/transaction&token=<?php echo $token; ?>'+'&dates='+dates+'&amts='+amts+'&feess='+feess+'&trainer_idss='+trainer_idss,
                method: "POST",
                dataType: 'json',
                success: function(json)
                {
                    if (json['success'] == '1') {
                        var date = $('#date_of_license_renewal').val();
                        var amt = $('#input-license_amt').val();
                        var fees = $('#input-fees_paid_type').val();
                        $('#hidden_renewal_datess').val(date);
                        $('#hidden_renewal_amt').val(amt);
                        $('#hidden_renewal_fee').val(fees);
                        $("#hidden_transaction").show();
                        location.reload();
                    }
                }
            });
        }

        function removes($id){
            var id = $id;
            $.ajax({
                url:'index.php?route=catalog/jockey/delete_transaction&token=<?php echo $token; ?>'+'&id='+id,
                method: "POST",
                dataType: 'json',
                success: function(json)
                {
                    if (json['success'] == '1') {
                        location.reload();
                    }
                }
            });
        }
    </script>
    <script type="text/javascript">
        function country(element, zone_id) {
            $.ajax({
                url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + element.value,
                dataType: 'json',
                beforeSend: function() {
                    $('select[name=\'country1\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    console.log(json['zone']);
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'country1[postcode]\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'country1[postcode]\']').parent().parent().removeClass('required');
                    }

                    html = '<option value="">Please Select</option>';

                    if (json['zone'] && json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                            if (json['zone'][i]['zone_id'] == zone_id) {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="0">None</option>';
                    }

                    $('select[name=\'zone_id\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }

        $('select[name$=\'[country_id]\']').trigger('change');


        function countrys(element, zone_id) {
            $.ajax({
                url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + element.value,
                dataType: 'json',
                beforeSend: function() {
                    $('select[name=\'country2\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    console.log(json['zone']);
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'country[postcode]\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'country[postcode]\']').parent().parent().removeClass('required');
                    }

                    html = '<option value="">Please Select</option>';

                    if (json['zone'] && json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                            if (json['zone'][i]['zone_id'] == zone_id) {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="0">None</option>';
                    }

                    $('select[name=\'zone_id2\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }

        $('select[name$=\'[country_id]\']').trigger('change');

    </script>
</div>
<script type="text/javascript">
$( document ).ready(function() {
var apprentice =  $('#apprentice').val();
var ages = $('#input-age').val();
//alert(assistant_trainer);div_master_trainer
    if((apprentice == 'Yes') && (ages < 25)){
        $('#allowance_claiming_l').show();
        $('#allowance_claiming_div').show();
        $('#div_master_trainer').show();
    } else {
        $('#allowance_claiming_l').hide();
        $('#allowance_claiming_div').hide();
        $('#div_master_trainer').hide();
    }
});
$( "#apprentice" ).change(function(){
var apprentice =  $(this).val()
var ages = $('#input-age').val();
console.log(apprentice);
    if((apprentice == 'Yes') && (ages < 25)){
        $('#allowance_claiming_l').show();
        $('#allowance_claiming_div').show();
        $('#div_master_trainer').show();
    } else {
        $('#allowance_claiming_l').hide();
        $('#allowance_claiming_div').hide();
        $('#div_master_trainer').hide();
    }
});

// $(document).on('input', '#input-age', function(){
//     alert("inn");
// });

setInterval(function() {
    var apprentice =  $(this).val()
    var ages = $('#input-age').val();
    if((apprentice == 'Yes') && (ages < 25)){
        $('#allowance_claiming_l').show();
        $('#allowance_claiming_div').show();
        $('#div_master_trainer').show();
    } else {
        $('#allowance_claiming_l').hide();
        $('#allowance_claiming_div').hide();
        $('#div_master_trainer').hide();
    }
}, 100);

// $( document ).ready(function() {
// var age =  $('#input-age').val();
// //alert(assistant_trainer);div_master_trainer
//     if(age < '25'){
//         $('#allowance_claiming_l').show();
//         $('#allowance_claiming_div').show();
//         $('#div_master_trainer').show();
//     } else {
//         $('#allowance_claiming_l').hide();
//         $('#allowance_claiming_div').hide();
//         $('#div_master_trainer').hide();
//     }
// });
// $( "#input-age" ).change(function(){
// var age =  $('#input-age').val()
//     if(age < '25'){
//         $('#allowance_claiming_l').show();
//         $('#allowance_claiming_div').show();
//         $('#div_master_trainer').show();
//     } else {
//         $('#allowance_claiming_l').hide();
//         $('#allowance_claiming_div').hide();
//         $('#div_master_trainer').hide();
//     }
// });



$('.date').datetimepicker({
    pickTime: false
});

$('.time').datetimepicker({
    pickDate: false
});

$('.datetime').datetimepicker({
    pickDate: true,
    pickTime: true
});

</script>
<script type="text/javascript">
    function total(){
        var out_station_wins =  parseInt($('#out_station_wins').val()) || 0;
        var station_wins =  parseInt($('#station_wins').val()) || 0;

        var total_wins =(out_station_wins + station_wins);

        $('#total_wins').val('');
        $('#total_wins').val(total_wins);

    }
</script>
<script language="javascript">
function changeIt(count){
    var i;

    $('.mydivs').remove();
    for(i=0;i<count;i++) {
        my_div.innerHTML = my_div.innerHTML +"<div class='mydivs'><label class='col-sm-2 control-label'>Horse"+i+":</label><div class='col-sm-2'><input class='form-control' type='text' name='out_station_name["+i+"][name]</div></div>'>";

    }
}
</script>

<script type="text/javascript">
$('#master_trainer').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/jockey/autocompletetrainer&token=<?php echo $token; ?>&trainer=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['vendor_name'],
            id: item['vendor_id'],
            value: item['vendor_name'],
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('#master_trainer').val(item['label']);
    $('#master_trainer').val(item['value']);
    $('#input-master_trainer_s_date').val('');
    $('#input-master_trainer_e_date').val('');
    $('.dropdown-menu').hide();
  }
});
</script>

<script type="text/javascript">
$( document ).ready(function() {
    var jockey_returned_by_type =  $('#input-jockey_returned_by').val();
    if(jockey_returned_by_type == 'Y'){
        $('.add-owner-div').show();
    } else {
        $('.add-owner-div').hide();
    }
});

$('#add-ownrs').click(function() {
    auto_id = $('.owners_cnt').val();
    id = parseInt(auto_id) + 1;
    html = '';
    html += '<div class="form-group" id="remove_div_'+id+'">';
        html += '<div class="col-sm-10">';
            html += '<input type="text" name=private_trainers_owner_name['+id+'][owner_name] class="form-control trainer_owner_name" id="trainer_owner_id_'+id+'" />';
            html += '<input type="hidden" name=private_trainers_owner_name['+id+'][owner_id] id="hidden_trainer_owner_id_'+id+'" />';
        html += '</div>';

        html += '<div class="col-sm-2">';
            html += '<a onclick="rmvOwnerName('+id+')" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
        html += '</div>';
    html += '</div>';
    

    $('.owner_data-div').append(html);
    $('.owners_cnt').val(id);
    $('#trainer_owner_id_'+id).focus();
});

$(document).on('keyup', '.trainer_owner_name', function(e) {
    $('.trainer_owner_name').autocomplete({
        'source': function(request, response) {
            $.ajax({
            url: 'index.php?route=catalog/jockey/autocompleteOwnername&token=<?php echo $token; ?>&owner_name=' +  encodeURIComponent(request),
            dataType: 'json',
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.owner_name,
                        value: item.owner_name,
                        ownercode: item.owner_id
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            idzz = $(this).attr('id');
            idss = idzz.split('_');
            $('#'+idzz).val(item['label']);
            $('#hidden_trainer_owner_id_'+idss[3]).val(item['ownercode']);
            return false;
        }
    });
});

function rmvOwnerName(id){
    $('#remove_div_'+id).remove();

}
</script>

<script type="text/javascript">
    $('#button-other_document_1').on('click', function() {
$('#form-other_document_1').remove();
$('body').prepend('<form enctype="multipart/form-data" id="form-other_document_1" style="display: none;"><input type="file" name="file" /></form>');
$('#form-other_document_1 input[name=\'file\']').trigger('click');
if (typeof timer != 'undefined') {
    clearInterval(timer);
}
timer = setInterval(function() {
  if ($('#form-other_document_1 input[name=\'file\']').val() != '') {
    clearInterval(timer); 
    image_name = 'file_number';  
    $.ajax({ 
    url: 'index.php?route=catalog/jockey/upload_profile&token=<?php echo $token; ?>'+'&image_name='+image_name,
    type: 'post',   
    dataType: 'json',
    data: new FormData($('#form-other_document_1')[0]),
    cache: false,
    contentType: false,
    processData: false,   
    beforeSend: function() {
      $('#button-upload').button('loading');
    },
    complete: function() {
      $('#button-upload').button('reset');
    },  
    success: function(json) {
      if (json['error']) {
      alert(json['error']);
      }
      if (json['success']) {
      alert(json['success']);
      console.log(json);
      $('input[name=\'file_number_upload\']').attr('value', json['filename']);
      $('input[name=\'uploaded_file_source\']').attr('value', json['link_href']);
      d = new Date();
      $('#blah').remove();
      var previewHtml = '<a target="_blank" class = "btn btn-primary" style="cursor: pointer;margin-left:5px;" id="uploaded_file_source" href="'+json['link_href']+'">View Document</a>';
      var image_data = '<img src="'+json['link_href']+'" height="60" id="blah" alt=""  />'
      $('#uploaded_file_source').remove();
       $('#profile_pic').append(image_data);
      }
    },      
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
    });
  }
  }, 500);
});

$(document).on('input', '#jockey_name', function(){
    var name = $( "#jockey_name" ).val();
    $( "#name1" ).val(name);
});


function readURL(input) {
    alert(input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(100)
                .height(80);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
<script>
// $( document ).ready(function() {
// var age =  $('#input-age').val();
// //alert(assistant_trainer);div_master_trainer
//     if(age < '25'){
//         $('#allowance_claiming_l').show();
//         $('#allowance_claiming_div').show();
//         $('#div_master_trainer').show();
//     } else {
//         $('#allowance_claiming_l').hide();
//         $('#allowance_claiming_div').hide();
//         $('#div_master_trainer').hide();
//     }
// });
// $( "#input-age" ).change(function(){
// var age =  $('#input-age').val()
//     if(age < '25'){
//         $('#allowance_claiming_l').show();
//         $('#allowance_claiming_div').show();
//         $('#div_master_trainer').show();
//     } else {
//         $('#allowance_claiming_l').hide();
//         $('#allowance_claiming_div').hide();
//         $('#div_master_trainer').hide();
//     }
// });


var timer = null;
$('#input-date_of_birth').keyup(function(){
    clearTimeout(timer); 
    $('#error_date_foal_date_post').hide();
    var date_foal_date_valid =  $('#input-date_of_birth').val();
    date_foal_date_valid1 = date_foal_date_valid.replace(/[^0-9-]+/i, '');
    $("#input-date_of_birth").val(date_foal_date_valid1);
    foaldate();
});

function foaldate() {
   var date_foal_date_valid =  $('#input-date_of_birth').val();
   var date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
    if (!(date_regex.test(date_foal_date_valid))) {
        $('#error_date_foal_date').show();
        return false;
    } else {
            $('#error_date_foal_date').hide();
            if(date_foal_date_valid != ''){

            var date_foal_date_for_age1 = date_foal_date_valid.split('-');
            var s_date  = date_foal_date_for_age1[0];
            var s_month = date_foal_date_for_age1[1];
            var s_year  = date_foal_date_for_age1[2];

            var start = s_year + "-" + s_month + "-" + s_date;

            var current_year = new Date().getFullYear()
            var age   = current_year - s_year ;
            $('#input-age').val(age);
            $('#input-age').attr('value', age);
        }
    }
}
$('.input-date_of_birth').datetimepicker().on('dp.change', function (e) {  
    $('#error_date_foal_date').css('display','none');
    $('#error_date_foal_date_post').css('display','none');

    var date_foal_date_for_age =  $('#input-date_of_birth').val();
    if(date_foal_date_for_age != ''){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var current_date = yyyy + "-" + mm + "-" + dd;

        var date_foal_date_for_age1 = date_foal_date_for_age.split('-');
        var s_date  = date_foal_date_for_age1[0];
        var s_month = date_foal_date_for_age1[1];
        var s_year  = date_foal_date_for_age1[2];

        var start = s_year + "-" + s_month + "-" + s_date;

        var starts = new Date(start);
        var end   = new Date(current_date);

        diff  = new Date(end - starts);
        days  = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
        $('#input-age').val(days);
        $('#input-age').attr('value', days);
    }

});
</script>
<?php echo $footer; ?>