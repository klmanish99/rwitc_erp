<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
                </div>
                <div class="panel-body">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-general">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="med_code">Medicine Code</label>
                                    <div class="col-sm-4">
                                        <input type="hidden" value="<?php echo $user_log_grp_id; ?>" name="user_log_grp_id">
                                        <input type="hidden" value="<?php echo $user_log_id; ?>" name="user_log_id">
                                        <input type="hidden"  name="alpha" id="alpha" value="<?php echo $alpha; ?>">
                                        <input type="hidden"  name="alpha_no" id="alpha_no" value="<?php echo $alpha_no; ?>">

                                        <input type="text" readonly name="med_code" value="<?php echo $med_code; ?>" placeholder="Medicine Code" id="med_code" class="form-control" data-index="1" />
                                        <?php if (isset($valierr_med_code)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_med_code; ?></span><?php } ?>
                                    </div>
                                    <label class="col-sm-2 control-label" for="med_name">Medicine Name</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="med_name" value="<?php echo $med_name; ?>" placeholder="Medicine Name" id="med_name" class="form-control" data-index="2" />
                                        <?php if (isset($valierr_med_name)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_med_name; ?></span><?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="select-med_type">Medicine Type</label>
                                    <div class="col-sm-4">
                                        <select name="med_type" id="select-med_type" class="form-control" data-index="3">
                                            <?php foreach ($medType as $key => $value) { ?>
                                            <?php if ($value == $med_type) { ?>
                                            <option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <?php if (isset($error_status)) { ?><span class="errors" style="color: #ff0000;"><?php echo $error_status; ?></span><?php } ?>
                                    </div>
                                    <label class="col-sm-2 control-label" for="unit_cost">Selling Price</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="unit_cost" value="<?php echo $unit_cost; ?>" placeholder="Selling Price" id="unit_cost" class="form-control" data-index="4"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="normal">Usage Quantiy</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="normal" value="<?php echo $normal; ?>" placeholder="Normal" id="normal" class="form-control" data-index="5"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="store_unit">Usage Quantiy Unit</label>
                                    <div class="col-sm-4">

                                        <select name="store_unit" id="quan" class="form-control">
                                            <option value="" >Please Select</option>
                                            <?php foreach ($quan_unit as $key => $value) { ?>
                                            <?php if ($value == $store_unit) { ?>
                                            <option value="<?php echo $value; ?>"  selected="selected" ><?php echo $value; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $value; ?>"><?php echo $value ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                            
                                        </select>

                                        <!-- <input type="text" name="store_unit" value="<?php echo $store_unit; ?>" placeholder="Store Unit" id="store_unit" class="form-control" data-index="6"/> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="reorder_qty">Reorder Quantity</label>
                                    <div class="col-sm-2">
                                        <input type="text" name="reorder_qty" value="<?php echo $reorder_qty; ?>" placeholder="Reorder Quantity" id="reorder_qty" class="form-control" data-index="7"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="reorder_lvl">Reorder Level</label>
                                    <div class="col-sm-2">
                                        <input type="text" name="reorder_lvl" value="<?php echo $reorder_lvl; ?>" placeholder="Reorder Level" id="reorder_lvl" class="form-control" data-index="8"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="reorder_lvl"><?php echo $entry_pur?></label>
                                    <div class="col-sm-2">
                                        <input type="text" name="pur_price" value="<?php echo $p_price; ?>" placeholder="<?php echo $entry_pur?>" id="reorder_lvl" class="form-control" data-index="8"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="pack"><?php echo $entry_pack;?></label>
                                    <div class="col-sm-2">
                                        <select name="pack" id="pkd" class="form-control">
                                            <option value="" >Please Select</option>
                                            <?php foreach ($pack_types as $key => $value) { ?>
                                            <?php if ( $value == $pack) { ?>
                                            <option value="<?php echo $value; ?>"  selected="selected" ><?php echo $value; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $value; ?>"><?php echo $value ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                            
                                        </select>
                                    </div>
                                    <label class="col-sm-2 control-label" for="volume"><?php echo $entry_volume;?></label>
                                    <div class="col-sm-2">
                                        <input type="text" name="volume" value="<?php echo $volume; ?>" placeholder="Volume" id="volume" class="form-control" data-index="8"/>
                                    </div>
                                    
                                    <label class="col-sm-2 control-label" for="unit"><?php echo $entry_unit;?></label>
                                    <div class="col-sm-2">
                                        
                                        <select name="unit" id="unit" class="form-control">
                                            <option value="" >Please Select</option>
                                            <?php foreach ($unit_list as $unit_id) { ?>
                                            <?php if ($unit_id == $unit) { ?>
                                            <option value="<?php echo $unit_id; ?>"  selected="selected" ><?php echo $unit_id; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $unit_id; ?>"><?php echo $unit_id ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="brand">Brand</label>
                                    <div class="col-sm-2">
                                        <input type="text" name="brand" value="<?php echo $brand; ?>" placeholder="Brand" id="brand" class="form-control" data-index="9"/>
                                    </div>

                                     <label class="col-sm-2 control-label" for="gst"><?php  echo $entry_gst ?></label>
                                    <div class="col-sm-2">
                                        <input type="text" name="gst" value="<?php echo $gst; ?>" placeholder="GST Rate" id="gst" class="form-control" data-index="9"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="select-isActive">Status</label>
                                    <div class="col-sm-2">
                                        <select name="isActive" id="select-isActive" class="form-control" tabindex="5">
                                            <?php foreach ($Active as $key => $value) { ?>
                                            <?php if ($value == $isActive) { ?>
                                            <option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <?php if ($med_type == 'I/A') { ?>
                                <div id="cost_div" class="form-group">
                                    <?php foreach ($doctors as $dkey => $doctor) { ?>
                                    <div  class="form-group">
                                        <label class="col-sm-2 control-label" for="cost_a1"><?php echo $doctor['doctor_name']; ?></label>
                                        <div class="col-sm-4">
                                            <input type="text" name="final[<?php echo $doctor['id'] ?>][cost]" value="<?php echo $doctor['cost']; ?>" placeholder="Cost A1" id="cost_a1" class="form-control" data-index="10"/>
                                            <input type="hidden" name="final[<?php echo $doctor['id'] ?>][doctor_id]" value="<?php echo $doctor['id']; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } else { ?>
                                <div style="display: none;" id="cost_div" class="form-group">
                                    <?php foreach ($doctors as $dkey => $doctor) { ?>
                                    <div  class="form-group">
                                        <label class="col-sm-2 control-label" for="cost_a1"><?php echo $doctor['doctor_name']; ?></label>
                                        <div class="col-sm-4">
                                            <input type="text" name="final[<?php echo $doctor['id'] ?>][cost]" value="<?php echo $doctor['cost']; ?>" placeholder="Cost A1" id="cost_a1" class="form-control" data-index="10"/>
                                            <input type="hidden" name="final[<?php echo $doctor['id'] ?>][doctor_id]" value="<?php echo $doctor['id']; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </script>
            <script type="text/javascript">
            $(document).ready(function(){
            $('[data-index= 2]').focus();
            });
            $('#form-category').on('keydown','input', function (event) {
            if (event.which == 13) {
            event.preventDefault();
            var $this = $(event.target);
            var index = parseFloat($this.attr('data-index'));
            $('[data-index="' + (index + 1).toString() + '"]').focus();
            }
            });
            $('#form-category').on('keydown','select', function (event) {
            if (event.which == 13) {
            event.preventDefault();
            var $this = $(event.target);
            var index = parseFloat($this.attr('data-index'));
            //console.log(index);
            $('[data-index="' + (index + 1).toString() + '"]').focus();
            }
            });
            $('#select-med_type').on('change', function () {
            var type = $('#select-med_type').val();
            if (type == 'I/A') {
            $('#cost_div').show();
            } else {
            $('#cost_div').hide();
            }
            });

            $(document).on("keyup","#med_name",function()  {
                item_name = $(this).val();
                if(item_name.length > 0 ){
                    $.ajax({
                        url: 'index.php?route=catalog/medicine/MedicineIdGenerate&token=<?php echo $token; ?>&filter_medicine_name=' + item_name[0], 
                        dataType: 'json',
                        success: function(json) {
                           // console.log(json); 
                            $('#med_code').val(json.med_code); 
                            $('#alpha').val(json.alpha); 
                            $('#alpha_no').val(json.alpha_no); 

                        } 
                    });
                } else {
                    if(item_name == ''){
                        $('#med_code').val('');
                    }
                }
            });
            
            </script>
            
            <script type="text/javascript"><!--
            $('#language a:first').tab('show');
        //--></script></div>
        <?php echo $footer; ?>