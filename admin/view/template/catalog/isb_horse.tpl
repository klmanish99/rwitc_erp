<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
	<!-- <div class="container-fluid">
	  <h1><?php echo $heading_title; ?></h1>
	  <ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	  </ul>
	</div> -->
  </div>
  <div class="container-fluid">
	<div class="panel panel-default">
	  <div class="panel-heading">
		<h3 class="panel-title" style=""><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
		&nbsp;&nbsp;<a href="<?php echo $back_horse; ?>"  class="btn btn-warning" style="background-color: #f33333;border-color:#f33333;position: absolute !important;bottom: 2px !important;right: 5px !important;width: 55px !important;  ">Back </a>
	  </div>
	  <div class="panel-body">
		<div class="well">
		  <div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<div class="col-sm-2">
						<input type="text" name="filterHorseName" id="filterHorseName" value="<?php echo $filter_hourse_name; ?>" placeholder="Foal Name" class="form-control">
						<input type="hidden" name="filtermare_name" id="filtermare_name" value="<?php echo $filtermare_name; ?>"  class="form-control">
					</div>
					<div class="col-sm-2">
						<input type="text" name="filtersire" id="filtersire" value="<?php echo $filtersire; ?>" placeholder="Sire Name" class="form-control">
					</div>
					<div class="col-sm-2">
						<input type="text" name="filterstudname" id="filterstudname" value="<?php echo $filterstudname; ?>" placeholder=" Stud Name" class="form-control">
					</div>
					<div class="col-sm-2">
					  <select name="filter_status" id="filter_status" class="form-control" data-toggle="tooltip" title="Status" >
						<option value="" selected="true"><?php echo 'All'?></option>
						<?php foreach ($status_horse as $skey => $svalue) { ?>
							<?php if($skey == $filter_status){ ?>
							  <option value="<?php echo $skey ?>" selected = "selected"><?php echo $svalue?></option>
							<?php } else {  ?>
								  <option value="<?php echo $skey ?>"><?php echo $svalue?></option>
							<?php } ?>
						<?php } ?>    
					</select>
					</div>
					<div class="col-sm-2">
					   <select name="year_of_foaling" id="year_of_foaling" class="form-control" data-toggle="tooltip" title="Year Of Foaling">
						<option value="" selected="true"><?php echo 'All'?></option>
						<?php foreach ($year_of_foalings as $ykey => $yvalue) { ?>
							<?php if($yvalue['YROFFLNG'] == $year_of_foaling){ ?>
							  <option value="<?php echo $yvalue['YROFFLNG'] ?>" selected = "selected"><?php echo $yvalue['YROFFLNG']?></option>
							<?php } else {  ?>
								  <option value="<?php echo $yvalue['YROFFLNG'] ?>"><?php echo $yvalue['YROFFLNG']?></option>
							<?php } ?>
						<?php } ?>    
					</select>
					</div>
					<a onclick="filter();"  id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
					 &nbsp;&nbsp;<a  href="<?php echo $import_horse; ?>"style="background-color: #1e91cf;border-color: #1978ab;" type="button" class="btn btn-danger">Import Horse</a>
				</div>
			</div>
		  </div>
		</div>
		<div class="table-responsive">
		  <table class="table table-bordered">
			<thead>
			  <tr>
				<td class="text-center"><?php echo 'Year Of Foaling'; ?></td>
			   <td class="text-center"><?php echo 'Foal Name'; ?></td>
				<td class="text-center"><?php echo 'Country'; ?></td>
				<td class="text-center"><?php echo 'Sex'; ?></td>
				 <td class="text-center"><?php echo 'Color'; ?></td>
				<td class="text-center"><?php echo 'Mare Name'; ?></td>
				<td class="text-center"><?php echo 'Sire Name'; ?></td>
				<td class="text-center"><?php echo 'Stud Name'; ?></td>
				<td class="text-center"><?php echo 'Foal Date'; ?></td>
				<td class="text-center"><?php echo 'Status'; ?></td>
				<td class="text-center"> <?php echo 'Action'; ?></td>
			  </tr>
			</thead>
			<tbody>
			  <?php if ($isb_horse_datas) { ?>
			  <?php foreach ($isb_horse_datas as $isb_horse => $isb_horse_data) { ?>

				<?php //$hourse_name = $isb_horse_data['MARENAT'] ? $isb_horse_data['MARENAME'].'/'.$isb_horse_data['YROFFLNG'].'['.$isb_horse_data['MARENAT'].']' : $isb_horse_data['MARENAME'].'/'.$isb_horse_data['YROFFLNG'];
					if($isb_horse_data['FOALNAME']){
						$hourse_name = $isb_horse_data['FOALNAME'];
					} else {
						if($isb_horse_data['MARENAT']){
							$hourse_name =  $isb_horse_data['MARENAME'].'['.$isb_horse_data['MARENAT'].']'.' '.'/'.$isb_horse_data['YROFFLNG'];
						} else {
							$hourse_name =  $isb_horse_data['MARENAME'].'/'.$isb_horse_data['YROFFLNG'];
						}
					}
				   // $Status = $isb_horse_data['status'] == 0 ? 'Created' : 'Not Created';
					$Status = ($isb_horse_data['status'] == 0) ? 'Created' : (($isb_horse_data['status'] == 1) ? 'Not Created' : 'Temp-Horse');
					$foal_date = ($isb_horse_data['FOALDATE'] != '' && $isb_horse_data['FOALDATE'] != '0000-00-00') ? date('d-m-Y', strtotime($isb_horse_data['FOALDATE'])) : '';

				?>
			  <tr id="tr_<?php echo $isb_horse; ?>">
				<td class="text-left" >
					<?php echo $isb_horse_data['YROFFLNG']; ?>
				</td>
				<td class="text-left" >
					<?php echo $hourse_name; ?>
				</td>
				<td class="text-left">
					<?php echo $isb_horse_data['MARENAT']; ?>
				</td>
				<td class="text-left">
					<?php echo $isb_horse_data['HORSESEX']; ?>
				</td>
				<td class="text-left">
					<?php echo $isb_horse_data['HORSECOLOR']; ?>
				</td>
				<td class="text-left">
					<?php echo $isb_horse_data['MARENAME']; ?>
				</td>
				 <td class="text-left">
					<?php echo $isb_horse_data['SIRENAME']; ?>
				</td>
				  <td class="text-left">
					<?php echo $isb_horse_data['STUDNAME']; ?>
					</td>
				 <td class="text-right">
					<?php echo  $foal_date; ?>
					</td>
				<td class="text-left">
					<?php echo $Status; ?>
				</td>
				<td> 
					<?php  if($isb_horse_data['status'] == 1) { ?>
					<a onclick='updateisb(<?php echo $isb_horse ?>);' class="btn btn-primary" id = "addition_isb_horse<?php echo  $isb_horse; ?>"><i class="fa fa-plus"></i></a>
					<a onclick="removehorseDetail(<?php echo $isb_horse_data['id'] ?>,<?php echo $isb_horse ?>)" style= "display: none;"  class="btn btn-warning" id = "remove_isb_horse<?php echo  $isb_horse; ?>" ><i class="fa fa-remove" ></i></a>
				<?php } else if($isb_horse_data['status'] == 3) { ?>
					 <a onclick="removehorseDetail(<?php echo $isb_horse_data['id'] ?>,<?php echo $isb_horse ?>)"   class="btn btn-warning" id = "remove_isb_horse<?php echo  $isb_horse; ?>" ><i class="fa fa-remove" ></i></a>
					 <a onclick='updateisb(<?php echo $isb_horse ?>);' class="btn btn-primary" style= "display: none;" id = "addition_isb_horse<?php echo  $isb_horse; ?>"><i class="fa fa-plus"></i></a>
				 <?php } else { ?>
					 <i class="fa fa-check" style="font-size: 21px;"></i>
				 <?php } ?>
				</td>
					<input type="hidden" id="hidden_isb_id_<?php echo  $isb_horse; ?>" value = "<?php echo $isb_horse_data['id']; ?>" >
					<input type="hidden" id="isb_micro_chipone_<?php echo  $isb_horse; ?>" value = "<?php echo $isb_horse_data['MICROCHIP1']; ?>" >
					<input type="hidden" id="isb_micro_chiptwo_<?php echo  $isb_horse; ?>" value = "<?php echo $isb_horse_data['MICROCHIP2']; ?>" >
				  
			  </tr>
			  <?php } ?>
			  <?php } else { ?>
			  <tr>
				<td class="text-center" colspan="11"><?php echo $text_no_results; ?></td>
			  </tr>
			  <?php } ?>
			</tbody>
		  </table>
		</div>
		<div class="row">
		  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
function updateisb(isb_horsezzz){
	var hidden_isb_id = $('#hidden_isb_id_'+isb_horsezzz+'').val();
	 $.ajax({
	   url:'index.php?route=catalog/isb_horse/insertISbhorse&token=<?php echo $token; ?>',
		method: "POST",
		dataType: 'json',
		 async: true,
		data: {
			isb_horse_id: hidden_isb_id,
		},
		success: function(json)
		{
		   if (json['success']== 1) {
			$('#remove_isb_horse'+isb_horsezzz+'').show();
			 $('#addition_isb_horse'+isb_horsezzz+'').hide();
		   } 
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert(errorThrown);
		}
	});
}


function removehorseDetail(isb_horse_id,id){
	if (confirm("Are you sure to Cancel.")) {
		$.ajax({
			url:'index.php?route=catalog/isb_horse/update_status&token=<?php echo $token; ?>'+'&hourse_id='+isb_horse_id,
			method: "POST",
			dataType: 'json',
			success: function(json)
			{
				 if (json['success'] == 1) {
					$('#addition_isb_horse'+id+'').show();
					$('#remove_isb_horse'+id+'').hide();
				} 
				
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error deleting data');
			}
		});
		 return false;
	}
}

	function filter() { 
		var filter_hourse_name = $('#filterHorseName').val();
		var filtermare_name = $('#filtermare_name').val();
		var filter_status = $('#filter_status').val();

		var filtersire = $('#filtersire').val();
		var filterstudname = $('#filterstudname').val();
		var year_of_foaling = $('#year_of_foaling').val();

		if(filter_hourse_name == ''){
		  var filtermare_name = '';
		}
		 url = 'index.php?route=catalog/isb_horse&token=<?php echo $token; ?>';
			if (filter_hourse_name) {
			  url += '&filter_hourse_name=' + encodeURIComponent(filter_hourse_name);
			}

			if (filtermare_name) {
			  url += '&filtermare_name=' + encodeURIComponent(filtermare_name);
			}

			if (filter_status) {
				url += '&filter_status=' + encodeURIComponent(filter_status);
			} 

			 if (filtersire) {
				url += '&filtersire=' + encodeURIComponent(filtersire);
			} 
			 if (filterstudname) {
				url += '&filterstudname=' + encodeURIComponent(filterstudname);
			} 

			 if (year_of_foaling) {
				url += '&year_of_foaling=' + encodeURIComponent(year_of_foaling);
			} 
			console.log(url);
			window.location.href = url; 
		
	  
	}

	$('#filterHorseName').autocomplete({
	delay: 500,
	source: function(request, response) {
		$('#filtermare_name').val('');
		if(request != ''){
			$.ajax({
				url: 'index.php?route=catalog/isb_horse/autocompleteHorse&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {   
					response($.map(json, function(item) {
						return {
							label: item.horse_name,
							value: item.MARENAME,
							horse_ids:item.horse_id,
						}
					}));
				}
			});
		}
	}, 
	select: function(event, ui) {
		console.log(event, ui);
		$('#filterHorseName').val(ui.item.label);
		$('#filtermare_name').val(ui.item.value);
		$('.dropdown-menu').hide();
		return false;
	},
	});
	$('#filtersire').autocomplete({
	delay: 500,
	source: function(request, response) {
		if(request != ''){
			$.ajax({
				url: 'index.php?route=catalog/isb_horse/autocompletesire&token=<?php echo $token; ?>&sire_name=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {   
					response($.map(json, function(item) {
						return {
							label: item.SIRENAME,
						}
					}));
				}
			});
		}
	}, 
	select: function(event, ui) {
		console.log(event, ui);
		$('#filtersire').val(ui.item.label);
		$('.dropdown-menu').hide();
		return false;
	},
	});
	 $('#filterstudname').autocomplete({
	delay: 500,
	source: function(request, response) {
		if(request != ''){
			$.ajax({
				url: 'index.php?route=catalog/isb_horse/autocompletestudname&token=<?php echo $token; ?>&stud_name=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {   
					response($.map(json, function(item) {
						return {
							label: item.STUDNAME,
						}
					}));
				}
			});
		}
	}, 
	select: function(event, ui) {
		console.log(event, ui);
		$('#filterstudname').val(ui.item.label);
		$('.dropdown-menu').hide();
		return false;
	},
	});
	$('#filterHorseName').keydown(function(e) {
	  if (e.keyCode == 13) {
		filter();
	  }
	});
	 $('#filtersire').keydown(function(e) {
	  if (e.keyCode == 13) {
		filter();
	  }
	});
	  $('#filterstudname').keydown(function(e) {
	  if (e.keyCode == 13) {
		filter();
	  }
	});
	   $('#year_of_foaling').keydown(function(e) {
	  if (e.keyCode == 13) {
		filter();
	  }
	});
	$('#status').keydown(function(e) {
	  if (e.keyCode == 13) {
		filter();
	  }
	});
</script>
<?php echo $footer; ?>