<div class="table-responsive">
<div style="float: right;padding-bottom: 10px;">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"  onclick="closeaddbuu1()"><i class="fa fa-plus-circle"></i></button>
    </div>
    <div class="col-sm-1">
        <input type="hidden" name="id_hidden_band" value="1" id="id_hidden_band" class="form-control" />
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Owner Authority To Trainer</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-owner_name"><b style="color: red">*</b><?php echo "Owner Name:"; ?></label>
                            <div class="col-sm-8">
                                <input type="text" name="check_name" value="<?php echo $owner_info['check_name'] ?>" placeholder="<?php echo "Owner Name"; ?>" id="input-owner_name" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b><?php echo "Start Date:"; ?></label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <input type="text" name="start_date" value="<?php echo $start_date ?>" placeholder="Start Date" data-date-format="YYYY-MM-DD" id="start_date_ban" class="form-control" />
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-horse"><?php echo "End Date:"; ?></label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <input type="text" name="end_date" value="<?php echo $end_date ?>" placeholder="End Date" data-date-format="YYYY-MM-DD" id="end_date_ban" class="form-control" />
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-stock-status">Monthly Maint Cost:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="monthly_maint_cost_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="monthly_maint_cost_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                            <label class="col-sm-3 control-label" for="input-stock-status">Commission To Tm & Jockey:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="Commission_to_tm_and_jockey_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="Commission_to_tm_and_jockey_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-stock-status">Syces: Diwali, Yearly Bonus:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="syces_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="syces_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                            <label class="col-sm-3 control-label" for="input-stock-status">Payment to other Clubs:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="Payment_other_clubs_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="Payment_other_clubs_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-stock-status">SWater Charges:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="water_charges_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="water_charges_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                            <label class="col-sm-3 control-label" for="input-stock-status">Stable Rent:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="stable_rent_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="stable_rent_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-stock-status">Extra Oats:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="extra_oats_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="extra_oats_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                            <label class="col-sm-3 control-label" for="input-stock-status">Private Doctors Vet Bills:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="private_doctors_bills_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="private_doctors_bills_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-stock-status">EIA Test Charges:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="eia_test_charges_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="eia_test_charges_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                            <label class="col-sm-3 control-label" for="input-stock-status">Extras:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="extras_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="extras_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-stock-status">Electricitty Charges:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="electricitty_charges_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="electricitty_charges_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                            <label class="col-sm-3 control-label" for="input-stock-status">Buy:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="buy_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="buy_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-stock-status">Sell:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="sell_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="sell_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                            <label class="col-sm-3 control-label" for="input-stock-status">Any Other Payment</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="any_other_payment_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="any_other_payment_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-stock-status">Appoint Sub_agent:</label>
                            <div class="col-sm-2 radio-inline">
                                <input type="radio" name="appoint_sub_agent_checked"  value="1"  /><?php echo "Yes"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="appoint_sub_agent_checked"  value="0"  /> <?php echo "No"; ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id = "owner_save_id" onclick="AuthorityFunction()"  data-dismiss="modal">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>