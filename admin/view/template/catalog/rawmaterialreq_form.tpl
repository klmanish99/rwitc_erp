<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button  onclick="submit_form()" type="button" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a onclick="prev_ind()" data-toggle="tooltip" title="<?php echo 'Previous Indent'; ?>" class="btn btn-primary">Previous Indent</a>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
				<?php if($user_group_id == 15 && $approval_status == 0){ ?>
					<a href="<?php echo $approval; ?>" data-toggle="tooltip" title="<?php echo 'Approve'; ?>" class="btn btn-primary"><i class="fa fa-check"></i></a>
					<a onclick="reason_approve()" data-toggle="tooltip" title="<?php echo 'Reject'; ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
				<?php } ?>
			</div>
			<div class="pull-right" >
				<textarea style="display: none; " rows="3" placeholder="Reason" class="col-sm-8" id="reason"></textarea>
				<a onclick="rejected()" style="display: none; margin-left: 10px;" data-toggle="tooltip" title="<?php echo 'Reject'; ?>" class="btn btn-danger col-sm-3" id="reject" >Reject</a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
					<div class="tab-content">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-order_no">Indent Code</label>
								<div class="col-sm-1">
									<input type="hidden" value="<?php echo $user_log_grp_id; ?>" name="user_log_grp_id">
									<input type="hidden" value="<?php echo $user_log_id; ?>" name="user_log_id">
									<input type="hidden" value="" id="input-req" class="form-control" />
									<input type="hidden" value="" id="input-datas" class="form-control" />
									<input type="hidden" name="order_id" value="<?php echo $order_id; ?>" id="input-order_id" class="form-control" />
									<input disabled="disabled" tabindex="" type="text" name="order_no" value="<?php echo $order_no; ?>" placeholder="Indent Code" id="input-order_no" class="form-control" />
									<input type="hidden" name="hidden_order_no" value="<?php echo $order_no; ?>" placeholder="Indent Code" id="input-order_no" class="form-control" />
								</div>
								<div class="col-sm-1">
								</div>
								<div class="col-sm-2">
								</div>
								<label class="col-sm-2 control-label" for="input-date"><?php echo ' Date'; ?></label>
					            <div class="col-sm-3">
					            	<input type="hidden" name="date" value="<?php echo $date; ?>" id="input-date" class="form-control"/>
					            	<input readonly="readonly" type="text" name="date" value="<?php echo $date; ?>" placeholder="<?php echo 'Date'; ?>" id="input-date" class="form-control date"/>
					        	</div>
							</div>
							<div class="form-group main_div">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<td style="width: 20%;" class="col-sm-1 text-left">Requested By</td>
											<td class="col-sm-1 text-left">Code</td>
											<td class="text-left">Medicine Name <a target="_blank" href="<?php echo $add_med; ?>" data-toggle="tooltip" title="<?php echo 'Add Medicine'; ?>" class="pull-right btn btn-primary">Add Medicine</a></td>
											<td class="text-left">Supplier Name</td>
											<td class="col-sm-1 text-left">Quantity</td>
										</tr>
									</thead>
									<tbody>
										<tr style="background: white;" >
											<td class="text-left">
												<input type="text" name="requested_by" value="" placeholder="<?php echo 'Requested By'; ?>" id="input-requested_by" class="form-control" />
												<span style="display: none;" id="valid_supplier" class="errors">Please Enter Valid Requested By!</span>
											</td>
											<td class="text-left">
												<input type="text" name="productnew_code" value="" placeholder="<?php echo 'Code'; ?>" id="input-productnew_code" class="form-control" />
											</td>
											<td class="text-left">
												<input type="text" name="productnew" value="" placeholder="<?php echo 'Medicine Name'; ?>" id="input-productnew" class="form-control" />
											</td>
											<td class="text-left" id="auto_supplier" ></td>
											<td class="text-left">
												<input type="number" name="quantity" value="" placeholder="<?php echo 'Quantity'; ?>" id="input-quantity" class="quantitys form-control" />
												<input type="hidden" name="unit" value="" id="input-unit" class="form-control" />
												<input type="hidden" name="packing" value="" id="input-packing" class="form-control" />
												<input type="hidden" name="auto_volume" value="" id="input-auto_volume" class="form-control" />
												<input type="hidden" name="auto_units" value="" id="input-auto_unit" class="form-control" />
												<input type="hidden" name="volume" value="" id="input-volume" class="form-control" />
												<input type="hidden" readonly="readonly" name="ordered_quantity" value="" id="input-ordered_quantity" class="" />
												<input type="hidden" readonly="readonly" name="ordered_quantitys" value="" id="input-ordered_quantitys" class="" />
												<input type="hidden" readonly="readonly" name="purchase_price" value="" id="input-purchase_price" class="" />
												<input type="hidden" readonly="readonly" name="med_id" value="" id="input-med_id" class="" />
												<input type="hidden" readonly="readonly" name="purchase_value" value="" id="input-purchase_value" class="" />
												<input type="hidden" readonly="readonly" name="gst_rate" value="" id="input-gst_rate" class="" />
												<input type="hidden" readonly="readonly" name="gst_value" value="<?php echo $gst_value; ?>" id="input-gst_value" class="" />
												<input type="hidden" readonly="readonly" name="total" value="0" id="input-total" class="" />
												<input type="hidden" name="totals" value="<?php echo $total; ?>" id="input-totals" class="form-control" />
												<input type="hidden" name="vals" value="<?php echo $purchase_value; ?>" id="input-vals" class="form-control" />
												<input type="hidden" name="supplier" value="" id="input-supplier" class="form-control" />
												<input type="hidden" name="gst_val" value="<?php echo $gst_value; ?>" id="input-gst_val" class="form-control" />
											</td>
										</tr>
									</tbody>
								</table>
								<br>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<td class="text-left">Available Qty</td>
											<td class="text-left">Volume</td>
											<td class="text-left">Packing Type</td>
											<td class="text-left">Converted Qty</td>
											<td class="text-left">Rate</td>
											<td class="text-left">Value</td>
											<td class="text-left">GST Rate</td>
											<td class="text-left">GST Value</td>
											<td class="text-left">Total</td>
										</tr>
										<tbody>
											<tr>
												<td class="text-left" id="current_qtys" ></td>
												<td class="text-left" id="auto_volume" ></td>
												<td class="text-left" id="auto_packing" ></td>
												<td class="text-left" id="auto_order" ></td>
												<td class="text-left" id="auto_purchase_price" ></td>
												<td class="text-left" id="auto_purchase_value" ></td>
												<td class="text-left" id="auto_gst_rate" ></td>
												<td class="text-left" id="auto_gst_value" ></td>
												<td class="text-left" id="auto_total" ></td>
											</tr>
										</tbody>
									</thead>
								</table>
							</div>
							<div class="form-group main_div">
								<table id="tblPartners" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<td class="text-center">Requested By</td>
											<td class="text-center">Code</td>
											<td class="text-center">Medicine Name</td>
											<td class="text-center">Supplier Name</td>
											<td class="text-center">Available Qty</td>
											<td class="text-center">Volume</td>
											<td class="text-center">Packing Type</td>
											<td class="text-center">Quantity</td>
											<td class="text-center">Converted Qty</td>
											<td class="text-center">Rate Rs</td>
											<td class="text-center">Value Rs</td>
											<td class="text-center">GST Rate</td>
											<td class="text-center">GST Value Rs</td>
											<td class="text-center">Total Rs</td>
											<td class="text-center">Action</td>
										</tr>
									</thead>
									<tbody>
										<?php $extra_field = 1; ?>
										<?php $tab_index_1 = 7; ?>
										<?php if($productraw_datas){ ?>
											<?php foreach($productraw_datas as $ckey => $cvalue){ ?>
											<tr id="productraw_row<?php echo $extra_field; ?>">
												<td class="text-left">
													<?php echo $cvalue['request']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][request]" value="<?php echo $cvalue['request']; ?>" id="" class="form-control" />
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][med_id]" value="<?php echo $cvalue['med_id']; ?>" id="" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['product_id']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][productraw_code]" value="<?php echo $cvalue['product_id']; ?>" id="" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['productraw_name']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][productraw_name]" value="<?php echo $cvalue['productraw_name']; ?>" id="" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['supplier_name']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][supplier_name]" value="<?php echo $cvalue['supplier_name']; ?>" id="" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['current_qty']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][current_qty]" value="<?php echo $cvalue['current_qty']; ?>" id="" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['volume']; ?> <?php echo $cvalue['unit']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][volume]" value="<?php echo $cvalue['volume']; ?>" id="" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['packing']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][packing]" value="<?php echo $cvalue['packing']; ?>" id="" class="form-control" />
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][unit]" value="<?php echo $cvalue['unit']; ?>" id="" class="form-control" />
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][auto_unit]" value="<?php echo $cvalue['auto_unit']; ?>" id="" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['quantity']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][quantity]" value="<?php echo $cvalue['quantity']; ?>" id="" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['ordered_qty']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][ordered_qty]" value="<?php echo $cvalue['ordered_qty']; ?>" id="" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['rate']; ?> / <?php echo $cvalue['packing']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][rate]" value="<?php echo $cvalue['rate']; ?>" id="" class="form-control" />
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][purchase_price]" value="<?php echo $cvalue['rate']; ?>" id="" class="form-control" />
												</td>
												<td class="text-right">
													<?php echo $cvalue['purchase_value']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][purchase_value]" value="<?php echo $cvalue['purchase_value']; ?>" id="" class="form-control" />
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][purchase_values]" value="<?php echo $cvalue['purchase_value']; ?>" id="input-purchase_values<?php echo $extra_field; ?>" class="form-control" />
												</td>
												<td class="text-left">
													<?php echo $cvalue['gst_rate']; ?> %
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][gst_rate]" value="<?php echo $cvalue['gst_rate']; ?>" id="" class="form-control" />
												</td>
												<td class="text-right">
													<?php echo $cvalue['gst_value']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][gst_value]" value="<?php echo $cvalue['gst_value']; ?>" id="" class="form-control" />
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][gst_values]" value="<?php echo $cvalue['gst_value']; ?>" id="input-gst_values<?php echo $extra_field; ?>" class="form-control" />
												</td>
												<td class="text-right">
													<?php echo $cvalue['total']; ?>
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][total]" value="<?php echo $cvalue['total']; ?>" id="" class="form-control" />
													<input type="hidden" name="productraw_datas[<?php echo $extra_field ?>][totals]" value="<?php echo $cvalue['total']; ?>" id="input-totals<?php echo $extra_field; ?>" class="form-control" />
												</td>
												<?php $tab_index_1 ++; ?>
												<td class="text-left">
													<button onclick="remove_folder('<?php echo $extra_field; ?>')" class="btn btn-danger" id="remove<?php echo $extra_field; ?>" ><i class="fa fa-minus-circle"></i></button>
												</td>
											</tr>
											<?php $extra_field ++; ?>
											<?php } ?>
										<?php } ?>
										<input type="hidden" id="extra_field" name="extra_field" value="<?php echo $extra_field; ?>" />
										<input type="hidden" id="tab_index_1" name="tab_index_1" value="<?php echo $tab_index_1; ?>" />
									</tbody>
									<thead>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td><input readonly="readonly" type="hidden" name="final_val" value="<?php echo $gst_value; ?>" id="input-final_gst_val"/></td>
											<td><input readonly="readonly" type="hidden" name="final_val" value="<?php echo $purchase_value; ?>" id="input-final_val"/></td>
											<td></td>
											<td>Total</td>
											<td style="text-align: right;" id="auto_final_val"></td>
											<td><input readonly="readonly" type="hidden" name="total" value="<?php echo $total; ?>" id="input-final_total"/></td>
											<td style="text-align: right;" id="auto_final_gst"></td>
											<td style="text-align: right;" id="auto_final_tot"></td>
											<td></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-narration"><?php echo 'Narration'; ?></label>
					            <div class="col-sm-4">
					            	<textarea rows="5" cols="50" name="narration" id="input-narration" class="form-control"><?php echo $narration; ?></textarea>
					            </div>
							</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function reason_approve() {
		$('#reason').show();
		$('#reject').show();
	}

	function rejected() {
		var reason = $('#reason').val();
		var order_id = $('#order_id').val();
		url = 'index.php?route=catalog/rawmaterialreq/indent_dis_approval&token=<?php echo $token; ?>';
		if (reason != '') {
			url += '&reason=' + encodeURIComponent(reason);
			url += '&order_id=' + encodeURIComponent(order_id);
		} else {
			return false;
		}
		window.location.href = url;
	}
</script>

<script type="text/javascript">
// $('.date').datetimepicker({
//   pickTime: false,
//   format: 'DD-MM-YYYY',
// });

$(document).on('keypress','input,select,textarea', function (e) {
	if (e.which == 13) {
		e.preventDefault();
		var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
		if (!$next.length) {
			$next = $('[tabIndex=1]');
		}
		$next.focus();
	}
});

var extra_field = $('#extra_field').val();
var tab_index_1 = $('#tab_index_1').val();
function add_productraw(item){
	$('#input-datas').val(1);
	
	html = '';
  html += '<tr id="productraw_row' + extra_field + '">';

  	html += '<td id="input-request_label'+extra_field+'" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][request]" value="" id="input-requests'+extra_field+'" class="form-control" />';
	html += '</td>';

  	html += '<td id="input-code_label'+extra_field+'" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][productraw_code]" value="" id="input-productraw_codes'+extra_field+'" class="form-control" />';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][med_id]" value="" id="input-med_ids'+extra_field+'" class="form-control" />';
	html += '</td>';

	html += '<td id="input-name_label'+extra_field+'" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][productraw_name]" value="" id="input-productraw_names'+extra_field+'" class="form-control" />';
	html += '</td>';

	html += '<td id="input-supplier_label'+extra_field+'" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][supplier_name]" value="" id="input-supplier_names'+extra_field+'" class="form-control" />';
	html += '</td>';
	
	tab_index_1 ++;

	html += '<td id="input-current_qty_label'+extra_field+'"  style="background: white;" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][current_qty]" value="" id="input-current_qtys_'+extra_field+'" class="form-control" />';
	html += '</td>';

	html += '<td id="input-volume_label'+extra_field+'" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][volume]" value="" id="input-volumes'+extra_field+'" class="form-control" />';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][auto_unit]" value="" id="input-auto_units'+extra_field+'" class="form-control" />';
	html += '</td>';

	html += '<td id="input-packing_label'+extra_field+'" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][packing]" value="" id="input-packings'+extra_field+'" class="form-control" />';
	html += '</td>';

	html += '<td id="input-qty_label'+extra_field+'" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][quantity]" value="" id="input-quantitys_'+extra_field+'" class="form-control quantity_class" />';
	html += '</td>';
	
	html += '<td id="input-ordered_qty_label'+extra_field+'" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][ordered_qty]" value="" id="input-ordered_qtys_'+extra_field+'" class="form-control" />';
	html += '</td>';
	
	html += '<td id="input-purchase_price_label'+extra_field+'" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][purchase_price]" value="" id="input-purchase_prices_'+extra_field+'" class="form-control" />';
	html += '</td>';
	
	html += '<td id="input-purchase_value_label'+extra_field+'" class="text-right">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][purchase_value]" value="" id="input-purchase_values'+extra_field+'" class="form-control" />';
	html += '</td>';
	
	html += '<td id="input-gst_rate_label'+extra_field+'" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][gst_rate]" value="" id="input-gst_rates_'+extra_field+'" class="form-control" />';
	html += '</td>';
	
	html += '<td id="input-gst_value_label'+extra_field+'" class="text-right">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][gst_value]" value="" id="input-gst_values'+extra_field+'" class="form-control" />';
	html += '</td>';
	
	html += '<td id="input-total_label'+extra_field+'" class="text-right">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][total]" value="" id="input-totals'+extra_field+'" class="form-control" />';
	html += '</td>';

	html += '<td style="display: none;" class="text-left">';
		html += '<input type="hidden" name="productraw_datas['+extra_field+'][unit]" value="" id="input-units_'+extra_field+'" class="form-control" />';
	html += '</td>';
	
	html += '<td class="text-left"><button onclick="remove_folder('+extra_field+')" class="btn btn-danger" id="remove'+extra_field+'" ><i class="fa fa-minus-circle"></i></button></td>';
  html += '</tr>';
  $('#tblPartners tbody').append(html);
  	var qty = $('#input-quantity').val();
	var med_id = $("#input-med_id").val();
	var code = $("#input-productnew_code").val();
	var req = $("#input-requested_by").val();
	var name = $("#input-productnew").val();
	var supplier = $("#input-supplier").val();
	var unit = $("#input-unit").val();
	var packing = $("#input-packing").val();
	var volume = $("#input-volume").val();
	var auto_unit = $("#input-auto_unit").val();
	var ordered_qty = $("#input-ordered_quantity").val();
	var ordered_qtys = $("#input-ordered_quantitys").val();
	var gst_rate = $("#input-gst_rate").val();
	var gst_value = $("#input-gst_value").val();
	var total = $("#input-total").val();
	var purchase_price = $("#input-purchase_price").val();
	var purchase_value = $("#input-purchase_value").val();
	var current_qtys = $('#current_qtys').html() || '';
	//alert(current_qtys);
	//alert(code);
	//alert(name);
	$('#input-quantity_'+extra_field+'').val(qty);
	$('#input-quantitys_'+extra_field+'').val(qty);
	$('#input-med_ids'+extra_field+'').val(med_id);
	$('#input-productraw_code'+extra_field+'').val(code);
	$('#input-productraw_codes'+extra_field+'').val(code);
	$('#input-productraw_name'+extra_field+'').val(name);
	$('#input-productraw_names'+extra_field+'').val(name);
	$('#input-supplier_names'+extra_field+'').val(supplier);
	$('#input-request'+extra_field+'').val(req);
	$('#input-requests'+extra_field+'').val(req);
	$('#input-unit_'+extra_field+'').val(unit);
	$('#input-units_'+extra_field+'').val(unit);
	$('#input-packing'+extra_field+'').val(packing);
	$('#input-packings'+extra_field+'').val(packing);
	$('#input-auto_units'+extra_field+'').val(auto_unit);
	$('#input-ordered_qtys_'+extra_field+'').val(ordered_qty);
	$('#input-gst_rates_'+extra_field+'').val(gst_rate);
	$('#input-gst_values_'+extra_field+'').val(gst_value);
	$('#input-purchase_prices_'+extra_field+'').val(purchase_price);
	$('#input-purchase_values'+extra_field+'').val(purchase_value);
	$('#input-gst_values'+extra_field+'').val(gst_value);
	$('#input-totals'+extra_field+'').val(total);

	current_qtys_lbl ='';
	current_qtys_lbl +='<label style="font-weight: 400;" class="text-left">'+current_qtys+'</label>';
	$('#input-current_qty_label'+extra_field+'').append(current_qtys_lbl);
	$('#input-current_qtys_'+extra_field+'').val(current_qtys);
	request_label ='';
	request_label +='<label style="font-weight: 400;" class="text-left">'+req+'</label>';
	$('#input-request_label'+extra_field+'').append(request_label);
	code_label ='';
	code_label +='<label style="font-weight: 400;" class="text-left">'+code+'</label>';
	$('#input-code_label'+extra_field+'').append(code_label);
	name_label ='';
	name_label +='<label style="font-weight: 400;" class="text-left">'+name+'</label>';
	$('#input-name_label'+extra_field+'').append(name_label);
	qty_label ='';
	qty_label +='<label style="font-weight: 400;" class="text-left">'+qty+'</label>';
	$('#input-qty_label'+extra_field+'').append(qty_label);
	packing_label ='';
	packing_label +='<label style="font-weight: 400;" class="text-left">'+packing+'</label>';
	$('#input-packing_label'+extra_field+'').append(packing_label);
	volume_label ='';
	volume_label +='<label style="font-weight: 400;" class="text-left">'+volume+' '+unit+'</label>';
	$('#input-volume_label'+extra_field+'').append(volume_label);
	ordered_qty_label ='';
	ordered_qty_label +='<label style="font-weight: 400;" class="text-left">'+ordered_qtys+'</label>';
	$('#input-ordered_qty_label'+extra_field+'').append(ordered_qty_label);
	gst_rate_label ='';
	gst_rate_label +='<label style="font-weight: 400;" class="text-left">'+gst_rate+'%'+'</label>';
	$('#input-gst_rate_label'+extra_field+'').append(gst_rate_label);
	gst_value_label ='';
	gst_value_label +='<label style="font-weight: 400;" class="text-left">'+gst_value+'</label>';
	$('#input-gst_value_label'+extra_field+'').append(gst_value_label);
	purchase_price_label ='';
	purchase_price_label +='<label style="font-weight: 400;" class="text-left">'+purchase_price+' / '+packing+'</label>';
	$('#input-purchase_price_label'+extra_field+'').append(purchase_price_label);
	purchase_value_label ='';
	purchase_value_label +='<label style="font-weight: 400;" class="text-left">'+purchase_value+'</label>';
	$('#input-purchase_value_label'+extra_field+'').append(purchase_value_label);
	total_label ='';
	total_label +='<label style="font-weight: 400;" class="text-left">'+total+'</label>';
	$('#input-total_label'+extra_field+'').append(total_label);
	supplier_label ='';
	supplier_label +='<label style="font-weight: 400;" class="text-left">'+supplier+'</label>';
	$('#input-supplier_label'+extra_field+'').append(supplier_label);

	$('#input-volume'+extra_field+'').val(volume);
	$('#input-volumes'+extra_field+'').val(volume);
	$('#input-auto_units'+extra_field+'').val(auto_unit);
	$('#input-requested_by').val('');
	$('#input-productnew_code').val('');
	$('#input-productnew').val('');
	$('#input-supplier').val('');
	$('#input-quantity').val('');
	$('#input-unit').val('');
	$('#input-packing').val('');
	$('#input-volume').val('');
	$('#input-ordered_quantity').val('');
	$('#input-ordered_quantitys').val('');
	$('#input-gst_rate').val('');
	$('#input-gst_value').val('');
	$('#input-purchase_price').val('');
	$('#input-purchase_value').val('');
	$('#auto_packing').html('');
	$('#auto_volume').html('');
	$('#current_qtys').html('');
	$('#auto_order').html('');
	$('#auto_supplier').html('');
	$('#auto_purchase_price').html('');
	$('#auto_purchase_value').html('');
	$('#auto_gst_rate').html('');
	$('#auto_gst_value').html('');
	$('#auto_total').html('');
	//$('#input-productnew').focus();
  extra_field++;
}

function remove_folder(extra_field){
	var final_val = $('#input-final_total').val();
	var purchase_price = $('#input-totals'+extra_field+'').val();
	var final_totals = parseFloat(final_val - purchase_price).toFixed(2);
	$('#input-final_total').val(final_totals);
	$('#input-totals').val(final_totals);

	var final_val2 = $('#input-final_val').val();
	var purchase_val2 = $('#input-purchase_values'+extra_field+'').val();
	var final_vals2 = parseFloat(final_val2 - purchase_val2).toFixed(2);
	$('#input-final_val').val(final_vals2);
	$('#input-vals').val(final_vals2);

	var final_val3 = $('#input-final_gst_val').val();
	var purchase_val = $('#input-gst_values'+extra_field+'').val();
	var final_vals = parseFloat(final_val3 - purchase_val).toFixed(2);
	$('#input-final_gst_val').val(final_vals);
	$('#input-gst_val').val(final_vals);

	$('#productraw_row'+extra_field).remove();
}

document.getElementById("input-productnew_code").onkeydown = function(event){
	//alert(qty);
	//return false;
    if (event.keyCode == 13 || event.which == 13){
    	var code = $('#input-productnew_code').val();
    	//alert(code);
    	if (code != '') {
	    	var med_code = $('#input-productnew_code').val();
	        $.ajax({
				url: 'index.php?route=catalog/rawmaterialreq/autocomplete_raw_code&token=<?php echo $token; ?>'+'&med_code='+med_code,
				type: 'post', 
				dataType: 'json',
				success: function(json) {
					//console.log('inn');
					console.log(json.length);
					if (json.length != 0) {
						var code = $('#input-productnew_code').val();
						if (code != '0') {
							if(json.med_id != ''){
								$.ajax({
							        url: 'index.php?route=catalog/rawmaterialreq/CheckIndentProduct&token=<?php echo $token; ?>&medicine_id=' +  encodeURIComponent(json.med_id),
							        dataType: 'json',
							        cache: false,
							        success: function(checkindent_array) {
							            if(checkindent_array.already_indent_product == 1){
							            	alert('Indent For This Medicine Already Creted By '+checkindent_array.user_name);
							       		}
							        }
							    }); 
							}
							$('#input-productnew').val(json.med_name);
							$('#input-quantity').val(json.quantity);
							$('#input-unit').val(json.unit);
							$('#input-packing').val(json.packing);
							$('#input-volume').val(json.volume);
							$('#input-auto_volume').val(json.volume);
							$('#input-gst_rate').val(json.gst_rate);
							$('#input-gst_rates').val(json.gst_rate+'%');
							$('#input-purchase_price').val(json.purchase_price);
							$('#input-med_id').val(json.med_id);
							$('#input-supplier').val(json.supplier);
							$('#auto_volume').html('');
							$('#auto_packing').html('');
							$('#auto_purchase_price').html('');
							auto_packing ='';
							auto_packing +='<label style="font-weight: 400;" class="text-left">'+json.packing+'</label>';
							$('#auto_packing').append(auto_packing);
							if(json.med_id != ''){
								$('#current_qtys').html('');
								$.ajax({
							        url: 'index.php?route=catalog/rawmaterialreq/CurrentDayQty&token=<?php echo $token; ?>&medicine_id=' +  encodeURIComponent(json.med_id)+'&medicine_code='+encodeURIComponent(json.med_code),
							        dataType: 'json',
							        cache: false,
							        success: function(json) {
							            $('#current_qtys').html(json.treatment_entry_qty);
							        }
							    }); 
							}
							auto_volume ='';
							auto_volume +='<label style="font-weight: 400;" class="text-left">'+json.volume+' '+json.unit+'</label>';
							$('#auto_volume').append(auto_volume);
							auto_purchase_price ='';
							auto_purchase_price +='<label style="font-weight: 400;" class="text-left">'+'Rs '+json.purchase_price+' / '+json.packing+'</label>';
							$('#auto_purchase_price').append(auto_purchase_price);
							auto_supplier ='';
							auto_supplier +='<label style="font-weight: 400;" class="text-left">'+json.supplier+'</label>';
							$('#auto_supplier').append(auto_supplier);
							$("#input-quantity").keyup(function()  {
								var unit =  $('#input-unit').val() ;
							  	var qty = $('#input-quantity').val() ;
							  	var purchase_price = $('#input-purchase_price').val();
							  	var packing_type = $('#input-packing').val();
								if ((unit == 'litre') || (unit == 'gm')) {
									final_qty = (qty.replace(/[^0-9]+/i, '')) * 1000;
								} else {
									final_qty = qty.replace(/[^0-9]+/i, '');
								}
							  	var volume = $('#input-auto_volume').val();
							  	//alert(final_qty);
							  	//alert(volume);
							  	var order_qty = final_qty * volume;
							  	var value = qty * purchase_price;
							  	var units = $('#input-unit').val();
							  	if ((units == 'litre') || (units == 'ml')) {
							  		final_unit = 'ML';
							  	} else if ((units == 'gm') || (units == 'Kg')) {
							  		final_unit = 'KG';
							  	} else {
							  		final_unit = '';
							  	}

							  	$('#input-ordered_quantity').val('');
								$('#input-auto_unit').val(final_unit);
							  	$('#input-ordered_quantity').val('');
								$('#input-ordered_quantity').val(order_qty);
								$('#input-ordered_quantitys').val('');
								$('#input-ordered_quantitys').val(order_qty+' '+final_unit);
								$('#input-purchase_value').val('');
								$('#input-purchase_value').val(value);
								$('#input-purchase_values').val('');
								$('#input-purchase_values').val(value+' Rs / '+packing_type);
								var purchase_value = $('#input-purchase_value').val();
							  	var gst = $('#input-gst_rate').val();
							  	var gst_rate = (purchase_value * gst)/100;
								$('#input-gst_value').val('');
								$('#input-gst_value').val(gst_rate);
								var total = value + gst_rate;
								$('#input-total').val(total);

								$('#auto_order').html('');
								auto_order ='';
								auto_order +='<label style="font-weight: 400;" class="text-left">'+order_qty+' '+final_unit+'</label>';
								$('#auto_order').append(auto_order);

								$('#auto_purchase_value').html('');
								auto_purchase_value ='';
								auto_purchase_value +='<label style="font-weight: 400;" class="text-left">'+'Rs '+value+'</label>';
								$('#auto_purchase_value').append(auto_purchase_value);

								$('#auto_gst_rate').html('');
								auto_gst_rate ='';
								auto_gst_rate +='<label style="font-weight: 400;" class="text-left">'+gst+'%'+'</label>';
								$('#auto_gst_rate').append(auto_gst_rate);

								$('#auto_gst_value').html('');
								auto_gst_value ='';
								auto_gst_value +='<label style="font-weight: 400;" class="text-left">'+'Rs '+gst_rate+'</label>';
								$('#auto_gst_value').append(auto_gst_value);

								$('#auto_total').html('');
								auto_total ='';
								auto_total +='<label style="font-weight: 400;" class="text-left">'+'Rs '+total+'</label>';
								$('#auto_total').append(auto_total);
							});

							$('#input-quantity').focus();
							$('.dropdown-menu').hide();
							document.getElementById("input-quantity").onkeypress = function(event){
							    if (event.keyCode == 13 || event.which == 13){
							    	var qty = $('#input-quantity').val();
							    	if (qty != '') {
										if (qty != 0) {
											var req_by = $('#input-requested_by').val();
											$.ajax({
										        url: 'index.php?route=catalog/rawmaterialreq/valid_supplier&token=<?php echo $token; ?>'+'&requested_by='+req_by,
										        dataType: 'json',
										        success: function(json) {
										        	var valid = json.valid;
									            	if (valid == 0) {
									            		alert("Please Enter Valid Requested By!");
									            	} else {
														var final_total = $('#input-totals').val();
														var tots = $('#input-total').val();
														var final_totals = parseFloat(final_total) + parseFloat(tots);
														var final_totalss = final_totals.toFixed(2);
														$('#input-totals').val(final_totalss);
														$('#input-final_total').val(final_totalss);

														var final_val = $('#input-vals').val();
														var vals = $('#input-purchase_value').val();
														var final_vals = parseFloat(final_val) + parseFloat(vals);
														var final_valss = final_vals.toFixed(2);
														$('#input-vals').val(final_valss);
														$('#input-final_val').val(final_valss);

														var final_gst_val = $('#input-gst_val').val();
														var gst_val = $('#input-gst_value').val();
														var final_gst_vals = parseFloat(final_gst_val) + parseFloat(gst_val);
														var final_gst_valss = final_gst_vals.toFixed(2);
														$('#input-gst_val').val(final_gst_valss);
														$('#input-final_gst_val').val(final_gst_valss);

											    		$('#input-requested_by').focus();
											        	add_productraw()
											        }
										        }
									        });
								        } else {
								        	alert('Please Enter Quantity!');
											return false;
								        }
							    	} else {
										alert('Please Enter Quantity!');
										return false;
									}
							    }
							};
						} else {
							alert('This Medicine Is Not Available!');
							return false;
						}
					} else {
						alert('This Medicine Is Not Available!');
						return false;
					}
				}
			});
    	} else {
    		$('#input-productnew').focus();
    	}
    }
};

$(document).on('keydown', '.form-control', '.custom-control-input', function(e) {
    var name = $(this).attr('name'); 
    var class_name = $(this).attr('class'); 
    var id = $(this).attr('id');
    console.log(name);
    console.log(class_name);
    console.log(id);
    var value = $(this).val();
    if(e.which == 13){
        if(id == 'input-date'){
            $('#input-narration').focus();
        }
        if(id == 'input-narration'){
            $('#input-requested_by').focus();
        }
        // if(id == 'input-requested_by'){
        //     $('#input-productnew_code').focus();
        // }
    }
});


$(document).on('focusout','.quantity_class', function (e) {

  idss = $(this).attr('id');
  s_id = idss.split('_');
  id = s_id[1];
  price = $('#input-price'+id).val();
  quantity = $(this).val();
  total = price * quantity;
  total = total.toFixed(2);
  $('#total'+id).text(total);
  $('#input-total'+id).val(total);

  grand_total = 0;
  $('.quantity_class').each(function( index ) {
	idss1 = $(this).attr('id');
	s_id1 = idss1.split('_');
	id1 = s_id1[1];
	price1 = $('#input-price'+id1).val();
	quantity1 = $('#input-quantity_'+id1).val();
	total1 = price1 * quantity1;
	total1 = total1.toFixed(2);
	grand_total = parseFloat(grand_total) + parseFloat(total1);
  });
  grand_total = grand_total.toFixed(2);
  $('#costprice').text(grand_total);
  $('#input-cost-price').val(grand_total);
});
</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
	
$('#input-productnew').autocomplete({
  	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/rawmaterialreq/autocomplete_raw&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['med_name'],
						id: item['id'],
						med_id: item['med_id'],
						med_code: item['med_code'],
						supplier: item['supplier'],
						value: item['med_name'],
					    quantity: item['quantity'],
					    unit: item['unit'],
					    packing: item['packing'],
					    volume: item['volume'],
					    gst_rate: item['gst_rate'],
					    purchase_price: item['purchase_price'],
					}
				}));
			}
		});
  	},
  	'select': function(item, ui) {
  		if(ui.item.med_id != ''){
			$.ajax({
		        url: 'index.php?route=catalog/rawmaterialreq/CheckIndentProduct&token=<?php echo $token; ?>&medicine_id=' +  encodeURIComponent(ui.item.med_id),
		        dataType: 'json',
		        cache: false,
		        success: function(checkindent_array) {
		            if(checkindent_array.already_indent_product == 1){
		            	alert('Indent For This Medicine Already Creted By '+checkindent_array.user_name);
		       		}
		        }
		    }); 
		}

		$('#input-med_id').val(ui.item.med_id);
		$('#input-productnew_code').val(ui.item.med_code);
		$('#input-supplier').val(ui.item.supplier);
		$('#input-productnew').val(ui.item.value);
		$('#input-quantity').val(ui.item.quantity);
		$('#input-unit').val(ui.item.unit);
		$('#input-packing').val(ui.item.packing);
		$('#input-volume').val(ui.item.volume);
		$('#input-auto_volume').val(ui.item.volume);
		$('#input-gst_rate').val(ui.item.gst_rate);
		$('#input-gst_rates').val(ui.item.gst_rate+'%');
		$('#input-purchase_price').val(ui.item.purchase_price);
		if(ui.item.med_code != ''){
			$('#current_qtys').html('');
			$.ajax({
		        url: 'index.php?route=catalog/rawmaterialreq/CurrentDayQty&token=<?php echo $token; ?>&medicine_id=' +  encodeURIComponent(ui.item.med_id)+'&medicine_code='+encodeURIComponent(ui.item.med_code),
		        dataType: 'json',
		        cache: false,
		        success: function(json) {
		            $('#current_qtys').html(json.treatment_entry_qty);
		        }
		    }); 
		}
		$('#auto_volume').html('');
		$('#auto_packing').html('');
		$('#auto_purchase_price').html('');
		auto_packing ='';
		auto_packing +='<label style="font-weight: 400;" class="text-left">'+ui.item.packing+'</label>';
		$('#auto_packing').append(auto_packing);
		auto_volume ='';
		auto_volume +='<label style="font-weight: 400;" class="text-left">'+ui.item.volume+' '+ui.item.unit+'</label>';
		$('#auto_volume').append(auto_volume);
		auto_purchase_price ='';
		auto_purchase_price +='<label style="font-weight: 400;" class="text-left">'+'Rs '+ui.item.purchase_price+' / '+ui.item.packing+'</label>';
		$('#auto_purchase_price').append(auto_purchase_price);
		auto_supplier ='';
		auto_supplier +='<label style="font-weight: 400;" class="text-left">'+ui.item.supplier+'</label>';
		$('#auto_supplier').append(auto_supplier);
		$("#input-quantity").keyup(function()  {
			var unit =  $('#input-unit').val() ;
		  	var qty = $('#input-quantity').val() ;
		  	var purchase_price = $('#input-purchase_price').val();
		  	var packing_type = $('#input-packing').val();
			if ((unit == 'litre') || (unit == 'gm')) {
				final_qty = (qty.replace(/[^0-9]+/i, '')) * 1000;
			} else {
				final_qty = qty.replace(/[^0-9]+/i, '');
			}
		  	var volume = $('#input-auto_volume').val();
		  	//alert(final_qty);
		  	//alert(volume);
		  	var order_qty = final_qty * volume;
		  	var value = qty * purchase_price;
		  	var units = $('#input-unit').val();
		  	if ((units == 'litre') || (units == 'ml')) {
		  		final_unit = 'ML';
		  	} else if ((units == 'gm') || (units == 'Kg')) {
		  		final_unit = 'KG';
		  	} else {
		  		final_unit = '';
		  	}

		  	$('#input-ordered_quantity').val('');
			$('#input-auto_unit').val(final_unit);
		  	$('#input-ordered_quantity').val('');
			$('#input-ordered_quantity').val(order_qty);
			$('#input-ordered_quantitys').val('');
			$('#input-ordered_quantitys').val(order_qty+' '+final_unit);
			$('#input-purchase_value').val('');
			$('#input-purchase_value').val(value);
			$('#input-purchase_values').val('');
			$('#input-purchase_values').val(value+' Rs / '+packing_type);
			var purchase_value = $('#input-purchase_value').val();
		  	var gst = $('#input-gst_rate').val();
		  	var gst_rate = (purchase_value * gst)/100;
			$('#input-gst_value').val('');
			$('#input-gst_value').val(gst_rate);
			var total = value + gst_rate;
			$('#input-total').val(total);

			$('#auto_order').html('');
			auto_order ='';
			auto_order +='<label style="font-weight: 400;" class="text-left">'+order_qty+' '+final_unit+'</label>';
			$('#auto_order').append(auto_order);

			$('#auto_purchase_value').html('');
			auto_purchase_value ='';
			auto_purchase_value +='<label style="font-weight: 400;" class="text-left">'+'Rs '+value+'</label>';
			$('#auto_purchase_value').append(auto_purchase_value);

			$('#auto_gst_rate').html('');
			auto_gst_rate ='';
			auto_gst_rate +='<label style="font-weight: 400;" class="text-left">'+gst+'%'+'</label>';
			$('#auto_gst_rate').append(auto_gst_rate);

			$('#auto_gst_value').html('');
			auto_gst_value ='';
			auto_gst_value +='<label style="font-weight: 400;" class="text-left">'+'Rs '+gst_rate+'</label>';
			$('#auto_gst_value').append(auto_gst_value);

			$('#auto_total').html('');
			auto_total ='';
			auto_total +='<label style="font-weight: 400;" class="text-left">'+'Rs '+total+'</label>';
			$('#auto_total').append(auto_total);
		});
		$('#input-quantity').focus();
		$('.dropdown-menu').hide();
		
		document.getElementById("input-quantity").onkeypress = function(event){
		    if (event.keyCode == 13 || event.which == 13){
				var qty = $('#input-quantity').val();
				if (qty != '') {
					if (qty != 0) {
						if (event.keyCode == 13 || event.which == 13){
							var req_by = $('#input-requested_by').val();
							$.ajax({
						        url: 'index.php?route=catalog/rawmaterialreq/autocompletereq&token=<?php echo $token; ?>'+'&requested_by='+req_by,
						        dataType: 'json',
						        success: function(json) {
						        	var req_by = $('#input-requested_by').val();
									$.ajax({
								        url: 'index.php?route=catalog/rawmaterialreq/valid_supplier&token=<?php echo $token; ?>'+'&requested_by='+req_by,
								        dataType: 'json',
								        success: function(json) {
								        	var valid = json.valid;
							            	if (valid == 0) {
							            		alert("Please Enter Valid Requested By!");
							            	} else {
							            		//alert("inn")
							            		var final_total = $('#input-totals').val();
												var tots = $('#input-total').val();
												var final_totals = parseFloat(final_total) + parseFloat(tots);
												var final_totalss = final_totals.toFixed(2);
												$('#input-totals').val(final_totalss);
												$('#input-final_total').val(final_totalss);

												var final_val = $('#input-vals').val();
												var vals = $('#input-purchase_value').val();
												var final_vals = parseFloat(final_val) + parseFloat(vals);
												var final_valss = final_vals.toFixed(2);
												$('#input-vals').val(final_valss);
												$('#input-final_val').val(final_valss);

												var final_gst_val = $('#input-gst_val').val();
												var gst_val = $('#input-gst_value').val();
												var final_gst_vals = parseFloat(final_gst_val) + parseFloat(gst_val);
												var final_gst_valss = final_gst_vals.toFixed(2);
												$('#input-gst_val').val(final_gst_valss);
												$('#input-final_gst_val').val(final_gst_valss);
												$('#input-requested_by').focus();
									        	add_productraw(item)
							            	}
								        }
							        });
						        }
					        });
						}
			        } else {
			        	alert('Please Enter Quantity!');
			        }
		    	} else {
					alert('Please Enter Quantity!');
					return false;
				}
			}
		};
  	}
});

document.getElementById("form-manufacturer").onsubmit = function(){
    setTimeout(function () {
	    window.location.reload(1);
	}, 5000)
}

$('#input-requested_by').autocomplete({
	'source': function(request, response) {
		$('#input-req').val(0);
	    if(request != ''){
	        $.ajax({
		        url: 'index.php?route=catalog/rawmaterialreq/autocompletereq&token=<?php echo $token; ?>&requested_by=' +  encodeURIComponent(request.term),
		        dataType: 'json',
		        success: function(json) {
		            response($.map(json, function(item) {
		            	return {
			                label: item['request'],
			                value: item['request']
		            	}
		            }));
		        }
	        });
	    }
	},
	'select': function(item, ui) {
	    $('#input-requested_by').val(ui.item.value);
	    $('#input-req').val(1);
	    document.getElementById("input-requested_by").onkeypress = function(event){
		    if (event.keyCode == 13 || event.which == 13){
		    	var req = $('#input-req').val();
				if (req == 1) {
					$('#input-productnew_code').focus();
				} else {
					$('#valid_supplier').show();
					$('#input-requested_by').focus();
				}
			}
		};
	    return false;
	}
});

$('#input-productnew_code').click(function () {
	var reqs_by = $("#input-req").val();
	if ((reqs_by == '') || (reqs_by == 0)) {
		//alert("Please Enter Valid Supplier!");
		$('#valid_supplier').show();
		$('#input-requested_by').focus();
	} else {
		$('#valid_supplier').hide();
	}
});

$('#input-productnew').click(function () {
	var reqs_by = $("#input-req").val();
	if ((reqs_by == '') || (reqs_by == 0)) {
		//alert("Please Enter Valid Supplier!");
		$('#valid_supplier').show();
		$('#input-requested_by').focus();
	} else {
		$('#valid_supplier').hide();
	}
});

$('#input-quantity').click(function () {
	var reqs_by = $("#input-req").val();
	if ((reqs_by == '') || (reqs_by == 0)) {
		//alert("Please Enter Valid Supplier!");
		$('#valid_supplier').show();
		$('#input-requested_by').focus();
	} else {
		$('#valid_supplier').hide();
	}
});

$('#input-requested_by').click(function () {
	$("#input-req").val(0);
});

$('#input-narration').click(function () {
	var reqs_by = $("#input-req").val();
	if ((reqs_by == '') || (reqs_by == 0)) {
		//alert("Please Enter Valid Supplier!");
		$('#valid_supplier').show();
		$('#input-requested_by').focus();
	}
});

document.getElementById("input-requested_by").onkeypress = function(event){
    if (event.keyCode == 13 || event.which == 13){
    	var req_by = $('#input-requested_by').val();
		$.ajax({
	        url: 'index.php?route=catalog/rawmaterialreq/autocompletereq&token=<?php echo $token; ?>'+'&requested_by='+req_by,
	        dataType: 'json',
	        success: function(json) {
	        	var req_by = $('#input-requested_by').val();
	            $.each(json, function (i, item) {
	            	if (req_by == item.request || req_by == '') {
	            		$('#input-productnew_code').focus();
	            	}
			    });
	        }
        });
	}
};


$('input[name=\'ordered_quantity\']').on('change', function() {
	alert('in');
});

</script>
<script>
setInterval(function() {
	var final_val = $('#input-final_val').val();
	$('#auto_final_val').html('');
	if (final_val > 0) {
		auto_final_val ='';
		auto_final_val +='<label style="font-weight: 400;" class="text-left"><b>'+final_val+'</b></label>';
		$('#auto_final_val').append(auto_final_val);
	}

	var final_total = $('#input-final_total').val();
	$('#auto_final_tot').html('');
	if (final_total > 0) {
		auto_final_tot ='';
		auto_final_tot +='<label style="font-weight: 400;" class="text-left"><b>'+final_total+'</b></label>';
		$('#auto_final_tot').append(auto_final_tot);
	}

	var final_gst_val = $('#input-final_gst_val').val();
	$('#auto_final_gst').html('');
	if (final_gst_val > 0) {
		auto_final_gst ='';
		auto_final_gst +='<label style="font-weight: 400;" class="text-left"><b>'+final_gst_val+'</b></label>';
		$('#auto_final_gst').append(auto_final_gst);
	}

}, 100);

$(document).ready(function() {
    $('#input-requested_by').focus();
});

function prev_ind() {
	var datas = $("#input-datas").val();
	if (datas == 1) {
		var check = confirm('Warning: Please Save This Indent! \n Click Ok To Discard This Indent');
		if (check == true) {
			url = 'index.php?route=catalog/rawmaterialreq&token=<?php echo $token; ?>';
			window.location.href = url;
		} else {
			return false;
		}
	} else {
		url = 'index.php?route=catalog/rawmaterialreq&token=<?php echo $token; ?>';
		window.location.href = url;
	}
}

function submit_form() {
	var order_id = $("#input-order_id").val();
	if (order_id == '') {
		var datas = $("#input-datas").val();
		if (datas == 1) {
			document.getElementById("form-manufacturer").submit();
		} else {
			alert("Please Enter Atleast One Medicine!");
			return false;
		}
	} else {
			document.getElementById("form-manufacturer").submit();
	}
}

$('.form-control').on('keydown', function(e) { 
    if (e.keyCode == 9) {
        $(this).focus();
       e.preventDefault();
    }
});
</script>
<?php echo $footer; ?>
