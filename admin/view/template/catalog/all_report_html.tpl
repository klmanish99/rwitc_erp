<?php 
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title><?php echo 'All Handicap Report'; ?></title>
		<base href="<?php echo $base; ?>" />
		<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
	</head>
	<body style="height: 100%;margin: 0;padding: 0;font-family: initial; !important;font-size: 20px;color: #666666; text-rendering: optimizeLegibility;">
		<div class="container" style="padding-right: 60px;padding-left: 60px;padding-top: 15px;margin-right: auto;margin-left: auto;">
			<div style="page-break-after: always;">
				<div>
				 	<?php if ($report_data) { ?>
					 	<?php foreach ($report_data as $key => $value) { ?>
							<div>
					      		<span style="font-size:28px;font-weight: bold;color: #000;">
							  		<center>ROYAL WESTERN INDIA TURF CLUB, LTD.</center>
					      		</span>
						    	<span style="font-size:20px;font-weight: bold;color: #000;">
						    		<center><?php echo $value['season']; ?> Meeting <?php echo $value['year']; ?>, <?php echo $value['race_day']; ?>, <?php echo $value['race_date_days']; ?>, <?php echo $value['race_date_day']; ?>ST <?php echo $value['race_date_month']; ?>, <?php echo $value['race_date_year']; ?>.</center>
					      		</span><br><br>
								<span style="font-size:20px;font-weight: bold;color: #000;">
							  		<center>HANDICAPS</center>
					      		</span><br>
					      		<span style="font-size:20px;font-weight: bold;color: #000;">
							  		<?php echo $value['race_name']; ?>(<?php echo $value['race_description']; ?>)
					      		</span><br>
					      		<span style="font-size:20px;font-weight: bold;color: #000;">
							  		(About) <?php echo $value['distance']; ?> Metres
					      		</span><br><br>
					      		<table>
					      			<tr>
							      		<?php foreach ($horse_data as $hkey => $hvalue) { ?>
									  		<td style="font-size:10px;font-weight: bold;color: #000;" >(<?php echo $hvalue['horse_rating']; ?>)</td> <td style="font-size:10px;font-weight: bold;color: #000;" ><?php echo $hvalue['horse_name']; ?></td> <td style="font-size:10px;font-weight: bold;padding-left: 50px;padding-right: 50px;color: #000;" ><?php echo $hvalue['weight']; ?></td>
							      		<?php } ?>
							      	</tr>
						      	</table>
					      		<br><br><br>
					      		<span style="font-size:30px;font-weight: bold;color: #000;">
							  		<center>******</center>
					      		</span><br>
					      		<span style="font-size:30px;font-weight: bold;color: #000;">
							  		<center>******</center>
					      		</span><br>
					      		<span style="font-size:20px;font-weight: bold;color: #000;">
							  		<center># FIRST TIME RUNNER</center>
					      		</span>
					      		<span style="font-size:20px;font-weight: bold;color: #000;">
							  		<center>ACCEPTANCES: <?php echo $value['final_acceptance_date_times']; ?> ON <?php echo $value['acceptance_date_days']; ?>, <?php echo $value['acceptance_date_day']; ?>TH <?php echo $value['acceptance_date_month']; ?>, <?php echo $value['acceptance_date_year']; ?>.</center>
					      		</span>
					      		<span style="font-size:20px;font-weight: bold;color: #000;">
							  		<center>DECLARATIONS: <?php echo $value['final_declaration_date_times']; ?> ON <?php echo $value['declaration_date_days']; ?>, <?php echo $value['declaration_date_day']; ?>TH <?php echo $value['declaration_date_month']; ?>, <?php echo $value['declaration_date_year']; ?>.</center>
					      		</span><br>
					      		<span style="float:right; font-size:20px;font-weight: bold;color: #000;">
							  		Handicapper,
					      		</span><br>
					      		<span style="float:right; font-size:20px;font-weight: bold;color: #000;">
							  		ROYAL WESTERN INDIA TURF CLUB LTD,
					      		</span><br>
					      		<span style="font-size:20px;font-weight: bold;color: #000;">
							  		<?php echo $value['season']; ?>: <?php echo $value['acceptance_date_day']; ?>TH <?php echo $value['acceptance_date_month']; ?>, <?php echo $value['acceptance_date_year']; ?>
					      		</span><br><br><br>
					      		<span style="font-size:20px;font-weight: bold;color: #000;">
							  		Shoe and bits on record are indicated below. If there is a change at acceptance trainers are to indicate the same in the blue from to be submitted with white scratching form
					      		</span><br><br>
					      		<span style="font-size:20px;font-weight: bold;color: #000;">
							  		<?php echo $value['race_name']; ?>
					      		</span><br><br><br><br>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>					
		</div>
	</body>
</html>