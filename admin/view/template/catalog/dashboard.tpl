<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
    <div class="container-fluid">
        <?php if ($error_install) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_install; ?>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div style="display: flex; width: 135%;">
            <div style="display: grid; width: 45%;">
                <div style="height: auto; width: 100%;" class="row">
                    <div style="width: 50%;" class="col-sm-6"><?php echo $order; ?></div>
                    <div style="width: 50%;" class="col-sm-6"><?php echo $sale; ?></div>
                </div>
                <div style=" width: 100%;" class="row">
                    <div style="width: 50%;" class="col-sm-6"><?php echo $customer; ?></div>
                    <div style="width: 50%;" class="col-sm-6"><?php echo $online; ?></div>
                </div>
                <div class="row">
                    <div style="width: 96%;" class="col-lg-6 col-md-12 col-sx-12 col-sm-12"><?php echo $map; ?></div>
                </div>
            </div>
            <div style="display: grid; width: 55%;" >
                <div class="col-lg-6 col-md-6 col-sx-6 col-sm-6"><?php echo $chart; ?></div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>