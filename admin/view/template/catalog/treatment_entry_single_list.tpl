<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header" >
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> 
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Go To Dashboard" class="btn btn-primary">Go To Dashboard</a>
				<button style="display: none;"> type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-inward').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<h1>Treatment Entry</h1>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i>Treatment Entry</h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-inward">
					<div class="well">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="col-sm-2">
										<input type="text" name="filter_clinic_name" value="<?php echo $filter_clinic_name ?>" placeholder="Clinic Name" id="input-filter_clinic_name" class="form-control" />
										<input type="hidden" name="filter_clinic_id" value="<?php echo $filter_clinic_id ?>" placeholder="Clinic Name" id="input-filter_clinic_id" class="form-control" />
									</div>
									<div class="col-sm-2">
										<input type="text" name="filter_doctor" value="<?php echo $filter_doctor ?>" placeholder="<?php echo "Doctor Name"; ?>" id="input-filter_doctor" class="form-control" />
										<input type="hidden" name="filter_doctor_id" value="<?php echo $filter_doctor_id ?>" placeholder="doctor Name" id="input-filter_doctor_id" class="form-control" />
									</div>
									<div class="col-sm-2">
			                   			<input type="text" name="filterHorseName" id="filterHorseName" value="<?php echo $filterHorseName; ?>" placeholder="Horse Name" class="form-control">
			                   			<input type="hidden" name="filter_horse_id" value="<?php echo $filter_horse_id ?>"  id="filter_horse_id" class="form-control" />
			                  		</div>
			                  		<div class="col-sm-2">
			                   			<input type="text" name="filterTrainerName" id="filterTrainerName" value="<?php echo $filterTrainerName; ?>" placeholder="Trainer Name" class="form-control">
			                   			<input type="hidden" name="filter_trainer_id" value="<?php echo $filter_trainer_id ?>"  id="filter_trainer_id" class="form-control" />
			                  		</div>
									 <div class="col-sm-2" >
				                          <div class="input-group date">
				                              <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="From Date" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
				                              <span class="input-group-btn">
				                                  <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
				                              </span>
				                          </div>
				                    </div>
									 <div class="col-sm-2" >
				                          <div class="input-group date">
				                              <input type="text" name="filter_dates" value="<?php echo $filter_dates; ?>" placeholder="To Date" data-date-format="DD-MM-YYYY" id="input-filter_dates" class="form-control" />
				                              <span class="input-group-btn">
				                                  <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
				                              </span>
				                          </div>
				                    </div>
									<div class="pull-right col-sm-2">
										<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<?php if($is_user == '0'){ ?>
										<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
										<?php } ?>
										
										<td class="text-left"><?php echo ' Code'; ?></td>
										<td class="text-left"><?php echo 'Date'; ?></td>
										<td class="text-left"><?php echo 'Clinic Name'; ?></td>
										<td class="text-left"><?php echo 'Doctor Name'; ?></td>
										<td class="text-left"><?php echo 'Horse Name'; ?></td>
										<td class="text-left"><?php echo 'Trainer Name'; ?></td>
										<td class="text-left"><?php echo 'Total Item'; ?></td>
										<td class="text-left"><?php echo 'Total Qty'; ?></td>
										<td class="text-left"><?php echo 'Total Amt'; ?></td>
										<td class="text-left"><?php echo 'Logs'; ?></td>
										<td class="text-right"><?php echo 'View'; ?></td>
									</tr>
								</thead>
								<tbody>
									<?php if ($medicine_trans_datas) { ?>
										<?php $i = 1; ?>
										<?php foreach ($medicine_trans_datas as $medicine_transfer) { ?>
										<tr>
											<?php if($is_user == '0'){ ?>
											<td style="display: none;" class="text-center"><?php if (in_array($medicine_transfer['id'], $selected)) { ?>
												<input type="checkbox" name="selected[]" value="<?php echo $medicine_transfer['id']; ?>" checked="checked" />
												<?php } else { ?>
												<input type="checkbox" name="selected[]" value="<?php echo $medicine_transfer['id']; ?>" />
												<?php } ?>
											</td>
											<?php } ?>
										 
											
											<td class="text-left">
												<?php echo $medicine_transfer['issue_no']; ?>
												<input type="hidden" name="count" class="count" value="<?php echo $i; ?>" />
											</td>
											<td class="text-left"><?php echo date('d-M-Y', strtotime($medicine_transfer['entry_date'])); ?></td>
											<td class="text-left"><?php echo $medicine_transfer['clinic_name']; ?></td>
											<td class="text-left"><?php echo $medicine_transfer['doctor_name']; ?></td>
											<td class="text-left"><?php echo $medicine_transfer['horse_name']; ?></td>
											<td class="text-left"><?php echo $medicine_transfer['trainer_name']; ?></td>

											<td  class="text-right">
												<?php echo $medicine_transfer['total_item']; ?>
											</td>
											<td  class="text-right">
												<?php echo $medicine_transfer['total_qty']; ?>
											</td>
											<td  class="text-right">
												<!-- <a data-toggle="modal" data-target="#myModal" class=""> --><?php echo $medicine_transfer['total_amt']; ?><!-- </a> -->
											</td>
											<td class="text-left"><?php echo $medicine_transfer['log_datas']; ?></td>
											<td class="text-right">
												<!-- <a href="<?php echo $medicine_transfer['print']; ?>" data-toggle="tooltip" title="<?php echo 'Print'; ?>" class="btn btn-primary"><i class="fa fa-print"></i></a> -->
												<a href="<?php echo $medicine_transfer['edit']; ?>" data-toggle="tooltip" title="<?php echo 'View'; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
											</td>
										</tr>
										<?php $i ++; ?>
										<?php } ?>
									<?php } else { ?>
									<tr>
										<td class="text-center" colspan="10"><?php echo $text_no_results; ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>
		        <div class="row">
		        	<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		        	<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		        </div>
	        </div>
		</div>
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Finished Product Overview</h4>
					</div>
					<div class="modal-body edit-content">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--

	$('input[name=\'filter_clinic_name\']').autocomplete({
	  'source': function(request, response) {
	  	$('input[name=\'filter_clinic_name\']').val('');
		$.ajax({
		  url: 'index.php?route=catalog/medicine_transfer/autocompleteClinic&token=<?php echo $token; ?>&filter_clinic_name=' +  encodeURIComponent(request),
		  dataType: 'json',
		  success: function(json) {
			response($.map(json, function(item) {
			  return {
				label: item['clinic_name'],
				value: item['clinic_id']
			  }
			}));
		  }
		});
	  },
	  'select': function(item) {
		$('input[name=\'filter_clinic_name\']').val(item['label']);
		$('input[name=\'filter_clinic_id\']').val(item['value']);
	  }
	});

	$('input[name=\'filter_doctor\']').autocomplete({
	  'source': function(request, response) {
		$.ajax({
		  url: 'index.php?route=catalog/medicine_transfer/autocompleteDoctor&token=<?php echo $token; ?>&filter_doctor_name=' +  encodeURIComponent(request),
		  dataType: 'json',
		  success: function(json) {
			response($.map(json, function(item) {
			  return {
				label: item['doctor_name'],
				value: item['doctor_id']
			  }
			}));
		  }
		});
	  },
	  'select': function(item) {
		$('input[name=\'filter_doctor\']').val(item['label']);
		$('input[name=\'filter_doctor_id\']').val(item['value']);
	  }
	});

	 $('#filterTrainerName').autocomplete({
	    delay: 500,
	    source: function(request, response) {
	        $('#trainer_id').val('');
	        if(request != ''){
	            $.ajax({
	                url: 'index.php?route=catalog/trainer/autocompleteTrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
	                dataType: 'json',
	                success: function(json) {   
	                    response($.map(json, function(item) {
	                        return {
	                            label: item.trainer_name,
	                            value: item.trainer_name,
	                            trainer_id:item.trainer_id,
	                            trainer_codes:item.trainer_code
	                        }
	                    }));
	                }
	            });
	        }
	    }, 
	    select: function(item) {
	        console.log(item);
	        $('#filterTrainerName').val(item.value);
	        $('#filter_trainer_id').val(item.trainer_id);
	        $('.dropdown-menu').hide();
	        trainerfilter();
	        return false;
	    },
	});

	 	$('#filterHorseName').autocomplete({
    delay: 500,
    source: function(request, response) {
        $('#filter_horse_id').val('');
        if(request != ''){
            $.ajax({
                url: 'index.php?route=catalog/horse/autocompleteHorse&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {   
                    $('#trainer_codes_id').find('option').remove();
                    response($.map(json, function(item) {
                        return {
                            label: item.trainer_name,
                            value: item.trainer_name,
                            trainer_id:item.trainer_id,
                        }
                    }));
                }
            });
        }
    }, 
    select: function(item) {
        console.log(item);
        $('#filterHorseName').val(item.value);
        $('#filter_horse_id').val(item.trainer_id);
        $('.dropdown-menu').hide();
        filter();
        return false;
    },
	});


$('#button-filter').on('click', function() {
  var url = 'index.php?route=catalog/treatment_entry_single/getList&token=<?php echo $token; ?>';

  var filter_clinic_name = $('input[name=\'filter_clinic_name\']').val();
 
  if (filter_clinic_name) {
	var filter_clinic_id = $('input[name=\'filter_clinic_id\']').val();
	if (filter_clinic_id) {
	  url += '&filter_clinic_id=' + encodeURIComponent(filter_clinic_id);
	}
	url += '&filter_clinic_name=' + encodeURIComponent(filter_clinic_name);
  
  }

  var filter_doctor = $('input[name=\'filter_doctor\']').val();
 
  if (filter_doctor) {
	var filter_doctor_id = $('input[name=\'filter_doctor_id\']').val();
	if (filter_doctor_id) {
	  url += '&filter_doctor_id=' + encodeURIComponent(filter_doctor_id);
	}
	url += '&filter_doctor=' + encodeURIComponent(filter_doctor);
  
  }

  var filterHorseName = $('input[name=\'filterHorseName\']').val();
 
  if (filterHorseName) {
	var filter_horse_id = $('input[name=\'filter_horse_id\']').val();
	if (filter_horse_id) {
	  url += '&filter_horse_id=' + encodeURIComponent(filter_horse_id);
	}
	url += '&filterHorseName=' + encodeURIComponent(filterHorseName);
  
  }

   var filterTrainerName = $('input[name=\'filterTrainerName\']').val();
 
  if (filterTrainerName) {
	var filter_trainer_id = $('input[name=\'filter_trainer_id\']').val();
	if (filter_trainer_id) {
	  url += '&filter_trainer_id=' + encodeURIComponent(filter_trainer_id);
	}
	url += '&filterTrainerName=' + encodeURIComponent(filterTrainerName);
  
  }

  	var filter_date = $('input[name=\'filter_date\']').val();
    if (filter_date) {
        url += '&filter_date=' + encodeURIComponent(filter_date);
    }
  	var filter_dates = $('input[name=\'filter_dates\']').val();
    if (filter_dates) {
        url += '&filter_dates=' + encodeURIComponent(filter_dates);
    }

  location = url;
});



$(document).ready(function()               
    {
        // enter keyd
        $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                 $('#button-filter').trigger('click');
             }
        });
    });

//--></script>
<script type="text/javascript">
$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY',
});
</script>



<?php echo $footer; ?>