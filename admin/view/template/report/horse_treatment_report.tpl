<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header" >
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Go To Dashboard" class="btn btn-primary">Go To Dashboard</a>
        	</div>
			<h1>Datewise Horse Report</h1>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i>Datewise Horse Report</h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-requested_by">From Date</label>
								<label class="col-sm-2 control-label" for="input-requested_by">To Date</label>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
			                    <div class="col-sm-2" >
			                        <div class="input-group date">
			                            <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
			                            <span class="input-group-btn">
			                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="col-sm-2" >
			                        <div class="input-group date">
			                            <input type="text" name="filter_dates" value="<?php echo $filter_dates; ?>" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-filter_dates" class="form-control" />
			                            <span class="input-group-btn">
			                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			                            </span>
			                        </div>
			                    </div>
								<div class="col-sm-1">
									<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
								</div>
								<div class="col-sm-1">
									<button type="button" id="button-export" class="btn btn-primary"><i class="fa fa-download"></i> <?php echo 'Export'; ?></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
									<td class="text-center"><?php echo 'Sr No'; ?></td>
									<td class="text-center"><?php echo 'Entry Date'; ?></td>
									<td class="text-center"><?php echo 'Horse'; ?></td>
									<td class="text-center"><?php echo 'Trainer'; ?></td>
									<td class="text-center"><?php echo 'Medicine Name'; ?></td>
									<td class="text-center"><?php echo 'Type'; ?></td>
									<td class="text-center"><?php echo 'Unit'; ?></td>
									<td class="text-center"><?php echo 'Medicine Qty'; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php if ($horsetreatments) { ?>
									<?php $i = 1; ?>
									<?php foreach ($horsetreatments as $horsetreatment) { ?>
									<tr>
			                        	<td class="text-center"><?php echo $i; ?></td>
			                        	<td class="text-center"><?php echo $horsetreatment['entry_date']; ?></td>
			                        	<td class="text-left"><?php echo $horsetreatment['horse_name']; ?></td>
			                        	<td class="text-left"><?php echo $horsetreatment['trainer_name']; ?></td>
			                        	<td class="text-left"><?php echo $horsetreatment['medicine_name']; ?></td>
			                        	<td class="text-left"><?php echo $horsetreatment['med_type']; ?></td>
			                        	<td class="text-left"><?php echo $horsetreatment['unit']; ?></td>
			                        	<td class="text-right"><?php echo $horsetreatment['medicine_qty']; ?></td>
									</tr>
									<?php $i ++; ?>
									<?php } ?>
								<?php } else { ?>
								<tr>
									<td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
		        	<div style="display: none;" class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		        	<div style="display: none;" class="col-sm-6 text-right"><?php echo $results; ?></div>
		        </div>
	        </div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--

$('#button-filter').on('click', function() {
  var url = 'index.php?route=report/horse_treatment_report&token=<?php echo $token; ?>';

  var filter_date = $('input[name=\'filter_date\']').val();

  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  
  }

  var filter_dates = $('input[name=\'filter_dates\']').val();

  if (filter_dates) {
    url += '&filter_dates=' + encodeURIComponent(filter_dates);
  
  }

  location = url;
});

$("#button-export").on('click', function() {
	var url = 'index.php?route=report/horse_treatment_report/prints&token=<?php echo $token; ?>';

	var filter_date = $('input[name=\'filter_date\']').val();
	if (filter_date) {
		url += '&filter_date=' + encodeURIComponent(filter_date);
	}

	var filter_dates = $('input[name=\'filter_dates\']').val();
	if (filter_dates) {
		url += '&filter_dates=' + encodeURIComponent(filter_dates);
	}

	location = url;
});
</script>

<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
</script>
<?php echo $footer; ?>