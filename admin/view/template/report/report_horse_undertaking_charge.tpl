<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <!-- <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div> -->
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-3">
                        <input type="text" name="filterHorseName" id="filterHorseName" value="<?php echo $filter_hourse_name; ?>" placeholder="Horse Name" class="form-control">
                        <input type="hidden" name="filterHorseId" id="filterHorseId" value="<?php echo $filterHorseId; ?>"  class="form-control">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" name="trainer_name"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $filter_trainer_name; ?>" placeholder="Trainer Name" id="input-trainer_name" class="form-control" />
                        <input type="hidden" name="trainer_id"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $filter_trainer_id; ?>" id="trainer_id" class="form-control" />
                    </div>
                    <a onclick="filter();"  id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
                </div>
            </div>
    
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left"><?php echo 'Horse Name'; ?></td>
                <td class="text-left"><?php echo 'Trainer Name'; ?></td>
              <!--   <td class="text-right"><?php echo 'Date Of Charge'; ?></td> -->
                <td class="text-right"><?php echo 'Left Charge'; ?></td>
                <td class="text-right"><?php echo 'Date After Left'; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php if ($undertakingcharges_datas) { ?>
              <?php foreach ($undertakingcharges_datas as $undertaking_charge) { ?>
              <tr>
                <td class="text-left"><?php echo $undertaking_charge['horse_name']; ?></td>
                <td class="text-left"><?php echo $undertaking_charge['trainer_name']; ?></td>
                <!-- <td class="text-right"><?php echo $undertaking_charge['date_of_charge']; ?></td> -->
                <td class="text-right"><?php echo $undertaking_charge['left_date_of_charge']; ?></td>
                <td class="text-right"><?php echo $undertaking_charge['getintrval_left_Charge_date']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<script type="text/javascript">
    function filter() { 
        var filter_hourse_name = $('#filterHorseName').val();
        var filter_hourse_id = $('#filterHorseId').val();
        var filter_trainer_name = $('#input-trainer_name').val();
        var filter_trainer_id = $('#trainer_id').val();
        url = 'index.php?route=report/report_horse_undertaking_charge&token=<?php echo $token; ?>';
        if (filter_hourse_name) {
          url += '&filter_hourse_name=' + encodeURIComponent(filter_hourse_name);
        }

        if (filter_hourse_id) {
          url += '&filter_hourse_id=' + encodeURIComponent(filter_hourse_id);
        }

        if (filter_trainer_name) {
            url += '&filter_trainer_name=' + encodeURIComponent(filter_trainer_name);
        } 
        if (filter_trainer_id) {
            url += '&filter_trainer_id=' + encodeURIComponent(filter_trainer_id);
        }
        window.location.href = url;
      
    }

    $('#filterHorseName').autocomplete({
    delay: 500,
    source: function(request, response) {
        $('#filterHorseId').val('');
        if(request != ''){
            $.ajax({
                url: 'index.php?route=report/report_horse_undertaking_charge/autocompleteHorse&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {   
                    response($.map(json, function(item) {
                        return {
                            label: item.horse_name,
                            value: item.horse_name,
                            horse_id:item.horse_id,
                        }
                    }));
                }
            });
        }
    }, 
    select: function(item) {
        console.log(item);
        $('#filterHorseName').val(item.value);
        $('#filterHorseId').val(item.horse_id);
        $('.dropdown-menu').hide();
        return false;
    },
    });
     $('#input-trainer_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#trainer_id').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/horse/autocompleteTrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
                        dataType: 'json',
                        success: function(json) {   
                            $('#trainer_codes_id').find('option').remove();
                            response($.map(json, function(item) {
                                return {
                                    label: item.trainer_name,
                                    value: item.trainer_name,
                                    trainer_id:item.trainer_id,
                                    trainer_codes:item.trainer_code
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(item) {
                console.log(item);
                $('#input-trainer_name').val(item.value);
                $('#trainer_id').val(item.trainer_id);
                $('.dropdown-menu').hide();
                $('#date_charge_trainer').focus();
                return false;
            },
        });
    $('#filterHorseName').keydown(function(e) {
      if (e.keyCode == 13) {
        filter();
      }
    });
    $('#input-trainer_name').keydown(function(e) {
      if (e.keyCode == 13) {
        filter();
      }
    });
</script>
<?php echo $footer; ?>