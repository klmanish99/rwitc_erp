<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
  </div>
  <link type="text/css" href="view/stylesheet/myform.css" rel="stylesheet" media="screen" />
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
    <?php } ?>
    <?php if ($error_medicine_error) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_medicine_error; ?>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <?php } ?>
    <?php if ($filter_doctor_id_error) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $filter_doctor_id_error; ?>
        <button type="button" class="close" data-dismiss="alert"></button>
    </div>
    <?php } ?>
    <?php if ($day_closed_warming) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $day_closed_warming; ?>
        <button type="button" class="close" data-dismiss="alert"></button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="col-sm-12 panel-heading">
        <h3 class="panel-title"><i class="fa fa-hospital-o"></i>Store Reconsilation</h3>
        <div class="pull-right">
          <a href="<?php echo $back_monthly_daily_reconsilation; ?>"  class="btn btn-warning" style="border-color:#f33333;bottom: 2px !important;right: 5px !important;width: 161px !important; display: none; "><i class="fa fa-arrow-right"></i>Monthly Reconsilation</a>
          <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Go To Dashboard" class="btn btn-primary">Go To Dashboard</a>
        </div>
      </div>
      <div style="margin-top: 5%;" class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
            <div class="well">
              <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="col-sm-3" >
                              <div class="input-group date">
                                  <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="Date Start" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
                                  <span class="input-group-btn">
                                      <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                  </span>
                              </div>
                                
                         </div>
                        <div class="col-sm-1"><a onclick="filter();"  id="button_filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a></div>
                        <div class="col-sm-1"><button type="button" id="button-export" class="btn btn-primary"><i class="fa fa-download"></i> <?php echo 'Export'; ?></button></div>
                        <div class="col-sm-1"><button onclick="confirm('<?php echo 'Do You want to Save the Changes'; ?>') ? $('#form-manufacturer').submit() : false;" type="button" form="form-manufacturer"  class="btn btn-primary dayclosebtn" style = "background-color: #1d1d1e;border-color: #1d1d1e;">Day Close</button></div> 
                        <!-- <input type="submit" name="submit"  class="btn btn-primary"> -->
                    </div>
                </div>
        
              </div>
            </div>
            <?php if ($med_transaction) {// echo "<pre>";print_r($med_transaction);exit; ?>
                <div class="table-responsive">
                    <?php foreach ($med_transaction as $medicine_transfer_datas_doctor) {  //echo "<pre>";print_r($medicine_transfer_datas_doctor);exit;?>
                        <?php foreach ($medicine_transfer_datas_doctor as $doctores =>  $medicine_transfer_datas_doctors) { // echo "<pre>";print_r($doctores);exit;?>
                            <table class="table table-bordered table-hover" id="tbladdMedicineTran">
                                <thead>
                                    <?php if(isset($doctores)){ ?>
                                        <tr>
                                            <td class="text-left" colspan ="9" style="color: #030303" > <?php echo strtoupper($doctores); ?> 
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td class="text-center">Sr No</td>
                                        <td class="text-center">Medicine Name</td>
                                        <td class="text-center">Unit</td>
                                        <td class="text-center">Opening</td>
                                        <td class="text-center">Transfer Quantity</td>
                                        <td class="text-center">Treatment Quantity</td>
                                        <td class="text-center">Return Medicine</td>
                                        <td class="text-center">Avilable Qty</td>
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; //echo '<pre>';print_r($medicine_transfer_datas);exit; ?>
                                    <?php foreach ($medicine_transfer_datas_doctors as $key => $medicine_transfer_data) { //echo '<pre>';print_r($medicine_transfer_data['product_name'] ); ?>
                                         <?php if(isset($medicine_transfer_data['product_name'])) {  ?>
                                            <tr>
                                                <td class="text-center"><?php echo $i++; ?></td>
                                                <td class="text-left"><?php echo $medicine_transfer_data['product_name'] ?></td>
                                                <td class="text-center"><?php echo $medicine_transfer_data['store_unit']; ?></td>
                                                <td class="text-center"><?php echo $medicine_transfer_data['opening']; ?></td>
                                                <td class="text-center"><?php echo $medicine_transfer_data['total_trans']; ?></td>
                                                <td class="text-center"><?php echo $medicine_transfer_data['treatment_medicine']; ?></td>
                                                <td class="text-center"><?php echo $medicine_transfer_data['return_medicine']; ?></td>
                                                <td class="text-center"><?php echo $medicine_transfer_data['closing']; ?></td>
                                                <td class="col-sm-3">
                                                   <?php if(($medicine_transfer_data['redirect_transfer'] != '') && ($medicine_transfer_data['redirect_tratement'] != '')) { ?>
                                                         <?php echo 'Current Day'; ?>
                                                     <?php } else if(($medicine_transfer_data['redirect_transfer'] != '') && ($medicine_transfer_data['redirect_tratement'] == '')) {  ?>
                                                        <?php echo $medicine_transfer_data['redirect_transfer']; ?>
                                                    <?php } else { ?>
                                                        <?php echo 'Day Closed'; ?>
                                                    <?php }  ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php }  ?>
                                </tbody>
                            </table>
                        <?php } ?> 
                    <?php } ?>
                </div>
            <?php } else if(empty($med_transaction) && $no_result == 0)  { ?>
                 <h3 class="text-center noresults" >Please Day Close !</h3>
            <?php } else { ?>
                <h3 class="text-center noresults" ><?php echo $text_no_results; ?></h3>
            <?php } ?>
            <?php if ($new_med_treatment_transaction) { ?>
               
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr ><td class="text-left" colspan ="6" >Medicine Used For Treatment But Not Transfer</td></tr>
                                <tr>
                                   <td class="text-center">Sr No</td>
                                   <td class="text-center">Date</td>
                                    <td class="text-center">Medicine Name</td>
                                    <td class="text-center">Unit</td>
                                    <td class="text-center">Quantity</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($new_med_treatment_transaction as $medicine_transfer) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $i++; ?></td>
                                        <td class="text-left"><?php echo date('d-m-Y', strtotime($medicine_transfer['entry_date'])); ?></td>
                                        <td class="text-left"><?php echo $medicine_transfer['product_name']; ?></td>
                                        <td  class="text-left"><?php echo $medicine_transfer['store_unit']; ?></td>
                                        <td  class="text-right"><?php echo $medicine_transfer['medicine_qtys']; ?></td>
                                    </tr>
                                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
               
            <?php } ?>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    function filter() { 
        url = 'index.php?route=report/store_reconsilation&token=<?php echo $token; ?>';

        var filter_date = $('input[name=\'filter_date\']').val();
        if (filter_date) {
            url += '&filter_date=' + encodeURIComponent(filter_date);
        }
        
        window.location.href = url;
    }

    $("#button-export").on('click', function() {
        var url = 'index.php?route=report/store_reconsilation/prints&token=<?php echo $token; ?>';

        var filter_date = $('input[name=\'filter_date\']').val();
        if (filter_date) {
            url += '&filter_date=' + encodeURIComponent(filter_date);
        }
        
        window.location.href = url;
    });

    $( document ).ready(function() {
        /*var tds = $("table#tbladdMedicineTran tr td:contains('Day Closed')").text();
        //alert(tds);
        if(tds){
            $('.dayclosebtn').hide();
        } else {
             $('.dayclosebtn').show();
        }*/
        var filter_date = $('input[name=\'filter_date\']').val() || '';
        currrent_date_entry = 0;
         if(filter_date != ''){
            filter_date = filter_date.split('-');
            fromdate = filter_date[2] + "-" + filter_date[1] + "-" + filter_date[0];
            //alert(fromdate);
            var start_date = new Date(fromdate);
            var today = new Date();
            if(today.getDate() == start_date.getDate()){
                currrent_date_entry = 1;
                var tds = $("table#tbladdMedicineTran tr td:contains('Day Closed')").text();
                if(tds){
                   currrent_date_entry = 0;  
                }
            }
         }
        var tds = $("table#tbladdMedicineTran tr td:contains('Day Not Closed')").text();
        //alert(currrent_date_entry);
        if(tds ||  currrent_date_entry == 1){
            $('.dayclosebtn').show();
        } else {
            $('.dayclosebtn').hide(); 
        }

    });

    /*$('#filter_doctor_name').autocomplete({
      delay: 500,
      source: function(request, response) {
          if(request != ''){
            $('#filter_doctor_id').val('');
              $.ajax({
                  url: 'index.php?route=report/medicine_transaction/autocompletedoc&token=<?php echo $token; ?>&filter_doctor_name=' +  encodeURIComponent(request.term), dataType: 'json',
                  success: function(json) {   
                      response($.map(json, function(item) {
                          return {
                              label: item.doctor_name,
                              value: item.doctor_name,
                              docter_id: item.id,
                          }
                      }));
                  }
              });
          }
      }, 
      select: function(event, ui) {
            $('#filter_doctor_name').val(ui.item.value);
            $('#filter_doctor_id').val(ui.item.docter_id);
            $('.dropdown-menu').hide();
          return false;
      },
    });*/
</script>
<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
</script>
<?php echo $footer; ?>