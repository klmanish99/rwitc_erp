<?php
/*echo'<pre>';
print_r($indents);
exit;*/
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}

td {
  border: 1px solid #dddddd;
  padding: 8px;
}

</style>
</head>
<body>

<h2 style="text-align: center">Inward Report</h2>
<?php 
	$timestamp = time(); 
?>
<h4 style="text-align: center">Geneated On: <?php echo(date("d-m-Y h:i A", $timestamp)) ?></h4>
<?php if ($from_date && $to_date) { ?>
	<h4 style="text-align: center">From: <?php echo $from_date ?> To: <?php echo $to_date ?></h4>
<?php } ?>

<table>
	<tr>
		<th>Inward Code</th>
		<th>Date</th>
		<th>Po No</th>
		<th>Medicine</th>
		<th>Po Quantity</th>
		<th>Quantity Inward</th>
		<th>GST Rate</th>
		<th>GST Value Rs</th>
		<th>Indent</th>
	</tr>
	<?php if ($inwards) { ?>
		<?php foreach ($inwards as $key => $value) { ?>
			<tr>
			    <td style="text-align: left;"><?php echo $value['inward_code'] ?></td>
			    <td style="text-align: left;"><?php echo $value['date'] ?></td>
			    <td style="text-align: left;"><?php echo $value['po_no'] ?></td>
			    <td style="text-align: left;"><?php echo $value['productraw_name'] ?></td>
			    <td style="text-align: right;"><?php echo $value['po_qty'] ?></td>
			    <td style="text-align: right;"><?php echo $value['qty'] ?></td>
			    <td style="text-align: left;"><?php echo $value['gst_rate'] ?> %</td>
			    <td style="text-align: right;"><?php echo $value['gst_value'] ?></td>
			    <td style="text-align: left;"><?php echo $value['indent'] ?></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>

</body>
</html>
