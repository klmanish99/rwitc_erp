<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<!-- <div class="page-header" >
		<div class="container-fluid">
			<div class="pull-right">
			</div>
			<h1>Medicine Dump Report</h1>
		</div>
	</div> -->
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i>Medicine Dump Report</h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="input-requested_by">Medicine</label>
								<label class="col-sm-2 control-label" for="input-requested_by">From Date</label>
								<label class="col-sm-2 control-label" for="input-requested_by">To Date</label>
								<div class="col-sm-2">
				                    <button id="reset_filter" class="pull-right btn btn-primary" type="button">Reset</button>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<div class="col-sm-3">
									<input type="text" name="filter_medicine" value="<?php echo $filter_medicine; ?>" placeholder="<?php echo "Medicine"; ?>" id="input-filter_medicine" class="form-control" />
								</div>
			                    <div class="col-sm-2" >
			                        <div class="input-group date">
			                            <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
			                            <span class="input-group-btn">
			                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="col-sm-2" >
			                        <div class="input-group date">
			                            <input type="text" name="filter_dates" value="<?php echo $filter_dates; ?>" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-filter_dates" class="form-control" />
			                            <span class="input-group-btn">
			                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			                            </span>
			                        </div>
			                    </div>
			                    <div style="display: none;" class="col-sm-3">
									<input type="text" name="filter_po_no" value="<?php echo $filter_po_no; ?>" placeholder="<?php echo "Po Number"; ?>" id="input-filter_po_no" class="form-control" />
								</div>
								<div class="col-sm-1">
									<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
								</div>
								<div class="col-sm-1">
									<button type="button" id="button-export" class="btn btn-primary"><i class="fa fa-download"></i> <?php echo 'Export'; ?></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
				<label class="col-sm-1 control-label" for="input-requested_by">Note:</label>
					<div class="col-sm-8">
						<input type="text" name="note" placeholder="<?php echo "Note"; ?>" id="input-note" class="form-control" />
					</div>
				</div>
				<?php if ($inwards) { ?>
					<h3>GRN</h3>
					<div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<?php if($is_user == '0'){ ?>
										<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
										<?php } ?>
										
										<td class="text-center"><?php echo 'Dates'; ?></td>
										<td class="text-center"><?php echo 'Supplier'; ?></td>
										<td class="text-center"><?php echo 'Quantity'; ?></td>
										<td class="text-center"><?php echo 'Unit'; ?></td>
										<td class="text-center"><?php echo 'Value'; ?></td>
										<td class="text-center"><?php echo 'GRN NO'; ?></td>
										<td class="text-center"><?php echo 'Ex Date'; ?></td>
										<td class="text-center"><?php echo 'Batch No'; ?></td>
									</tr>
								</thead>
								<tbody>
									<?php $i = 1; ?>
									<?php foreach ($inwards as $inward) { ?>
									<tr>
			                        	<td class="text-left"><?php echo $inward['date']; ?></td>
			                        	<td class="text-left"><?php echo $inward['supplier']; ?></td>
			                        	<td class="text-right"><?php echo $inward['quantity']; ?></td>
			                        	<td class="text-left"><?php echo $inward['unit']; ?></td>
			                        	<td class="text-right"><?php echo $inward['value']; ?></td>
			                        	<td class="text-left"><?php echo $inward['order_no']; ?></td>
			                        	<td class="text-left"><?php echo $inward['ex_date']; ?></td>
			                        	<td class="text-left"><?php echo $inward['batch_no']; ?></td>
									</tr>
									<?php $i ++; ?>
									<?php } ?>
									<tr>
										<td class="text-center" colspan="2"><b><?php echo 'Total' ?></b></td>
										<td class="text-right" colspan="1"><b><?php echo $grn_qty_total; ?></b></td>
										<td></td>
										<td class="text-right" colspan="1"><b><?php echo $total; ?></b></td>
										<td class="text-center" colspan="3"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				<?php } ?>
				<?php if ($medtransfer) { ?>
					<h3>Medicine Transfer</h3>
					<div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<?php if($is_user == '0'){ ?>
										<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
										<?php } ?>
										
										<td class="text-center"><?php echo 'Date'; ?></td>
										<td class="text-center"><?php echo 'Clinic Name'; ?></td>
										<td class="text-center"><?php echo 'Doctor'; ?></td>
										<td class="text-center"><?php echo 'Quantity'; ?></td>
										<td class="text-center"><?php echo 'Unit'; ?></td>
										<td class="text-center"><?php echo 'Trn No'; ?></td>
									</tr>
									
								</thead>
								<tbody>
									<?php $i = 1; ?>
									<?php foreach ($medtransfer as $value) { ?>
									<tr>
			                        	<td class="text-left"><?php echo $value['entry_date']; ?></td>
			                        	<td class="text-left"><?php echo $value['parent_doctor_name']; ?></td>
			                        	<td class="text-left"><?php echo $value['doctor_name']; ?></td>
			                        	<td class="text-right"><?php echo $value['product_qty']; ?></td>
			                        	<td class="text-left"><?php echo $value['unit']; ?></td>
			                        	<td class="text-left"><?php echo $value['issue_no']; ?></td>
									</tr>
									<?php $i ++; ?>
									<?php } ?>
									<tr>
										<td class="text-center" colspan="3"><b><?php echo 'Total' ?></b></td>
										<td class="text-right" colspan="1"><b><?php echo $qty_total; ?></b></td>
										<td class="text-center" colspan="1"></td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				<?php } ?>
				<?php if ($medicine_trans_datas) { ?>
					<h3>ISSUES</h3>
					<div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<?php if($is_user == '0'){ ?>
										<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
										<?php } ?>
										
										<td class="text-center"><?php echo 'Date'; ?></td>
										<td class="text-center"><?php echo 'Doctor Name'; ?></td>
										<td class="text-center"><?php echo 'Horse Name'; ?></td>
										<td class="text-center"><?php echo 'Total Qty'; ?></td>
										<td class="text-center"><?php echo 'Unit'; ?></td>
										<td class="text-center"><?php echo 'Clinic Name'; ?></td>
										<td class="text-center"><?php echo 'Total Item'; ?></td>
									</tr>
								</thead>
								<tbody>
									<?php $i = 1; ?>
									<?php foreach ($medicine_trans_datas as $medicine_transfer) { ?>
									<tr>
										<?php if($is_user == '0'){ ?>
										<td style="display: none;" class="text-center"><?php if (in_array($medicine_transfer['id'], $selected)) { ?>
											<input type="checkbox" name="selected[]" value="<?php echo $medicine_transfer['id']; ?>" checked="checked" />
											<?php } else { ?>
											<input type="checkbox" name="selected[]" value="<?php echo $medicine_transfer['id']; ?>" />
											<?php } ?>
										</td>
										<?php } ?>
									 
										
										<td class="text-left"><?php echo date('d-m-Y', strtotime($medicine_transfer['entry_date'])); ?></td>
										<td class="text-left"><?php echo $medicine_transfer['doctor_name']; ?></td>
										<td class="text-left"><?php echo $medicine_transfer['horse_name']; ?></td>
										<td  class="text-right"><?php echo $medicine_transfer['total_qty']; ?></td>
										<td  class="text-left"><?php echo $medicine_transfer['unit']; ?></td>
										<td class="text-left"><?php echo $medicine_transfer['clinic_name']; ?></td>
										<td  class="text-right">
											<?php echo $medicine_transfer['total_item']; ?>
										</td>
									</tr>
									<?php $i ++; ?>
									<?php } ?>
									<tr>
										<td class="text-center" colspan="3"><b><?php echo 'Total' ?></b></td>
										<td class="text-right" colspan="1"><b><?php echo $iss_qty_total; ?></b></td>
										<td class="text-center" colspan="3"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				<?php } ?>
	        </div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--

$('#myModal').on('show.bs.modal', function(e) {
	var $modal = $(this),
	esseyId = e.relatedTarget.id;
	idsss = e.relatedTarget.className;
	s_ids = idsss.split(' ');
	idss = s_ids[1];
	s_id = idss.split('-');
	id = s_id[1];
	order_id = $('#order_id-'+id).val();
	//console.log(productfinished_id);
	//return false;
	$.ajax({
		cache: false,
		type: 'POST',
		url: 'index.php?route=catalog/inward/getdata&token=<?php echo $token; ?>&filter_order_id=' +  encodeURIComponent(order_id),
		data: 'filter_order_id=' + order_id,
		success: function(data) {
			//console.log(data.html);
			$modal.find('.edit-content').html(data.html);
		}
	});
})

$('#button-filter').on('click', function() {
  var url = 'index.php?route=report/medicine_report&token=<?php echo $token; ?>';

  var filter_medicine = $('input[name=\'filter_medicine\']').val();
  if (filter_medicine) {
    url += '&filter_medicine=' + encodeURIComponent(filter_medicine);
  }

  var filter_po_no = $('input[name=\'filter_po_no\']').val();
  if (filter_po_no) {
    url += '&filter_po_no=' + encodeURIComponent(filter_po_no);
  }

  var filter_date = $('input[name=\'filter_date\']').val();

  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  
  }

  var filter_dates = $('input[name=\'filter_dates\']').val();

  if (filter_dates) {
    url += '&filter_dates=' + encodeURIComponent(filter_dates);
  
  }

	var filter_productsort = $('select[name=\'filter_productsort\']').val();
	//alert(filter_productsort);

	if (filter_productsort) {
	  url += '&filter_productsort=' + encodeURIComponent(filter_productsort);
	}

  location = url;
});

$('input[name=\'filter_inward\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/inward/autocomplete&token=<?php echo $token; ?>&filter_order_id=' +  encodeURIComponent(request),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['order_id'],
			value: item['order_id']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter_inward\']').val(item['label']);
	$('input[name=\'filter_order_id\']').val(item['value']);
  }
});

$(document).ready(function()               
    {
        // enter keyd
        $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                 $('#button-filter').trigger('click');
             }
        });
    });

$("#button-export").on('click', function() {
	var url = 'index.php?route=report/medicine_report/prints&token=<?php echo $token; ?>';

	var filter_medicine = $('input[name=\'filter_medicine\']').val();
	if (filter_medicine) {
		url += '&filter_medicine=' + encodeURIComponent(filter_medicine);
	}

	var filter_po_no = $('input[name=\'filter_po_no\']').val();
	if (filter_po_no) {
		url += '&filter_po_no=' + encodeURIComponent(filter_po_no);
	}

	var filter_date = $('input[name=\'filter_date\']').val();
	if (filter_date) {
		url += '&filter_date=' + encodeURIComponent(filter_date);
	}

	var filter_dates = $('input[name=\'filter_dates\']').val();
	if (filter_dates) {
		url += '&filter_dates=' + encodeURIComponent(filter_dates);
	}

	var note = $('input[name=\'note\']').val();
	if (note) {
		url += '&note=' + encodeURIComponent(note);
	}	

	location = url;
});

$('input[name=\'filter_medicine\']').autocomplete({
	'source': function(request, response) {
		if(request != ''){
			$.ajax({
				url: 'index.php?route=report/medicine_report/autocomplete&token=<?php echo $token; ?>&filter_medicine=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['med_name'],
						value: item['med_name']
					}
				}));
				}
			});
		}
	},
  'select': function(item) {
	$('input[name=\'filter_medicine\']').val(item['label']);
	$('input[name=\'filter_order_id\']').val(item['value']);
  }
});

$('input[name=\'filter_po_no\']').autocomplete({
	'source': function(request, response) {
		if(request != ''){
			$.ajax({
				url: 'index.php?route=report/medicine_report/autocompletePo&token=<?php echo $token; ?>&filter_po_no=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['po_no'],
						value: item['po_no']
					}
				}));
				}
			});
		}
	},
  'select': function(item) {
	$('input[name=\'filter_po_no\']').val(item['label']);
	$('input[name=\'filter_order_id\']').val(item['value']);
  }
});

//--></script>
<script type="text/javascript">
$( document ).ready(function() {
	var refer = "<?php echo $refer; ?>";
	if(refer == '1') {
	  close();
	}
});

$('#reset_filter').click(function(){
    $('#input-filter_medicine').val('');
    $('#input-filter_date').val(''); 
    $('#input-filter_dates').val(''); 
});
</script>

<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
</script>
<?php echo $footer; ?>