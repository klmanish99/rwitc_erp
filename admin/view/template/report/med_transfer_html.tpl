<?php
/*echo'<pre>';
print_r($indents);
exit;*/
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

</style>
</head>
<body>

<h2 style="text-align: center;" >Medicine Transfer Report</h2>
<?php 
	$timestamp = time(); 
?>
<h4 style="text-align: center">Geneated On: <?php echo(date("d-m-Y h:i A", $timestamp)) ?></h4>
<h4 style="text-align: center;" >From: <?php echo $from_date ?> To: <?php echo $to_date ?></h4>

<table>
	<tr>
		<th>Issue No</th>
		<th>Date</th>
		<th>Clinic</th>
		<th>Doctor</th>
		<th>Medicine</th>
		<th>Type</th>
		<th>Expiry Date</th>
		<th>Quantity</th>
	</tr>
	<?php if ($medtransfer) { ?>
		<?php foreach ($medtransfer as $key => $value) { ?>
			<tr>
			    <td><?php echo $value['issue_no'] ?></td>
			    <td><?php echo $value['entry_date'] ?></td>
			    <td><?php echo $value['clinic'] ?></td>
			    <td><?php echo $value['doctor_name'] ?></td>
			    <td><?php echo $value['product_name'] ?></td>
			    <td><?php echo $value['med_type'] ?></td>
			    <td><?php echo $value['expire_date'] ?></td>
			    <td><?php echo $value['product_qty'] ?> <?php echo $value['unit'] ?></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>

</body>
</html>
