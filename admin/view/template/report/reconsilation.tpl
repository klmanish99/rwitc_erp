<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
  </div>
  <link type="text/css" href="view/stylesheet/myform.css" rel="stylesheet" media="screen" />
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
    <?php } ?>
    <?php if ($error_medicine_error) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_medicine_error; ?>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <?php } ?>
     <?php if ($filter_doctor_id_error) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $filter_doctor_id_error; ?>
        <button type="button" class="close" data-dismiss="alert"></button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="col-sm-12 panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i>Reconsilation</h3>
        <div class="pull-right">
        <!--   <a href="<?php echo $back_monthly_daily_reconsilation; ?>"  class="btn btn-warning" style="border-color:#f33333;bottom: 2px !important;right: 5px !important;width: 161px !important;  "><i class="fa fa-arrow-right"></i>Monthly Reconsilation</a> -->
          <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Go To Dashboard" class="btn btn-primary">Go To Dashboard</a>
        </div>
      </div>
      <div style="margin-top: 5%;" class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
            <div class="well">
              <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="col-sm-3" >
                              <div class="input-group date">
                                  <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="Date Start" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
                                  <span class="input-group-btn">
                                      <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                  </span>
                              </div>
                                
                         </div>
                       <!--  <div class="col-sm-3">
                            <input type="text" name="filter_doctor_name" id="filter_doctor_name" value="<?php echo $filter_doctor_name; ?>" placeholder="Doctor Name" class="form-control">
                            <input type="hidden" name="filter_doctor_id" id="filter_doctor_id" value="<?php echo $filter_doctor_id; ?>"  class="form-control">
                        </div> -->
                         <div class="col-sm-3"><a onclick="filter();"  id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a></div>
                    </div>
                </div>
        
              </div>
            </div>
            <div class="table-responsive">
                    <table class="table table-bordered" id="tbladdMedicineTran">
                        <thead>
                            <tr>
                                <td class="text-center">Sr No</td>
                                <td class="text-center">Entry Date</td>
                                <td class="text-center">Medicine Name</td>
                                <td class="text-center">Unit</td>
                                <td class="text-center">Quantity</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($reconcilation_datas) { ?>
                                <?php $i=1; ?>
                                <?php foreach ($reconcilation_datas as $key => $value) { ?>
                                        <tr>
                                            <td class="text-center"><?php echo $i++; ?></td>
                                            <td class="text-center"><?php echo $value['entry_date']; ?></td>
                                            <td class="text-left"><?php echo $value['medicine_name'] ?></td>
                                            <td class="text-left"><?php echo $value['store_unit']; ?> </td>
                                            <td class="text-right"><?php echo $value['total_trans']; ?></td>
                                        </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <h3 class="text-center noresults" ><?php echo $text_no_results; ?></h3>
                            <?php } ?>
                        </tbody>
                    </table>
            </div>
            <div class="row" >
              <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
              <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    function filter() { 
        url = 'index.php?route=report/reconsilation_report&token=<?php echo $token; ?>';

        var filter_date = $('input[name=\'filter_date\']').val();
        if (filter_date) {
            url += '&filter_date=' + encodeURIComponent(filter_date);
        }

       

        /*var filter_doctor_name = $('input[name=\'filter_doctor_name\']').val();
        if (filter_doctor_name) {
             var filter_doctor_id = $('input[name=\'filter_doctor_id\']').val();
                if (filter_doctor_id) {
                    url += '&filter_doctor_id=' + encodeURIComponent(filter_doctor_id);
                }
            url += '&filter_doctor_name=' + encodeURIComponent(filter_doctor_name);
        }*/
        window.location.href = url;
    }

    $('#filter_doctor_name').autocomplete({
      delay: 500,
      source: function(request, response) {
          if(request != ''){
            $('#filter_doctor_id').val('');
              $.ajax({
                  url: 'index.php?route=report/medicine_transaction/autocompletedoc&token=<?php echo $token; ?>&filter_doctor_name=' +  encodeURIComponent(request.term), dataType: 'json',
                  success: function(json) {   
                      response($.map(json, function(item) {
                          return {
                              label: item.doctor_name,
                              value: item.doctor_name,
                              docter_id: item.id,
                          }
                      }));
                  }
              });
          }
      }, 
      select: function(event, ui) {
            $('#filter_doctor_name').val(ui.item.value);
            $('#filter_doctor_id').val(ui.item.docter_id);
            $('.dropdown-menu').hide();
          return false;
      },
  });

    $('#filterMedicineName').autocomplete({
      delay: 500,
      source: function(request, response) {
          if(request != ''){
              $.ajax({
                  url: 'index.php?route=catalog/medicine/autocomplete&token=<?php echo $token; ?>&filter_medicine_name=' +  encodeURIComponent(request.term), dataType: 'json',
                  success: function(json) {   
                      response($.map(json, function(item) {
                          return {
                              label: item.med_name,
                              value: item.med_name,
                              med_code:item.med_code,
                              store_unit:item.store_unit
                          }
                      }));
                  }
              });
          }
      }, 
      select: function(event, ui) {
          $('#filterMedicineName').val(ui.item.value);
          $('#filterMedicineName').attr('value',ui.item.value);
           $('#filterMedicineCode').val(ui.item.med_code);
          $('.dropdown-menu').hide();
          var extra_field = $('#tbladdMedicineTran >tbody >tr').length;

          var srno = parseInt(extra_field) + 1
          if(ui.item.value){
            html = '';
            html += '<tr id="productraw_row' + srno + '">';
                html += '<td id="input_mt_qty_label'+extra_field+'" class="text-center">'+srno+'<br />';
                html += '</td>';

                html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">' + ui.item.value + '';
                    html += '<input type="hidden" name="productraw_datas['+extra_field+'][product_name]" value="'+ui.item.value+'"  class="form-control po_qty_class" />';
                     html += '<input type="hidden" name="productraw_datas['+extra_field+'][product_id]" value="'+ui.item.med_code+'"  class="form-control po_qty_class" />';
                html += '</td>';

                html += '<td id="input_mt_qty_label'+extra_field+'" class="text-center">' + ui.item.store_unit + '';
                    html += '<input type="hidden" name="productraw_datas['+extra_field+'][store_unit]" value="'+ui.item.store_unit+'"  class="form-control po_qty_class" />';
                html += '</td>';
                html += '<td id="input_mt_qty_label'+extra_field+'" class="text-center allqtyTotals">' + 0 + '</td>';

                html += '<td id="input_mt_qty_label'+extra_field+'" class="text-center allPurchaseTotals">';
                    html += '<input type="number" name="productraw_datas['+extra_field+'][actual_qty]"  class="form-control po_qty_class" />';
                html += '</td>';
                
            html += '</tr>';
                $('#tbladdMedicineTran tbody').append(html);
                $('.noresults').hide();
                $('#filterMedicineName').val('');
                $('#filterMedicineName').attr('');
                $('#filterMedicineCode').val('');
          }
          return false;
      },
  });

</script>
<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
</script>
<?php echo $footer; ?>