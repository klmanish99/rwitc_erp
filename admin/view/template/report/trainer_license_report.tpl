<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <!-- <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div> -->
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <!-- <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="input-requested_by">From Date</label>
                    <label class="col-sm-3 control-label" for="input-requested_by">To Date</label>
                </div>
            </div> -->
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-3" >
                        <div class="input-group date">
                            <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="License Start Date" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3" >
                        <div class="input-group date">
                            <input type="text" name="filter_end_dates" value="<?php echo $filter_end_dates; ?>" placeholder="License End Date" data-date-format="DD-MM-YYYY" id="input-filter_end_dates" class="form-control" />
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                    <a onclick="filter();"  id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
                </div>
            </div>
    
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center">Sr.No</th>
                <th class="text-center">Season</th>
              
                <th class="text-center">Trainer Name</th>
                <th class="text-center">Fees Type</th>
                <th class="text-center">No OF Horses</th>
                <th class="text-center">Amount</th>
                <th class="text-center">License Start Date</th>
                <th class="text-center">License End Date</th>
                <th class="text-center">License Type</th>
                <th class="text-center">Apprenties</th>
                <th class="text-center">Entry Date</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($trainer_license_datas) {  $sr_no = 1;?>
              <?php foreach ($trainer_license_datas as $trainer_license) { ?>
              <tr>
                <td><?php echo $sr_no++;  ?></td>
                <td class="text-left"><?php echo $trainer_license['season']; ?></td>
                <td class="text-left"><?php echo $trainer_license['trainer_name']; ?></td>
                <td class="text-left"><?php echo $trainer_license['fees']; ?></td>
                <td class="text-right"><?php echo $trainer_license['no_horse']; ?></td>
                <td class="text-right"><?php echo $trainer_license['amount']; ?></td>
                <td class="text-right"><?php echo $trainer_license['license_start_date']; ?></td>
                <td class="text-right"><?php echo $trainer_license['license_end_date']; ?></td>
                <td class="text-left"><?php echo $trainer_license['license_type']; ?></td>
                <td class="text-left"><?php echo $trainer_license['apprenties']; ?></td>
                <td class="text-left"><?php echo $trainer_license['entry_date']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="12"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<script type="text/javascript">
    function filter() { 
        url = 'index.php?route=report/trainer_license_report&token=<?php echo $token; ?>';
        
        var filter_date = $('input[name=\'filter_date\']').val();

        if (filter_date) {
            url += '&filter_date=' + encodeURIComponent(filter_date);
          
        }

        var filter_end_dates = $('input[name=\'filter_end_dates\']').val();

        if (filter_end_dates) {
            url += '&filter_end_dates=' + encodeURIComponent(filter_end_dates);
      
        }
        window.location.href = url;
    }

    $('input[name=\'filter_date\']').keydown(function(e) {
      if (e.keyCode == 13) {
        filter();
      }
    });
    $('input[name=\'filter_end_dates\']').keydown(function(e) {
      if (e.keyCode == 13) {
        filter();
      }
    });


     $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

</script>
<?php echo $footer; ?>