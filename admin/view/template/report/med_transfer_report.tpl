<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header" >
		<div class="container-fluid">
			<div class="pull-right">
        		<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Cancel" class="btn btn-default"><i class="fa fa-reply"></i></a>
        	</div>
			<h1>Medicine Transfer Report</h1>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i>Medicine Transfer Report</h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-requested_by">Medicine</label>
								<label class="col-sm-2 control-label" for="input-requested_by">Clinic</label>
								<label class="col-sm-2 control-label" for="input-requested_by">Doctor</label>
								<label class="col-sm-2 control-label" for="input-requested_by">From Date</label>
								<label class="col-sm-2 control-label" for="input-requested_by">To Date</label>
								<div class="col-sm-2">
				                    <button id="reset_filter" class="pull-right btn btn-primary" type="button">Reset</button>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<div class="col-sm-2">
									<input type="text" name="filter_medicine" value="<?php echo $filter_medicine; ?>" placeholder="<?php echo "Medicine"; ?>" id="input-filter_medicine" class="form-control" />
									<input type="hidden" name="hidden_med_id" value="" class="form-control" />
								</div>
								<div class="col-sm-2">
									<select name="filter_hospital" id="input-filter_hospital" class="form-control">
										<option value="0">Please Select</option>
				                        <?php foreach ($hospital as $key => $tvalue) { ?>
				                        	<?php if($filter_hospital == $tvalue['hospitals']){ ?>
                                           		<option value="<?php echo $tvalue['hospitals'] ?>" selected="selected"><?php echo $tvalue['hospitals']?></option>
				                        	<?php } else { ?>
				                            	<option value="<?php echo $tvalue['hospitals'] ?>"><?php echo $tvalue['hospitals'] ?></option>
				                        	<?php } ?>
				                        <?php } ?>    
				                    </select>
				                    <input type="hidden" name="hidden_id" value="" class="form-control" />
								</div>
			                    <div class="col-sm-2">
									<select name="filter_doctor" id="input-filter_doctor" class="form-control">
										<option value="0">Please Select</option>
				                        <?php foreach ($name as $key => $tvalue) { ?>
				                        	<?php if($filter_doctor == $tvalue['id']){ ?>
				                        		<option value="<?php echo $tvalue['id'] ?>" selected="selected"><?php echo $tvalue['names']?></option>
				                        	<?php } else { ?>
				                            	<option value="<?php echo $tvalue['id'] ?>"><?php echo $tvalue['names'] ?></option>
				                            <?php } ?> 
				                        <?php } ?>    
				                    </select>
				                    <input type="hidden" name="hidden_id" value="" class="form-control" />
								</div>
			                    <div class="col-sm-2" >
			                        <div class="input-group date">
			                            <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
			                            <span class="input-group-btn">
			                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="col-sm-2" >
			                        <div class="input-group date">
			                            <input type="text" name="filter_dates" value="<?php echo $filter_dates; ?>" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-filter_dates" class="form-control" />
			                            <span class="input-group-btn">
			                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			                            </span>
			                        </div>
			                    </div>

								<div class="col-sm-1">
									<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
								</div>
								<div class="col-sm-1">
									<button type="button" id="button-export" class="btn btn-primary"><i class="fa fa-download"></i> <?php echo 'Export'; ?></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<?php if($is_user == '0'){ ?>
									<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
									<?php } ?>
									
									<td class="text-left"><?php echo 'Issue No'; ?></td>
									<td class="text-left"><?php echo 'Date'; ?></td>
									<td class="text-left"><?php echo 'Clinic'; ?></td>
									<td class="text-left"><?php echo 'Doctor'; ?></td>
									<td class="text-left"><?php echo 'Medicine'; ?></td>
									<td class="text-left"><?php echo 'Type'; ?></td>
									<td class="text-left"><?php echo 'Expiry Date'; ?></td>
									<td class="text-left"><?php echo 'Quantity'; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php if ($medtransfer) { ?>
									<?php $i = 1; ?>
									<?php foreach ($medtransfer as $value) { ?>
									<tr>
			                        	<td class="text-left"><?php echo $value['issue_no']; ?></td>
			                        	<td class="text-left"><?php echo $value['entry_date']; ?></td>
			                        	<td class="text-left"><?php echo $value['clinic']; ?></td>
			                        	<td class="text-left"><?php echo $value['doctor_name']; ?></td>
			                        	<td class="text-left"><?php echo $value['product_name']; ?></td>
			                        	<td class="text-left"><?php echo $value['med_type']; ?></td>
			                        	<td class="text-left"><?php echo $value['expire_date']; ?></td>
			                        	<td class="text-left"><?php echo $value['product_qty']; ?> <?php echo $value['unit']; ?></td>
									</tr>
									<?php $i ++; ?>
									<?php } ?>
								<?php } else { ?>
								<tr>
									<td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
		        	<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		        	<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		        </div>
	        </div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
</script>
<script type="text/javascript"><!--

$('#button-filter').on('click', function() {
  var url = 'index.php?route=report/med_transfer_report&token=<?php echo $token; ?>';

  var filter_medicine = $('input[name=\'filter_medicine\']').val();
  if (filter_medicine) {
    url += '&filter_medicine=' + encodeURIComponent(filter_medicine);
  }

  var filter_doctor = $('select[name=\'filter_doctor\']').val();
  if (filter_doctor) {
    url += '&filter_doctor=' + encodeURIComponent(filter_doctor);
  }

  var filter_hospital = $('select[name=\'filter_hospital\']').val();
  if (filter_hospital) {
    url += '&filter_hospital=' + encodeURIComponent(filter_hospital);
  }

  var filter_date = $('input[name=\'filter_date\']').val();

  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  
  }

  var filter_dates = $('input[name=\'filter_dates\']').val();

  if (filter_dates) {
    url += '&filter_dates=' + encodeURIComponent(filter_dates);
  
  }

  location = url;
});

$("#button-export").on('click', function() {
	var url = 'index.php?route=report/med_transfer_report/prints&token=<?php echo $token; ?>';

	var filter_medicine = $('input[name=\'hidden_med_id\']').val();
	if (filter_medicine) {
		url += '&filter_medicine=' + encodeURIComponent(filter_medicine);
	}

	var filter_doctor = $('select[name=\'filter_doctor\']').val();
	if (filter_doctor) {
    	url += '&filter_doctor=' + encodeURIComponent(filter_doctor);
	}

	var filter_hospital = $('select[name=\'filter_hospital\']').val();
	if (filter_hospital) {
    	url += '&filter_hospital=' + encodeURIComponent(filter_hospital);
	}

	var filter_date = $('input[name=\'filter_date\']').val();
	if (filter_date) {
		url += '&filter_date=' + encodeURIComponent(filter_date);
	}

	var filter_dates = $('input[name=\'filter_dates\']').val();
	if (filter_dates) {
		url += '&filter_dates=' + encodeURIComponent(filter_dates);
	}

	location = url;
});

$('input[name=\'filter_medicine\']').autocomplete({
	'source': function(request, response) {
		if(request != ''){
			$.ajax({
				url: 'index.php?route=report/stock_report/autocomplete&token=<?php echo $token; ?>&filter_medicine=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['med_name'],
						value: item['id']
					}
				}));
				}
			});
		}
	},
  'select': function(item) {
	$('input[name=\'filter_medicine\']').val(item['label']);
	$('input[name=\'hidden_med_id\']').val(item['value']);
  }
});

$('input[name=\'filter_doctor\']').autocomplete({
	'source': function(request, response) {
		if(request != ''){
			$.ajax({
				url: 'index.php?route=report/med_transfer_report/autocompleteDoctor&token=<?php echo $token; ?>&filter_doctor=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['doctor_name'],
						value: item['id']
					}
				}));
				}
			});
		}
	},
  'select': function(item) {
	$('input[name=\'filter_doctor\']').val(item['label']);
	$('input[name=\'hidden_id\']').val(item['value']);
  }
});

$('#reset_filter').click(function(){
    $('#input-filter_medicine').val('');
    $('#input-filter_hospital').val(''); 
    $('#input-filter_doctor').val('');
    $('#input-filter_date').val(''); 
    $('#input-filter_dates').val(''); 
});

//--></script>

<?php echo $footer; ?>