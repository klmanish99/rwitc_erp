<?php
/*echo'<pre>';
print_r($indents);
exit;*/
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td {
  border: 1px solid #dddddd;
  padding: 8px;
}

th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}

</style>
</head>
<body>

<h2 style="margin-left: 40%;" >Inward Report</h2>
<?php 
	$timestamp = time(); 
?>
<h4 style="text-align: center">Geneated On: <?php echo(date("d-m-Y h:i A", $timestamp)) ?></h4>
<table style="width: 50%;margin-left: 23%;" >
	<tr>
		<th>Inward Code</th>
		<th>Supplier</th>
		<th>Date</th>
	</tr>
	<?php if ($inwards) { ?>
		<?php foreach ($inwards as $key => $value) { ?>
			<tr>
			    <td style="text-align: center;"><?php echo $value['inward_code'] ?></td>
			    <td><?php echo $value['supplier'] ?></td>
			    <td style="text-align: center;"><?php echo $value['date'] ?></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
<br>
<table>
	<tr>
		<th>Medicine Code</th>
		<th>Medicine Name</th>
		<th>Po No</th>
		<th>Po Qty</th>
		<th>Pending Po Qty</th>
		<th>Packing Type</th>
		<th>Quantity</th>
		<th>Convert Qty</th>
		<th>Rate Rs</th>
		<th>Value Rs</th>
		<th>GST Rate %</th>
	 	<th>GST Value Rs</th>
	 	<th>Total Rs</th>
		<th>Expiry Date</th>
		<th>Batch No</th>
	</tr>
	<!-- <?php if ($inwards_item) { ?> -->
		<?php foreach ($inwards_item as $pkey => $pvalue) { ?>
			<tr>
			    <td style="text-align: left;"><?php echo $pvalue['med_code'] ?></td>
			    <td style="text-align: left;"><?php echo $pvalue['med_name'] ?></td>
			    <td style="text-align: left;"><?php echo $pvalue['po_no'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['po_qty'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['reduce_po_qty'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['packing'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['quantity'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['ordered_quantity'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['purchase_price'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['value'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['gst_rate'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['gst_value'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['total'] ?></td>
			    <td><?php echo $pvalue['ex_date'] ?></td>
			    <td style="text-align: right;"><?php echo $pvalue['batch_no'] ?></td>
			</tr>
		<?php } ?>
	<!-- <?php } ?> -->
</table>
<br>
<table style="width: 50%;margin-left: 23%;" >
	<tr>
		<th>Value Total Rs</th>
		<th>GST Value Total Rs</th>
		<th>All Total Rs</th>
	</tr>
	<?php if ($inwards) { ?>
		<?php foreach ($inwards as $key => $value) { ?>
			<tr>
			    <td style="text-align: right;"><?php echo $value['value_total'] ?></td>
			    <td style="text-align: right;"><?php echo $value['gst_value_total'] ?></td>
			    <td style="text-align: right;"><?php echo $value['all_total'] ?></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
<div style="text-align: right;padding-top: 150px;padding-right: 100px;">
	<label><b>Signature</b></label>
</div>
</body>
</html>
