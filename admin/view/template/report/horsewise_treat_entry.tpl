<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header" >
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Go To Dashboard" class="btn btn-primary">Go To Dashboard</a>
        	</div>
			<h1>Horse Treatment</h1>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i>Horse Treatment</h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-requested_by">From Date</label>
								<label class="col-sm-2 control-label" for="input-requested_by">To Date</label>
								<label class="col-sm-2 control-label" for="input-requested_by">Horse</label>
								<label class="col-sm-2 control-label" for="input-requested_by">Trainer</label>
								<label class="col-sm-2 control-label" for="input-requested_by">Medicine</label>
								<div class="col-sm-2">
				                    <button id="reset_filter" class="pull-right btn btn-primary" type="button">Reset</button>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
			                    <div class="col-sm-2" >
			                        <div class="input-group date">
			                            <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="From Date" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
			                            <span class="input-group-btn">
			                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="col-sm-2" >
			                        <div class="input-group date">
			                            <input type="text" name="filter_dates" value="<?php echo $filter_dates; ?>" placeholder="To Date" data-date-format="DD-MM-YYYY" id="input-filter_dates" class="form-control" />
			                            <span class="input-group-btn">
			                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="col-sm-2" >
									<input type="text" name="filter_horse" value="<?php echo $filter_horse; ?>" placeholder="<?php echo "Horse"; ?>" id="input-filter_horse" class="form-control" />
			                    </div>
			                    <div class="col-sm-2" >
									<input type="text" name="filter_trainer" value="<?php echo $filter_trainer; ?>" placeholder="<?php echo "Trainer"; ?>" id="input-filter_trainer" class="form-control" />
			                    </div>
								<div class="col-sm-2">
									<input type="text" name="filter_medicine" value="<?php echo $filter_medicine; ?>" placeholder="<?php echo "Medicine"; ?>" id="input-filter_medicine" class="form-control" />
								</div>
								<div class="col-sm-1">
									<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
								</div>
								<div class="col-sm-1">
									<button type="button" id="button-export" class="btn btn-primary"><i class="fa fa-download"></i> <?php echo 'Export'; ?></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="table-responsive">
						<?php if ($horsetreatments) { ?>
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
										<td class="text-center"><?php echo 'Sr No'; ?></td>
										<td class="text-center"><?php echo 'Entry Date'; ?></td>
										<td class="text-center"><?php echo 'Horse'; ?></td>
										<td class="text-center"><?php echo 'Trainer'; ?></td>
										<td class="text-center"><?php echo 'Medicine Name'; ?></td>
										<td class="text-center"><?php echo 'Type'; ?></td>
										<td class="text-center"><?php echo 'Unit'; ?></td>
										<td class="text-center"><?php echo 'Medicine Qty'; ?></td>
									</tr>
								</thead>
								<tbody>
										<?php $i = 1; ?>
										<?php foreach ($horsetreatments as $horsetreatment) { ?>
										<tr>
				                        	<td class="text-center"><?php echo $i; ?></td>
				                        	<td class="text-center"><?php echo $horsetreatment['entry_date']; ?></td>
				                        	<td class="text-left"><?php echo $horsetreatment['horse_name']; ?></td>
				                        	<td class="text-left"><?php echo $horsetreatment['trainer_name']; ?></td>
				                        	<td class="text-left"><?php echo $horsetreatment['medicine_name']; ?></td>
				                        	<td class="text-left"><?php echo $horsetreatment['med_type']; ?></td>
				                        	<td class="text-left"><?php echo $horsetreatment['unit']; ?></td>
				                        	<td class="text-right"><?php echo $horsetreatment['medicine_qty']; ?></td>
										</tr>
										<?php $i ++; ?>
										<?php } ?>
								</tbody>
							</table>
						<?php } ?>
					</div>
				</div>
	        </div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--

$('#button-filter').on('click', function() {
  var url = 'index.php?route=report/horsewise_treat_entry&token=<?php echo $token; ?>';

  var filter_horse = $('input[name=\'filter_horse\']').val();
  if (filter_horse) {
    url += '&filter_horse=' + encodeURIComponent(filter_horse);
  }

  var filter_trainer = $('input[name=\'filter_trainer\']').val();
  if (filter_trainer) {
    url += '&filter_trainer=' + encodeURIComponent(filter_trainer);
  }

  var filter_medicine = $('input[name=\'filter_medicine\']').val();
  if (filter_medicine) {
    url += '&filter_medicine=' + encodeURIComponent(filter_medicine);
  }

  var filter_date = $('input[name=\'filter_date\']').val();

  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  
  }

  var filter_dates = $('input[name=\'filter_dates\']').val();

  if (filter_dates) {
    url += '&filter_dates=' + encodeURIComponent(filter_dates);
  
  }


  location = url;
});

$("#button-export").on('click', function() {
	var url = 'index.php?route=report/horsewise_treat_entry/prints&token=<?php echo $token; ?>';

	var filter_horse = $('input[name=\'filter_horse\']').val();
	if (filter_horse) {
		url += '&filter_horse=' + encodeURIComponent(filter_horse);
	}

	var filter_trainer = $('input[name=\'filter_trainer\']').val();
	if (filter_trainer) {
	    url += '&filter_trainer=' + encodeURIComponent(filter_trainer);
	}

  var filter_medicine = $('input[name=\'filter_medicine\']').val();
  if (filter_medicine) {
    url += '&filter_medicine=' + encodeURIComponent(filter_medicine);
  }

  var filter_date = $('input[name=\'filter_date\']').val();

  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  
  }

  var filter_dates = $('input[name=\'filter_dates\']').val();

  if (filter_dates) {
    url += '&filter_dates=' + encodeURIComponent(filter_dates);
  
  }

	location = url;
});

$('#input-filter_horse').autocomplete({
delay: 500,
source: function(request, response) {
    if(request != ''){
        $.ajax({
            url: 'index.php?route=report/horsewise_treat_entry/autocompleteHorse&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
            dataType: 'json',
            success: function(json) {   
                $('#trainer_codes_id').find('option').remove();
                response($.map(json, function(item) {
                    return {
                        label: item.trainer_name,
                        value: item.trainer_name,
                        trainer_id:item.trainer_id,
                    }
                }));
            }
        });
    }
}, 
select: function(item) {
    $('#input-filter_horse').val(item.value);
    $('.dropdown-menu').hide();
    return false;
},
});

$('#input-filter_trainer').autocomplete({
    delay: 500,
    source: function(request, response) {
        $('#trainer_id').val('');
        if(request != ''){
            $.ajax({
                url: 'index.php?route=report/horsewise_treat_entry/autocompleteTrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {   
                    $('#trainer_codes_id').find('option').remove();
                    response($.map(json, function(item) {
                        return {
                            label: item.trainer_name,
                            value: item.trainer_name,
                            trainer_id:item.trainer_id,
                            trainer_codes:item.trainer_code
                        }
                    }));
                }
            });
        }
    }, 
    select: function(item) {
        console.log(item);
        $('#input-filter_trainer').val(item.value);
        $('.dropdown-menu').hide();
        return false;
    },
});

$('input[name=\'filter_medicine\']').autocomplete({
	'source': function(request, response) {
		if(request != ''){
			$.ajax({
				url: 'index.php?route=report/horsewise_treat_entry/autocomplete&token=<?php echo $token; ?>&filter_medicine=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['med_name'],
						value: item['med_name']
					}
				}));
				}
			});
		}
	},
  'select': function(item) {
	$('input[name=\'filter_medicine\']').val(item['label']);
	$('input[name=\'filter_order_id\']').val(item['value']);
  }
});

$('#reset_filter').click(function(){
    $('#input-filter_date').val(''); 
    $('#input-filter_dates').val(''); 
    $('#input-filter_horse').val('');
    $('#input-filter_trainer').val('');
    $('#input-filter_medicine').val('');
});
</script>

<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
</script>
<?php echo $footer; ?>