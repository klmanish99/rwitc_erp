<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
  </div>
  <link type="text/css" href="view/stylesheet/myform.css" rel="stylesheet" media="screen" />
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
    <?php } ?>
    <?php if ($error_medicine_error) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_medicine_error; ?>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i>Monthly Daily Reconsilation</h3>
        <a href="<?php echo $back_daily_reconsilation; ?>"  class="btn btn-warning" style="border-color:#f33333;position: absolute !important;bottom: 2px !important;right: 5px !important;width: 161px !important;  "><i class="fa fa-reply"></i>Daily Reconsilation </a>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
            <div class="well">
              <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="col-sm-3" >
                            <select name="filter_months" id="filter_months" placeholder = "Months" class="form-control"> 
                                <option value="" selected="true" disabled="disabled">Month</option>
                                  <?php for($i=01; $i<=12; $i++) {  $cr_date =  '01-'.$i.'-2010';?>
                                    <?php if($filter_months == $i) { ?>
                                      <option value="<?php echo $i ?>" selected="selected"><?php echo $cr_month = date('F', strtotime($cr_date)); ?></option>
                                    <?php } else { ?>
                                      <option value="<?php echo $i ?>"><?php echo $cr_month = date('F', strtotime($cr_date)); ?></option>
                                    <?php } ?>
                                  <?php } ?>
                            </select>
                         </div>
                        <div class="col-sm-3" >
                            <select name="filter_year" id="filter_year" placeholder = "Year" class="form-control"> 
                                <option value="" selected="true" disabled="disabled">Year</option>
                                  <?php for($i=2018; $i<=date('Y'); $i++) { ?>
                                    <?php if($filter_year == $i) { ?>
                                      <option value="<?php echo $i ?>" selected="selected"><?php echo $i; ?></option>
                                    <?php } else { ?>
                                      <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                  <?php } ?>
                            </select>
                         </div>

                         <div class="col-sm-1"><a onclick="filter();"  id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a></div>
                         <div class="col-sm-4">
                            <input type="text" name="filterMedicineName" id="filterMedicineName" value="" placeholder="Medicine Name" class="form-control">
                            <input type="hidden" name="filterMedicineCode" id="filterMedicineCode" value="" placeholder="Medicine Name" class="form-control">
                             <?php if (isset($valierr_clinic)) { ?><span class="errors"><?php echo $valierr_clinic; ?></span><?php } ?>
                          </div>
                           <button onclick="confirm('<?php echo 'Do You want to Save the Changes'; ?>') ? $('#form-manufacturer').submit() : false;" type="button" form="form-manufacturer"  class="btn btn-primary">Save</button>
                    </div>
                </div>
        
              </div>
            </div>
            <div class="table-responsive">
                    <table class="table table-bordered" id="tbladdMedicineTran">
                        <thead>
                            <tr>
                                <td class="text-left">Sr No</td>
                                <td class="text-center">Medicine Name</td>
                                <td class="text-center">Unit</td>
                                <td class="text-center">Quantity</td>
                                <td class="text-center">Actual Qty</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($med_transaction) { ?>
                                <?php $i=1; ?>
                                <?php foreach ($med_transaction as $key => $value) { ?>
                                        <tr>
                                            <td class="text-left"><?php echo $i++; ?></td>
                                            <td class="text-left"><?php echo $value['product_name'] ?>
                                                <input type="hidden" name="productraw_datas[<?php echo $key ?>][product_name]" value="<?php echo $value['product_name'] ?>"  class="form-control po_qty_class" />
                                                <input type="hidden" name="productraw_datas[<?php echo $key ?>][product_id]" value="<?php echo $value['product_id'] ?>"  class="form-control po_qty_class" />
                                            </td>
                                            <td class="text-left"><?php echo $value['store_unit']; ?>
                                                <input type="hidden" name="productraw_datas[<?php echo $key ?>][store_unit]" value="<?php echo $value['store_unit'] ?>"  class="form-control po_qty_class" />
                                            </td>
                                            <td class="text-right"><?php echo $value['total_trans']; ?>
                                                <input type="hidden" name="productraw_datas[<?php echo $key ?>][product_id]" value="<?php echo $value['total_trans'] ?>"  class="form-control po_qty_class" />
                                            </td>
                                            
                                            <td class="col-sm-3">
                                                <input type="number" name="productraw_datas[<?php echo $key ?>][actual_qty]"   class="form-control po_qty_class" />
                                            </td>
                                        </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <h3 class="text-center noresults" ><?php echo $text_no_results; ?></h3>
                            <?php } ?>
                        </tbody>
                    </table>
            </div>
            <div class="row" style="display: none;">
              <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
              <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    function filter() { 
        url = 'index.php?route=report/monthly_medicine_transaction&token=<?php echo $token; ?>';

        var filter_months = $('select[name=\'filter_months\']').val();
       
        
        if (filter_months) {
            url += '&filter_months=' + encodeURIComponent(filter_months);
        }

        var filter_year = $('select[name=\'filter_year\']').val();
        if (filter_year) {
            url += '&filter_year=' + encodeURIComponent(filter_year);
        }
        window.location.href = url;
    }

    $('#filterMedicineName').autocomplete({
      delay: 500,
      source: function(request, response) {
          if(request != ''){
              $.ajax({
                  url: 'index.php?route=catalog/medicine/autocomplete&token=<?php echo $token; ?>&filter_medicine_name=' +  encodeURIComponent(request.term), dataType: 'json',
                  success: function(json) {   
                      response($.map(json, function(item) {
                          return {
                              label: item.med_name,
                              value: item.med_name,
                              med_code:item.med_code,
                              store_unit:item.store_unit
                          }
                      }));
                  }
              });
          }
      }, 
      select: function(event, ui) {
          $('#filterMedicineName').val(ui.item.value);
          $('#filterMedicineName').attr('value',ui.item.value);
           $('#filterMedicineCode').val(ui.item.med_code);
          $('.dropdown-menu').hide();
          var extra_field = $('#tbladdMedicineTran >tbody >tr').length;

          var srno = parseInt(extra_field) + 1
          if(ui.item.value){
            html = '';
            html += '<tr id="productraw_row' + srno + '">';
                html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">'+srno+'<br />';
                html += '</td>';

                html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">' + ui.item.value + '';
                    html += '<input type="hidden" name="productraw_datas['+extra_field+'][product_name]" value="'+ui.item.value+'"  class="form-control po_qty_class" />';
                     html += '<input type="hidden" name="productraw_datas['+extra_field+'][product_id]" value="'+ui.item.med_code+'"  class="form-control po_qty_class" />';
                html += '</td>';

                html += '<td id="input_mt_qty_label'+extra_field+'" class="text-left">' + ui.item.store_unit + '';
                    html += '<input type="hidden" name="productraw_datas['+extra_field+'][store_unit]" value="'+ui.item.store_unit+'"  class="form-control po_qty_class" />';
                html += '</td>';
                html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allqtyTotals">' + 0 + '</td>';

                html += '<td id="input_mt_qty_label'+extra_field+'" class="text-right allPurchaseTotals">';
                    html += '<input type="number" name="productraw_datas['+extra_field+'][actual_qty]"  class="form-control po_qty_class" />';
                html += '</td>';
                
            html += '</tr>';
                $('#tbladdMedicineTran tbody').append(html);
                $('.noresults').hide();
                $('#filterMedicineName').val('');
                $('#filterMedicineName').attr('');
                $('#filterMedicineCode').val('');
          }
          return false;
      },
  });

</script>
<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
</script>
<?php echo $footer; ?>