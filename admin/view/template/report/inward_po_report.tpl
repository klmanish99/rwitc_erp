<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header" >
		<div class="container-fluid">
			<div class="pull-right">
        		<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Cancel" class="btn btn-default"><i class="fa fa-reply"></i></a>
        	</div>
			<h1>Inward Report</h1>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i>Inward Report</h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="input-requested_by">Medicine</label>
								<label class="col-sm-2 control-label" for="input-requested_by">From Date</label>
								<label class="col-sm-2 control-label" for="input-requested_by">To Date</label>
								<label class="col-sm-3 control-label" for="input-requested_by">Po Number</label>
								<div class="col-sm-2">
				                    <button id="reset_filter" class="pull-right btn btn-primary" type="button">Reset</button>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<div class="col-sm-3">
									<input type="text" name="filter_medicine" value="<?php echo $filter_medicine; ?>" placeholder="<?php echo "Medicine"; ?>" id="input-filter_medicine" class="form-control" />
								</div>
			                    <div class="col-sm-2" >
			                        <div class="input-group date">
			                            <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
			                            <span class="input-group-btn">
			                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="col-sm-2" >
			                        <div class="input-group date">
			                            <input type="text" name="filter_dates" value="<?php echo $filter_dates; ?>" placeholder="Date" data-date-format="DD-MM-YYYY" id="input-filter_dates" class="form-control" />
			                            <span class="input-group-btn">
			                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="col-sm-3">
									<input type="text" name="filter_po_no" value="<?php echo $filter_po_no; ?>" placeholder="<?php echo "Po Number"; ?>" id="input-filter_po_no" class="form-control" />
								</div>
								<div class="col-sm-1">
									<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
								</div>
								<div class="col-sm-1">
									<button type="button" id="button-export" class="btn btn-primary"><i class="fa fa-download"></i> <?php echo 'Export'; ?></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<?php if($is_user == '0'){ ?>
									<td style = "display:none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
									<?php } ?>
									
									<td class="text-left"><?php echo 'Inward Code:'; ?></td>
									<td class="text-left"><?php echo 'Date'; ?></td>
									<td class="text-left"><?php echo 'Po No'; ?></td>
									<td class="text-left"><?php echo 'Medicine:'; ?></td>
									<td class="text-left"><?php echo 'Po Quantity'; ?></td>
									<td class="text-left"><?php echo 'Quantity Inward'; ?></td>
									<td class="text-left"><?php echo 'GST rate'; ?></td>
									<td class="text-left"><?php echo 'GST Value Rs'; ?></td>
									<td class="text-left"><?php echo 'Indent'; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php if ($inwards) { ?>
									<?php $i = 1; ?>
									<?php foreach ($inwards as $inward) { ?>
									<tr>
			                        	<td class="text-left"><?php echo $inward['inward_code']; ?></td>
			                        	<td class="text-left"><?php echo $inward['date']; ?></td>
			                        	<td class="text-left"><?php echo $inward['po_no']; ?></td>
			                        	<td class="text-left"><?php echo $inward['productraw_name']; ?></td>
			                        	<td class="text-left"><?php echo $inward['po_qty']; ?></td>
			                        	<td class="text-left"><?php echo $inward['qty']; ?></td>
			                        	<td class="text-left"><?php echo $inward['gst_rate']; ?></td>
			                        	<td class="text-left"><?php echo $inward['gst_value']; ?></td>
			                        	<td class="text-left"><?php echo $inward['indent']; ?></td>
									</tr>
									<?php $i ++; ?>
									<?php } ?>
								<?php } else { ?>
								<tr>
									<td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
		        	<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		        	<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		        </div>
	        </div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--

$('#myModal').on('show.bs.modal', function(e) {
	var $modal = $(this),
	esseyId = e.relatedTarget.id;
	idsss = e.relatedTarget.className;
	s_ids = idsss.split(' ');
	idss = s_ids[1];
	s_id = idss.split('-');
	id = s_id[1];
	order_id = $('#order_id-'+id).val();
	//console.log(productfinished_id);
	//return false;
	$.ajax({
		cache: false,
		type: 'POST',
		url: 'index.php?route=catalog/inward/getdata&token=<?php echo $token; ?>&filter_order_id=' +  encodeURIComponent(order_id),
		data: 'filter_order_id=' + order_id,
		success: function(data) {
			//console.log(data.html);
			$modal.find('.edit-content').html(data.html);
		}
	});
})

$('#button-filter').on('click', function() {
  var url = 'index.php?route=report/inward_po_report&token=<?php echo $token; ?>';

  var filter_inward = $('input[name=\'filter_inward\']').val();
 
  if (filter_inward) {
	var filter_order_id = $('input[name=\'filter_order_id\']').val();
	if (filter_order_id) {
	  url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}
	url += '&filter_inward=' + encodeURIComponent(filter_inward);
  
  }

  var filter_order_no = $('input[name=\'filter_order_no\']').val();
  if (filter_order_no) {
    url += '&filter_order_no=' + encodeURIComponent(filter_order_no);
  }

  var filter_medicine = $('input[name=\'filter_medicine\']').val();
  if (filter_medicine) {
    url += '&filter_medicine=' + encodeURIComponent(filter_medicine);
  }

  var filter_po_no = $('input[name=\'filter_po_no\']').val();
  if (filter_po_no) {
    url += '&filter_po_no=' + encodeURIComponent(filter_po_no);
  }

  var filter_date = $('input[name=\'filter_date\']').val();

  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  
  }

  var filter_dates = $('input[name=\'filter_dates\']').val();

  if (filter_dates) {
    url += '&filter_dates=' + encodeURIComponent(filter_dates);
  
  }

	var filter_productsort = $('select[name=\'filter_productsort\']').val();
	//alert(filter_productsort);

	if (filter_productsort) {
	  url += '&filter_productsort=' + encodeURIComponent(filter_productsort);
	}

  location = url;
});

$('input[name=\'filter_inward\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/inward/autocomplete&token=<?php echo $token; ?>&filter_order_id=' +  encodeURIComponent(request),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['order_id'],
			value: item['order_id']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter_inward\']').val(item['label']);
	$('input[name=\'filter_order_id\']').val(item['value']);
  }
});

$(document).ready(function()               
    {
        // enter keyd
        $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                 $('#button-filter').trigger('click');
             }
        });
    });

$("#button-export").on('click', function() {
	var url = 'index.php?route=report/inward_po_report/prints&token=<?php echo $token; ?>';

	var filter_medicine = $('input[name=\'filter_medicine\']').val();
	if (filter_medicine) {
		url += '&filter_medicine=' + encodeURIComponent(filter_medicine);
	}

	var filter_po_no = $('input[name=\'filter_po_no\']').val();
	if (filter_po_no) {
		url += '&filter_po_no=' + encodeURIComponent(filter_po_no);
	}

	var filter_date = $('input[name=\'filter_date\']').val();
	if (filter_date) {
		url += '&filter_date=' + encodeURIComponent(filter_date);
	}

	var filter_dates = $('input[name=\'filter_dates\']').val();
	if (filter_dates) {
		url += '&filter_dates=' + encodeURIComponent(filter_dates);
	}

	location = url;
});

$('input[name=\'filter_medicine\']').autocomplete({
	'source': function(request, response) {
		if(request != ''){
			$.ajax({
				url: 'index.php?route=report/stock_report/autocomplete&token=<?php echo $token; ?>&filter_medicine=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['med_name'],
						value: item['med_name']
					}
				}));
				}
			});
		}
	},
  'select': function(item) {
	$('input[name=\'filter_medicine\']').val(item['label']);
	$('input[name=\'filter_order_id\']').val(item['value']);
  }
});

$('input[name=\'filter_po_no\']').autocomplete({
	'source': function(request, response) {
		if(request != ''){
			$.ajax({
				url: 'index.php?route=report/stock_report/autocompletePo&token=<?php echo $token; ?>&filter_po_no=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['po_no'],
						value: item['po_no']
					}
				}));
				}
			});
		}
	},
  'select': function(item) {
	$('input[name=\'filter_po_no\']').val(item['label']);
	$('input[name=\'filter_order_id\']').val(item['value']);
  }
});

//--></script>
<script type="text/javascript">
$( document ).ready(function() {
	var refer = "<?php echo $refer; ?>";
	if(refer == '1') {
	  close();
	}
});

$('#reset_filter').click(function(){
    $('#input-filter_medicine').val(''); 
    $('#input-filter_date').val(''); 
    $('#input-filter_dates').val(''); 
    $('#input-filter_po_no').val('');    
});

</script>

<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
</script>
<?php echo $footer; ?>