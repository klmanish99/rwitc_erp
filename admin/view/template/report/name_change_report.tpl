<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <!-- <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div> -->
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-3">
                        <input type="text" name="filterHorseName" id="filterHorseName" value="<?php echo $filter_hourse_name; ?>" placeholder="Horse Name" class="form-control">
                        <input type="hidden" name="filterHorseId" id="filterHorseId" value="<?php echo $filterHorseId; ?>"  class="form-control">
                    </div>
                    <div class="col-sm-3" style="display: none;">
                        <input type="text" name="owner_name"  placeholder="<?php echo "Owner Name"; ?>" value="<?php echo $filter_owner_name; ?>" placeholder="Trainer Name" id="input_owner_name" class="form-control" />
                        <input type="hidden" name="owner_id"  placeholder="<?php echo "Owner Name"; ?>" value="<?php echo $filter_owner_id; ?>" id="owner_id" class="form-control" />
                    </div>
                    <div class="col-sm-3" >
                          <div class="input-group date">
                              <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="Date Start" data-date-format="DD-MM-YYYY" id="input-filter_date" class="form-control" />
                              <span class="input-group-btn">
                                  <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                              </span>
                          </div>
                    </div>
                  <div class="col-sm-3" >
                      <div class="input-group date">
                          <input type="text" name="filter_dates" value="<?php echo $filter_date_end; ?>" placeholder="Date End" data-date-format="DD-MM-YYYY" id="input-filter_date_end" class="form-control" />
                          <span class="input-group-btn">
                              <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span>
                      </div>
                  </div>
                    <a onclick="filter();"  id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
                </div>
            </div>
    
          </div>
        </div>
        <div class="table-responsive">
            <?php if ($final_arrival_charges) { $i=1;?>
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <td class="text-left" style="color: #1e91cf;"><?php echo 'Sr No'; ?></td>
                        <td class="text-center" style="color: #1e91cf;"><?php echo $column_horse_name ?></td>
                        <td class="text-center" style="color: #1e91cf;"><?php echo $column_registeration_type ?></td>
                        <td class="text-center" style="color: #1e91cf;"><?php echo $column_regierstion_fee ?></td>
                        <td class="text-center" style="color: #1e91cf;"><?php echo $column_regierstion_date ?></td>
                        <td class="text-center" style="color: #1e91cf;"><?php echo $column_reason ?></td>
                      </tr>
                    </thead>
                    <?php foreach ($final_arrival_charges as $final_key =>  $resultsvalue) { //echo "<pre>";print_r($resultsvalue['horse_name']);exit;?>
                        <thead>
                          <tr>
                            <?php   $reg_date = ($resultsvalue['registration_date'] == '1970-01-01' || $resultsvalue['registration_date'] == '0000-00-00') ? "" :  date('d-m-Y', strtotime($resultsvalue['registration_date']));
                              $modify = ($resultsvalue['modification'] == '1') ? "(modification)" :  "";

                             ?>  
                            <td class="text-left" ><?php { echo $i++; } ?></td>
                            <td class="text-left"><?php  echo $resultsvalue['horse_name']; ?></td>
                            <td class="text-left"><?php  echo $resultsvalue['registeration_type'].' '.$modify; ?></td>
                            <td class="text-right"><?php echo $resultsvalue['registeration_fee']; ?></td>
                            <td class="text-right"><?php  echo $reg_date; ?></td>
                            <td class="text-left"><?php  echo $resultsvalue['remark']; ?></td> 
                          </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($resultsvalue['owners_datas'])) { ?>
                                <?php foreach ($resultsvalue['owners_datas'] as $ownerdata) {//echo "<pre>";print_r($ownerdatas);exit; ?>
                                    
                                  <tr>
                                    <td class="text-left"></td>
                                    <td class="text-left"><?php echo $ownerdata['owner_name']; ?></td>
                                  
                                     <?php 
                                        $provisional_ownership  = ($ownerdata['provisional_ownership'] == 'Yes') ? 'Provisional' : "";

                                        $from_date = date('d-m-Y', strtotime($ownerdata['date_of_ownership']));
                                        $to_date = date('d-m-Y', strtotime($ownerdata['end_date_of_ownership'])); ?>

                                        <?php if($ownerdata['ownership_type'] == 'Lease'){ ?>
                                            <td class="text-left"><?php echo $provisional_ownership.' L'; ?></td>
                                        <?php } elseif($ownerdata['ownership_type'] == 'Sub Lease'){ ?>
                                            <td class="text-left"><?php echo $provisional_ownership.' SL'; ?></td>
                                         <?php } else { ?> 
                                             <td class="text-left"><?php echo $provisional_ownership.' O'; ?></td>
                                        <?php } ?> 

                                        <?php if($ownerdata['ownership_type'] == 'Lease'){ ?>
                                            <td class="text-right"> <?php echo $from_date. ' - ' .$to_date; ?></td>
                                        <?php } elseif($ownerdata['ownership_type'] == 'Sub Lease'){ ?>
                                            <td class="text-right" ><?php echo $from_date. ' - ' .$to_date; ?></td>
                                         <?php } else { ?> 
                                             <td class="text-right"><?php echo $from_date ?></td>
                                        <?php } ?>  
                                            <td class="text-right" ><?php echo $ownerdata['owner_sharss'].'%'; ?></td>
                                   
                                        <td class="text-right" colspan="2">Rs: <?php echo number_format($ownerdata['charge_amount'],2) ; ?></td>
                                  </tr>
                              <?php } ?>
                          <?php } ?>
                        </tbody>
                    <?php } ?>
                </table>
            <?php } else { ?>
            <h3 class="text-center" style="font-weight: bold"><?php echo $text_no_results; ?></h3>
            <?php } ?>
        </div>
        <div class="row" style="display: none;">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<script type="text/javascript">
    function filter() { 
        var filter_hourse_name = $('#filterHorseName').val();
        var filter_hourse_id = $('#filterHorseId').val();
        var filter_trainer_name = $('#input-trainer_name').val();
        var filter_trainer_id = $('#trainer_id').val();
        url = 'index.php?route=report/name_change_report&token=<?php echo $token; ?>';
        if (filter_hourse_name) {
          url += '&filter_hourse_name=' + encodeURIComponent(filter_hourse_name);
           if (filter_hourse_id) {
                url += '&filter_hourse_id=' + encodeURIComponent(filter_hourse_id);
            }
        }
       

        

        var filter_owner_name = $('#input_owner_name').val();

       if (filter_owner_name) {
             url += '&filter_owner_name=' + encodeURIComponent(filter_owner_name);
            var filter_owner_id = $('#owner_id').val();
             if (filter_owner_id) {
                url += '&filter_owner_id=' + encodeURIComponent(filter_owner_id);
            }
        }

        var filter_date = $('input[name=\'filter_date\']').val();
        if (filter_date) {
            url += '&filter_date=' + encodeURIComponent(filter_date);
        }

        var filter_date_end = $('input[name=\'filter_dates\']').val();

        if (filter_date_end) {
            url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
        }

        window.location.href = url;
      
    }

    $('#filterHorseName').autocomplete({
    delay: 500,
    source: function(request, response) {
        $('#filterHorseId').val('');
        if(request != ''){
            $.ajax({
                url: 'index.php?route=report/report_horse_undertaking_charge/autocompleteHorse&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {   
                    response($.map(json, function(item) {
                        return {
                            label: item.horse_name,
                            value: item.horse_name,
                            horse_id:item.horse_id,
                        }
                    }));
                }
            });
        }
    }, 
    select: function(item) {
        console.log(item);
        $('#filterHorseName').val(item.value);
        $('#filterHorseId').val(item.horse_id);
        $('.dropdown-menu').hide();
        return false;
    },
    });
     $('#input-trainer_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#trainer_id').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=catalog/horse/autocompleteTrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request),
                        dataType: 'json',
                        success: function(json) {   
                            $('#trainer_codes_id').find('option').remove();
                            response($.map(json, function(item) {
                                return {
                                    label: item.trainer_name,
                                    value: item.trainer_name,
                                    trainer_id:item.trainer_id,
                                    trainer_codes:item.trainer_code
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(item) {
                console.log(item);
                $('#input-trainer_name').val(item.value);
                $('#trainer_id').val(item.trainer_id);
                $('.dropdown-menu').hide();
                $('#date_charge_trainer').focus();
                return false;
            },
        });
    $('#input_owner_name').autocomplete({
    delay: 500,
    source: function(request, response) {
        $('#towner_id').val('');
        if(request != ''){
            $.ajax({
                url: 'index.php?route=report/name_change_report/autocompleteOwner&token=<?php echo $token; ?>&owner_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {   
                    response($.map(json, function(item) {
                        return {
                            label: item.owner_name,
                            value: item.owner_name,
                            owner_id:item.owner_id,
                        }
                    }));
                }
            });
        }
    }, 
    select: function(item) {
        console.log(item);
        $('#input_owner_name').val(item.value);
        $('#owner_id').val(item.owner_id);
        $('.dropdown-menu').hide();
        filter();
        return false;
    },
  });
    $('#filterHorseName').keydown(function(e) {
      if (e.keyCode == 13) {
        filter();
      }
    });
    $('#input-trainer_name').keydown(function(e) {
      if (e.keyCode == 13) {
        filter();
      }
    });
</script>
<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
</script>
<?php echo $footer; ?>