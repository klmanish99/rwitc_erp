<?php
/*echo'<pre>';
print_r($indents);
exit;*/
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td {
  border: 1px solid #dddddd;
  padding: 8px;
}

th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}

</style>
</head>
<body>
	<h2 style="margin-left: 40%;" >Medicine Transfer Report</h2>
	<?php 
		$timestamp = time(); 
	?>
	<h4 style="text-align: center">Geneated On: <?php echo(date("d-m-Y h:i A", $timestamp)) ?></h4>
	<table style="width: 50%;margin-left: 23%;" >
		<tr>
			<th>Issue No</th>
			<th>Clinic</th>
			<th>Doctor</th>
			<th>Date</th>
		</tr>
		<?php if ($medicine_transfers) { ?>
			<?php foreach ($medicine_transfers as $key => $value) { ?>
				<tr>
				    <td style="text-align: center;"><?php echo $value['issue_no'] ?></td>
				    <td style="text-align: center;"><?php echo $value['clinic'] ?></td>
				    <td style="text-align: center;"><?php echo $doctor_name ?></td>
				    <td style="text-align: center;"><?php echo $value['entry_date'] ?></td>
				</tr>
			<?php } ?>
		<?php } ?>
	</table>
	<br>
	<table>
		<tr>
			<th>Medicine Code</th>
			<th>Medicine Name</th>
			<th>Medicine Type</th>
			<th>Expire Date</th>
			<th>Quantity</th>
			<th>Unit</th>
		</tr>
			<?php foreach ($medicine_transfer_item as $mkey => $mvalue) { ?>
				<tr>
				    <td style="text-align: center;"><?php echo $mvalue['med_code'] ?></td>
				    <td style="text-align: center;"><?php echo $mvalue['med_name'] ?></td>
				    <td style="text-align: center;"><?php echo $mvalue['medicine_type'] ?></td>
				    <td style="text-align: center;"><?php echo $mvalue['expire_date'] ?></td>
				    <td style="text-align: center;"><?php echo $mvalue['product_qty'] ?></td>
				    <td style="text-align: center;"><?php echo $mvalue['medicine_unit'] ?></td>
				</tr>
			<?php } ?>
	</table>
</body>
</html>
