<?php
/*echo'<pre>';
print_r($indents);
exit;*/
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 5px;
}

td {
  border: 1px solid #dddddd;
  padding: 5px;
}

</style>
</head>
<body>

<h2 style="text-align: center">Indent Report</h2>
<?php 
	$timestamp = time(); 
?>
<h4 style="text-align: center">Geneated On: <?php echo(date("d-m-Y h:i A", $timestamp)) ?></h4>
<?php if ($from_date && $to_date) { ?>
	<h4 style="text-align: center">From: <?php echo $from_date ?> To: <?php echo $to_date ?></h4>
<?php } ?>

<table>
	<tr>
		<th>Requested By</th>
		<th>Indent Code</th>
		<th>Date</th>
		<th>Medicine</th>
		<th>Volume</th>
		<th>Packing Type</th>
		<th>Quantity</th>
		<th>Converted Qty</th>
		<th>Rate Rs</th>
		<th>Value Rs</th>
		<th>GST Rate</th>
		<th>GST Value Rs</th>
		<th>Total Rs</th>
	</tr>
	<?php if ($indents) { ?>
		<?php foreach ($indents as $key => $value) { ?>
			<tr>
			    <td style="text-align: left;"><?php echo $value['request'] ?></td>
			    <td style="text-align: left;"><?php echo $value['indent_code'] ?></td>
			    <td style="text-align: left;"><?php echo $value['date'] ?></td>
			    <td style="text-align: left;"><?php echo $value['productraw_name'] ?></td>
			    <td style="text-align: left;"><?php echo $value['volume'] ?></td>
			    <td style="text-align: left;"><?php echo $value['packing'] ?></td>
			    <td style="text-align: right;"><?php echo $value['quantity'] ?></td>
			    <td style="text-align: left;"><?php echo $value['ordered_qty'] ?></td>
			    <td style="text-align: right;"><?php echo $value['purchase_price'] ?></td>
			    <td style="text-align: right;"><?php echo $value['purchase_value'] ?></td>
			    <td style="text-align: left;"><?php echo $value['gst_rate'] ?></td>
			    <td style="text-align: right;"><?php echo $value['gst_value'] ?></td>
			    <td style="text-align: right;"><?php echo $value['total'] ?></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>

</body>
</html>
