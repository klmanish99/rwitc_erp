<?php
/*echo'<pre>';
print_r($indents);
exit;*/
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 5px;
}

</style>
</head>
<body>

<h2 style="text-align: center;" >Medicine Dump Report</h2>
<div>
	<h4 style="text-align: center;">Medicine Name : <?php echo $medicinenName; ?></h4>
	<?php 
		$timestamp = time(); 
	?>
	<h4 style="text-align: center">Geneated On: <?php echo(date("d-m-Y h:i A", $timestamp)) ?></h4>
	<?php if ($from_date && $to_date) { ?>
		<h5 style="text-align: center;">(From : <?php echo $from_date; ?> - To : <?php echo $to_date; ?>)</h5>
	<?php } ?>
</div>
<h5>NOTE : <?php echo $note; ?></h5>
<h3>GRN</h3>
<div class="col-sm-12">
	<div class="table-responsive">
		<table>
			<tr>
				<th class="text-left"><?php echo 'Date'; ?></th>
				<th class="text-left"><?php echo 'Supplier'; ?></th>
				<th class="text-left"><?php echo 'Quantity'; ?></th>
				<th class="text-left"><?php echo 'Unit'; ?></th>
				<th class="text-left"><?php echo 'Value'; ?></th>
				<th class="text-left"><?php echo 'GRN NO'; ?></th>
				<th class="text-left"><?php echo 'Ex Date'; ?></th>
				<th class="text-left"><?php echo 'Batch No'; ?></th>
		</tr>
			<?php if ($inwards) { ?>
				<?php foreach ($inwards as $key => $value) { ?>
					<tr>
					    <td class="text-left"><?php echo $value['date']; ?></td>
		            	<td class="text-left"><?php echo $value['supplier']; ?></td>
		            	<td class="text-left"><?php echo $value['quantity']; ?></td>
		            	<td class="text-left"><?php echo $value['unit']; ?></td>
		            	<td class="text-left"><?php echo $value['value']; ?></td>
		            	<td class="text-left"><?php echo $value['order_no']; ?></td>
		            	<td class="text-left"><?php echo $value['ex_date']; ?></td>
		            	<td class="text-left"><?php echo $value['batch_no']; ?></td>
					                        	
					</tr>
				<?php } ?>
				<tr>
					<td class="text-center" colspan="2"><b><?php echo 'Total' ?></b></td>
					<td class="text-right" colspan="1"><?php echo $grn_qty_total; ?></td>
					<td class="text-right" colspan="1"><?php echo $total; ?></td>
					<td class="text-center" colspan="3"></td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>
<h3>Medicine Transfer</h3>
<div class="col-sm-12">
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="text-left"><?php echo 'Date'; ?></th>
					<th class="text-left"><?php echo 'Clinic Name'; ?></th>
					<th class="text-left"><?php echo 'Doctor'; ?></th>
					<th class="text-left"><?php echo 'Quantity'; ?></th>
					<th class="text-left"><?php echo 'Unit'; ?></th>
					<th class="text-left"><?php echo 'Trn No'; ?></th>
				</tr>
				
			</thead>
			<tbody>
				<?php if ($medtransfer) { ?>
					<?php foreach ($medtransfer as $mvalue) { ?>
					<tr>
                    	<td class="text-left"><?php echo $mvalue['entry_date']; ?></td>
                    	<td class="text-left"><?php echo $mvalue['parent_doctor_name']; ?></td>
                    	<td class="text-left"><?php echo $mvalue['doctor_name']; ?></td>
                    	<td class="text-left"><?php echo $mvalue['product_qty']; ?></td>
                    	<td class="text-left"><?php echo $mvalue['unit']; ?></td>
                    	<td class="text-left"><?php echo $mvalue['issue_no']; ?></td>
					</tr>
					<?php } ?>
					<tr>
						<td class="text-center" colspan="3"><b><?php echo 'Total' ?></b></td>
						<td class="text-right" colspan="1"><?php echo $qty_total; ?></td>
						<td class="text-center" colspan="1"></td>
					</tr>
				<?php } else { ?>
				<tr>
					<td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>

<h3>ISSUES</h3>
<div class="col-sm-12">
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="text-left"><?php echo 'Date'; ?></th>
					<th class="text-left"><?php echo 'Doctor Name'; ?></th>
					<th class="text-left"><?php echo 'Horse Name'; ?></th>
					<th class="text-left"><?php echo 'Total Qty'; ?></th>
					<th class="text-left"><?php echo 'Unit'; ?></th>
					<th class="text-left"><?php echo 'Clinic Name'; ?></th>
					<th class="text-left"><?php echo 'Total Item'; ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if ($medicine_trans_datas) { ?>
					<?php foreach ($medicine_trans_datas as $medicine_transfer) { ?>
					<tr>
						<td class="text-left"><?php echo date('d-m-Y', strtotime($medicine_transfer['entry_date'])); ?></td>
						<td class="text-left"><?php echo $medicine_transfer['doctor_name']; ?></td>
						<td class="text-left"><?php echo $medicine_transfer['horse_name']; ?></td>
						<td  class="text-right"><?php echo $medicine_transfer['total_qty']; ?></td>
						<td  class="text-right"><?php echo $medicine_transfer['unit']; ?></td>
						<td class="text-left"><?php echo $medicine_transfer['clinic_name']; ?></td>
						<td  class="text-right">
							<?php echo $medicine_transfer['total_item']; ?>
						</td>
					</tr>
					<?php } ?>
					<tr>
						<td class="text-center" colspan="3"><b><?php echo 'Total' ?></b></td>
						<td class="text-right" colspan="1"><?php echo $iss_qty_total; ?></td>
						<td class="text-center" colspan="2"></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<h3>Summary:</h3>
<div>
	<?php if ($inwards) { ?>
		<h5>GRN total Quantity : <?php echo $grn_qty_total; ?></h5>
		<h5>GRN total Value : <?php echo $total; ?></h5>
	<?php } ?>
	<?php if ($medtransfer) { ?>
		<h5>Medicine Transfer total Quantity : <?php echo $qty_total; ?></h5>
		<!-- <h5>Medicine Transfer total Value : <?php echo '' ?></h5> -->
	<?php } ?>
	<?php if ($medicine_trans_datas) { ?>
		<h5>Issues total Quantity : <?php echo $iss_qty_total; ?></h5>
		<h5>Issues total Value : <?php echo '' ?></h5>
	<?php } ?>
	<h5>Available Stock : <?php echo $avl_stock; ?></h5>
	
</div>
</body>
</html>
