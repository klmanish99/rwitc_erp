<?php
/*echo'<pre>';
print_r($indents);
exit;*/
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 5px;
}

td {
  border: 1px solid #dddddd;
  padding: 5px;
}

</style>
</head>
<body>

<h2 style="text-align: center" >Indent Report</h2>
<?php 
	$timestamp = time(); 
?>
<h4 style="text-align: center">Geneated On: <?php echo(date("d-m-Y h:i A", $timestamp)) ?></h4>
<table style="width: 50%;margin-left: 23%;" >
	<tr>
		<th>Indent Code</th>
		<th>Narration</th>
		<th>Date</th>
	</tr>
	<?php if ($indents) { ?>
		<?php foreach ($indents as $key => $value) { ?>
			<tr>
			    <td style="text-align: center"><?php echo $value['indent_code'] ?></td>
			    <td><?php echo $value['narration'] ?></td>
			    <td style="text-align: center"><?php echo $value['date'] ?></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
<br>
<table>
	<tr>
		<th>Requested By</th>
		<th>Code</th>
		<th>Medicine Name</th>
		<th>Supplier Name</th>
		<th>Volume</th>
		<th>Packing Type</th>
		<th>Quantity</th>
		<th>Converted Qty</th>
		<th>Rate Rs</th>
		<th>Value Rs</th>
		<th>GST Rate</th>
		<th>GST Value Rs</th>
		<th>Total Rs</th>
	</tr>
	<?php if ($indents_item) { ?>
		<?php foreach ($indents_item as $pkey => $pvalue) { ?>
			<tr>
			    <td style="text-align: left"><?php echo $pvalue['request'] ?></td>
			    <td style="text-align: left"><?php echo $pvalue['med_code'] ?></td>
			    <td style="text-align: left"><?php echo $pvalue['med_name'] ?></td>
			    <td style="text-align: left"><?php echo $pvalue['supplier_name'] ?></td>
			    <td style="text-align: left"><?php echo $pvalue['volume'] ?></td>
			    <td style="text-align: left"><?php echo $pvalue['packing'] ?></td>
			    <td style="text-align: right"><?php echo $pvalue['quantity'] ?></td>
			    <td style="text-align: left"><?php echo $pvalue['ordered_qty'] ?></td>
			    <td style="text-align: left"><?php echo $pvalue['purchase_price'] ?></td>
			    <td style="text-align: right"><?php echo $pvalue['purchase_value'] ?></td>
			    <td style="text-align: left"><?php echo $pvalue['gst_rate'] ?></td>
			    <td style="text-align: right"><?php echo $pvalue['gst_value'] ?></td>
			    <td style="text-align: right"><?php echo $pvalue['total'] ?></td>
			</tr>
		<?php } ?>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td><b>Total</b></td>
		<td style="text-align: right"><b><?php echo $final_value; ?></b></td>
		<td></td>
		<td style="text-align: right"><b><?php echo $final_gst_val; ?></b></td>
		<td style="text-align: right"><b><?php echo $final_total; ?></b></td>
		<br>
	<?php } ?>
</table>
		<div style="margin-top: 20px;"><b>APPROVED BY</b> <?php echo $value['approved_by'] ?></div>

</body>
</html>
