<?php
/*echo'<pre>';
print_r($indents);
exit;*/
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 5px;
}

</style>
</head>
<body>

<h2 style="text-align: center;" >Store Reconsilation</h2>
<div>
	<!-- <h4 style="text-align: center;">Doctor Name : <?php echo $filter_doctor_name; ?></h4> -->
	<?php 
		$timestamp = time(); 
	?>
	<h4 style="text-align: center">Geneated On: <?php echo(date("d-m-Y h:i A", $timestamp)) ?></h4>
	<?php if ($filter_date) { ?>
		<h5 style="text-align: center;">Date : <?php echo $filter_date; ?> </h5>
	<?php } ?>
</div>
 <?php if ($med_transaction) { ?>
    <div class="table-responsive">
         <?php foreach ($med_transaction as $medicine_transfer_datas_doctor) {  //echo "<pre>";print_r($medicine_transfer_datas_doctor);exit;?>
                <?php foreach ($medicine_transfer_datas_doctor as $doctores =>  $medicine_transfer_datas_doctors) { // echo "<pre>";print_r($doctores);exit;?>
            <table class="table table-bordered table-hover" id="tbladdMedicineTran" style="page-break-after: always;">
                <thead>
                    <?php if(isset($doctores)){ ?> <tr ><td class="text-left" colspan ="8" style="color: #030303;font-weight: 900;" > <?php echo strtoupper($doctores); ?>  </td></tr><?php } ?>
                    <tr>
                        <td style="font-weight: bold;text-align: center;">Sr No</td>
                        <td style="font-weight: bold;text-align: center;">Medicine Name</td>
                        <td style="font-weight: bold;text-align: center;">Unit</td>
                         <td style="font-weight: bold;text-align: center;">Opening</td>
                        <td style="font-weight: bold;text-align: center;">Transfer Quantity</td>
                        <td style="font-weight: bold;text-align: center;">Treatment Quantity</td>
                        <td style="font-weight: bold;text-align: center;">Return Medicine</td>
                        <td style="font-weight: bold;text-align: center;">Avilable Qty</td>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($medicine_transfer_datas_doctors as $key => $medicine_transfer_data) { ?>
                         <?php if(isset($medicine_transfer_data['product_name'])){   ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $i++; ?></td>
                            <td class="text-left"><?php echo $medicine_transfer_data['product_name'] ?></td>
                            <td class="text-center"><?php echo $medicine_transfer_data['store_unit']; ?></td>
                            <td style="text-align: right;"><?php echo $medicine_transfer_data['opening']; ?></td>
                            <td style="text-align: right;"><?php echo $medicine_transfer_data['total_trans']; ?></td>
                            <td style="text-align: right;"><?php echo $medicine_transfer_data['treatment_medicine']; ?></td>
                            <td style="text-align: right;"><?php echo $medicine_transfer_data['return_medicine']; ?></td>
                            <td style="text-align: right;"><?php echo $medicine_transfer_data['closing']; ?></td>
                        </tr>
                        <?php } ?><?php }  ?>
                </tbody>
            </table><br><br>
         <?php } ?>  <?php }  ?>
    </div>
<?php } else if(empty($med_transaction) && $no_result == 0)  { ?>
     <h3 class="text-center noresults" style="text-align: center;">Please Day Close !</h3>
<?php } else { ?>
    <h3 class="text-center noresults" style="text-align: center;"><?php echo $text_no_results; ?></h3>
<?php } ?>
<!-- <?php if ($new_med_treatment_transaction) { ?>
    <div class="table-responsive" style="margin-top: 2%;">
        <table class="table table-bordered table-hover">
            <thead>
                <tr ><td class="text-left" colspan ="6" style="font-weight: bold;">Medicine Used For Treatment But Not Transfer</td></tr>
                <tr>
                   <td  style="font-weight: bold;text-align: center;">Sr No</td>
                   <td  style="font-weight: bold;text-align: center;">Date</td>
                    <td  style="font-weight: bold;text-align: center;">Medicine Name</td>
                    <td style="font-weight: bold;text-align: center;">Unit</td>
                    <td  style="font-weight: bold;text-align: center;">Quantity</td>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                <?php foreach ($new_med_treatment_transaction as $medicine_transfer) { ?>
                    <tr>
                        <td style="text-align: center;"><?php echo $i++; ?></td>
                        <td class="text-left"><?php echo date('d-m-Y', strtotime($medicine_transfer['entry_date'])); ?></td>
                        <td class="text-left"><?php echo $medicine_transfer['product_name']; ?></td>
                        <td  class="text-left"><?php echo $medicine_transfer['store_unit']; ?></td>
                        <td  style="text-align: right;"><?php echo $medicine_transfer['medicine_qtys']; ?></td>
                    </tr>
                    <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?> -->


</body>
</html>
