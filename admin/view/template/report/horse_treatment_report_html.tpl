<?php
/*echo'<pre>';
print_r($indents);
exit;*/
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}

td {
  border: 1px solid #dddddd;
  padding: 8px;
}

</style>
</head>
<body>

<h2 style="text-align: center">Datewise Horse Report</h2>
<?php 
	$timestamp = time(); 
?>
<h4 style="text-align: center">Geneated On: <?php echo(date("d-m-Y h:i A", $timestamp)) ?></h4>

<table>
	<tr>
		<th>Entry Date</th>
		<th>Horse</th>
		<th>Trainer</th>
		<th>Medicine Name</th>
		<th>Type</th>
		<th>Unit</th>
		<th>Medicine Qty</th>
	</tr>
	<?php if ($horsetreatments) { ?>
		<?php foreach ($horsetreatments as $key => $horsetreatment) { ?>
			<tr>
			    <td style="text-align: center;"><?php echo $horsetreatment['entry_date'] ?></td>
			    <td style="text-align: left;"><?php echo $horsetreatment['horse_name'] ?></td>
			    <td style="text-align: left;"><?php echo $horsetreatment['trainer_name'] ?></td>
			    <td style="text-align: left;"><?php echo $horsetreatment['medicine_name'] ?></td>
			    <td style="text-align: left;"><?php echo $horsetreatment['med_type'] ?></td>
			    <td style="text-align: left;"><?php echo $horsetreatment['unit'] ?></td>
			    <td style="text-align: right;"><?php echo $horsetreatment['medicine_qty'] ?></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>

</body>
</html>
