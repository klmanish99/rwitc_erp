<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <!-- <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div> -->
  </div>
  <link type="text/css" href="view/stylesheet/myform.css" rel="stylesheet" media="screen" />
  <div class="container-fluid">
     <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-amount" class="form-horizontal">
            <div class="form-group" style="margin-left: 26%;">
                <label class="col-sm-2 control-label" for="input-horse_name"><?php echo "Horse Name:"; ?></label>
                <div class="col-sm-3">
                    <input readonly="readonly" type="text"  name="horse_name" id="horse_name" value="<?php echo $horse_name; ?>" placeholder="Horse Name" class="form-control" />
                </div>
            </div>
            <div class="form-group" style="margin-left: 26%;">
                <label class="col-sm-2 control-label" for="input-trainer_name"><?php echo "Trainer Name:"; ?></label>
                <div class="col-sm-3">
                    <input readonly="readonly" type="text"  name="trainer_name" id="trainer_name" value="<?php echo $trainer_name; ?>" placeholder="Trainer Name" class="form-control" />
                </div>
            </div>
            <div class="form-group" style="margin-left: 26%;">
                <label class="col-sm-2 control-label" for="input-amount"><i style="color: red"> * </i><?php echo "Amount:"; ?></label>
                <div class="col-sm-3">
                    <input type="text"  name="amount" id="amount" value="<?php echo $amount; ?>" placeholder="Amount" class="form-control" />
                    <?php if (isset($valierr_amount)) { ?><span class="errors"><?php echo $valierr_amount; ?></span><?php } ?>
                </div>
             </div>
            <div class="form-group" style="margin-left: 26%;border-top: 0;">
                <label class="col-sm-2 control-label" for="input-letter_date"><i style="color: red"> * </i><?php echo "Letter Dated :"; ?></label>
                <div class="col-sm-3">
                    <div class="input-group date input-letter_date">
                        <input type="text" name="letter_date" value="<?php echo $letter_date; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY"  class="form-control" />
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                    <?php if (isset($valierr_letter_date)) { ?><span class="errors"><?php echo $valierr_letter_date; ?></span><?php } ?>
                </div>
            </div>
            <div class="form-group" style="margin-left: 26%;border-top: 0;">
                <label class="col-sm-2 control-label" for="input-received_date"><i style="color: red"> * </i><?php echo "Received Date :"; ?></label>
                <div class="col-sm-3">
                    <div class="input-group date input-received_date">
                        <input type="text" name="received_date" value="<?php echo $received_date; ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY"  class="form-control" />
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                    <?php if (isset($valierr_received_date)) { ?><span class="errors"><?php echo $valierr_received_date; ?></span><?php } ?>
                </div>
            </div>
            <div style="margin-left: 45%;"><button type="submit" style="" class="btn btn-primary btn-lg" >Save</button></div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
    pickTime: false
});
//--></script></div>
<script type="text/javascript">
    $('.input').keypress(function (e) {
      if (e.which == 13) {
        $('#form-amount').submit();
        return false;    //<---- Add this line
      }
    });
</script>
<?php echo $footer; ?>