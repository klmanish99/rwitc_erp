<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
	<!-- <div class="container-fluid">
	  <h1><?php echo $heading_title; ?></h1>
	  <ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	  </ul>
	</div> -->
  </div>
  <div class="container-fluid">
	 <?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($error) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
	<div class="panel panel-default">
	  <div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
	  </div>
	  <div class="panel-body">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-horse_link" class="form-horizontal">
			<div class="form-group" >
				<label class="col-sm-2 control-label" for="input-horse"><?php echo "Horse Name:"; ?></label>
				<div class="col-sm-3">
					<input type="text"  name="filterHorseName" id="HorseName" value="<?php echo $official_name ?>" placeholder="Horse Name" class="form-control" readonly="readonly"/>
					<input type="hidden" name="filterHorseId" id="horse_id" value="<?php echo $horse_id ?>"  class="form-control">
				</div>
				<label class="col-sm-2 control-label" for="input-trainer"><?php echo "Trainer Name :"; ?></label>
				<div class="col-sm-3">
					 <input type="text" name="trainer_name"  placeholder="<?php echo "Trainer Name"; ?>"  value="<?php echo $trainer_name ?>" placeholder="Trainer Name" id="input-trainer_name" class="form-control"  readonly="readonly"/>
				<input type="hidden" name="trainer_id"  placeholder="<?php echo "Trainer Name"; ?>"  value="<?php echo $trainer_id ?>" id="trainer_id" class="form-control" />
				</div>
			</div>
			<div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Join Color</h4>
					  </div>
					  <div class="modal-body">
						<!-- <input type="hidden" name="isEdit" value="" placeholder="" id="isEdit" class="form-control" />
						<div class="form-group">
						  <label class="col-sm-3 control-label" for="select-club"><b style="color: red">*</b>Club:</label>
						  <div class="col-sm-8">
							<select  name="club" placeholder="Club" id="select-club" data-index="40" class="form-control">
								<option value="" selected="selected" disabled="disabled" >Please Select</option>
								<?php foreach ($clubs as $key => $value) { ?>
								<option value="<?php echo $value; ?>" ><?php echo $value ?></option>
								<?php } ?>
							</select>
							<span style="display: none;" id="error_ban_club" class="error">Please Select Club.</span>
						  </div>
						</div>
					  
						<div class="form-group">
						  <label class="col-sm-3 control-label" for="select-banType"><b style="color: red">*</b>Ban Type:</label>
						  <div class="col-sm-8">
							<select  name="ban_type" placeholder="Ban Type" id="select-banType" data-index="41" class="form-control">
								<option value="" selected="selected" disabled="disabled" >Please Select</option>
								<?php foreach ($banType as $key => $value) { ?>
								<option value="<?php echo $value; ?>" ><?php echo $value ?></option>
								<?php } ?>
							</select>
							<span style="display: none;" id="error_ban_type" class="error">Please Select Ban Type.</span>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-sm-3 control-label" for="input-banStartDate"><b class="rqrd">*</b> Start Date</label>
						  <div class="col-sm-8">
							<div class="input-group date input-banStartDate">
							  <input type="text" name="ban_start_date" value="" data-index="42" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="input-banStartDate" class="form-control" />
							  <span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
							  </span>
							</div>
							<span style="display: none;" id="error_start_date" class="error">Start Date can not be blank.</span>
							<span style="display: none;" id="error_start_date_2" class="error">Invalid Start Date.</span>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-sm-3 control-label" for="input-banEndDate">End Date</label>
						  <div class="col-sm-8">
							<div class="input-group date input-banEndDate">
							  <input type="text" name="ban_end_date" value="" data-index="43" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="input-banEndDate" class="form-control" />
							  <span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
							  </span>
							</div>
							<span style="display: none;" id="error_end_date" class="error">End Date can not be blank.</span>
							<span style="display: none;" id="error_end_date_2" class="error">Invalid End Date.</span>
							<span style="display: none;color: red;font-weight: bold;" id="error_greater_start_date" class="error"><?php echo 'End Date must not be less than Start Date!'; ?></span>
						  </div>
						</div>
 -->
						<div class="form-group">
						  <label class="col-sm-3 control-label" for="input-banAmount"><b style="color: red">*</b>Color</label>
						  <div class="col-sm-8">
							  <input type="text" name="join_color"  placeholder="Color" data-index="44" id="input-join_color" class="form-control" />
							  <input type="hidden" name="hidden_join_color"  placeholder="Color" id="hidden_join_color" class="form-control" />
						  </div>
						</div>

					  </div>
					  <!-- Model Footer -->
					  <div class="modal-footer">
						  <!-- <input type="text" name="id_hidden_BanFunction" value="" id="id_hidden_BanFunction" class="form-control" />
						  <input type="text" name="idban_increment_id" value="" id="idban_increment_id" class="form-control" /> -->
						  <button type="button" class="btn btn-primary" id = "ban_save_id" onclick="savecolor()"  >Save</button>
						  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					  </div>

				  </div>
			  </div>
			<div class="form-group">
				<table class="table table-striped table-bordered">

					<thead>
						<tr>
							<td class="text-center;" style="vertical-align: middle;"><?php echo "Name"; ?></td>
							<td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>
							<td class="text-center" style="vertical-align: middle;"><?php echo "Period"; ?></td>
							<td class="text-center" style="vertical-align: middle;"><?php echo "Share"; ?></td>
							<td class="text-center" style="vertical-align: middle;"><?php echo "Order"; ?></td>
							<td class="text-center" style="vertical-align: middle;"><?php echo "Color"; ?></td>
							<td class="text-center" style="vertical-align: middle;"><?php echo "Contingency"; ?></td>
							<td class="text-center" style="vertical-align: middle;"><?php echo "Ownership"; ?></td>
						</tr>
					</thead>
					<tbody id="equipmentdeatilsbody">
						<?php  $i=1; ?>
						<?php if($final_owner) { //echo'<pre>';print_r($final_owner); exit(); ?>
							<?php foreach($final_owner as $results => $result) { ?>
								<tr>
									<td class="r_<?php echo $results ?>" style="text-align: left;">
										<span><?php echo $result['to_owner']  ?> (<?php echo $result['parent_owner_color']  ?>)</span><br>
										<input type="hidden" name="owner_datas[<?php echo $results ?>][to_owner_id]" value="<?php echo $result['to_owner_id']?>" class="ent-evnt" id="owner_id_<?php echo $results ?>"  />
										<input type="hidden" class="owner_data" name="owner_datas[<?php echo $results ?>][horse_to_owner_id]" value="<?php echo $result['horse_to_owner_id']?>"  id="fro_ow_ck_<?php echo $results ?>"  />
										
									</td>
									
									<td style="text-align: center;">
										<?php
											$provisional_ownership  = ($result['provisional_ownership'] == 'Yes') ? 'Provisional Ownership' : "";

										if($result['ownership_type'] == 'Lease'){ ?>
											<span style="font-size:12px";><?php echo  $provisional_ownership ?> L </span>
										<?php } else if($result['ownership_type'] == 'Sub Lease'){ ?>
											<span style="font-size:12px";> <?php echo  $provisional_ownership ?> SL </span>
										<?php } else {	?>
											<span style="font-size:12px";> <?php echo  $provisional_ownership ?> O </span>
										<?php } ?>
									</td>

									<td style="text-align: left;">
										<?php
											$from_date = date('d-m-Y', strtotime($result['date_of_ownership']));
											$to_date = date('d-m-Y', strtotime($result['end_date_of_ownership']));

										if($result['ownership_type'] == 'Lease'){ ?>
											<span style="font-size:12px";>  <?php echo  $from_date. '- ' .$to_date ?> </span>
										<?php } else if($result['ownership_type'] == 'Sub Lease'){ ?>
											<span style="font-size:12px";>  <?php echo  $from_date. '- ' .$to_date ?> </span>
										<?php } else {
											$end_date = ($result['end_date_of_ownership'] != '0000-00-00') ? date('d-m-Y', strtotime($result['end_date_of_ownership'])) : "";
											?>
											<span style="font-size:12px";>  <?php echo $from_date.' - '. $end_date ?> </span>

										<?php } ?>
									</td>



									<td class="r_<?php echo $results ?>" style="text-align: right;">
										<span><?php echo $result['owner_percentage'].'%' ?></span>
										<input type="hidden" name="owner_datas[<?php echo $results ?>][owner_percentage]" value="<?php echo $result['owner_percentage']?>" class="ent-evnt" id="owner_percentaged_<?php echo $results ?>"  />
									</td>
									<td class="r_<?php echo $results ?>" style="text-align: right;width: 10%;">
										<?php 

										//$orderby = ($result['horse_to_owner_order'] == 0) ? $i : $result['horse_to_owner_order'];
											//echo '<pre>';print_r( $i);echo '<br/>';
											//echo '<pre>';print_r( $result['horse_to_owner_order']);

										if($i == $result['horse_to_owner_order']){ ?>
											<input  type="text" name="owner_datas[<?php echo $results ?>][owner_order]" value="<?php echo $result['horse_to_owner_order'] ?>" id="owner_order_<?php echo $results ?>" style="text-align: right;"  <?php if ($order_link == 0){ echo 'readonly="readonly"'; } ?> class="form-control ent-evnt" />
										<?php } else {  ?>
											<input  type="text" name="owner_datas[<?php echo $results ?>][owner_order]" value="<?php echo $i ?>" id="owner_order_<?php echo $results ?>" style="text-align: right;"  <?php if ($order_link == 0){ echo 'readonly="readonly"'; } ?> class="form-control ent-evnt" />
										
										<?php } ?>
									</td>
									<td class="text-left"  class="r_<?php echo $results ?>" style="text-align: center;width: 10%;" >
										<?php if($result['owner_color'] == 'Yes') {?>
											<input  type="radio" name="owner_color" class="checkbox_color checkbox_values" value="<?php echo $result['horse_to_owner_id']?>" id="owner_color_<?php echo $results ?>" <?php if ($color_link == 0){ echo 'onclick="return false"'; } ?> checked="checked"/>
											<input type="hidden" class="owner_coloz" id="owner_coloz_<?php echo $results ?>"  name="owner_datas[<?php echo $results ?>][horse_color_selected]" value="<?php echo $result['horse_to_owner_id']?>">
											
										<?php } else { ?>
											<input  type="radio" name="owner_color"  class="checkbox_color checkbox_values"  value="<?php echo $result['horse_to_owner_id']?>" id="owner_color_<?php echo $results ?>" <?php if ($color_link == 0){ echo 'onclick="return false"'; } ?>/>
											<input type="hidden" class="owner_coloz" id="owner_coloz_<?php echo $results ?>"  name="owner_datas[<?php echo $results ?>][horse_color_selected]" value="0">
										 <?php }  ?>
									</td>

									<td class="r_<?php echo $results ?>" style="text-align: center;">
										<?php 
											$contengency = ($result['contengency'] == 1) ? "Yes" : "N/A";
											$cont_percentage = ($result['cont_percentage'] != 0) ? $result['cont_percentage'] : "";
											$cont_amount = ($result['cont_amount'] != 0) ? $result['cont_amount'] : "0";
											
										?>

										<span><a class="modalopen" id="details_<?php echo $result['horse_to_owner_id']?>" style="cursor: pointer;">
                                        <?php if($contengency == 'Yes'){
                                         		echo $cont_percentage.'% / Rs.'.$cont_amount ?>
                                         	</a></span><br>
                                        <?php } ?>
                                    </td>
                                    <td class="r_<?php echo $results ?>" style="text-align: left;">
                                    	<?php if(isset($result['parent_owner_name']) && $result['ownership_type'] != 'Sale') {
                                    		if($result['ownership_type'] == 'Lease'){ ?>
	                                    		<span><?php echo $result['parent_owner_name']  ?> ( In the case of Lease )</span>
	                                    	<?php } else if($result['ownership_type'] == 'Sub Lease'){ ?>
	                                    		<span><?php echo $result['sub_parent_owner_name']  ?> ( In the case of Sub-Lease )</span> => <span><?php echo $result['parent_owner_name']  ?> ( In the case of Sub-Lease )</span>
	                                    	<?php } else { ?>
	                                    		<span><?php echo $result['sub_parent_owner_name']  ?> ( In the case of Lease )</span> => <span><?php echo $result['parent_owner_name']  ?> ( In the case of Sub-Lease )</span>
	                                    	<?php }  ?>
										<?php }  ?>
                                    </td>
								</tr>
							<?php $i++;  } // exit;?>
						<tr>
							<td>
								<span style="color: red" data-toggle="modal" data-target="#myModal">Join Color</span>
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td class="text-left" style="text-align: center;width: 10%;">
								<?php if($result['owner_color'] == 'join') {?>
									<input  type="radio" name="owner_color"  id="owner_color_<?php echo $results ?>" <?php if ($color_link == 0){ echo 'onclick="return false"'; } ?> class="checkbox_color checkbox_values checked_join"  value="<?php echo '2'?>"  checked="checked"/>
								<?php } else { ?>
									<input  type="radio" name="owner_color"  id="owner_color_<?php echo $results ?>" <?php if ($color_link == 0){ echo 'onclick="return false"'; } ?> class="checkbox_color checkbox_values checked_join"  value="<?php echo '2'?>" />
								<?php }  ?>
								<!-- <input  type="hidden" name="owner_color_join"  id="owner_color_<?php echo $results ?>" <?php if ($color_link == 0){ echo 'onclick="return false"'; } ?> class="checkbox_color checkbox_values checked_join_hidden" /> -->
								<!-- <input type="hidden" class="owner_coloz" id="owner_coloz_<?php echo $results ?>"  name="owner_datas[<?php echo $results ?>][horse_color_selected]" value="0"> -->
							</td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
					<?php  } ?>
				</table>
			</div>
			<?php if($final_owner)  { ?>

				<div style="display: flex;align-items: center;justify-content: center;">
					<button type="submit" name="only_update" class="btn btn-primary btn-lg" >Only Update</button>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="submit" name="print_letter" class="btn btn-primary btn-lg" style="background-color: #008a2b;border-color: #008a2b;">Print Letter</button>
				</div>
		   <?php } ?>
		</form>
	  </div>
	</div>
	<div class="modal fade" id="contModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contingency Details</h4>
                </div>
                <div class="modal-body" style="height: 240px;">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<script type="text/javascript">
	$('.input').keypress(function (e) {
	  if (e.which == 13) {
		$('#form-horse_link').submit();
		return false;    //<---- Add this line
	  }
	});

	$('.checkbox_color').change(function(){
		//alert($('input[name=owner_color]:checked', '#form-horse_link').val());
		horse_to_owner_id = $(this).val();
		auto_id = $(this).attr('id');
		id = auto_id.split('_');
		$('.owner_coloz').val('');

		$('#owner_coloz_'+id[2]).val(horse_to_owner_id);
		//alert(id);
	})


	//contingency modal 


$(document).on("click",".modalopen",function() {
    //alert('inn');
    console.log($(this).attr('id'));
    horse_owner_idssss = $(this).attr('id');
    horse_owner_id = horse_owner_idssss.split('_');

    $.ajax({
        url: 'index.php?route=transaction/ownership_shift_module/getContaingencyData&token=<?php echo $token; ?>&horse_owner_id='+horse_owner_id[1],
        dataType: 'json',
        success: function(json) {   
            $('.modal-body').html('');
            $('.modal-body').append(json);
    		$('#contModal').modal('show'); 
        }
    });




});
</script>
<script type="text/javascript">
	$('#input-join_color').autocomplete({
	'source': function(request, response) {
	$.ajax({
		url: 'index.php?route=transaction/horse_datas_link/autocompleteJoinColor&token=<?php echo $token; ?>&join_color=' +  encodeURIComponent(request),
		dataType: 'json',
		success: function(json) {
		response($.map(json, function(item) {
			return {
			label: item['doctor_name'],
			id: item['id'],
			value: item['doctor_name'],
			}
		}));
		}
	});
	},
	'select': function(item) {
	$('#input-join_color').val(item['label']);
	$('#input-join_color').val(item['value']);
	$('#hidden_join_color').val(item['id']);
	$('.dropdown-menu').hide();
	}
});
</script>
<script type="text/javascript">
	function savecolor(){
		var color = $('#input-join_color').val();
		var horse_id = $('#horse_id').val();
		//alert(horse_id);
		$.ajax({
		url: 'index.php?route=transaction/horse_datas_link/UpdateColor&token=<?php echo $token; ?>&color=' +color+'&horse_id=' +horse_id ,
		dataType: 'json',
		success: function(json) {
			if(json.success == 1){
				$('#myModal').modal('hide');
			}
		}
	});
	}
</script>
<!-- <script type="text/javascript">

$('.checked_join').change(function () {
    if ($(this).is(':checked')) {
        $('.checked_join_hidden').val('2');
    } else if ($(this).is(':checked') == false) {
        $('.checked_join_hidden').val('');
    };
});
</script> -->
<?php echo $footer; ?>