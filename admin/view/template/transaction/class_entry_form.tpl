<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="distance"><?php echo "Distance:"; ?></label>
                <div class="col-sm-2">
                    <select name="distance" id="distance" class="form-control"  tabindex="">
                      <option value="<?php  ?>">Please Select</option>
                      <?php foreach ($distances as $skey => $svalue) { ?>
                        <?php if ($skey == $distance) { ?>
                          <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                        <?php } else { ?>
                          <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                        <?php } ?>
                      <?php } ?>
                    </select>
                    <?php if (isset($valierr_distance)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_distance; ?></span><?php } ?>
                </div>
                <label class="col-sm-2 control-label" for="class"><?php echo "Class:"; ?></label>
                <div class="col-sm-2">
                    <select name="class" id="class" class="form-control"  tabindex="">
                      <option value="<?php  ?>">Please Select</option>
                        <?php foreach ($classs as $skey => $svalue) { ?>
                        <?php if ($skey == $class) { ?>
                          <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                        <?php } else { ?>
                          <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                        <?php } ?>
                      <?php } ?>
                    </select>
                    <?php if (isset($valierr_class)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_class; ?></span><?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="entry">Entry</label>
                <div class="col-sm-2">
                  <input type="text" name="entry" value="<?php echo $entry; ?>" placeholder="Entry" id="entry" class="form-control" tabindex="3"/>
                </div>
                <label class="col-sm-2 control-label" for="acceptance">Acceptance</label>
                <div class="col-sm-2">
                  <input type="text" name="acceptance" value="<?php echo $acceptance; ?>" placeholder="Acceptance" id="acceptance" class="form-control" tabindex="3"/>
                </div>
                <label class="col-sm-1 control-label" for="division">Division</label>
                <div class="col-sm-2">
                  <input type="text" name="division" value="<?php echo $division; ?>" placeholder="Division" id="division" class="form-control" tabindex="3"/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="two_nov_dec_entry">2 Nov Dec Entry</label>
                <div class="col-sm-2">
                  <input type="text" name="two_nov_dec_entry" value="<?php echo $two_nov_dec_entry; ?>" placeholder="2 Nov Dec Entry" id="two_nov_dec_entry" class="form-control" tabindex="3"/>
                </div>
                <label class="col-sm-2 control-label" for="two_nov_dec_acceptance">2 Nov Dec Acceptance</label>
                <div class="col-sm-2">
                  <input type="text" name="two_nov_dec_acceptance" value="<?php echo $two_nov_dec_acceptance; ?>" placeholder="2 Nov Dec Acceptance" id="two_nov_dec_acceptance" class="form-control" tabindex="3"/>
                </div>
                <label class="col-sm-1 control-label" for="select-isActive">Sweepstake</label>
                <div class="col-sm-2">
                    <select name="sweepstake" id="select-sweepstake" class="form-control" tabindex="5">
                        <?php foreach ($Active as $key => $value) { ?>
                        <?php if ($value == $sweepstake) { ?>
                          <option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
                        <?php } else { ?>
                          <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</script>
  
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script>
<script type="text/javascript">
    $(document).on('keypress','input,select, textarea', function (e) {
//$('input,select').on('keypress', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
        if (!$next.length) {
            $next = $('[tabIndex=1]');
        }
        $next.focus();
    }
});
</script>
</div>
<?php echo $footer; ?>