<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><!-- <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> -->
                <!-- <button style="" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button> -->
            </div>
            <h1><?php echo $heading_title ?></h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
                    <div class="well" style="background-color: #ffffff;">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" name="race_name" id="input-race_name" value="<?php  echo $filter_race_name ;?>" placeholder="Race Name" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group date">
                                            <input type="text" name="race_date" value="<?php  echo $filter_race_date ;?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="race_date" class="form-control" />
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <a onclick="filter()" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="padding-bottom: 62px;">
	                    <div class="col-sm-12">
	                    	<button id="button-export" type="button" data-toggle="tooltip" title="" class="btn btn-primary">Run Handicapping All</button>
	                    	<!-- <button type="button" data-toggle="tooltip" title="" class="btn btn-primary">Run Handicapping Selected</button> -->
	                    	<button type="button" id="void_shift_button" data-toggle="tooltip" title="" onclick="void_shift();" class="btn btn-primary" >Void / Shift Race</button>
	                    	<button type="button" data-toggle="tooltip" style="float: right;" title="" class="btn btn-primary">Debit To Owners for The Race</button>
                    	</div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected1\']').prop('checked', this.checked);" /></td>
                                        <td style="width: 2px;" class="text-center">Sr No.</td>
                                        <td style="width: 35%;" class="text-center">Race Name</td>
                                        <td style="width: 10%;" class="text-center">Race Date</td>
                                        <td style="width: 1px;" class="text-center">Foreign Jockey</td>
                                        <td style="width: 6%;" class="text-center">Class</td>
                                        <td class="text-center">Distance</td>
                                        <td style="width: 1%;" class="text-center">Handicapping Status</td>
                                        <td class="text-center">Weight Changed</td>
                                        <td style="width: 10%;" class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($handicappingdatas)) { ?>
                                    <?php $i=1; ?>
                                    <?php foreach ($handicappingdatas as $handicapping) {  
                                        ?>
                                        <tr>
                                            <td class="text-center"><?php if (in_array($handicapping['pros_id'], $selected)) { ?>
                                                <input type="checkbox" name="selected1[]"  value="<?php echo $handicapping['pros_id']; ?>" class="checkboxclass" checked="checked" />
                                                  <?php } else { ?>
                                                <input type="checkbox" name="selected1[]" id="<?php echo $handicapping['pros_id']; ?>" class="checkboxclass" value="<?php echo $handicapping['pros_id']; ?>" />
                                                <?php } ?>
                                            </td>
                                            <td style="width: 2px;" class="text-left"><?php echo  $i++ ;?></td>
                                            <td style="width: 35%;" class="text-left" style="width: 200px;"><?php echo $handicapping['race_name']; ?>
                                            <input type="hidden" name="hidden_race_name" value="<?php echo $handicapping['race_name']; ?>" id="hidden_race_name_<?php echo $handicapping['pros_id']; ?>" class="form-control" />
                                            <input type="hidden" name="hidden_race_description" value="<?php echo $handicapping['race_description']; ?>" id="hidden_race_description_<?php echo $handicapping['pros_id']; ?>" class="form-control" />
                                            </td>
                                            <td style="width: 10%;" class="text-center"><?php echo $handicapping['race_date']; ?></td>
                                            <td style="width: 1px;" class="text-center"><?php 
                                                if ($handicapping['foreign_jockey'] =="Yes") { ?>
                                                <input type="checkbox"  class="checkbox_id" id="check_<?php echo $handicapping['pros_id']; ?>" name="selectedcheck" value="" checked="checked" />
                                                    <?php } else { ?>
                                                <input type="checkbox" name="selectedcheck" id="check_<?php echo $handicapping['pros_id']; ?>" class="checkbox_id"  value="" />
                                                <?php } ?>
                                            </td>
                                            <td style="width: 6%;" class="text-left"><?php echo $handicapping['class']; ?></td>
                                            <td class="text-left"><?php echo $handicapping['distance']; ?></td>
                                            <?php 
                                            if($handicapping['handicap_status']=='0'){
                                                $Handicapping_status = "Pending";
                                            }else{
                                                $Handicapping_status = "Done";
                                            }?>
                                            <td style="width: 1%;" class="text-left"><?php echo $Handicapping_status; ?></td></td>
                                            <td class="text-left"></td>
                                            <td style="width: 10%;" class="text-center">
                                            	<a  onclick="PopupdataFunction('<?php echo $handicapping['pros_id']; ?>');" class="btn btn-primary" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                <a  id="btn_exports" onclick="exports(<?php echo $handicapping['entry_id']; ?>)" class="btn btn-primary" ><i class="fa fa-print" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    <?php   } ?>
                                <?php }  ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
                <div class="col-sm-10" >
                    <div id="myModal" class="modal fade" style ="padding-top: 99px;" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                            <div class="modal-content" style="width: 107%;" >
                                <div class="modal-header">
                                    <button type="button" onclick="blankfunction()" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title" ><?php  //echo ?></h4>
                                    <span  id ="race_namespan"></span>
                                </div>
                                <div class="modal-body" style="margin-left: 24px;margin-right: 24px; height: 320px;">
                                     <div class="form-group">
                                        <label class="col-sm-2 control-label" for="race_description"><?php echo "Race Description"; ?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="race_description" value="" id="race_description" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div id="div_scroll" style="overflow-y:scroll; overflow-x:hidden;height: 200px;">
                                        	<div class="table-responsive">
    				                            <table class="table table-bordered table-hover">
    				                                <thead>
    				                                    <tr>
    				                                        <td class="text-center">Horses Rating</td>
    				                                        <td class="text-center">Horses Name</td>
    				                                        <td class="text-center">Handicapping Weight</td>
    				                                        <td class="text-center">Horse Ban</td>
    				                                    </tr>
    				                                </thead>
    				                                <tbody id="popup_table">
    				                                </tbody>
    				                            </table>
    				                        </div>
                                        </div>
				                    </div>
				                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="weight"><?php echo "Weight"; ?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="weight" value="" id="weight" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                              	<div class="modal-footer">
	                                <button type="button" class="btn btn-default" onclick="datablank();" data-dismiss="modal">Close</button>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10" >
                    <div id="myModal1" class="modal fade" style ="padding-top: 150px;" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                            <div class="modal-content" style="width: 110%" >
                                <div class="modal-header">
                                    <button type="button" onclick="blankfunction()" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title" ><?php  //echo ?></h4>
                                    <span  id ="race_namespan"></span>
                                </div>
                                <div class="modal-body" >
                                    <form id="form_shift_void">
                                        <div style="padding-bottom: 11px;" class="row">
                                            <label class="col-sm-3 control-label" style="padding-top: 10px;">Selected Race Total:</label>
                                            <div>
                                                <input type="text" name="selected_count" value="" style="width: 200px;" id="selected_count" class="form-control col-sm-2" readonly="readonly" />
                                            </div>
                                        </div>
                                        <div id="div_scroll_shift_void" style="overflow-y:scroll; overflow-x:hidden;height: 170px;">
                                            <table id="table_rows_count" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <td class="text-center">ID</td>
                                                        <td class="text-center">Race Name</td>
                                                        <td class="text-center">Race Type</td>
                                                        <td class="text-center">Action</td>
                                                    </tr>
                                                </thead>
                                                <tbody id="shift_void_table">
                                                </tbody>
                                            </table>
                                        </div>
                                         <div  style="padding-left: 230px;padding-top: 11px;">
                                            <button type="button" id="void_button" data-toggle="tooltip" title="" onclick="voidracebutton()" class="btn btn-primary" >Void Race</button>
                                            <button type="button" id="shift_button" data-toggle="tooltip" title="" onclick="shiftracebutton()" class="btn btn-primary" >Shift Race</button>
                                        </div>
                                    </form> 
                                </div>
                                <div class="modal-footer">
                                    <button type="button" onclick="blankfunction()" class="btn btn-default"  data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10" >
                    <div id="myModal2" class="modal fade" style ="padding-top: 150px;padding-left: 102px;" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                            <div class="modal-content" style="width: 110%" >
                                <div class="modal-header">
                                    <button type="button" class="close" onclick="blankfunction()" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title" >
                                    <span  id ="race_namespan"></span>
                                </div>
                                <div class="modal-body" >
                                    <form id="form_shift">
                                        <div class="form-group row">
                                            <label class="col-sm-4 control-label" for="date_changed" style="padding-top: 10px;padding-left: 130px;"><?php echo "Shift Date:"; ?></label>
                                            <div class="col-sm-5"> 
                                                <div class="input-group date">
                                                    <input type="text" name="date_changed" value=""  placeholder="Shift Date" data-date-format="DD-MM-YYYY" id="date_changed" class="form-control" />
                                                    <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="div_scroll_shift_void" style="overflow-y:scroll; overflow-x:hidden;height: 170px;">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <td class="text-center">ID</td>
                                                        <td class="text-center">Race Name</td>
                                                        <td class="text-center">Race Type</td>
                                                        <td class="text-center">Race Date</td>
                                                    </tr>
                                                </thead>
                                                <tbody id="shift_table">
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                        <div  style="padding-left: 230px;padding-top: 11px;">
                                            <button type="button" id="cancle_button" data-toggle="tooltip" title="" onclick="blankfunction()" class="btn btn-primary" >Cancle</button>
                                            <button type="button" id="save_button" data-toggle="tooltip" title="" onclick="shift_date()" class="btn btn-primary" >Save</button>
                                        </div>
                                    </form> 
                                </div>
                                <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-default" onclick="blankfunction()" data-dismiss="modal">Close</button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
         function blankfunction(){
            $("#shift_void_table").remove();
            window.location.reload();
        }

        function void_shift(){
            var checkboxes = $('input:checkbox.checkboxclass:checked').length;
           // alert(checkboxes);
           if(checkboxes > 0){
                var r = confirm("Are You Sure!");
                if (r == true) {
                    $('#myModal1').modal('show');
                    var selected =$('.checkboxclass').val();
                    var pros_id = [];
                    $('.checkboxclass:checked').each(function() {
                       pros_id.push($(this).val());
                    });
                    $.ajax({
                        type: "POST",
                        url: 'index.php?route=transaction/handicapping/selectedfunction11&token=<?php echo $token; ?>&pros_id='+pros_id,
                        data: pros_id,
                        dataType: "json",
                        success: function (json1) {
                            $.each(json1, function(i, value) {
                                html = '<tr id="id'+value.pros_id+'">';
                                    html += '<td class="text-center"  >'+value.pros_id+'';
                                        html += '  <input type="hidden" name=shift_void_data[' + value.pros_id + '][pros_id] value="'+value.pros_id+'"  class="form-control" />';
                                    html += '</td>';
                                    html += '<td class="text-center"  >'+value.race_name+'';
                                        // html += '  <input type="hidden" name=shift_void_data[' + value.pros_id + '][race_name] value="'+value.race_name+'"  class="form-control" />';
                                    html += '</td>';
                                    html += '<td class="text-center"  >'+value.race_type+'';
                                        // html += '  <input type="hidden" name=shift_void_data[' + value.pros_id + '][race_type] value="'+value.race_type+'"  class="form-control" />';
                                    html += '</td>';
                                    html += '<td class="text-center"  ><a onclick="removetd('+value.pros_id+')" data-toggle="tooltip" title="<?php echo "Remove"; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
                                    html += '</td>';
                                html += '</tr >';
                            $('#shift_void_table').append(html);
                            });
                            var rowCount = $('#table_rows_count tr').length -1;
                            $('#selected_count').val(rowCount);
                        }
                       
                    });
                }
            }else{
                alert("Please Select Race");
            }
        }

        function removetd(pros_id){
            $('#id'+pros_id+'').closest("tr").remove();
            var rowCount = $('#table_rows_count tr').length -1;
            $('#selected_count').val(rowCount);
        }

        function shiftracebutton(){
            $("#myModal1").modal("hide");
            $('#myModal2').modal('show');
            var formdata= $('#form_shift_void').serialize();
            $.ajax({
                type: "POST",
                url: 'index.php?route=transaction/handicapping/shiftracebutton&token=<?php echo $token; ?>',
                data: formdata,
                dataType: "json",
                success: function (json1) {
                    $.each(json1, function(i, value) {

                        html = '<tr id="id'+value.pros_id+'">';
                            html += '<td class="text-center"  >'+value.pros_id+'';
                                html += '  <input type="hidden" name=shift_date[' + value.pros_id + '][pros_id] value="'+value.pros_id+'"  class="form-control" />';
                            html += '</td>';
                            html += '<td class="text-center">'+value.race_name+'';
                            html += '</td>';
                            html += '<td class="text-center">'+value.race_type+'';
                            html += '</td>';
                            html += '<td class="text-left">'+value.race_date+'';
                                // html += '<div class="input-group date"  >';
                                //     html += '<input type="date" id="race_date_'+value.pros_id+'" name=shift_date[' + value.pros_id + '][race_date] value="'+value.race_date+'"  placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" class="form-control" />';
                                    // html += '<input type="date" id="race_date_'+value.pros_id+'" name=shift_date[' + value.pros_id + '][race_date] value="'+value.race_date+'"  placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" class="form-control date" />';
                                    // html += '<span class="input-group-btn">';
                                    // html += '<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>';
                                    // html += ' </span>';
                                //     html += '</div>';
                                // html += '</td>';
                           
                        html += '</tr >';
                    $('#shift_table').append(html);
                    });
                }
            });
        }

        // $(document).on('click', '.datepicker', function() {
        //   $( this ).datepicker();
        //  });

        function voidracebutton(){
            var voiddata= $('#form_shift_void').serialize();
            $.ajax({
                type: "POST",
                url: 'index.php?route=transaction/handicapping/voidbutton&token=<?php echo $token; ?>',
                data: voiddata,
                dataType: "json",
                 success: function(json) {
                    $("#myModal1").modal("hide");
                     window.location.reload();
                }
            });
        }

        function shift_date(){
            var formdata= $('#form_shift').serialize();
            $.ajax({
                type: "POST",
                url: 'index.php?route=transaction/handicapping/shiftbutton&token=<?php echo $token; ?>',
                data: formdata,
                dataType: "json",
                 success: function(json) {
                    $("#myModal2").modal("hide");
                    window.location.reload();
                }
            });
        }
        
    </script>
    <script type="text/javascript"></script>



  </script>
    <script type="text/javascript">
        function filter() { 
            var filter_race_name =$('#input-race_name').val();
            var filter_race_date = $('#race_date').val();
            //if(filter_race_name !='' || filter_race_date !=''){
                url = 'index.php?route=transaction/handicapping&token=<?php echo $token; ?>';
                if (filter_race_name) {
                  url += '&filter_race_name=' + encodeURIComponent(filter_race_name);
                }

                if (filter_race_date) {
                    url += '&filter_race_date=' + encodeURIComponent(filter_race_date);
                } 
                window.location.href = url;
           // }
            /* else {
                alert('Please select the filters');
                return false;
            }*/
            
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function(){

       
            $('.date').datetimepicker({
                pickTime: false
            });

            $('.time').datetimepicker({
                pickDate: false
            });

            $('.datetime').datetimepicker({
                pickDate: true,
                pickTime: true
            });
        })
    </script>


    <script type="text/javascript">
        function PopupdataFunction(pros_id){
            $('#popup_table').html('');
            var desc =$('#hidden_race_description_'+pros_id+'').val();
            var name =$('#hidden_race_name_'+pros_id+'').val();
                    //alert(name);
            $.ajax({
                type: "POST",
                url: 'index.php?route=transaction/handicapping/edit1&token=<?php echo $token; ?>&pros_id=' + pros_id,
                dataType: 'json',
                success: function(json) { 
                    console.log(json);
                	$('#myModal').modal('show');
                    var myid=1;
                    $('#race_description').val(desc);
                    $('#race_name').val(name);
                    $('#race_namespan').html(name);
                    html = '';  
                    json.forEach(function(item,index) {
                         $('#par1').html('');
                    html += '<tr id="par'+myid+'">';
                        html += '<td class="text-left"><span id="rating_'+myid+'">'+item.horse_rating+'</span>';
                        html += '</td>';
                         html += '<td class="text-left"  >'+item.horse_name+'';
                        html += '</td>';
                         html += '<td class="text-left"  >';
                        html += '</td>';
                         html += '<td class="text-left"  >';
                        html += '</td>';
                    html += '</tr >';
                   } );
                    $('#popup_table').append(html);
                },
                error: function(){
                   
                    alert("eroor");
                }
            });
            event.preventDefault();
        }
    </script>
    <script type="text/javascript">
        document.getElementById('div_scroll').scrollIntoView();
         document.getElementById('div_scroll_shift_void').scrollIntoView();
    </script>

    <script type="text/javascript">
        function exports(entry_id){
            setTimeout(function(){location.reload()}, 11000);
            url = 'index.php?route=transaction/handicapping/reports&token=<?php echo $token; ?>&entry_id=' + entry_id;
            location = url;
        }
        $('#button-export').on('click', function() {
            setTimeout(function(){location.reload()}, 11000);
            var filter_race_name =$('#input-race_name').val();
            var filter_race_date = $('#race_date').val();
            url = 'index.php?route=transaction/handicapping/report&token=<?php echo $token; ?>';
            if (filter_race_name) {
              url += '&filter_race_name=' + encodeURIComponent(filter_race_name);
            }
            if (filter_race_date) {
                url += '&filter_race_date=' + encodeURIComponent(filter_race_date);
            }
            location = url;
        });

    </script>

    <script type="text/javascript">
        
        $('#input-race_name').autocomplete({
            delay: 300,
            source: function(request, response) {
                $('#pros_id').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/handicapping/autocompleteRace&token=<?php echo $token; ?>&race_name=' +  encodeURIComponent(request),
                        dataType: 'json',
                        success: function(json) {   
                            $('#trainer_codes_id').find('option').remove();
                            response($.map(json, function(item) {
                                return {
                                    label: item.race_name,
                                    value: item.race_name,
                                    pros_id:item.pros_id,
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(item) {
                console.log(item);
                $('#input-race_name').val(item.value);
                $('#pros_id').val(item.pros_id);
                $('.dropdown-menu').hide();
                return false;
            },
        });

    </script>
    
</div>
<?php echo $footer; ?>