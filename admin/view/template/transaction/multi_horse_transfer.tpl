<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a type="submit" form="form-category"  class="btn btn-primary pull-right save" style="margin-left: 10px;">Save</a>
                
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-body">
                <form action="<?php $filter ?>" method="get" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <div class="form-group" >
                        <div class="col-sm-1"></div>
                        <label class="col-sm-2" for="from_trainer"> From Trainer Name <span style="color: red;font-weight: bold">*</span> </label>
                        <div class="col-sm-3">
                            <input type="text" name="from_trainer" id="from_trainer" placeholder="Search Trainer Name" class="form-control">
                            <input type="hidden" name="from_trainer_id" id="from_trainer_id" class="form-control">
                            <input type="hidden" name="from_trainer_lic" id="from_trainer_lic" class="form-control">


                        </div>

                        <label class="col-sm-2" for="from_trainer_code"> From Trainer Code  </label>
                        <div class="col-sm-3">
                            <input type="text" name="from_trainer_code" id="from_trainer_code" placeholder="Search Trainer Code" class="form-control">
                        </div>
                       
                    </div>
                    
                    <div class="form-group horse_table" >
                        
                    </div>
                    <div class="form-group h_div" style="display: flex;align-items: center;display: none;">
                        <h2 style="margin-left: 35%;" class="htag"></h2>
                    </div>

                    <div class="form-group" >
                        <div class="col-sm-1"></div>
                        
                        <label class="col-sm-2" for="to_trainer"> To Trainer Name <span style="color: red;font-weight: bold">*</span> </label>
                        <div class="col-sm-3">
                            <input type="text" name="to_trainer" id="to_trainer" placeholder="Search Tariner Name" class="form-control">
                            <input type="hidden" name="to_trainer_id" id="to_trainer_id" class="form-control">
                            <input type="hidden" name="to_trainer_lic" id="to_trainer_lic" class="form-control">
                        </div>

                        <label class="col-sm-2" for="to_trainer_code"> To Trainer Code  </label>
                        <div class="col-sm-3">
                            <input type="text" name="to_trainer_code" id="to_trainer_code" placeholder="Search Trainer Code" class="form-control">
                        </div>

                        
                    </div>

                    <div class="form-group" >
                        <div class="col-sm-1"></div>
                        <label class="col-sm-2" for="date_of_charge"> Date of Charge</label>
                        <div class="col-sm-3">
                            <div class="input-group date input-date_of_charge">
                                <input type="text" name="date_of_charge" value="<?php echo date('d-m-Y') ?>" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="date_left_trainer" class="form-control" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                        <label class="col-sm-2" for="arrival_time"> Arrival Time</label>
                        <div class="col-sm-3">
                            <select name="arrival_time_trainers" id="arrival_time_trainer" class="form-control">
                                <?php foreach ($arrival_time_trainer as $arrival_time_value) { ?>
                                    <option value="<?php echo $arrival_time_value; ?>"><?php echo $arrival_time_value ?></option>
                                    
                                <?php } ?>
                            </select>
                        </div>

                        
                    </div>

                     <div class="form-group" >
                        <div class="col-sm-1"></div>
                        <label class="col-sm-2" for="narration"> Narration</label>
                        <div class="col-sm-3">
                            <textarea type="text" name="narration" id="narration" placeholder="Narration" class="form-control"></textarea>
                        </div>
                        <label class="col-sm-2" id="lbl_uc" for="undertaking_charge"> Undertaking Charge</label>
                        <div class="col-sm-2">
                            <div class="checkbox" >
                                <label>
                                    <input type="checkbox"  name="undertaking_charge" value="1"  id="undertaking_charge"  class="form-control" tabindex="5" />
                                </label>
                            </div>

                        </div>

                        <label class="col-sm-1" for="provisional"> Provisional</label>
                        <div class="col-sm-1">
                            <div class="checkbox" >
                                <label>
                                    <input type="checkbox"  name="provisional"  id="provisional"  value="1" class="form-control" tabindex="5" />
                                </label>
                            </div>
                        </div>
                    </div>
                    
                    <a id="sv"  style="margin-left:50%;display: none;" class="btn btn-primary save">Save</a>
                </form>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
  
</style>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
        $('.date').datetimepicker({
            pickTime: false
        });

        $(document).ready(function(){
            $('#from_trainer').focus();
        })

        /* Search For From Trainer And Appending Table of All Records*/
        $('#from_trainer').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/multi_horse_transfer/autocompleteFromTrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {
                        console.log(json);   
                            response($.map(json.final_trainers, function(item) {
                                return {
                                    label: item.name,
                                    value: item.name,
                                    trainer_id : item.id,
                                    lic_type : item.license_type
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#from_trainer').val(ui.item.label );
                $('#from_trainer_id').val(ui.item.trainer_id);
                $('#from_trainer_lic').val(ui.item.lic_type);

                $('.dropdown-menu').hide();
                $('#to_trainer').focus();
                if(ui.item.trainer_id != ''){
                $.ajax({
                        url: 'index.php?route=transaction/multi_horse_transfer/allTrainerHorses&token=<?php echo $token; ?>&trainer_id=' +  encodeURIComponent(ui.item.trainer_id),
                        dataType: 'json',
                        cache: false,
                        success: function(json) {
                            $('.htag').html('');
                            $('.horse_table').html('');
                            if(json.status == 1 && json.owners_present == 1){
                                 $(".h_div").hide();
                                 $('#sv').show();
                                $('.horse_table').append(json.html);
                            } else {
                                $(".h_div").show();
                                 $('#sv').hide();

                                $('.htag').append("Horse Not Assign To This Trainer");
                            }
                        }
                    }); 
                }
                $('#ownership_type_id').focus();
                return false;
            },
        });

        $('#from_trainer_code').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/multi_horse_transfer/autocompleteFromTrainer&token=<?php echo $token; ?>&trainer_code=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {
                        console.log(json);   
                            response($.map(json.final_trainers, function(item) {
                                return {
                                    label: item.trainer_code,
                                    value: item.trainer_code,
                                    trainer_id : item.id,
                                    lic_type : item.license_type,
                                    name : item.name

                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#from_trainer').val(ui.item.name );
                $('#from_trainer_id').val(ui.item.trainer_id);
                $('#from_trainer_lic').val(ui.item.lic_type);
                $('#from_trainer_code').val(ui.item.value);

                $('.dropdown-menu').hide();
                $('#to_trainer').focus();
                if(ui.item.trainer_id != ''){
                $.ajax({
                        url: 'index.php?route=transaction/multi_horse_transfer/allTrainerHorses&token=<?php echo $token; ?>&trainer_id=' +  encodeURIComponent(ui.item.trainer_id),
                        dataType: 'json',
                        cache: false,
                        success: function(json) {
                            $('.htag').html('');
                            $('.horse_table').html('');
                            if(json.status == 1 && json.owners_present == 1){
                                 $(".h_div").hide();
                                 $('#sv').show();
                                $('.horse_table').append(json.html);
                            } else {
                                $(".h_div").show();
                                 $('#sv').hide();

                                $('.htag').append("Horse Not Assign To This Trainer");
                            }
                        }
                    }); 
                }
                $('#ownership_type_id').focus();
                return false;
            },
        });


        /* Search For To Owner Autocomplete */
        $('#to_trainer').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/multi_horse_transfer/autocompleteFromTrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {
                        console.log(json);   
                            response($.map(json.final_trainers, function(item) {
                                return {
                                    label: item.name,
                                    value: item.name,
                                    trainer_id : item.id,
                                    lic_type : item.license_type
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#to_trainer').val(ui.item.label );
                $('#to_trainer_id').val(ui.item.trainer_id);
                $('#to_trainer_lic').val(ui.item.lic_type);
                let from_trainer_lic = $('#from_trainer_lic').val();
                let to_trainer_lic = ui.item.lic_type;
                
                if( (from_trainer_lic == 'A' || from_trainer_lic == 'B') && (to_trainer_lic == 'A' || to_trainer_lic == 'B') ){
                    $('#undertaking_charge').hide();
                    $('#lbl_uc').hide();
                } else {
                    $('#undertaking_charge').show();
                    $('#lbl_uc').show();

                }
                $('.dropdown-menu').hide();
            }
        });


        $('#to_trainer_code').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/multi_horse_transfer/autocompleteFromTrainer&token=<?php echo $token; ?>&trainer_code=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {
                        console.log(json);   
                            response($.map(json.final_trainers, function(item) {
                                return {
                                    label: item.trainer_code,
                                    value: item.trainer_code,
                                    trainer_id : item.id,
                                    lic_type : item.license_type,
                                    name : item.name

                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#to_trainer').val(ui.item.name );
                $('#to_trainer_id').val(ui.item.trainer_id);
                $('#to_trainer_lic').val(ui.item.lic_type);
                $('#to_trainer_code').val(ui.item.value);

                let from_trainer_lic = $('#from_trainer_lic').val();
                let to_trainer_lic = ui.item.lic_type;
                
                if( (from_trainer_lic == 'A' || from_trainer_lic == 'B') && (to_trainer_lic == 'A' || to_trainer_lic == 'B') ){
                    $('#undertaking_charge').hide();
                    $('#lbl_uc').hide();
                } else {
                    $('#undertaking_charge').show();
                    $('#lbl_uc').show();

                }
                $('.dropdown-menu').hide();
            }
        });

        /* To Save Data In table */

        $('.save').click(function(){
            datasss =  $('#form-category').serialize();
            let status = 1;
           /* Checking Checked Value to Transfer */
            $('.checkbox_value:checked').each(function(){
                idss = $(this).attr('id');
                check_status = $('#'+idss).prop('checked');
                if(check_status == true){
                    status = 0;
                }
            });
            let from_t = $("#from_trainer").val();
            let to_t = $("#to_trainer").val();
            /* Validation for trainers */
            if(from_t.trim().length > 0 && to_t.trim().length > 0 && status == 0){
                $.ajax({
                    method: "POST",
                    url: 'index.php?route=transaction/multi_horse_transfer/saveAllDatas&token=<?php echo $token; ?>',
                    dataType: 'json',
                    data: datasss,
                    success: function(json) {
                        if(json.status == 1){
                            path = 'index.php?route=transaction/multi_horse_transfer&token=<?php echo $token; ?>';
                            window.location.href = path;
                        } else {
                            alert('Please Select Trainer To Transfer');
                        }
                    }
                });
            } else {
                if(status == 1){
                    alert("Please Select at least on horse");
                    return false;
                } else {
                    alert("Please Select Trainer To Transfer");
                    return false;
                }
            }
        });
        
        /* To Checked All Option */
        $(document).on("change",".all_checked", function() {
            $(".checkbox_value").prop("checked", this.checked);
        });    
    </script>
</div>
<?php echo $footer; ?>