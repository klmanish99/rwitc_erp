<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-user"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <div class="form-group" >
                        <label class="col-sm-2 control-label" for="input-horse"><b style="color: red;"> * </b> <?php echo "Horse Name:"; ?></label>
                        <div class="col-sm-3">
                            <input type="text"  name="filterHorseName" id="filterHorseName" value="<?php echo $horse_name ?>" placeholder="Horse Name" class="form-control" />
                            <input type="hidden" name="filterHorseId" id="filterHorseId" value="<?php echo $horse_id ?>"  class="form-control">
                        </div>
                        <label class="col-sm-2 control-label" for="input-trainer"><?php echo "Trainer Name :"; ?></label>
                        <div class="col-sm-3">
                            <input type="hidden" id="check_provisonal" value="<?php echo $check_provisonal ?>" >
                            <input type="text" name="trainer_name"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $trainer_name ?>" placeholder="Trainer Name" id="input-trainer_name" class="form-control"  readonly="readonly"/>
                            <input type="hidden" name="trainer_id" value="<?php echo $trainer_id ?>" id="trainer_id" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group" style="border-top: 0;">
                        <label class="col-sm-2 control-label" for="input-trainer_name"><?php echo "Ownership Type:"; ?></label>
                        <div class="col-sm-3">
                            <select name="ownership_type_name" id="ownership_type_id" class="form-control ent-evnt">
                                <option value="0" >Please Select</option>    
                                <?php foreach ($ownerships_types as $o_typekey => $o_typevalue) { ?>
                                    <option value="<?php echo $o_typevalue; ?>" ><?php echo $o_typevalue; ?></option>
                                <?php } ?>
                            </select>
                            <span style="display: none;color: red;font-weight: bold;" id="error_ownership_type" class="error"><?php echo 'Please Select Ownership Type'; ?></span>
                        </div>
                        
                        <div class="col-sm-2 text-center">
                            <label>Provisional Ownership</label><br>
                            <?php if($arrival_charge == 1){ ?>
                                <input id="check_provi_owner" value="0" type="checkbox" name="check_provi_owner" checked="checked" >
                            <?php } else { ?>
                                <input id="check_provi_owner" value="0" type="checkbox" name="check_provi_owner" >
                            <?php } ?>
                        </div>

                        <div class="col-sm-2 text-center">
                            <select name="charge_type" id="charge_type_id" class="form-control">
                                <option value="0" >Please Select Charges</option>    
                                <?php foreach ($allCharges as $a_typekey => $a_typevalue) { 
                                    if($a_typevalue == $charge_type){ ?>
                                        <option value="<?php echo $a_typevalue; ?>"  selected="selected"><?php echo $a_typevalue; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $a_typevalue; ?>" ><?php echo $a_typevalue; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <span style="display: none;color: red;font-weight: bold;" id="error_charge_type" class="error"><?php echo 'Please Select Charge Type'; ?></span>
                        </div>

                        <div class="col-sm-2">
                            <a id="chng_color"  data-toggle="tooltip" title="Change Color" class="btn btn-primary"><i class="fa fa-plus">Change Color</i></a>
                        </div>
                    </div>

                    <div class="table-responsive owner_datas" style="border-top: 0;">
                    </div>
                    
                    <div class="form-group selectsharediv" style="display: none;border-top: 0;">
                        <label class="col-sm-2 control-label" for="input-share"><?php echo "Selected Shares :"; ?></label>
                        <div class="col-sm-3">
                            <input type="text"  name="total_share" id="total_share" value="" placeholder="Selected Shares" class="form-control ent-evnt" readonly />
                            <input type="hidden" name="pre_total_share" id="pre_total_share" value="" class="form-control ent-evnt" />
                        </div>
                        <div class="col-sm-1" style="padding-top: 7px;font-size: 18px;padding-left: 0px;">
                            <b>%</b>
                        </div>
                        <div class="col-sm-3">
                            <a id="addnewowner" data-toggle="tooltip" title="Add Owner" class="btn btn-primary"><i class="fa fa-plus">Add Owner</i></a>
                        </div>
                    </div>
                    
                    <div class="form-group sale-owner-div" >
                    </div>
                    
                    <div class="form-group from-div" style="display: none;"> 
                        <div class="form-group " style="border-top: 0; height: 100%;display: flex;justify-content: center;align-items: center;" >
                            <label class="col-sm-1 control-label" for="input-fromdate" required><b style="color: red;"> * </b> From Date</label>
                            <div class="col-sm-3" >
                                <input type="text" name="fromdate" value="<?php echo $fromdate ?>"  id="fromdate"  class="form-control" required/>
                            </div>
                            <label class="col-sm-1 control-label" for="input-todate"> <b class="todate_val" style="color: red;display:none;"> * </b> To Date</label>
                            <div class="col-sm-3" >
                                <input type="text" name="todate" value=""  id="todate"  class="form-control" />
                            </div>
                            <div class="col-sm-2" style="display:none;">
                                <label></label>
                                <div class="checkbox" >
                                    <input type="checkbox" name="date_check"  id="lease"  class="lease" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" for="input-todate">Remark</label>
                            <div class="col-sm-7" >
                                <textarea type="text" name="remark" value=""  id="remark"  class="form-control" ></textarea>
                            </div>
                        </div>

                        <div class="form-group " style="border-top: 0; height: 100%;display: flex;justify-content: center;align-items: center;" >
                            <a id="save" class="col-sm-2 btn btn-primary">Next</a>
                        </div>
                    </div>
                    <input type="hidden" id="last_owner_id" value="0" class="form-control last_owner_id" />
                    <input type="hidden" id="selected_pre_owners" value="0" class="form-control selected_pre_owners" />
                    <input type="hidden" id="all_selected_pre_owners" value="0" class="form-control all_selected_pre_owners" />
                    <input type="hidden" id="newuser_status" value="0" class="form-control newuser_status" />
                    <input type="hidden" id="check_contingency" value="" class="form-control" />
                    <input type="hidden" id="contingency_percentage" value="" class="form-control" />
                    <input type="hidden" id="contingency_amounts" value="" class="form-control" />
                    <input type="hidden" id="contingency_wgs" value="" class="form-control" />
                    <input type="hidden" id="contingency_wns" value="" class="form-control" />
                    <input type="hidden" id="contingency_place" value="" class="form-control" />
                    <input type="hidden" id="contingency_million_over" value="" class="form-control" />
                    <input type="hidden" id="contingency_million_amt" value="" class="form-control" />
                    <input type="hidden" id="contingency_gradeone" value="" class="form-control" />
                    <input type="hidden" id="contingency_grade_two" value="" class="form-control" />
                    <input type="hidden" id="contingency_grade_three" value="" class="form-control" />
                    <input type="hidden" id="hidden_lease_date_start" value="" class="form-control" />
                    <input type="hidden" id="hidden_lease_date_end" value="" class="form-control" />

                    <div class="mod" >
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contingency Details</h4>
                </div>
                <div class="modal-body" style="height: 240px;">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
    .blink {
        -webkit-animation: blink 1s step-end infinite;
                animation: blink 1s step-end infinite;
    }
    @-webkit-keyframes blink { 50% { visibility: hidden; }}
            @keyframes blink { 50% { visibility: hidden; }}
    </style>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">

        $( "#fromdate, #todate" ).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
        });
        
        $('#save').click(function(){
            wrong_enter = 0;
            improper_amount = 0;
            $('.percentage').each(function() {
                if($(this).val() < 10 ){
                    wrong_enter = 1;
                } else {
                    if (($(this).val() % 5 != 0) && ($(this).val() != 33.33)){
                        improper_amount = 1;
                    } 
                }
            });

            wrong_owner_enter = 0;
            $('.nowners_id').each(function() {
                if($(this).val() == '' ){
                    wrong_owner_enter = 1;
                }
            });

            if( wrong_enter == 1 || wrong_owner_enter == 1 || improper_amount == 1){
                if( wrong_enter == 1){
                    alert('Share Should have More Then 10 Percentage');
                } 
                if(wrong_owner_enter){
                    alert('Please Select Proper Owner Name');
                }
                if( improper_amount == 1){
                    alert('Please Enter Proper Share');
                } 
                return false;
            }

            total_share = parseInt($('#total_share').val());
            type = $('#ownership_type_id').val();
            selected_pre_owners = $('#selected_pre_owners').val();

            fromdate = $('#fromdate').val();
            todate = $('#todate').val();
            lease_start_datess = $('#hidden_lease_date_start').val();
            lease_end_datess = $('#hidden_lease_date_end').val();
            
            status = 0;
            if(type == 'Sub Lease'){
                if(todate == undefined || todate == '' || fromdate == undefined || fromdate == ''){ 
                    status = 1; 
                    alert('E05: Please Select Proper Date');    
                    return false;   
                }
                from_date = fromdate.split('-');
                to_date = todate.split('-');
                fromdate = from_date[2] + "-" + from_date[1] + "-" + from_date[0];
                todate = to_date[2] + "-" + to_date[1] + "-" + to_date[0];

                var start_date = new Date(fromdate);
                var end_date = new Date(todate);

                var lease_start_date = new Date(lease_start_datess);
                var lease_end_date = new Date(lease_end_datess);

                if(start_date > lease_end_date){
                    status = 1;
                    alert('E01: Please Select Proper Date');
                    return false;
                }

                if(start_date < lease_start_date){
                    status = 1;
                    alert('E02: Please Select Proper Date');
                    return false;
                }  

                if(end_date < lease_start_date){
                    status = 1;
                    alert('E03: Please Select Proper Date');
                    return false;
                }

                if(end_date > lease_end_date){
                    status = 1;
                    alert('E04: Please Select Proper Date');
                    return false;
                }
            }
            
            var prov_owner = $("#check_provi_owner").val();
            if (prov_owner == 1) {
                $('form#form-category').submit();
            } else if( (total_share == 0) && (type == 'Sale') && (fromdate != '')){
                $('form#form-category').submit();
            } else if((type == 'Lease' || type == 'Sub Lease') && selected_pre_owners == 1 && todate != '') {
                if(type == "Sub Lease" && status == 0){
                    $('form#form-category').submit();
                } else {
                    $('form#form-category').submit();
                }
            } else if((type == 'Lease' || type == 'Sub Lease') && selected_pre_owners != 1 && total_share == 0 && todate != '') {
                if(type == "Sub Lease" && status == 0 ){
                    $('form#form-category').submit();
                } else {
                    $('form#form-category').submit();
                }
            } else {
                alert("Please check Share or Date !");
                return false;
            }
        });


        $(document).ready(function(){
            gt_id = '<?php echo $gt_id ?>';
            if(gt_id == 0){
                $('#filterHorseName').focus();
            } 
        });

        $('#chng_color').click(function(){
            tokan = '<?php echo $token; ?>';
            horse_id = $('#filterHorseId').val();
            url = '<?php echo HTTPS_SERVER ?>';
            if(horse_id != ''){
                window.open(url+"index.php?route=transaction/horse_datas_link&token="+tokan+"&horse_id="+horse_id+"&color=1", "_self");
            } else {
                alert('Please Select Horse Name');
                return false;
            }
        });

        $('#filterHorseName').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#filterHorseId').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/ownership_shift_module/autocompleteHorse&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.horse_name,
                                    value: item.horse_name,
                                    horse_id:item.horse_id,
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#filterHorseName').val(ui.item.value);
                $('#filterHorseId').val(ui.item.horse_id);
                $('.dropdown-menu').hide();
                if(ui.item.horse_id != ''){
                $.ajax({
                        url: 'index.php?route=transaction/ownership_shift_module/autoHorseToTrainer&token=<?php echo $token; ?>&horse_id=' +  encodeURIComponent(ui.item.horse_id),
                        dataType: 'json',
                        cache: false,
                        success: function(json) {
                            if(json){
                                $.each(json, function (i, item) {
                                    $('#input-trainer_name').val(item.trainer_name);
                                    $('#trainer_id').val(item.trainer_id);
                                });
                            }
                        }
                    }); 
                }
                $('#ownership_type_id').focus();
                return false;
            },
        });

        $( "#ownership_type_id" ).change(function(){
            var ownership_type_id =  $(this).val();
            var filter_hourse_id = $('#filterHorseId').val();
            if((filter_hourse_id != '') && (ownership_type_id != '')){
                $.ajax({
                    url: 'index.php?route=transaction/ownership_shift_module/getOwnerData&token=<?php echo $token; ?>&filter_hourse_id='+filter_hourse_id+'&ownership_type_id='+ownership_type_id,
                    dataType: 'json',
                    success: function(json) {
                        if(json.new_user == 0 && ownership_type_id != 0){
                            $('.ownertable').remove();
                            $('#total_share').val('');
                            $('#pre_total_share').val('');
                            $('.owner_datas').append(json.html);
                        } else {
                            $('.ownertable').remove();
                            if(ownership_type_id == "Sale"){
                                $('.selectsharediv').css("display", "");
                                $('#total_share').val(100);
                                $('#pre_total_share').val(100);
                            } else {
                                $('.selectsharediv').css("display", "none");
                                $('#total_share').val('');
                                $('#pre_total_share').val('');
                            }
                            $('#newuser_status').val(1);
                        }
                    }
                }); 
            } else {
                $('.ownertable').remove();
                return false;
            }

            if(ownership_type_id == 'Lease' || ownership_type_id == 'Sub Lease'){
                $('.todate_val').css("display", "");
            } else {
                $('.todate_val').css("display", "none");
            }
        });

        $(document).ready(function(){
            var ownership_type_id =  $('#ownership_type_id').val();
            //alert(ownership_type_id);
            var filter_hourse_id = $('#filterHorseId').val();
            if((filter_hourse_id != '') && (ownership_type_id != '')){
                $.ajax({
                    url: 'index.php?route=transaction/ownership_shift_module/getOwnerData&token=<?php echo $token; ?>&filter_hourse_id='+filter_hourse_id+'&ownership_type_id='+ownership_type_id,
                    dataType: 'json',
                    success: function(json) {
                        if(json.new_user == 0 && ownership_type_id != 0){
                            $('.ownertable').remove();
                            $('#total_share').val('');
                            $('#pre_total_share').val('');
                            $('.owner_datas').append(json.html);
                        } else {
                            $('.ownertable').remove();
                            if(ownership_type_id == "Sale"){
                                $('.selectsharediv').css("display", "");
                                $('#total_share').val(100);
                                $('#pre_total_share').val(100);
                            } else {
                                $('.selectsharediv').css("display", "");
                                $('#total_share').val(100);
                                $('#pre_total_share').val(100);
                            }
                            $('#newuser_status').val(1);
                        }
                    }
                }); 
            } else {
                $('.ownertable').remove();
                return false;
            }

            if(ownership_type_id == 'Lease' || ownership_type_id == 'Sub Lease'){
                $('.todate_val').css("display", "");
            } else {
                $('.todate_val').css("display", "none");
            }
        });

        // contingency popup details 

        $(document).on("click",".modalopen",function() {
            horse_owner_idssss = $(this).attr('id');
            horse_owner_id = horse_owner_idssss.split('_');
            $.ajax({
                url: 'index.php?route=transaction/ownership_shift_module/getContaingencyData&token=<?php echo $token; ?>&horse_owner_id='+horse_owner_id[1],
                dataType: 'json',
                success: function(json) {   
                    $('.modal-body').html('');
                    $('.modal-body').append(json);
                    $('#myModal').modal('show'); 
                }
            });
        });

        // owner Partners Detail Popup

        $(document).on("click",".partners",function() {
            parent_owner_idss = $(this).attr('id');
            parent_owner_id = parent_owner_idss.split('_');
            horse_id = $('#filterHorseId').val();
            $.ajax({
                url: 'index.php?route=transaction/ownership_shift_module/getPartnersData&token=<?php echo $token; ?>&parent_owner_id='+parent_owner_id[1]+'&horse_id='+horse_id,
                dataType: 'json',
                success: function(json) {   
                    $('#partners_data_'+parent_owner_id[1]).remove();
                    $('.mod').append(json);
                    $('#partners_data_'+parent_owner_id[1]).modal('show'); 
                }
            });
        });
   
        $(document).on("change",".checkbox_value",function() {
            var idss = $(this).attr('id');
            service = $('#'+idss).prop('checked');
            if(service == true){
                crnt_id = idss.split('_');
                curr_id = parseInt(crnt_id[3]);
                owner_increment_id = $("#fro_ow_ck_"+curr_id).val();
                $.ajax({
                    url: 'index.php?route=transaction/ownership_shift_module/getContaingencyDataForHidden&token=<?php echo $token; ?>&horse_owner_id='+owner_increment_id,
                    dataType: 'json',
                    success: function(json) {
                        if(json.length != 0){
                            if((json.ownership_type == 'Sale' || json.ownership_type == 'Lease' || json.ownership_type == 'Sub Lease') && (json.contengency == '1')){
                                $('#check_contingency').attr('value', json.contengency);
                                $('#contingency_percentage').attr('value', json.cont_percentage);
                                $('#contingency_amounts').attr('value', json.cont_amount);
                                $('#contingency_wgs').attr('value', json.win_gross_stake);
                                $('#contingency_wns').attr('value', json.win_net_stake);
                                $('#contingency_place').attr('value', json.cont_place);
                                $('#contingency_million_over').attr('value', json.millionover);
                                $('#contingency_million_amt').attr('value', json.millionover_amt);
                                $('#contingency_gradeone').attr('value', json.grade1);
                                $('#contingency_grade_two').attr('value', json.grade2);
                                $('#contingency_grade_three').attr('value', json.grade3);
                                
                                if ($("#"+idss).is(':checked')) {
                                    $('.checkbox_value').attr('onclick', 'return false');
                                    $('.checkbox_value').prop('checked', false); 
                                    $("#"+idss).removeAttr("onclick");
                                    $("#"+idss).prop('checked', true);
                                } 

                                if (json.ownership_type == 'Lease'){
                                    $('#hidden_lease_date_start').attr('value', json.date_of_ownership);
                                    $('#hidden_lease_date_end').attr('value', json.end_date_of_ownership);
                                }
                               
                            } else if (json.ownership_type == 'Lease' || json.ownership_type == 'Sub Lease'){
                                $('#hidden_lease_date_start').attr('value', json.date_of_ownership);
                                $('#hidden_lease_date_end').attr('value', json.end_date_of_ownership);
                            }
                        }
                        calculation_checkbox_table();
                    }
                });
                
            } else {
                $('.checkbox_value').removeAttr('onclick');
                $('#check_contingency').attr('value', '');
                $('#contingency_percentage').attr('value', '');
                $('#contingency_amounts').attr('value', '');
                $('#contingency_wgs').attr('value', '');
                $('#contingency_wns').attr('value', '');
                $('#contingency_place').attr('value', '');
                $('#contingency_million_over').attr('value', '');
                $('#contingency_million_amt').attr('value', '');
                $('#contingency_gradeone').attr('value', '');
                $('#contingency_grade_two').attr('value', '');
                $('#contingency_grade_three').attr('value', '');

                $('#hidden_lease_date_start').attr('value', '');
                $('#hidden_lease_date_end').attr('value', '');
                calculation_checkbox_table();
            }
            
        });

        function calculation_checkbox_table(){
            var total = 0;
            var selected_pre_owners = 0;
            var all_selected_owners = '';
            $('.checkbox_value:checked').each(function(){
                idss = $(this).attr('id');
                ids = idss.split('_');
                id = ids[3];
                //total += isNaN(parseInt($('#owner_percentage_'+id).val())) ? 0 : parseInt($('#owner_percentage_'+id).val());
                var check_num = isNaN(parseInt($('#owner_percentage_'+id).val())) ? 0 : $('#owner_percentage_'+id).val();
                 if(check_num % 1 == 0){
                     total += parseInt(check_num);
                } else {
                     total += parseFloat(check_num);
                }
                selected_pre_owners++; 
                all_selected_owners += $('#to_owner_id_'+id).val()+','; 
            });   
            if(total % 1 != 0){
                if(total == 99.99){
                    total = Math.round(total);
                }
            } 
            $(".selectsharediv").show();
            $("#selected_pre_owners").val(selected_pre_owners);
            $("#all_selected_pre_owners").val(all_selected_owners);
            $("#total_share").val(total);
            $("#pre_total_share").val(total);
        }

        $(document).on('keydown', '.ent-evnt', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13){
                var class_name = $(this).attr('class');
                var id = $(this).attr('id');
                incr = 1; 
                new_user_stat = $("#newuser_status").val();
                if(new_user_stat == 0){
                    if(id == 'ownership_type_id'){
                        $('#fro_ow_ckper_1').focus();
                    }
                } else {
                    $('#total_share').focus();
                }

                if(class_name == 'checkbox_value ent-evnt'){
                    idz = $('.checkbox_value').last().attr('id');//$( ".checkbox_value:last-child" ).attr( "id" );
                    crnt_id = id.split('_');
                    curr_id = parseInt(crnt_id[3]);
                    next_id = curr_id + 1;
                    var index = $('.checkbox_value ent-evnt').index(this)+ 1;
                    next_code_by_index = $("#fro_ow_ckper_"+next_id).eq(index).attr("id");
                    if(next_code_by_index != undefined){
                        incr = incr + 1;
                    }
                    $('#'+next_code_by_index).focus();
                    if(id == idz){
                        $('#total_share').focus();
                    }
                }

                if(id == 'total_share'){
                    ows_type = $('#ownership_type_id').val();
                    if((new_user_stat == 0 || new_user_stat == 1) && ows_type == 'Sale') {
                        $( "#addnewowner" ).trigger( "click" );
                    } else {
                        if(new_user_stat == 1){
                            alert("Sorry You Can not give horse on lease please add ownership first ");
                            return false;
                        } else {
                            $( "#addnewowner" ).trigger( "click" );

                        }
                    }
                }
            }
        });

        //add new owner sale option starts here Add btn

        $('#addnewowner').click(function(){
            amt = $('#total_share').val();
            type = $('#ownership_type_id').val();
            new_user_status = $("#newuser_status").val();
            check_contingency = $("#check_contingency").val();

            if(amt > 9){
                var last_owner_id = $('.last_owner_id').val();
                if(last_owner_id != 0 ){
                    lo_id = parseInt(last_owner_id) + 1;
                } else {
                    lo_id = 1;
                }
                html = '';
                html += '<div class="form-group parent_div_'+lo_id+'">';
                    html += '<div class="form-group ">';
                        html += '<div class="col-sm-2">';
                        html += '</div>';
                        html += '<div class="col-sm-3">';
                            html += '<input type="text"  name="new_owners['+lo_id+'][ownername]" id="ownername_'+lo_id+'" value="" class="form-control nowners" /><br>';
                            html += '<input type="hidden"  name="new_owners['+lo_id+'][ownername_id]" id="ownername_id_'+lo_id+'" value="" class="form-control nowners_id" />';
                            html += '<span id="parent_detail_id_'+lo_id+'"></span>';

                        html += '</div>';
                        html += '<div class="col-sm-1">';
                            html += '<input type="number"  name="new_owners['+lo_id+'][percentage]" id="percentage_'+lo_id+'" value="" class="form-control percentage" min="1" />';
                        html += '</div>';
                        html += '<div class="col-sm-1" style="padding-top: 7px;font-size: 16px;padding-left: 0px;">';
                            html +=  '<span>%</span>';
                        html += '</div>';
                        if(new_user_status == 0){
                            html += '<div class="col-sm-2" style="text-align: center">';
                                html += '<label>Contingency</label>';
                                html += '<div class="checkbox" style="padding-top:0px">';
                                html += '<input type="checkbox" name="new_owners['+lo_id+'][contengency]"  id="contengency_'+lo_id+'"  class="contengency"  />';
                                html += '</div>';
                            html += '</div>';
                        }
                        
                        html += '<div class="col-sm-4">';
                        html += '</div>';
                        
                    html += '</div>';
                html += '</div>';
                $('.sale-owner-div').append(html);
                $('.last_owner_id').val(lo_id);
                $('#ownername_'+lo_id).focus();

                var prov_owner = $("#check_provi_owner").val();
                if (prov_owner == 1) {
                    $('#percentage_'+lo_id).val();
                }

                if(check_contingency == 1){$('#contengency_'+lo_id).trigger("click" ); }
                
                $('.from-div').show();

            } else {
                alert('No Share Availabe To Add New Owner');
            }
        });


        $(document).on('keydown', '.nowners, .percentage, .contengency', function(e) {
            var call_id = $(this).attr('id');
            all_id = call_id.split('_');
            added_ids = '';
             $('.nowners_id').each(function(){
                added_ids += $(this).val()+',';
                    
             });
               
            $('.nowners').autocomplete({
                delay: 500,
                source: function(request, response) {
                    horse_ids = $('#filterHorseId').val();
                    all_pre_owners = $('#all_selected_pre_owners').val();
                    if(request != ''){
                        $.ajax({
                            url: 'index.php?route=transaction/ownership_shift_module/autocompleteOwners&token=<?php echo $token; ?>&owner_name=' +  encodeURIComponent(request.term),
                            dataType: 'json',
                            data: {
                                pre_owners: all_pre_owners,
                                call_id : all_id[1],
                                horse_ids : horse_ids,
                                added_ids: added_ids

                            },
                            success: function(json) {   
                                response($.map(json, function(item) {
                                    return {
                                        label: item.owner_name,
                                        value: item.owner_name,
                                        owner_id:item.owner_id,
                                        status : item.status,
                                    }
                                }));
                            }
                        });
                    }
                }, 
                select: function(event, ui) {
                    var id = $(this).attr('id');
                    on_id = id.split('_');
                    $('#'+id).val(ui.item.value);
                    if(ui.item.status == 1){
                        $('#parent_detail_id_'+on_id[1]).append('<a style="cursor: pointer;color: red" id="parentid_'+ui.item.owner_id+'"  class="partners" ><span class="blink"> Partner Details </span></a>');
                        $('#parent_detail_id_'+on_id[1]).show();
                    }
                    $('#ownername_id_'+on_id[1]).val(ui.item.owner_id);
                    $('#percentage_'+on_id[1]).focus();
                    $('#percentage_'+on_id[1]).select();

                },
            });

            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13){
                var class_name = $(this).attr('class');
                console.log(class_name);
                if(class_name == 'form-control percentage'){
                    var id = $(this).attr('id');
                    per_id = id.split('_');
                    //$('#contengency_'+per_id[1]).focus();
                    remaining_shares = $('#total_share').val();
                    pre_remaining_shares = $('#pre_total_share').val();

                    if(remaining_shares > 9){
                        //alert('iii');
                        if($(this).val() > 9 ){
                            if (($(this).val()% 5 != 0) && ($(this).val() != 33.33)  ){
                                
                                alert('Please Enter Proper Share');
                                return false;
                            }
                            if($(this).val() == 33.33){
                                if(pre_remaining_shares != 100){
                                    alert('Please Enter Proper Share');
                                    return false;
                                }
                            }
                            $( "#addnewowner" ).trigger( "click" );
                        } else {
                            alert('Share Should have More Then 10 Percentage');
                            return false;
                        }
                    } else {
                        alert('No Share Availabe To Add New Owner');
                    }
                }
            }
        });

        $(document).on('change', '.contengency, .millionover', function(){
            var class_name = $(this).attr('class');
            var idss = $(this).attr('id');
            idz = idss.split('_');
            check_status = $('#'+idss).prop('checked');
            if(class_name == 'contengency'){
                if(check_status == true){
                    if(parseInt(idz[1]) != 1){
                        copyParentData(idss);
                    } else {
                        allDataDiv(idss);
                    }
                }  else {
                    $(".all-data-div_"+idz[1]).remove();
                }
            }
        
            if(class_name == 'millionover'){
                if(check_status == true){
                    $(".mo-rs-div_"+idz[1]).show();
                }  else {
                    $(".mo-rs-div_"+idz[1]).hide();
                }
            }
        });


    function allDataDiv(idzz){
        dataa_id = idzz.split('_');
        id = dataa_id[1];
        check_contingency = $("#check_contingency").val() || 0;
        if(check_contingency == 1){
            check_contingency = $('#check_contingency').val();
            contingency_percentage = $('#contingency_percentage').val();
            contingency_amounts = $('#contingency_amounts').val();
            contingency_wgs = $('#contingency_wgs').val();
            contingency_wns = $('#contingency_wns').val();
            contingency_place = $('#contingency_place').val();
            contingency_million_over = $('#contingency_million_over').val();
            contingency_million_amt = $('#contingency_million_amt').val();
            contingency_gradeone = $('#contingency_gradeone').val();
            contingency_grade_two = $('#contingency_grade_two').val();
            contingency_grade_three = $('#contingency_grade_three').val();

           html = '';
            html += '<div class="form-group all-data-div_'+id+'">';
                html += '<div class="form-group">';
                    html += '<div class="row">';
                        html += '<label class="col-sm-2 control-label">Percentage</label>';
                        html += '<div class="col-sm-2">';
                            if(contingency_percentage != 0){
                                html += '<input type="text"  name="new_owners['+id+'][per]" id="per_'+id+'" value="'+contingency_percentage+'" class="form-control pre" />';
                            } else {
                                html += '<input type="text"  name="new_owners['+id+'][per]" id="per_'+id+'" value="0" class="form-control pre" />';

                            }
                        html += '</div>';
                        html += '<div class="col-sm-1" style="padding-top: 7px;font-size: 16px;padding-left: 0px;">';
                            html +=  '<span>%</span>';
                        html += '</div>';

                        html += '<label class="col-sm-2 control-label">Amount</label>';
                        html += '<div class="col-sm-2">';
                            if(contingency_amounts != 0){
                                html += '<input type="text"  name="new_owners['+id+'][amount]" id="amount_'+id+'" value="'+contingency_amounts+'" class="form-control amount" />';
                            } else {
                                html += '<input type="text"  name="new_owners['+id+'][amount]" id="amount_'+id+'" value="0" class="form-control amount" />';

                            }                html += '</div>';
                            html += '<div class="col-sm-1" style="padding-top: 7px;font-size: 16px;padding-left: 0px;">';
                                html +=  '<span>(Rs.)</span>';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';

                    html += '<div class="form-group" style="padding-top: 10px;">';
                        html += '<div class="row">';
                            html += '<div class="col-sm-2">';
                                html += '</div>';
                            html += '<div class="col-sm-2">';
                                html += '<label >Win Gross Stake</label><br>';
                                if(contingency_wgs == 1){
                                    html += '<input type="checkbox" name="new_owners['+id+'][wgs]"  id="wgs_'+id+'"  class="wgs"  checked="checked"/>';
                                } else {
                                    html += '<input type="checkbox" name="new_owners['+id+'][wgs]"  id="wgs_'+id+'"  class="wgs" />';
                                }
                            html += '</div>';
                            html += '<div class="col-sm-2">';
                                html += '<label >Win Net Stake</label><br>';
                                if(contingency_wns == 1){
                                    html += '<input type="checkbox" name="new_owners['+id+'][wns]"  id="wns_'+id+'"  class="wns" checked="checked" />';
                                }else {
                                    html += '<input type="checkbox" name="new_owners['+id+'][wns]"  id="wns_'+id+'"  class="wns" />';
                                }
                            html += '</div>';
                            html += '<div class="col-sm-2">';
                                html += '<label >Place</label><br>';
                                if(contingency_place == 1){
                                    html += '<input type="checkbox" name="new_owners['+id+'][place]"  id="place_'+id+'"  class="place" checked="checked" />';
                                } else{
                                    html += '<input type="checkbox" name="new_owners['+id+'][place]"  id="place_'+id+'"  class="place" />';
                                }
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';


                    html += '<div class="form-group" style="padding-top: 10px;">';
                        html += '<div class="row">';
                            html += '<div class="col-sm-2"></div>';
                            html += '<div class="col-sm-2">';
                                html += '<label >Million & Over</label><br>';
                                if(contingency_million_over == 1){
                                    html += '<input type="checkbox" name="new_owners['+id+'][millionover]"  id="millionover_'+id+'"  class="millionover"  checked="checked" />';
                                } else {
                                    html += '<input type="checkbox" name="new_owners['+id+'][millionover]"  id="millionover_'+id+'"  class="millionover" />';

                                }
                            html += '</div>';
                            if(contingency_million_over == 1){
                                styl = "display: '';"
                            } else {
                                styl = "display: none;"
                            }

                            html += '<div class="col-sm-2 mo-rs-div_'+id+'" style="'+styl+'">';
                                html += '<label> > (Rs.)</label><br>';
                                if(contingency_million_amt != 0){
                                    html += '<input type="text" name="new_owners['+id+'][mo_rs]" value="'+contingency_million_amt+'"  id="mo_rs_'+id+'"  class="form-control mo_rs" />';
                                } else {
                                    html += '<input type="text" name="new_owners['+id+'][mo_rs]"  id="mo_rs_'+id+'"  class="form-control mo_rs" />';
                                }
                            html += '</div>';
                            html += '<div class="col-sm-2">';
                                html += '<label >Grade 1</label><br>';
                                if(contingency_gradeone == 1){
                                    html += '<input type="checkbox" name="new_owners['+id+'][grade1]"  id="grade1_'+id+'"  class="grade1" checked="checked"  />';
                                } else {
                                    html += '<input type="checkbox" name="new_owners['+id+'][grade1]"  id="grade1_'+id+'"  class="grade1" />';
                                }
                            html += '</div>';
                            html += '<div class="col-sm-2">';
                                html += '<label>Grade 2</label><br>';
                                if(contingency_grade_two == 1){
                                    html += '<input type="checkbox" name="new_owners['+id+'][grade2]"  id="grade2_'+id+'"  class="grade2" checked="checked" />';
                                } else {
                                    html += '<input type="checkbox" name="new_owners['+id+'][grade2]"  id="grade2_'+id+'"  class="grade2" />';

                                }
                            html += '</div>';

                            html += '<div class="col-sm-2">';
                                html += '<label >Grade 3</label><br>';
                                if(contingency_grade_three == 1){
                                    html += '<input type="checkbox" name="new_owners['+id+'][grade3]"  id="grade3_'+id+'"  class="grade3" checked="checked" />';
                                } else {
                                    html += '<input type="checkbox" name="new_owners['+id+'][grade3]"  id="grade3_'+id+'"  class="grade3" />';

                                }
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        } else {
            html = '';
            html += '<div class="form-group all-data-div_'+id+'" >';
                html += '<div class="form-group">';
                    html += '<div class="row">';
                        html += '<label class="col-sm-2 control-label" for="input-pre">Percentage</label>';
                        html += '<div class="col-sm-2">';
                            html += '<input type="text"  name="new_owners['+id+'][per]" id="per_'+id+'" value="0" class="form-control pre" />';
                        html += '</div>';
                        html += '<div class="col-sm-1" style="padding-top: 7px;font-size: 16px;padding-left: 0px;">';
                            html +=  '<span>%</span>';
                        html += '</div>';
                        
                        html += '<label class="col-sm-2 control-label" for="input-amount">Amount</label>';
                        html += '<div class="col-sm-2">';
                            html += '<input type="text"  name="new_owners['+id+'][amount]" id="amount_'+id+'" value="0" class="form-control amount" />';
                        html += '</div>';
                        html += '<div class="col-sm-1" style="padding-top: 7px;font-size: 16px;padding-left: 0px;">';
                            html +=  '<span>(Rs.)</span>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';


                html += '<div class="form-group" style="padding-top: 10px;">';
                    html += '<div class="row">';
                        html += '<div class="col-sm-2">';
                        html += '</div>';
                        html += '<div class="col-sm-2">';
                        html += '<label class="" for="input-wgs" >Win Gross Stake</label><br>';
                            html += '<input type="checkbox" name="new_owners['+id+'][wgs]"  id="wgs_'+id+'"  class="wgs" />';
                        html += '</div>';
                        
                        html += '<div class="col-sm-2">';
                            html += '<label  for="input-wns" >Win Net Stake</label><br>';
                            html += '<input type="checkbox" name="new_owners['+id+'][wns]"  id="wns_'+id+'"  class="wns" />';
                        html += '</div>';
                    
                        html += '<div class="col-sm-2">';
                        html += '<label>Place</label><br>';
                            html += '<input type="checkbox" name="new_owners['+id+'][place]"  id="place_'+id+'"  class="place" />';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';

                html += '<div class="form-group" style="padding-top: 10px;">';
                    html += '<div class="row">';
                        html += '<div class="col-sm-2">';
                        html += '</div>';
                        html += '<div class="col-sm-2">';
                            html += '<label  for="input-millionover" >Million & Over</label><br>';
                            html += '<input type="checkbox" name="new_owners['+id+'][millionover]"  id="millionover_'+id+'"  class="millionover" />';
                        html += '</div>';
                        html += '<div class="col-sm-2 mo-rs-div_'+id+'" style="display: none;">';
                            html += '<label for="input-mo_rs"> > (Rs.)</label>';
                            html += '<input type="text" name="new_owners['+id+'][mo_rs]"  id="mo_rs_'+id+'"  class="form-control mo_rs" />';
                        html += '</div>';

                        html += '<div class="col-sm-2">';
                            html += '<label  for="input-grade1" >Grade 1</label><br>';
                            html += '<input type="checkbox" name="new_owners['+id+'][grade1]"  id="grade1_'+id+'"  class="grade1" />';
                        html += '</div>';
                        
                        html += '<div class="col-sm-2">';
                            html += '<label  for="input-grade2" >Grade 2</label><br>';
                            html += '<input type="checkbox" name="new_owners['+id+'][grade2]"  id="grade2_'+id+'"  class="grade2" />';
                        html += '</div>';
                    
                        html += '<div class="col-sm-2">';
                            html += '<label for="input-grade3" >Grade 3</label><br>';
                            html += '<input type="checkbox" name="new_owners['+id+'][grade3]"  id="grade3_'+id+'"  class="grade3" />';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            html += '</div>'; 
        }

        $('.parent_div_'+id).append(html);
    }


    function copyParentData(idsz){
        check_status = $('#contengency_1').prop('checked');
        if(check_status == true){
            percentage = $('#per_1').val();
            amount = $('#amount_1').val();
            wgs = $('#wgs_1').prop('checked');
            wns = $('#wns_1').prop('checked');
            place = $('#place_1').prop('checked');
            millionover_status =  $('#millionover_1').prop('checked');
            mo_rs = $('#mo_rs_1').val();
            grade1 = $('#grade1_1').prop('checked');
            grade2 = $('#grade2_1').prop('checked');
            grade3 = $('#grade3_1').prop('checked');
        } else {
            percentage = 0;
            amount = 0;
            wgs = false;
            wns = false;
            place = false;
            millionover_status =  false;
            mo_rs = 0;
            grade1 = false;
            grade2 = false;
            grade3 = false;
        }

        dataa_id = idsz.split('_');
        id = dataa_id[1];
        html = '';
        html += '<div class="form-group all-data-div_'+id+'">';
            html += '<div class="form-group">';
                html += '<div class="row">';
                    html += '<label class="col-sm-2 control-label">Percentage</label>';
                    html += '<div class="col-sm-2">';
                        if(percentage != 0){
                            html += '<input type="text"  name="new_owners['+id+'][per]" id="per_'+id+'" value="'+percentage+'" class="form-control pre" />';
                        } else {
                            html += '<input type="text"  name="new_owners['+id+'][per]" id="per_'+id+'" value="0" class="form-control pre" />';

                        }
                    html += '</div>';
                    html += '<div class="col-sm-1" style="padding-top: 7px;font-size: 16px;padding-left: 0px;">';
                        html +=  '<span>%</span>';
                    html += '</div>';

                    html += '<label class="col-sm-2 control-label">Amount</label>';
                    html += '<div class="col-sm-2">';
                        if(amount != 0){
                            html += '<input type="text"  name="new_owners['+id+'][amount]" id="amount_'+id+'" value="'+amount+'" class="form-control amount" />';
                        } else {
                            html += '<input type="text"  name="new_owners['+id+'][amount]" id="amount_'+id+'" value="0" class="form-control amount" />';

                        }                html += '</div>';
                        html += '<div class="col-sm-1" style="padding-top: 7px;font-size: 16px;padding-left: 0px;">';
                            html +=  '<span>(Rs.)</span>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';

                html += '<div class="form-group" style="padding-top: 10px;">';
                    html += '<div class="row">';
                        html += '<div class="col-sm-2">';
                            html += '</div>';
                        html += '<div class="col-sm-2">';
                            html += '<label >Win Gross Stake</label><br>';
                            if(wgs == true){
                                html += '<input type="checkbox" name="new_owners['+id+'][wgs]"  id="wgs_'+id+'"  class="wgs"  checked="checked"/>';
                            } else {
                                html += '<input type="checkbox" name="new_owners['+id+'][wgs]"  id="wgs_'+id+'"  class="wgs" />';
                            }
                        html += '</div>';
                        html += '<div class="col-sm-2">';
                            html += '<label >Win Net Stake</label><br>';
                            if(wns == true){
                                html += '<input type="checkbox" name="new_owners['+id+'][wns]"  id="wns_'+id+'"  class="wns" checked="checked" />';
                            }else {
                                html += '<input type="checkbox" name="new_owners['+id+'][wns]"  id="wns_'+id+'"  class="wns" />';
                            }
                        html += '</div>';
                        html += '<div class="col-sm-2">';
                            html += '<label >Place</label><br>';
                            if(place == true){
                                html += '<input type="checkbox" name="new_owners['+id+'][place]"  id="place_'+id+'"  class="place" checked="checked" />';
                            } else{
                                html += '<input type="checkbox" name="new_owners['+id+'][place]"  id="place_'+id+'"  class="place" />';
                            }
                        html += '</div>';
                    html += '</div>';
                html += '</div>';


                html += '<div class="form-group" style="padding-top: 10px;">';
                    html += '<div class="row">';
                        html += '<div class="col-sm-2"></div>';
                        html += '<div class="col-sm-2">';
                            html += '<label >Million & Over</label><br>';
                            if(millionover_status == true){
                                html += '<input type="checkbox" name="new_owners['+id+'][millionover]"  id="millionover_'+id+'"  class="millionover"  checked="checked" />';
                            } else {
                                html += '<input type="checkbox" name="new_owners['+id+'][millionover]"  id="millionover_'+id+'"  class="millionover" />';

                            }
                        html += '</div>';
                        if(millionover_status == true){
                            styl = "display: '';"
                        } else {
                            styl = "display: none;"
                        }

                        html += '<div class="col-sm-2 mo-rs-div_'+id+'" style="'+styl+'">';
                            html += '<label> > (Rs.)</label><br>';
                            if(mo_rs != 0){
                                html += '<input type="text" name="new_owners['+id+'][mo_rs]" value="'+mo_rs+'"  id="mo_rs_'+id+'"  class="form-control mo_rs" />';
                            } else {
                                html += '<input type="text" name="new_owners['+id+'][mo_rs]"  id="mo_rs_'+id+'"  class="form-control mo_rs" />';
                            }
                        html += '</div>';
                        html += '<div class="col-sm-2">';
                            html += '<label >Grade 1</label><br>';
                            if(grade1 == true){
                                html += '<input type="checkbox" name="new_owners['+id+'][grade1]"  id="grade1_'+id+'"  class="grade1" checked="checked"  />';
                            } else {
                                html += '<input type="checkbox" name="new_owners['+id+'][grade1]"  id="grade1_'+id+'"  class="grade1" />';
                            }
                        html += '</div>';
                        html += '<div class="col-sm-2">';
                            html += '<label>Grade 2</label><br>';
                            if(grade2 == true){
                                html += '<input type="checkbox" name="new_owners['+id+'][grade2]"  id="grade2_'+id+'"  class="grade2" checked="checked" />';
                            } else {
                                html += '<input type="checkbox" name="new_owners['+id+'][grade2]"  id="grade2_'+id+'"  class="grade2" />';

                            }
                        html += '</div>';

                        html += '<div class="col-sm-2">';
                            html += '<label >Grade 3</label><br>';
                            if(grade3 == true){
                                html += '<input type="checkbox" name="new_owners['+id+'][grade3]"  id="grade3_'+id+'"  class="grade3" checked="checked" />';
                            } else {
                                html += '<input type="checkbox" name="new_owners['+id+'][grade3]"  id="grade3_'+id+'"  class="grade3" />';

                            }
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</div>';
        $('.parent_div_'+id).append(html);
    }

    $(document).ready(function(){
        var check_provisonal = $('#check_provisonal').val();
        if(check_provisonal == 1){
            $('.selectsharediv').show();
            $('#total_share').val(100);
            $('#check_provi_owner').val(1);
            $('#ownership_type_id').val('Sale');
            $('#ownership_type_id').attr('readonly', true);

        }
    });

    $(document).on('keyup', '.percentage', function(){
        tot_share = $('#pre_total_share').val();
        //contangency calculation
        parent_contamt = 0;
        $('.checkbox_value:checked').each(function(){
            idss = $(this).attr('id');
            ids = idss.split('_');
            parent_contamt = $('#parent_cont_amt_'+ids[3]).val();

           // console.log(`parent owner checked id is:  ${idss} AND parent cont amt ${parent_contamt}`);
            
        });   

        idzz = $(this).attr('id');
        idz = idzz.split('_');
        if(parseFloat($(this).val()) > 0){
            current_share = parseFloat($(this).val());
            total_selected_share = parseFloat(tot_share);
            total_cont_amt = parseFloat(parent_contamt);
            pay_amt = total_cont_amt * current_share / total_selected_share;
            $('#amount_'+idz[1]).val(pay_amt);
        } else {
            $('#amount_'+idz[1]).val(parent_contamt);
        }

        percentage = 0;//$(this).val();
        
        $('.percentage').each(function(){
            percentage += isNaN(parseFloat($(this).val())) ? 0 : parseFloat($(this).val());
        });

        remain_share = tot_share - percentage;
        //console.log("re sh "+remain_share);
        if(remain_share >= 0){
            $('#total_share').val(parseFloat(remain_share).toFixed(2));
            //console.log("re sh "+$('#total_share').val());
        } else {
            $('#'+idzz).val(0);
            $('#total_share').val(tot_share);
            $('#amount_'+idz[1]).val(0);

            alert("Owner Share is greater then total share please check !");
            return false;
        }
    });

    $(document).on('change', '#check_provi_owner', function(){ 
        console.log($(this).prop('checked') );
        if ($(this).prop('checked') == true) {
            $('.selectsharediv').show();
            $('#total_share').val(100);
            $('#check_provi_owner').val(1);
            $('#ownership_type_id').val('Sale');
            $('#ownership_type_id').attr('readonly', true);
        } else {
            $('.selectsharediv').hide();
            $('#total_share').val('');
            $('#check_provi_owner').val(0);
            $('#ownership_type_id').val(0);
        }
    });
</script>
</div>
<?php echo $footer; ?>