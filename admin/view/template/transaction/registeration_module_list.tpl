<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clipboard"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form  method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <div class="form-group" >
                        <label class="col-sm-2 control-label" for="input-horse"><b style="color: red;"> * </b> <?php echo "Horse Name:"; ?></label>
                        <div class="col-sm-3">
                            <input type="text"  name="filterHorseName" id="filterHorseName" value="<?php echo $horse_name ?>" placeholder="Horse Name" class="form-control" />
                            <input type="hidden" name="filterHorseId" id="filterHorseId" value="<?php echo $horse_id ?>"  class="form-control">
                            <br>
                            <!-- <span id="pedegree"></span> -->
                            <input disabled="disabled" style="background: white;width: 190%;border: none;" id="pedegree" value="" name="">
                            
                        </div>
                        <label class="col-sm-2 control-label" for="input-trainer"><?php echo "Trainer Name :"; ?></label>
                        <div class="col-sm-3">
                             <input type="text" name="trainer_name"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $trainer_name ?>" placeholder="Trainer Name" id="input-trainer_name" class="form-control"  readonly="readonly"/>
                            <input type="hidden" name="trainer_id" value="<?php echo $trainer_id ?>" id="trainer_id" class="form-control" />
                            <br>
                            <span id="licence_type"></span>

                            
                        </div>
                    </div>

                    
                    
                    <div class="form-group from-div" style="display: none;"> 
                        <div class="form-group " >
                            <div class="col-sm-3">
                                
                            </div>
                            <label class="col-sm-2 control-label" for="input-registeration_type" required><b style="color: red;"> * </b> Registration Type </label>
                             <div class="col-sm-3">
                                <select name="registeration_type" id="registeration_type_id" class="form-control">
                                    <option value="" >Please Select</option>    
                                    <?php foreach ($registration_type as $r_typekey => $r_typevalue) { ?>
                                        <option value="<?php echo $r_typevalue; ?>" ><?php echo $r_typevalue; ?></option>
                                    <?php } ?>
                                </select>
                                <span style="display: none;color: red;font-weight: bold;" id="error_registeration_type" class="error"><?php echo 'Please Select Registration Type'; ?></span>
                            </div>
                            <label class="col-sm-2 control-label" for="modification" required><b style="color: red;"></b> Modification </label>
                            <div class="col-sm-2">
                                <input type="checkbox" name="modification" id="modification">
                                <input type="hidden" name="hiddenmodify" id="hiddenmodify" value="2">
                            </div>
                        </div>

                        <div class="form-group " >
                            <div class="col-sm-3">
                                
                            </div>
                            <label class="col-sm-2 control-label" for="input-horse_name" required><b style="color: red;"> * </b> Proposed name </label>
                            <div class="col-sm-3" >
                                <input type="text" name="horse_name" value=""  id="horse_name_id"  class="form-control" required/>
                              <span style="display: none;color: red;font-weight: bold;" id="error_horse_name_id" class="error"><?php echo 'Please Enter Proposed name'; ?></span>
                            </div>
                        </div>
                        <div class="form-group " >
                            <div class="col-sm-3">
                                
                            </div>
                            <label class="col-sm-2 control-label" >Print letters</label>
                            <div class="col-sm-3 text-start" >
                                
                                <input checked="checked" type="checkbox" name="print_latters" id="print_latters">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                
                            </div>
                            <label class="col-sm-2 control-label" for="input-remark" required><b style="color: red;"></b>Remark</label>
                            <div class="col-sm-3" >
                                <textarea type="text" name="remark_regi" value=""  id="remark_regi"  class="form-control" ></textarea>
                            <!--  <span style="display: none;color: red;font-weight: bold;" id="error_registration_remark_id" class="error"><?php echo 'Please Enter Remark'; ?></span> -->
                            </div>
                        </div>


                        
                        <div class="form-group p_l" >
                            <div class="col-sm-3">
                            </div>
                            <label class="col-sm-2 control-label" for="input-registration_fee" required><b style="color: red;"></b> Registration Fee </label>
                            <div class="col-sm-3" >
                                <input type="number" name="registration_fee" value=""  id="registration_fee_id"  class="form-control" required/>
                              <span style="display: none;color: red;font-weight: bold;" id="error_registration_fee_id" class="error"><?php echo 'Please Enter Registration Fee'; ?></span>
                            </div>
                        </div>

                        <div class="form-group p_l">
                            <div class="col-sm-3">
                            </div>
                            <label class="col-sm-2 control-label" for="input-reg_date"><?php echo "Registration Date :"; ?></label>
                            <div class="col-sm-3">
                                <div class="input-group date input-reg_date">
                                    <input type="text" name="reg_date" id="reg_date" value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY"  class="form-control" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        


                        <div class="form-group p_l">
                           <div class="col-sm-3">
                            </div>
                            <label class="col-sm-2 control-label" for="input-letter_date"><?php echo "Letter Dated :"; ?></label>
                            <div class="col-sm-3">
                                <div class="input-group date input-letter_date">
                                    <input type="text" name="letter_date" id="letter_date" value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY"  class="form-control" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group p_l" >
                            <div class="col-sm-3">
                            </div>
                            <label class="col-sm-2 control-label" for="input-letter_received"><?php echo "Letter Received :"; ?></label>
                            <div class="col-sm-3">
                                <div class="input-group date input-letter_received">
                                    <input type="text" name="letter_received" id="letter_received" value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY"  class="form-control" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            
                        </div>



                        

                        <div class="form-group " style="border-top: 0; height: 100%;display: flex;justify-content: center;align-items: center;" >
                            <a id="save"  class="col-sm-2 btn btn-primary">Save</a>
                        </div>

                         <div class="table-responsive regiered_horse_dats" style="border-top: 0;">
                        </div>


                        <div class="table-responsive owner_datas" style="border-top: 0;">
                        </div>

                       
                    </div>
                    <input type="hidden" id="last_owner_id" value="0" class="form-control last_owner_id" />
                    <input type="hidden" id="selected_pre_owners" value="0" class="form-control selected_pre_owners" />
                    <input type="hidden"  id="is_owners" class="is_owners">

                </form>
            </div>
        </div>
    </div>
    <div class="mod" >
       
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contingency Details</h4>
                </div>
                <div class="modal-body" style="height: 240px;">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $(document).ready(function(){
        $('#filterHorseName').focus();
    })
    $('#modification').change(function(){
        //console.log(this.checked);
        if(this.checked){
            var modifications = '1';
            $("#hiddenmodify").val(modifications);
        } else {
            var modifications = '2';
            $("#hiddenmodify").val(modifications);
        }
    });

    //function submit(){
            $('#save').click(function(){
             var is_owners = $('#is_owners').val() ;
            if(is_owners == 1){
                var type = $("#registeration_type_id").val();
                var id = $("#filterHorseId").val();

                $.ajax({
                    url: 'index.php?route=transaction/registeration_module/reg_datas&token=<?php echo $token; ?>'+'&type='+type+'&id='+id,
                    dataType: 'json',
                    success: function(json) {
                        console.log('inn');
                        console.log(json.length);
                        var modifications = $("#hiddenmodify").val();
                        if (json.length >= '0' && modifications == '1') {
                            var registeration_type_id =  $( "#registeration_type_id" ).val() || '';
                            var registration_fee_id =  $( "#registration_fee_id" ).val() || '';
                            //var remark_regi = $('#remark_regi').val() || '';
                            var horse_name = $('#horse_name_id').val() || '';
                            var print_latters = $('#print_latters').val() || '';
                            var reg_date = $('#reg_date').val() || '';
                            var letter_date = $('#letter_date').val() || '';
                            var letter_received = $('#letter_received').val() || '';
                            var old_horse_name = $('#filterHorseName').val() || '';
                            var old_horse_id = $('#filterHorseId').val() || '';




                            if(registeration_type_id != ''){
                                $('#error_registeration_type').hide();
                            } else {
                                $('#error_registeration_type').show();
                            }

                            /* if(registration_fee_id != ''){
                                $('#error_registration_fee_id').hide();
                            } else {
                                $('#error_registration_fee_id').show();
                            }*/

                            /* if(remark_regi != ''){
                                $('#error_registration_remark_id').hide();
                            } else {
                                $('#error_registration_remark_id').show();
                            }*/

                            if(horse_name != ''){
                                $('#error_horse_name_id').hide();
                            } else {
                                $('#error_horse_name_id').show();
                            }





                            if(registeration_type_id == '' || registration_fee_id == '' ||  horse_name == ''){
                                return false;
                            }

                            var rowCount = $('.regitabless').length;
                            var modify = $("#modification").val();

                            $.ajax({
                                method: "POST",
                                url:'index.php?route=transaction/registeration_module/sumbit_registeration_form&token=<?php echo $token; ?>'+'&modifications='+modify,
                                method: "POST",
                                dataType: 'json',
                                async: true,
                                
                                data : $('#form-category').serialize(),
                                success: function(json)
                                {
                                    console.log(json);
                                   if (json) {
                                        if(rowCount == '0'){
                                            html = '<label class="col-sm-2 control-label name_change_label" for="input-trainer_name" >Name Change History:</label><br/>';
                                            html += '<table class="table table-bordered regitabless" style= "margin-top: 2%;">';
                                                html += '<thead>';
                                                    html += '<tr>';
                                                        html += '<td style="text-align: center;">Horse Name</td>';
                                                        html += '<td style="width: 160px;text-align: center;">Registration Type</td>';
                                                        html += '<td style="text-align: center;">Reason</td>';
                                                    html += '</tr>';
                                                html += '</thead>';
                                                html += '<tbody>';
                                             html  += '</table>';
                                              $('.regiered_horse_dats').append(html);
                                                   
                                        }
                                        html = '<tr">';
                                            html += '<td class="text-left"  >'+json.horse_name+'</td>';
                                            html += '<td class="text-left"  >'+json.registeration_type+'</td>';
                                            html += '<td class="text-left"  >'+json.remark+'</td>';
                                        html += '</tr >';
                                            
                                        $('.regitabless').append(html);
                                } 

                                   $( "#registeration_type_id" ).val('');
                                    $( "#registration_fee_id" ).val('');
                                    //$('#remark_regi').val('');
                                    $('#horse_name_id').val('');
                                        location.reload(true);
                                },
                                error: function (jqXHR, textStatus, errorThrown)
                                {
                                    alert(errorThrown);
                                }
                            });
                        } else if (json.length != 0 && modifications == '2') {
                            alert('You are doing it for the second time!');
                            return false;
                        } else {
                            var registeration_type_id =  $( "#registeration_type_id" ).val() || '';
                            var registration_fee_id =  $( "#registration_fee_id" ).val() || '';
                            //var remark_regi = $('#remark_regi').val() || '';
                            var horse_name = $('#horse_name_id').val() || '';
                            var print_latters = $('#print_latters').val() || '';
                            var reg_date = $('#reg_date').val() || '';
                            var letter_date = $('#letter_date').val() || '';
                            var letter_received = $('#letter_received').val() || '';
                            var old_horse_name = $('#filterHorseName').val() || '';
                            var old_horse_id = $('#filterHorseId').val() || '';


                            if(registeration_type_id != ''){
                                $('#error_registeration_type').hide();
                            } else {
                                $('#error_registeration_type').show();
                            }

                             if(registration_fee_id != ''){
                                $('#error_registration_fee_id').hide();
                            } else {
                                $('#error_registration_fee_id').show();
                            }

                            //  if(remark_regi != ''){
                            //     $('#error_registration_remark_id').hide();
                            // } else {
                            //     $('#error_registration_remark_id').show();
                            // }

                            if(horse_name != ''){
                                $('#error_horse_name_id').hide();
                            } else {
                                $('#error_horse_name_id').show();
                            }

                            if(registeration_type_id == '' || registration_fee_id == '' ||  horse_name == ''){
                                return false;
                            }

                            var rowCount = $('.regitabless').length;
                            var modify = $("#modification").val();

                            $.ajax({
                                method: "POST",
                                url:'index.php?route=transaction/registeration_module/sumbit_registeration_form&token=<?php echo $token; ?>'+'&modifications='+modify,
                                method: "POST",
                                dataType: 'json',
                                async: true,
                                
                                data : $('#form-category').serialize(),
                                success: function(json)
                                {
                                    console.log(json);
                                   if (json) {
                                        if(rowCount == '0'){
                                            html = '<label class="col-sm-2 control-label name_change_label" for="input-trainer_name" >Name Change History:</label><br/>';
                                            html += '<table class="table table-bordered regitabless" style= "margin-top: 2%;">';
                                                html += '<thead>';
                                                    html += '<tr>';
                                                        html += '<td style="text-align: center;">Horse Name</td>';
                                                        html += '<td style="width: 160px;text-align: center;">Registration Type</td>';
                                                        html += '<td style="text-align: center;">Reason</td>';
                                                    html += '</tr>';
                                                html += '</thead>';
                                                html += '<tbody>';
                                             html  += '</table>';
                                              $('.regiered_horse_dats').append(html);
                                                   
                                        }
                                        html = '<tr">';
                                            html += '<td class="text-left"  >'+json.horse_name+'</td>';
                                            html += '<td class="text-left"  >'+json.registeration_type+'</td>';
                                            html += '<td class="text-left"  >'+json.remark+'</td>';
                                        html += '</tr >';
                                            
                                        $('.regitabless').append(html);
                                } 

                                   $( "#registeration_type_id" ).val('');
                                    $( "#registration_fee_id" ).val('');
                                    //$('#remark_regi').val('');
                                    $('#horse_name_id').val('');
                                        location.reload(true);
                                },
                                error: function (jqXHR, textStatus, errorThrown)
                                {
                                    alert(errorThrown);
                                }
                            });
                        }
                    }
                });
             } else {
                        alert("Add Owners ");

                    }
            });
       
        
        $('#filterHorseName').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#filterHorseId').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/registeration_module/autocompleteHorse&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.horse_name,
                                    value: item.horse_name,
                                    horse_id:item.horse_id,
                                    pedegree : item.pedegree,
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                console.log(ui.item);
                $('#filterHorseName').val(ui.item.value);
                $('#filterHorseId').val(ui.item.horse_id);
                $('#pedegree').val('');
                $('#pedegree').val(ui.item.pedegree);

                $('.dropdown-menu').hide();
                if(ui.item.horse_id != ''){
                $.ajax({
                        url: 'index.php?route=transaction/registeration_module/autoHorseToTrainer&token=<?php echo $token; ?>&horse_id=' +  encodeURIComponent(ui.item.horse_id),
                        dataType: 'json',
                        cache: false,
                        success: function(json) {
                            //console.log(json);
                            if(json){
                                $.each(json, function (i, item) {
                                //console.log(item);
                                    $('#input-trainer_name').val(item.trainer_name);
                                    $('#trainer_id').val(item.trainer_id);
                                 $('#licence_type').html('');

                                    
                                    $('#licence_type').append("<b>Trainer lisence type </b>: " +item.licence_type);

                                });
                            }
                        }
                    }); 
                }

                if((ui.item.horse_id != '')){
                    $.ajax({
                        url: 'index.php?route=transaction/registeration_module/getOwnerData&token=<?php echo $token; ?>&horse_id=' +  encodeURIComponent(ui.item.horse_id),
                        dataType: 'json',
                        cache: false,
                        success: function(json) {
                            if(json.status == 1){
                                $('.ownertable').remove();
                                $('.current_owner_label').html('');
                                $('.owner_datas').append(json.html_owner);
                                $('.is_owners').val(json.is_owners);
                            }
                        }
                    }); 
                } else {
                    //alert('Please select Horse');
                    $('.ownertable').remove();
                     $('.current_owner_label').html('');
                }

                if((ui.item.horse_id != '')){
                    $.ajax({
                        url: 'index.php?route=transaction/registeration_module/getRegisteredHorse&token=<?php echo $token; ?>&horse_id=' +  encodeURIComponent(ui.item.horse_id),
                        dataType: 'json',
                        cache: false,
                        success: function(json) {
                            $('.regitabless').remove();
                            $('.name_change_label').html('');
                            $('.regiered_horse_dats').append(json);
                        }
                    }); 
                } else {
                    //alert('Please select Horse');
                    $('.regitabless').remove();
                    $('.name_change_label').html('');
                }
                $('.from-div').show();
                $('#registeration_type_id').focus();
                return false;
            },
        });

        
      $('#print_latters').change(function(){
        //console.log(this.checked);
        if(this.checked){
            $('.p_l').show();
        } else {
            $('.p_l').hide();
        }
      });


      //refresh owners
      $(document).on('click','#ref-owners',function(){
            filterHorseId = $('#filterHorseId').val();
            if(filterHorseId != ''){
              // console.log(datasss);
                $.ajax({
                    method: "POST",
                    url: 'index.php?route=transaction/registeration_module/refreshOwners&token=<?php echo $token; ?>&filterHorseId='+filterHorseId,
                    dataType: 'json',
                    success: function(json) {
                        if(json.status == 1){
                            $('.owner_datas').html('');
                            $('.owner_datas').append(json.html_owner);
                            $('.is_owners').val(json.is_owners);
                        }
                    }
                       
                        
                });
            } else {
                alert("Please Enter Date");
                return false;
            }
        });



       //partners Datassss
        $(document).on("click",".partners",function() {
            parent_owner_idss = $(this).attr('id');
            parent_owner_id = parent_owner_idss.split('_');
            horse_id = $('#filterHorseId').val();
            $.ajax({
                url: 'index.php?route=transaction/arrival_charges/getPartnersData&token=<?php echo $token; ?>&parent_owner_id='+parent_owner_id[1]+'&horse_id='+horse_id,
                dataType: 'json',
                success: function(json) {   
                    $('#partners_data_'+parent_owner_id[1]).remove();
                    $('.mod').append(json);
                    $('#partners_data_'+parent_owner_id[1]).modal('show'); 
                }
            });
        });

         // contingency popup details 

        $(document).on("click",".modalopen",function() {
            //alert('inn');
           // console.log($(this).attr('id'));
            horse_owner_idssss = $(this).attr('id');
            horse_owner_id = horse_owner_idssss.split('_');

            $.ajax({
                url: 'index.php?route=transaction/ownership_shift_module/getContaingencyData&token=<?php echo $token; ?>&horse_owner_id='+horse_owner_id[1],
                dataType: 'json',
                success: function(json) {   
                    $('.modal-body').html('');
                    $('.modal-body').append(json);
                    $('#myModal').modal('show'); 
                }
            });
        });

        $(document).on('keydown', '#registeration_type_id, #horse_name_id, #print_latters, #remark_regi, #registration_fee_id , #reg_date, #letter_date, #letter_received', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13){
                var id = $(this).attr('id');
                if(id == 'registeration_type_id'){
                    $('#horse_name_id').focus();
                } else if(id == 'horse_name_id'){
                    $('#print_latters').focus();

                } else if(id == 'print_latters'){
                    $('#remark_regi').focus();
                    
                } else if(id == 'remark_regi'){
                    $('#registration_fee_id').focus();
                    
                }  else if(id == 'registration_fee_id'){
                    $('#reg_date').focus();
                    
                }  else if(id == 'reg_date'){
                    $('#letter_date').focus();
                    
                }  else if(id == 'letter_date'){
                    $('#letter_received').focus();
                    
                }


            }
        });

    </script>
</div>
<?php echo $footer; ?>