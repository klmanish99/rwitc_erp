<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a type="submit" form="form-category"  class="btn btn-primary pull-right save" style="margin-left: 10px;">Save</a>
                <a href="<?php echo $arrival ?>" class="btn btn-warning pull-right" >Individual Arrival Charge</a>
               
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>


        <div class="panel panel-default">
            


            <div class="panel-body">
                <form action="<?php $filter ?>" method="get" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <div class="form-group" >

                        <div class="col-sm-4">
                            
                        </div>
                        <label class="col-sm-1" > Charge type</label>
                        <div class="col-sm-2">
                            <select name="charge_type" class="form-control" >
                                <option value="">Please Select</option>
                                <?php foreach ($charge_types as $key => $value) { 
                                    if($value == $charge_type){
                                    ?>
                                        <option value="<?php echo $value ?>" selected="selected"> <?php echo $value; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $value ?>"> <?php echo $value; ?></option>

                                    <?php } ?>

                                <?php } ?>

                            </select>
                        </div>

                        <div>
                        <a onclick="filter();" id="filter" class="btn btn-primary">Filter</a>  
                        </div>
                        
                    </div>
                    <?php if($horse_datas){ ?>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'horse_idss\']').prop('checked', this.checked);" /></th>
                                    <th>Sr.No.</th>
                                    <th>Horse Name</th>
                                    <th>Ownership</th>
                                    <th >Trainer</th>
                                    <th>Amount</th>
                                    <th style="width: 25%">Action</th>
    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sr_no = 1;
                                //$extra_field = 1;
                                 foreach ($horse_datas as $hkey => $hvalue) { ?>
                                    <tr  id="horse_<?php echo $sr_no; ?>">
                                        <td class="text-center"><?php if (in_array($hvalue['horse_id'], $selected)) { ?>
                                            <input type="checkbox" name="horse_idss[]"  value="<?php echo $hvalue['horse_id']; ?>" class="checkboxclass" checked="checked" />
                                              <?php } else { ?>
                                            <input type="checkbox" name="horse_idss[<?php echo $hkey ?>][horse_id]" id="<?php echo $hvalue['horse_id']; ?>" class="checkboxclass" value="<?php echo $hvalue['horse_id']; ?>" />
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $sr_no ?></td>
                                        <td><?php echo $hvalue['horse_name'] ?>

                                            <input type="hidden" name="" value="<?php echo $hvalue['horse_id'] ?>" id="horse_ids" class="form-control"> 
                                            <input type="hidden" name="horse_idss[<?php echo $hkey ?>][horse_name]" value="<?php echo $hvalue['horse_name'] ?>" class="form-control"> 
                                        </td>
                                        
                                        <td id="owner_<?php echo $sr_no; ?>" ><?php echo $hvalue['owner_name'] ?>
                                            <div class="pull-right" >
                                                <a target="_blank" href="<?php echo $hvalue['change_ownership'] ?>" title="Change Ownership" class="btn btn-primary"><i class="fa fa-pencil" ></i></a>
                                                <a onclick="refresh_owners(<?php echo $sr_no; ?>,<?php echo $hvalue['horse_id'] ?>)" title="Refresh Ownership" class="btn btn-primary"><i class="fa fa-refresh" ></i></a>
                                            </div>
                                        </td>
                                       
                                        <td><?php echo $hvalue['trainer_name'] ?>
                                            <input type="hidden" name="horse_idss[<?php echo $hkey ?>][trainer_name]" value="<?php echo $hvalue['trainer_name']?>" class="form-control"> 
                                            <input type="hidden" name="horse_idss[<?php echo $hkey ?>][trainer_id]" value="<?php echo $hvalue['trainer_id'] ?>" class="form-control"> 

                                        </td>
                                        <td><input type="number" name="horse_idss[<?php echo $hkey ?>][amt]" min="1" value="15000" class="form-control"> </td>
                                        <td>
                                        </td>
                                    </tr>
                                <?php $sr_no++; } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    
                    
                        
                        
                    

                        <a id="sv"  style="margin-left:50%;display: none;" class="btn btn-primary save">Save</a>
                   
                    

                       
                       
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $(document).ready(function(){
        $('#filterHorseName').focus();
        is_fil = '<?php echo $is_filter ?>';
        if(is_fil == 1){
            $('#sv').show();
        } else {
            $('#sv').hide();
        }
    })
    

    function filter() { 
        var charge_type = $('select[name=\'charge_type\']').val();

        url = 'index.php?route=transaction/multi_arrival_charges&token=<?php echo $token; ?>';

        if (charge_type) {
            url += '&charge_type=' + encodeURIComponent(charge_type);
        }

        window.location.href = url;
    }

    function refresh_owners(sr_no,horse_id) {
        //alert(horse_id);
        sr_no1 = sr_no;
        $.ajax({
            method: "POST",
            url: 'index.php?route=transaction/multi_arrival_charges/refresh_owners&token=<?php echo $token; ?>',
            dataType: 'json',
            data : {
                horse_id : horse_id,
                sr_no : sr_no,
            },
            success: function(json) {
                $('#owner_'+sr_no1+'').html('');
                $('#owner_'+sr_no1+'').append(json);
            }
        });

    }
     

        $('.save').click(function(){
           datasss =  $('#form-category').serialize();
           //console.log(datasss);
           //return false;
            $.ajax({
                method: "POST",
                url: 'index.php?route=transaction/multi_arrival_charges/arrival_datas&token=<?php echo $token; ?>',
                dataType: 'json',
                data: datasss,
                success: function(json) {
                    if(json.status == 1){
                        //location.reload(true);
                        path = 'index.php?route=transaction/send_mail_charges&token=<?php echo $token; ?>';
                        window.location.href = path;
                    } else {
                        alert('Please Select One Horse');
                    }
                }
            });
        });
        
        

         $(document).on('click','#ref-owners',function(){
            filterHorseId = $('#filterHorseId').val();

            if(filterHorseId != ''){
              // console.log(datasss);
                $.ajax({
                    method: "POST",
                    url: 'index.php?route=transaction/arrival_charges/refreshOwners&token=<?php echo $token; ?>&filterHorseId='+filterHorseId,
                    dataType: 'json',
                    success: function(json) {
                        if(json.status == 1){
                            $('.owners_datas').html('');
                            $('.owners_datas').append(json.html_owner);
                            $('.is_owners').val(json.is_owners);
                        }
                    }
                       
                        
                });
            } else {
                alert("Please Enter Date");
                return false;
            }
        });

         //partners Datassss
        $(document).on("click",".partners",function() {
            parent_owner_idss = $(this).attr('id');
            parent_owner_id = parent_owner_idss.split('_');
            horse_id = $('#filterHorseId').val();
            $.ajax({
                url: 'index.php?route=transaction/arrival_charges/getPartnersData&token=<?php echo $token; ?>&parent_owner_id='+parent_owner_id[1]+'&horse_id='+horse_id,
                dataType: 'json',
                success: function(json) {   
                    $('#partners_data_'+parent_owner_id[1]).remove();
                    $('.mod').append(json);
                    $('#partners_data_'+parent_owner_id[1]).modal('show'); 
                }
            });
        });


        //charge Histroy poup

        $(document).on("click",".chargehis",function() {
            charge_history_idss = $(this).attr('id');
            charge_history_id = charge_history_idss.split('_');
            horse_id = $('#filterHorseId').val();
            $.ajax({
                url: 'index.php?route=transaction/arrival_charges/getChargeOwnersHistory&token=<?php echo $token; ?>&charge_history_id='+charge_history_id[1]+'&horse_id='+horse_id,
                dataType: 'json',
                success: function(json) {   
                    $('#chargeownershis_data_'+charge_history_id[1]).remove();
                    $('.modalChargeHistory').append(json);
                    $('#chargeownershis_data_'+charge_history_id[1]).modal('show'); 
                }
            });
        });


        // contingency popup details 

        $(document).on("click",".modalopen",function() {
            //alert('inn');
           // console.log($(this).attr('id'));
            horse_owner_idssss = $(this).attr('id');
            horse_owner_id = horse_owner_idssss.split('_');

            $.ajax({
                url: 'index.php?route=transaction/ownership_shift_module/getContaingencyData&token=<?php echo $token; ?>&horse_owner_id='+horse_owner_id[1],
                dataType: 'json',
                success: function(json) {   
                    $('.modal-body').html('');
                    $('.modal-body').append(json);
                    $('#myModal').modal('show'); 
                }
            });
        });



        
      $('#print_latters').change(function(){
        //console.log(this.checked);
        if(this.checked){
            $('.p_l').show();
        } else {
            $('.p_l').hide();
        }
      });


        $(document).on('keydown', '#registeration_type_id, #horse_name_id, #print_latters, #remark_regi, #registration_fee_id , #reg_date, #letter_date, #letter_received', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13){
                var id = $(this).attr('id');
                if(id == 'registeration_type_id'){
                    $('#horse_name_id').focus();
                } else if(id == 'horse_name_id'){
                    $('#print_latters').focus();

                } else if(id == 'print_latters'){
                    $('#remark_regi').focus();
                    
                } else if(id == 'remark_regi'){
                    $('#registration_fee_id').focus();
                    
                }  else if(id == 'registration_fee_id'){
                    $('#reg_date').focus();
                    
                }  else if(id == 'reg_date'){
                    $('#letter_date').focus();
                    
                }  else if(id == 'letter_date'){
                    $('#letter_received').focus();
                    
                }


            }
        });

    </script>
</div>
<?php echo $footer; ?>