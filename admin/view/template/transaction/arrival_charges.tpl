<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $multi_arrival ?>" class="btn btn-primary pull-right" >Multi Arrival Charge</a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            
            <div class="panel-body">
                <form  method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <div class="form-group" >
                        <label class="col-sm-2 control-label" for="input-horse"><b style="color: red;"> * </b> <?php echo "Horse Name:"; ?></label>
                        <div class="col-sm-3">
                            <input type="text"  name="filterHorseName" id="filterHorseName" value="<?php echo $horse_name ?>" placeholder="Horse Name" class="form-control" />
                            <input type="hidden" name="filterHorseId" id="filterHorseId" value="<?php echo $horse_id ?>"  class="form-control">
                            
                            
                        </div>
                        <label class="col-sm-2 control-label" for="input-trainer"><?php echo "Trainer Name :"; ?></label>
                        <div class="col-sm-3">
                             <input type="text" name="trainer_name"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $trainer_name ?>" placeholder="Trainer Name" id="input-trainer_name" class="form-control"  readonly="readonly"/>
                            <input type="hidden" name="trainer_id" value="<?php echo $trainer_id ?>" id="trainer_id" class="form-control" />
                             <input type="hidden" name="user_id" value="<?php echo $user_id ?>" id="user_id" class="form-control" />
                            <input type="hidden" name="license_type" value="" id="license_type_id" class="license_type_id">
                            <input type="hidden"  id="is_owners" class="is_owners">

                            <br>
                            <span id="license_type"></span>

                            
                        </div>
                    </div>

                    
                    
                    <div class="form-group from-div-typeb" > 
                        <div class="form-group p_l">
                            
                            <label class="col-sm-2 control-label" for="input-reg_date"><?php echo "Date :"; ?></label>
                            <div class="col-sm-3">
                                <div class="input-group date input-reg_date">
                                    <input type="text" name="reg_date" id="reg_date" value="" data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY"  class="form-control" />
                                    <span class="input-group-btn cal-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group type-a" style="display:none;">
                            
                            <label class="col-sm-2 control-label" for="input-levy_charge" required><b style="color: red;"></b> 1 Time Levy </label>
                            <div class="col-sm-3" >
                                <input type="number" name="levy_charge" value="15000"  id="levy_charge"  class="form-control" required/>
                              <span style="display: none;color: red;font-weight: bold;" id="error_registration_fee_id" class="error"><?php echo 'Time Levy'; ?></span>
                            </div>
                        </div>

                        <div class="form-group type-b" style="display:none;" >
                            
                            <label class="col-sm-2 control-label" for="input-arrival_charge" required><b style="color: red;"></b> Arrival Charge </label>
                            <div class="col-sm-3" >
                                <input type="number" name="arrival_charge" value="15000"  id="arrival_charge"  class="form-control" required/>
                              <span style="display: none;color: red;font-weight: bold;" id="error_registration_fee_id" class="error"><?php echo 'Arrival Charge'; ?></span>
                            </div>
                        </div>

                        

                        
                        
                    <div class="form-group">
                         <div class="table-responsive owners_datas" style="border-top: 0;">
                        </div>
                    </div>

                        <a id="save" style="margin-left:50%;" class="btn btn-primary">Save</a>
                    <!-- <div class="form-group btn-save" style="margin-left:5%;" >
                    </div> -->
                       
                     <div class="form-group">
                        <div class="table-responsive pre_arrival_datas" style="border-top: 0;">
                        </div>
                    </div>


                       
                       
                    </div>
                    <input type="hidden" id="last_owner_id" value="0" class="form-control last_owner_id" />
                    <input type="hidden" id="selected_pre_owners" value="0" class="form-control selected_pre_owners" />
                </form>
            </div>
        </div>
    </div>
     <div class="mod" >
       
    </div>
     <div class="modalChargeHistory" >
       
    </div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contingency Details</h4>
                </div>
                <div class="modal-body" style="height: 240px;">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $(document).ready(function(){
        $('#filterHorseName').focus();
    })
    $('#modification').change(function(){
        //console.log(this.checked);
        if(this.checked){
            var modifications = '1';
            $("#hiddenmodify").val(modifications);
        } else {
            var modifications = '2';
            $("#hiddenmodify").val(modifications);
        }
    });
     

        $('#save').click(function(){
            datess = $('#reg_date').val();
            is_owners = $('.is_owners').val();

            if(datess != '' && is_owners == 1){
            
               datasss =  $('#form-category').serialize();
              // console.log(datasss);
                $.ajax({
                    method: "POST",
                    url: 'index.php?route=transaction/arrival_charges/arrival_datas&token=<?php echo $token; ?>',
                    dataType: 'json',
                    data: datasss,
                    success: function(json) {
                        console.log(json);
                        if(json.status == 1){
                            //location.reload(true);
                            path = 'index.php?route=transaction/send_mail_charges&token=<?php echo $token; ?>';
                            window.location.href = path;
                        }
                    }
                       
                        
                });
            } else {
                alert("Please Enter Date OR Owners");
                return false;
            }
        });
        
        $('#filterHorseName').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#filterHorseId').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/arrival_charges/autocompleteHorse&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.horse_name,
                                    value: item.horse_name,
                                    horse_id:item.horse_id,
                                    pedegree : item.pedegree,
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                //console.log(ui.item);
                $('#filterHorseName').val(ui.item.value);
                $('#filterHorseId').val(ui.item.horse_id);
                
                $('.dropdown-menu').hide();
                if(ui.item.horse_id != ''){
                $.ajax({
                        url: 'index.php?route=transaction/arrival_charges/autoHorseToTrainer&token=<?php echo $token; ?>&horse_id=' +  encodeURIComponent(ui.item.horse_id),
                        dataType: 'json',
                        cache: false,
                        success: function(json) {
                            if(json){
                                $('#input-trainer_name').val(json.trainer_name);
                                $('#trainer_id').val(json.trainer_code);
                                $('#license_type_id').val(json.license_type);
                                $('#license_type').html('');
                                $('#license_type').append("<b>Trainer lisence type </b>: " +json.license_type);
                                
                                if(json.license_type == 'A'){
                                    $('.type-a').show();
                                    $('.type-b').hide();
                                    $('.btn-save').css("display","");
                                    $('#reg_date').prop("readonly",false);
                                    $('#levy_charge').prop("readonly",false);
                                } else if(json.license_type == 'B'){
                                    $('.type-b').show();
                                    $('.type-a').hide();
                                    $('.btn-save').css("display","");
                                    $('#reg_date').prop("readonly",false);
                                    $('#arrival_charge').prop("readonly",false);
                                } else {
                                    $('.type-a').hide();
                                    $('.type-b').hide();
                                    $('.btn-save').css("display","none");
                                }
                            }

                            if(json.pre_exist == 1){
                                //if(json.is_atype == 1){
                                    $('.cal-btn').css("display", "none");
                                    $('#reg_date').attr("readonly","readonly");
                                    $('#arrival_charge').attr("readonly","readonly");
                                    $('#levy_charge').attr("readonly","readonly");
                                    $('.btn-save').css("display","none");

                                //}
                            }
                            $('.is_owners').val(json.is_owners);
                            $('.owners_datas').html('');
                            $('.owners_datas').append(json.html_owner);

                            $('.pre_arrival_datas').html('');
                            $('.pre_arrival_datas').append(json.html);

                        }
                    }); 
                }
                $('#reg_date').focus();
                return false;
            },
        });


         $(document).on('click','#ref-owners',function(){
            filterHorseId = $('#filterHorseId').val();

            if(filterHorseId != ''){
              // console.log(datasss);
                $.ajax({
                    method: "POST",
                    url: 'index.php?route=transaction/arrival_charges/refreshOwners&token=<?php echo $token; ?>&filterHorseId='+filterHorseId,
                    dataType: 'json',
                    success: function(json) {
                        if(json.status == 1){
                            $('.owners_datas').html('');
                            $('.owners_datas').append(json.html_owner);
                            $('.is_owners').val(json.is_owners);
                        }
                    }
                       
                        
                });
            } else {
                alert("Please Enter Date");
                return false;
            }
        });

         //partners Datassss
        $(document).on("click",".partners",function() {
            parent_owner_idss = $(this).attr('id');
            parent_owner_id = parent_owner_idss.split('_');
            horse_id = $('#filterHorseId').val();
            $.ajax({
                url: 'index.php?route=transaction/arrival_charges/getPartnersData&token=<?php echo $token; ?>&parent_owner_id='+parent_owner_id[1]+'&horse_id='+horse_id,
                dataType: 'json',
                success: function(json) {   
                    $('#partners_data_'+parent_owner_id[1]).remove();
                    $('.mod').append(json);
                    $('#partners_data_'+parent_owner_id[1]).modal('show'); 
                }
            });
        });


        //charge Histroy poup

        $(document).on("click",".chargehis",function() {
            charge_history_idss = $(this).attr('id');
            charge_history_id = charge_history_idss.split('_');
            horse_id = $('#filterHorseId').val();
            $.ajax({
                url: 'index.php?route=transaction/arrival_charges/getChargeOwnersHistory&token=<?php echo $token; ?>&charge_history_id='+charge_history_id[1]+'&horse_id='+horse_id,
                dataType: 'json',
                success: function(json) {   
                    $('#chargeownershis_data_'+charge_history_id[1]).remove();
                    $('.modalChargeHistory').append(json);
                    $('#chargeownershis_data_'+charge_history_id[1]).modal('show'); 
                }
            });
        });


        // contingency popup details 

        $(document).on("click",".modalopen",function() {
            //alert('inn');
           // console.log($(this).attr('id'));
            horse_owner_idssss = $(this).attr('id');
            horse_owner_id = horse_owner_idssss.split('_');

            $.ajax({
                url: 'index.php?route=transaction/ownership_shift_module/getContaingencyData&token=<?php echo $token; ?>&horse_owner_id='+horse_owner_id[1],
                dataType: 'json',
                success: function(json) {   
                    $('.modal-body').html('');
                    $('.modal-body').append(json);
                    $('#myModal').modal('show'); 
                }
            });
        });



        
      $('#print_latters').change(function(){
        //console.log(this.checked);
        if(this.checked){
            $('.p_l').show();
        } else {
            $('.p_l').hide();
        }
      });


        $(document).on('keydown', '#registeration_type_id, #horse_name_id, #print_latters, #remark_regi, #registration_fee_id , #reg_date, #letter_date, #letter_received', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13){
                var id = $(this).attr('id');
                if(id == 'registeration_type_id'){
                    $('#horse_name_id').focus();
                } else if(id == 'horse_name_id'){
                    $('#print_latters').focus();

                } else if(id == 'print_latters'){
                    $('#remark_regi').focus();
                    
                } else if(id == 'remark_regi'){
                    $('#registration_fee_id').focus();
                    
                }  else if(id == 'registration_fee_id'){
                    $('#reg_date').focus();
                    
                }  else if(id == 'reg_date'){
                    $('#letter_date').focus();
                    
                }  else if(id == 'letter_date'){
                    $('#letter_received').focus();
                    
                }


            }
        });

    </script>
</div>
<?php echo $footer; ?>