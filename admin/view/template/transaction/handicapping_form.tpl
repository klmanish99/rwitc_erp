<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $race_name; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="race_description"><?php echo "Race Description:"; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="race_description" value="<?php echo $race_description; ?>" placeholder="<?php echo "Race Description"; ?>" id="race_description" class="form-control" tabindex=""/>
                        </div>
                    </div>
                    <div class="form-group" style="background-color:#51515105;">
                        <label class="col-sm-2 control-label" for="horse_name"><?php echo "Horse Entries"; ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="horse_name" id="horse_name" value="" onkeyup="myFunction()"  placeholder="Horse Name" class="form-control">
                        </div>
                        <label class="col-sm-2 control-label" for="count"><?php echo "Selected Horse Count"; ?></label>
                        <input type="hidden" name="hide_pros_id" id="hide_pros_id" value="<?php echo $pros_id ;?>">
                        <div class="col-sm-3">
                            <input type="text" readonly="readonly" name="count" id="count" value="<?php  echo $count ;?>" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div>
                        
                    </div>
                    <div  class="form-group">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                  <td class="text-center"><?php echo "Horse Name"; ?></td>
                                    <td class="text-center"><?php echo "Rating"; ?></td>
                                    <td class="text-center"><?php echo "Age"; ?></td>
                                    <td class="text-center"><?php echo "Sex"; ?></td>
                                    <td class="text-center"><?php echo "Trainer Name"; ?></td>
                                </tr>
                            </thead>
                            <tbody id="selected_horses">
                                <?php if(isset($entrydata1s) ){  ?>
                                     <?php foreach($entrydata1s as $key => $result) { ?>
                                        <tr id="<?php echo $result['horse_id']; ?>">
                                            <td class="text-left">
                                                <span><?php echo  $result['horse_name']  ?></span>
                                                <input type="hidden" id="horse_name_select_<?php echo $result['horse_id']; ?>" name="selecthors[<?php echo $result['horse_id']; ?>][horse_name]" value="<?php echo  $result['horse_name']  ?>"  />
                                                <input type= "hidden"  name= "selecthors[<?php echo $result['horse_id']; ?>][horse_id]" id="horse_id_select_<?php echo $result['horse_id']; ?>" value = "<?php echo  $result['horse_id'];  ?>">
                                            </td>
                                            <td class="text-left">
                                                <span><?php echo  $result['horse_rating']  ?></span>
                                                <input type="hidden" id="horse_rating_select_<?php echo $result['horse_id']; ?>" name="selecthors[<?php echo $result['horse_id']; ?>][horse_rating]" value="<?php echo  $result['horse_rating'];  ?>"  />
                                            </td>
                                            <td class="text-left">
                                                <span><?php echo  $result['horse_age']  ?></span>
                                                <input type="hidden" id="horse_age_select_<?php echo $result['horse_id']; ?>" name="selecthors[<?php echo $result['horse_id']; ?>][horse_age]" value="<?php echo  $result['horse_age'] ; ?>"  />
                                            </td>
                                            <td class="text-left">
                                                <span><?php echo  $result['horse_sex']  ?></span>
                                                <input type="hidden" id="horse_sex_select_<?php echo $result['horse_id']; ?>" name="selecthors[<?php echo $result['horse_id']; ?>][horse_sex]" value="<?php echo  $result['horse_sex'] ; ?>"  />
                                            </td>
                                             <td class="text-left">
                                                <span><?php echo  $result['trainer_name']  ?></span>
                                                <input type="hidden" id="trainer_name_select_<?php echo $result['horse_id']; ?>" name="selecthors[<?php echo $result['horse_id']; ?>][trainer_name]" value="<?php echo  $result['trainer_name'];  ?>"  />
                                                <input type="hidden" id="trainer_id_select_<?php echo $result['horse_id']; ?>" name="selecthors[<?php echo $result['horse_id']; ?>][trainer_id]" value="<?php echo  $result['trainer_id'] ; ?>"  />
                                            </td>
                                        </tr>
                                     <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div id="div_scroll" style="overflow-y:scroll; overflow-x:hidden; height:400px;">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="table11">
                                <thead>
                                    <tr>
                                        <td style="width: 1px;" class="text-center"><!-- <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /> --></td>
                                        <td class="text-center"><?php echo "Horse Name"; ?></td>
                                        <td class="text-center"><?php echo "Rating"; ?></td>
                                        <td class="text-center"><?php echo "Age"; ?></td>
                                        <td class="text-center"><?php echo "Sex"; ?></td>
                                        <td class="text-center"><?php echo "Trainer Name"; ?></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if ($horseDatas) { ?>
                                        <?php foreach ($horseDatas as $key => $horseData) {  ?>
                                            <tr>
                                               <td class="text-center"><?php 
                                                if (in_array($horseData['horseID'], $abc)) { ?>
                                                <input type="checkbox"  class="checkbox_id" id="check_<?php echo $horseData['horseID']; ?>" name="selectedcheck" value="<?php echo $horseData['horseID']; ?>" checked="checked" />
                                                    <?php } else { ?>
                                                <input type="checkbox" name="selectedcheck" id="check_<?php echo $horseData['horseID']; ?>" class="checkbox_id"  value="<?php echo $horseData['horseID']; ?>" />
                                                <?php } ?></td>

                                                <td class="text-left"><?php echo $horseData['horseName']; ?>
                                                    <input type="hidden" name="hide_horse_name" id="hide_horse_name_<?php echo $horseData['horseID']; ?>" value="<?php echo $horseData['horseName'] ;?>">
                                                    <input type="hidden" name="hide_horse_id" id="hide_horse_id_<?php echo $horseData['horseID']; ?>" value="<?php echo $horseData['horseID'] ;?>">
                                                </td>
                                                 <td class="text-left"><?php echo $horseData['horseRating']; ?>
                                                  <input type="hidden" name="hide_horse_rating" id="hide_horse_rating_<?php echo $horseData['horseID']; ?>" value="<?php echo $horseData['horseRating'] ;?>">
                                                <td class="text-left"><?php echo $horseData['horseAge']; ?>
                                                    <input type="hidden" name="hide_horse_age" id="hide_horse_age_<?php echo $horseData['horseID']; ?>" value="<?php echo $horseData['horseAge'] ;?>">
                                                </td>
                                                <td class="text-left"><?php echo $horseData['horseSex']; ?>
                                                    <input type="hidden" name="hide_horse_sex" id="hide_horse_sex_<?php echo $horseData['horseID']; ?>" value="<?php echo $horseData['horseSex'] ;?>">
                                                </td>
                                                <td class="text-left"><?php echo $horseData['trainer_name']; ?>
                                                    <input type="hidden" name="hide_trainer_name" id="hide_trainer_name_<?php echo $horseData['horseID']; ?>" value="<?php echo $horseData['trainer_name'] ;?>">
                                                    <input type="hidden" name="hide_trainer_id" id="hide_trainer_id_<?php echo $horseData['horseID']; ?>" value="<?php echo $horseData['trainer_id'] ;?>">
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        
       $('.checkbox_id').change(function() {
        //var id=1;
         if(this.checked== true) {
            var valuemy = $(this).attr("id");
            var check_id= valuemy.substring(6);
            //alert(check_id);
            var hide_horse_name =  $( '#hide_horse_name_'+check_id+'' ).val();
            var hide_horse_rating =  $( '#hide_horse_rating_'+check_id+'' ).val();
            var hide_horse_age =  $( '#hide_horse_age_'+check_id+'' ).val();
            var hide_horse_sex =  $( '#hide_horse_sex_'+check_id+'' ).val();
            var hide_horse_id =  $( '#hide_horse_id_'+check_id+'' ).val();
            var hide_trainer_name =  $( '#hide_trainer_name_'+check_id+'' ).val();
            var hide_trainer_id =  $( '#hide_trainer_id_'+check_id+'' ).val();
            //var id =$( '#count' ).val();
            var id=hide_horse_id;
            //alert(id);
            /*alert(hide_horse_rating);*/
           /* alert(hide_horse_name);
            alert(hide_horse_sex);
            alert(hide_horse_id);
            alert(hide_trainer_name);*/
                html  = '';
                html += '<tr id="' + id + '">';
                    html += '<td class="text-left" >'+hide_horse_name+'';
                        html += '<input type= "hidden"  name= "selecthors['+id+'][horse_name]" id="horse_name_select_'+id+'" value = "'+hide_horse_name+'">';
                        html += '<input type= "hidden"  name= "selecthors['+id+'][horse_id]" id="horse_id_select_'+id+'" value ="'+hide_horse_id+'">';
                    html += '  </td>';
                    html += '<td class="text-left" >'+hide_horse_rating+'';
                        html += '<input type= "hidden"  name= "selecthors['+id+'][horse_rating]" id="horse_rating_select_'+id+'" value = "'+hide_horse_rating+'">';
                    html += '  </td>';
                    html += '<td class="text-left" >'+hide_horse_age+'';
                        html += '<input type= "hidden"  name= "selecthors['+id+'][horse_age]" id="horse_age_select_'+id+'" value = "'+hide_horse_age+'">';
                    html += '  </td>';
                    html += '<td class="text-left" >'+hide_horse_sex+'';
                        html += '<input type= "hidden"  name= "selecthors['+id+'][horse_sex]" id="horse_sex_select_'+id+'" value = "'+hide_horse_sex+'">';
                    html += '  </td>';
                    html += '<td class="text-left" >'+hide_trainer_name+'';
                        html += '<input type= "hidden"  name= "selecthors['+id+'][trainer_name]" id="trainer_name_select_'+id+'" value = "'+hide_trainer_name+'">';
                         html += '<input type= "hidden"  name= "selecthors['+id+'][trainer_id]" id="trainer_id_select_'+id+'" value = "'+hide_trainer_id+'">';
                    html += '  </td>';
                html += '</tr>';
                //document.getElementById("selected_horses").innerHTML = html;
                $('#selected_horses').append(html);
                 id++;
         } else {
             var valuemy = $(this).attr("id");
            var check_id= valuemy.substring(6);
           
            $('#'+check_id+'').closest("tr").remove();
         }
         var rows_count =$('#selected_horses tr').length; /*document.getElementById("selected_horses").rows.length;*/
         $('#count').val(rows_count);
           
        });
      
    </script>
    <script>
        /*$(document).ready(function(){
            $("#horse_name").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#table11 tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });*/

        function myFunction() {
          // Declare variables 
          var input, filter, table, tr, td, i;
          input = document.getElementById("horse_name");
          filter = input.value.toUpperCase();
          table = document.getElementById("table11");
          tr = table.getElementsByTagName("tr");
          // Loop through all table rows, and hide those who don't match the search query
          for (i = 1; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            } 
          }
        }
    </script>


    <script type="text/javascript">
        document.getElementById('div_scroll').scrollIntoView();
    </script>

    <script type="text/javascript">
        $('.date').datetimepicker({
            pickTime: false
        });

        $('.time').datetimepicker({
            pickDate: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });
    </script>

    <script type="text/javascript"><!--
        $('#language a:first').tab('show');
    </script>
</div>
<?php echo $footer; ?>