<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
             
                <button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title ?></h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
                    <div class="well" style="background-color: #ffffff;">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <input type="text" name="race_name" id="race_name" value="<?php  echo $filter_race_name ;?>" placeholder="Race Name" class="form-control">
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="input-group date">
                                            <input type="text" name="race_date" value="<?php  echo $filter_race_date ;?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="race_date" class="form-control" />
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <a onclick="filter()" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo "Filter"; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td style="display: none;" "width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                        <td class="text-center">Sr No</td>
                                        <td class="text-center">Race Date</td>
                                        <td class="text-center" style="width: 200px;">Race Name</td>
                                        <td class="text-center">Race Type</td>
                                        <td class="text-center" style="width: 200px;">Race Description</td>
                                        <td class="text-center">Grade/Class</td>
                                        <td class="text-center">Race Category</td>
                                        <td class="text-center">Total Stakes</td>
                                        <td class="text-center">Forgeign Jockey</td>
                                        <!-- <td class="text-center">Race Da</td> -->
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($prospectusdatas)) { ?>
                                    <?php $i=1; ?>
                                    <?php foreach ($prospectusdatas as $prospectusdata) { ?>
                                        <tr>
                                            <td style="display: none;" class="text-center"><?php if (in_array($prospectusdata['pros_id'], $selected)) { ?>
                                                <input type="checkbox" name="selected[]" value="<?php echo $prospectusdata['pros_id']; ?>" checked="checked" />
                                                  <?php } else { ?>
                                                <input type="checkbox" name="selected[]" value="<?php echo $prospectusdata['pros_id']; ?>" />
                                                <?php } ?>
                                            </td>
                                            <td class="text-left"><?php echo $i++; ?></td>
                                           <?php  $date = date('d/m/Y', strtotime($prospectusdata['race_date'])); ?>
                                            <td class="text-left"><?php echo  $date ;?></td>
                                            <td class="text-left" style="width: 200px;"><?php echo $prospectusdata['race_name']; ?></td>
                                            <td class="text-left"><?php echo $prospectusdata['race_type']; ?></td>
                                            <td class="text-left" style="width: 200px;"><?php echo $prospectusdata['race_description']; ?></td>
                                            <?php if($prospectusdata['race_type'] == 'Handicap Race'){
                                                $grade_class =$prospectusdata['class'];
                                            } 
                                            else if ($prospectusdata['race_type'] == 'Term Race'){
                                                $grade_class =$prospectusdata['grade'];
                                            }
                                            else{
                                                $grade_class='';
                                            }
                                            ?>
                                            <td class="text-left"><?php echo $grade_class; ?></td>
                                            <td class="text-left"><?php echo $prospectusdata['race_category']; ?></td>
                                            <td class="text-left"><?php echo $prospectusdata['max_stakes_race']; ?></td>
                                            <td class="text-left"><?php echo $prospectusdata['foreign_jockey_eligible']; ?></td>
                                           <!--  <td class="text-left"><?php //echo $prospectusdata['']; ?></td> -->
                                            <td class="text-center"><a href="<?php echo $prospectusdata['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function filter() { 
            var filter_race_name =$('#race_name').val();
            var filter_race_date = $('#race_date').val();
            //if(filter_race_name !='' || filter_race_date !=''){
                url = 'index.php?route=transaction/prospectus&token=<?php echo $token; ?>';
                if (filter_race_name) {
                  url += '&filter_race_name=' + encodeURIComponent(filter_race_name);
                }

                if (filter_race_date) {
                    url += '&filter_race_date=' + encodeURIComponent(filter_race_date);
                } 
                window.location.href = url;
           // }
            /* else {
                alert('Please select the filters');
                return false;
            }*/
            
        }
    </script>
    <script type="text/javascript">
        $('.date').datetimepicker({
                pickTime: false
            });

            $('.time').datetimepicker({
                pickDate: false
            });

            $('.datetime').datetimepicker({
                pickDate: true,
                pickTime: true
            });
    </script>
</div>
<?php echo $footer; ?>