<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button onclick="confirm('<?php echo 'Do You want to Save the Changes'; ?>')" type="button" form="form-category" data-toggle="tooltip" title="Save" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                      <div class="form-group">
                        <div class="col-sm-3" >
                            <input type="text" name="filter_jockey" value="<?php echo $filter_jockey; ?>" placeholder="<?php echo "Jockey Name"; ?>" id="input-filter_jockey" class="form-control" />
                        </div>
                        <div class="col-sm-3" >
                            
                        </div>
                        <div class="col-sm-2 pull-right" >
                            <button onclick="filter();" style="" type="button" id="button-filter" class="btn btn-primary " ><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
                        </div>
                      </div>
                    </div>
                </div>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <div class="form-group" >
                        <div class="col-sm-1"></div>
                        <span class="col-sm-4 control-label" for="input-horse" style="font-size: 1.3em;"><b>Season: </b>  <span style="margin-left: 2%;">  <?php echo $season_start ?></span>  <span style="margin-left: 2%;">  - To - <?php echo $season_end ?></span></span>
                        <span class="col-sm-5 control-label" style="font-size: 1.3em;" ><b>Current Date - </b><?php echo $current_date ?></span>
                        <input type="hidden" name="season_start" value="<?php echo $season_start ?>">
                        <input type="hidden" name="season_end" value="<?php echo $season_end ?>">
                    </div>


                    <div class="form-group">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                     <th  class="text-center"><input type="checkbox" onclick="$('input[class*=\'jockey_selected\']').prop('checked', this.checked);" /></th>
                                    <th class="text-center">Sr.No</th>
                                    <th class="text-center">Jockey Name</th>
                                    <th class="text-center">Age</th>
                                    <th class="text-center">Fees Type</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">License Start Date</th>
                                    <th class="text-center">License End Date</th>
                                    <th class="text-center">License Type</th>
                                    <th class="text-center">Apprenties</th>
                                </tr>

                            </thead>
                            <tbody>
                                <?php
                                $sr_no = 1;
                                 foreach ($jockey_data as $jkey => $jvalue) {  ?>
                                    <tr>
                                        <td class="text-center">
                                            <input  style="margin-left: 20%;" type="checkbox" name="jockey_datas[<?php echo $jkey ?>][selected_jockey]" class="form-control jockey_selected">
                                        </td>
                                        <td><?php echo $sr_no  ?></td>
                                        <td>
                                            <input  type="text" name="jockey_datas[<?php echo $jkey ?>][jockey_name]" value="<?php echo  $jvalue['jockey_name'] ?>" class="form-control" readonly />
                                            <input  type="hidden" name="jockey_datas[<?php echo $jkey ?>][jockey_id]" value="<?php echo  $jvalue['jockey_id'] ?>" class="form-control" readonly />
                                        </td>
                                        <td>
                                            <?php if ($jvalue['age'] >= 55) { ?>
                                                <input style="color: red" type="text" name="jockey_datas[<?php echo $jkey ?>][age]" value="<?php echo  $jvalue['age'] ?>" class="form-control" />
                                            <?php } else{ ?>
                                                <input type="text" name="jockey_datas[<?php echo $jkey ?>][age]" value="<?php echo  $jvalue['age'] ?>" class="form-control" />
                                            <?php  } ?>
                                        </td>
                                        <td>
                                            <select name="jockey_datas[<?php echo $jkey ?>][fees_type]" id="fees_type" class="form-control ent-evnt">
                                                <option value="" >Please Select</option>    
                                                <?php foreach ($fees_types as $fkey => $fvalue) {
                                                    if($fvalue == $fees_type){
                                                 ?> 
                                                    
                                                    <option value="<?php echo $fvalue; ?>" selected="selected" ><?php echo $fvalue; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $fvalue; ?>" ><?php echo $fvalue; ?></option>
                                                <?php } ?>


                                                <?php } ?>

                                            </select>
                                        </td>
                                        <td class="text-right" >
                                            <input style="text-align: right;" id="amt_<?php echo $jkey; ?>" type="text" name="jockey_datas[<?php echo $jkey ?>][amt]" value="<?php echo $jvalue['amount'] ?>" class="form-control">
                                        </td>
                                        <td>
                                            <div class="input-group date">
                                                <input type="text" name="jockey_datas[<?php echo $jkey ?>][lisence_start_date]" value="<?php echo $jvalue['lisence_start_date'] ?>"  placeholder="Date" data-date-format="DD-MM-YYYY" id="input-lisence_start_date" class="form-control" />
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                         </td>
                                        <td>
                                            <div class="input-group date">
                                                <input type="text" name="jockey_datas[<?php echo $jkey ?>][lisence_end_date]" value="<?php echo $jvalue['lisence_end_date'] ?>"  placeholder="Date" data-date-format="DD-MM-YYYY" id="input-lisence_end_date" class="form-control" />
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>

                                        </td>

                                        <td>
                                            <select name="jockey_datas[<?php echo $jkey ?>][lisence_type]" id="lisence_type" class="form-control ent-evnt">
                                                <option value="" >Please Select</option>    
                                                <?php foreach ($lisence_types as $lkey => $lvalue) { ?> 
                                                    <option value="<?php echo $lvalue; ?>" ><?php echo $lvalue; ?></option>

                                                <?php } ?>

                                            </select>
                                        </td>
                                        <td class="text-center">
                                            <input id="apprenties_<?php echo $jkey ?>" type="checkbox" name="jockey_datas[<?php echo $jkey ?>][apprenties]" class="form-control text-center apprenties" style="margin-left: 43%;">
                                        </td>
                                        

                                    </tr>

                                <?php $sr_no++; } ?>
                            </tbody>
                        </table>
                    </div>
                    
                    
                    <div class="text-center" > 
                        <button onclick="confirm('<?php echo 'Do You want to Save the Changes'; ?>')" type="button" form="form-category" data-toggle="tooltip" title="Save" class="btn btn-primary"> Save <i class="fa fa-save"></i></button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    
    $('.apprenties').click(function() {
        if($(this).prop("checked") == true){
            appr_amt = '<?php echo APPRENTIES_AMT ?>';
            idss = $(this).attr('id');
            id = idss.split('_');
            $("#amt_"+id[1]).val(appr_amt);
        } else {
            normal_amt = '<?php echo NORMAL_AMT ?>';
            idss = $(this).attr('id');
            id = idss.split('_');
            $("#amt_"+id[1]).val(normal_amt);
        }
    });

    </script>
     <script>
    function filter() {
        var url = 'index.php?route=transaction/jockey_lisence_trans&token=<?php echo $token; ?>';

        var filter_jockey = $('input[name=\'filter_jockey\']').val();

        if (filter_jockey) {
            url += '&filter_jockey=' + encodeURIComponent(filter_jockey);
        }

        location = url;
    }
    </script>
    <script type="text/javascript">
        function confirm() {
            console.log($('.jockey_selected').prop('checked') );
            if ($('.jockey_selected').prop('checked') == true) {
                $('#form-category').submit();
            } else {
                alert('Please select one');
            }
        }
    </script>
</div>
<?php echo $footer; ?>