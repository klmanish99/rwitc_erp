<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
    </div>

    <div class="container-fluid">
        
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clipboard"></i> <?php echo $text_list; ?></h3>
            </div>


            <div class="panel-body">
                <form action="<?php $filter ?>" method="get" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    
                    <?php if($final_datas){ ?>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 1px;" class="text-center"></th>
                                    <th class="text-center">Sr.No.</th>
                                    <th class="text-center">Horse Name</th>
                                    <th class="text-center">Ownership</th>
                                    <th class="text-center" >Trainer</th>
                                    <th class="text-center">Charge Type</th>
                                    <th class="text-center">Charge Amount</th>
                                    <!-- <th>Action</th>
     -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sr_no = 1;
                                 foreach ($final_datas as $hkey => $hvalue) {  ?>
                                    <tr>
                                        <td class="text-center"><i class="fa fa-check" aria-hidden="true" style="color: green;font-size: 20px;"></i> 
                                            <input type="hidden" name="trans_id[]" value="<?php echo $hvalue['trans_id'] ?>">

                                        </td>
                                        <td><?php echo $sr_no ?></td>
                                        <td><?php echo $hvalue['horse_name'] ?> </td>
                                        <td ><?php echo $hvalue['owner_name'] ?></td>
                                        <td><?php echo $hvalue['trainer_name'] ?> </td>
                                        <td> <?php echo $hvalue['charge_type'] ?> </td>
                                        <td class="text-right"> <?php echo $hvalue['charge_amt'] ?> </td>
                                    </tr>
                                <?php $sr_no++; } ?>
                                <input type="hidden" name="submit_type" value="arrival_charge">

                            </tbody>
                        </table>
                    <?php } ?>

                    <?php if($jockey_datas){ ?>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 1px;" class="text-center"></th>
                                    <th class="text-center">Sr.No.</th>
                                    <th class="text-center">Jockey Name</th>
                                    <th class="text-center">Fees Type</th>
                                    <th class="text-center" >Amount</th>
                                    <th class="text-center">License Start Date</th>
                                    <th class="text-center">License End Date</th>
                                    <th class="text-center">License Type</th>
                                    <th class="text-center">Apprenties</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sr_no = 1;
                                 foreach ($jockey_datas as $hkey => $hvalue) {  ?>
                                    <tr>
                                        <td class="text-center">
                                            <i class="fa fa-check" aria-hidden="true" style="color: green;font-size: 20px;"></i> 
                                            <input type="hidden" name="trans_id[]" value="<?php echo $hvalue['trans_id'] ?>">

                                        </td>
                                        <td><?php echo $sr_no ?></td>
                                        <td><?php echo $hvalue['jockey_name'] ?> </td>
                                        <td ><?php echo $hvalue['fees_type'] ?></td>
                                        <td><?php echo $hvalue['amount'] ?> </td>
                                        <td> <?php echo $hvalue['license_start_date'] ?> </td>
                                        <td> <?php echo $hvalue['license_end_date'] ?> </td>
                                        <td> <?php echo $hvalue['license_type'] ?> </td>
                                        <td class="text-center">
                                            <?php if($hvalue['apprenties'] == 'Yes'){ ?>
                                                <i class="fa fa-check" aria-hidden="true" style="color: green;font-size: 20px;"></i> 
                                            <?php } else { ?>
                                                <i></i>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php $sr_no++; } ?>
                                <input type="hidden" name="submit_type" value="jockey_license_trans">
                            </tbody>
                        </table>
                    <?php } ?>


                     <?php if($trainer_datas){ ?>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 1px;" class="text-center"></th>
                                    <th class="text-center">Sr.No.</th>
                                    <th class="text-center">Trainer Name</th>
                                    <th class="text-center">Fees Type</th>
                                    <th class="text-center" >Amount</th>
                                    <th class="text-center">License Start Date</th>
                                    <th class="text-center">License End Date</th>
                                    <th class="text-center">License Type</th>
                                    <th class="text-center">Apprenties</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sr_no = 1;
                                 foreach ($trainer_datas as $hkey => $hvalue) {  ?>
                                    <tr>
                                        <td class="text-center">
                                            <i class="fa fa-check" aria-hidden="true" style="color: green;font-size: 20px;"></i> 
                                            <input type="hidden" name="trans_id[]" value="<?php echo $hvalue['trans_id'] ?>">

                                        </td>
                                        <td><?php echo $sr_no ?></td>
                                        <td><?php echo $hvalue['trainer_name'] ?> </td>
                                        <td ><?php echo $hvalue['fees_type'] ?></td>
                                        <td><?php echo $hvalue['amount'] ?> </td>
                                        <td> <?php echo $hvalue['license_start_date'] ?> </td>
                                        <td> <?php echo $hvalue['license_end_date'] ?> </td>
                                        <td> <?php echo $hvalue['license_type'] ?> </td>
                                        <td class="text-center">
                                            <?php if($hvalue['apprenties'] == 'Yes'){ ?>
                                                <i class="fa fa-check" aria-hidden="true" style="color: green;font-size: 20px;"></i> 
                                            <?php } else { ?>
                                                <i></i>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php $sr_no++; } ?>
                                <input type="hidden" name="submit_type" value="trainer_license_trans">
                            </tbody>
                        </table>
                    <?php } ?>



                    <a id="sv"  style="margin-left:50%;display: none;" class="btn btn-primary save">Send Mail</a>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $(document).ready(function(){
        $('#filterHorseName').focus();
        is_fil = '<?php echo $is_filter ?>';
        if(is_fil == 1){
            $('#sv').show();
        } else {
            $('#sv').hide();
        }
    })
    
    $('.save').click(function(){
       datasss =  $('#form-category').serialize();
       //console.log(datasss);
       //return false;
        $.ajax({
            method: "POST",
            url: 'index.php?route=transaction/send_mail_charges/insert_datas&token=<?php echo $token; ?>',
            dataType: 'json',
            data: datasss,
            success: function(json) {
                if(json.status == 1){
                    if(json.pp == 'AC'){
                        //location.reload(true);
                        path = 'index.php?route=transaction/multi_arrival_charges&token=<?php echo $token; ?>';
                        window.location.href = path;
                    } else if(json.pp == 'JLT') {
                        path = 'index.php?route=transaction/jockey_lisence_trans&token=<?php echo $token; ?>';
                        window.location.href = path;
                    } else if(json.pp == 'TLT') {
                        path = 'index.php?route=transaction/trainer_license_trans&token=<?php echo $token; ?>';
                        window.location.href = path;
                    }
                }
            }
        });
    });
        
    </script>
</div>
<?php echo $footer; ?>