<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <ul class="nav nav-tabs"  id="myTab">
                        <li class="active"><a data-index="1" tabindex="1" href="#tab_Prospectus_Details" data-toggle="tab">Prospectus Details</a></li>
                        <li><a data-index="2" tabindex="2" href="#tab_Race_Eligibility_Criteria" data-toggle="tab">Race Eligibility Criteria</a></li>
                        <li><a data-index="3" tabindex="3" href="#tab_Race_Stakes_Details" data-toggle="tab">Race Stakes Details</a></li>
                         <li id="hide_tab_sweepstake_race" style="display: none;"><a data-index="4" tabindex="4" href="#tab_sweepstake_race" data-toggle="tab">Sweepstakes Race</a></li>
            
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_Prospectus_Details">
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-race_date"><?php echo "Race Date"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date input-race_date">
                                        <input type="text" name="race_date" value="<?php echo $race_date; ?>" data-index="4" placeholder="Race Date" data-date-format="DD-MM-YYYY" id="race_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" id="btn_race_date" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <button class="btn btn-default" id="check" type="button">Check</button>
                                    <?php if (isset($valierr_race_date)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_race_date; ?></span><?php } ?>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_race_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>
                                <label class="col-sm-2 control-label" for="input-serial_no"><?php echo "Serial Number"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date input-serial_no">
                                        <input type="text" name="serial_no" value="<?php echo $serial_no; ?>" placeholder="Serial Number" id="serial_no" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="season"><?php echo "Season:"; ?></label>
                                <div class="col-sm-3">
                                    <select name="season" id="season" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($seasons as $skey => $svalue) { ?>
                                        <?php if ($skey == $season) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label class="col-sm-2 control-label" for="year">Year</label>
                                <div class="col-sm-3">
                                    <input type="text" readonly="readonly" name="year" value="<?php echo $year; ?>" placeholder="<?php echo "Year"; ?>" id="year" class="form-control" tabindex="3"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="race_day"><?php echo "Race Day:"; ?></label>
                                <div class="col-sm-3">
                                    <select name="race_day" id="race_day" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($days as $skey => $svalue) { ?>
                                        <?php if ($skey == $race_day) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label class="col-sm-2 control-label" for="evening_race"><?php echo "Evening Race:"; ?></label>
                                <div class="col-sm-3" style="padding-top: 10px;">
                                    <?php if($evening_race == 'Yes' ){ ?>
                                        <input type="checkbox" name="evening_race" class="form-control" id="evening_race" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="evening_race" class="form-control" id="evening_race">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="race_name">Race Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="race_name" value="<?php echo $race_name; ?>" placeholder="<?php echo "Race Name"; ?>" id="race_name" class="form-control" tabindex=""/> <?php if (isset($valierr_race_name)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_race_name; ?></span><?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="race_type"><?php echo "Race Type:"; ?></label>
                                <div class="col-sm-3">
                                    <select name="race_type" id="race_type" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($race_types as $skey => $svalue) { ?>
                                        <?php if ($skey == $race_type) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div id="hide_class" style="display: none;">
                                    <label class="col-sm-2 control-label" for="class"><?php echo "Class:"; ?></label>
                                    <div class="col-sm-3">
                                        <select name="class" id="class" class="form-control"  tabindex="">
                                            <option value="<?php  ?>">Please Select</option>
                                            <?php foreach ($classs as $skey => $svalue) { ?>
                                            <?php if ($skey == $class) { ?>
                                            <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="hide_grade" style="display: none;">
                                    <label class="col-sm-2 control-label" for="grade"><?php echo "Grade:"; ?></label>
                                    <div class="col-sm-3">
                                        <select name="grade" id="grade" class="form-control"  tabindex="">
                                            <option value="<?php  ?>">Please Select</option>
                                            <?php foreach ($terms as $skey => $svalue) { ?>
                                            <?php if ($svalue == $grade) { ?>
                                            <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="race_category">Race Category</label>
                                <div class="col-sm-3">
                                    <input type="text" name="race_category" value="<?php echo $race_category; ?>" placeholder="<?php echo "Race Category"; ?>" id="race_category" class="form-control" tabindex=""/>
                                </div>
                                <div id="hide_lower_class_aligible" >
                                    <label class="col-sm-2 control-label" for="lower_class_aligible"><?php echo "Lower Class Aligible:"; ?></label>
                                    <div class="col-sm-3" style="padding-top: 10px;">
                                        <?php if($lower_class_aligible == 'Yes') { ?>
                                            <input type="checkbox" name="lower_class_aligible" class="form-control" id="lower_class_aligible" checked="checked">
                                        <?php } else { ?>
                                            <input type="checkbox" name="lower_class_aligible" class="form-control" id="lower_class_aligible">

                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="race_description">Race Description</label>
                                <div class="col-sm-8">
                                    <input type="text" name="race_description" value="<?php echo $race_description; ?>" placeholder="<?php echo "Race Description"; ?>" id="race_description" class="form-control" tabindex=""/>
                                </div>
                            </div>
                            <div id="rating_from_to">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="rating_from">Rating From</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date input-rating_from">
                                            <input type="number" name="rating_from" value="<?php echo $rating_from?>" placeholder="Rating From"  id="rating_from" class="form-control" />
                                        </div>
                                    </div>
                                    <label class="col-sm-2 control-label" for="rating_to">Rating to</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date input-rating_to">
                                            <input type="number" name="rating_to" value="<?php echo $rating_to?>" placeholder="Rating To"  id="rating_to" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="distance"><?php echo "Distance:"; ?></label>
                                <div class="col-sm-3">
                                    <select name="distance" id="distance" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($distances as $skey => $svalue) { ?>
                                        <?php if ($skey == $distance) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-entries_close_date"><?php echo "Entries Close Date"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date input-entries_close_date">
                                        <input type="text" name="entries_close_date" value="<?php echo $entries_close_date; ?>" data-index="4" placeholder="Entries Close Date" data-date-format="DD-MM-YYYY" id="entries_close_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <?php if (isset($valierr_greater_race_date)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_greater_race_date; ?></span><?php } ?>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_entries_close_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-3">
                                    <select name="entries_close_date_time" id="entries_close_date_time" class="form-control"  tabindex="">
                                        <option value="">Please Select</option>
                                        <?php foreach ($times as $skey => $svalue) { ?>
                                        <?php if ($skey == $entries_close_date_time) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-handicaps_date"><?php echo "Handicap's Date"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date input-handicaps_date">
                                        <input type="text" name="handicaps_date" value="<?php echo $handicaps_date; ?>" data-index="4" placeholder="Handicap's Date" data-date-format="DD-MM-YYYY" id="handicaps_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <?php if (isset($valierr_greater_handicaps_date)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_greater_handicaps_date; ?></span><?php } ?>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_handicaps_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-3">
                                    <select name="handicaps_date_time" id="handicaps_date_time" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($times as $skey => $svalue) { ?>
                                        <?php if ($skey == $handicaps_date_time) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-acceptance_date"><?php echo "Acceptance's Date"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date input-acceptance_date">
                                        <input type="text" name="acceptance_date" value="<?php echo $acceptance_date; ?>" data-index="4" placeholder="Acceptance's Date" data-date-format="DD-MM-YYYY" id="acceptance_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <?php if (isset($valierr_greater_acceptance_date)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_greater_acceptance_date; ?></span><?php } ?>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_acceptance_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-3">
                                    <select name="acceptance_date_time" id="acceptance_date_time" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($times as $skey => $svalue) { ?>
                                        <?php if ($skey == $acceptance_date_time) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-declaration_date"><?php echo "Declaration Date"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date input-declaration_date">
                                        <input type="text" name="declaration_date" value="<?php echo $declaration_date; ?>" data-index="4" placeholder="Declaration Date" data-date-format="DD-MM-YYYY" id="declaration_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <?php if (isset($valierr_greater_declaration_date)) { ?><span class="errors" style="color: #ff0000;"><?php echo $valierr_greater_declaration_date; ?></span><?php } ?>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_declaration_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>
                                <div class="col-sm-2"></div>

                                <div class="col-sm-3">
                                    <select name="declaration_date_time" id="declaration_date_time" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($times as $skey => $svalue) { ?>
                                        <?php if ($skey == $declaration_date_time) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_Race_Eligibility_Criteria">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-age_from"><?php echo "Age From"; ?></label>
                                <div class="col-sm-3">
                                    <input type="Number" name="age_from" value="<?php echo $age_from;  ?>" placeholder="Age From" id="age_from" class="form-control" tabindex="" />
                                </div>
                                <label class="col-sm-2 control-label" for="input-age_to"><?php echo "Age To"; ?></label>
                                <div class="col-sm-3">
                                    <input type="Number" name="age_to" value="<?php echo $age_to;  ?>" placeholder="Age To" id="age_to" class="form-control" tabindex=""/>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-max_win"><?php echo "Max Win"; ?></label>
                                <div class="col-sm-3">
                                    <input type="Number" name="max_win" value="<?php echo $max_win;   ?>" placeholder="Max Win" id="max_win" class="form-control" tabindex="" />
                                    
                                </div>
                                <label class="col-sm-2 control-label" for="input-sex"><?php echo "Sex"; ?></label>
                                <div class="col-sm-3">
                                    <select name="sex" id="sex" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($sexs as $skey => $svalue) { ?>
                                        <?php if ($skey == $sex) { ?>
                                        <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="maidens"><?php echo "Maidens"; ?></label>
                                <div class="col-sm-2" style="padding-top: 10px;">
                                    <?php if($maidens == 'Yes' ){ ?>
                                        <input type="checkbox" name="maidens" class="form-control" id="maidens" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="maidens" class="form-control" id="maidens">
                                    <?php } ?>

                                </div>
                                <label class="col-sm-1 control-label" for="unraced"><?php echo "Unraced"; ?></label>
                                <div class="col-sm-2" style="padding-top: 10px;">
                                    <?php if($unraced == 'Yes' ){ ?>
                                        <input type="checkbox" name="unraced" class="form-control" id="unraced" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="unraced" class="form-control" id="unraced">
    
                                    <?php } ?>
                                </div>
                                <label class="col-sm-1 control-label" for="unplaced"><?php echo "Unplaced"; ?></label>
                                <div class="col-sm-2"  style="padding-top: 10px;">
                                    <?php if($unplaced == 'Yes' ){ ?>
                                     <input type="checkbox" name="unplaced" class="form-control" id="unplaced" checked="checked">
                                     <?php } else { ?>
                                     <input type="checkbox" name="unplaced" class="form-control" id="unplaced">
    
                                      <?php } ?>  
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-not1_2_3"><?php echo "Not 1/2/3"; ?></label>
                                <div class="col-sm-1"  style="padding-top: 10px;">
                                    <?php if($not1_2_3 == 'Yes' ){ ?>
                                     <input type="checkbox" name="not1_2_3" class="form-control" id="not1_2_3" checked="checked">
                                     <?php } else { ?>
                                     <input type="checkbox" name="not1_2_3" class="form-control" id="not1_2_3">
    
                                      <?php } ?>  
                                </div>
                                <label class="col-sm-2 control-label" for="max_stakes_race">Max Stakes In A Race</label>
                                <div class="col-sm-3">
                                    <input type="Number" name="max_stakes_race" value="<?php echo $max_stakes_race; ?>" placeholder="<?php echo "Max Stakes In A Race"; ?>" id="max_stakes_race" class="form-control" tabindex=""/>
                                </div>
                                
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="run_thrice"><?php echo "Run Thrice But Not 1/2/3/4 from Configrable Date:"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date input-run_thrice_date">
                                        <input type="text" name="run_thrice_date" value="<?php echo $run_thrice_date; ?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="run_thrice_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_run_thrice_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>
                                <label class="col-sm-2 control-label" for="run_not_placed"><?php echo "Run Not Placed 1/2/3 from Configrable Date:"; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date input-run_not_placed_date">
                                        <input type="text" name="run_not_placed_date" value="<?php echo $run_not_placed_date; ?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="run_not_placed_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_run_not_placed_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="run_not_won"><?php echo "Run Not Won From Configrable Date:"; ?></label>
                                
                                <div class="col-sm-3">
                                    <div class="input-group date input-run_not_won_date">
                                        <input type="text" name="run_not_won_date" value="<?php echo $run_not_won_date; ?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="run_not_won_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_run_not_won_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>

                                <label class="col-sm-2 control-label" for="run_and_not_won_during_mtg"><?php echo "Run And Not Won During This MTG:"; ?></label>
                                
                                <div class="col-sm-3">
                                    <div class="input-group date input-run_and_not_won_during_mtg_date">
                                        <input type="text" name="run_and_not_won_during_mtg_date" value="<?php echo $run_and_not_won_during_mtg_date; ?>" data-index="4" placeholder="Date" data-date-format="DD-MM-YYYY" id="run_and_not_won_during_mtg_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <span style="display: none;color: red;font-weight: bold;" id="error_run_and_not_won_during_mtg_date" class="error"><?php echo 'Please Select Valid Date'; ?></span>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="no_whip"><?php echo "No Whip:"; ?></label>
                                <div class="col-sm-1" style="padding-top: 10px;">
                                    <?php if($no_whip == 'Yes' ){ ?>
                                        <input type="checkbox" name="no_whip" class="form-control" id="no_whip" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="no_whip" class="form-control" id="no_whip">

                                    <?php } ?>
                                </div>
                                <label class="col-sm-2 control-label" for="foreign_jockey_eligible"><?php echo "Foreign Jockey Eligible:"; ?></label>
                                <div class="col-sm-1" style="padding-top: 10px;">
                                    <?php if($foreign_jockey_eligible == 'Yes' ){ ?>
                                        <input type="checkbox" name="foreign_jockey_eligible" class="form-control" id="foreign_jockey_eligible" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="foreign_jockey_eligible" class="form-control" id="foreign_jockey_eligible">
                                    <?php } ?>
                                </div>
                                <label class="col-sm-2 control-label" for="foreign_horse_eligible"><?php echo "Foreign Horse Eligible:"; ?></label>
                                <div class="col-sm-1" style="padding-top: 10px;">
                                    <?php if($foreign_horse_eligible == 'Yes' ){ ?>
                                        <input type="checkbox" name="foreign_horse_eligible" class="form-control" id="foreign_horse_eligible" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="foreign_horse_eligible" class="form-control" id="foreign_horse_eligible">
                                    <?php } ?> 
                                    
                                </div>

                                <label class="col-sm-2 control-label" for="sire_dam"><?php echo "Sire/Dam:"; ?></label>
                                <div class="col-sm-1" style="padding-top: 10px;">
                                    <?php if($sire_dam == 'Yes' ){ ?>
                                        <input type="checkbox" name="sire_dam" class="form-control" id="sire_dam" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="sire_dam" class="form-control" id="sire_dam">

                                    <?php } ?>

                                    
                                </div>
                            </div>
                            
                            
                        </div>
                        <div class="tab-pane " id="tab_Race_Stakes_Details">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="stakes_in"><?php echo "Stakes In:"; ?></label>
                                <div class="col-sm-2">
                                    <select name="stakes_in" id="stakes_in" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($stakes as $skey => $svalue) { ?>
                                        <?php if ($skey == $stakes_in) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>

                                <label class="col-sm-3 control-label resize_div" for="positions"><?php echo "Positions :"; ?></label>
                                <div class="col-sm-3 resize_div">
                                    <select name="positions" id="positions" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($positionss as $skey => $svalue) { ?>
                                        <?php if ($skey == $positions) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                
                                <label class="col-sm-2 control-label stacks_per_val" for="stakes_in"><?php echo "Stacks % :"; ?></label>
                                <div class="col-sm-1 stacks_per_val">
                                    <input type="number" name="stakes_per" value="<?php echo $stakes_per ?>" class="form-control" id="stakes_per" min="1">
                                </div>

                                <div class="col-sm-1 stacks_per_val" style="font-size: 20px; color: #1e91cf">
                                   <i class="fa fa-refresh " aria-hidden="true" id="refresh_btn" ></i>
                                </div>

                                
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label " for="trophy_value_amount">Trophy Value Amount:</label>
                                <div class="col-sm-3 resize_div">
                                    <input type="Number" name="trophy_value_amount" value="<?php echo $trophy_value_amount; ?>" placeholder="<?php echo "Trophy Value Amount"; ?>" id="trophy_value_amount" class="form-control" tabindex=""/>
                                </div>

                                <label class="col-sm-2 control-label" for="stakes_money">Stakes Money:</label>
                                <div class="col-sm-3 resize_div">
                                    <input type="Number" name="stakes_money" value="<?php echo $stakes_money; ?>" placeholder="<?php echo "Stakes Money"; ?>" id="stakes_money" class="form-control" tabindex=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="bonus_applicable"><?php echo "Bonus Applicable:"; ?></label>
                                <div class="col-sm-3" style="padding-top: 10px;">
                                    <?php if($bonus_applicable == 'Yes' ){ ?>
                                        <input type="checkbox" name="bonus_applicable" class="form-control" id="bonus_applicable" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="bonus_applicable" class="form-control" id="bonus_applicable">

                                    <?php } ?>
                                    
                                </div>
                                <div  id="hide_sweepstake_race_textbox" style="display: none;">
                                    <label class="col-sm-2 control-label" for="sweepstake_race">Sweepstake Race:</label>
                                    <div class="col-sm-3">
                                        <select name="sweepstake_race" id="sweepstake_race" class="form-control"  tabindex="">
                                            <option value="<?php  ?>">Please Select</option>
                                            <?php foreach ($type as $skey => $svalue) { ?>
                                            <?php if ($skey == $sweepstake_race) { ?>
                                            <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="onclick_sweepstake_race" style="display: none;">
                                <div class="form-group">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td class="text-center" ><?php echo "Placing"; ?></td>
                                                <td class="text-center" ><?php echo "Owner%"; ?></td>
                                                <td class="text-center" ><?php echo "Owner"; ?></td>
                                                <td class="text-center" ><?php echo "Trainer%"; ?></td>
                                                <td class="text-center" ><?php echo "Trainer"; ?></td>
                                                <td class="text-center" ><?php echo "Jockey%"; ?></td>
                                                <td class="text-center" ><?php echo "Jockey"; ?></td>
                                            </tr>
                                        </thead>
                                        <tbody id="sweepstake_race_table">
                                             <?php if(isset($prize_distributions) ){  ?>
                                                 <?php foreach($prize_distributions as $key => $result) { ?>
                                                    <tr id="id<?php echo $key; ?>">
                                                        <td class="text-left">
                                                            <span><?php echo  $result['placing']  ?></span>
                                                            <input type="hidden" id="placing_<?php  echo $key; ?>" name="race[<?php echo $key; ?>][placing]" value="<?php echo  $result['placing']  ?>"  />
                                                        </td>
                                                        <td class="text-left">
                                                            <!-- <span><?php echo  $result['owner_per']  ?></span> -->
                                                            <input type="Number" id="owner_per_<?php echo $key; ?>" name="race[<?php echo $key; ?>][owner_per]" value="<?php echo  $result['owner_per']  ?>" onfocusout="calculate('<?php echo $key; ?>')" placeholder="Owner %" class="form-control" />
                                                        </td>
                                                        <td class="text-left">
                                                            <span id="owner_<?php echo $key; ?>"><?php echo  $result['owner']  ?></span>
                                                            <input type="hidden" id="hide_owner_<?php echo $key; ?>" name="race[<?php echo $key; ?>][owner]" value="<?php echo  $result['owner']  ?>"  />
                                                        </td>
                                                        <td class="text-left">
                                                            <span id="trainer_perc_<?php echo $key; ?>"><?php echo  $result['trainer_perc']  ?></span>
                                                            <input type="hidden" id="hide_trainer_perc_<?php echo $key; ?>" name="race[<?php echo $key; ?>][trainer_perc]" value="<?php echo  $result['trainer_perc']  ?>"  />
                                                        </td>
                                                        <td class="text-left">
                                                            <span id="trainer_<?php echo $key; ?>"><?php echo  $result['trainer']  ?></span>
                                                            <input type="hidden" id="hide_trainer_<?php echo $key; ?>" name="race[<?php echo $key; ?>][trainer]" value="<?php echo  $result['trainer']  ?>"  />
                                                        </td>
                                                        <td class="text-left">
                                                            <span id="jockey_perc_<?php echo $key; ?>"><?php echo  $result['jockey_perc']  ?></span>
                                                            <input type="hidden" id="hide_jockey_perc_<?php echo $key; ?>" name="race[<?php echo $key; ?>][jockey_perc]" value="<?php echo  $result['jockey_perc']  ?>"  />
                                                        </td>
                                                        <td class="text-left">
                                                            <span id="jockey_<?php echo $key; ?>"><?php echo  $result['jockey']  ?></span>
                                                            <input type="hidden" id="hide_jockey_<?php echo $key; ?>" name="race[<?php echo $key; ?>][jockey]" value="<?php echo  $result['jockey']  ?>"  />
                                                        </td>
                                                    </tr>

                                            
                                                 <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="rs_select_race" style="display: none;">
                                <div class="form-group">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td class="text-center" style="vertical-align: middle;" >Placing</td>
                                                <td class="text-center" style="vertical-align: middle;" >Owner <br> (Rs.)</td>
                                                <td class="text-center" style="vertical-align: middle;" >Trainer <br>(Rs.)</td>
                                                <td class="text-center" style="vertical-align: middle;" >Jockey <br>(Rs.)</td>
                                                <td class="text-center" style="vertical-align: middle;" >Total <br> (Rs.)</td>
                                                <td class="text-center" style="vertical-align: middle;" >Performance <br> Incentive<br> (Rs.)</td>

                                            </tr>
                                        </thead>
                                        <tbody id="race_rs_table">
                                             <?php if(isset($stacks_value) ){  ?>
                                                 <?php foreach($stacks_value as $key => $result) { ?>
                                                    <tr id="id<?php echo $key; ?>">
                                                        <td class="text-left">
                                                            <span><?php echo  $result['placing']  ?></span>
                                                            <input type="hidden" id="placing_<?php  echo $key; ?>" name="race_rs[<?php echo $key; ?>][placing]" value="<?php echo  $result['placing']  ?>"  />
                                                        </td>
                                                        <td class="text-right">
                                                             <span><?php echo  $result['owner']  ?></span>
                                                            <input type="hidden" id="owner_per_<?php echo $key; ?>" name="race_rs[<?php echo $key; ?>][owner_per]" value="<?php echo  $result['owner_per']  ?>" onfocusout="calculate('<?php echo $key; ?>')" placeholder="Owner %" class="form-control" />
                                                        </td>
                                                        
                                                        <td class="text-right">
                                                            <span id="trainer_perc_<?php echo $key; ?>"><?php echo  $result['trainer']  ?></span>
                                                            <input type="hidden" id="hide_trainer_perc_<?php echo $key; ?>" name="race_rs[<?php echo $key; ?>][trainer_perc]" value="<?php echo  $result['trainer']  ?>"  />
                                                        </td>
                                                        
                                                        <td class="text-right">
                                                            <span id="jockey_perc_<?php echo $key; ?>"><?php echo  $result['jockey']  ?></span>
                                                            <input type="hidden" id="hide_jockey_perc_<?php echo $key; ?>" name="race_rs[<?php echo $key; ?>][jockey_perc]" value="<?php echo  $result['jockey']  ?>"  />
                                                        </td>

                                                         <td class="text-right">
                                                            <span id="total_<?php echo $key; ?>"><?php echo  $result['total']  ?></span>
                                                            <input type="hidden" id="total<?php echo $key; ?>" name="race_rs[<?php echo $key; ?>][total]" value="<?php echo  $result['total']  ?>"  />
                                                        </td>

                                                         <td class="text-right">
                                                            <span id="performance_incentive_<?php echo $key; ?>"><?php echo  $result['performance_incentive']  ?></span>
                                                            <input type="hidden" id="hide_performance_incentive_<?php echo $key; ?>" name="race_rs[<?php echo $key; ?>][performance_incentive]" value="<?php echo  $result['performance_incentive']  ?>"  />
                                                        </td>
                                                        
                                                    </tr>

                                            
                                                 <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>

                        <div class="tab-pane" id="tab_sweepstake_race">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="sweepstake_of"><?php echo "A Sweepstake Of"; ?></label>
                                <div class="col-sm-3">
                                    <input type="Number" name="sweepstake_of" value="<?php echo $sweepstake_of;  ?>" placeholder="Sweepstake Of" id="sweepstake_of" class="form-control" tabindex="" />
                                </div>
                            </div>
                            <div class="form-group" style="display: none;">
                                <label class="col-sm-2 control-label" for="no_of_forfiet"><?php echo " No. Of Forfiet"; ?></label>
                                <div class="col-sm-3">
                                    <select name="no_of_forfiet" id="no_of_forfiet" class="form-control"  tabindex="">
                                            <option value="<?php  ?>">Please Select</option>
                                            <?php foreach ($type as $skey => $svalue) { ?>
                                            <?php if ($skey == $no_of_forfiet) { ?>
                                            <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="entry_date"><?php echo "Entry Date"; ?></label>
                                <div class="col-sm-2">
                                    <div class="input-group date">
                                        <input type="text" name="entry_date" value="<?php echo $entry_date; ?>" data-index="4" placeholder="Entry Date" data-date-format="DD-MM-YYYY" id="entry_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label" for="entry_time"><?php echo "Entry Time"; ?></label>
                                <div class="col-sm-2">
                                    <select name="entry_time" id="entry_time" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($times as $skey => $svalue) { ?>
                                        <?php if ($skey == $entry_time) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                 <label class="col-sm-2 control-label" for="entry_fees"><?php echo "Entry Fees"; ?></label>
                                <div class="col-sm-2">
                                    <input type="Number" name="entry_fees" value="<?php echo $entry_fees;  ?>" placeholder="Entry Fees" id="entry_fees" class="form-control" tabindex="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="forefit_date"><?php echo "Forefit Date"; ?></label>
                                <div class="col-sm-2">
                                    <div class="input-group date">
                                        <input type="text" name="forefit_date" value="<?php echo $forefit_date; ?>" data-index="4" placeholder="Entry Date" data-date-format="DD-MM-YYYY" id="forefit_date" class="form-control" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label" for="forefit_time"><?php echo "Forefit Time"; ?></label>
                                <div class="col-sm-2">
                                    <select name="forefit_time" id="forefit_time" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($times as $skey => $svalue) { ?>
                                        <?php if ($skey == $entry_time) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                 <label class="col-sm-2 control-label" for="forefit_amt"><?php echo "Forefit Amount"; ?></label>
                                <div class="col-sm-2">
                                    <input type="Number" name="forefit_amt" value="<?php echo $forefit_amt;  ?>" placeholder="Forefit Amount" id="forefit_amt" class="form-control" tabindex="" />
                                </div>
                               
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="additional_entry"><?php echo "Additional Entry:"; ?></label>
                                <div class="col-sm-3"  style="padding-top: 10px;">
                                    <?php if($additional_entry == 'Yes' ){ ?>
                                        <input type="checkbox" name="additional_entry" class="form-control" id="additional_entry" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="additional_entry" class="form-control" id="additional_entry">

                                    <?php } ?>
                                </div>
                                <label class="col-sm-2 control-label" for="bonus_to_breeder"><?php echo "Bonus To Breeder:"; ?></label>
                                <div class="col-sm-3" style="padding-top: 10px;">
                                    <?php if($bonus_to_breeder == 'Yes' ){ ?>
                                        <input type="checkbox" name="bonus_to_breeder" class="form-control" id="bonus_to_breeder" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="bonus_to_breeder" class="form-control" id="bonus_to_breeder">

                                    <?php } ?>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="remarks"><?php echo "Remark:" ?></label>
                                <div class="col-sm-10">
                                  <textarea name="remarks" id="remarks"cols="100" class="form-control" value="<?php echo $remarks ?>" tabindex="16"><?php echo $remarks; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    
#refresh_btn {
  padding: 5px 10px;
  text-align: center;
  cursor: pointer;
  outline: none;
  color: #fff;
  background-color: #1e91cf;
  border: none;
  border-radius: 15px;
  box-shadow: 0 6px #1e91cf;
}

#refresh_btn:hover {background-color: #1e51cf}

#refresh_btn:active {
  background-color: #1e91cf;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}
</style>
    
    <script>

        $(document).ready(() => {
            stacks_in_val = $('#stakes_in').val();
            if(stacks_in_val == 'Rs'){
                stakes_per_decress =  $('#stakes_per').val();
                positions =  $('#positions').val();
                race_type =  $('#race_type').val();
                class_type =  $('#class').val();
               

                $.ajax({
                    url: 'index.php?route=transaction/prospectus/getManupulatedStackDatas&token=<?php echo $token; ?>&class_type='+class_type+'&race_type='+race_type+'&positions='+positions+'&decress_per='+stakes_per_decress,
                    dataType: 'json',
                    success: function(json) {  
                        //console.log(json);
                        
                        $('#race_rs_table').html('');
                        $('#race_rs_table').append(json.html);
                    }
                });

               

                $('#rs_select_race').show();
                $('.stacks_per_val').show();
                $('.resize_div').removeClass("col-sm-3");
                $('.resize_div').addClass("col-sm-2");

            } else {
                $('.stacks_per_val').hide();
            }
        });

        $('#myTab a').click(function(e) {
          e.preventDefault();
          $(this).tab('show');
        });

        // store the currently selected tab in the hash value
        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
          var id = $(e.target).attr("href").substr(1);
          window.location.hash = id;
        });

        // on load of the page: switch to the currently selected tab
        var hash = window.location.hash;
        $('#myTab a[href="' + hash + '"]').tab('show');
    

       $( "#positions" ).change(function() {
            stakes_type = $('#stakes_in').val();
            var html = "";
            var no_of_rows =  $('#positions').val();
            var i;
            if(stakes_type == '%'){
                var placing = ["Winner", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth"];
                
                for (i = 0; i < no_of_rows; i++) {
                    html += '<tr id="id'+i+'">';
                        html += '  <td class="text-left">';
                            html += placing[i] + "<br>";
                            html += '<input type="hidden" id="placing_'+i+'" name="race[' + i + '][placing]" value='+placing[i]+'  />';
                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html += '<input type="Number" id="owner_per_'+i+'" name="race[' + i + '][owner_per]" value="" onfocusout="calculate('+i+')" placeholder="Owner %" class="form-control" />';
                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html += '<span id="owner_'+i+'"></span>';
                            html += '<input type="hidden" id="hide_owner_'+i+'" name="race[' + i + '][owner]" value=""  />';

                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html +='<span id="trainer_perc_'+i+'"></span>';
                            html += '<input type="hidden" id="hide_trainer_perc_'+i+'" name="race[' + i + '][trainer_perc]" value=""  />';
                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html +='<span id="trainer_'+i+'"></span>';
                            html += '<input type="hidden" id="hide_trainer_'+i+'" name="race[' + i + '][trainer]" value=""  />';
                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html +='<span id="jockey_perc_'+i+'"></span>';
                            html += '<input type="hidden" id="hide_jockey_perc_'+i+'" name="race[' + i + '][jockey_perc]" value=""  />';
                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html +='<span id="jockey_'+i+'"></span>';
                            html += '<input type="hidden" id="hide_jockey_'+i+'" name="race[' + i + '][jockey]" value=""  />';
                        html += '  </td>';
                    html += '</tr>';
                    document.getElementById("sweepstake_race_table").innerHTML = html;
                } 
            } else {
                race_type =  $('#race_type').val();
                class_type =  $('#class').val();
                $.ajax({
                    url: 'index.php?route=transaction/prospectus/getStackDatas&token=<?php echo $token; ?>&class_type='+class_type+'&race_type='+race_type+'&positions='+no_of_rows,
                    dataType: 'json',
                    success: function(json) {  
                        console.log(json);
                        $('#rs_select_race').show();
                        $('#race_rs_table').html('');
                        $('#race_rs_table').append(json.html);
                    }
                });
            }
                   $("input, textarea, select, checkbox").keypress(function(event) {
                        if (event.which == 13) {
                            event.preventDefault();
                        }
                    });
                   /*  $(document).on('keydown', '.form-control', function(e) {
                        var name = $(this).attr('name'); 
                        var class_name = $(this).attr('class'); 
                        var id = $(this).attr('id');
                        var value = $(this).val();
                        if(e.which == 13){
                           
                            var increment = i+1;
                            //alert(increment);
                            //var id =$('owner_per_'+i).val();
                            if(id == 'owner_per_'+i){
                                if ($('#owner_per_'+increment).is(':visible')) {
                                    $('#owner_per_'+increment).focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            
                        }
                    });*/
                
                    $(document).on('keydown', '.form-control', function(e) {
                        var name = $(this).attr('name'); 
                        var class_name = $(this).attr('class'); 
                        var id = $(this).attr('id');
                        var value = $(this).val();
                        if(e.which == 13){
                            if(id == 'owner_per_0'){
                                if ($('#owner_per_1').is(':visible')) {
                                    $('#owner_per_1').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_1'){
                                if ($('#owner_per_2').is(':visible')) {
                                    $('#owner_per_2').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_2'){
                                if ($('#owner_per_3').is(':visible')) {
                                    $('#owner_per_3').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_3'){
                                if ($('#owner_per_4').is(':visible')) {
                                    $('#owner_per_4').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_4'){
                                if ($('#owner_per_5').is(':visible')) {
                                    $('#owner_per_5').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_5'){
                                if ($('#owner_per_6').is(':visible')) {
                                    $('#owner_per_6').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_6'){
                                if ($('#owner_per_7').is(':visible')) {
                                    $('#owner_per_7').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_7'){
                                if ($('#owner_per_8').is(':visible')) {
                                    $('#owner_per_8').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_8'){
                                if ($('#owner_per_9').is(':visible')) {
                                    $('#owner_per_9').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_9'){
                                    $('#stakes_in').focus();
                            }
                                    
                        }
                               
                    });
                    
                
        });
    </script>
    <script type="text/javascript">
        $('#check').click(function(){
            var race_date = $('#race_date').val();
            $.ajax({
                url: 'index.php?route=transaction/prospectus/getRaceData&token=<?php echo $token; ?>&race_date='+race_date,
                dataType: 'json',
                success: function(json) {  
                    console.log(json);
                    if (json != '') {
                        $('#season').val(json.season);
                        $('#year').val(json.year);
                        $('#race_day').val(json.race_day);
                        $('#entries_close_date').val(json.entries_close_date);
                        $('#handicaps_date').val(json.handicaps_date);
                        $('#acceptance_date').val(json.acceptance_date);
                        $('#declaration_date').val(json.declaration_date);
                        $('#serial_no').val(json.serial_no + 1);
                    } else{
                        $('#season').val('');
                        $('#year').val('');
                        $('#race_day').val('');
                        $('#entries_close_date').val('');
                        $('#handicaps_date').val('');
                        $('#acceptance_date').val('');
                        $('#declaration_date').val('');
                        $('#serial_no').val('');
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        function calculate(i,key){
            var value =$('#stakes_money').val();
            var xa = $('#owner_per_'+i).val();
            var cal =(xa * value) / 100;
            var owner =(cal / value) * 82.5;
            var owner_res = (cal * 82.5) /100;
            var trainer =(cal / value) * 10;
            var trainer_res = (cal * 10) /100;
            var jockey =(cal / value) * 7.5;
            var jockey_res = (cal * 7.5) /100;
            //$('#owner_per_'+i).val('');
            $('#owner_'+i).html('');
            $('#trainer_perc_'+i).html('');
            $('#trainer_'+i).html('');
            $('#jockey_perc_'+i).html('');
            $('#jockey_'+i).html('');



            $('#owner_per_'+i).val(owner);
            $('#owner_'+i).html(owner_res);
            $('#trainer_perc_'+i).html(trainer);
            $('#trainer_'+i).html(trainer_res);
            $('#jockey_perc_'+i).html(jockey);
            $('#jockey_'+i).html(jockey_res);

            $('#hide_owner_per_'+i).val(owner);
            $('#hide_owner_'+i).val(owner_res);
            $('#hide_trainer_perc_'+i).val(trainer);
            $('#hide_trainer_'+i).val(trainer_res);
            $('#hide_jockey_perc_'+i).val(jockey);
            $('#hide_jockey_'+i).val(jockey_res);
        }
        
    </script>


    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
    
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'owner_per_0'){
                    if ($('#owner_per_1').is(':visible')) {
                        $('#owner_per_1').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_1'){
                    if ($('#owner_per_2').is(':visible')) {
                        $('#owner_per_2').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_2'){
                    if ($('#owner_per_3').is(':visible')) {
                        $('#owner_per_3').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_3'){
                    if ($('#owner_per_4').is(':visible')) {
                        $('#owner_per_4').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_4'){
                    if ($('#owner_per_5').is(':visible')) {
                        $('#owner_per_5').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_5'){
                    if ($('#owner_per_6').is(':visible')) {
                        $('#owner_per_6').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_6'){
                    if ($('#owner_per_7').is(':visible')) {
                        $('#owner_per_7').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_7'){
                    if ($('#owner_per_8').is(':visible')) {
                        $('#owner_per_8').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_8'){
                    if ($('#owner_per_9').is(':visible')) {
                        $('#owner_per_9').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_9'){
                        $('#stakes_in').focus();
                }
                        
            }
                   
        });
    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
       
        $( document ).ready(function() {
            $('#race_date').focus();
        });


         $('#stakes_in').change(function() {
            var select_p =  $('#stakes_in').val();
            var select =  $('#sweepstake_race').val();
            if(select_p == '%'){
                $('#hide_sweepstake_race_textbox').show();
                $('#sweepstake_race').val('Yes');
                $('#onclick_sweepstake_race').show();
                $('#hide_tab_sweepstake_race').show();
                $('#rs_select_race').hide();
                $('.stacks_per_val').hide();
                $('.resize_div').removeClass("col-sm-2");
                $('.resize_div').addClass("col-sm-3");

            } else if(select_p == 'Rs') {
                $('.resize_div').removeClass("col-sm-3");
                $('.resize_div').addClass("col-sm-2");
                 $('.stacks_per_val').show();
                $('#rs_select_race').show();
                 $('#hide_sweepstake_race_textbox').hide();
                $('#sweepstake_race').val('');
                $('#onclick_sweepstake_race').hide();
                $('#hide_tab_sweepstake_race').hide();
            } else{
                $('#hide_sweepstake_race_textbox').hide();
                $('#sweepstake_race').val('');
                $('#onclick_sweepstake_race').hide();
                $('#hide_tab_sweepstake_race').hide();
                $('#rs_select_race').hide();
                 $('.stacks_per_val').hide();
                 $('.resize_div').removeClass("col-sm-2");
                $('.resize_div').addClass("col-sm-3");

            }

        });

        $('#sweepstake_race').change(function() {
            var select =  $('#sweepstake_race').val();
            //var select_p =  $('#stakes_in').val();
            if (select == 'Yes') {
                $('#onclick_sweepstake_race').show();
                 $('#hide_tab_sweepstake_race').show();
               
            } else {
                $('#onclick_sweepstake_race').hide();
                $('#hide_tab_sweepstake_race').hide();
            }
        });



        $('#sweepstake_race').ready(function() {
        var select_p =  $('#stakes_in').val();
            var select =  $('#sweepstake_race').val();
            if(select_p == '%'){
                $('#hide_sweepstake_race_textbox').show();
                $('#sweepstake_race').val('Yes');
                $('#onclick_sweepstake_race').show();
                $('#hide_tab_sweepstake_race').show();


            }
            else{
                $('#hide_sweepstake_race_textbox').hide();
                $('#sweepstake_race').val('');
                $('#onclick_sweepstake_race').hide();
                $('#hide_tab_sweepstake_race').hide();

            }
        });


        $('#race_type').change(function() {
            var select_h =  $('#race_type').val();
            if (select_h == 'Handicap Race') {
                $('#hide_class').show();
                $('#hide_lower_class_aligible').show();
                $('#rating_from_to').show();
                $('#class').val('');
               
            } else {
                $('#hide_class').hide();
                $('#hide_lower_class_aligible').hide();
                $('#rating_from_to').hide();
                $('#grade').val('');

            }
        });

        $( document ).ready(function() {
            var select_h =  $('#race_type').val();
            if (select_h == 'Handicap Race') {
                $('#hide_class').show();
                $('#hide_lower_class_aligible').show();
                $('#rating_from_to').show();
               
            } else {
                $('#hide_class').hide();
                $('#hide_lower_class_aligible').hide();
                $('#rating_from_to').hide();
            }
        });


        $('#race_type').change(function() {
            var select_t =  $('#race_type').val();
            if (select_t == 'Term Race') {
                $('#hide_grade').show();
                $('#grade').val('');
               
            } else {
                $('#hide_grade').hide();
                $('#grade').val('');

            }
        });
        $( document ).ready(function() {
            var select_t =  $('#race_type').val();
            if (select_t == 'Term Race') {
                $('#hide_grade').show();
               
            } else {
                $('#hide_grade').hide();
            }
        });

        $('#season').change(function() {
            var select_p =  $('#season').val();
            var race_date =  $('#race_date').val();
            var date =race_date.split('-');
            var year  = date[2];
            var month  = date[1];
            var bb = year-1;
            var year1 =parseInt(year)+parseInt(1);
            
            if (select_p == 'PUNE') {
                $('#year').val(year);
            } else if(select_p == 'MUMBAI') {
                if('11' <= month){
                    $('#year').val(year+'-'+year1);
                }
                else if('04'>= month){
                    $('#year').val(bb+'-'+year);
                }
                else{
                    $('#year').val(year+'-'+year1);
                }
                //
            }else{
                $('#year').val('');
            }
            // if (select_p == 'PUNE') {
            //     $('#year').val(year);
            // } else if(select_p == 'MUMBAI') {
            //     if('11'+'-'.year <= month+'-'+year){
            //         $('#year').val(year+'-'+year1);
            //     }
            //     else if('04'+'-'.year >= month+'-'+year){
            //         $('#year').val(bb+'-'+year1);
            //     }
            //     else{
            //         $('#year').val(year+'-'+year1);
            //     }
            //     //
            // }else{
            //     $('#year').val('');
            // }
        });

        
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'race_date'){
                    $('#season').focus();
                }
                if(id == 'season'){
                    $('#race_day').focus();
                }
                /*if(id == 'year'){
                    $('#race_day').focus();
                }*/
                if(id == 'race_day'){
                    $('#evening_race').focus();
                }
                if(id == 'evening_race'){
                    $('#race_name').focus();
                }
                if(id == 'race_name'){
                    $('#race_type').focus();
                }
                if(id == 'race_type'){
                    if ($('#hide_grade').is(':visible')) {
                        $('#grade').focus();
                    } else if ($('#hide_class').is(':visible')) {
                         $('#class').focus();
                    }else {
                         $('#race_category').focus();
                    }
                }
                if(id == 'class'){
                    $('#race_category').focus();
                }
                if(id == 'grade'){
                    $('#race_category').focus();
                }
                if(id == 'race_category'){
                    $('#race_description').focus();
                }
                if(id == 'race_description'){
                    $('#rating_from').focus();
                }

                if(id == 'rating_from'){
                    $('#rating_to').focus();
                }

                if(id == 'rating_to'){
                    $('#distance').focus();
                }

                if(id == 'distance'){
                    if ($('#hide_lower_class_aligible').is(':visible')) {
                        $('#lower_class_aligible').focus();
                    } else {
                         $('#entries_close_date').focus();
                    }
                }
                if(id == 'lower_class_aligible'){
                    $('#entries_close_date').focus();
                }

                if(id == 'entries_close_date'){
                    $('#entries_close_date_time').focus();
                }
                
                if(id == 'entries_close_date_time'){
                    $('#handicaps_date').focus();
                }
                if(id == 'handicaps_date'){
                    $('#handicaps_date_time').focus();
                }
                
                if(id == 'handicaps_date_time'){
                    $('#acceptance_date').focus();
                }
                if(id == 'acceptance_date'){
                    $('#acceptance_date_time').focus();
                }
                
                if(id == 'acceptance_date_time'){
                    $('#declaration_date').focus();
                }
                if(id == 'declaration_date'){
                    $('#declaration_date_time').focus();
                }
                
                if(id == 'declaration_date_time'){
                    $('#race_date').focus();
                }

            }
        });
    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
       
        $( document ).ready(function() {
            $('#age_from ').focus();
        });

       
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'age_from'){
                    $('#age_to').focus();
                }
                if(id == 'age_to'){
                    $('#max_win').focus();
                }
                if(id == 'max_win'){
                    $('#sex').focus();
                }
                if(id == 'sex'){
                    $('#maidens').focus();
                }
                if(id == 'maidens'){
                    $('#unraced').focus();
                }
                if(id == 'unraced'){
                    $('#not1_2_3').focus();
                }
                if(id == 'not1_2_3'){
                    $('#unplaced').focus();
                }
                if(id == 'unplaced'){
                    $('#short_not').focus();
                }
                if(id == 'short_not'){
                    $('#max_stakes_race').focus();
                }
                if(id == 'max_stakes_race'){
                    $('#run_thrice').focus();
                }
                if(id == 'run_thrice'){
                    $('#run_thrice_date').focus();
                }
                if(id == 'run_thrice_date'){
                    $('#run_not_placed').focus();
                }
                
                if(id == 'run_not_placed'){
                    $('#run_not_placed_date').focus();
                }
                if(id == 'run_not_placed_date'){
                    $('#run_not_won').focus();
                }
                
                if(id == 'run_not_won'){
                    $('#run_not_won_date').focus();
                }
                if(id == 'run_not_won_date'){
                    $('#run_and_not_won_during_mtg').focus();
                }
                
                if(id == 'run_and_not_won_during_mtg'){
                    $('#run_and_not_won_during_mtg_date').focus();
                }
                if(id == 'run_and_not_won_during_mtg_date'){
                    $('#no_whip').focus();
                }
                
                if(id == 'no_whip'){
                    $('#foreign_jockey_eligible').focus();
                }
                if(id == 'foreign_jockey_eligible'){
                    $('#foreign_horse_eligible').focus();
                }

                if(id == 'foreign_horse_eligible'){
                    $('#sire_dam').focus();
                }

                if(id == 'sire_dam'){
                    $('#age_from').focus();
                }

                if(id == 'stakes_in'){
                    $('#trophy_value_amount').focus();
                }
                if(id == 'trophy_value_amount'){
                    $('#positions').focus();
                }
                if(id == 'positions'){
                    $('#stakes_money').focus();
                }

                if(id == 'stakes_money'){
                    $('#bonus_applicable').focus();
                }
                if(id == 'bonus_applicable'){
                    $('#sweepstake_race').focus();
                }
                if(id == 'sweepstake_race'){
                    $('#stakes_in').focus();
                }
                
            }
        });
    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
        $( document ).ready(function() {
            $('#sweepstake_of').focus();
        });
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'sweepstake_of'){
                    $('#entry_date').focus();
                }
                if(id == 'entry_date'){
                    $('#entry_time').focus();
                }
                if(id == 'entry_time'){
                    $('#entry_fees').focus();
                }
                if(id == 'entry_fees'){
                    $('#additional_entry').focus();
                }
                if(id == 'additional_entry'){
                    $('#bonus_to_breeder').focus();
                }
                if(id == 'bonus_to_breeder'){
                    $('#remarks').focus();
                }
                if(id == 'remarks'){
                    $('#sweepstake_of').focus();
                }
            }
        });


        $(document).on('keyup','#entry_fees',function() {
            total_amt = parseInt($('#sweepstake_of').val());
            first_amt = parseInt($(this).val());
            remain_amt = total_amt - first_amt;
            console.log(total_amt+'  '+first_amt+'  '+remain_amt)
            if(first_amt < total_amt){
                $('#forefit_amt').val(remain_amt);
            } else {
                alert("Amount Should not greater");
                $('#forefit_amt').val(0);
                $(this).val(0);


                return false;
            }

           // console.log(remain_amt);

        });
    </script>

    <script type="text/javascript">
        $('.date').datetimepicker({
            pickTime: false
        });

        $('.time').datetimepicker({
            pickDate: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });
    </script>

    <script type="text/javascript"><!--
        $('#language a:first').tab('show');
    </script>


    <script type="text/javascript">

        $('#refresh_btn').click(() => {
            stakes_per_decress =  $('#stakes_per').val();
            positions =  $('#positions').val();
            race_type =  $('#race_type').val();
            class_type =  $('#class').val();
           if(stakes_per_decress > 0 && stakes_per_decress < 101 && positions != ''){

                $.ajax({
                    url: 'index.php?route=transaction/prospectus/getManupulatedStackDatas&token=<?php echo $token; ?>&class_type='+class_type+'&race_type='+race_type+'&positions='+positions+'&decress_per='+stakes_per_decress,
                    dataType: 'json',
                    success: function(json) {  
                        //console.log(json);
                        
                        $('#race_rs_table').html('');
                        $('#race_rs_table').append(json.html);
                    }
                });

           } else {

                alert("Please Enter Stacks Percent / Select Positions To Update Amount");
           }
        })

        $('#class, #grade').change(function(){
           race_type =  $('#race_type').val();
           class_type =  $(this).val();
            lower_check_status = 0;


           //console.log(race_type + 'race_type' + class_type)

           $.ajax({
                url: 'index.php?route=transaction/prospectus/getCateDescOfClass&token=<?php echo $token; ?>&class_type='+class_type+'&race_type='+race_type+'&lower_check_status=0',
                dataType: 'json',
                success: function(json) {  
                    console.log(json);
                    $('#race_category').val('');
                    $('#race_description').val(''); 
                    $('#rating_from').val('');
                    $('#rating_to').val('');
                    $('#lower_class_aligible').prop("checked", false);

                    if(json){
                        $('#race_category').val(json.race_category);
                        $('#race_description').val(json.race_description);
                        $('#rating_from').val(json.rating_from);
                        $('#rating_to').val(json.rating_to);
                        if(json.low_class_alig == 'Yes'){
                            $('#lower_class_aligible').prop("checked", true);

                        }
                    }
                }
            });
        });

        $('#lower_class_aligible').change(function(){

            if($(this).prop("checked") == true){
                lower_check_status = 1;
            } else {
                lower_check_status = 0;
            }
            $.ajax({
                url: 'index.php?route=transaction/prospectus/getCateDescOfClass&token=<?php echo $token; ?>&class_type='+class_type+'&race_type='+race_type+'&lower_check_status='+lower_check_status,
                dataType: 'json',
                success: function(json) {  
                    $('#race_category').val('');
                    $('#race_description').val('');
                    $('#rating_from').val('');
                    $('#rating_to').val(''); 
                    if(json){
                        $('#race_category').val(json.race_category);
                        $('#race_description').val(json.race_description);
                         $('#rating_from').val(json.rating_from);
                        $('#rating_to').val(json.rating_to);
                    }
                }
            });
        })



        
        $('#race_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/prospectus/autocompleteracename&token=<?php echo $token; ?>&race_name=' +  encodeURIComponent(request),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.race_name,
                                    value: item.race_name,
                                    

                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(item) {
                $('#race_name').val(item.value);
                $('#race_type').focus();
                return false;
            },
        });
    </script>
    <script type="text/javascript">
        $('#race_date').keyup(function(){
            $('#valierr_race_date').hide();
            var race_date_valid =  $('#race_date').val();
            race_date_valid1 = race_date_valid.replace(/[^0-9-]+/i, '');
            $("#race_date").val(race_date_valid1);
            var race_date_valid_again =  $('#race_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(race_date_valid_again))) {
                $('#error_race_date').show();
                return false;
            } else {
                $('#error_race_date').hide();
            }

        });
        $('.input-race_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_race_date').css('display','none');
        });


        $('#entries_close_date').keyup(function(){
            $('#valierr_entries_close_date').hide();
            var entries_close_date_valid =  $('#entries_close_date').val();
            entries_close_date_valid1 = entries_close_date_valid.replace(/[^0-9-]+/i, '');
            $("#entries_close_date").val(entries_close_date_valid1);
            var entries_close_date_valid_again =  $('#entries_close_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(entries_close_date_valid_again))) {
                $('#error_entries_close_date').show();
                return false;
            } else {
                $('#error_entries_close_date').hide();
            }
        });
        $('.input-entries_close_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_entries_close_date').css('display','none');
        });


        $('#handicaps_date').keyup(function(){
            $('#valierr_handicaps_date').hide();
            var handicaps_date_valid =  $('#handicaps_date').val();
            handicaps_date_valid1 = handicaps_date_valid.replace(/[^0-9-]+/i, '');
            $("#handicaps_date").val(handicaps_date_valid1);
            var handicaps_date_valid_again =  $('#handicaps_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(handicaps_date_valid_again))) {
                $('#error_handicaps_date').show();
                return false;
            } else {
                $('#error_handicaps_date').hide();
            }
        });
        $('.input-handicaps_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_handicaps_date').css('display','none');
        });

        $('#acceptance_date').keyup(function(){
            $('#valierr_acceptance_date').hide();
            var acceptance_date_valid =  $('#acceptance_date').val();
            acceptance_date_valid1 = acceptance_date_valid.replace(/[^0-9-]+/i, '');
            $("#acceptance_date").val(acceptance_date_valid1);
            var acceptance_date_valid_again =  $('#acceptance_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(acceptance_date_valid_again))) {
                $('#error_acceptance_date').show();
                return false;
            } else {
                $('#error_acceptance_date').hide();
            }
        });
        $('.input-acceptance_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_acceptance_date').css('display','none');
        });


        $('#declaration_date').keyup(function(){
            $('#valierr_declaration_date').hide();
            var declaration_date_valid =  $('#declaration_date').val();
            declaration_date_valid1 = declaration_date_valid.replace(/[^0-9-]+/i, '');
            $("#declaration_date").val(declaration_date_valid1);
            var declaration_date_valid_again =  $('#declaration_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(declaration_date_valid_again))) {
                $('#error_declaration_date').show();
                return false;
            } else {
                $('#error_declaration_date').hide();
            }
        });
        $('.input-declaration_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_declaration_date').css('display','none');
        });


        $('#run_thrice_date').keyup(function(){
            $('#valierr_run_thrice_date').hide();
            var run_thrice_date_valid =  $('#run_thrice_date').val();
            run_thrice_date_valid1 = run_thrice_date_valid.replace(/[^0-9-]+/i, '');
            $("#run_thrice_date").val(run_thrice_date_valid1);
            var run_thrice_date_valid_again =  $('#run_thrice_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(run_thrice_date_valid_again))) {
                $('#error_run_thrice_date').show();
                return false;
            } else {
                $('#error_run_thrice_date').hide();
            }
        });
        $('.input-run_thrice_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_run_thrice_date').css('display','none');
        });


        $('#run_not_placed_date').keyup(function(){
            $('#valierr_run_not_placed_date').hide();
            var run_not_placed_date_valid =  $('#run_not_placed_date').val();
            run_not_placed_date_valid1 = run_not_placed_date_valid.replace(/[^0-9-]+/i, '');
            $("#run_not_placed_date").val(run_not_placed_date_valid1);
            var run_not_placed_date_valid_again =  $('#run_not_placed_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(run_not_placed_date_valid_again))) {
                $('#error_run_not_placed_date').show();
                return false;
            } else {
                $('#error_run_not_placed_date').hide();
            }
        });
        $('.input-run_not_placed_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_run_not_placed_date').css('display','none');
        });


        $('#run_not_won_date').keyup(function(){
            $('#valierr_run_not_won_date').hide();
            var run_not_won_date_valid =  $('#run_not_won_date').val();
            run_not_won_date_valid1 = run_not_won_date_valid.replace(/[^0-9-]+/i, '');
            $("#run_not_won_date").val(run_not_won_date_valid1);
            var run_not_won_date_valid_again =  $('#run_not_won_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(run_not_won_date_valid_again))) {
                $('#error_run_not_won_date').show();
                return false;
            } else {
                $('#error_run_not_won_date').hide();
            }
        });
        $('.input-run_not_won_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_run_not_won_date').css('display','none');
        });


        $('#run_and_not_won_during_mtg_date').keyup(function(){
            $('#valierr_run_and_not_won_during_mtg_date').hide();
            var run_and_not_won_during_mtg_date_valid =  $('#run_and_not_won_during_mtg_date').val();
            run_and_not_won_during_mtg_date_valid1 = run_and_not_won_during_mtg_date_valid.replace(/[^0-9-]+/i, '');
            $("#run_and_not_won_during_mtg_date").val(run_and_not_won_during_mtg_date_valid1);
            var run_and_not_won_during_mtg_date_valid_again =  $('#run_and_not_won_during_mtg_date').val();
            var date_format = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
            if (!(date_format.test(run_and_not_won_during_mtg_date_valid_again))) {
                $('#error_run_and_not_won_during_mtg_date').show();
                return false;
            } else {
                $('#error_run_and_not_won_during_mtg_date').hide();
            }
        });
        $('.input-run_and_not_won_during_mtg_date').datetimepicker().on('dp.change', function (e) {  
            $('#error_run_and_not_won_during_mtg_date').css('display','none');
        });
    </script>
<?php echo $footer; ?>