<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-index="1" tabindex="1" href="#tab_stacks_Details" data-toggle="tab">Stacks Details</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_stacks_Details">
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="race_type"><?php echo "Race Type:"; ?></label>
                                <div class="col-sm-3">
                                    <select name="race_type" id="race_type" class="form-control"  tabindex="">
                                        <option value="<?php  ?>">Please Select</option>
                                        <?php foreach ($race_types as $skey => $svalue) { ?>
                                        <?php if ($skey == $race_type) { ?>
                                        <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <?php if (isset($error_race)) { ?><span class="error" style="color: #ff0000;"><?php echo $error_race; ?></span><?php } ?>
                                </div>
                                <div id="hide_class" style="display: none;">
                                    <label class="col-sm-2 control-label" for="class"><?php echo "Class:"; ?></label>
                                    <div class="col-sm-3">
                                        <select name="class" id="class" class="form-control"  tabindex="">
                                            <option value="<?php  ?>">Please Select</option>
                                            <?php foreach ($classs as $skey => $svalue) { ?>
                                            <?php if ($skey == $class) { ?>
                                            <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="hide_grade" style="display: none;">
                                    <label class="col-sm-2 control-label" for="grade"><?php echo "Grade:"; ?></label>
                                    <div class="col-sm-3">
                                        <select name="grade" id="grade" class="form-control"  tabindex="">
                                            <option value="<?php  ?>">Please Select</option>
                                            <?php foreach ($terms as $skey => $svalue) { ?>
                                            <?php if ($svalue == $grade) { ?>
                                            <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                              <div class="form-group">
                                <div class="pull-right col-sm-1">
                                  <button type="button" onclick="add_new_2();"class="btn btn-primary" data-original-title="add"><i class="fa fa-plus-circle"></i></button>
                                </div>
                              </div>
                              <div class="main_div_2"> 
                                <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-name" ><?php echo 'Stacks Type'; ?></label>
                                        <label class="col-sm-2 control-label" for="input-owner" ><?php echo 'Owner'; ?></label>
                                        <label class="col-sm-2 control-label" for="input-trainer" ><?php echo 'Trainer'; ?></label>
                                        <label class="col-sm-2 control-label" for="input-jockey"><?php echo 'Jockey'; ?></label>
                                        <label class="col-sm-2 control-label" for="input-total"><?php echo 'Total'; ?></label>
                                        <label class="col-sm-1 control-label" for="input-incentive"><?php echo 'Incentive'; ?></label>
                                  </div>
                                <?php $extra_field_2 = 1; ?>
                                <?php $tab_index_1 = 0; ?>
                                <?php if($contact_datas){  ?>
                                  <?php foreach($contact_datas as $ckey => $cvalue){ ?>
                                    
                                    <div class="form-group contact_row<?php echo $extra_field_2; ?>">
                                      <div class="col-sm-2">
                                        <!-- <input tabindex="<?php echo $tab_index_1; ?>" type="text" name="contact_datas[<?php echo $extra_field_2 ?>][name]" value="<?php echo $cvalue['stacks_type']; ?>" placeholder="<?php echo 'Stacks Type'; ?>" id="input-bank_name<?php echo $extra_field_2; ?>" class="form-control" /> -->

                                        <select name="contact_datas[<?php echo $extra_field_2 ?>][name]" id="input-bank_name<?php echo $extra_field_2; ?>" class="form-control"  tabindex="<?php echo $tab_index_1; ?>">
                                            <option value="<?php  ?>">Please Select</option>
                                            <?php foreach ($stacks_types as $skey => $svalue) { ?>
                                                <?php if ($skey == $cvalue['stacks_type']) { ?>
                                                    <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                      </div>
                                      <?php $tab_index_1 ++; ?>
                                      
                                      <div class="col-sm-2">
                                        <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][owner]" value="<?php echo $cvalue['owner']; ?>" placeholder="<?php echo 'Owner'; ?>" id="input-owner<?php echo $extra_field_2; ?>" class="form-control" />
                                      </div>
                                      <?php $tab_index_1 ++; ?>
                                      
                                      <div class="col-sm-2">
                                        <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][trainer]" value="<?php echo $cvalue['trainer']; ?>" placeholder="<?php echo 'Trainer'; ?>" id="input-trainer<?php echo $extra_field_2; ?>" class="form-control" />
                                      </div>
                                    
                                      <?php $tab_index_1 ++; ?>
                                      
                                      <div class="col-sm-2">
                                        <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][jockey]" value="<?php echo $cvalue['jockey']; ?>" placeholder="<?php echo 'Jockey'; ?>" id="input-jockey<?php echo $extra_field_2; ?>" class="form-control" />
                                      </div>
                                      <?php $tab_index_1 ++; ?>
                                      
                                      <div class="col-sm-2">
                                        <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][total]" value="<?php echo $cvalue['total']; ?>" placeholder="<?php echo 'Total'; ?>" id="input-total<?php echo $extra_field_2; ?>" class="form-control" />
                                      </div>
                                      <?php $tab_index_1 ++; ?>                    
                                      
                                      <div class="col-sm-1">
                                        <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][incentive]" value="<?php echo $cvalue['incentive']; ?>" placeholder="<?php echo 'Incentive'; ?>" id="input-incentive<?php echo $extra_field_2; ?>" class="form-control" />
                                      </div>
                                       <div class="pull-right col-sm-1">
                                        <button type="button" onclick="remove_folder_2('<?php echo $extra_field_2; ?>')" data-toggle="tooltip" class="btn btn-danger" data-original-title="Remove" id="remove_2<?php echo $extra_field_2; ?>" ><i class="fa fa-minus-circle"></i></button>
                                      </div>
                                    </div>
                                    <?php $extra_field_2 ++; ?>
                                  <?php } ?>
                                <?php } else { ?>
                                  <div class="form-group contact_row<?php echo $extra_field_2; ?>">
                                    <div class="col-sm-2">
                                      <!-- <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][name]" value="" placeholder="<?php echo 'Stacks Type'; ?>" id="input-bank_name<?php echo $extra_field_2; ?>" class="form-control" />-->
                                        <select name="contact_datas[<?php echo $extra_field_2 ?>][name]" id="input-bank_name<?php echo $extra_field_2; ?>" class="form-control"  tabindex="<?php echo $tab_index_1; ?>">
                                            <option value="<?php  ?>">Please Select</option>
                                            <?php foreach ($stacks_types as $skey => $svalue) { ?>
                                                <?php if ($skey == $cvalue['stacks_type']) { ?>
                                                    <option value="<?php echo $svalue; ?>" selected="selected"><?php echo $svalue; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $svalue; ?>"><?php echo $svalue; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $tab_index_1 ++; ?>
                                    
                                    <div class="col-sm-2">
                                      <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][owner]" value="" placeholder="<?php echo 'Owner'; ?>" id="input-owner<?php echo $extra_field_2; ?>" class="form-control" />
                                    </div>
                                    <?php $tab_index_1 ++; ?>
                                    
                                    <div class="col-sm-2">
                                      <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][trainer]" value="" placeholder="<?php echo 'Trainer'; ?>" id="input-trainer<?php echo $extra_field_2; ?>" class="form-control" />
                                    </div>
                                 
                                    <?php $tab_index_1 ++; ?>
                                    
                                    <div class="col-sm-2">
                                      <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][jockey]" value="" placeholder="<?php echo 'Jockey'; ?>" id="input-jockey<?php echo $extra_field_2; ?>" class="form-control" />
                                    </div>
                                    <?php $tab_index_1 ++; ?>
                                    
                                    <div class="col-sm-2">
                                      <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][total]" value="" placeholder="<?php echo 'Total'; ?>" id="input-total<?php echo $extra_field_2; ?>" class="form-control" />
                                    </div>
                                    <?php $tab_index_1 ++; ?>
                                    
                                    <div class="col-sm-1">
                                      <input tabindex="<?php echo $tab_index_1; ?>" type="number" name="contact_datas[<?php echo $extra_field_2 ?>][incentive]" value="" placeholder="<?php echo 'Incentive'; ?>" id="input-incentive<?php echo $extra_field_2; ?>" class="form-control" />
                                    </div>
                                    <div class="pull-right col-sm-1">
                                        <button type="button" onclick="remove_folder_2('<?php echo $extra_field_2; ?>')" data-toggle="tooltip" class="btn btn-danger" data-original-title="Remove" id="remove_2<?php echo $extra_field_2; ?>" ><i class="fa fa-minus-circle"></i></button>
                                    </div>
                                  </div>
                                  <?php $extra_field_2 ++; ?>
                                <?php } ?>
                              </div>
                              <input type="hidden" id="extra_field_2" name="extra_field_2" value="<?php echo $extra_field_2; ?>" />
                              <input type="hidden" id="tab_index_1" name="tab_index_1" value="<?php echo $tab_index_1; ?>" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
    <script>
       $( "#positions" ).change(function() {

            var placing = ["Winner", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth"];
            var html = "";
            var no_of_rows =  $('#positions').val();
            var i;
                for (i = 0; i < no_of_rows; i++) {
                    html += '<tr id="id'+i+'">';
                        html += '  <td class="text-left">';
                            html += placing[i] + "<br>";
                            html += '<input type="hidden" id="placing_'+i+'" name="race[' + i + '][placing]" value='+placing[i]+'  />';
                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html += '<input type="Number" id="owner_per_'+i+'" name="race[' + i + '][owner_per]" value="" onfocusout="calculate('+i+')" placeholder="Owner %" class="form-control" />';
                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html += '<span id="owner_'+i+'"></span>';
                            html += '<input type="hidden" id="hide_owner_'+i+'" name="race[' + i + '][owner]" value=""  />';

                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html +='<span id="trainer_perc_'+i+'"></span>';
                            html += '<input type="hidden" id="hide_trainer_perc_'+i+'" name="race[' + i + '][trainer_perc]" value=""  />';
                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html +='<span id="trainer_'+i+'"></span>';
                            html += '<input type="hidden" id="hide_trainer_'+i+'" name="race[' + i + '][trainer]" value=""  />';
                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html +='<span id="jockey_perc_'+i+'"></span>';
                            html += '<input type="hidden" id="hide_jockey_perc_'+i+'" name="race[' + i + '][jockey_perc]" value=""  />';
                        html += '  </td>';
                        html += '  <td class="text-right">';
                            html +='<span id="jockey_'+i+'"></span>';
                            html += '<input type="hidden" id="hide_jockey_'+i+'" name="race[' + i + '][jockey]" value=""  />';
                        html += '  </td>';
                    html += '</tr>';
                    document.getElementById("sweepstake_race_table").innerHTML = html;
                   $("input, textarea, select, checkbox").keypress(function(event) {
                        if (event.which == 13) {
                            event.preventDefault();
                        }
                    });
                   /*  $(document).on('keydown', '.form-control', function(e) {
                        var name = $(this).attr('name'); 
                        var class_name = $(this).attr('class'); 
                        var id = $(this).attr('id');
                        var value = $(this).val();
                        if(e.which == 13){
                           
                            var increment = i+1;
                            //alert(increment);
                            //var id =$('owner_per_'+i).val();
                            if(id == 'owner_per_'+i){
                                if ($('#owner_per_'+increment).is(':visible')) {
                                    $('#owner_per_'+increment).focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            
                        }
                    });*/
                
                    $(document).on('keydown', '.form-control', function(e) {
                        var name = $(this).attr('name'); 
                        var class_name = $(this).attr('class'); 
                        var id = $(this).attr('id');
                        var value = $(this).val();
                        if(e.which == 13){
                            if(id == 'owner_per_0'){
                                if ($('#owner_per_1').is(':visible')) {
                                    $('#owner_per_1').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_1'){
                                if ($('#owner_per_2').is(':visible')) {
                                    $('#owner_per_2').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_2'){
                                if ($('#owner_per_3').is(':visible')) {
                                    $('#owner_per_3').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_3'){
                                if ($('#owner_per_4').is(':visible')) {
                                    $('#owner_per_4').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_4'){
                                if ($('#owner_per_5').is(':visible')) {
                                    $('#owner_per_5').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_5'){
                                if ($('#owner_per_6').is(':visible')) {
                                    $('#owner_per_6').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_6'){
                                if ($('#owner_per_7').is(':visible')) {
                                    $('#owner_per_7').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_7'){
                                if ($('#owner_per_8').is(':visible')) {
                                    $('#owner_per_8').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_8'){
                                if ($('#owner_per_9').is(':visible')) {
                                    $('#owner_per_9').focus();
                                } else {
                                     $('#stakes_in').focus();
                                }
                            }
                            if(id == 'owner_per_9'){
                                    $('#stakes_in').focus();
                            }
                                    
                        }
                               
                    });
                    
                }
        });
    </script>

    <script type="text/javascript">
        function calculate(i,key){
            var value =$('#stakes_money').val();
            var xa = $('#owner_per_'+i).val();
            var cal =(xa * value) / 100;
            var owner =(cal / value) * 82.5;
            var owner_res = (cal * 82.5) /100;
            var trainer =(cal / value) * 10;
            var trainer_res = (cal * 10) /100;
            var jockey =(cal / value) * 7.5;
            var jockey_res = (cal * 7.5) /100;
            //$('#owner_per_'+i).val('');
            $('#owner_'+i).html('');
            $('#trainer_perc_'+i).html('');
            $('#trainer_'+i).html('');
            $('#jockey_perc_'+i).html('');
            $('#jockey_'+i).html('');



            $('#owner_per_'+i).val(owner);
            $('#owner_'+i).html(owner_res);
            $('#trainer_perc_'+i).html(trainer);
            $('#trainer_'+i).html(trainer_res);
            $('#jockey_perc_'+i).html(jockey);
            $('#jockey_'+i).html(jockey_res);

            $('#hide_owner_per_'+i).val(owner);
            $('#hide_owner_'+i).val(owner_res);
            $('#hide_trainer_perc_'+i).val(trainer);
            $('#hide_trainer_'+i).val(trainer_res);
            $('#hide_jockey_perc_'+i).val(jockey);
            $('#hide_jockey_'+i).val(jockey_res);
        }
        
    </script>


    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
    
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'owner_per_0'){
                    if ($('#owner_per_1').is(':visible')) {
                        $('#owner_per_1').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_1'){
                    if ($('#owner_per_2').is(':visible')) {
                        $('#owner_per_2').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_2'){
                    if ($('#owner_per_3').is(':visible')) {
                        $('#owner_per_3').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_3'){
                    if ($('#owner_per_4').is(':visible')) {
                        $('#owner_per_4').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_4'){
                    if ($('#owner_per_5').is(':visible')) {
                        $('#owner_per_5').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_5'){
                    if ($('#owner_per_6').is(':visible')) {
                        $('#owner_per_6').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_6'){
                    if ($('#owner_per_7').is(':visible')) {
                        $('#owner_per_7').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_7'){
                    if ($('#owner_per_8').is(':visible')) {
                        $('#owner_per_8').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_8'){
                    if ($('#owner_per_9').is(':visible')) {
                        $('#owner_per_9').focus();
                    } else {
                         $('#stakes_in').focus();
                    }
                }
                if(id == 'owner_per_9'){
                        $('#stakes_in').focus();
                }
                        
            }
                   
        });
    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
       
        $( document ).ready(function() {
            $('#race_date').focus();
        });


         $('#stakes_in').change(function() {
            var select_p =  $('#stakes_in').val();
            var select =  $('#sweepstake_race').val();
            if(select_p == '%'){
                $('#hide_sweepstake_race_textbox').show();
                $('#sweepstake_race').val('Yes');
                $('#onclick_sweepstake_race').show();
                $('#hide_tab_sweepstake_race').show();


            }
            else{
                $('#hide_sweepstake_race_textbox').hide();
                $('#sweepstake_race').val('');
                $('#onclick_sweepstake_race').hide();
                $('#hide_tab_sweepstake_race').hide();

            }

        });

        $('#sweepstake_race').change(function() {
        var select =  $('#sweepstake_race').val();
        //var select_p =  $('#stakes_in').val();
            if (select == 'Yes') {
                $('#onclick_sweepstake_race').show();
                 $('#hide_tab_sweepstake_race').show();
               
            } else {
                $('#onclick_sweepstake_race').hide();
                $('#hide_tab_sweepstake_race').hide();
            }
        });



        $('#sweepstake_race').ready(function() {
        var select_p =  $('#stakes_in').val();
            var select =  $('#sweepstake_race').val();
            if(select_p == '%'){
                $('#hide_sweepstake_race_textbox').show();
                $('#sweepstake_race').val('Yes');
                $('#onclick_sweepstake_race').show();
                $('#hide_tab_sweepstake_race').show();


            }
            else{
                $('#hide_sweepstake_race_textbox').hide();
                $('#sweepstake_race').val('');
                $('#onclick_sweepstake_race').hide();
                $('#hide_tab_sweepstake_race').hide();

            }
        });


        $('#race_type').change(function() {
            var select_h =  $('#race_type').val();
            if (select_h == 'Handicap Race' ) {
                $('#hide_class').show();
                $('#hide_lower_class_aligible').show();
               
            } else {
                $('#hide_class').hide();
                 $('#hide_lower_class_aligible').hide();
            }
        });

        $( document ).ready(function() {
            var select_h =  $('#race_type').val();
            if (select_h == 'Handicap Race') {
                $('#hide_class').show();
                $('#hide_lower_class_aligible').show();
               
            } else {
                $('#hide_class').hide();
                $('#hide_lower_class_aligible').hide();
            }
        });


       $('#race_type').change(function() {
            var select_t =  $('#race_type').val();
            if (select_t == 'Term Race') {
                $('#hide_grade').show();
               
            } else {
                $('#hide_grade').hide();
            }
        });
        $( document ).ready(function() {
            var select_t =  $('#race_type').val();
            if (select_t == 'Term Race') {
                $('#hide_grade').show();
               
            } else {
                $('#hide_grade').hide();
            }
        });

        $('#season').change(function() {
            var select_p =  $('#season').val();
            var race_date =  $('#race_date').val();
            var date =race_date.split('-');
            var year  = date[2];
            var month  = date[1];
            var bb = year-1;
            var year1 =parseInt(year)+parseInt(1);
            
            if (select_p == 'PUNE') {
                $('#year').val(year);
            } else if(select_p == 'MUMBAI') {
                if('11' <= month){
                    $('#year').val(year+'-'+year1);
                }
                else if('04'>= month){
                    $('#year').val(bb+'-'+year);
                }
                else{
                    $('#year').val(year+'-'+year1);
                }
                //
            }else{
                $('#year').val('');
            }
            // if (select_p == 'PUNE') {
            //     $('#year').val(year);
            // } else if(select_p == 'MUMBAI') {
            //     if('11'+'-'.year <= month+'-'+year){
            //         $('#year').val(year+'-'+year1);
            //     }
            //     else if('04'+'-'.year >= month+'-'+year){
            //         $('#year').val(bb+'-'+year1);
            //     }
            //     else{
            //         $('#year').val(year+'-'+year1);
            //     }
            //     //
            // }else{
            //     $('#year').val('');
            // }
        });

        
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'race_date'){
                    $('#season').focus();
                }
                if(id == 'season'){
                    $('#race_day').focus();
                }
                /*if(id == 'year'){
                    $('#race_day').focus();
                }*/
                if(id == 'race_day'){
                    $('#evening_race').focus();
                }
                if(id == 'evening_race'){
                    $('#race_name').focus();
                }
                if(id == 'race_name'){
                    $('#race_type').focus();
                }
                if(id == 'race_type'){
                    if ($('#hide_grade').is(':visible')) {
                        $('#grade').focus();
                    } else if ($('#hide_class').is(':visible')) {
                         $('#class').focus();
                    }else {
                         $('#race_category').focus();
                    }
                }
                if(id == 'class'){
                    $('#race_category').focus();
                }
                if(id == 'grade'){
                    $('#race_category').focus();
                }
                if(id == 'race_category'){
                    $('#race_description').focus();
                }
                if(id == 'race_description'){
                    $('#distance').focus();
                }

                if(id == 'distance'){
                    if ($('#hide_lower_class_aligible').is(':visible')) {
                        $('#lower_class_aligible').focus();
                    } else {
                         $('#entries_close_date').focus();
                    }
                }
                if(id == 'lower_class_aligible'){
                    $('#entries_close_date').focus();
                }

                if(id == 'entries_close_date'){
                    $('#entries_close_date_time').focus();
                }
                
                if(id == 'entries_close_date_time'){
                    $('#handicaps_date').focus();
                }
                if(id == 'handicaps_date'){
                    $('#handicaps_date_time').focus();
                }
                
                if(id == 'handicaps_date_time'){
                    $('#acceptance_date').focus();
                }
                if(id == 'acceptance_date'){
                    $('#acceptance_date_time').focus();
                }
                
                if(id == 'acceptance_date_time'){
                    $('#declaration_date').focus();
                }
                if(id == 'declaration_date'){
                    $('#declaration_date_time').focus();
                }
                
                if(id == 'declaration_date_time'){
                    $('#race_date').focus();
                }

            }
        });
    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
       
        $( document ).ready(function() {
            $('#age_from ').focus();
        });

       
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'age_from'){
                    $('#age_to').focus();
                }
                if(id == 'age_to'){
                    $('#max_win').focus();
                }
                if(id == 'max_win'){
                    $('#sex').focus();
                }
                if(id == 'sex'){
                    $('#maidens').focus();
                }
                if(id == 'maidens'){
                    $('#unraced').focus();
                }
                if(id == 'unraced'){
                    $('#not1_2_3').focus();
                }
                if(id == 'not1_2_3'){
                    $('#unplaced').focus();
                }
                if(id == 'unplaced'){
                    $('#short_not').focus();
                }
                if(id == 'short_not'){
                    $('#max_stakes_race').focus();
                }
                if(id == 'max_stakes_race'){
                    $('#run_thrice').focus();
                }
                if(id == 'run_thrice'){
                    $('#run_thrice_date').focus();
                }
                if(id == 'run_thrice_date'){
                    $('#run_not_placed').focus();
                }
                
                if(id == 'run_not_placed'){
                    $('#run_not_placed_date').focus();
                }
                if(id == 'run_not_placed_date'){
                    $('#run_not_won').focus();
                }
                
                if(id == 'run_not_won'){
                    $('#run_not_won_date').focus();
                }
                if(id == 'run_not_won_date'){
                    $('#run_and_not_won_during_mtg').focus();
                }
                
                if(id == 'run_and_not_won_during_mtg'){
                    $('#run_and_not_won_during_mtg_date').focus();
                }
                if(id == 'run_and_not_won_during_mtg_date'){
                    $('#no_whip').focus();
                }
                
                if(id == 'no_whip'){
                    $('#foreign_jockey_eligible').focus();
                }
                if(id == 'foreign_jockey_eligible'){
                    $('#foreign_horse_eligible').focus();
                }

                if(id == 'foreign_horse_eligible'){
                    $('#sire_dam').focus();
                }

                if(id == 'sire_dam'){
                    $('#age_from').focus();
                }

                if(id == 'stakes_in'){
                    $('#trophy_value_amount').focus();
                }
                if(id == 'trophy_value_amount'){
                    $('#positions').focus();
                }
                if(id == 'positions'){
                    $('#stakes_money').focus();
                }

                if(id == 'stakes_money'){
                    $('#bonus_applicable').focus();
                }
                if(id == 'bonus_applicable'){
                    $('#sweepstake_race').focus();
                }
                if(id == 'sweepstake_race'){
                    $('#stakes_in').focus();
                }
                
            }
        });
    </script>

    <script type="text/javascript">
        $("input, textarea, select, checkbox").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
            }
        });
        $( document ).ready(function() {
            $('#sweepstake_of').focus();
        });
        $(document).on('keydown', '.form-control', function(e) {
            var name = $(this).attr('name'); 
            var class_name = $(this).attr('class'); 
            var id = $(this).attr('id');
            var value = $(this).val();
            if(e.which == 13){
                if(id == 'sweepstake_of'){
                    $('#entry_date').focus();
                }
                if(id == 'entry_date'){
                    $('#entry_time').focus();
                }
                if(id == 'entry_time'){
                    $('#entry_fees').focus();
                }
                if(id == 'entry_fees'){
                    $('#additional_entry').focus();
                }
                if(id == 'additional_entry'){
                    $('#bonus_to_breeder').focus();
                }
                if(id == 'bonus_to_breeder'){
                    $('#remarks').focus();
                }
                if(id == 'remarks'){
                    $('#sweepstake_of').focus();
                }
            }
        });
    </script>

    <script type="text/javascript">
        $('.date').datetimepicker({
            pickTime: false
        });

        $('.time').datetimepicker({
            pickDate: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });
    </script>

    <script type="text/javascript"><!--
        $('#language a:first').tab('show');
    </script>


    <script type="text/javascript">
        
        $('#race_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/stacks/autocompleteracename&token=<?php echo $token; ?>&race_name=' +  encodeURIComponent(request),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.race_name,
                                    value: item.race_name,
                                    

                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(item) {
                $('#race_name').val(item.value);
                $('#race_type').focus();
                return false;
            },
        });
    </script>
    

<script type="text/javascript">
    var extra_field_1 = $('#extra_field_1').val();
    var tab_index_2 = $('#tab_index_2').val();

    function remove_folder_1(extra_field_1){
      $('.bank_row'+extra_field_1).remove();
    }

    var extra_field_2 = $('#extra_field_2').val();
    var tab_index_1 = $('#tab_index_1').val();
    function add_new_2() {
      
      html = '<div class="form-group contact_row' + extra_field_2 + '">';
        html += '<div class="col-sm-2">';
          // html += '<input tabindex="'+tab_index_1+'" type="text" name="contact_datas['+extra_field_2+'][name]" value="" placeholder="Stacks Type" id="input-name'+extra_field_2+'" class="form-control" />';
        html += ' <select name="contact_datas['+extra_field_2+'][name]" id="input-name'+extra_field_2+'"  class="form-control"  tabindex="'+tab_index_1+'">';
        html += '<option value="<?php  ?>">Please Select</option>';
        html += '  <?php foreach ($stacks_types as $skey => $svalue) { ?>';
        html += '        <option value="<?php echo $svalue; ?>" ><?php echo $svalue; ?></option>';
        html += '   <?php } ?>';
        html += '    </select>';
        html += '</div>';
        tab_index_1 ++;
        
        html += '<div class="col-sm-2">';
          html += '<input tabindex="'+tab_index_1+'" type="number" name="contact_datas['+extra_field_2+'][owner]" value="" placeholder="Owner" id="input-owner'+extra_field_2+'" class="form-control" />'; 
        html += '</div>';
        tab_index_1 ++;
        
        html += '<div class="col-sm-2">';
          html += '<input tabindex="'+tab_index_1+'" type="number" name="contact_datas['+extra_field_2+'][trainer]" value="" placeholder="Trainer" id="input-trainer'+extra_field_2+'" class="form-control" />';
        html += '</div>';
      
        tab_index_1 ++;
        
        html += '<div class="col-sm-2">';
          html += '<input tabindex="'+tab_index_1+'" type="number" name="contact_datas['+extra_field_2+'][jockey]" value="" placeholder="Jockey" id="input-jockey'+extra_field_2+'" class="form-control" />';
        html += '</div>';
        tab_index_1 ++;
        
        html += '<div class="col-sm-2">';
          html += '<input tabindex="'+tab_index_1+'" type="number" name="contact_datas['+extra_field_2+'][total]" value="" placeholder="Total" id="input-total'+extra_field_2+'" class="form-control" />';
        html += '</div>';
        tab_index_1 ++;
        
        html += '<div class="col-sm-1">';
          html += '<input tabindex="'+tab_index_1+'" type="number" name="contact_datas['+extra_field_2+'][incentive]" value="" placeholder="Incentive" id="input-incentive'+extra_field_2+'" class="form-control" />';
        html += '</div>';
        html += '<div class="pull-right col-sm-1">';
        html += '<button type="button" onclick="remove_folder_2('+extra_field_2+')" data-toggle="tooltip" class="btn btn-danger" data-original-title="Remove" id="remove_2'+extra_field_2+'" ><i class="fa fa-minus-circle"></i></button>';
        html += '</div>';
      html += '</div>';  
      $('.main_div_2').append(html);
      extra_field_2++;
    }

    function remove_folder_2(extra_field_2){
            $('.contact_row'+extra_field_2).remove();
    }


    $(document).ready(function(){
        $('#input-trainer'+extra_field_2).keyup(function()  {
        var input_valid =  $('#input-trainer'+extra_field_2).val() ;
        alert(input_valid);
        //var input_valid1 = input_valid.replace(/[^0-9]+/i, '');
        // $("#input-purchase_price").val(input_valid);
        // var qty =  $('#input-quantity').val();
        // var value = input_valid * qty;
        
        // //console.log(value);
        // $('#input-Value').val('');
        // $('#input-Values').val(value);
        });
    });
</script>
<?php echo $footer; ?>