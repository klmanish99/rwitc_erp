<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title ?></h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
                    <div class="well" style="background-color: #ffffff;">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                    <label for="filter_authority"> Authority </label>
                                        <select name="authority" id="authority" class="form-control" >
                                            <option value="">All</option>
                                            <?php foreach ($authoritys as $auth) { 
                                                if($auth == $filter_authority){ ?>
                                                    <option value="<?php echo $auth; ?>" selected="selected"><?php echo $auth ?></option>
                                                <?php } else { ?>
                                                        <option value="<?php echo $auth; ?>" ><?php echo $auth ?></option>
                                                <?php } ?>
                                            <?php } ?>

                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                    <label  for="filter_trainer_name"> Trainer </label>
                                        <input type="text" name="filter_trainer_name" class="form-control" value="<?php echo $filter_trainer_name ?>" id="filter_trainer_name">
                                        <input type="hidden" name="filter_trainer_id" class="form-control"  id="filter_trainer_id">
                                    </div>

                                    <div class="col-sm-3">
                                    <label  for="filter_horse_name"> Horse Name </label>
                                        <input type="text" name="filter_horse_name" class="form-control" value="<?php echo $filter_horse_name ?>" id="filter_horse_name">
                                        <input type="hidden" name="filter_horse_id" class="form-control"  id="filter_horse_id">

                                    </div>
                                    <div class="col-sm-2">
                                    <label  for="filter_authority"> </label>
                                    <a class="btn btn-primary form-control" id="filter" onclick="filter()">Filter</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="text-center">Sr No.</td>
                                        <td class="text-center">Trainer Code</td>
                                        <td class="text-center">Trainer Name</td>
                                        <td class="text-center">Club</td>
                                        <td class="text-center">Authority</td>
                                        <td class="text-center">Offences</td>
                                        <td class="text-center">Horse Name</td>
                                        <td class="text-center">Race No</td>
                                        <td class="text-center">Race Date</td>
                                        <td class="text-center">Fine</td>
                                        <td class="text-center">Suspension From</td>
                                        <td class="text-center">Suspension To</td>
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($ban_datas) { ?>
                                    <?php $i=1; ?>
                                    <?php foreach ($ban_datas as $bkey => $bvalue) { 
                                        $date = date('d/m/Y', strtotime($bvalue['race_date'])); 
                                        $start_date = date('d/m/Y', strtotime($bvalue['sus_from'])); 
                                        $end_date = date('d/m/Y', strtotime($bvalue['sus_to']));
                                        ?>
                                        <tr>
                                            <td class="text-left"><?php echo  $i++ ;?></td>
                                            <td class="text-left"><?php echo $bvalue['trainer_code']; ?></td>
                                            <td class="text-left"><?php echo $bvalue['trainer_name']; ?></td>
                                            <td class="text-left"><?php echo $bvalue['club']; ?></td>
                                            <?php if($bvalue['auth'] == 'Board of Authority') { ?>
                                                <td class="text-left">Borad of Authority</td>
                                            <?php } else if($bvalue['auth'] == 'Stewards') { 
                                                ?>
                                                <td class="text-left">Steward</td>
                                            <?php } else { 
                                                ?>
                                                <td class="text-left">Stipes</td>
                                            <?php } ?>
                                            <td class="text-left"><?php echo $bvalue['offences']; ?></td>
                                            <td class="text-left"><?php echo $bvalue['horse_name']; ?></td>
                                            <td class="text-left"><?php echo $bvalue['race_no']; ?></td>
                                            <td class="text-left"><?php echo  $date ;?></td>
                                            <td class="text-left"> <?php echo $bvalue['fine'] ?> </td>
                                            <td class="text-left"><?php echo  $start_date ;?></td>
                                            <td class="text-left"><?php echo  $end_date ;?></td>
                                            <td class="text-center">
                                                <a style="margin-bottom: 5px;" href="<?php echo $bvalue['edit_stipes']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i>Stipes Edit </a><br>
                                                <a  style="margin-bottom: 5px;" href="<?php echo $bvalue['edit_stewards']; ?>"  class="btn btn-primary"><i class="fa fa-edit"></i>Stewards Edit</a><br>
                                                <?php if($bvalue['auth'] != 'Stipes') { ?>
                                                    <a href="<?php echo $bvalue['edit_boa']; ?>"  class="btn btn-primary"><i class="fa fa-edit"></i>BOA Edit</a>
                                                <?php } ?>
                                                </td>
                                        </tr>
                                    <?php   } ?>
                                <?php }  ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
        function filter() { 
            var filter_authority =$('#authority').val();
            var filter_trainer_id =$('#filter_trainer_id').val();
            var filter_horse_id =$('#filter_horse_id').val();
            var filter_trainer_name =$('#filter_trainer_name').val();
            var filter_horse_name =$('#filter_horse_name').val();

            url = 'index.php?route=transaction/trainer_ban&token=<?php echo $token; ?>';
            if (filter_authority) {
              url += '&filter_authority=' + encodeURIComponent(filter_authority);
            }

            if (filter_trainer_id) {
              url += '&filter_trainer_id=' + encodeURIComponent(filter_trainer_id);
              url += '&filter_trainer_name=' + encodeURIComponent(filter_trainer_name);

            }

            if (filter_horse_id) {
              url += '&filter_horse_id=' + encodeURIComponent(filter_horse_id);
              url += '&filter_horse_name=' + encodeURIComponent(filter_horse_name);

            }
            window.location.href = url;
        }
    </script>
    <script type="text/javascript">
        $('.date').datetimepicker({
                pickTime: false
            });

            $('.time').datetimepicker({
                pickDate: false
            });

            $('.datetime').datetimepicker({
                pickDate: true,
                pickTime: true
            });

        $('#filter_trainer_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/trainer_ban/autocompletetrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {
                        //console.log(json.final_trainer);
                            if(json.final_trainer){   
                                response($.map(json.final_trainer, function(item) {
                                    return {
                                        label: item.name,
                                        value: item.name,
                                        trainer_id : item.id,
                                        trainer_code : item.trainer_code_new
                                    }
                                }));
                            }
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#filter_trainer_name').val(ui.item.label );
                $('#filter_trainer_id').val(ui.item.trainer_id);
                $('.dropdown-menu').hide();
                
                return false;
            },
        });

        $('#filter_horse_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/trainer_ban/autocompleteHorse&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {
                        //console.log(json);
                            if(json.final_horse){   
                                response($.map(json.final_horse, function(item) {
                                    return {
                                        label: item.name,
                                        value: item.name,
                                        horse_id : item.id
                                    }
                                }));
                            }
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#filter_horse_name').val(ui.item.label );
                $('#filter_horse_id').val(ui.item.horse_id);
                $('.dropdown-menu').hide();
            }
        });
    </script>
</div>
<?php echo $footer; ?>