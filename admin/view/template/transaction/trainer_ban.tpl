<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">

                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
                <a type="submit" form="form-category"  class="btn btn-primary pull-right save" style="margin-left: 10px;">Save</a>
                
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-body">
                <form action="<?php $filter ?>" method="get" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                     <?php if($ban_id != 0 ) {
                        if($editby == 'stipes' && $auth_type == 'Stipes'){
                            $sty = '';
                            $sty1 = '';
                            $sty2 = '';
                            $sty3 = '';
                            $select_sty = '';
                            $check_box = '';
                            $select_sty1 = '';
                            $check_box1 = '';
                        } else if($editby == 'stewards' && $auth_type == 'Stipes') {
                            $sty1 = '';
                            $select_sty1 = '';
                            $check_box1 = '';
                            $sty2 = 'readonly="readonly"';
                            $sty = 'readonly="readonly"';
                            $sty3 = 'readonly="readonly"';
                            $select_sty = 'background: #eee;pointer-events: none;touch-action: none';
                            $check_box = 'disabled="disabled"';
                        } else if($editby == 'boa' && $auth_type == 'Stewards') {
                            $sty1 = '';
                            $select_sty1 = '';
                            $check_box1 = '';
                            $sty2 = 'readonly="readonly"';
                            $sty = 'readonly="readonly"';
                            $sty3 = 'readonly="readonly"';
                            $select_sty = 'background: #eee;pointer-events: none;touch-action: none';
                            $check_box = 'disabled="disabled"';
                        } else if($editby == 'stewards' && $auth_type == 'Stewards') {
                            $sty1 = '';
                            $sty2 = '';
                            $sty3 = 'readonly="readonly"';
                            $select_sty1 = '';
                            $check_box1 = '';
                            $sty = 'readonly="readonly"';
                            $select_sty = 'background: #eee;pointer-events: none;touch-action: none';
                            $check_box = 'disabled="disabled"';
                        } else if($editby == 'stewards' && $auth_type == 'Board of Authority') {
                            $sty1 = 'readonly="readonly"';
                            $sty2 = 'readonly="readonly"';
                            $sty3 = 'readonly="readonly"';
                            $select_sty1 = 'background: #eee;pointer-events: none;touch-action: none';
                            $check_box1 = 'disabled="disabled"';
                            $sty = 'readonly="readonly"';
                            $select_sty = 'background: #eee;pointer-events: none;touch-action: none';
                            $check_box = 'disabled="disabled"';
                        } else if($editby == 'boa' && $auth_type == 'Stewards') {
                            $sty1 = '';
                            $select_sty1 = '';
                            $check_box1 = '';
                            $sty2 = 'readonly="readonly"';
                            $sty = 'readonly="readonly"';
                            $sty3 = 'readonly="readonly"';
                            $select_sty = 'background: #eee;pointer-events: none;touch-action: none';
                            $check_box = 'disabled="disabled"';
                        }  else if($editby == 'boa' && $auth_type == 'Board of Authority') {
                            $sty1 = '';
                            $select_sty1 = '';
                            $check_box1 = '';
                            $sty2 = 'readonly="readonly"';
                            $sty3 = '';
                            $sty = 'readonly="readonly"';
                            $select_sty = 'background: #eee;pointer-events: none;touch-action: none';
                            $check_box = 'disabled="disabled"';
                        } else {
                            $sty = 'readonly="readonly"';
                            $select_sty = 'background: #eee;pointer-events: none;touch-action: none';
                            $check_box = 'disabled="disabled"';
                            $sty1 = 'readonly="readonly"';
                            $sty2 = 'readonly="readonly"';
                            $sty3 = 'readonly="readonly"';
                            $select_sty1 = 'background: #eee;pointer-events: none;touch-action: none';
                            $check_box1 = 'disabled="disabled"';
                        }
                    } else {
                        $sty = '';
                        $sty2 = '';
                        $sty3 = '';
                        $select_sty = '';
                        $check_box = '';
                        $sty1 = '';
                        $select_sty1 = '';
                        $check_box1 = '';
                    } ?>
                   <?php if($auth_type == 'Stipes' && $editby != 'stipes' ) { ?>
                        <div class="form-group" >
                            <div class="col-sm-2"></div>
                            <label class="col-sm-2" for="remark_stewards"> Stewards Remark</label>
                            <div class="col-sm-5">
                                <textarea name="remark_stewards" class="form-control" value="<?php echo $remark_stewards ?>" ></textarea>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if($auth_type == 'Stewards' && $editby == 'boa') { ?>
                        <div class="form-group" >
                            <div class="col-sm-2"></div>
                            <label class="col-sm-2" for="remark_board_of_authority"> Board of Authority Remark</label>
                            <div class="col-sm-5">
                                <textarea name="remark_board_of_authority" class="form-control" value="<?php echo $remark_board_of_authority ?>" ></textarea>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="form-group" >
                        <div class="col-sm-2"></div>
                        
                        <label class="col-sm-2" for="authority"> Authority</label>
                        <div class="col-sm-3">
                            <select name="authority" id="authority" class="form-control" style="<?php echo $select_sty ?>">
                                <?php foreach ($authoritys as $auth) { 
                                    if($auth == $auth_type){ ?>
                                        <option value="<?php echo $auth; ?>" selected="selected"><?php echo $auth ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $auth; ?>"><?php echo $auth ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>

                        <label class="col-sm-2" for="club"> Club </label>
                        <div class="col-sm-3">
                            <select name="club" id="club" class="form-control" style="<?php echo $select_sty ?>">
                                <?php foreach ($clubs as $clb) { 
                                    if($clb == $club){ ?>
                                        <option value="<?php echo $clb; ?>" selected="selected"><?php echo $clb ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $clb; ?>"><?php echo $clb ?></option>

                                    <?php } ?>
                                <?php } ?>

                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group" >
                        <div class="col-sm-2"></div>
                        <label class="col-sm-2" for="trainer_code"> Trainer Code <span style="color: red;font-weight: bold">*</span> </label>
                        <div class="col-sm-3">
                            <input type="text" name="trainer_code" id="trainer_code" value="<?php echo $trainer_code ?>" placeholder="Search Trainer" class="form-control" <?php echo $sty ?>>
                            <span class="trainer_error alert-danger" style="font-weight: bold;"></span>
                        </div>

                        <label class="col-sm-2" for="trainer_name"> Trainer Name <span style="color: red;font-weight: bold">*</span> </label>
                        <div class="col-sm-3">
                            <input type="text" name="trainer_name" id="trainer_name" value="<?php echo $trainer_name ?>"  placeholder="Search Trainer" class="form-control" <?php echo $sty ?>>
                            <input type="hidden" name="trainer_id" id="trainer_id" value="<?php echo $trainer_id ?>"  class="form-control">
                        </div>
                    </div>

                    <div class="form-group" >
                        <div class="col-sm-2"></div>
                        <label class="col-sm-2" for="offences"> Offences</label>
                        <div class="col-sm-3">
                            <select name="offences" id="offences" class="form-control" style="<?php echo $select_sty1 ?>">
                                <?php foreach ($offencess as $key => $value) { 
                                    if($value['offences'] == $offences){ ?>
                                        <option value="<?php echo $value['offences']; ?>" selected="selected" ><?php echo $value['offences'] ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $value['offences']; ?>"><?php echo $value['offences'] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" >
                        <div class="col-sm-2"></div>
                        <label class="col-sm-2" for="horse_name"> Horse Name </label>
                        <div class="col-sm-3">
                            <input type="text" name="horse_name" id="horse_name" value="<?php echo $horse_name ?>"  placeholder="Search horse" class="form-control" <?php echo $sty ?>>
                            <input type="hidden" name="horse_id" id="horse_id" value="<?php echo $horse_id ?>"  class="form-control">
                            <span class="horse_error alert-danger" style="font-weight: bold;"></span>

                        </div>
                    </div>

                    <div class="form-group" >
                        <div class="col-sm-2"></div>
                        <label class="col-sm-2" for="race_no"> Race No. <span style="color: red;font-weight: bold">*</span> </label>
                        <div class="col-sm-3">
                            <input type="text" name="race_no" id="race_no" value="<?php echo $race_no ?>"  placeholder="Search Race No" class="form-control" <?php echo $sty ?>>
                            <span class="race_error alert-danger" style="font-weight: bold;"></span>

                        </div>

                        <label class="col-sm-2" for="date_of_charge"> Race Date </label>
                        <div class="col-sm-3">
                            <input type="text" name="race_date" id="race_date"  value="<?php echo $race_date ?>"  class="form-control" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group" >
                        <div class="col-sm-2"></div>
                        <label class="col-sm-2" for="fine"> Fine </label>
                        <div class="col-sm-3">
                            <input type="number" name="fine" id="fine" min="1" value="<?php echo $fine ?>"  placeholder="Enter Fine" class="form-control" <?php echo $sty1 ?> >
                        </div>
                    </div>

                    <div class="form-group" >
                        <div class="col-sm-2"></div>
                        <label class="col-sm-2" for="suspension_from">Suspension From  Date</label>
                        <div class="col-sm-3">
                            <div class="input-group date input-suspension_from">
                                <input type="text" name="suspension_from" value="<?php echo $sus_from ?>"  data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="suspension_from" class="form-control" <?php echo $sty1 ?>/>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"  <?php echo $check_box1 ?> ><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>

                        <label class="col-sm-2" for="suspension_to"> Suspension To Date</label>
                        <div class="col-sm-3">
                            <div class="input-group date input-suspension_to">
                                <input type="text" name="suspension_to" value="<?php echo $sus_to ?>"  data-index="4" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="suspension_to" class="form-control"  <?php echo $sty1 ?>/>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"  <?php echo $check_box1 ?>><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group" >
                        <div class="col-sm-2"></div>
                        <label class="col-sm-2" for="wdr_lic"> WDR Lisence</label>
                        <div class="col-sm-1">
                            <div class="checkbox" >
                                <label>
                                    <?php if($wdr_lic == 1){ ?>
                                        <input type="checkbox" checked="checked" name="wdr_lic" value="1"  id="wdr_lic"  class="form-control" tabindex="5" <?php echo $check_box  ?> />
                                    <?php } else { ?>
                                        <input type="checkbox"  name="wdr_lic" value="1"  id="wdr_lic"  class="form-control" tabindex="5" <?php echo $check_box ?> />
                                    <?php } ?>
                                </label>
                            </div>
                        </div>

                        <label class="col-sm-2" for="ride_work"> Ride Work</label>
                        <div class="col-sm-1">
                            <div class="checkbox" >
                                <label>
                                    <?php if($ride_work == 1){ ?>
                                        <input type="checkbox" checked="checked" name="ride_work" value="1"  id="ride_work"  class="form-control" tabindex="5" <?php echo $check_box  ?> />
                                    <?php } else { ?>
                                        <input type="checkbox"  name="ride_work"  id="ride_work"  value="1" class="form-control" tabindex="5" <?php echo $check_box  ?> />
                                    <?php } ?>

                                </label>
                            </div>
                        </div>
                        
                        <label class="col-sm-2" for="mock_race"> Mock Race</label>
                        <div class="col-sm-1">
                            <div class="checkbox" >
                                <label>
                                    <?php if($mock_race == 1){ ?>
                                        <input type="checkbox" checked="checked" name="mock_race" value="1"  id="mock_race"  class="form-control" tabindex="5" <?php echo $check_box  ?> />
                                    <?php } else { ?>
                                        <input type="checkbox"  name="mock_race"  id="mock_race"  value="1" class="form-control" tabindex="5"  <?php echo $check_box  ?> />
                                    <?php } ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" >
                        <div class="col-sm-2"></div>
                        <label class="col-sm-2" for="remark">Stipes Remark</label>
                        <div class="col-sm-5">
                            <textarea name="remark" class="form-control"  <?php echo $sty ?>><?php echo $remark ?></textarea>
                        </div>
                    </div>
                     <?php if($auth_type == 'Stewards' || $auth_type == 'Board of Authority') { ?>
                        <div class="form-group" >
                            <div class="col-sm-2"></div>
                            <label class="col-sm-2" for="remark_stewards"> Stewards Remark</label>
                            <div class="col-sm-5">
                                <textarea name="remark_stewards" class="form-control" value="<?php echo $remark_stewards ?>"<?php echo $sty2 ?>><?php echo $remark_stewards ?></textarea>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if( $auth_type == 'Board of Authority') { ?>
                        <div class="form-group" >
                            <div class="col-sm-2"></div>
                            <label class="col-sm-2" for="remark_board_of_authority"> Board of Authority Remark</label>
                            <div class="col-sm-5">
                                <textarea name="remark_board_of_authority" class="form-control" value="<?php echo $remark_board_of_authority ?>"  <?php echo $sty3 ?>><?php echo $remark_board_of_authority ?></textarea>
                            </div>
                        </div>
                    <?php } ?>
                    
                    <a id="sv"  style="margin-left:50%;" class="btn btn-primary save">Save</a>
                </form>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
  
</style>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
        $('.date').datetimepicker({
            pickTime: false
        });

        $(document).ready(function(){
            $('#trainer_code').focus();
        })

        /* Search For From trainer */
        $('#trainer_code').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/trainer_ban/autocompletetrainer&token=<?php echo $token; ?>&trainer_code=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {
                        //console.log(json); 
                            if(json.final_trainer){  
                                response($.map(json.final_trainer, function(item) {
                                    return {
                                        label: item.trainer_code_new,
                                        value: item.trainer_code_new,
                                        trainer_id : item.id,
                                        trainer_name : item.name
                                    }
                                }));
                            }
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#trainer_name').val(ui.item.trainer_name);
                $('#trainer_code').val(ui.item.value);
                $('#trainer_id').val(ui.item.trainer_id);
                $('#offences').focus();
                $('.dropdown-menu').hide();
                
                return false;
            },
        });

        $('#trainer_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/trainer_ban/autocompletetrainer&token=<?php echo $token; ?>&trainer_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {
                        //console.log(json.final_trainer);
                            if(json.final_trainer){   
                                response($.map(json.final_trainer, function(item) {
                                    return {
                                        label: item.name,
                                        value: item.name,
                                        trainer_id : item.id,
                                        trainer_code : item.trainer_code_new
                                    }
                                }));
                            }
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#trainer_name').val(ui.item.label );
                $('#trainer_code').val(ui.item.trainer_code);
                $('#trainer_id').val(ui.item.trainer_id);
                $('#offences').focus();
                $('.dropdown-menu').hide();
                
                return false;
            },
        });

        /* Search For To horse Autocomplete */
        $('#horse_name').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/trainer_ban/autocompleteHorse&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {
                        //console.log(json);
                            if(json.final_horse){   
                                response($.map(json.final_horse, function(item) {
                                    return {
                                        label: item.name,
                                        value: item.name,
                                        horse_id : item.id
                                    }
                                }));
                            }
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#horse_name').val(ui.item.label );
                $('#horse_id').val(ui.item.horse_id);
                $('.dropdown-menu').hide();
            }
        });


        $('#race_no').autocomplete({
            delay: 500,
            source: function(request, response) {
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=transaction/trainer_ban/autocompleteRaceNo&token=<?php echo $token; ?>&race_no=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {
                        //console.log(json); 
                            if(json.final_race){  
                                response($.map(json.final_race, function(item) {
                                    return {
                                        label: item.id,
                                        value: item.id,
                                        race_date : item.race_date
                                    }
                                }));
                            }
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#race_no').val(ui.item.label );
                $('#race_date').val(ui.item.race_date);
                $('#fine').focus();
                $('.dropdown-menu').hide();
            }
        });

        /* To Save Data In table */

        $('.save').click(function(){
            datasss =  $('#form-category').serialize();
            ban_id = '<?php echo $ban_id ?>';
            editby = '<?php echo $editby ?>';

            $.ajax({
                method: "POST",
                url: 'index.php?route=transaction/trainer_ban/saveAllDatas&token=<?php echo $token; ?>&ban_id='+ban_id+'&editby='+editby,
                dataType: 'json',
                data: datasss,
                success: function(json) {
                    if(json.status == 0){
                        if(json.e_horse == 1){
                            $('.horse_error').append("Please Select Valid Horse"); 
                        } 

                        if(json.e_trainer == 1){
                            $('.trainer_error').append("Please Select Valid trainer"); 
                        }

                        if(json.e_race == 1){
                            $('.race_error').append("Please Select Valid Race No"); 
                        }

                        alert("Please Check all required filled carefully!");
                        return false;
                    } else {
                        // console.log("Json Data: "+ json);
                        // return false;

                        path = 'index.php?route=transaction/trainer_ban&token=<?php echo $token; ?>';
                        window.location.href = path;
                    }
                    
                }
            });
        });
        
        /* To Checked All Option */
        $(document).on("keyup",".form-control", function(e) {
            var code = e.keyCode || e.which;
             if(code == 13) {
                ids = $(this).attr("id");
                if(ids == 'horse_name'){
                    $('#race_no').focus();
                }
                console.log(ids);
             }
        });

          
    </script>
</div>
<?php echo $footer; ?>