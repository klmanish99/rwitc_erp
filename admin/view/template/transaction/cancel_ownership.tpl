<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-user"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <div class="form-group" >
                        <label class="col-sm-2 control-label" for="input-horse"><b style="color: red;"> * </b> <?php echo "Horse Name:"; ?></label>
                        <div class="col-sm-3">
                            <input type="text"  name="filterHorseName" id="filterHorseName" value="<?php echo $horse_name ?>" placeholder="Horse Name" class="form-control" />
                            <input type="hidden" name="filterHorseId" id="filterHorseId" value="<?php echo $horse_id ?>"  class="form-control">
                        </div>
                        <label class="col-sm-2 control-label" for="input-trainer"><?php echo "Trainer Name :"; ?></label>
                        <div class="col-sm-3">
                            <input type="hidden" id="check_provisonal" value="<?php echo $check_provisonal ?>" >
                            <input type="text" name="trainer_name"  placeholder="<?php echo "Trainer Name"; ?>" value="<?php echo $trainer_name ?>" placeholder="Trainer Name" id="input-trainer_name" class="form-control"  readonly="readonly"/>
                            <input type="hidden" name="trainer_id" value="<?php echo $trainer_id ?>" id="trainer_id" class="form-control" />
                        </div>
                    </div>

                    <div class="table-responsive owner_datas" style="border-top: 0;">
                    </div>

                     <div class="form-group selectsharediv" style="display: none;border-top: 0;">
                        <span style="display: flex;color: red;font-weight: bold;font-size: 17px;justify-content: center;" ><?php echo 'Please Assign Horse'; ?></span>
                    </div>
                    
                    <div class="form-group sale-owner-div" >
                    </div>
                    
                    <div class="form-group from-div" style="display: none;"> 
                        <div class="form-group " style="border-top: 0; height: 100%;display: flex;justify-content: center;align-items: center;" >
                            <label class="col-sm-1 control-label" for="input-fromdate" required><b style="color: red;"> * </b> From Date</label>
                            <div class="col-sm-3" >
                                <input type="text" name="fromdate" value="<?php echo $fromdate ?>"  id="fromdate"  class="form-control" required/>
                            </div>
                            <label class="col-sm-1 control-label" for="input-todate"> <b class="todate_val" style="color: red;display:none;"> * </b> To Date</label>
                            <div class="col-sm-3" >
                                <input type="text" name="todate" value=""  id="todate"  class="form-control" />
                            </div>
                            <div class="col-sm-2" style="display:none;">
                                <label></label>
                                <div class="checkbox" >
                                    <input type="checkbox" name="date_check"  id="lease"  class="lease" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" for="input-todate">Remark</label>
                            <div class="col-sm-7" >
                                <textarea type="text" name="remark" value=""  id="remark"  class="form-control" ></textarea>
                            </div>
                        </div>

                        <div class="form-group " style="border-top: 0; height: 100%;display: flex;justify-content: center;align-items: center;" >
                            <a id="save" class="col-sm-2 btn btn-primary">Next</a>
                        </div>
                    </div>
                    <input type="hidden" id="last_owner_id" value="0" class="form-control last_owner_id" />
                    <input type="hidden" id="selected_pre_owners" value="0" class="form-control selected_pre_owners" />
                    <input type="hidden" id="all_selected_pre_owners" value="0" class="form-control all_selected_pre_owners" />
                    <input type="hidden" id="newuser_status" value="0" class="form-control newuser_status" />
                    <input type="hidden" id="check_contingency" value="" class="form-control" />
                    <input type="hidden" id="contingency_percentage" value="" class="form-control" />
                    <input type="hidden" id="contingency_amounts" value="" class="form-control" />
                    <input type="hidden" id="contingency_wgs" value="" class="form-control" />
                    <input type="hidden" id="contingency_wns" value="" class="form-control" />
                    <input type="hidden" id="contingency_place" value="" class="form-control" />
                    <input type="hidden" id="contingency_million_over" value="" class="form-control" />
                    <input type="hidden" id="contingency_million_amt" value="" class="form-control" />
                    <input type="hidden" id="contingency_gradeone" value="" class="form-control" />
                    <input type="hidden" id="contingency_grade_two" value="" class="form-control" />
                    <input type="hidden" id="contingency_grade_three" value="" class="form-control" />
                    <input type="hidden" id="hidden_lease_date_start" value="" class="form-control" />
                    <input type="hidden" id="hidden_lease_date_end" value="" class="form-control" />

                    <div class="mod" >
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contingency Details</h4>
                </div>
                <div class="modal-body" style="height: 240px;">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Owner Details</h4>
                </div>
                <div class="modal-body2" style="height: 240px;">
                    
                </div>
                <div class="modal-footer modal-footer2">
                    <span style="font-size: 14px;display: flex;color:red"> <b style = "color:red">Note :</b>If lease cancel then sub lease will be cancel</span> <br><br>
                    <button type="button" class="btn btn-primary" id = "ban_save_id"  style="display: none;" >Cancel Lease</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">

        $( "#fromdate, #todate" ).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
        });

        $(document).ready(function(){
            gt_id = '<?php echo $gt_id ?>';
            if(gt_id == 0){
                $('#filterHorseName').focus();
            } 
        });

        //--------------------------basic filters-----
        $('#filterHorseName').autocomplete({
            delay: 500,
            source: function(request, response) {
                $('#filterHorseId').val('');
                if(request != ''){
                    $.ajax({
                        url: 'index.php?route=report/report_horse_undertaking_charge/autocompleteHorse&token=<?php echo $token; ?>&horse_name=' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {   
                            response($.map(json, function(item) {
                                return {
                                    label: item.horse_name,
                                    value: item.horse_name,
                                    horse_id:item.horse_id,
                                }
                            }));
                        }
                    });
                }
            }, 
            select: function(event, ui) {
                $('#filterHorseName').val(ui.item.value);
                $('#filterHorseId').val(ui.item.horse_id);
                $('.dropdown-menu').hide();
                if(ui.item.horse_id != ''){
                    $.ajax({
                            url: 'index.php?route=transaction/ownership_shift_module/autoHorseToTrainer&token=<?php echo $token; ?>&horse_id=' +  encodeURIComponent(ui.item.horse_id),
                            dataType: 'json',
                            cache: false,
                            success: function(json) {
                                if(json){
                                    $.each(json, function (i, item) {
                                        $('#input-trainer_name').val(item.trainer_name);
                                        $('#trainer_id').val(item.trainer_id);
                                    });
                                }
                            }
                        }); 
                    $.ajax({
                        url: 'index.php?route=transaction/cancel_ownership/getOwnerData&token=<?php echo $token; ?>&filter_hourse_id='+ui.item.horse_id,
                        dataType: 'json',
                        success: function(json) {
                            if(json.new_user == 0 ){
                                $('.ownertable').remove();
                                $('.selectsharediv').css("display", "none");
                                $('.owner_datas').append(json.html);
                            } else {
                                $('.ownertable').remove();
                                $('.selectsharediv').css("display", "");
                            }
                        }
                    }); 
                } else {
                    $('.ownertable').remove();
                    return false;
                }
               // $('#ownership_type_id').focus();
                return false;
            },
        });

        // contingency popup details 

        $(document).on("click",".modalopen",function() {
            horse_owner_idssss = $(this).attr('id');
            horse_owner_id = horse_owner_idssss.split('_');
            $.ajax({
                url: 'index.php?route=transaction/cancel_ownership/getContaingencyData&token=<?php echo $token; ?>&horse_owner_id='+horse_owner_id[1],
                dataType: 'json',
                success: function(json) {   
                    $('.modal-body').html('');
                    $('.modal-body').append(json);
                    $('#myModal').modal('show'); 
                }
            });
        });

        //-------------------------open popup owner----
        $(document).on("click",".checkbox_value",function() {
            filter_hourse_id = $('#filterHorseId').val();
            idss = $(this).attr('id');
            crnt_id = idss.split('_');
            curr_id = parseInt(crnt_id[3]);
            ownership_type_id = $("#fro_ow_ck_"+curr_id).val();
            batch_ids = $("#batch_ids_"+curr_id).val();

            //console.log(ownership_type_id);
            owner_type = $("#own_type_"+curr_id).val() || '';
            if((filter_hourse_id != '') && (ownership_type_id != '')){
               $.ajax({
                url: 'index.php?route=transaction/cancel_ownership/getOwnersParent&token=<?php echo $token; ?>&filter_hourse_id='+filter_hourse_id+'&ownership_type_id='+ownership_type_id,
                    dataType: 'json',
                success: function(json) { 
                    // console.log(json);
                    // return false;
                    if(json['html'] != '' && json['containgency_status'] != 1){
                        $('.modal-body2').html('');
                        $('.modal-body2').append(json['html']);
                        $('#ban_save_id').show();
                        $('#ban_save_id').attr('onclick', '');
                        $('#ban_save_id').attr('onclick', 'UpdateOwnerStatus('+ownership_type_id+',"'+owner_type+'", '+batch_ids+')');
                        $('#myModal2').modal('show'); 
                    } else {
                        alert("Please remove containgency to cancel lease or sub-lease");
                        return false;
                    } 
                    
                }
            }); 
            }
        });

        // owner Partners Detail Popup

        $(document).on("click",".partners",function() {
            parent_owner_idss = $(this).attr('id');
            parent_owner_id = parent_owner_idss.split('_');
            horse_id = $('#filterHorseId').val();
            $.ajax({
                url: 'index.php?route=transaction/ownership_shift_module/getPartnersData&token=<?php echo $token; ?>&parent_owner_id='+parent_owner_id[1]+'&horse_id='+horse_id,
                dataType: 'json',
                success: function(json) {   
                    $('#partners_data_'+parent_owner_id[1]).remove();
                    $('.mod').append(json);
                    $('#partners_data_'+parent_owner_id[1]).modal('show'); 
                }
            });
        });

         //------------------contigencey remove----------
        $(document).on("click",".rmcontigencey",function() {
            filter_hourse_id = $('#filterHorseId').val();
            horse_owner_id = $(this).attr('id');
           // horse_owner_id = horse_owner_idssss.split('_');
            $.ajax({
                url: 'index.php?route=transaction/cancel_ownership/updateown&token=<?php echo $token; ?>&filter_hourse_id='+filter_hourse_id+'&ownership_type_id='+horse_owner_id+'&from_contigencey='+1,
                dataType: 'json',
                success: function(json) {   
                   if(json.new_user == 0 && horse_owner_id != 0 && json.status != 0 ){
                        $('.ownertable').remove();
                        $('.selectsharediv').css("display", "none");
                        $('.owner_datas').append(json.html);
                    } else {
                        alert('Something Wrong')
                        $('.ownertable').remove();
                        $('.selectsharediv').css("display", "");
                    }
                }
            });
        });

        //------------------on popup cancel owner----------
        function UpdateOwnerStatus(horse_owner_id, owner_type, batch_id){
           // console.log(owner_type);return false;
             filter_hourse_id = $('#filterHorseId').val();
            $.ajax({
                url: 'index.php?route=transaction/cancel_ownership/updateown&token=<?php echo $token; ?>&filter_hourse_id='+filter_hourse_id+'&ownership_type_id='+horse_owner_id+'&owner_type='+owner_type+'&from_contigencey=0&batch_id='+batch_id,
                    dataType: 'json',
                success: function(json) { 
                    if(json.new_user == 0 && horse_owner_id != 0 && json.status != 0){
                        $("#myModal2").modal("toggle");
                        $('.ownertable').remove();
                        $('.selectsharediv').css("display", "none");
                        $('.owner_datas').append(json.html);
                    } else {
                        $("#myModal2").modal("toggle");
                        alert('Something Wrong')
                        $('.ownertable').remove();
                        $('.selectsharediv').css("display", "");
                    }
                }
            });

        }
    </script>
</div>
<?php echo $footer; ?>