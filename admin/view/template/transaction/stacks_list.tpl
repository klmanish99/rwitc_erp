<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
             
                <button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title ?></h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
                    
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td style="display: none;" "width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                        <td class="text-center">Sr No</td>
                                        <td class="text-center">Race Type</td>
                                        <td class="text-center">Grade/Class</td>
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($stacksdatas)) { ?>
                                    <?php $i=1; ?>
                                    <?php foreach ($stacksdatas as $stacksdata) { ?>
                                        <tr>
                                            <td style="display: none;" class="text-center"><?php if (in_array($stacksdata['stacks_id'], $selected)) { ?>
                                                <input type="checkbox" name="selected[]" value="<?php echo $stacksdata['stacks_id']; ?>" checked="checked" />
                                                  <?php } else { ?>
                                                <input type="checkbox" name="selected[]" value="<?php echo $stacksdata['stacks_id']; ?>" />
                                                <?php } ?>
                                            </td>
                                            <td class="text-left"><?php echo $i++; ?></td>
                                            <td class="text-left"><?php echo $stacksdata['race_type']; ?></td>
                                            
                                            <td class="text-left"><?php echo $stacksdata['class']; ?></td>
                                            <td class="text-center"><a href="<?php echo $stacksdata['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function filter() { 
            var filter_race_name =$('#race_name').val();
            var filter_race_date = $('#race_date').val();
            //if(filter_race_name !='' || filter_race_date !=''){
                url = 'index.php?route=transaction/stacks&token=<?php echo $token; ?>';
                if (filter_race_name) {
                  url += '&filter_race_name=' + encodeURIComponent(filter_race_name);
                }

                if (filter_race_date) {
                    url += '&filter_race_date=' + encodeURIComponent(filter_race_date);
                } 
                window.location.href = url;
           // }
            /* else {
                alert('Please select the filters');
                return false;
            }*/
            
        }
    </script>
    <script type="text/javascript">
        $('.date').datetimepicker({
                pickTime: false
            });

            $('.time').datetimepicker({
                pickDate: false
            });

            $('.datetime').datetimepicker({
                pickDate: true,
                pickTime: true
            });
    </script>
</div>
<?php echo $footer; ?>