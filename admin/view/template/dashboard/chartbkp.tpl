<div class="panel panel-default">
  	<div class="panel-heading">
    	<div class="pull-right"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> <i class="caret"></i></a>
      		<ul id="range" class="dropdown-menu dropdown-menu-right">
        		<li><a href="day"><?php echo $text_day; ?></a></li>
        		<li><a href="week"><?php echo $text_week; ?></a></li>
        		<li class="active"><a href="month"><?php echo $text_month; ?></a></li>
        		<li><a href="year"><?php echo $text_year; ?></a></li>
      		</ul>
    	</div>
    	<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> <?php echo "Race Calendar"; ?></h3>
  	</div>
  	<div class="panel-body">
    	<center>
			<script language="javascript" type="text/javascript">
			var day_of_week = new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
			var month_of_year = new Array('January','February','March','April','May','June','July','August','September','October','November','December');

			//  DECLARE AND INITIALIZE VARIABLES
			var Calendar = new Date();

			var year = Calendar.getFullYear();     // Returns year
			var month = Calendar.getMonth();    // Returns month (0-11)
			var today = Calendar.getDate();    // Returns day (1-31)
			var weekday = Calendar.getDay();    // Returns day (1-31)

			var DAYS_OF_WEEK = 7;    // "constant" for number of days in a week
			var DAYS_OF_MONTH = 31;    // "constant" for number of days in a month
			var cal;    // Used for printing

			Calendar.setDate(1);    // Start the calendar day at '1'
			Calendar.setMonth(month);    // Start the calendar month at now


			/* VARIABLES FOR FORMATTING
			NOTE: You can format the 'BORDER', 'BGCOLOR', 'CELLPADDING', 'BORDERCOLOR'
			      tags to customize your caledanr's look. */

			var TR_start = '<TR>';
			var TR_end = '</TR>';
			var highlight_start = '<TD WIDTH="100" Height="70"><TABLE CELLSPACING=0 BORDER=1 BGCOLOR=DEDEFF BORDERCOLOR=CCCCCC><TR><TD WIDTH=100><B><CENTER>';
			var highlight_end   = '</CENTER></TD></TR></TABLE></B>';
			var TD_start = '<TD WIDTH="100" Height="70"><CENTER>';
			var TD_end = '</CENTER></TD>';

			/* BEGIN CODE FOR CALENDAR
			NOTE: You can format the 'BORDER', 'BGCOLOR', 'CELLPADDING', 'BORDERCOLOR'
			tags to customize your calendar's look.*/

			cal =  '<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=0 BORDERCOLOR=BBBBBB><TR><TD>';
			cal += '<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>' + TR_start;
			cal += '<TD Height="70" COLSPAN="' + DAYS_OF_WEEK + '" BGCOLOR="#279fe0"><CENTER><B>';
			cal += month_of_year[month]  + '   ' + year + '</B>' + TD_end + TR_end;
			cal += TR_start;

			//   DO NOT EDIT BELOW THIS POINT  //

			// LOOPS FOR EACH DAY OF WEEK
			for(index=0; index < DAYS_OF_WEEK; index++)
			{

			// BOLD TODAY'S DAY OF WEEK
			if(weekday == index)
			cal += TD_start + '<B>' + day_of_week[index] + '</B>' + TD_end;

			// PRINTS DAY
			else
			cal += TD_start + day_of_week[index] + TD_end;
			}

			cal += TD_end + TR_end;
			cal += TR_start;

			// FILL IN BLANK GAPS UNTIL TODAY'S DAY
			for(index=0; index < Calendar.getDay(); index++)
			cal += TD_start + '  ' + TD_end;

			// LOOPS FOR EACH DAY IN CALENDAR
			for(index=0; index < DAYS_OF_MONTH; index++)
			{
			if( Calendar.getDate() > index )
			{
			  // RETURNS THE NEXT DAY TO PRINT
			  week_day =Calendar.getDay();

			  // START NEW ROW FOR FIRST DAY OF WEEK
			  if(week_day == 0)
			  cal += TR_start;

			  if(week_day != DAYS_OF_WEEK)
			  {

			  // SET VARIABLE INSIDE LOOP FOR INCREMENTING PURPOSES
			  var day  = Calendar.getDate();

			  // HIGHLIGHT TODAY'S DATE
			  if( today==Calendar.getDate() )
			  cal += highlight_start + day + highlight_end + TD_end;

			  // PRINTS DAY
			  else
			  cal += TD_start + day + TD_end;
			  }

			  // END ROW FOR LAST DAY OF WEEK
			  if(week_day == DAYS_OF_WEEK)
			  cal += TR_end;
			  }

			  // INCREMENTS UNTIL END OF THE MONTH
			  Calendar.setDate(Calendar.getDate()+1);

			}// end for loop

			cal += '</TD></TR></TABLE></TABLE>';

			//  PRINT CALENDAR
			document.write(cal);

			//  End -->
			</script>
		</center>
  	</div>
</div>
<script type="text/javascript" src="view/javascript/jquery/flot/jquery.flot.js"></script> 
<script type="text/javascript" src="view/javascript/jquery/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript"><!--
$('#range a').on('click', function(e) {
	e.preventDefault();
	
	$(this).parent().parent().find('li').removeClass('active');
	
	$(this).parent().addClass('active');
	
	$.ajax({
		type: 'get',
		url: 'index.php?route=dashboard/chart/chart&token=<?php echo $token; ?>&range=' + $(this).attr('href'),
		dataType: 'json',
		success: function(json) {
                        if (typeof json['order'] == 'undefined') { return false; }
			var option = {	
				shadowSize: 0,
				colors: ['#9FD5F1', '#1065D2'],
				bars: { 
					show: true,
					fill: true,
					lineWidth: 1
				},
				grid: {
					backgroundColor: '#FFFFFF',
					hoverable: true
				},
				points: {
					show: false
				},
				xaxis: {
					show: true,
            		ticks: json['xaxis']
				}
			}
			
			$.plot('#chart-sale', [json['order'], json['customer']], option);	
					
			$('#chart-sale').bind('plothover', function(event, pos, item) {
				$('.tooltip').remove();
			  
				if (item) {
					$('<div id="tooltip" class="tooltip top in"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + item.datapoint[1].toFixed(2) + '</div></div>').prependTo('body');
					
					$('#tooltip').css({
						position: 'absolute',
						left: item.pageX - ($('#tooltip').outerWidth() / 2),
						top: item.pageY - $('#tooltip').outerHeight(),
						pointer: 'cusror'
					}).fadeIn('slow');	
					
					$('#chart-sale').css('cursor', 'pointer');		
			  	} else {
					$('#chart-sale').css('cursor', 'auto');
				}
			});
		},
        error: function(xhr, ajaxOptions, thrownError) {
           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});

$('#range .active a').trigger('click');
//--></script> 
<style type="text/css">
	* {box-sizing: border-box;}
ul {list-style-type: none;}
body {font-family: Verdana, sans-serif;}

.month {
  padding: 70px 25px;
  width: 100%;
  background: #279fe0;
  text-align: center;
}

.month ul {
  margin: 0;
  padding: 0;
}

.month ul li {
  color: white;
  font-size: 20px;
  text-transform: uppercase;
  letter-spacing: 3px;
}

.month .prev {
  float: left;
  padding-top: 10px;
}

.month .next {
  float: right;
  padding-top: 10px;
}

.weekdays {
  margin: 0;
  padding: 10px 0;
  background-color: #ddd;
}

.weekdays li {
  display: inline-block;
  width: 13.6%;
  color: #666;
  text-align: center;
}

.days {
  padding: 10px 0;
  background: #eee;
  margin: 0;
}

.days li {
  list-style-type: none;
  display: inline-block;
  width: 13.6%;
  text-align: center;
  margin-bottom: 5px;
  font-size:12px;
  color: #777;
}

.days li .active {
  padding: 5px;
  background: #279fe0;
  color: white !important
}

/* Add media queries for smaller screens */
@media screen and (max-width:720px) {
  .weekdays li, .days li {width: 13.1%;}
}

@media screen and (max-width: 420px) {
  .weekdays li, .days li {width: 12.5%;}
  .days li .active {padding: 2px;}
}

@media screen and (max-width: 290px) {
  .weekdays li, .days li {width: 12.2%;}
}
</style>