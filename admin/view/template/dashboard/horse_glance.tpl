<div style="border-radius: 5px">
    <a href="<?php echo $horse_at_glance; ?>"><div class="tile" style="background: linear-gradient(100deg, #764BA2 4%, #667EEA 65%);">
        <div class="tile-heading" style="background: linear-gradient(72deg, #764BA2 4%, #667EEA 65%);"></div>
        <div class="">
            <h4 style="font-size: 14px;padding-top: 5px;text-align: center;"><?php echo "Horse At Glance"; ?></h4>
        </div>
        <div class="tile-footer" style="background: linear-gradient(100deg, #764BA2 4%, #667EEA 65%);"></div>
    </div></a>
</div>
