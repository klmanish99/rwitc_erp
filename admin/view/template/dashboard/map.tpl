<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Todays Task</h3>
	</div>
 	<div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td style="width: 11%;">Sr No</td>
                        <td style="width: 19%;">Date</td>
                        <td>Description</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-left">1</td>
                        <td class="text-left">01-02-2020</td>
                        <td class="text-left">Welcome to RWITC. Here you can see your todays task to be done. You should complete this task today.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>