<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo 'Import Indent'; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-pencil"></i> Import Indent</h3>
        </div>
        <div class="panel-body">
            <div class="tab-pane active" id="tab-general">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="indent_no">Indent No</label>
                    <div class="col-sm-2">
                        <input type="text" name="indent_no" value="" placeholder="Indent No" id="indent_no" class="form-control" data-index="1" />
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary" onclick="import_po()" >Import</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</script>

<script type="text/javascript">

$('#indent_no').autocomplete({
    delay: 500,
    source: function(request, response) {
    if(request != ''){
        $.ajax({
            url: 'index.php?route=snippet/import_indent/autocompleteIndent&token=<?php echo $token; ?>&indent_no=' +  encodeURIComponent(request),
            dataType: 'json',
            success: function(json) {
            response($.map(json, function(item) {
                return {
                label: item.order_no,
                value: item.order_no,
                }
            }));
          }
        });
    }
    }, 
    select: function(item) {
    console.log(item);
    $('#indent_no').val(item.value);
    $('#indent_no').focus();
    $('.dropdown-menu').hide();
    return false;
    },
});

document.getElementById("indent_no").onkeypress = function(event){
    if (event.keyCode == 13 || event.which == 13){
        alert("inn");
    }
};

function import_po() {
    var indent_no = $('#indent_no').val();
    url = 'index.php?route=snippet/import_indent/import_po&token=<?php echo $token; ?>';
    if (indent_no != '') {
        url += '&indent_no=' + encodeURIComponent(indent_no);
    }
    window.location.href = url;
}

</script>
  
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script></div>
<?php echo $footer; ?>