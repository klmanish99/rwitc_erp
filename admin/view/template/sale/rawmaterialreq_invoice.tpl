<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
</head>
<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
<div class="container" style="page-break-after: always;width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
	<div style=" margin-bottom: -35px;margin-top: -15px" align="center">
		<img src="<?php echo $header_image; ?>" style="width: 100px;" />
	</div>
	<br/>
	<br/>
	<br/>
	<table class="table table-bordered table_custom" style="margin-top: 0px;border: 1px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 12px;">
		<thead>
			<tr>
				<td colspan="2" style="font-size: 12px;text-align: center;font-weight: bold;">
					Rawmaterial Request
				</td>
			</tr>
			<tr>
				<td style="border-bottom: 1px solid black;width:50%;font-size: 12px;font-weight: bold;padding-left: 5px;">
				</td>
				<td style="border-bottom: 1px solid black;width:50%;font-size: 12px;text-align: right;font-weight: bold;padding-right: 5px;">
				</td>
			</tr>
			<tr>
				<td style="border-bottom: 1px solid black;border-right: 1px solid black;line-height: 1.42857143;width: 50%;vertical-align: top;font-size:12px;padding-left: 5px;">
					<span style="font-size: 12px;font-weight: bold;"><?php echo 'Company Address :'; ?><br /></span>
					<span style="font-size: 12px;font-weight:normal;padding-top: 10px;">
					Somerset Exports PVT LTD
					Shreeji Aarambh, First Floor Kanakia Road, 
					Mira Road East. Thane 400107
				  </span>
				</td>
				<td style="width: 50%;vertical-align: top;">
					<br/>
					  Order No : <?php echo $order_no; ?> <br/><br/>
					  Date : <?php echo date('d-m-Y', strtotime($date)); ?> <br/><br/>
					  Raw Material Req No : <?php echo $order_id; ?>
				</td>
			  </tr>
			  <tr>
			  	<td style="vertical-align: top;">
					Submitted By : <?php echo $requested_by; ?>
				</td>
				<td style="vertical-align: top;border: 1px solid black;">
					Narration : <?php echo $narration; ?>
				</td>
			  </tr>
		</thead>
  	</table>
  	<br/>
  	<br/>
  	<table class="table table-bordered table_custom" style="border: 1px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 10px;">
	    <thead>
			<tr style="background: #E7EFEF;">
			  <td style="text-align:center;width:32%;font-size:12px;border: 1px solid black;padding: 2px;"><b><?php echo 'Raw Material Code'; ?></b></td>
			  <td style="text-align:center;width:32%;font-size:12px;border: 1px solid black;padding: 2px;"><b><?php echo 'Product Name'; ?></b></td>
			  <td style="text-align:center;width:32%;font-size:12px;border: 1px solid black;padding: 2px;"><b><?php echo 'Quantity Ordered'; ?></b></td>
			  <td style="width:32%;text-align: center;font-size:12px;border: 1px solid black;padding: 2px;" class="text-right"><b><?php echo 'Quantity Sent'; ?></b></td>
			</tr>
	  	</thead>
		<?php foreach ($productraw_datas as $pkey => $productraw_data) { ?>
			<tbody>  
			  	<tr>
			  		<td style="text-align: center;font-size:12px;border: 1px solid black;padding: 2px;"><?php echo $productraw_data['product_code']; ?></td>
					<td style="text-align: left;font-size:12px;border: 1px solid black;padding: 2px;"><?php echo $productraw_data['productraw_name']; ?></td>
					<td style="text-align: right;font-size:12px;border: 1px solid black;padding: 2px;"><?php echo $productraw_data['quantity']; ?></td>
					<td style="text-align: right;font-size:12px;border: 1px solid black;padding: 2px;"><?php echo $productraw_data['total_qty']; ?></td>
				</tr>
			</tbody>
		<?php } ?>
	</table>
	<br/>
	<br/>
	<table class="table table-bordered table_custom" style="width: 100%;max-width: 100%;margin-bottom: 15px;background-color: transparent;border-collapse: collapse;padding: 2px !important;font-size: 10px;">
					<tr>
						<td style="width: 100%;text-align: right;padding: 16px;font-size: 12px;">
							<b> Received By </b>
						</td>
					</tr>
			</table>

</div></body></html>
<?php
  function decimal_to_words($x) {
	$x = str_replace(',','',$x);
	$pos = strpos((string)$x, ".");
	if ($pos !== false) { $decimalpart= substr($x, $pos+1, 2); $x = substr($x,0,$pos); }
	$tmp_str_rtn = number_to_words ($x);
	if(!empty($decimalpart) && $decimalpart != '00' && $decimalpart != '0') {
	  $tmp_str_rtn .= ' and '  . number_to_words ($decimalpart) . ' paise only';
	} else {
	  $tmp_str_rtn .= ' only';  
	}
	return   strtoupper($tmp_str_rtn);
  } 

  function number_to_words ($x) {
	  $nwords = array(  "", "one", "two", "three", "four", "five", "six", 
			"seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", 
			"fourteen", "fifteen", "sixteen", "seventeen", "eightteen", 
		  "nineteen", "twenty", 30 => "thirty", 40 => "fourty",
					 50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eigthy",
					 90 => "ninety" );
	  //global $nwords; 
	   if(!is_numeric($x))
	   {
		   $w = '#';
	   }else if(fmod($x, 1) != 0)
	   {
		   $w = '#';
	   }else{
		   if($x < 0)
		   {
			   $w = 'minus ';
			   $x = -$x;
		   }else{
			   $w = '';
		   }
		   if($x < 21)
		   {
			  if(!isset($nwords[$x])){
				  $x = ltrim($x, '0');
			  }
		if($x != ''){ 
				$w .= $nwords[$x];
		}
		   }else if($x < 100)
		   {
			   $w .= $nwords[10 * floor($x/10)];
			   $r = fmod($x, 10);
			   if($r > 0)
			   {
				   $w .= ' '. $nwords[$r];
			   }
		   } else if($x < 1000)
		   {
	  
			   $w .= $nwords[floor($x/100)] .' hundred';
			   $r = fmod($x, 100);
			   if($r > 0)
			   {
				   $w .= ' '. number_to_words($r);
			   }
		   } else if($x < 100000)
		   {
			$w .= number_to_words(floor($x/1000)) .' thousand';
			   $r = fmod($x, 1000);
			   if($r > 0)
			   {
				   $w .= ' ';
				   if($r < 100)
				   {
					   $w .= ' ';
				   }
				   $w .= number_to_words($r);
			   }
		   } else {
			   $w .= number_to_words(floor($x/100000)) .' lakh';
			   $r = fmod($x, 100000);
			   if($r > 0)
			   {
				   $w .= ' ';
				   if($r < 100)
				   {
					   $word .= ' ';
				   }
				   $w .= number_to_words($r);
			   }
		   }
	   }
	   return $w;
  }
?> 