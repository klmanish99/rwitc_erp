<?php echo $header; ?><?php echo $column_left; ?>

   
<div id="content" style=" overflow-x: hidden;">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_install) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_install; ?>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="container-fluid" >
            <div class="row" >
                <?php if (($user_group_id == 1) || ($user_group_id == 12) || ($user_group_id == 11)) { ?>
                    <div class="col-sm-12 col_six_div col_twl" >
                        <div class="heading_div" >
                            <label >Racing Master </label>
                        </div>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth" >
                                    <img src="view/image/Owner/owner.jpg" class="img_tag"  />
                                    <div class="mask">
                                      <a href="<?php echo $owner_master ?>" ><h2>Owner</h2></a>
                                      <a href="<?php echo $owner_master ?>" ><p>Total Owner : <?php echo $total_owner; ?></p></a>
                                      <a href="<?php echo $owner_at_glance ?>" class="info"> Owner At Glance </a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2"  >
                                <div class="view view-tenth">
                                    <img src="view/image/\Trainer/trainer.jpg" class="img_tag" />
                                    <div class="mask">
                                      <a href="<?php echo $trainer_master ?>" ><h2>Trainer</h2></a>
                                      <a href="<?php echo $trainer_master ?>" ><p>Total Trainer : <?php echo $total_trainer; ?></p></a>
                                      <a href="<?php echo $trainer_at_glance ?>" class="info"> Trainer At Glance </a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Horse/horse.jpg" class="img_tag" />
                                    <div class="mask">
                                      <a href="<?php echo $horse_master ?>" ><h2>Horse</h2></a>
                                      <a href="<?php echo $horse_master ?>" ><p>Total Horses : <?php echo $total; ?></p></a>
                                      <a href="<?php echo $horse_at_glance ?>" class="info"> Horse At Glance </a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Jockey/jockey.jpg" class="img_tag" />
                                    <div class="mask">
                                      <a href="<?php echo $jockey_master ?>" ><h2>Jockey</h2></a>
                                      <a href="<?php echo $jockey_master ?>" ><p>Total Jockey : <?php echo $total_jockey; ?></p></a>
                                      <a href="<?php echo $jockey_at_glance ?>" class="info"> Jockey At Glance </a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        
                    </div>
                <?php } ?>


                <?php if (($user_group_id == 1) || ($user_group_id == 12) ) { ?>
                    <div class="col-sm-12 col_six_div col_twl" >
                        <div class="heading_div" >
                            <label > Equine Master </label>
                        </div>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Supplier/supplier.jpg" class="img_tag" />
                                    <div class="mask">
                                      <a href="<?php echo $supplier_master ?>" ><h2 style="margin: 20px 8px 0px 9px;" >Supplier</h2></a>
                                      <a href="<?php echo $supplier_master ?>" ><p style="padding: 0px 13px 39px;" >Total Suppliers : <?php echo $total_supplier; ?></p></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Medicine/medicine.jpg" class="img_tag" />
                                    <div class="mask">
                                      <a href="<?php echo $medicine_master ?>" ><h2 style="margin: 20px 4px 0px 7px;">Medicine</h2></a>
                                      <a href="<?php echo $medicine_master ?>" ><p style="padding: 0px 9px 39px">Total Medicines : <?php echo $total_medicine; ?></p></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Doctor/doctor.jpg" class="img_tag" />
                                    <div class="mask">
                                      <a href="<?php echo $doctor_master ?>" ><h2 style="margin: 20px 14px 0px 13px;">Doctor</h2></a>
                                      <a href="<?php echo $doctor_master ?>" ><p style="padding: 0px 10px 39px 13px;">Total Doctors : <?php echo $total_doctor; ?></p></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 'no') || ($user_group_id == 'no')) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Service/service.png" class="img_tag" />
                                    <div class="mask">
                                      <a href="<?php echo $service_master ?>" ><h2 style="margin: 20px 11px 0px 11px;">Service Charge</h2></a>
                                      <a href="<?php echo $service_master ?>" ><p style="padding: 0px 20px 0px;">Total Service Charges : <?php echo $total_service_charge; ?></p></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>


                <?php if (($user_group_id == 1)  || ($user_group_id == 11) || ($user_group_id == 15)) { ?>
                    <div class="col-sm-12 col_six_div_trans col_twl">
                        <div class="heading_div" >
                            <label >Racing Transaction </label>
                        </div>
                        
                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth ">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />

                                    <div class="mask mask_2">
                                        <a href="<?php echo $prospectus_master ?>" ><h2>Prospectus</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />

                                    <div class="mask mask_1">
                                        <a href="<?php echo $entries_master ?>" ><h2>Entries</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img style="padding-left: 0px;padding-top: 0px;height: 129px;width: 129px;" src="view/image/Dashboard/Handicapping.jpeg" class="img_tag" />

                                    <div style="width: 129px;" class="mask mask_2 mask_4 ">
                                        <a href="<?php echo $handicapping_master ?>" ><h2 style="padding-left: 3px;">Handicapping</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth align">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />

                                    <div class="mask mask_2">
                                        <a href="<?php echo $acceptance_master ?>" ><h2>Acceptance</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $ownership_master ?>" ><h2>Ownership Registration/<br>Transfer</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $cancel_ownership_master ?>" ><h2 style="padding-left: 27px;">Cancel Lease Owner</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $reg_master ?>" ><h2>Name Registration</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $arrival_charges_master ?>" ><h2>Arrival Charges</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $jockey_lisence_trans ?>" ><h2>Jockey License</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $trainer_license_trans ?>" ><h2>Trainer License</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $assign_trainer ?>" ><h2>Trainer To Staff</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

                <?php if (($user_group_id == 1) || ($user_group_id == 12)  || ($user_group_id == 15)) { ?>
                    <div class="col-sm-12 col_six_div_trans col_twl">
                        <div class="heading_div" >
                            <label > Equine Transaction </label>
                        </div>
                        
                        
                        <?php if (($user_group_id == 1) || ($user_group_id == 12) || ($user_group_id == 15)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $indent_master ?>" ><h2 style="padding-left: 25px;">Indent</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $importtally_master ?>" ><h2 style="padding-left: 0px;margin: 20px 0px 0px 17px;">Tally PO List</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12) || ($user_group_id == 15)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $inward_master ?>" ><h2 style="padding-left: 25px;">Inward</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $medicinetransfer_master ?>" ><h2 style="padding-left: 14px;">Medicine Transfer</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $treatment_entry ?>" ><h2 style="padding-left: 14px;">Treatment Entry</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $medicine_transaction ?>" ><h2 style="padding-left: 0px;">Daily Reconsilation</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $store_reconsilation ?>" ><h2 style="padding-left: 0px;">Store Reconsilation</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2" >
                                <div class="view view-tenth">
                                    <img src="view/image/Registration/registraion.jpg" class="img_tag" />
                                    <div class="mask mask_2 mask_3">
                                        <a href="<?php echo $medicine_return ?>" ><h2 style="padding-left: 17px;">Medicine Return</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

                <?php if (($user_group_id == 1)  || ($user_group_id == 11)) { ?>
                    <div class="col-sm-12 col_six_div_trans col_twl">
                        <div class="heading_div" >
                            <label >Racing Report </label>
                        </div>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $arrival_report ?>" ><h2>Arrival Charge</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $ownership_report ?>" ><h2>Ownership Charge</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $name_change_report ?>" ><h2>Name Change</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $undertaking_charge_master ?>" ><h2>Undertaking Charge</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $trainer_license_transaction ?>" ><h2 style="padding-left: 20px;">Trainer License</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (($user_group_id == 1) || ($user_group_id == 11)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $jockey_license_transaction ?>" ><h2 style="padding-left: 20px;">Jockey License</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

                <?php if (($user_group_id == 1) || ($user_group_id == 12) ) { ?>
                    <div class="col-sm-12 col_six_div_trans col_twl">
                        <div class="heading_div" >
                            <label >Equine Report </label>
                        </div>

                        
                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $inward_report ?>" ><h2>Inward</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $indent_report ?>" ><h2>Indent</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $med_transfer_report ?>" ><h2>Medicine Transfer</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $stock_report ?>" ><h2>Stock</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $medicine_report ?>" ><h2>Medicine Dump</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $horsewise_treat_entry ?>" ><h2>Horse Treatment</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (($user_group_id == 1) || ($user_group_id == 12)) { ?>
                            <div class="col-sm-2 col_sm_2 " >
                                <div class="view view-tenth">
                                    <img src="view/image/Reports/cheque.png" class="img_tag" />
                                    <div class="mask mask_2">
                                        <a href="<?php echo $report_reconsilation ?>" ><h2 style="padding-left: 0px;">Reconsilation</h2></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
 <style type="text/css">
    /* My Css  */

    @media (min-width: 768px) 
           {
         .col_sm_2{
            width: 14% !important;
        }
    }
   
    .col_six_div{
         height: auto;
         box-shadow:0px 9px 16px #ccc;
        /* width: 61em;*/
    }

    .img_tag{
      padding-left: 24px;
      width: 80%;
      height: 85%; 
      padding-top: 16%;
    }
    
     .col_six_div_trans{
        height: auto;
        box-shadow:7px 12px 20px #ccc;
        /*width: 61em;*/
       margin-top:4em;
    }

    .heading_div{
       text-align: left;
       font-size: 18px;
       color: black;
       padding-top: 1em;

    }

    .col_twl{
       padding-bottom: 1em;
    }


/* generic css */


.view {
  margin: 10px;
  float: left;
  border: 2px solid #fff;
  overflow: hidden;
  position: relative;
  /*text-align: center;*/
  box-shadow: 1px 6px 10px #ccc;
  cursor: default;
  height: 133px;
  width: 133px;
  background: #fff;
}

.view .mask,
.view .content {
  /*width: 150px;*/
  position: absolute;
  overflow: hidden;
  top: 0rem;
  left: 0
}


.view img {
  display: block;
  position: relative
}

.view h2 {
  text-transform: uppercase;
  color: #fff;
  /*text-align: center;*/
  position: relative;
  font-size: 20px;
  font-family: Raleway, serif;
  padding: 10px;
  background: rgba(0, 0, 0, 0.8);
  margin: 20px 0 0 0
}

.view p {
  font-family: Merriweather, serif;
  /*font-style: italic;*/
  font-size: 14px;
  position: relative;
  color: #fff;
  padding: 0px 20px 0px;
  /*text-align: center*/
}

.view a.info {
  display: inline-block;
  text-decoration: none;
  padding: 3px 11px;
  background: #000;
  color: #fff;
  font-family: Raleway, serif;
  text-transform: uppercase;
  box-shadow: 0 0 1px #000;

}

a.info{
    margin-top: 1rem !important;
}

.view a.info:hover {
  box-shadow: 0 0 5px #000
}

/*3*/

.view-tenth img {
  transform: scaleY(1);
  transition: all .7s ease-in-out;
}

.view-tenth .mask {
  background-color: rgb(136 244 68 / 30%);
  transition: all 0.5s linear;
  opacity: 0;
}

.view-tenth h2 {
  /*border-bottom: 1px solid rgba(0, 0, 0, 0.3);*/
  background: transparent;
  margin: 20px 40px 0px 17px;
  /*margin-left: 20px;*/
  transform: scale(0);
  color: #333;
  transition: all 0.5s linear;
  opacity: 0;
}

.view-tenth p {
  color: #333;
  opacity: 0;
  transform: scale(0);
  transition: all 0.5s linear;
  
}

.view-tenth a.info {
  opacity: 0;
  transform: scale(0);
  transition: all 0.5s linear;
}

.view-tenth:hover img {
  -webkit-transform: scale(10);
  transform: scale(10);
  opacity: 0;
}

.view-tenth:hover .mask {
  opacity: 1;
}

.view-tenth:hover h2,
.view-tenth:hover p,
.view-tenth:hover a.info {
  transform: scale(1);
  opacity: 1;
}

.align:hover .mask h2{
  
}


.mask_2{text-align: center;padding: 11px;background-color: rgb(136 244 68 / 30%);height: 200px;}
.mask_2 a h2{font-size: 14px;margin-left: 0px;}
.mask_2 a p{font-size: 16px;margin-left: -38px;}
.mask_3 a p{font-size: 16px;margin-left: -60px;}
.mask_4 a h2{margin-left: 0px;}


.mask_1{height: 200px;}
.mask_1 a h2{padding-left: 0px;padding-top: 16px;}
.mask_1 a p{padding-left: 28px;
    padding-top: 4px;}
    </style>
<?php echo $footer; ?>