<ul id="menu">
	<li id="dashboard"><a href="<?php echo $home; ?>"><i class="fa fa-dashboard fa-fw"></i> <span><?php echo $text_dashboard; ?></span></a></li>
<!-- ---------------Admin-------------------------- -->
	<?php if ($group_id == '1') { ?>
		<li id="catalog"><a class="parent"><i class="fa fa-user fa-fw"></i> <span><?php echo $text_catalog; ?></span></a>
			<ul>
				<li><a class="parent">Racing</a>
				  	<ul>
						<li><a href="<?php echo $owner; ?>"><?php echo $text_owner; ?></a></li>
						<li><a href="<?php echo $trainer; ?>"><?php echo $text_trainer; ?></a></li>
						<li><a href="<?php echo $horse; ?>"><?php echo $text_horse; ?></a></li>
						<li><a href="<?php echo $jockey; ?>"><?php echo $text_jockey; ?></a></li>
						<li><a href="<?php echo $color; ?>"><?php echo $text_color; ?></a></li>
						<li><a href="<?php echo $shoe; ?>"><?php echo 'Shoe'; ?></a></li>
						<li><a href="<?php echo $bit; ?>"><?php echo 'Bit'; ?></a></li>
						<li><a href="<?php echo $equipments; ?>"><?php echo $text_equipments; ?></a></li>
						<li><a href="<?php echo $racetype; ?>"><?php echo $text_racetype; ?></a></li>
				  		<li><a href="<?php echo $stacks; ?>"><?php echo "Stakes"; ?></a></li>
				  		<li><a href="<?php echo $class_entry; ?>"><?php echo "Class Entry"; ?></a></li>
				  		<li><a href="<?php echo $trainer_staff; ?>">Trainer Staff</a></li>
					</ul>
				</li>
				<li><a class="parent">Equine</a>
				  	<ul>
						<li><a href="<?php echo $vendor; ?>"><?php echo $text_vendor; ?></a></li>
						<li><a href="<?php echo $medicine; ?>">Medicine</a></li>
						<li><a href="<?php echo $doctor; ?>">Doctor</a></li>
						<li><a href="<?php echo $service; ?>">Service Charge</a></li>
						
				  	</ul>
				</li>
			</ul>
		</li>

		<li id="transaction"><a class="parent"><i class="fa fa-exchange fa-fw"></i> <span><?php echo "Transaction"; ?></span></a>
		  	<ul>
		  		<li><a class="parent">Racing</a>
		  			<ul>
				  		<li><a href="<?php echo $tranProspectus; ?>"><?php echo "Prospectus"; ?></a></li>
				  		<li><a href="<?php echo $tranentries; ?>"><?php echo "Entries"; ?></a></li>
				  		<li><a href="<?php echo $tranhandicapping; ?>"><?php echo "Handicapping"; ?></a></li>
				  		<li><a href="<?php echo $tranacceptance; ?>"><?php echo "Acceptance"; ?></a></li>
				  		<li><a href="<?php echo $ownership_shift_module; ?>"><?php echo "Ownership Reg"; ?></a></li>
				  		<li><a href="<?php echo $cancel_ownership; ?>"><?php echo "Cancel Lease Own."; ?></a></li>
				  		<li><a href="<?php echo $registeration_module; ?>"><?php echo "Name Registration"; ?></a></li>
				  		<li><a href="<?php echo $multi_arrival_charges; ?>"><?php echo "Arrival Charges"; ?></a></li>
				  		<li><a href="<?php echo $jockey_lisence_trans; ?>"><?php echo "Jockey License"; ?></a></li>
				  		<li><a href="<?php echo $trainer_license_trans; ?>"><?php echo "Trainer License"; ?></a></li>
				  		<li><a href="<?php echo $assign_trainer; ?>">Trainer To Staff</a></li>
				  		<li><a href="<?php echo $multi_horse_transfer; ?>">Multi Horse Transfer</a></li>
				  		<li><a href="<?php echo $jockey_ban; ?>">Jockey Ban</a></li>
				  		<li><a href="<?php echo $trainer_ban; ?>">Trainer Ban</a></li>

				  	</ul>
		  		</li>
		  		<li><a class="parent">Equine</a>
				  	<ul>
						<li><a href="<?php echo $intend; ?>">Indent</a></li>
						<li><a href="<?php echo $import_list; ?>">Tally PO List</a></li>
						<li><a href="<?php echo $inward; ?>">Inward</a></li>
						<li><a href="<?php echo $medicine_tran; ?>">Medicine Transfer</a></li>
						<li><a href="<?php echo $treatment_entry_single; ?>">Treatment Entry</a></li>
						<li><a href="<?php echo $med_transaction; ?>"  ><?php echo "Daily Reconsilation"; ?></a></li>
						<li><a href="<?php echo $store_reconsilation; ?>"  ><?php echo "Store Reconsilation"; ?></a></li>
						<li><a href="<?php echo $medicine_return; ?>">Medicine Return</a></li>
				  	</ul>
				</li>
		  	</ul>
		</li>

		<li id="reports"><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span><?php echo $text_reports; ?></span></a>
			<ul>
				<li><a class="parent">Racing</a>
					<ul>
						<li><a href="<?php echo $arrival_charge_report; ?>"  ><?php echo "Arrival Charge"; ?></a></li>
						<li><a href="<?php echo $ownership_registration_report; ?>"  ><?php echo "Ownership/Reg"; ?></a></li>
						<li><a href="<?php echo $name_change_report; ?>"  ><?php echo "Name Change"; ?></a></li>
						<li><a href="<?php echo $report_horse_undertaking_charge; ?>"  ><?php echo "Undertaking Charge"; ?></a></li>
						<li><a href="<?php echo $trainer_license_transaction; ?>"  ><?php echo "Trainer License"; ?></a></li>
						<li><a href="<?php echo $jockey_license_transaction; ?>"  ><?php echo "Jockey License"; ?></a></li>
					</ul>
				</li>
				<li><a class="parent">Equine</a>
					<ul>
						<li style="display: none;"><a class="parent"><?php echo "Racing"; ?></a></li>
						<li><a href="<?php echo $inward_report; ?>"  ><?php echo "Inward"; ?></a></li>
						<li><a href="<?php echo $indent_report; ?>"  ><?php echo "Indent"; ?></a></li>
						<li><a href="<?php echo $med_transfer_report; ?>"  ><?php echo "Medicine Transfer"; ?></a></li>
						<li><a href="<?php echo $stock_report; ?>"  ><?php echo "Stock"; ?></a></li>
						<li><a href="<?php echo $medicine_report; ?>"  ><?php echo "Medicine Dump"; ?></a></li>
						<li><a href="<?php echo $horsewise_treat_report; ?>"  ><?php echo "Horse Treatment"; ?></a></li>
						<li><a href="<?php echo $report_reconsilation; ?>"  ><?php echo "Reconsilation"; ?></a></li>
						<li><a href="<?php echo $cancel_own; ?>"  ><?php echo "Cancel Ownership"; ?></a></li>
						
					</ul>
				</li>
			</ul>
		</li>

		<li id="system"> <a class="parent"><i class="fa fa-lock fa-fw"></i> <span>User</span></a>
			<ul>
		  		<li><a href="<?php echo $user; ?>"><?php echo $text_user; ?></a></li>
		  		<li><a href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?></a></li>
		  		<li><a href="<?php echo $import_indent; ?>">Import Indent</a></li>
		  		<li><a href="<?php echo $is_end; ?>"><?php echo 'TR License Expired'; ?></a></li>
				<li><a href="<?php echo $is_end_j; ?>"><?php echo 'JK License Expired'; ?></a></li>
			</ul>
		</li>

<?php } ?>
<!-- ---------------Admin-------------------------- -->

<!-- ---------------Racing-------------------------- -->

<?php if ($group_id == '11') { ?>
	<li id="catalog"><a class="parent"><i class="fa fa-user fa-fw"></i> <span>Master</span></a>
		<ul>
			<li><a href="<?php echo $owner; ?>"><?php echo $text_owner; ?></a></li>
			<li><a href="<?php echo $trainer; ?>"><?php echo $text_trainer; ?></a></li>
			<li><a href="<?php echo $horse; ?>"><?php echo $text_horse; ?></a></li>
			<li><a href="<?php echo $jockey; ?>"><?php echo $text_jockey; ?></a></li>
		</ul>
	</li>

	<li id="catalog"><a class="parent"><i class="fa fa-exchange fa-fw"></i> <span>Transaction</span></a>
		<ul>
			<li><a href="<?php echo $tranProspectus; ?>"><?php echo "Prospectus"; ?></a></li>
			<li><a href="<?php echo $tranentries; ?>"><?php echo "Entries"; ?></a></li>
			<li><a href="<?php echo $tranhandicapping; ?>"><?php echo "Handicapping"; ?></a></li>
			<li><a href="<?php echo $tranacceptance; ?>"><?php echo "Acceptance"; ?></a></li>
			<li><a href="<?php echo $ownership_shift_module; ?>"><?php echo "Ownership Reg"; ?></a></li>
			<li><a href="<?php echo $cancel_ownership; ?>"><?php echo "Cancel Lease Own."; ?></a></li>
	  		<li><a href="<?php echo $registeration_module; ?>"><?php echo "Name Registration"; ?></a></li>
	  		<li><a href="<?php echo $multi_arrival_charges; ?>"><?php echo "Arrival Charges"; ?></a></li>
	  		<li><a href="<?php echo $jockey_lisence_trans; ?>"><?php echo "Jockey License"; ?></a></li>
	  		<li><a href="<?php echo $trainer_license_trans; ?>"><?php echo "Trainer License"; ?></a></li>
	  		<li><a href="<?php echo $assign_trainer; ?>">Trainer To Staff</a></li>
			<li><a href="<?php echo $multi_horse_transfer; ?>">Multi Horse Transfer</a></li>
			<li><a href="<?php echo $jockey_ban; ?>">Jockey Ban</a></li>
			<li><a href="<?php echo $trainer_ban; ?>">Trainer Ban</a></li>
		</ul>
	</li>

	<li id="reports"><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span><?php echo $text_reports; ?></span></a>
		<ul>
			<li><a href="<?php echo $arrival_charge_report; ?>"  ><?php echo "Arrival Charge"; ?></a></li>
			<li><a href="<?php echo $ownership_registration_report; ?>"  ><?php echo "Ownership/Reg"; ?></a></li>
			<li><a href="<?php echo $name_change_report; ?>"  ><?php echo "Name Change"; ?></a></li>
			<li><a href="<?php echo $report_horse_undertaking_charge; ?>"  ><?php echo "Undertaking Charge"; ?></a></li>
			<li><a href="<?php echo $trainer_license_transaction; ?>"  ><?php echo "Trainer License"; ?></a></li>
			<li><a href="<?php echo $jockey_license_transaction; ?>"  ><?php echo "Jockey License"; ?></a></li>
		</ul>
	</li>
<?php } ?>
<!-- ---------------Racing-------------------------- -->

<!-- ---------------Equine-------------------------- -->
<?php if ($group_id == '12') { ?>
	<li id="transaction"><a class="parent"><i class="fa fa-user fa-fw"></i> <span>Master</span></a>
		<ul>
			<li><a href="<?php echo $horse; ?>"><?php echo $text_horse; ?></a></li>
			<li><a href="<?php echo $vendor; ?>"><?php echo $text_vendor; ?></a></li>
			<li><a href="<?php echo $medicine; ?>">Medicine</a></li>
			<li><a href="<?php echo $doctor; ?>">Doctor</a></li>
			<li><a href="<?php echo $service; ?>">Service Charge</a></li>
		</ul>
	</li>
	<li id="catalog"><a class="parent"><i class="fa fa-exchange fa-fw"></i> <span>Transaction</span></a>
		<ul>
			<li><a href="<?php echo $intend; ?>">Indent</a></li>
			<li><a href="<?php echo $import_list; ?>"><?php echo 'Tally PO List'; ?></a></li>
			<li><a href="<?php echo $inward; ?>">Inward</a></li>
			<li><a href="<?php echo $medicine_tran; ?>">Medicine Transfer</a></li>
			<li><a href="<?php echo $treatment_entry_single; ?>">Treatment Entry</a></li>
			<li><a href="<?php echo $med_transaction; ?>"  ><?php echo "Daily Reconsilation"; ?></a></li>
			<li><a href="<?php echo $store_reconsilation; ?>"  ><?php echo "Store Reconsilation"; ?></a></li>
			<li><a href="<?php echo $medicine_return; ?>">Medicine Return</a></li>
		</ul>
	</li>

	<li id="reports"><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span><?php echo $text_reports; ?></span></a>
		<ul>
			<li><a href="<?php echo $inward_report; ?>"  ><?php echo "Inward"; ?></a></li>
			<li><a href="<?php echo $indent_report; ?>"  ><?php echo "Indent"; ?></a></li>
			<li><a href="<?php echo $med_transfer_report; ?>"  ><?php echo "Medicine Transfer"; ?></a></li>
			<li><a href="<?php echo $stock_report; ?>"  ><?php echo "Stock"; ?></a></li>
			<li><a href="<?php echo $medicine_report; ?>"  ><?php echo "Medicine Dump"; ?></a></li>
			<li><a href="<?php echo $horsewise_treat_report; ?>"  ><?php echo "Horse Treatment"; ?></a></li>
			<li><a href="<?php echo $report_reconsilation; ?>"  ><?php echo "Reconsilation"; ?></a></li>
			<li><a href="<?php echo $cancel_own; ?>"  ><?php echo "Cancel Ownership"; ?></a></li>
		</ul>
	</li>
	<li id="reports"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span>Import Indent</span></a>
		<ul>
			<li><a href="<?php echo $import_indent; ?>">Generate PO</a></li>
		</ul>
	</li>

<?php } ?>
<!-- ---------------Equine-------------------------- -->

<!-- ---------------Approval-------------------------- -->
<?php if ($group_id == '15') { ?>

	<li id="transaction"><a class="parent"><i class="fa fa-exchange fa-fw"></i> <span><?php echo "Transaction"; ?></span></a>
	  	<ul>
			<li><a href="<?php echo $intend; ?>">Indent</a></li>
			<li><a href="<?php echo $inward; ?>">Inward</a></li>
	  	</ul>
	</li>
<?php } ?>

<!-- ---------------Approval-------------------------- -->

<!-- ---------------stipes-------------------------- -->
<?php if ($group_id == '16') { ?>

	<li id="transaction"><a class="parent"><i class="fa fa-exchange fa-fw"></i> <span><?php echo "Transaction"; ?></span></a>
		<ul>
	  		<li><a href="<?php echo $jockey_ban; ?>">Jockey Ban</a></li>
	  	</ul>
	</li>
<?php } ?>

<!-- ---------------stipes-------------------------- -->

  <li style="display: none;" id="extension"><a class="parent"><i class="fa fa-puzzle-piece fa-fw"></i> <span><?php echo $text_extension; ?></span></a>
	<ul>
	  <li><a href="<?php echo $installer; ?>"><?php echo $text_installer; ?></a></li>
	  <li><a href="<?php echo $modification; ?>"><?php echo $text_modification; ?></a></li>
	  <li><a href="<?php echo $theme; ?>"><?php echo $text_theme; ?></a></li>
	  <li><a href="<?php echo $analytics; ?>"><?php echo $text_analytics; ?></a></li>
	  <li><a href="<?php echo $captcha; ?>"><?php echo $text_captcha; ?></a></li>
	  <li><a href="<?php echo $feed; ?>"><?php echo $text_feed; ?></a></li>
	  <li><a href="<?php echo $fraud; ?>"><?php echo $text_fraud; ?></a></li>
	  <li><a href="<?php echo $module; ?>"><?php echo $text_module; ?></a></li>
	  <li><a href="<?php echo $payment; ?>"><?php echo $text_payment; ?></a></li>
	  <li><a href="<?php echo $shipping; ?>"><?php echo $text_shipping; ?></a></li>
	  <li><a href="<?php echo $total; ?>"><?php echo $text_total; ?></a></li>
	  <?php if ($openbay_show_menu == 1) { ?>
	  <li ><a class="parent"><?php echo $text_openbay_extension; ?></a>
		<ul>
		  <li><a href="<?php echo $openbay_link_extension; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
		  <li><a href="<?php echo $openbay_link_orders; ?>"><?php echo $text_openbay_orders; ?></a></li>
		  <li><a href="<?php echo $openbay_link_items; ?>"><?php echo $text_openbay_items; ?></a></li>
		  <?php if ($openbay_markets['ebay'] == 1) { ?>
		  <li><a class="parent"><?php echo $text_openbay_ebay; ?></a>
			<ul>
			  <li><a href="<?php echo $openbay_link_ebay; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
			  <li><a href="<?php echo $openbay_link_ebay_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
			  <li><a href="<?php echo $openbay_link_ebay_links; ?>"><?php echo $text_openbay_links; ?></a></li>
			  <li><a href="<?php echo $openbay_link_ebay_orderimport; ?>"><?php echo $text_openbay_order_import; ?></a></li>
			</ul>
		  </li>
		  <?php } ?>
		  <?php if ($openbay_markets['amazon'] == 1) { ?>
		  <li><a class="parent"><?php echo $text_openbay_amazon; ?></a>
			<ul>
			  <li><a href="<?php echo $openbay_link_amazon; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
			  <li><a href="<?php echo $openbay_link_amazon_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
			  <li><a href="<?php echo $openbay_link_amazon_links; ?>"><?php echo $text_openbay_links; ?></a></li>
			</ul>
		  </li>
		  <?php } ?>
		  <?php if ($openbay_markets['amazonus'] == 1) { ?>
		  <li><a class="parent"><?php echo $text_openbay_amazonus; ?></a>
			<ul>
			  <li><a href="<?php echo $openbay_link_amazonus; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
			  <li><a href="<?php echo $openbay_link_amazonus_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
			  <li><a href="<?php echo $openbay_link_amazonus_links; ?>"><?php echo $text_openbay_links; ?></a></li>
			</ul>
		  </li>
		  <?php } ?>
		  <?php if ($openbay_markets['etsy'] == 1) { ?>
		  <li><a class="parent"><?php echo $text_openbay_etsy; ?></a>
			<ul>
			  <li><a href="<?php echo $openbay_link_etsy; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
			  <li><a href="<?php echo $openbay_link_etsy_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
			  <li><a href="<?php echo $openbay_link_etsy_links; ?>"><?php echo $text_openbay_links; ?></a></li>
			</ul>
		  </li>
		  <?php } ?>
		</ul>
	  </li>
	  <?php } ?>
	</ul>
  </li>
  <li style="display: none;" id="design"><a class="parent"><i class="fa fa-television fa-fw"></i> <span><?php echo $text_design; ?></span></a>
	<ul>
	  <li><a href="<?php echo $layout; ?>"><?php echo $text_layout; ?></a></li>
	  <li><a href="<?php echo $banner; ?>"><?php echo $text_banner; ?></a></li>
	</ul>
  </li>
  <li style="display: none;" id="sale"><a class="parent"><i class="fa fa-shopping-cart fa-fw"></i> <span><?php echo $text_sale; ?></span></a>
	<ul>
	  <li><a href="<?php echo $prospectus; ?>"><?php echo $text_prospectus; ?></a></li>
	  <li><a href="<?php echo $entries; ?>"><?php echo $text_entries; ?></a></li>
	  <li><a href="<?php echo $handicapping; ?>"><?php echo $text_handicapping; ?></a></li>
	  <li><a href="<?php echo $acceptance; ?>"><?php echo $text_acceptance; ?></a></li>
	  <li><a href="<?php echo $declaration; ?>"><?php echo $text_declaration; ?></a></li>
	  <li><a href="<?php echo $race_result; ?>"><?php echo $text_race_result; ?></a></li>
	  <li><a href="<?php echo $re_rating; ?>"><?php echo $text_re_rating; ?></a></li>

	  <!-- <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li> -->
	  <li style="display: none;"><a href="<?php echo $order_recurring; ?>"><?php echo $text_order_recurring; ?></a></li>
	  <li style="display: none;"><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
	  <li style="display: none;"><a class="parent"><?php echo $text_voucher; ?></a>
		<ul>
		  <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
		  <li><a href="<?php echo $voucher_theme; ?>"><?php echo $text_voucher_theme; ?></a></li>
		</ul>
	  </li>
	  <li style="display: none;"><a class="parent"><?php echo $text_paypal ?></a>
		<ul>
		  <li><a href="<?php echo $paypal_search ?>"><?php echo $text_paypal_search ?></a></li>
		</ul>
	  </li>
	</ul>
  </li>
  <li style="display: none;" id="customer"><a class="parent"><i class="fa fa-user fa-fw"></i> <span><?php echo $text_customer; ?></span></a>
	<ul>
	  <li><a href="<?php echo $customer; ?>"><?php echo $text_customer; ?></a></li>
	  <li><a href="<?php echo $customer_group; ?>"><?php echo $text_customer_group; ?></a></li>
	  <li><a href="<?php echo $custom_field; ?>"><?php echo $text_custom_field; ?></a></li>
	</ul>
  </li>
  <li style="display: none;"><a class="parent"><i class="fa fa-share-alt fa-fw"></i> <span><?php echo $text_marketing; ?></span></a>
	<ul>
	  <li><a href="<?php echo $marketing; ?>"><?php echo $text_marketing; ?></a></li>
	  <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
	  <li><a href="<?php echo $coupon; ?>"><?php echo $text_coupon; ?></a></li>
	  <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
	</ul>
  </li>
  <li id="system" style="display: none;"> <a class="parent"><i class="fa fa-cog fa-fw"></i> <span><?php echo $text_system; ?></span></a>
	<ul>
	  <li><a href="<?php echo $setting; ?>"><?php echo $text_setting; ?></a></li>
	  <li><a class="parent"><?php echo $text_users; ?></a>
		<ul>
		  <li><a href="<?php echo $user; ?>"><?php echo $text_user; ?></a></li>
		  <li><a href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?></a></li>
		  <li><a href="<?php echo $api; ?>"><?php echo $text_api; ?></a></li>
		</ul>
	  </li>
	  <li><a class="parent"><?php echo $text_localisation; ?></a>
		<ul>
		  <li><a href="<?php echo $location; ?>"><?php echo $text_location; ?></a></li>
		  <li><a href="<?php echo $language; ?>"><?php echo $text_language; ?></a></li>
		  <li><a href="<?php echo $currency; ?>"><?php echo $text_currency; ?></a></li>
		  <li><a href="<?php echo $stock_status; ?>"><?php echo $text_stock_status; ?></a></li>
		  <li><a href="<?php echo $order_status; ?>"><?php echo $text_order_status; ?></a></li>
		  <li><a class="parent"><?php echo $text_return; ?></a>
			<ul>
			  <li><a href="<?php echo $return_status; ?>"><?php echo $text_return_status; ?></a></li>
			  <li><a href="<?php echo $return_action; ?>"><?php echo $text_return_action; ?></a></li>
			  <li><a href="<?php echo $return_reason; ?>"><?php echo $text_return_reason; ?></a></li>
			</ul>
		  </li>
		  <li><a href="<?php echo $country; ?>"><?php echo $text_country; ?></a></li>
		  <li><a href="<?php echo $zone; ?>"><?php echo $text_zone; ?></a></li>
		  <li><a href="<?php echo $geo_zone; ?>"><?php echo $text_geo_zone; ?></a></li>
		  <li><a class="parent"><?php echo $text_tax; ?></a>
			<ul>
			  <li><a href="<?php echo $tax_class; ?>"><?php echo $text_tax_class; ?></a></li>
			  <li><a href="<?php echo $tax_rate; ?>"><?php echo $text_tax_rate; ?></a></li>
			</ul>
		  </li>
		  <li><a href="<?php echo $length_class; ?>"><?php echo $text_length_class; ?></a></li>
		  <li><a href="<?php echo $weight_class; ?>"><?php echo $text_weight_class; ?></a></li>
		</ul>
	  </li>
	  <li><a class="parent"><?php echo $text_tools; ?></a>
		<ul>
		  <li><a href="<?php echo $upload; ?>"><?php echo $text_upload; ?></a></li>
		  <li><a href="<?php echo $backup; ?>"><?php echo $text_backup; ?></a></li>
		  <li><a href="<?php echo $error_log; ?>"><?php echo $text_error_log; ?></a></li>
		</ul>
	  </li>
	</ul>
  </li>
  <!-- <li id="help"><a class="parent"><i class="fa fa-cogs fa-fw"></i><span><?php echo 'Utility'; ?></span></a>
	<ul>
		<li><a href="<?php echo $import_horses; ?>"><?php echo 'Import Horse'; ?></a></li>
		
	</ul>
  </li> -->
</ul>
