<?php //echo $header; ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link type="text/css" href="view/stylesheet/custom.css" rel="stylesheet" media="screen" />

<div class="mainContainer">
    <div class="myCard">
        <div class="mainlogoContainer">
            <div class="logoContainer">
                <img src="../image/rwitc_logo3.PNG">
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <?php if ($error_warning) { ?>
            <p class="errorLogin">Username or password does not matched.</p>
        <?php } ?>
            <div class="inputUsername">
                <label style="margin-bottom:0px" for="input-username" class="pure-material-textfield-standard">
                    <input id="input-username" type="text" name="username" value="<?php echo $username; ?>" placeholder=" " autocomplete="off">
                    <span class="username_span">Username</span>
                </label> 
            </div>
            <div class="inputUsername">
                <label style="margin-bottom:0px" for="input-password" class="pure-material-textfield-standard">
                    <input id="input-password" type="password" name="password" value="<?php echo $password; ?>" placeholder=" " autocomplete="off">
                    <span class="password_span">Password</span>
                </label> 
            </div>
            <div class="formFooter" >
                <?php if ($forgotten) { ?>
                    <span class="help-block"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten.'?'; ?></a></span>
                <?php } ?>
              <div class="text-right">
                <button type="submit" class="btn btn-primary btnLogin"><i class="fa fa-key"></i> <?php echo 'Submit'; ?></button>
              </div>
              <p class="txtVersion">v2.2.0.0</p>
              <?php if ($redirect) { ?>
                <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
            </div>
        </form>
    </div>
</div>
<?php //echo $footer; ?>