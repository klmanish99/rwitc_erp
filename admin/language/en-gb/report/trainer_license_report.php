<?php
// Heading
$_['heading_title']     = 'Trainer License Report';

// Text
$_['text_list']         = 'Trainer License Report';
$_['text_year']         = 'Years';
$_['text_month']        = 'Months';
$_['text_week']         = 'Weeks';
$_['text_day']          = 'Days';
$_['text_all_status']   = 'All Statuses';

// Column
$_['column_date'] = 'Date';
$_['column_horse_name'] = 'Horse Name';
$_['column_Trainer'] = 'Trainer Name';
$_['column_License'] = 'License Type';
$_['column_Amount'] = 'Amount';
$_['column_Charge'] = 'Charge Type';
$_['column_orders']     = 'No. Orders';
$_['column_products']   = 'No. Products';
$_['column_tax']        = 'Tax';
$_['column_total']      = 'Total';

// Entry
$_['entry_date_start']  = 'Date Start';
$_['entry_date_end']    = 'Date End';
$_['entry_group']       = 'Group By';
$_['entry_status']      = 'Order Status';