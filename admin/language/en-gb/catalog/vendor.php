<?php
// Heading
$_['heading_title']      = 'Supplier';

// Text
$_['text_success']       = 'Success: You have modified Supplier!';
$_['text_list']          = 'Supplier List';
$_['text_add']           = 'New Supplier';
$_['text_edit']          = 'Edit Supplier';
$_['text_default']       = 'Default';
$_['text_percent']       = 'Percentage';
$_['text_amount']        = 'Fixed Amount';
$_['text_upload']       = 'Your file was successfully uploaded!';

// Column
$_['column_name']        = 'Supplier Name';
$_['column_sort_order']  = 'Sort Order';
$_['column_action']      = 'Action';

// Entry
$_['entry_name']         = 'Supplier Name';
$_['entry_place']        = 'place';
$_['entry_phone_no']     = 'phone_no';
$_['entry_contact_person']        = 'contact_person';
$_['entry_email_id']     = 'email_id';
$_['entry_items']        = 'items';
$_['entry_store']        = 'Stores';
$_['entry_keyword']      = 'SEO URL';
$_['entry_image']        = 'Image';
$_['entry_sort_order']   = 'Sort Order';
$_['entry_type']         = 'Type';

// Help
$_['help_keyword']       = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify supplier!';
$_['error_name']         = 'Supplier Name must be between 2 and 64 characters!';
$_['error_keyword']      = 'SEO URL already in use!';
$_['error_product']      = 'Warning: This Supplier cannot be deleted as it is currently assigned to %s products!';
$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_exists']      = 'File does not exist!';
$_['error_mask']        = 'Mask must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
