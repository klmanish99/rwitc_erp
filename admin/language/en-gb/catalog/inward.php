<?php
// Heading
$_['heading_title']      = 'Inward';

// Text
$_['text_success']       = 'Success: You have modified Finished Product!';
$_['text_list']          = 'Inward List';
$_['text_add']           = 'New Inward';
$_['text_edit']          = 'Edit Raw Material';
$_['text_default']       = 'Default';
$_['text_percent']       = 'Percentage';
$_['text_amount']        = 'Fixed Amount';

// Column
$_['column_name']        = 'Order No';
$_['column_sort_order']  = 'Sort Order';
$_['column_action']      = 'Edit';

// Entry
//$_['entry_name']         = 'Finished Product Name';
$_['entry_name']         = 'Order No:';
$_['entry_store']        = 'Stores';
$_['entry_keyword']      = 'SEO URL';
$_['entry_image']        = 'Image';
$_['entry_sort_order']   = 'Sort Order';
$_['entry_type']         = 'Type';

// Help
$_['help_keyword']       = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify service!';
$_['error_name']         = 'Select from list!';
$_['error_keyword']      = 'SEO URL already in use!';
$_['error_product']      = 'Warning: This Finished Product cannot be deleted as it is currently assigned to %s products!';
