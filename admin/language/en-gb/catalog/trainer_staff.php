<?php
// Heading
$_['heading_title']          = 'Trainer Staff Entry';

// Text
$_['text_success']           = 'Success: You have modified Trainer Staff Entrys!';
$_['text_list']              = 'Trainer Staff Entry List';
$_['text_add']               = 'Add Trainer Staff Entry';
$_['text_edit']              = 'Edit Trainer Staff Entry';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Trainer Staff Entry Name';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Trainer Staff Entry Name';
$_['entry_trainer_staff_code']			 = 'Trainer Staff Entry Code';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword']     = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_parent']           = 'Parent';
$_['entry_filter']           = 'Filters';
$_['entry_store']            = 'Stores';
$_['entry_image']            = 'Image';
$_['entry_top']              = 'Top';
$_['entry_column']           = 'Columns';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_status']           = 'Medicine Type';
$_['entry_layout']           = 'Layout Override';

$_['tab_general']			 ='Add Trainer Staff Entry';

// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent categories.';
$_['help_column']            = 'Number of columns to use for the bottom 3 categories. Only works for the top parent categories.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Trainer Staff Entry Name must be between 2 and 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
