<?php
// Heading
$_['heading_title']          = 'Race Type';

// Text
$_['text_success']           = 'Success: You have modified race type!';
$_['text_list']              = 'Race Type List';
$_['text_add']               = 'Add Race Type';
$_['text_edit']              = 'Edit Race Type';
$_['text_default']           = 'Default';

// Column
$_['column_racetype']        = 'Race Type';
$_['column_class']     		 = 'Race Class / Race Grade';
$_['column_action']          = 'Action';
$_['column_lower']          = 'Lower Class Eligible';

// Entry
$_['entry_racetype']         = 'Race Type Name';
$_['entry_class']     		 = 'Race Class';
$_['entry_class_eligible'] 	 = 'Lower Class Eligible';
// $_['entry_meta_keyword']     = 'Meta Tag Keywords';
$_['entry_description'] 	 = 'Description';
$_['entry_category']         = 'Category';
// $_['entry_parent']           = 'Parent';
// $_['entry_filter']           = 'Filters';
// $_['entry_store']            = 'Stores';
// $_['entry_image']            = 'Image';
// $_['entry_top']              = 'Top';
// $_['entry_column']           = 'Columns';
// $_['entry_sort_order']       = 'Sort Order';
// $_['entry_status']           = 'Status';
// $_['entry_layout']           = 'Layout Override';

// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent Race Type.';
$_['help_column']            = 'Number of columns to use for the bottom 3 Race Type. Only works for the top parent Race Type.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify Race Type!';
$_['error_name']             = 'Owner Name must be between 2 and 255 characters!';
$_['error_status']           = 'You can not In-active status because it is assinged Horses!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
//$_['error_racetype']          = 'Please Select Race Types';
//$_['error_class']          = 'Please Select Race Class';
//$_['error_grade']          = 'Please Select Race Grade';
