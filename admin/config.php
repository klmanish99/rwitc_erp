<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/rwitc_erp/admin/');
define('HTTP_CATALOG', 'http://localhost/rwitc_erp/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/rwitc_erp/admin/');
define('HTTPS_CATALOG', 'http://localhost/rwitc_erp/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/rwitc_erp/admin/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/rwitc_erp/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/rwitc_erp/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/rwitc_erp/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/rwitc_erp/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/rwitc_erp/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/rwitc_erp/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/rwitc_erp/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/rwitc_erp/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/rwitc_erp/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/rwitc_erp/system/storage/upload/');
define('DIR_CATALOG', 'C:/xampp/htdocs/rwitc_erp/catalog/');

define('APPRENTIES_AMT', '100');
define('NORMAL_AMT', '250');

define('TRAINER_APPRENTIES_AMT', '2000');
define('TRAINER_NORMAL_AMT', '5000');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'admin');
define('DB_DATABASE', 'db_rwitc_erp2');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
