<?php
class ControllerSnippetImportIndent extends Controller {
	private $error = array();

	public function index() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/

		$this->document->setTitle('Import Indent');

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => 'Import Indent',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Import Indent',
			'href' => $this->url->link('snippet/import_indent', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('snippet/indent/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('snippet/indent/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('snippet/indent/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['filter_short_name'])) {
			$filter_short_name = $this->request->get['filter_short_name'];
			$data['filter_short_name'] = $this->request->get['filter_short_name'];
		}
		else{
			$filter_short_name = '';
			$data['filter_short_name'] = '';
		}

		if (isset($this->request->get['filter_medicine_id'])) {
			$filter_medicine_id = $this->request->get['filter_medicine_id'];
			$data['filter_medicine_id'] = $this->request->get['filter_medicine_id'];
		}
		else{
			$filter_medicine_id = '';
			$data['filter_medicine_id'] = '';
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = 'Active';
			$data['filter_status'] = 'Active';
		}

		if (isset($this->request->post['indent_name'])) {
			$data['indent_name'] = $this->request->post['indent_name'];
		} elseif (!empty($category_info)) {
			$data['indent_name'] = $category_info['indent_name'];
		} else {
			$data['indent_name'] = '';
		}

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_short_name'	=>	$filter_short_name,
			'filter_medicine_id'	=>	$filter_medicine_id,
			'filter_status'	=>	$filter_status
		);

		$data['status'] =array(
				'Active'  =>"Active",
				'In-Active'  =>'In-Active'
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('snippet/indent', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('snippet/indent', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = '';
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('snippet/indent', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('snippet/import_indent', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'snippet/indent')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if($this->request->post['short_name'] == ''){
			$this->error['valierr_short_name'] = 'Please Select Short Name';
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	public function autocompleteIndent() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$json = array();

		if (isset($this->request->get['indent_no'])) {

			$results = $this->db->query("SELECT * FROM oc_rawmaterialreq WHERE order_no = '".$this->request->get['indent_no']."' ")->rows;
			//echo '<pre>';print_r("SELECT * FROM oc_rawmaterialreq WHERE order_no = '".$this->request->get['indent_no']."' ");exit;
			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'order_no' => $result['order_no'],
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['order_no'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function import_po() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$inwards = $this->db->query("SELECT * FROM oc_rawmaterialreqitem WHERE order_no = '".$this->request->get['indent_no']."' ")->rows;
		if($inwards){
			foreach ($inwards as $result) {
				$inw_datas = $this->db->query("SELECT * FROM oc_tally_po WHERE 1=1 ORDER BY tally_po_id DESC LIMIT 1 ");
				if ($inw_datas->num_rows > 0) {
					$voucher_nos = $inw_datas->row['voucher_no'];
					$in_idss = str_replace('MOV-POM-EQU/', '', $voucher_nos);
					$in_ids = str_replace('/18-19', '', $in_idss);
					$in_id = $in_ids + 1;
				} else {
					$voucher_nos = '';
				}
				if ($voucher_nos != '') {
					$inc_no = $in_id;
				} else {
					$inc_no = 1;
				}
				$voucher_no = 'MOV-POM-EQU/'.$inc_no.'/18-19';
				$po_no = 'MOV-POM-EQU/'.$inc_no.'/18-19';
				$current_date = date("Y-m-d");
				$user_id = $this->user->getId();
				$this->db->query("INSERT INTO `oc_tally_po` SET voucher_no = '".$voucher_no."',
					voucher_type = 'PO-MAT-MOV-EQUINE',
					date_of_posting = '2018-06-07',
					po_no  = '".$po_no."',
					pi_no = 'MOV-INM-EQU/28/18-19',
					pi_date = '2018-06-07',
					supplier_po = '',
					indent_no = 'MOV-INM-EQU/28/18-19',
					cln_supplier_name = 'RUDRA ENTERPRISES',
					clc_supplier_ledger_code = 'GZR8110',
					debit_ledger_name = 'COMMON  PURCHASE LEDGER',
					debit_ledger_code = 'Z1785',
					name_of_the_items = '".$result['productraw_name']."',
					qty = '".$result['quantity']."',
					uom = '".$result['auto_unit']."',
					rate = '".$result['purchase_price']."',
					amount = '".$result['purchase_value']."',
					godown_name = 'STORES PUNE',
					narration = '',
					locations = 'Pune',
					divisions = 'Veterinary-Equine Hospital',
					input_sgst_not_eligible_for_itc = '5.0',
					input_cgst_not_eligible_for_itc = '5.0',
					input_igst_not_eligible_for_itc = '5.0',
					total = '".$result['total']."',
					date_added = '".$current_date."',
					user_id = '".$user_id."'
					");
			}
		}
		
		$this->response->redirect($this->url->link('snippet/import_indent', 'token=' . $this->session->data['token']));
	}

}
