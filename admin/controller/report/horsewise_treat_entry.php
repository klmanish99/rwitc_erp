<?php

class ControllerReportHorseWiseTreatEntry extends Controller {
	private $error = array();

	public function index() {
		$this->load->model('report/horsewise_treat_entry');

		$this->document->setTitle('Horse Treatment');

		$this->getList();
	}

	protected function getList() {

		if (isset($this->request->get['filter_horse'])) {
			$filter_horse = $this->request->get['filter_horse'];
		} else {
			$filter_horse = null;
		}

		if (isset($this->request->get['filter_trainer'])) {
			$filter_trainer = $this->request->get['filter_trainer'];
		} else {
			$filter_trainer = null;
		}

		if (isset($this->request->get['filter_medicine'])) {
			$filter_medicine = $this->request->get['filter_medicine'];
		} else {
			$filter_medicine = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['filter_dates'])) {
			$filter_dates = $this->request->get['filter_dates'];
		} else {
			$filter_dates = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_horse'])) {
			$url .= '&filter_horse=' . urlencode(html_entity_decode($this->request->get['filter_horse'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . urlencode(html_entity_decode($this->request->get['filter_trainer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['horsetreatments'] = array();

		$filter_data = array(
			'filter_horse'	  => $filter_horse,
			'filter_trainer'	  => $filter_trainer,
			'filter_medicine'	  => $filter_medicine,
			'filter_date'	  => $filter_date,
			'filter_dates'	  => $filter_dates,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
			//echo '<pre>';
			 //print_r($filter_data);
			//exit;

			$results = $this->model_report_horsewise_treat_entry->getHorses($filter_data);

			if ($results) {
				foreach ($results as $result) {

					$med_types = $this->db->query("SELECT med_type FROM medicine WHERE med_name LIKE '".$result['medicine_name']."%' ");
					if ($med_types->num_rows > 0) {
						$med_type = $med_types->row['med_type'];
					} else {
						$med_type = '';
					}

					$data['horsetreatments'][] = array(
						'entry_date' => date('d-m-Y', strtotime($result['entry_date'])),
						'horse_name' => $result['horse_name'],
						'trainer_name' => $result['trainer_name'],
						'medicine_qty' => $result['medicine_qty'],
						'unit' => $result['unit'],
						'medicine_name' => $result['medicine_name'],
						'med_type' => $med_type,
					);
				}
			}

		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_horse'])) {
			$url .= '&filter_horse=' . urlencode(html_entity_decode($this->request->get['filter_horse'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . urlencode(html_entity_decode($this->request->get['filter_trainer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . '&sort=inward_name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_horse'])) {
			$url .= '&filter_horse=' . urlencode(html_entity_decode($this->request->get['filter_horse'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . urlencode(html_entity_decode($this->request->get['filter_trainer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['filter_horse'] = $filter_horse;
		$data['filter_trainer'] = $filter_trainer;
		$data['filter_medicine'] = $filter_medicine;
		$data['filter_date'] = $filter_date;
		$data['filter_dates'] = $filter_dates;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];

		$this->response->setOutput($this->load->view('report/horsewise_treat_entry', $data));
	}

	public function prints() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$data['horsetreatments'] = array();
		$data['horse_name'] = '';
		$data['trainer_name'] = '';

		if (isset($this->request->get['filter_horse'])) {
			$filter_horse = $this->request->get['filter_horse'];
		} else {
			$filter_horse = '';
		}

		if (isset($this->request->get['filter_trainer'])) {
			$filter_trainer = $this->request->get['filter_trainer'];
		} else {
			$filter_trainer = '';
		}

		if (isset($this->request->get['filter_medicine'])) {
			$filter_medicine = $this->request->get['filter_medicine'];
		} else {
			$filter_medicine = '';
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['filter_dates'])) {
			$filter_dates = $this->request->get['filter_dates'];
		} else {
			$filter_dates = null;
		}


		$sql = "SELECT * FROM oc_treatment_entry_trans tt LEFT JOIN oc_treatment_entry t ON (tt.parent_id = t.id)";

		$sql .= " WHERE 1=1";

		if ($filter_horse != '') {
			$sql .= " AND horse_name LIKE '%" . $this->db->escape($filter_horse) . "%'";
		}

		if ($filter_trainer != '') {
			$sql .= " AND trainer_name LIKE '%" . $this->db->escape($filter_trainer) . "%'";
		}

		if ($filter_medicine != '') {
			$sql .= " AND medicine_name LIKE '%" . $this->db->escape($filter_medicine) . "%'";
		}

		if ($filter_date != '' && $filter_dates != '') {
			$sql .= " AND entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($filter_date))) . "'";
			$sql .= " AND entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($filter_dates))) . "'";
		}

		$treat_datas = $this->db->query($sql)->rows;

		if ($treat_datas) {
			foreach ($treat_datas as $pvalue) {

				$med_types = $this->db->query("SELECT med_type FROM medicine WHERE med_name LIKE '".$pvalue['medicine_name']."%' ");
				if ($med_types->num_rows > 0) {
					$med_type = $med_types->row['med_type'];
				} else {
					$med_type = '';
				}
				
				$data['horsetreatments'][] = array(
					'trainer_name' => $pvalue['trainer_name'],
					'horse_name' => $pvalue['horse_name'],
					'medicine_name' => $pvalue['medicine_name'],
					'med_type' => $med_type,
					'medicine_qty' => $pvalue['medicine_qty'],
					'unit' => $pvalue['unit'],
					'entry_date' => date('d-m-Y', strtotime($pvalue['entry_date'])),
				);

				$data['filter_date'] = $filter_date;
				$data['filter_dates'] = $filter_dates;
			}
			
			$html = $this->load->view('report/horsewise_treat_entry_html', $data);
			//echo $html;exit;
			$filename = 'Horsewise Report.xls';
			//file_put_contents(DIR_DOWNLOAD.Indent, $html);
			header('Content-disposition: attachment; filename=' . $filename);
			header('Content-type: text/html');
			echo $html;exit;
		} else {
			$this->response->redirect($this->url->link('report/horsewise_treat_entry', 'token=' . $this->session->data['token'] , true));
		}
	}

	public function autocompleteHorse() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['trainer_name'])) {
			$this->load->model('report/horsewise_treat_entry');

			$results = $this->model_report_horsewise_treat_entry->getHorsesAuto($this->request->get['trainer_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'trainer_id' => $result['horseseq'],
						'trainer_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteTrainer() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['trainer_name'])) {
			$this->load->model('report/horsewise_treat_entry');

			$results = $this->model_report_horsewise_treat_entry->getTrainersAuto($this->request->get['trainer_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'trainer_code' => $result['trainer_code'],
						'trainer_id' => $result['id'],
						'trainer_name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		
		if (isset($this->request->get['filter_medicine'])) {
			
			$sql = "SELECT * FROM `medicine` WHERE 1=1 ";

			if (!empty($this->request->get['filter_medicine'])) {
				$sql .= " AND med_name LIKE '%" . $this->db->escape($this->request->get['filter_medicine']) . "%'";
			}

			$results = $this->db->query($sql)->rows;

			foreach ($results as $result) {
				$json[] = array(
					'med_name' => $result['med_name'],
				);
			}
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
