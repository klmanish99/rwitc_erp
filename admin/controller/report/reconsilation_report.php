<?php
class ControllerreportReconsilationReport extends Controller {
	private $error = array();
	public function index() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

	/*	echo '<pre>';
			print_r($this->error);
			exit*/
		//$this->load->language('report/reconsilation_report');

		$this->document->setTitle('Reconsilation');

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = '';
		}


		if (isset($this->request->get['filter_doctor_id'])) {
			$filter_doctor_id = $this->request->get['filter_doctor_id'];
		} else {
			$filter_doctor_id = '';
		}

		if (isset($this->request->get['filter_doctor_name'])) {
			$filter_doctor_name = $this->request->get['filter_doctor_name'];
		} else {
			$filter_doctor_name = '';
		}



		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date']) ) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_doctor_id']) ) {
			$url .= '&filter_doctor_id=' . $this->request->get['filter_doctor_id'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/reconsilation_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('report/reconsilation_report');
		$data['action'] = $this->url->link('report/reconsilation_report/add', 'token=' . $this->session->data['token'] , true);

		$data['back_monthly_daily_reconsilation'] = $this->url->link('report/monthly_medicine_transaction', 'token=' . $this->session->data['token'] , true);

		$filter_data = array(
			'filter_date'	     => $filter_date,
			'filter_doctor_id'	     => $filter_doctor_id,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin'),
		);

		$order_total = 0;
		$data['results'] = 0;

		//echo "<pre>";print_r($filter_data);exit;

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_date'] = $this->language->get('column_date');
		$data['column_Trainer'] = $this->language->get('column_Trainer');
		$data['column_Amount'] = $this->language->get('column_Amount');
		$data['column_Charge'] = $this->language->get('column_Charge');
		$data['column_horse_name'] = $this->language->get('column_horse_name');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_group'] = $this->language->get('entry_group');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['error_medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['error_medicine_error'] = '';
		}

		if (isset($this->error['filter_date'])) {
			$data['filter_date'] = $this->error['filter_date'];
		} else {
			$data['filter_date'] = '';
		}

		if (isset($this->error['filter_doctor_id_error'])) {
			$data['filter_doctor_id_error'] = $this->error['filter_doctor_id_error'];
		} else {
			$data['filter_doctor_id_error'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['medicine_error'] = '';
		}

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['reconcilation_datas'] = array();
		$order_total = $this->model_report_reconsilation_report->getTotalReconcilation($filter_data);
		$data['reconcilation_datas']= $this->model_report_reconsilation_report->getReconcilation($filter_data);

		//echo '<pre>';print_r($order_total);exit;

		//echo "<pre>";print_r($data['med_transaction']);exit;

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/reconsilation_report', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		if($filter_date != ''){
			$data['filter_date'] = date('d-m-Y', strtotime($filter_date));
		} else {
			$data['filter_date'] = '';
		}

		
		$data['filter_doctor_name'] = $filter_doctor_name;
		$data['filter_doctor_id'] = $filter_doctor_id;

		$this->response->setOutput($this->load->view('report/reconsilation', $data));
	}

	public function add() {
		$this->load->language('report/reconsilation_report');
		/*$this->load->model('catalog/product');*/
		/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('report/reconsilation_report');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		/*	echo '<pre>';
			print_r($this->request->post);
			exit;*/
			$this->model_report_medicine_transaction->addMedData($this->request->post);

			$this->session->data['success'] = 'Success: You have added Entry!';

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			//$this->response->redirect($this->url->link('report/reconsilation_report', 'token=' . $this->session->data['token'] . $url, true));
		}
		//$this->response->redirect($this->url->link('report/reconsilation_report', 'token=' . $this->session->data['token'] . $url, true));
		$this->index();
	}
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'report/reconsilation_report')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		/*echo '<pre>';
		print_r($this->request->post);
		exit;*/
		

		if ((utf8_strlen($this->request->post['filter_date']) == '')) {
			$this->error['filter_date'] = "Please Select date";
		}

		if ((utf8_strlen($this->request->post['filter_doctor_id']) == '')) {
			$this->error['filter_doctor_id_error'] = "Please Select Doctor For Entry";
		}


		if(!isset($this->request->post['productraw_datas'])){
			$this->error['medicine_error'] = 'Please Enter Atleast Entry';
		} 
		$datas = $this->request->post;
		return !$this->error;
	}

	public function autocompletedoc() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_doctor_name'])) {
			$this->load->model('report/reconsilation_report');

			$results = $this->model_report_medicine_transaction->getDoctorAuto($this->request->get['filter_doctor_name']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name'     => strip_tags(html_entity_decode($result['doctor_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_name'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}
}