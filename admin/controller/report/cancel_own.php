<?php

class Controllerreportcancelown extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('report/cancel_own');
		$this->load->model('report/cancel_own');
		$this->document->setTitle('Cancel Ownership Report');

		$this->getList();
	}

	protected function getList() {
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		if (isset($this->request->get['filter_inward'])) {
			$filter_inward = $this->request->get['filter_inward'];
		} else {
			$filter_inward = null;
		}

		if (isset($this->request->get['filter_order_no'])) {
			$filter_order_no = $this->request->get['filter_order_no'];
		} else {
			$filter_order_no = null;
		}

		if (isset($this->request->get['filter_medicine'])) {
			$filter_medicine = $this->request->get['filter_medicine'];
		} else {
			$filter_medicine = null;
		}

		if (isset($this->request->get['filter_po_no'])) {
			$filter_po_no = $this->request->get['filter_po_no'];
		} else {
			$filter_po_no = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['filter_dates'])) {
			$filter_dates = $this->request->get['filter_dates'];
		} else {
			$filter_dates = null;
		}

		if (isset($this->request->get['filter_productsort'])) {
			$filter_productsort = $this->request->get['filter_productsort'];
		} else {
			$filter_productsort = null;
		}

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$filter_inward_category = $this->request->get['filter_inward_category'];
		} else {
			$filter_inward_category = null;
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$filter_inward_category_id = $this->request->get['filter_inward_category_id'];
		} else {
			$filter_inward_category_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_po_no'])) {
			$url .= '&filter_po_no=' . urlencode(html_entity_decode($this->request->get['filter_po_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}


		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/inward/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['stock'] = $this->url->link('catalog/inward/stock', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/inward/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['inwards'] = array();

		$filter_data = array(
			'filter_inward'	  => $filter_inward,
			'filter_order_no'	  => $filter_order_no,
			'filter_medicine'	  => $filter_medicine,
			'filter_po_no'	  => $filter_po_no,
			'filter_date'	  => $filter_date,
			'filter_dates'	  => $filter_dates,
			'filter_order_id'  => $filter_order_id,
			'filter_inward_category'	  => $filter_inward_category,
			'filter_inward_category_id'  => $filter_inward_category_id,
			'filter_productsort'	=> $filter_productsort,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
			//echo '<pre>';
			 //print_r($filter_data);
			//exit;

		$inward_total = $this->model_report_cancel_own->getTotalinwards($filter_data);

		$results = $this->model_report_cancel_own->getinwards($filter_data);
		$user_group_id = $this->user->getGroupId();

		// echo "<pre>";
		// print_r($results);
		// exit;

		foreach ($results as $result) {

			$horse_name = $this->db->query("SELECT official_name FROM horse1 WHERE horseseq = '".$result['horse_id']."' ")->row;

			if ($result['trainer_id'] != 0) {
				$trainer_name = $this->db->query("SELECT name FROM trainers WHERE id = '".$result['trainer_id']."' ")->row;
			} else{
				$trainer_name = '';
			}

			$data['cancel_leases'][] = array(
				'horse_name'=> $horse_name['official_name'],
				'trainer_name'=> $trainer_name['name'],
				'to_owner'=> $result['to_owner'],
				'ownership_type'=> $result['ownership_type'],
				'cancel_lease_status'=> $result['cancel_lease_status'],
				// 'gst_rate' => $result['gst_rate'].'%',
				// 'gst_value' => $result['gst_value'],
				// 'indent'=> $indent,
				// 'date'=> date('d-m-Y', strtotime($result['date'])),
			);
		}

		$data['productsorts'] = array(
			'a'=>'A','b'=>'B','c'=>'C','d'=>'D','e'=>'E','f'=>'F','g'=>'G','h'=>'H',
			'i'=>'I','j'=>'J','k'=>'K','l'=>'L','m'=>'M','n'=>'N','o'=>'O','p'=>'P',
			'q'=>'Q','r'=>'R','s'=>'S','t'=>'T','u'=>'U','v'=>'V','w'=>'W','x'=>'X',
			'y'=>'Y','z'=>'Z','Other'
		);
			// echo '<pre>';
			// print_r($data['productsorts']);
			// exit;
		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token']);
		$data['user_group_id'] = $this->user->getGroupId();
		//$data['user_type'] = $this->user->getUserType();	
		$data['base_link_1'] = $this->url->link('catalog/inward/edit', 'token=' . $this->session->data['token']);
		$data['base_link'] = $this->url->link('catalog/inward/edit', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_po_no'])) {
			$url .= '&filter_po_no=' . urlencode(html_entity_decode($this->request->get['filter_po_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . '&sort=inward_name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_po_no'])) {
			$url .= '&filter_po_no=' . urlencode(html_entity_decode($this->request->get['filter_po_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $inward_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($inward_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($inward_total - $this->config->get('config_limit_admin'))) ? $inward_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $inward_total, ceil($inward_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_inward_category'] = $filter_inward_category;
		$data['filter_inward_category_id'] = $filter_inward_category_id;
		$data['filter_inward'] = $filter_inward;
		$data['filter_order_no'] = $filter_order_no;
		$data['filter_medicine'] = $filter_medicine;
		$data['filter_po_no'] = $filter_po_no;
		$data['filter_date'] = $filter_date;
		$data['filter_dates'] = $filter_dates;
		$data['filter_productsort'] = $filter_productsort;
		$data['filter_order_id'] = $filter_order_id;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];
		
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		$this->response->setOutput($this->load->view('report/cancel_own', $data));
	}

	public function autocomplete() {
		$json = array();
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		
		if (isset($this->request->get['filter_medicine'])) {
			
			$sql = "SELECT * FROM `medicine` WHERE 1=1 ";

			if (!empty($this->request->get['filter_medicine'])) {
				$sql .= " AND med_name LIKE '%" . $this->db->escape($this->request->get['filter_medicine']) . "%'";
			}

			$results = $this->db->query($sql)->rows;

			foreach ($results as $result) {
				$json[] = array(
					'med_name' => $result['med_name'],
				);
			}
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletePo() {
		$json = array();
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		
		if (isset($this->request->get['filter_po_no'])) {
			
			$sql = "SELECT * FROM `oc_inwarditem` WHERE 1=1 ";

			if (!empty($this->request->get['filter_po_no'])) {
				$sql .= " AND po_no LIKE '%" . $this->db->escape($this->request->get['filter_po_no']) . "%'";
			}

			$results = $this->db->query($sql)->rows;

			foreach ($results as $result) {
				$json[] = array(
					'po_no' => $result['po_no'],
				);
			}
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function prints() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$data['inwards'] = array();
		if (isset($this->request->get['filter_medicine'])) {
			$medicine_name = $this->request->get['filter_medicine'];
		} else {
			$medicine_name = '';
		}
		if (isset($this->request->get['filter_po_no'])) {
			$po_no = $this->request->get['filter_po_no'];
		} else {
			$po_no = '';
		}
		if (isset($this->request->get['filter_date'])) {
			$from_date = $this->request->get['filter_date'];
		} else {
			$from_date = '';
		}
		if (isset($this->request->get['filter_dates'])) {
			$to_date = $this->request->get['filter_dates'];
		} else {
			$to_date = '';
		}

		$sql = "SELECT * FROM oc_inward i LEFT JOIN oc_inwarditem ii ON (i.order_no = ii.order_no) WHERE 1=1 ";

		if (!empty($medicine_name)) {
			$sql .= " AND productraw_name LIKE '%" . $this->db->escape($medicine_name) . "%'";
		}

		if (!empty($medicine_name)) {
			$sql .= " AND po_no LIKE '%" . $this->db->escape($po_no) . "%'";
		}

		if ($from_date != '' && $to_date != '') {
			$sql .= " AND date >= '" . $this->db->escape(date('Y-m-d', strtotime($from_date))) . "'";
			$sql .= " AND date <= '" . $this->db->escape(date('Y-m-d', strtotime($to_date))) . "'";
		}

		// echo'<pre>';
		// print_r($sql);
		// exit;

		$inward = $this->db->query($sql)->rows;

		foreach ($inward as $pvalue) {

			$indents = $this->db->query("SELECT * FROM oc_tally_po WHERE po_no = '".$pvalue['po_no']."' ");
			if ($indents->num_rows > 0) {
				$indent = $indents->row['indent_no'];
			} else {
				$indent = '';
			}
			
			$data['inwards'][] = array(
				'inward_code'=> $pvalue['order_no'],
				'date'=> date('d-m-Y', strtotime($pvalue['date'])),
				'po_no'=> $pvalue['po_no'],
				'productraw_name'=> $pvalue['productraw_name'],
				'po_qty'=> $pvalue['po_qty'],
				'qty'=> $pvalue['quantity'],
				'gst_rate' => $pvalue['gst_rate'].'%',
				'gst_value' => $pvalue['gst_value'],
				'indent'=> $indent,
			);

			$data['from_date'] = $from_date;
			$data['to_date'] = $to_date;
		}

		// echo'<pre>';
		// print_r($data['inwards']);
		// exit;

		$html = $this->load->view('report/inward_html', $data);
		//echo $html;exit;
		$filename = 'Inward.html';
		//file_put_contents(DIR_DOWNLOAD.Indent, $html);
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;
		$this->response->setOutput($this->load->view('catalog/inward_form', $data));
	}
}
