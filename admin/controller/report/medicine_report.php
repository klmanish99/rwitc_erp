<?php

class Controllerreportmedicinereport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('report/medicine_report');
		$this->load->model('report/medicine_report');
		$this->document->setTitle('Medicine Dump Report');

		$this->getList();
	}

	protected function getList() {
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		if (isset($this->request->get['filter_inward'])) {
			$filter_inward = $this->request->get['filter_inward'];
		} else {
			$filter_inward = null;
		}

		if (isset($this->request->get['filter_order_no'])) {
			$filter_order_no = $this->request->get['filter_order_no'];
		} else {
			$filter_order_no = null;
		}

		if (isset($this->request->get['filter_medicine'])) {
			$filter_medicine = $this->request->get['filter_medicine'];
		} else {
			$filter_medicine = null;
		}

		if (isset($this->request->get['filter_po_no'])) {
			$filter_po_no = $this->request->get['filter_po_no'];
		} else {
			$filter_po_no = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = '';
		}

		if (isset($this->request->get['filter_dates'])) {
			$filter_dates = $this->request->get['filter_dates'];
		} else {
			$filter_dates = '';
		}

		if (isset($this->request->get['filter_productsort'])) {
			$filter_productsort = $this->request->get['filter_productsort'];
		} else {
			$filter_productsort = null;
		}

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$filter_inward_category = $this->request->get['filter_inward_category'];
		} else {
			$filter_inward_category = null;
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$filter_inward_category_id = $this->request->get['filter_inward_category_id'];
		} else {
			$filter_inward_category_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_po_no'])) {
			$url .= '&filter_po_no=' . urlencode(html_entity_decode($this->request->get['filter_po_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}


		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/indent', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/indent/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['stock'] = $this->url->link('catalog/indent/stock', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/indent/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['inwards'] = array();
		$data['transfers'] = array();
		$data['medtransfer'] = array();
		$data['medicine_trans_datas'] = array();

		// if ($filter_date == '') {
		// 	$date_filter = date('d-m-Y');
		// } else{
		// 	$date_filter = $filter_date;
		// }
		$filter_data = array(
			'filter_inward'	  => $filter_inward,
			'filter_order_no'	  => $filter_order_no,
			'filter_medicine'	  => $filter_medicine,
			'filter_po_no'	  => $filter_po_no,
			'filter_date'	  => $filter_date,
			'filter_dates'	  => $filter_dates,
			'filter_order_id'  => $filter_order_id,
			'filter_inward_category'	  => $filter_inward_category,
			'filter_inward_category_id'  => $filter_inward_category_id,
			'filter_productsort'	=> $filter_productsort,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
			//echo '<pre>';
			 //print_r($filter_data);
			//exit;

		//$inward_total = $this->model_report_medicine_report->getTotalindents($filter_data);

		$results = $this->model_report_medicine_report->getindents($filter_data);
		$user_group_id = $this->user->getGroupId();

		if (isset($filter_data) && ($filter_medicine != '')) {
			foreach ($results as $result) {

				$med_types = $this->db->query("SELECT med_type,unit FROM medicine WHERE med_name LIKE '".$result['productraw_name']."%' ");
				if ($med_types->num_rows > 0) {
					$unit = $med_types->row['unit'];
				} else {
					$unit = '';
				}

				$data['inwards'][] = array(
					'date'=> date('d-m-Y', strtotime($result['date'])),
					'productraw_name'=> $result['productraw_name'],
					'supplier'=> $result['supplier'],
					'quantity'=> $result['quantity'],
					'value'=> $result['value'],
					'order_no'=> $result['order_no'],
					'ex_date'=>  date('d-m-Y', strtotime($result['ex_date'])),
					'batch_no'=> $result['batch_no'],
					'unit'=> $unit
				);
			}//exit;
			$grn_totalss = "SELECT SUM(value) AS tot_val, SUM(quantity) AS tot_qty FROM `oc_inwarditem` WHERE 1=1 ";
			if ($filter_date && $filter_dates) {
				$grn_totalss .= " AND date_added >= '" . $this->db->escape(date('Y-m-d', strtotime($filter_date))) . "'";
				$grn_totalss .= " AND date_added <= '" . $this->db->escape(date('Y-m-d', strtotime($filter_dates))) . "'";
			}

			if ($filter_medicine) {
				$grn_totalss .= " AND productraw_name LIKE '%" . $this->db->escape($filter_medicine) . "%' ";
			}
			$grn_total = $this->db->query($grn_totalss)->row;

			$data['grn_qty_total'] = $grn_total['tot_qty'];
			$data['total'] = $grn_total['tot_val'];
		


		$transfer = $this->model_report_medicine_report->gettransfer($filter_data);
		$user_group_id = $this->user->getGroupId();

		foreach ($transfer as $result) {
			$doctors = $this->db->query("SELECT doctor_name FROM doctor WHERE id = '".$result['child_doctor_id']."' ");
			if ($doctors->num_rows > 0) {
				$doctor = $doctors->row['doctor_name'];
			} else {
				$doctor = '';
			}

			$med_types = $this->db->query("SELECT med_type,unit FROM medicine WHERE med_name LIKE '".$result['product_name']."%' ");
			if ($med_types->num_rows > 0) {
				$unit = $med_types->row['unit'];
			} else {
				$unit = '';
			}

			$data['medtransfer'][] = array(
				'issue_no'=> $result['issue_no'],
				'entry_date'=> date('d-m-Y', strtotime($result['entry_date'])),
				'doctor_name'=> $doctor,
				'product_name'=> $result['product_name'],
				'expire_date'=> $result['expire_date'],
				'product_qty'=> $result['product_qty'],
				'parent_doctor_name'=> $result['parent_doctor_name'],
				'unit'=> $unit
			);
		}

		$trans_totalsss = "SELECT SUM(mi.product_qty) AS product_qtys FROM medicine_transfer m LEFT JOIN medicine_trans_items mi ON (m.id = mi.parent_id) WHERE 1=1 ";
		if ($filter_date && $filter_dates) {
			$trans_totalsss .= " AND m.entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($filter_date))) . "'";
			$trans_totalsss .= " AND m.entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($filter_dates))) . "'";
		}

		if ($filter_medicine) {
			$trans_totalsss .= " AND mi.product_name LIKE '%" . $this->db->escape($filter_medicine) . "%' ";
		}
		$trans_total = $this->db->query($trans_totalsss)->row;


		$data['qty_total'] = $trans_total['product_qtys'];


		$medi_datas = array();
		$medicine_trans_datas = $this->model_report_medicine_report->gettreatment($filter_data);


		foreach ($medicine_trans_datas as $key => $value) {
			$doc_datas = $this->db->query("SELECT * FROM doctor WHERE id = '".$value['doctor_id']."'");
			if ($doc_datas->num_rows > 0) {
				$doctor_name = $doc_datas->row['doctor_name'];
			} else {
				$doctor_name = '';
			}

			$med_types = $this->db->query("SELECT med_type,unit FROM medicine WHERE med_name LIKE '".$result['product_name']."%' ");
			if ($med_types->num_rows > 0) {
				$unit = $med_types->row['unit'];
			} else {
				$unit = '';
			}

			$medi_datas[] = array(
				'id' => $value['id'],
				'issue_no' => $value['issue_no'],
				'entry_date' => $value['entry_date'],
				'clinic_name' => $value['clinic_name'],
				'horse_name' => $value['horse_name'],
				'trainer_name' => $value['trainer_name'],
				'doctor_name' => $doctor_name,
				'total_item' => $value['total_item'],
				'total_qty' => $value['total_qty'],
				'total_amt' => $value['total_amt'],
				'unit' => $unit,
			);
		}
		$data['medicine_trans_datas'] = $medi_datas;

		$treat_totalsss = "SELECT SUM(t.total_qty) AS total_qtys FROM oc_treatment_entry t LEFT JOIN oc_treatment_entry_trans tt ON (t.id = tt.parent_id) WHERE 1=1 ";
		if ($filter_date && $filter_dates) {
			$treat_totalsss .= " AND t.entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($filter_date))) . "'";
			$treat_totalsss .= " AND t.entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($filter_dates))) . "'";
		}

		if ($filter_medicine) {
			$treat_totalsss .= " AND tt.medicine_name LIKE '%" . $this->db->escape($filter_medicine) . "%' ";
		}
		$treat_total = $this->db->query($treat_totalsss)->row;


		$data['iss_qty_total'] = $treat_total['total_qtys'];
		}

		$data['filter_date'] = $filter_date;


		$data['productsorts'] = array(
			'a'=>'A','b'=>'B','c'=>'C','d'=>'D','e'=>'E','f'=>'F','g'=>'G','h'=>'H',
			'i'=>'I','j'=>'J','k'=>'K','l'=>'L','m'=>'M','n'=>'N','o'=>'O','p'=>'P',
			'q'=>'Q','r'=>'R','s'=>'S','t'=>'T','u'=>'U','v'=>'V','w'=>'W','x'=>'X',
			'y'=>'Y','z'=>'Z','Other'
		);
		$data['current'] = date('d-m-Y');
			// echo '<pre>';
			// print_r($data['productsorts']);
			// exit;
		$data['user_group_id'] = $this->user->getGroupId();
		//$data['user_type'] = $this->user->getUserType();	
		$data['base_link_1'] = $this->url->link('catalog/indent/edit', 'token=' . $this->session->data['token']);
		$data['base_link'] = $this->url->link('catalog/indent/edit', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_po_no'])) {
			$url .= '&filter_po_no=' . urlencode(html_entity_decode($this->request->get['filter_po_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/indent', 'token=' . $this->session->data['token'] . '&sort=inward_name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/indent', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_po_no'])) {
			$url .= '&filter_po_no=' . urlencode(html_entity_decode($this->request->get['filter_po_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		// $pagination = new Pagination();
		// $pagination->total = $inward_total;
		// $pagination->page = $page;
		// $pagination->limit = $this->config->get('config_limit_admin');
		// $pagination->url = $this->url->link('catalog/indent', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		// $data['pagination'] = $pagination->render();

		// $data['results'] = sprintf($this->language->get('text_pagination'), ($inward_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($inward_total - $this->config->get('config_limit_admin'))) ? $inward_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $inward_total, ceil($inward_total / $this->config->get('config_limit_admin')));

		// $pagination = new Pagination();
		// $pagination->total = $transfer_total;
		// $pagination->page = $page;
		// $pagination->limit = $this->config->get('config_limit_admin');
		// $pagination->url = $this->url->link('catalog/indent', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		// $data['pagination1'] = $pagination->render();

		// $data['results1'] = sprintf($this->language->get('text_pagination'), ($transfer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($transfer_total - $this->config->get('config_limit_admin'))) ? $transfer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $transfer_total, ceil($transfer_total / $this->config->get('config_limit_admin')));

		// $pagination = new Pagination();
		// $pagination->total = $treatment_total;
		// $pagination->page = $page;
		// $pagination->limit = $this->config->get('config_limit_admin');
		// $pagination->url = $this->url->link('catalog/indent', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		// $data['pagination2'] = $pagination->render();

		// $data['results2'] = sprintf($this->language->get('text_pagination'), ($treatment_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($treatment_total - $this->config->get('config_limit_admin'))) ? $treatment_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $treatment_total, ceil($treatment_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_inward_category'] = $filter_inward_category;
		$data['filter_inward_category_id'] = $filter_inward_category_id;
		$data['filter_inward'] = $filter_inward;
		$data['filter_order_no'] = $filter_order_no;
		$data['filter_medicine'] = $filter_medicine;
		$data['filter_po_no'] = $filter_po_no;
		$data['filter_date'] = $filter_date;
		$data['filter_dates'] = $filter_dates;
		$data['filter_productsort'] = $filter_productsort;
		$data['filter_order_id'] = $filter_order_id;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];
		
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		$this->response->setOutput($this->load->view('report/medicine_report', $data));
	}

	public function autocomplete() {
		$json = array();
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		
		if (isset($this->request->get['filter_medicine'])) {
			
			$sql = "SELECT * FROM `medicine` WHERE 1=1 ";

			if (!empty($this->request->get['filter_medicine'])) {
				$sql .= " AND med_name LIKE '%" . $this->db->escape($this->request->get['filter_medicine']) . "%'";
			}

			$results = $this->db->query($sql)->rows;

			foreach ($results as $result) {
				$json[] = array(
					'med_name' => $result['med_name'],
				);
			}
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletePo() {
		$json = array();
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		
		if (isset($this->request->get['filter_po_no'])) {
			
			$sql = "SELECT * FROM `oc_inwarditem` WHERE 1=1 ";

			if (!empty($this->request->get['filter_po_no'])) {
				$sql .= " AND po_no LIKE '%" . $this->db->escape($this->request->get['filter_po_no']) . "%'";
			}

			$results = $this->db->query($sql)->rows;

			foreach ($results as $result) {
				$json[] = array(
					'po_no' => $result['po_no'],
				);
			}
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function prints() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$data['inwards'] = array();
		$data['transfers'] = array();
		$data['medtransfer'] = array();
		$data['medicine_trans_datas'] = array();

		if (isset($this->request->get['filter_medicine'])) {
			$medicine_name = $this->request->get['filter_medicine'];
		} else {
			$medicine_name = '';
		}
		if (isset($this->request->get['filter_date'])) {
			$from_date = $this->request->get['filter_date'];
		} else {
			$from_date = '';
		}
		if (isset($this->request->get['filter_dates'])) {
			$to_date = $this->request->get['filter_dates'];
		} else {
			$to_date = '';
		}

		if (isset($this->request->get['note'])) {
			$note = $this->request->get['note'];
		} else {
			$note = '';
		}

		$sql = "SELECT * FROM oc_inward i LEFT JOIN oc_inwarditem ii ON (i.order_no = ii.order_no) WHERE 1=1 ";

		if (!empty($medicine_name)) {
			$sql .= " AND ii.productraw_name LIKE '%" . $this->db->escape($medicine_name) . "%'";
		}

		if ($from_date != '' && $to_date != '') {
			$sql .= " AND i.date >= '" . $this->db->escape(date('Y-m-d', strtotime($from_date))) . "'";
			$sql .= " AND i.date <= '" . $this->db->escape(date('Y-m-d', strtotime($to_date))) . "'";
		}

		$inward = $this->db->query($sql)->rows;


		foreach ($inward as $pvalue) {

			$med_types = $this->db->query("SELECT med_type,unit FROM medicine WHERE med_name LIKE '".$pvalue['productraw_name']."%' ");
			if ($med_types->num_rows > 0) {
				$unit = $med_types->row['unit'];
			} else {
				$unit = '';
			}

			$data['inwards'][] = array(
				'date'=> date('d-m-Y', strtotime($pvalue['date'])),
				'productraw_name'=> $pvalue['productraw_name'],
				'supplier'=> $pvalue['supplier'],
				'quantity'=> $pvalue['quantity'],
				'value'=> $pvalue['value'],
				'order_no'=> $pvalue['order_no'],
				'ex_date'=>  date('d-m-Y', strtotime($pvalue['ex_date'])),
				'batch_no'=> $pvalue['batch_no'],
				'unit'=> $unit,
			);
		}
		$date_totalss = "SELECT SUM(value) AS tot_val, SUM(quantity) AS tot_qty FROM `oc_inwarditem` WHERE 1=1 ";
		if ($from_date && $to_date) {
			$date_totalss .= " AND date_added >= '" . $this->db->escape(date('Y-m-d', strtotime($from_date))) . "'";
			$date_totalss .= " AND date_added <= '" . $this->db->escape(date('Y-m-d', strtotime($to_date))) . "'";
		}

		if ($medicine_name) {
			$date_totalss .= " AND productraw_name LIKE '%" . $this->db->escape($medicine_name) . "%' ";
		}
		$date_total = $this->db->query($date_totalss)->row;

		$data['grn_qty_total'] = $date_total['tot_qty'];
		$data['total'] = $date_total['tot_val'];
		
		$sql1 = "SELECT * FROM medicine_transfer m LEFT JOIN medicine_trans_items mi ON (m.id = mi.parent_id) WHERE 1=1 ";

		if (!empty($medicine_name)) {
			$sql1 .= " AND product_name LIKE '%" . $this->db->escape($medicine_name) . "%'";
		}

		if ($from_date != '' && $to_date != '') {
			$sql1 .= " AND m.entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($from_date))) . "'";
			$sql1 .= " AND m.entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($to_date))) . "'";
		}

		// // echo'<pre>';
		// // print_r($sql1);
		// // exit;

		$transfer = $this->db->query($sql1)->rows;

		foreach ($transfer as $result) {
			$doctors = $this->db->query("SELECT doctor_name FROM doctor WHERE id = '".$result['child_doctor_id']."' ");
			if ($doctors->num_rows > 0) {
				$doctor = $doctors->row['doctor_name'];
			} else {
				$doctor = '';
			}

			$med_types = $this->db->query("SELECT med_type,unit FROM medicine WHERE med_name LIKE '".$result['product_name']."%' ");
			if ($med_types->num_rows > 0) {
				$unit = $med_types->row['unit'];
			} else {
				$unit = '';
			}
			
			$data['medtransfer'][] = array(
				'issue_no'=> $result['issue_no'],
				'entry_date'=> date('d-m-Y', strtotime($result['entry_date'])),
				'doctor_name'=> $doctor,
				'product_name'=> $result['product_name'],
				'expire_date'=> $result['expire_date'],
				'product_qty'=> $result['product_qty'],
				'parent_doctor_name'=> $result['parent_doctor_name'],
				'unit'=> $unit,
			);
		}
		$trans_totalsss ="SELECT SUM(mi.product_qty) AS med_tot_qty FROM medicine_transfer m LEFT JOIN medicine_trans_items mi ON (m.id = mi.parent_id) WHERE 1=1 ";

		if ($from_date && $to_date) {
			$trans_totalsss .= " AND m.entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($from_date))) . "'";
			$trans_totalsss .= " AND m.entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($to_date))) . "'";
		}

		if ($medicine_name) {
			$trans_totalsss .= " AND mi.product_name LIKE '%" . $this->db->escape($medicine_name) . "%' ";
		}
		$trans_total = $this->db->query($trans_totalsss)->row;

		$data['qty_total'] = $trans_total['med_tot_qty'];

		$sql2 = "SELECT * FROM oc_treatment_entry t LEFT JOIN oc_treatment_entry_trans tt ON (t.id = tt.parent_id) WHERE 1=1 ";

		if (!empty($medicine_name)) {
			$sql2 .= " AND medicine_name LIKE '%" . $this->db->escape($medicine_name) . "%'";
		}

		if ($from_date != '' && $to_date != '') {
			$sql2 .= " AND entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($from_date))) . "'";
			$sql2 .= " AND entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($to_date))) . "'";
		}

		$medicine_trans_datas = $this->db->query($sql2)->rows;

		foreach ($medicine_trans_datas as $key => $value) {
			$doc_datas = $this->db->query("SELECT * FROM doctor WHERE id = '".$value['doctor_id']."'");
			if ($doc_datas->num_rows > 0) {
				$doctor_name = $doc_datas->row['doctor_name'];
			} else {
				$doctor_name = '';
			}

			$med_types = $this->db->query("SELECT med_type,unit FROM medicine WHERE med_name LIKE '".$result['product_name']."%' ");
			if ($med_types->num_rows > 0) {
				$unit = $med_types->row['unit'];
			} else {
				$unit = '';
			}

			$data['medicine_trans_datas'][] = array(
				'id' => $value['id'],
				'issue_no' => $value['issue_no'],
				'entry_date' => $value['entry_date'],
				'clinic_name' => $value['clinic_name'],
				'horse_name' => $value['horse_name'],
				'trainer_name' => $value['trainer_name'],
				'doctor_name' => $doctor_name,
				'total_item' => $value['total_item'],
				'total_qty' => $value['total_qty'],
				'total_amt' => $value['total_amt'],
				'unit'=> $unit,
			);
		}

		$treat_totalsss = "SELECT SUM(t.total_qty) AS iss_total_qty FROM oc_treatment_entry t LEFT JOIN oc_treatment_entry_trans tt ON (t.id = tt.parent_id) WHERE 1=1 ";
		if ($from_date && $to_date) {
			$treat_totalsss .= " AND t.entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($from_date))) . "'";
			$treat_totalsss .= " AND t.entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($to_date))) . "'";
		}

		if ($medicine_name) {
			$treat_totalsss .= " AND tt.medicine_name LIKE '%" . $this->db->escape($medicine_name) . "%' ";
		}

		$treat_total = $this->db->query($treat_totalsss)->row;

		$data['iss_qty_total'] = $treat_total['iss_total_qty'];

		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['medicinenName'] = $medicine_name;
		$data['from_date'] = $from_date;
		$data['to_date'] = $to_date;
		$data['note'] = $note;
		$data['avl_stock'] =  $date_total['tot_qty'] - $treat_total['iss_total_qty'];

		// echo'<pre>';
		// print_r($data['inwards']);
		// exit;

		$html = $this->load->view('report/medicine_html', $data);
		//echo $html;exit;
		$filename = 'Medicine_Dump_Report.html';
		//file_put_contents(DIR_DOWNLOAD.Indent, $html);
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;
		$this->response->setOutput($this->load->view('catalog/medicine_report', $data));
	}
}
