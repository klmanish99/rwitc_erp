<?php
class Controllerreportstorereconsilation extends Controller {
	private $error = array();
	public function index() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

	/*	echo '<pre>';
			print_r($this->error);
			exit*/
		//$this->load->language('report/store_reconsilation');

		$this->document->setTitle('Store Reconsilation');

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = date('Y-m-d');
		}



		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date']) ) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_doctor_id']) ) {
			$url .= '&filter_doctor_id=' . $this->request->get['filter_doctor_id'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/store_reconsilation', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('report/store_reconsilation');
		$data['action'] = $this->url->link('report/store_reconsilation/add', 'token=' . $this->session->data['token'] , true);

		$data['back_monthly_daily_reconsilation'] = $this->url->link('report/monthly_medicine_transaction', 'token=' . $this->session->data['token'] , true);

		$filter_data = array(
			'filter_date'	     => $filter_date,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin'),
		);

		$order_total = 0;
		$data['results'] = 0;

		//echo "<pre>";print_r($filter_data);exit;

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_date'] = $this->language->get('column_date');
		$data['column_Trainer'] = $this->language->get('column_Trainer');
		$data['column_Amount'] = $this->language->get('column_Amount');
		$data['column_Charge'] = $this->language->get('column_Charge');
		$data['column_horse_name'] = $this->language->get('column_horse_name');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_group'] = $this->language->get('entry_group');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['error_medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['error_medicine_error'] = '';
		}

		if (isset($this->error['filter_date'])) {
			$data['filter_date'] = $this->error['filter_date'];
		} else {
			$data['filter_date'] = '';
		}

		if (isset($this->error['filter_doctor_id_error'])) {
			$data['filter_doctor_id_error'] = $this->error['filter_doctor_id_error'];
		} else {
			$data['filter_doctor_id_error'] = '';
		}

		if (isset($this->error['day_closed_warming'])) {
			$data['day_closed_warming'] = $this->error['day_closed_warming'];
		} else {
			$data['day_closed_warming'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['medicine_error'] = '';
		}

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['med_transaction'] = array();
		$data['no_result'] = 0;

		//if (!empty($filter_data['filter_doctor_id'])) {
			$results = $this->model_report_store_reconsilation->getMedicineTranscation($filter_data);//echo "<pre>";print_r($results);exit;

			if(!empty($results)){
				$check_results = $this->model_report_store_reconsilation->CheckDayClose($filter_data);//echo "<pre>";print_r($check_results);exit;
				if($check_results === 'day_close'){
					$results = array();
				} else if($check_results === 'no_results'){
					$results = array();
					$data['no_result'] = 1;
				}
			} else { //echo "<pre>";print_r('innnnnnnnnnnnnnnnnn');exit;
				$data['no_result'] = 1;
			}
			
		/*}  else {
			$data['no_result'] = 1;
			$results = array();
		}*/

		//---------------------------day close all array-----------------------------------------------
		$day_close_product_alls = $this->db->query("SELECT DISTINCT * FROM store_reconsilation WHERE entry_date = '".$this->db->escape(date('Y-m-d', strtotime($filter_data['filter_date'])))."' ")->rows;	
		$day_close_product = array();
		foreach($day_close_product_alls as $okey => $ovalue){
			$day_close_product[$ovalue['entry_date'].'_'.$ovalue['doctor_id'].'_'.$ovalue['medicine_code']][] = $ovalue;
		}
		
		if ($results) {
			foreach ($results as $key => $result) {
				if($result['child_doctor_id'] == 0){
					$doctor_id = $result['parent_doctor_id'];
				} else {
					$doctor_id = $result['child_doctor_id'];
				}
				$get_doctor_name =  $this->db->query("SELECT DISTINCT doctor_name FROM doctor WHERE id = '".$doctor_id."' ");
				if($get_doctor_name->num_rows > 0){
					$doctor_name = $get_doctor_name->row['doctor_name'];
				} else {
					$doctor_name = '';
				}

				//echo $doctor_name;exit;
				//$data['med_transaction'][$key][$doctor_id]['doctor_name'] = $doctor_name;
				$treatment_medicine = 0;
				$avilable_qyantity = 0;
				$opening = 0;
				$closing = 0;
				$transfermedicine =  0;
				$return_medicine = 0;

				//---------------------------------get Current transfer---------------------------

				$get_transfer_sql =  $this->db->query("SELECT DISTINCT *, SUM(product_qty) AS total_trans FROM `medicine_transfer`m LEFT JOIN medicine_trans_items mi ON(m.id = mi.parent_id) WHERE (case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end) = '".$doctor_id."' AND mi.product_id = '".$result['product_id']."' AND m.entry_date = '".$this->db->escape(date('Y-m-d', strtotime($filter_data['filter_date'])))."' GROUP BY mi.product_id,(case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end)");

				


				if($get_transfer_sql->num_rows > 0 &&  $get_transfer_sql->row['total_trans'] != NULL){
					$transfermedicine = $get_transfer_sql->row['total_trans'];
				} 

				//---------------------------------get Current treatment---------------------------

				$get_treatment_sql =  $this->db->query("SELECT DISTINCT *, SUM(medicine_qty) AS medicine_qtys FROM `oc_treatment_entry`m LEFT JOIN oc_treatment_entry_trans mi ON(m.id = mi.parent_id) WHERE (case when `doctor_id` = 0 then clinic_id else doctor_id end) = '".$doctor_id."' AND mi.medicine_code = '".$result['product_id']."' AND m.entry_date = '".$this->db->escape(date('Y-m-d', strtotime($filter_data['filter_date'])))."' GROUP BY mi.medicine_code,(case when `doctor_id` = 0 then clinic_id else doctor_id end)");

				if($get_treatment_sql->num_rows > 0 &&  $get_treatment_sql->row['medicine_qtys'] != NULL){
					$treatment_medicine = $get_treatment_sql->row['medicine_qtys'];
				} 


				//------------------------------------get current return----------------------------

				$get_return_sql =  $this->db->query("SELECT DISTINCT *, SUM(product_qty) AS total_trans FROM `medicine_return`m LEFT JOIN medicine_return_items mi ON(m.id = mi.parent_id) WHERE  (case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end) = '".$doctor_id."'AND mi.product_id = '".$result['product_id']."' AND m.entry_date = '".$this->db->escape(date('Y-m-d', strtotime($filter_data['filter_date'])))."' GROUP BY mi.product_id,(case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end)");




				if($get_return_sql->num_rows > 0 &&  $get_return_sql->row['total_trans'] != NULL){
					$return_medicine = $get_return_sql->row['total_trans'];
				} 

				$avilable_qyantity = ($transfermedicine - ($treatment_medicine + $return_medicine)) ;

				//-----------------------------------medicine unit------------------------------------
				$medicine_product = $this->db->query("SELECT store_unit FROM `medicine` WHERE `med_code` = '".$result['product_id']."' ");
				if($medicine_product->num_rows > 0){
					$medicine_product_code = $medicine_product->row['store_unit'] ;
				} else {
					$medicine_product_code = '' ;
				}

				//---------------------------------opening_amt---------------------------------------
				$opening_from_day_close =  $this->db->query("SELECT DISTINCT SUM(closing) AS closings FROM `store_reconsilation` WHERE doctor_id = '".$doctor_id."' AND medicine_code = '".$result['product_id']."' AND entry_date = '".$this->db->escape(date('Y-m-d', strtotime($filter_data['filter_date'] . ' -1 day')))."' GROUP BY medicine_code,doctor_id");
				
				if($opening_from_day_close->num_rows > 0 &&  $opening_from_day_close->row['closings'] != ''){
					$opening = $opening_from_day_close->row['closings'] ;
				} else {
					$opening = 0;
				}
				//---------------------------------closing amt calculation--------------
				if($opening != 0){
					$closing = ($transfermedicine + $opening) - ($treatment_medicine + $return_medicine);
				} else {
					$closing = $avilable_qyantity;
				}

				if(isset($day_close_product[date('Y-m-d', strtotime($filter_data['filter_date'])).'_'.$doctor_id.'_'.$result['product_id']])){
					$redirect_transfer = '';
					$redirect_tratement = '';
				} else {
					if(date('Y-m-d', strtotime($filter_data['filter_date'])) == date('Y-m-d')){
						$redirect_transfer = HTTP_SERVER.'index.php?route=catalog/medicine_transfer&token='.$this->session->data['token'].'&date_reconsilation=' . date('d-m-Y', strtotime($filter_data['filter_date']));
						$redirect_tratement = HTTP_SERVER.'index.php?route=catalog/treatment_entry_single/add&token='.$this->session->data['token'].'&date_reconsilation=' . date('d-m-Y', strtotime($filter_data['filter_date']));
					} else {
						if(date('Y-m-d', strtotime($filter_data['filter_date'])) > date('Y-m-d')){ 
							$redirect_transfer = 'Day Is Future';
							$redirect_tratement = '';
						} else {
							$redirect_transfer = 'Day Not Closed';
							$redirect_tratement = '';
							
						}
					}
					
				}

				$data['med_transaction'][$doctor_id][$doctor_name][]= array(
					'entry_date'=> $result['entry_date'],
					'total_trans'=> $transfermedicine,
					'treatment_medicine'=> $treatment_medicine,
					'product_name'=> $result['product_name'],
					'product_id'=> $result['product_id'],
					'store_unit'=> $medicine_product_code,
					'avilable_qyantity' => $avilable_qyantity,
					'opening' => $opening,
					'closing' => $closing,
					'redirect_transfer' => $redirect_transfer,
					'redirect_tratement' => $redirect_tratement,
					'return_medicine' => $return_medicine,
				);
			}
		}

		//echo "<pre>";print_r($data['med_transaction']);exit;

		//-----------------------medicine present in treatment but not in transfer---------------
		if (!empty($filter_data['filter_doctor_id'])) {
			$medicine_treatment = $this->model_report_store_reconsilation->getMedicineTreatmentTranscation($filter_data);
		}  else {
			$medicine_treatment = array();
		}
		$data['new_med_treatment_transaction'] = array();
		//echo "<pre>";print_r($medicine_treatment);exit;
		if ($medicine_treatment) {
			foreach ($medicine_treatment as $mkey => $mresult) {
				if($mresult['doctor_id'] == 0){
					$doctor_id = $mresult['clinic_id'];
				} else {
					$doctor_id = $mresult['doctor_id'];
				}

				$get_treatment_sql =  $this->db->query("SELECT *, SUM(product_qty) AS total_trans FROM `medicine_transfer`m LEFT JOIN medicine_trans_items mi ON(m.id = mi.parent_id) WHERE (case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end) = '".$doctor_id."' AND mi.product_id = '".$mresult['medicine_code']."'  GROUP BY mi.product_id ,(case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end)");

				if($get_treatment_sql->num_rows == 0 &&  $get_treatment_sql->row == NULL){
					$data['new_med_treatment_transaction'][] = array(
						'entry_date'=> $mresult['entry_date'],
						'medicine_qtys'=> $mresult['medicine_qtys'],
						'product_name'=> $mresult['medicine_name'],
						'product_id'=> $mresult['medicine_code'],
						'store_unit'=> $mresult['unit'],
					);
				} 
			}
		}

		//echo "<pre>";print_r($data['new_med_treatment_transaction']);exit;

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/store_reconsilation', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['filter_date'] = date('d-m-Y', strtotime($filter_date));
		//$data['filter_doctor_name'] = $filter_doctor_name;
		//$data['filter_doctor_id'] = $filter_doctor_id;

		$this->response->setOutput($this->load->view('report/store_reconsilation', $data));
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function add() {
		$this->load->language('report/store_reconsilation');
		/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('report/store_reconsilation');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/

			$post_date = date('Y-m-d', strtotime($this->request->post['filter_date'] . ' -1 day'));

			$all_docters =  $this->db->query("SELECT DISTINCT * FROM `doctor` WHERE 1=1 ");
			$all_medicine_stock_status =  $this->db->query("SELECT DISTINCT * FROM `medicine_stock_status` WHERE med_status = 'Active' ");
			$data['med_transaction'] = array();
			foreach ($all_docters->rows as $dockey => $docvalue) {
				foreach ($all_medicine_stock_status->rows as $medkey => $medvalue) {
					//-------------------transfer-------------------------------------------
					$all_medicine_transfer_data = $this->db->query("SELECT DISTINCT SUM(product_qty) AS total_trans FROM `medicine_transfer`m LEFT JOIN medicine_trans_items mi ON(m.id = mi.parent_id) WHERE  m.entry_date = '" .$this->db->escape(date('Y-m-d', strtotime($this->request->post['filter_date']))). "' AND (case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end) = '".$this->db->escape($docvalue['id'])."' AND mi.product_id = '".$medvalue['med_code']."' GROUP BY mi.product_id ORDER BY m.entry_date");

					
					$medicine_transfers = 0;
					$openings = '0000';
					$opening = 0;

					$medicine_treatments = 0;
					$closing = 0;
					if($all_medicine_transfer_data->num_rows > 0){
						$medicine_transfers = $all_medicine_transfer_data->row['total_trans'];
					}

					//------------------------treatment------------------------------------------
					$all_medicine_treatment_data =  $this->db->query("SELECT DISTINCT SUM(medicine_qty) AS medicine_qtys FROM `oc_treatment_entry`m LEFT JOIN oc_treatment_entry_trans mi ON(m.id = mi.parent_id) WHERE (case when `doctor_id` = 0 then clinic_id else doctor_id end) = '".$docvalue['id']."' AND mi.medicine_code = '".$medvalue['med_code']."' AND m.entry_date = '".$this->db->escape(date('Y-m-d', strtotime($this->request->post['filter_date'])))."' GROUP BY mi.medicine_code");
					
					if($all_medicine_treatment_data->num_rows > 0){
						$medicine_treatments = $all_medicine_treatment_data->row['medicine_qtys'];
					}
				
					//----------------------------------opening----------------------------------
					$opening_from_day_close =  $this->db->query("SELECT DISTINCT SUM(closing) AS closings FROM `store_reconsilation` WHERE doctor_id = '".$docvalue['id']."' AND medicine_code = '".$medvalue['med_code']."' AND entry_date = '".$post_date."' GROUP BY medicine_code");
					
					if($opening_from_day_close->num_rows > 0 && $opening_from_day_close->row['closings'] != NULL){
						$opening = $opening_from_day_close->row['closings'];
						$openings = $opening_from_day_close->row['closings'];
					} else {
						$openings = 0;
					}
					//---------------------------------calculation----------------------------------------------------------
					
					$closing = ($medicine_transfers + $opening) - $medicine_treatments;
					 

					if($openings != '0000' || $medicine_treatments > 0 || $medicine_transfers > 0){
						$this->db->query("INSERT INTO `store_reconsilation` SET entry_date = '".date('Y-m-d',strtotime($this->request->post['filter_date']))."', doctor_id = '".$this->db->escape($docvalue['id'])."',doctor_name = '".$this->db->escape($docvalue['doctor_name'])."',medicine_name = '".$this->db->escape($medvalue['med_name'])."',medicine_code = '".$this->db->escape($medvalue['med_code'])."', opening = '".$opening."', closing = '".$closing."' ,day_close_status = 1 ,store_id = 1");
						
					}
				}
			}
				//exit;
			//$this->model_report_store_reconsilation->addMedData($this->request->post);
			$this->session->data['success'] = 'Success: Day Closed!';
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}
		}
		$this->index();
	}
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'report/store_reconsilation')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		/*echo '<pre>';
		print_r($this->request->post);
		exit;*/
		

		if ((utf8_strlen($this->request->post['filter_date']) == '')) {
			$this->error['filter_date'] = "Please Select date";
		}
		//----------------------------check_date_differeence validation-------------------------------------------------------------

		/*$id_check_date_close_data = $this->db->query("SELECT * FROM store_reconsilation  WHERE entry_date = '".date('Y-m-d', strtotime($this->request->post['filter_date'] . ' -1 day'))."' ORDER BY entry_date DESC LIMIT 1");

		if($id_check_date_close_data->num_rows > 0){
			$this->error['day_closed_warming'] = "Already Day Close Done ".date('Y-m-d', strtotime($id_check_date_close_data->row['entry_date']));
		} else {*/
			$day_close_product_alls = $this->db->query("SELECT MAX(entry_date) AS start_date FROM store_reconsilation  ORDER BY entry_date DESC LIMIT 1 ");
			if($day_close_product_alls->num_rows > 0 && $day_close_product_alls->row['start_date'] != ''){
				$datetime1 = new DateTime(date('Y-m-d', strtotime($this->request->post['filter_date'])));
				$datetime2 = new DateTime(date('Y-m-d', strtotime($day_close_product_alls->row['start_date'])));
				$interval = $datetime1->diff($datetime2);
				if($interval->d > 1){
					$this->error['day_closed_warming'] = "Last Day Closed Date Is ".date('Y-m-d', strtotime($day_close_product_alls->row['start_date'])).' You have to Day Close First '.date('Y-m-d', strtotime($day_close_product_alls->row['start_date'] . ' +1 day'));
				}
			} else {
				$is_transfer = $this->db->query("SELECT MIN(entry_date) AS start_date FROM medicine_transfer  ORDER BY entry_date DESC LIMIT 1 ");

				if($is_transfer->num_rows > 0 && $is_transfer->row['start_date'] != ''){
					$datetime1 = new DateTime(date('Y-m-d', strtotime($this->request->post['filter_date'])));
					$datetime2 = new DateTime(date('Y-m-d', strtotime($is_transfer->row['start_date'])));
					$interval = $datetime1->diff($datetime2);
					if($interval->d > 1){
						$this->error['day_closed_warming'] = "You have to Day Close First ".date('Y-m-d', strtotime($is_transfer->row['start_date']));
					}
				} 
			}
		//}


		if(!isset($this->request->post['productraw_datas'])){
			//$this->error['medicine_error'] = 'Please Enter Atleast Entry';
		} 
		$datas = $this->request->post;
		return !$this->error;
	}

	public function autocompletedoc() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_doctor_name'])) {
			$this->load->model('report/store_reconsilation');

			$results = $this->model_report_store_reconsilation->getDoctorAuto($this->request->get['filter_doctor_name']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name'     => strip_tags(html_entity_decode($result['doctor_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_name'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}
	public function prints() {

        $this->document->setTitle('Store Reconsilation');

        if (isset($this->request->get['filter_date'])) {
            $filter_date = $this->request->get['filter_date'];
        } else {
            $filter_date = date('Y-m-d');
        }



        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_date']) ) {
            $url .= '&filter_date=' . $this->request->get['filter_date'];
        }

        if (isset($this->request->get['filter_doctor_id']) ) {
            $url .= '&filter_doctor_id=' . $this->request->get['filter_doctor_id'];
        }
        
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
        $data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . $url, true);
        
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('report/store_reconsilation', 'token=' . $this->session->data['token'] . $url, true)
        );

        $this->load->model('report/store_reconsilation');
        $data['action'] = $this->url->link('report/store_reconsilation/add', 'token=' . $this->session->data['token'] , true);

        $data['back_monthly_daily_reconsilation'] = $this->url->link('report/monthly_medicine_transaction', 'token=' . $this->session->data['token'] , true);

        $filter_data = array(
            'filter_date'        => $filter_date,
            'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'                  => $this->config->get('config_limit_admin'),
        );

        $order_total = 0;
        $data['results'] = 0;

        //echo "<pre>";print_r($filter_data);exit;

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_all_status'] = $this->language->get('text_all_status');

        $data['column_date'] = $this->language->get('column_date');
        $data['column_Trainer'] = $this->language->get('column_Trainer');
        $data['column_Amount'] = $this->language->get('column_Amount');
        $data['column_Charge'] = $this->language->get('column_Charge');
        $data['column_horse_name'] = $this->language->get('column_horse_name');
        $data['column_total'] = $this->language->get('column_total');

        $data['entry_date_start'] = $this->language->get('entry_date_start');
        $data['entry_date_end'] = $this->language->get('entry_date_end');
        $data['entry_group'] = $this->language->get('entry_group');
        $data['entry_status'] = $this->language->get('entry_status');

        $data['button_filter'] = $this->language->get('button_filter');


        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->error['medicine_error'])) {
            $data['error_medicine_error'] = $this->error['medicine_error'];
        } else {
            $data['error_medicine_error'] = '';
        }

        if (isset($this->error['filter_date'])) {
            $data['filter_date'] = $this->error['filter_date'];
        } else {
            $data['filter_date'] = '';
        }

        if (isset($this->error['filter_doctor_id_error'])) {
            $data['filter_doctor_id_error'] = $this->error['filter_doctor_id_error'];
        } else {
            $data['filter_doctor_id_error'] = '';
        }

        if (isset($this->error['day_closed_warming'])) {
            $data['day_closed_warming'] = $this->error['day_closed_warming'];
        } else {
            $data['day_closed_warming'] = '';
        }

        if (isset($this->error['medicine_error'])) {
            $data['medicine_error'] = $this->error['medicine_error'];
        } else {
            $data['medicine_error'] = '';
        }

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        $data['med_transaction'] = array();
        $data['no_result'] = 0;

        //if (!empty($filter_data['filter_doctor_id'])) {
            $results = $this->model_report_store_reconsilation->getMedicineTranscation($filter_data);//echo "<pre>";print_r($results);exit;

            if(!empty($results)){
                $check_results = $this->model_report_store_reconsilation->CheckDayClose($filter_data);//echo "<pre>";print_r($check_results);exit;
                if($check_results === 'day_close'){
                    $results = array();
                } else if($check_results === 'no_results'){
                    $results = array();
                    $data['no_result'] = 1;
                }
            } else { //echo "<pre>";print_r('innnnnnnnnnnnnnnnnn');exit;
                $data['no_result'] = 1;
            }
            
        /*}  else {
            $data['no_result'] = 1;
            $results = array();
        }*/

        //---------------------------day close all array-----------------------------------------------
        $day_close_product_alls = $this->db->query("SELECT DISTINCT * FROM store_reconsilation WHERE entry_date = '".$this->db->escape(date('Y-m-d', strtotime($filter_data['filter_date'])))."' ")->rows;  
        $day_close_product = array();
        foreach($day_close_product_alls as $okey => $ovalue){
            $day_close_product[$ovalue['entry_date'].'_'.$ovalue['doctor_id'].'_'.$ovalue['medicine_code']][] = $ovalue;
        }
        
        if ($results) {
            foreach ($results as $key => $result) {
                if($result['child_doctor_id'] == 0){
                    $doctor_id = $result['parent_doctor_id'];
                } else {
                    $doctor_id = $result['child_doctor_id'];
                }
                $get_doctor_name =  $this->db->query("SELECT DISTINCT doctor_name FROM doctor WHERE id = '".$doctor_id."' ");
                if($get_doctor_name->num_rows > 0){
                    $doctor_name = $get_doctor_name->row['doctor_name'];
                } else {
                    $doctor_name = '';
                }

                //echo $doctor_name;exit;
               // $data['med_transaction'][$doctor_id]['doctor_name'] = $doctor_name;
                $treatment_medicine = 0;
                $avilable_qyantity = 0;
                $opening = 0;
                $closing = 0;
                $transfermedicine =  0;
                $return_medicine = 0;

                //---------------------------------get Current transfer---------------------------

                $get_transfer_sql =  $this->db->query("SELECT DISTINCT *, SUM(product_qty) AS total_trans FROM `medicine_transfer`m LEFT JOIN medicine_trans_items mi ON(m.id = mi.parent_id) WHERE (case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end) = '".$doctor_id."' AND mi.product_id = '".$result['product_id']."' AND m.entry_date = '".$this->db->escape(date('Y-m-d', strtotime($filter_data['filter_date'])))."' GROUP BY mi.product_id,(case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end)");

                


                if($get_transfer_sql->num_rows > 0 &&  $get_transfer_sql->row['total_trans'] != NULL){
                    $transfermedicine = $get_transfer_sql->row['total_trans'];
                } 

                //---------------------------------get Current treatment---------------------------

                $get_treatment_sql =  $this->db->query("SELECT DISTINCT *, SUM(medicine_qty) AS medicine_qtys FROM `oc_treatment_entry`m LEFT JOIN oc_treatment_entry_trans mi ON(m.id = mi.parent_id) WHERE (case when `doctor_id` = 0 then clinic_id else doctor_id end) = '".$doctor_id."' AND mi.medicine_code = '".$result['product_id']."' AND m.entry_date = '".$this->db->escape(date('Y-m-d', strtotime($filter_data['filter_date'])))."' GROUP BY mi.medicine_code,(case when `doctor_id` = 0 then clinic_id else doctor_id end)");

                if($get_treatment_sql->num_rows > 0 &&  $get_treatment_sql->row['medicine_qtys'] != NULL){
                    $treatment_medicine = $get_treatment_sql->row['medicine_qtys'];
                } 


                //------------------------------------get current return----------------------------

                $get_return_sql =  $this->db->query("SELECT DISTINCT *, SUM(product_qty) AS total_trans FROM `medicine_return`m LEFT JOIN medicine_return_items mi ON(m.id = mi.parent_id) WHERE  (case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end) = '".$doctor_id."'AND mi.product_id = '".$result['product_id']."' AND m.entry_date = '".$this->db->escape(date('Y-m-d', strtotime($filter_data['filter_date'])))."' GROUP BY mi.product_id,(case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end)");




                if($get_return_sql->num_rows > 0 &&  $get_return_sql->row['total_trans'] != NULL){
                    $return_medicine = $get_return_sql->row['total_trans'];
                } 

                $avilable_qyantity = ($transfermedicine - ($treatment_medicine + $return_medicine)) ;

                //-----------------------------------medicine unit------------------------------------
                $medicine_product = $this->db->query("SELECT store_unit FROM `medicine` WHERE `med_code` = '".$result['product_id']."' ");
                if($medicine_product->num_rows > 0){
                    $medicine_product_code = $medicine_product->row['store_unit'] ;
                } else {
                    $medicine_product_code = '' ;
                }

                //---------------------------------opening_amt---------------------------------------
                $opening_from_day_close =  $this->db->query("SELECT DISTINCT SUM(closing) AS closings FROM `store_reconsilation` WHERE doctor_id = '".$doctor_id."' AND medicine_code = '".$result['product_id']."' AND entry_date = '".$this->db->escape(date('Y-m-d', strtotime($filter_data['filter_date'] . ' -1 day')))."' GROUP BY medicine_code,doctor_id");
                
                if($opening_from_day_close->num_rows > 0 &&  $opening_from_day_close->row['closings'] != ''){
                    $opening = $opening_from_day_close->row['closings'] ;
                } else {
                    $opening = 0;
                }
                //---------------------------------closing amt calculation--------------
                if($opening != 0){
                    $closing = ($transfermedicine + $opening) - ($treatment_medicine + $return_medicine);
                } else {
                    $closing = $avilable_qyantity;
                }

                if(isset($day_close_product[date('Y-m-d', strtotime($filter_data['filter_date'])).'_'.$doctor_id.'_'.$result['product_id']])){
                    $redirect_transfer = '';
                    $redirect_tratement = '';
                } else {
                    if(date('Y-m-d', strtotime($filter_data['filter_date'])) == date('Y-m-d')){
                        $redirect_transfer = HTTP_SERVER.'index.php?route=catalog/medicine_transfer&token='.$this->session->data['token'].'&date_reconsilation=' . date('d-m-Y', strtotime($filter_data['filter_date']));
                        $redirect_tratement = HTTP_SERVER.'index.php?route=catalog/treatment_entry_single/add&token='.$this->session->data['token'].'&date_reconsilation=' . date('d-m-Y', strtotime($filter_data['filter_date']));
                    } else {
                        if(date('Y-m-d', strtotime($filter_data['filter_date'])) > date('Y-m-d')){ 
                            $redirect_transfer = 'Day Is Future';
                            $redirect_tratement = '';
                        } else {
                            $redirect_transfer = 'Day Not Closed';
                            $redirect_tratement = '';
                            
                        }
                    }
                    
                }

                $data['med_transaction'][$doctor_id][$doctor_name][]= array(
                    'entry_date'=> $result['entry_date'],
                    'total_trans'=> $transfermedicine,
                    'treatment_medicine'=> $treatment_medicine,
                    'product_name'=> $result['product_name'],
                    'product_id'=> $result['product_id'],
                    'store_unit'=> $medicine_product_code,
                    'avilable_qyantity' => $avilable_qyantity,
                    'opening' => $opening,
                    'closing' => $closing,
                    'redirect_transfer' => $redirect_transfer,
                    'redirect_tratement' => $redirect_tratement,
                    'return_medicine' => $return_medicine,
                );
            }
        }

        //echo "<pre>";print_r($data['med_transaction']);exit;

        //-----------------------medicine present in treatment but not in transfer---------------
        if (!empty($filter_data['filter_doctor_id'])) {
            $medicine_treatment = $this->model_report_store_reconsilation->getMedicineTreatmentTranscation($filter_data);
        }  else {
            $medicine_treatment = array();
        }
        $data['new_med_treatment_transaction'] = array();
        //echo "<pre>";print_r($medicine_treatment);exit;
        if ($medicine_treatment) {
            foreach ($medicine_treatment as $mkey => $mresult) {
                if($mresult['doctor_id'] == 0){
                    $doctor_id = $mresult['clinic_id'];
                } else {
                    $doctor_id = $mresult['doctor_id'];
                }

                $get_treatment_sql =  $this->db->query("SELECT *, SUM(product_qty) AS total_trans FROM `medicine_transfer`m LEFT JOIN medicine_trans_items mi ON(m.id = mi.parent_id) WHERE (case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end) = '".$doctor_id."' AND mi.product_id = '".$mresult['medicine_code']."'  GROUP BY mi.product_id ,(case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end)");

                if($get_treatment_sql->num_rows == 0 &&  $get_treatment_sql->row == NULL){
                    $data['new_med_treatment_transaction'][] = array(
                        'entry_date'=> $mresult['entry_date'],
                        'medicine_qtys'=> $mresult['medicine_qtys'],
                        'product_name'=> $mresult['medicine_name'],
                        'product_id'=> $mresult['medicine_code'],
                        'store_unit'=> $mresult['unit'],
                    );
                } 
            }
        }

        $data['filter_date'] = date('d-m-Y', strtotime($filter_date));
        $html = $this->load->view('report/store_reconsilation_html', $data);
        //echo $html;exit;
        $filename = 'StoreReconsilation.html';
        //file_put_contents(DIR_DOWNLOAD.Indent, $html);
        header('Content-disposition: attachment; filename=' . $filename);
        header('Content-type: text/html');
        echo $html;exit;
        
    }
}