<?php
class Controllerreportmonthlymedicinetransaction extends Controller {
	private $error = array();
	public function index() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		//$this->load->language('report/monthly_medicine_transaction');

		$this->document->setTitle('Monthly Daily Reconsilation');

		if (isset($this->request->get['filter_months'])) {
			$filter_months = $this->request->get['filter_months'];
		} else {
			$filter_months = date('m');
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_months'])) {
			$url .= '&filter_months=' . $this->request->get['filter_months'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/monthly_medicine_transaction', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('report/monthly_medicine_transaction');
		$data['back_daily_reconsilation'] = $this->url->link('report/medicine_transaction', 'token=' . $this->session->data['token'] , true);
		$data['action'] = $this->url->link('report/monthly_medicine_transaction/add', 'token=' . $this->session->data['token'] , true);

		$filter_data = array(
			'filter_months' => $filter_months,
			'filter_year' => $filter_year,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin'),
		);

		$order_total = 0;
		$data['results'] = 0;

		//echo "<pre>";print_r($filter_data);exit;

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_date'] = $this->language->get('column_date');
		$data['column_Trainer'] = $this->language->get('column_Trainer');
		$data['column_Amount'] = $this->language->get('column_Amount');
		$data['column_Charge'] = $this->language->get('column_Charge');
		$data['column_horse_name'] = $this->language->get('column_horse_name');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_group'] = $this->language->get('entry_group');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['error_medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['error_medicine_error'] = '';
		}

		if (isset($this->error['filter_date'])) {
			$data['filter_date'] = $this->error['filter_date'];
		} else {
			$data['filter_date'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['medicine_error'] = '';
		}

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['med_transaction'] = array();

		$data['med_transaction'] = $this->model_report_monthly_medicine_transaction->getMedicineTranscation($filter_data);

		/*if ($results) {
			foreach ($results as $key => $result) {
				$data['med_transaction'][] = array(
					'product_name' 	  			=> $result['product_name'],
					'total_trans'   			=> $result['total_trans'],
				);
	
			}
		}*/

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);

		$url = '';

		if (isset($this->request->get['filter_months'])) {
			$url .= '&filter_months=' . $this->request->get['filter_months'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/monthly_medicine_transaction', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		$data['filter_months'] = $filter_months;
		$data['filter_year'] = $filter_year;

		$this->response->setOutput($this->load->view('report/monthly_medicine_transaction', $data));
	}

	public function add() {
		$this->load->language('report/monthly_medicine_transaction');
		/*$this->load->model('catalog/product');*/
		/*echo '<pre>';
			print_r($this->request->server['REQUEST_METHOD'] == 'POST');
			exit;*/

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('report/monthly_medicine_transaction');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
			$this->model_report_monthly_medicine_transaction->addMedData($this->request->post);

			$this->session->data['success'] = 'Success: You have added Entry!';

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/

			//$this->response->redirect($this->url->link('report/monthly_medicine_transaction', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->response->redirect($this->url->link('report/monthly_medicine_transaction', 'token=' . $this->session->data['token'] . $url, true));
		
	}
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'report/monthly_medicine_transaction')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		/*echo '<pre>';
		print_r($this->request->post);
		exit;*/
		

		if(!isset($this->request->post['productraw_datas'])){
			$this->error['medicine_error'] = 'Please Enter Atleast Entry';
		} 
		$datas = $this->request->post;
		return !$this->error;
	}
}