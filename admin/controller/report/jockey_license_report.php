<?php
class Controllerreportjockeylicensereport extends Controller {
	public function index() {
		$this->load->language('report/jockey_license_report');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = '';
		}

		if (isset($this->request->get['filter_end_dates'])) {
			$filter_end_dates = $this->request->get['filter_end_dates'];
		} else {
			$filter_end_dates = '';
		}


		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_end_dates'])) {
			$url .= '&filter_end_dates=' . urlencode(html_entity_decode($this->request->get['filter_end_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/jockey_license_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('report/jockey_license_report');

		$data['jockey_license_datas'] = array();

		$filter_data = array(
			'filter_date'	  => $filter_date,
			'filter_end_dates'	  => $filter_end_dates,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin'),
		);

		$order_total = $this->model_report_jockey_license_report->getTotalJockeyLicense($filter_data);

		$results = $this->model_report_jockey_license_report->getJockeyLicense($filter_data);
		$license_start_date = '';
		$license_end_date = '';
		$last_update_date = '';
		foreach ($results as $result) {
			if($result['license_start_date'] != '0000-00-00' &&  $result['license_start_date'] != '1970-01-01'){
				$license_start_date = date('d-m-Y', strtotime($result['license_start_date']));
			}

			if($result['license_end_date'] != '0000-00-00' &&  $result['license_end_date'] != '1970-01-01'){
				$license_end_date = date('d-m-Y', strtotime($result['license_end_date']));
			}

			if($result['renewal_date'] != '0000-00-00' &&  $result['renewal_date'] != '1970-01-01'){
				$renewal_date = date('d-m-Y', strtotime($result['renewal_date']));
			}

			if($result['last_update_date'] != '0000-00-00 00:00:00'){
				$last_update_date = date('d-m-Y', strtotime($result['last_update_date']));
			}
			
			$season = date('d-M-Y', strtotime($result['season_start'])).'<b> TO </b> '. date('d-M-Y', strtotime($result['season_end']));
			
			$data['jockey_license_datas'][] = array(
				'license_start_date' => $license_start_date ,
				'license_end_date'     => $license_end_date,
				'renewal_date'     => $renewal_date,
				'season'   =>$season,
				'entry_date'   => $last_update_date,
				'fees'   => $result['fees'],
				'age'   => $result['age'],
				'amount'   => $result['amount'],
				'jockey_name'   => $result['jockey_name'],
				'license_type'   => $result['license_type'],
				'apprenties'   => $result['apprenties'],
				'license_type'   => $result['license_type'],
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_date_start'] = $this->language->get('column_date_start');
		$data['column_date_end'] = $this->language->get('column_date_end');
		$data['column_orders'] = $this->language->get('column_orders');
		$data['column_products'] = $this->language->get('column_products');
		$data['column_tax'] = $this->language->get('column_tax');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_group'] = $this->language->get('entry_group');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/jockey_license_report', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['filter_date'] = $filter_date;
		$data['filter_end_dates'] = $filter_end_dates;

		$this->response->setOutput($this->load->view('report/jockey_license_report', $data));
	}

	
}