<?php
class Controllerreportnamechangereport extends Controller {
	public function index() {
		$this->load->language('report/name_change_report');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_hourse_name'])) {
			$filter_hourse_name = $this->request->get['filter_hourse_name'];
		} else {
			$filter_hourse_name = '';
		}

		if (isset($this->request->get['filter_hourse_id'])) {
			$filter_hourse_id = $this->request->get['filter_hourse_id'];
		} else {
			$filter_hourse_id = '';
		}


		if (isset($this->request->get['filter_owner_name'])) {
			$filter_owner_name = $this->request->get['filter_owner_name'];
		} else {
			$filter_owner_name = '';
		}

		if (isset($this->request->get['filter_owner_id'])) {
			$filter_owner_id = $this->request->get['filter_owner_id'];
		} else {
			$filter_owner_id = '';
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = '';
		}


		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}


		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_hourse_name']) ) {
			$url .= '&filter_hourse_name=' . $this->request->get['filter_hourse_name'];
		}

		if (isset($this->request->get['filter_hourse_id']) ) {
			$url .= '&filter_hourse_id=' . $this->request->get['filter_hourse_id'];
		}


		if (isset($this->request->get['filter_owner_name']) ) {
			$url .= '&filter_owner_name=' . $this->request->get['filter_owner_name'];
		}

		if (isset($this->request->get['filter_owner_id']) ) {
			$url .= '&filter_owner_id=' . $this->request->get['filter_owner_id'];
		}

		if (isset($this->request->get['filter_date']) ) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/name_change_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('report/name_change_report');

		$data['final_arrival_charges'] = array();

		$filter_data = array(
			'filter_date'	     => $filter_date,
			'filter_date_end'	     => $filter_date_end,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin'),
			'filter_hourse_id' => $filter_hourse_id,
			'filter_owner_id'	=>	$filter_owner_id,
		);

		$order_total = 0;
		$data['results'] = 0;
		$data['final_arrival_charges'] = $this->model_report_name_change_report->getUndertakingChareges($filter_data);

		//echo "<pre>";print_r($data['final_arrival_charges']);exit;

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_regierstion_date'] = $this->language->get('column_regierstion_date');
		$data['column_horse_name'] = $this->language->get('column_horse_name');
		$data['column_regierstion_fee'] = $this->language->get('column_regierstion_fee');
		$data['column_registeration_type'] = $this->language->get('column_registeration_type');
		$data['column_reason'] = $this->language->get('column_reason');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_group'] = $this->language->get('entry_group');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/name_change_report', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['filter_hourse_name'] = $filter_hourse_name;
		$data['filterHorseId'] = $filter_hourse_id;

		$data['filter_owner_name'] = $filter_owner_name;
		$data['filter_owner_id'] = $filter_owner_id;

		$data['filter_date'] = $filter_date;
		$data['filter_date_end'] = $filter_date_end;

		$this->response->setOutput($this->load->view('report/name_change_report', $data));
	}

	public function autocompleteHorse() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['horse_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesAuto($this->request->get['horse_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'horse_id' => $result['horseseq'],
						'horse_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		/*echo '<pre>';print_r($json);
			exit;*/
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['horse_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteOwner() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['owner_name'])) {
			$this->load->model('report/name_change_report');

			

			$results = $this->model_report_name_change_report->getOwnerAuto($this->request->get['owner_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'owner_id' => $result['owner_id'],
						'owner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		/*echo '<pre>';print_r($json);
			exit;*/
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['owner_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}
}