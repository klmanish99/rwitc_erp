<?php

class ControllerReportHorseTreatmentReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('report\inward_po_report');
		$this->load->model('report/horse_treatment_report');
		$this->document->setTitle('Datewise Horse Report');

		$this->getList();
	}

	protected function getList() {

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['filter_dates'])) {
			$filter_dates = $this->request->get['filter_dates'];
		} else {
			$filter_dates = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['horsetreatments'] = array();

		$filter_data = array(
			'filter_date'	  => $filter_date,
			'filter_dates'	  => $filter_dates,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$results = $this->model_report_horse_treatment_report->getHorses($filter_data);

		foreach ($results as $result) {
			$med_types = $this->db->query("SELECT med_type FROM medicine WHERE med_name LIKE '".$result['medicine_name']."%' ");
			if ($med_types->num_rows > 0) {
				$med_type = $med_types->row['med_type'];
			} else {
				$med_type = '';
			}

			$data['horsetreatments'][] = array(
				'entry_date' => date('d-m-Y', strtotime($result['entry_date'])),
				'horse_name' => $result['horse_name'],
				'trainer_name' => $result['trainer_name'],
				'medicine_name' => $result['medicine_name'],
				'med_type' => $med_type,
				'medicine_qty' => $result['medicine_qty'],
				'unit' => $result['unit'],

			);
		}

		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$url = '';

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_date'] = $filter_date;
		$data['filter_dates'] = $filter_dates;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];

		$this->response->setOutput($this->load->view('report/horse_treatment_report', $data));
	}

	public function prints() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$data['horsetreatments'] = array();

		if (isset($this->request->get['filter_date'])) {
			$from_date = $this->request->get['filter_date'];
		} else {
			$from_date = '';
		}
		if (isset($this->request->get['filter_dates'])) {
			$to_date = $this->request->get['filter_dates'];
		} else {
			$to_date = '';
		}

		$sql = "SELECT * FROM oc_treatment_entry_trans tt LEFT JOIN oc_treatment_entry t ON (tt.parent_id = t.id)";

		$sql .= " WHERE 1=1";
		
		if ($from_date != '' && $to_date != '') {
			$sql .= " AND entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($from_date))) . "'";
			$sql .= " AND entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($to_date))) . "'";
		}

		// echo'<pre>';
		// print_r($sql);
		// exit;

		$treat_datas = $this->db->query($sql)->rows;

		foreach ($treat_datas as $pvalue) {
			
			$med_types = $this->db->query("SELECT med_type FROM medicine WHERE med_name LIKE '".$pvalue['medicine_name']."%' ");
			if ($med_types->num_rows > 0) {
				$med_type = $med_types->row['med_type'];
			} else {
				$med_type = '';
			}
			
			$data['horsetreatments'][] = array(
				'horse_name' => $pvalue['horse_name'],
				'trainer_name' => $pvalue['trainer_name'],
				'medicine_name' => $pvalue['medicine_name'],
				'med_type' => $med_type,
				'medicine_qty' => $pvalue['medicine_qty'],
				'unit' => $pvalue['unit'],
				'entry_date' => date('d-m-Y', strtotime($pvalue['entry_date'])),
			);
		}

		// echo'<pre>';
		// print_r($data['inwards']);
		// exit;

		$html = $this->load->view('report/horse_treatment_report_html', $data);
		//echo $html;exit;
		$filename = 'Datewise Horse Report.xls';
		//file_put_contents(DIR_DOWNLOAD.Indent, $html);
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;
		$this->response->setOutput($this->load->view('catalog/inward_form', $data));
	}
}
