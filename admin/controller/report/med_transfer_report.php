<?php

class ControllerReportMedTransferReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->model('report/med_transfer_report');
		$this->document->setTitle('Medicine Transfer Report');

		$this->getList();
	}

	protected function getList() {
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;

		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		if (isset($this->request->get['filter_inward'])) {
			$filter_inward = $this->request->get['filter_inward'];
		} else {
			$filter_inward = null;
		}

		if (isset($this->request->get['filter_order_no'])) {
			$filter_order_no = $this->request->get['filter_order_no'];
		} else {
			$filter_order_no = null;
		}

		if (isset($this->request->get['filter_medicine'])) {
			$filter_medicine = $this->request->get['filter_medicine'];
		} else {
			$filter_medicine = null;
		}

		if (isset($this->request->get['filter_doctor'])) {
			$filter_doctor = $this->request->get['filter_doctor'];
		} else {
			$filter_doctor = null;
		}

		if (isset($this->request->get['filter_hospital'])) {
			$filter_hospital = $this->request->get['filter_hospital'];
		} else {
			$filter_hospital = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = date('d-m-Y');
		}

		if (isset($this->request->get['filter_dates'])) {
			$filter_dates = $this->request->get['filter_dates'];
		} else {
			$filter_dates = date('d-m-Y');
		}

		if (isset($this->request->get['filter_productsort'])) {
			$filter_productsort = $this->request->get['filter_productsort'];
		} else {
			$filter_productsort = null;
		}

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$filter_inward_category = $this->request->get['filter_inward_category'];
		} else {
			$filter_inward_category = null;
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$filter_inward_category_id = $this->request->get['filter_inward_category_id'];
		} else {
			$filter_inward_category_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_doctor'])) {
			$url .= '&filter_doctor=' . urlencode(html_entity_decode($this->request->get['filter_doctor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_hospital'])) {
			$url .= '&filter_hospital=' . urlencode(html_entity_decode($this->request->get['filter_hospital'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}


		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token']);
		$data['add'] = $this->url->link('catalog/inward/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['stock'] = $this->url->link('catalog/inward/stock', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/inward/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['medtransfer'] = array();

		$filter_data = array(
			'filter_inward'	  => $filter_inward,
			'filter_order_no'	  => $filter_order_no,
			'filter_medicine'	  => $filter_medicine,
			'filter_doctor'	  => $filter_doctor,
			'filter_hospital'	  => $filter_hospital,
			'filter_date'	  => $filter_date,
			'filter_dates'	  => $filter_dates,
			'filter_order_id'  => $filter_order_id,
			'filter_inward_category'	  => $filter_inward_category,
			'filter_inward_category_id'  => $filter_inward_category_id,
			'filter_productsort'	=> $filter_productsort,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
			//echo '<pre>';
			 //print_r($filter_data);
			//exit;

		$inward_total = $this->model_report_med_transfer_report->getTotalmedtransfer($filter_data);

		$results = $this->model_report_med_transfer_report->getmedtransfer($filter_data);
		$user_group_id = $this->user->getGroupId();

		foreach ($results as $result) {
			$doctors = $this->db->query("SELECT doctor_name FROM doctor WHERE id = '".$result['child_doctor_id']."' ");
			if ($doctors->num_rows > 0) {
				$doctor = $doctors->row['doctor_name'];
			} else {
				$doctor = '';
			}
			if($result['expire_date'] != '1970-01-01' && $result['expire_date'] != '0000-00-00'){
				$expire_date= date('d-m-Y', strtotime($result['expire_date']));
			} else {
				$expire_date = '';
			}
			$entry_dates = $this->db->query("SELECT entry_date FROM medicine_transfer WHERE id = '".$result['parent_id']."' ");
			if ($entry_dates->num_rows > 0) {
				$entry_date = $entry_dates->row['entry_date'];
			} else {
				$entry_date = '';
			}

			$med_types = $this->db->query("SELECT med_type,unit FROM medicine WHERE med_name LIKE '".$result['product_name']."%' ");
			if ($med_types->num_rows > 0) {
				$med_type = $med_types->row['med_type'];
				$unit = $med_types->row['unit'];
			} else {
				$med_type = '';
				$unit = '';
			}

			$data['medtransfer'][] = array(
				'issue_no'=> $result['issue_no'],
				'entry_date'=> date('d-m-Y', strtotime($entry_date)),
				'clinic'=> $result['parent_doctor_name'],
				'doctor_name'=> $doctor,
				'product_name'=> $result['product_name'],
				'med_type'=> $med_type,
				'unit'=> $unit,
				'expire_date'=> $expire_date,
				'product_qty'=> $result['product_qty'],
			);
		}

		$data['name'] = array();

		$doctor_data = $this->db->query("SELECT id,doctor_name FROM doctor WHERE isActive = 'Active' ")->rows;
		foreach ($doctor_data as $value) {
			$data['name'][] = array(
				'names' => $value['doctor_name'],
				'id' => $value['id']
			);
		}

		$data['hospital'] = array();

		$hospital_data = $this->db->query("SELECT parent FROM doctor WHERE isActive = 'Active' ")->rows;
		foreach ($hospital_data as $value) {
			$data['hospital'][] = array(
				'hospitals' => $value['parent']
			);
		}

		

		$data['user_group_id'] = $this->user->getGroupId();
		//$data['user_type'] = $this->user->getUserType();	
		$data['base_link_1'] = $this->url->link('catalog/inward/edit', 'token=' . $this->session->data['token']);
		$data['base_link'] = $this->url->link('catalog/inward/edit', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_doctor'])) {
			$url .= '&filter_doctor=' . urlencode(html_entity_decode($this->request->get['filter_doctor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_hospital'])) {
			$url .= '&filter_hospital=' . urlencode(html_entity_decode($this->request->get['filter_hospital'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . '&sort=inward_name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_doctor'])) {
			$url .= '&filter_doctor=' . urlencode(html_entity_decode($this->request->get['filter_doctor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_hospital'])) {
			$url .= '&filter_hospital=' . urlencode(html_entity_decode($this->request->get['filter_hospital'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $inward_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($inward_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($inward_total - $this->config->get('config_limit_admin'))) ? $inward_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $inward_total, ceil($inward_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_inward_category'] = $filter_inward_category;
		$data['filter_inward_category_id'] = $filter_inward_category_id;
		$data['filter_inward'] = $filter_inward;
		$data['filter_order_no'] = $filter_order_no;
		$data['filter_medicine'] = $filter_medicine;
		$data['filter_doctor'] = $filter_doctor;
		$data['filter_hospital'] = $filter_hospital;
		$data['filter_date'] = $filter_date;
		$data['filter_dates'] = $filter_dates;
		$data['filter_productsort'] = $filter_productsort;
		$data['filter_order_id'] = $filter_order_id;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];
		
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		$this->response->setOutput($this->load->view('report/med_transfer_report', $data));
	}

	public function autocomplete() {
		$json = array();
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		
		if (isset($this->request->get['filter_medicine'])) {
			
			$sql = "SELECT * FROM `medicine` WHERE 1=1 ";

			if (!empty($this->request->get['filter_medicine'])) {
				$sql .= " AND med_name LIKE '%" . $this->db->escape($this->request->get['filter_medicine']) . "%'";
			}

			$results = $this->db->query($sql)->rows;

			foreach ($results as $result) {
				$json[] = array(
					'med_name' => $result['med_name'],
				);
			}
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteDoctor() {
		$json = array();
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		
		if (isset($this->request->get['filter_doctor'])) {
			
			$sql = "SELECT * FROM `doctor` WHERE 1=1 ";

			if (!empty($this->request->get['filter_doctor'])) {
				$sql .= " AND doctor_name LIKE '%" . $this->db->escape($this->request->get['filter_doctor']) . "%'";
			}

			$results = $this->db->query($sql)->rows;

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
				);
			}
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function prints() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$data['medtransfer'] = array();
		if (isset($this->request->get['filter_medicine'])) {
			$medicine_name = $this->request->get['filter_medicine'];
		} else {
			$medicine_name = '';
		}
		if (isset($this->request->get['filter_doctor'])) {
			$doctor = $this->request->get['filter_doctor'];
		} else {
			$doctor = '';
		}
		if (isset($this->request->get['filter_hospital'])) {
			$hospital = $this->request->get['filter_hospital'];
		} else {
			$hospital = '';
		}
		if (isset($this->request->get['filter_date'])) {
			$from_date = $this->request->get['filter_date'];
		} else {
			$from_date = '';
		}
		if (isset($this->request->get['filter_dates'])) {
			$to_date = $this->request->get['filter_dates'];
		} else {
			$to_date = '';
		}

		$sql = "SELECT * FROM medicine_transfer m LEFT JOIN medicine_trans_items mi ON (m.id = mi.parent_id)";

		$sql .= " WHERE 1=1";

		if (!empty($medicine_name)) {
			$sql .= " AND mi.product_id = '" . $this->db->escape($medicine_name) . "'";
		}

		if (!empty($doctor)) {
			$sql .= " AND m.child_doctor_id = '" . $this->db->escape($doctor) . "'";
		}

		if (!empty($hospital)) {
			$sql .= " AND m.parent_doctor_name = '" . $this->db->escape($hospital) . "'";
		}

		if ($from_date != '' && $to_date != '') {
			$sql .= " AND m.entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($from_date))) . "'";
			$sql .= " AND m.entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($to_date))) . "'";
		}

		// echo $sql;
		// exit;

		$result = $this->db->query($sql)->rows;

		foreach ($result as $pvalue) {

			$doctors = $this->db->query("SELECT doctor_name FROM doctor WHERE id = '".$pvalue['child_doctor_id']."' ");
			if ($doctors->num_rows > 0) {
				$doctor = $doctors->row['doctor_name'];
			} else {
				$doctor = '';
			}
			if($pvalue['expire_date'] != '1970-01-01' && $pvalue['expire_date'] != '0000-00-00'){
				$expire_date= date('d-m-Y', strtotime($pvalue['expire_date']));
			} else {
				$expire_date = '';
			}

			$med_types = $this->db->query("SELECT med_type,unit FROM medicine WHERE med_name LIKE '".$pvalue['product_name']."%' ");
			if ($med_types->num_rows > 0) {
				$med_type = $med_types->row['med_type'];
				$unit = $med_types->row['unit'];
			} else {
				$med_type = '';
				$unit = '';
			}

			$data['medtransfer'][] = array(
				'issue_no'=> $pvalue['issue_no'],
				'entry_date'=> date('d-m-Y', strtotime($pvalue['entry_date'])),
				'clinic'=> $pvalue['parent_doctor_name'],
				'doctor_name'=> $doctor,
				'product_name'=> $pvalue['product_name'],
				'med_type'=> $med_type,
				'unit'=> $unit,
				'expire_date'=> $expire_date,
				'product_qty'=> $pvalue['product_qty'],
			);

			$data['from_date'] = $from_date;
			$data['to_date'] = $to_date;
		}

		// echo'<pre>';
		// print_r($data['inwards']);
		// exit;

		$html = $this->load->view('report/med_transfer_html', $data);
		//echo $html;exit;
		$filename = 'Medicine Transfer Report.html';
		//file_put_contents(DIR_DOWNLOAD.Indent, $html);
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;
		$this->response->setOutput($this->load->view('report/med_transfer_report', $data));
	}
}
