<?php
class Controllerreportreporthorseundertakingcharge extends Controller {
	public function index() {
		$this->load->language('report/report_horse_undertaking_charge');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_hourse_name'])) {
			$filter_hourse_name = $this->request->get['filter_hourse_name'];
		} else {
			$filter_hourse_name = '';
		}

		if (isset($this->request->get['filter_hourse_id'])) {
			$filter_hourse_id = $this->request->get['filter_hourse_id'];
		} else {
			$filter_hourse_id = '';
		}

		if (isset($this->request->get['filter_trainer_name'])) {
			$filter_trainer_name = $this->request->get['filter_trainer_name'];
		} else {
			$filter_trainer_name = '';
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$filter_trainer_id = $this->request->get['filter_trainer_id'];
		} else {
			$filter_trainer_id = '';
		}


		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}


		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_hourse_name']) ) {
			$url .= '&filter_hourse_name=' . $this->request->get['filter_hourse_name'];
		}

		if (isset($this->request->get['filter_hourse_id']) ) {
			$url .= '&filter_hourse_id=' . $this->request->get['filter_hourse_id'];
		}

		if (isset($this->request->get['filter_trainer_name']) ) {
			$url .= '&filter_trainer_name=' . $this->request->get['filter_trainer_name'];
		}

		if (isset($this->request->get['filter_trainer_id']) ) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/horse_undertaking_charge', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('report/horse_undertaking_charge');

		$data['undertakingcharges_datas'] = array();

		$filter_data = array(
			'filter_date_end'	     => $filter_date_end,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin'),
			'filter_hourse_name' => $filter_hourse_name,
			'filter_hourse_id' => $filter_hourse_id,
			'filter_trainer_name' => $filter_trainer_name,
			'filter_trainer_id'	=>	$filter_trainer_id
		);

		$order_total = $this->model_report_horse_undertaking_charge->getTotalUndertakingChareges($filter_data);

		$results = $this->model_report_horse_undertaking_charge->getUndertakingChareges($filter_data);

		foreach ($results as $result) {
			if($result['left_date_of_charge'] == '0000-00-00' || $result['left_date_of_charge'] == '1970-01-01'){
				$left_date_of_charge = '';
			} else {
				$left_date_of_charge = date('d-m-Y', strtotime($result['left_date_of_charge']));
			}

			if($result['date_of_charge'] == '0000-00-00' || $result['date_of_charge'] == '1970-01-01'){
				$date_of_charge = '';
			} else {
				$date_of_charge = date('d-m-Y', strtotime($result['date_of_charge']));
			}
			$current_date = date('Y-m-d 00:01:00');
			$left_date_of_charge_for_interval  = $left_date_of_charge.' 00:01:00';
			$start_date = new DateTime($current_date);
			$since_start = $start_date->diff(new DateTime($left_date_of_charge_for_interval));
			$getintrval_left_Charge_date = $since_start->days;
			
			$horse_name_sql = "SELECT official_name FROM `horse1` WHERE horseseq= '".$result['horse_id']."' ";
			$horse_name = $this->db->query($horse_name_sql);
			if($horse_name->num_rows > 0){
				$final_horse_name = $horse_name->row['official_name'];			
			} else {
				$final_horse_name = '';
			}
			$data['undertakingcharges_datas'][] = array(
				'horse_name' => $final_horse_name ,
				'date_of_charge'     => $date_of_charge,
				'left_date_of_charge'   =>$left_date_of_charge,
				'trainer_name'   => $result['trainer_name'],
				'getintrval_left_Charge_date'   => $getintrval_left_Charge_date,
				
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_date_start'] = $this->language->get('column_date_start');
		$data['column_date_end'] = $this->language->get('column_date_end');
		$data['column_orders'] = $this->language->get('column_orders');
		$data['column_products'] = $this->language->get('column_products');
		$data['column_tax'] = $this->language->get('column_tax');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_group'] = $this->language->get('entry_group');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/report_horse_undertaking_charge', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));
		
		$data['filter_date_end'] = $filter_date_end;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['filter_hourse_name'] = $filter_hourse_name;
		$data['filterHorseId'] = $filter_hourse_id;
		$data['filter_trainer_name'] = $filter_trainer_name;
		$data['filter_trainer_id'] = $filter_trainer_id;

		$this->response->setOutput($this->load->view('report/report_horse_undertaking_charge', $data));
	}

	public function autocompleteHorse() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['horse_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesAuto($this->request->get['horse_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'horse_id' => $result['horseseq'],
						'horse_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		/*echo '<pre>';print_r($json);
			exit;*/
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['horse_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}
}