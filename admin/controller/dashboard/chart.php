<?php
class ControllerDashboardChart extends Controller {
	public function index() {

		$this->load->library('Calender');
		$calenders = new Calender($this->registry);

		if(isset($_POST['txtmonth'])) {
			$month = $_POST['txtmonth'];
		} elseif (isset($_GET['month'])) {
			$month = $_GET['month'];
		} else {
			$month = date('m');
		}


		if(isset($_POST['txtyear'])) {
			$year = (int) $_POST['txtyear'];
		} elseif (isset($_GET['year'])) {
			$year = (int) $_GET['year'];
		} else {
			$year = (int) date('Y');
		}

		//echo $year;exit;

		unset($_POST['txtyear']);
		unset($_POST['txtmonth']);
		unset($_GET['year']);
		unset($_GET['month']);
		//echo $year;exit;
		/* date settings */
		//$month = (int) (isset($_GET['month']) ? $_GET['month'] : date('m'));
		//$year = (int)  (isset($_GET['year']) ? $_GET['year'] : date('Y'));
		// if(strlen($month) == 1){
		// 	$month = '0'.$month;
		// } 
		
		/* select month control */
		$select_month_control = '<select style = "color : black" name="month" id="month">';
		for($x = 1; $x <= 12; $x++) {
			$select_month_control.= '<option value="'.$x.'"'.($x != $month ? '' : ' selected="selected"').'>'.date('F',mktime(0,0,0,$x,1,$year)).'</option>';
		}
		$select_month_control.= '</select>';

		/* select year control */
		$year_range = 26;
		//echo $year;exit;
		$select_year_control = '<select style = "color : black" name="year" id="year">';
		for($x = 2000; $x <= 2022; $x++) {
			//echo $x;
			$select_year_control.= '<option value="'.$x.'"'.($x != $year ? '' : ' selected="selected"').'>'.$x.'</option>';
		}
		//exit;
		$select_year_control.= '</select>';

		$nmonth = HTTP_SERVER.'index.php?route=common/dashboard&token='.$this->session->data['token'].'&month='.($month != 12 ? $month + 1 : 1).'&year='.($month != 12 ? $year : $year + 1);
		/* "next month" control */
		$next_month_link = '<a style = "color : black" href="'.$nmonth.'" class="control">Next Month &gt;&gt;</a>';

		$pmonth = HTTP_SERVER.'index.php?route=common/dashboard&token='.$this->session->data['token'].'&month='.($month != 12 ? $month - 1 : 1).'&year='.($month != 12 ? $year : $year - 1);
		/* "previous month" control */
		$previous_month_link = '<a style = "color : black" href="'.$pmonth.'" class="control">&lt;&lt; 	Previous Month</a>';

		$hiddenf = '<input type="hidden" name = "txtmonth" id = "txtmonth" value = "'.$month.'"><input type="hidden" name = "txtyear" id = "txtyear" value = "'.$year.'">';

		$action = HTTP_SERVER.'index.php?route=common/dashboard&token='.$this->session->data['token'];
		//$action = HTTP_SERVER.'index.php?route=dashboard/chart&month='.($month).'&year='.($year).'&token='.$this->session->data['token'].'';
		/* bringing the controls together */
		$controls = '<form method="post" action = "'.$action.'">'.$select_month_control.'&nbsp;&nbsp;'.$select_year_control.$hiddenf.'&nbsp;&nbsp;<input type="submit" name="submit" value="Go" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>'.$previous_month_link.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$next_month_link.' <br><br></form>';

		/* get all events for the given month */
		$events = array();

		//$events = $this->model_sale_order->getevents(); 
		
		 // $row = array('title' =>'event1');
		 // $row1 = array('title' =>'event2');
		 // $events['2013-10-5'][] = $row;
		 // $events['2013-10-6'][] = $row1;
		// echo '<pre>';
		// print_r($events);
		// exit;
		$html = '<div style="float:center; padding-left:31%;">'.$controls.'</div>';
		$html .= '<h2 style="float:center; padding-left:35%;font-size: xx-large;">'.date('F',mktime(0,0,0,$month,1,$year)).' '.$year.'</h2>';
		$html .= '<div style="clear:both;"></div>';
		$html .= '<div style="float:center; border: solid; border-color: cornflowerblue; padding: inherit; border-width: thin;">'.$calenders->draw_calendar($month,$year,$events).'</div>';
		$html .= '<br /><br />';
		
		// echo '<pre>';
		// print_r($html);
		// exit;
		$data['calenders'] = $html;
		$this->load->language('dashboard/chart');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_day'] = $this->language->get('text_day');
		$data['text_week'] = $this->language->get('text_week');
		$data['text_month'] = $this->language->get('text_month');
		$data['text_year'] = $this->language->get('text_year');
		$data['text_view'] = $this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

		return $this->load->view('dashboard/chart', $data);
	}

	public function chart() {
		$this->load->language('dashboard/chart');

		$json = array();

		$this->load->model('report/sale');
		$this->load->model('report/customer');

		$json['order'] = array();
		$json['customer'] = array();
		$json['xaxis'] = array();

		$json['order']['label'] = '';//$this->language->get('text_order');
		$json['customer']['label'] = '';//$this->language->get('text_customer');
		$json['order']['data'] = array();
		$json['customer']['data'] = array();

		if (isset($this->request->get['range'])) {
			$range = $this->request->get['range'];
		} else {
			$range = 'day';
		}

		switch ($range) {
			default:
			case 'day':
				$results = $this->model_report_sale->getTotalOrdersByDay();

				foreach ($results as $key => $value) {
					$json['order']['data'][] = array($key, $value['total']);
				}

				$results = $this->model_report_customer->getTotalCustomersByDay();

				foreach ($results as $key => $value) {
					$json['customer']['data'][] = array($key, $value['total']);
				}

				for ($i = 0; $i < 24; $i++) {
					$json['xaxis'][] = array($i, $i);
				}
				break;
			case 'week':
				$results = $this->model_report_sale->getTotalOrdersByWeek();

				foreach ($results as $key => $value) {
					$json['order']['data'][] = array($key, $value['total']);
				}

				$results = $this->model_report_customer->getTotalCustomersByWeek();

				foreach ($results as $key => $value) {
					$json['customer']['data'][] = array($key, $value['total']);
				}

				$date_start = strtotime('-' . date('w') . ' days');

				for ($i = 0; $i < 7; $i++) {
					$date = date('Y-m-d', $date_start + ($i * 86400));

					$json['xaxis'][] = array(date('w', strtotime($date)), date('D', strtotime($date)));
				}
				break;
			case 'month':
				$results = $this->model_report_sale->getTotalOrdersByMonth();

				foreach ($results as $key => $value) {
					$json['order']['data'][] = array($key, $value['total']);
				}

				$results = $this->model_report_customer->getTotalCustomersByMonth();

				foreach ($results as $key => $value) {
					$json['customer']['data'][] = array($key, $value['total']);
				}

				for ($i = 1; $i <= date('t'); $i++) {
					$date = date('Y') . '-' . date('m') . '-' . $i;

					$json['xaxis'][] = array(date('j', strtotime($date)), date('d', strtotime($date)));
				}
				break;
			case 'year':
				$results = $this->model_report_sale->getTotalOrdersByYear();

				foreach ($results as $key => $value) {
					$json['order']['data'][] = array($key, $value['total']);
				}

				$results = $this->model_report_customer->getTotalCustomersByYear();

				foreach ($results as $key => $value) {
					$json['customer']['data'][] = array($key, $value['total']);
				}

				for ($i = 1; $i <= 12; $i++) {
					$json['xaxis'][] = array($i, date('M', mktime(0, 0, 0, $i)));
				}
				break;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}