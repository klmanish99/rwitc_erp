<?php
class ControllerDashboardOnline extends Controller {
	public function index() {
		$this->load->language('dashboard/online');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_view'] = $this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

		// Total Orders
		$this->load->model('catalog/jockey');

		// Customers Online
		$online_total = $this->model_catalog_jockey->getTotalCategoriess();

		$data['total'] = $online_total;

		$data['jockey'] = $this->url->link('catalog/jockey', 'token=' . $this->session->data['token'], true);

		return $this->load->view('dashboard/online', $data);
	}
}