<?php
class ControllerDashboardHorseGlance extends Controller {
	public function index() {
		$this->load->language('dashboard/order');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_view'] = $this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

		// Total Orders
		$this->load->model('catalog/horse');

		$order_total = $this->model_catalog_horse->getTotalCategories();

		$data['total'] = $order_total;

		$data['horse_at_glance'] = $this->url->link('catalog/horse_at_glance', 'token=' . $this->session->data['token'], true);

		return $this->load->view('dashboard/horse_glance', $data);
	}
}