<?php
class Controllerdashboardreporthorseundertakingcharge extends Controller {
	public function index() {
		$this->load->language('dashboard/order');

		$data['token'] = $this->session->data['token'];

		// Total Orders
		$this->load->model('report/horse_undertaking_charge');

		/*$today = $this->model_sale_order->getTotalOrders(array('filter_date_added' => date('Y-m-d', strtotime('-1 day'))));

		$yesterday = $this->model_sale_order->getTotalOrders(array('filter_date_added' => date('Y-m-d', strtotime('-2 day'))));

		$difference = $today - $yesterday;

		if ($difference && $today) {
			$data['percentage'] = round(($difference / $today) * 100);
		} else {
			$data['percentage'] = 0;
		}*/

		$data['total_undertaking_charge'] = 0;
		$results = $this->model_report_horse_undertaking_charge->getUndertakingChareges();
		$i = 0;
		foreach ($results as $result) {
			if($result['left_date_of_charge'] == '0000-00-00' || $result['left_date_of_charge'] == '1970-01-01'){
				$left_date_of_charge = '';
			} else {
				$left_date_of_charge = date('d-m-Y', strtotime($result['left_date_of_charge']));
			}
			
			$current_date = date('Y-m-d 00:01:00');
			$left_date_of_charge_for_interval  = $left_date_of_charge.' 00:01:00';
			$start_date = new DateTime($current_date);
			$since_start = $start_date->diff(new DateTime($left_date_of_charge_for_interval));
			$getintrval_left_Charge_date = $since_start->days;
			

			if($getintrval_left_Charge_date >= 15){
				$i++;
			}
			
		}
		if($i == 0){
			$data['total_undertaking_charge'] = $i;
			$data['color1']= 'background: linear-gradient(45deg, #28a745, #1c5c49);';
			$data['color'] ='background: linear-gradient(109deg, #28a745, #1c5c49);height: 25px';
			$data['color2'] ='background: linear-gradient(100deg, #28a745, #1c5c49);';
		} else {
			$data['color1'] ='background: linear-gradient(45deg, #c51111, #DC281E);';
			$data['color'] ='background: linear-gradient(115deg, #c51111, #DC281E);height: 25px';
			$data['color2'] ='background: linear-gradient(100deg, #c51111, #DC281E);';
			$data['total_undertaking_charge'] = $i+1;
		}

		

		/*if ($order_total > 1000000000000) {
			$data['total'] = round($order_total / 1000000000000, 1) . 'T';
		} elseif ($order_total > 1000000000) {
			$data['total'] = round($order_total / 1000000000, 1) . 'B';
		} elseif ($order_total > 1000000) {
			$data['total'] = round($order_total / 1000000, 1) . 'M';
		} elseif ($order_total > 1000) {
			$data['total'] = round($order_total / 1000, 1) . 'K';
		} else {
			$data['total'] = $order_total;
		}*/

		$data['undertaking_charge_report_link'] = $this->url->link('report/report_horse_undertaking_charge', 'token=' . $this->session->data['token'], true);

		return $this->load->view('dashboard/report_horse_undertaking_charge', $data);
	}
}