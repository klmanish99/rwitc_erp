<?php
class ControllerDashboardJockeyGlance extends Controller {
	public function index() {
		$this->load->language('dashboard/order');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_view'] = $this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

		// Total Orders
		$this->load->model('catalog/horse');

		$order_total = $this->model_catalog_horse->getTotalCategories();

		$data['total'] = $order_total;

		$data['jockey_at_glance'] = $this->url->link('catalog/jockey_at_glance', 'token=' . $this->session->data['token'], true);

		return $this->load->view('dashboard/jockey_glance', $data);
	}
}