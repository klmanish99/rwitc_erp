<?php
class ControllerTransactionstacks extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/stacks');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stacks');
		$this->getList();
	}

	public function add() {
		$this->load->language('transaction/stacks');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stacks');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_transaction_stacks->addstacks($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');


			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/stacks', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('transaction/stacks');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stacks');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
			$this->model_transaction_stacks->editstacks($this->request->get['stacks_id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/stacks', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('transaction/stacks');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stacks');
		if (isset($this->request->post['selected'])/* && $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $stacks_id) {
				$this->model_transaction_stacks->deletestacks($stacks_id);
			}
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/stacks', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getList();
	}

	public function repair() {
		$this->load->language('transaction/stacks');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stacks');
		if ($this->validateRepair()) {
			$this->model_transaction_stacks->repairCategories();
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('transaction/stacks', 'token=' . $this->session->data['token'], true));
		}
		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}


		if (isset($this->request->get['filter_race_name'])) {
			$data['filter_race_name'] =$this->request->get['filter_race_name'];
		} else{
			$data['filter_race_name']  ="";
		}

		if(isset($this->request->get['filter_race_date'])){
			$data['filter_race_date'] = $this ->request->get['filter_race_date'];
		}
		 else{
			$data['filter_race_date'] = '';
		}


		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/stacks', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/stacks/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/stacks/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/stacks/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_race_name' => $data['filter_race_name'],
			'filter_race_date' => $data['filter_race_date']
		);

		$category_total = $this->model_transaction_stacks->getTotalCategories($filter_data);
		$results = $this->model_transaction_stacks->getstacks($filter_data);
		foreach ($results as $result) {
			$data['stacksdatas'][] = array(
				'stacks_id' 	  	=> $result['stacks_id'],
				'race_type'	  	=> $result['race_type'],
				'class'	  	=> $result['class'],
				'edit'        	=> $this->url->link('transaction/stacks/edit', 'token=' . $this->session->data['token'] . '&stacks_id=' . $result['stacks_id'] . $url, true),
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/stacks', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/stacks', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('transaction/stacks', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/stacks_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['stacks_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');
		
		$data['evening_races'] = array(
			'No'	=> 'No',
			'Yes'	=> 'Yes'
		);

		$data['types'] = array(
			'No'	=> 'No',
			'Yes'	=> 'Yes'
		);

		
		$data['race_types'] = array(
			'Handicap Race'  =>'Handicap Race',
			'Term Race'  => 'Term Race',
		);

		$data['stacks_types'] = array(
			'Winner'  =>'Winner',
			'Second'  =>'Second',
			'Third'  =>'Third',
			'Fourth'  =>'Fourth',
			'Fifth'  =>'Fifth',
		);		

		$data['type']= array(
			'No'  =>'No',
			'Yes'  =>'Yes',
		);
		
		$data['classs']= array(
			'Class I'  =>'Class I',
			'Class II'  =>'Class II',
			'Class III'  =>'Class III',
			'Class IV'  =>'Class IV',
			'Class V'  =>'Class V',
			'Mix Class'  =>'Mix Class',
		);

		$data['positionss']= array(
			'1'  =>'1',
			'2'  =>'2',
			'3'  =>'3',
			'4'  =>'4',
			'5'  =>'5',
			'6'  =>'6',
			'7'  =>'7',
			'8'  =>'8',
			'9'  =>'9',
			'10'  =>'10',

		);

		$data['terms']= array(
			'Grade I'  =>'Grade I',
			'Grade II'  =>'Grade II',
			'Grade III'  =>'Grade III',
			'Mix Grade'  =>'Mix Grade',
		);
		 

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->error['valierr_race_date'])) {
			$data['valierr_race_date'] = $this->error['valierr_race_date'];
		}
		if (isset($this->error['valierr_greater_race_date'])) {
			$data['valierr_greater_race_date'] = $this->error['valierr_greater_race_date'];
		}
		if (isset($this->error['valierr_greater_handicaps_date'])) {
			$data['valierr_greater_handicaps_date'] = $this->error['valierr_greater_handicaps_date'];
		}
		if (isset($this->error['valierr_greater_acceptance_date'])) {
			$data['valierr_greater_acceptance_date'] = $this->error['valierr_greater_acceptance_date'];
		}
		if (isset($this->error['valierr_greater_declaration_date'])) {
			$data['valierr_greater_declaration_date'] = $this->error['valierr_greater_declaration_date'];
		}
		if (isset($this->error['valierr_race_name'])) {
			$data['valierr_race_name'] = $this->error['valierr_race_name'];
		}

		if (isset($this->error['gst_number'])) {
			$data['error_gst_number'] = $this->error['gst_number'];
		} else {
			$data['error_gst_number'] = '';
		}
		
		if (isset($this->error['sharedColor'])) {
			$data['error_sharedColor'] = $this->error['sharedColor'];
		} else {
			$data['error_sharedColor'] = '';
		}

		//echo "<pre>";print_r($this->error['error_race']);exit;
		if (isset($this->error['error_race'])) {
			$data['error_race'] = $this->error['error_race'];
		} else {
			$data['error_race'] = '';
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/stacks', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['stacks_id'])) {
			$data['action'] = $this->url->link('transaction/stacks/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/stacks/edit', 'token=' . $this->session->data['token'] . '&stacks_id=' . $this->request->get['stacks_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('transaction/stacks', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['stacks_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_transaction_stacks->getCategory($this->request->get['stacks_id']);

		}
		
		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();



		if (isset($this->request->post['stacks_id'])) {
			$data['stacks_id'] = $this->request->post['stacks_id'];
		} elseif (!empty($category_info)) {
			$data['stacks_id'] = $category_info['stacks_id'];
		} else {
			$data['stacks_id'] = '';
		}
		
		if (isset($this->request->post['grade'])) {
			$data['grade'] = $this->request->post['grade'];
		} elseif (!empty($category_info)) {
			$data['grade'] = $category_info['class'];
		} else {
			$data['grade'] = '';
		}

		if (isset($this->request->post['class'])) {
			$data['class'] = $this->request->post['class'];
		} elseif (!empty($category_info)) {
			$data['class'] = $category_info['class'];
		} else {
			$data['class'] = '';
		}

		
		if (isset($this->request->post['race_type'])) {
			$data['race_type'] = $this->request->post['race_type'];
		} elseif (!empty($category_info)) {
			$data['race_type'] = $category_info['race_type'];
		} else {
			$data['race_type'] = '';
		}

		if (isset($this->request->post['contact_datas'])) {
			$data['contact_datas'] = $this->request->post['contact_datas'];
		} elseif (!empty($category_info)) {
			$contact_datas = $this->model_transaction_stacks->getcontact_data($category_info['stacks_id']);
			$data['contact_datas'] = $contact_datas;
		} else {
			$data['contact_datas'] = array();
		}
		

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/stacks_form', $data));
	}
	

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/stacks')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		if(strlen($this->request->post['race_type']) == '') { 
			$this->error['error_race'] = 'Please Enter Valid Race Type';
		}

		foreach ($this->request->post['contact_datas'] as $key => $value) {
			if ((utf8_strlen($value['incentive']) == '') ) {
				$this->error['warning'] = 'Please Enter valid Incentive';
			}
			if ((utf8_strlen($value['total']) == '') ) {
				$this->error['warning'] = 'Please Enter valid Total';
			} 
			if ((utf8_strlen($value['jockey']) == '') ) {
				$this->error['warning'] = 'Please Enter valid Jockey';
			} 
			if ((utf8_strlen($value['trainer']) == '') ) {
				$this->error['warning'] = 'Please Enter valid Trainer';
			}
			if ((utf8_strlen($value['owner']) == '') ) {
				$this->error['warning'] = 'Please Enter valid Owner';
			} 
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'transaction/stacks')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'transaction/stacks')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}


	public function autocompleteracename() {
        $json = array();
        if (isset($this->request->get['race_name'])) {
            $this->load->model('transaction/stacks');
            $results = $this->model_transaction_stacks->getRacenameauto($this->request->get['race_name']);
            if($results){
                foreach ($results as $result) {
                    $json[] = array(
                        'race_name'        => strip_tags(html_entity_decode($result['race_name'], ENT_QUOTES, 'UTF-8')),
                       
                    );
                }
            }
        }
        $sort_order = array();
        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['race_name'];
            
        }
        array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }


	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('transaction/stacks');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'owner_Id'	  => $this->request->get['owner_Id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_stacks->getOwners($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'owner_id' => $result['id'],
					'owner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['owner_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
