<?php
class ControllerTransactionTrainerBan extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/trainer_ban');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/trainer_ban');
		$this->getList();
	}

	public function add() {
		$this->load->language('transaction/trainer_ban');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/trainer_ban');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
			// $this->model_transaction_trainer_ban->addProspectus($this->request->post);
			// $this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/trainer_ban', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}


	public function edit() {
		$this->load->language('transaction/trainer_ban');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/trainer_ban');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
			// $this->model_transaction_entries->editProspectus($this->request->get['pros_id'], $this->request->post);
			// $this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/trainer_ban', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}


	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_authority'])) {
			$data['filter_authority'] =$this->request->get['filter_authority'];
		} else{
			$data['filter_authority']  ="";
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$data['filter_trainer_id'] =$this->request->get['filter_trainer_id'];
		} else{
			$data['filter_trainer_id']  ="";
		}

		if (isset($this->request->get['filter_horse_id'])) {
			$data['filter_horse_id'] =$this->request->get['filter_horse_id'];
		} else{
			$data['filter_horse_id']  ="";
		}

		if (isset($this->request->get['filter_trainer_name'])) {
			$data['filter_trainer_name'] =$this->request->get['filter_trainer_name'];
		} else{
			$data['filter_trainer_name']  ="";
		}

		if (isset($this->request->get['filter_horse_name'])) {
			$data['filter_horse_name'] =$this->request->get['filter_horse_name'];
		} else{
			$data['filter_horse_name']  ="";
		}

		if(isset($this->request->get['filter_acceptance_date'])){
			$data['filter_acceptance_date'] = $this ->request->get['filter_acceptance_date'];
		}
		 else{
			$data['filter_acceptance_date'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/trainer_ban', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/trainer_ban/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/trainer_ban/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/trainer_ban/repair', 'token=' . $this->session->data['token'] . $url, true);


		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/trainer_ban/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/trainer_ban/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['arrival'] = $this->url->link('transaction/arrival_charges', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['authoritys'] = array(
			'Stipes'  => 'Stipes',
			'Stewards' => 'Stewards',
			'Board of Authority' => 'Board of Authority'
		);

		$data['clubs'] = array(
			'RWITC'  => 'RWITC',
		);

		$data['offencess'] = $this->db->query("SELECT * FROM `trainer_ban_offences` WHERE 1=1")->rows;
		$ban_datas = array();
		$sql = "SELECT * FROM `trainer_ban` WHERE 1=1 ";
		if($data['filter_authority'] != ''){
			if($data['filter_authority'] == 'Stipes') {
				$sql .= " AND reason_stewards = '' AND reason_board_of_authority = '' ";
			} elseif($data['filter_authority'] == 'Stewards'){
				$sql .= " AND reason_stewards <> '' AND reason_board_of_authority = '' ";
			} else{
				$sql .= " AND reason_stewards <> '' AND reason_board_of_authority <> '' ";
			}
		}

		if($data['filter_horse_id'] != '') {
			$sql .= " AND horse_id = '".$data['filter_horse_id']."' ";

		}

		if($data['filter_trainer_id'] != '') {
			$sql .= " AND trainer_id = '".$data['filter_trainer_id']."' ";
		}

		$sql .= " order by ban_id DESC ";
		$ban_dataz = $this->db->query($sql)->rows;
		foreach ($ban_dataz as $key => $value) {
			if($value['reason_board_of_authority'] != ""){
				$auth = 'Board of Authority';
			} elseif($value['reason_stewards'] != "") {
				$auth = 'Stewards';
			} else {
				$auth = 'Stipes';
			}

			if($value['board_of_authority_offences'] != ''){
                $offences_val = $value['board_of_authority_offences'];
            } elseif ($value['stewards_offences'] != '') {
                $offences_val = $value['stewards_offences'];
            } else {
                $offences_val = $value['offences'];
            }


            if($value['board_of_authority_fine'] != '' && $value['board_of_authority_fine'] != 0){
                $fine_val = $value['board_of_authority_fine'];
            } elseif ($value['stewards_fine'] != '' &&  $value['stewards_fine'] != 0) {
                $fine_val = $value['stewards_fine'];
            } else {
                $fine_val = $value['amount'];
            }
			$ban_datas[] = array(
				'trainer_code' => $value['trainer_code'],
				'trainer_name' => $value['trainer_name'],
				'club' 		  => $value['club'],
				'authority'   => $value['authority'],
				'offences'	  => $offences_val,
				'horse_name'  => $value['horse_name'],
				'race_no' 	  => $value['race_no'],
				'race_date'   => $value['race_date'],
				'fine'		  => $fine_val,
				'sus_from'	  => $value['start_date1'],
				'sus_to' 	  => $value['end_date1'],
				'auth' 		  => $auth,
				'edit_stipes'        => $this->url->link('transaction/trainer_ban/edit', 'token=' . $this->session->data['token'] . '&ban_id=' . $value['ban_id'] . '&auth=' . $auth . '&editby=stipes'  . $url, true),
				'edit_stewards'        => $this->url->link('transaction/trainer_ban/edit', 'token=' . $this->session->data['token'] . '&ban_id=' . $value['ban_id'] . '&auth=' . $auth . '&editby=stewards'  . $url, true),
				'edit_boa'        => $this->url->link('transaction/trainer_ban/edit', 'token=' . $this->session->data['token'] . '&ban_id=' . $value['ban_id'] . '&auth=' . $auth . '&editby=boa'  . $url, true),
			);
		}

		$data['ban_datas'] = $ban_datas;

		// echo'<pre>';
		// print_r($this->request->get);
		// exit;


		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_acceptance_date' => $data['filter_acceptance_date']
		);
		

		
		$horse_data = array();
		$is_filter = 0;
		

		if(isset($this->request->post['fromdate'])) {
			$data['fromdate'] = $this->request->post['fromdate'];
		} else {
			$data['fromdate'] = date('d-m-Y');
		}

		

		/*echo '<pre>';
		print_r($this->request->post);
		exit;
		*/
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/trainer_ban', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/trainer_ban', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$data['filter_authority'] = $data['filter_authority'];
		$data['filter_trainer_id'] = $data['filter_trainer_id'];
		$data['filter_horse_id'] = $data['filter_horse_id'];
		$data['filter_trainer_name'] = $data['filter_trainer_name'];
		$data['filter_horse_name'] = $data['filter_horse_name'];

		// /$data['charge_type'] = 

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/trainer_ban_list', $data));
	}

	protected function getForm() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_descp'] = $this->language->get('entry_descp');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_pack'] = $this->language->get('entry_pack');
		$data['entry_volume'] = $this->language->get('entry_volume');
		$data['entry_unit'] = $this->language->get('entry_unit');
		$data['entry_kg'] = $this->language->get('entry_kg');
		$data['entry_gm'] = $this->language->get('entry_gm');
		$data['entry_l'] = $this->language->get('entry_l');
		$data['entry_ml'] = $this->language->get('entry_ml');
		$data['entry_mg'] = $this->language->get('entry_mg');
		$data['entry_gst'] = $this->language->get('entry_gst');
		$data['entry_pur'] = $this->language->get('entry_purchase');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/trainer_ban', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['ban_id'])) {
			$data['action'] = $this->url->link('transaction/trainer_ban/add', 'token=' . $this->session->data['token'] . $url, true);
			$data['ban_id'] = 0;
			$data['auth_type'] = "";
			$data['editby'] = "";

		} else {
			$data['action'] = $this->url->link('transaction/trainer_ban/edit', 'token=' . $this->session->data['token'] . '&ban_id=' . $this->request->get['ban_id'] . $url, true);
			$data['ban_id'] = $this->request->get['ban_id'];
			$data['auth_type'] = $this->request->get['auth'];
			$data['editby'] = $this->request->get['editby'];

		}

		
		$data['cancel'] = $this->url->link('transaction/trainer_ban', 'token=' . $this->session->data['token'] . $url, true);
		

		if (isset($this->request->get['ban_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$ban_info = $this->model_transaction_trainer_ban->getBanData($this->request->get['ban_id']);
		}

		if($data['ban_id'] != 0 ){
			$data['authoritys'] = array(
				'Stipes'  => 'Stipes',
				'Stewards' => 'Stewards',
				'Board of Authority' => 'Board of Authority'
			);
		} else {
			$data['authoritys'] = array(
				'Stipes'  => 'Stipes',
			);
		}

		$data['clubs'] = array(
			'RWITC'  => 'RWITC',
		);

		$data['offencess'] = $this->db->query("SELECT * FROM `trainer_ban_offences` WHERE 1=1")->rows;


		// echo '<pre>';
		// print_r($category_info);
		// exit;
		

		if (isset($this->request->post['authority'])) {
			$data['authority'] = $this->request->post['authority'];
		} elseif (!empty($ban_info)) {
			$data['authority'] = $ban_info['authority'];
		} else {
			$data['authority'] = '';
		}

		if (isset($this->request->post['club'])) {
			$data['club'] = $this->request->post['club'];
		} elseif (!empty($ban_info)) {
			$data['club'] = $ban_info['club'];
		} else {
			$data['club'] = '';
		}

		if (isset($this->request->post['trainer_code'])) {
			$data['trainer_code'] = $this->request->post['trainer_code'];
		} elseif (!empty($ban_info)) {
			$data['trainer_code'] = $ban_info['trainer_code'];
		} else {
			$data['trainer_code'] = '';
		}

		if (isset($this->request->post['trainer_id'])) {
			$data['trainer_id'] = $this->request->post['trainer_id'];
		} elseif (!empty($ban_info)) {
			$data['trainer_id'] = $ban_info['trainer_id'];
		} else {
			$data['trainer_id'] = '';
		}

		if (isset($this->request->post['trainer_name'])) {
			$data['trainer_name'] = $this->request->post['trainer_name'];
		} elseif (!empty($ban_info)) {
			$data['trainer_name'] = $ban_info['trainer_name'];
		} else {
			$data['trainer_name'] = '';
		}

		if (isset($this->request->post['offences'])) {
			$data['offences'] = $this->request->post['offences'];
		} elseif (!empty($ban_info)) {
			if($ban_info['board_of_authority_offences'] != ''){
				$data['offences'] = $ban_info['board_of_authority_offences'];
			} elseif($ban_info['stewards_offences'] != ''){
				$data['offences'] = $ban_info['stewards_offences'];
			} else {
				$data['offences'] = $ban_info['offences'];
			}
		} else {
			$data['offences'] = '';
		}

		if (isset($this->request->post['horse_name'])) {
			$data['horse_name'] = $this->request->post['horse_name'];
		} elseif (!empty($ban_info)) {
			$data['horse_name'] = $ban_info['horse_name'];
		} else {
			$data['horse_name'] = '';
		}

		if (isset($this->request->post['horse_id'])) {
			$data['horse_id'] = $this->request->post['horse_id'];
		} elseif (!empty($ban_info)) {
			$data['horse_id'] = $ban_info['horse_id'];
		} else {
			$data['horse_id'] = '';
		}

		if (isset($this->request->post['race_no'])) {
			$data['race_no'] = $this->request->post['race_no'];
		} elseif (!empty($ban_info)) {
			$data['race_no'] = $ban_info['race_no'];
		} else {
			$data['race_no'] = '';
		}

		if (isset($this->request->post['race_date'])) {
			$data['race_date'] = $this->request->post['race_date'];
		} elseif (!empty($ban_info)) {
			$data['race_date'] = date('d-m-Y', strtotime($ban_info['race_date']));
		} else {
			$data['race_date'] = '';
		}

		if (isset($this->request->post['fine'])) {
			$data['fine'] = $this->request->post['fine'];
		} elseif (!empty($ban_info)) {
			if($ban_info['board_of_authority_fine'] != ''  && $ban_info['board_of_authority_fine'] != 0){
				$data['fine'] = $ban_info['board_of_authority_fine'];
			} elseif($ban_info['stewards_fine'] != '' && $ban_info['stewards_fine'] != 0){
				$data['fine'] = $ban_info['stewards_fine'];
			} else {
				$data['fine'] = $ban_info['amount'];
			}
		} else {
			$data['fine'] = '';
		}

		if (isset($this->request->post['suspension_from'])) {
			$data['sus_from'] = $this->request->post['suspension_from'];
		} elseif (!empty($ban_info)) {
			$data['sus_from'] = date('d-m-Y', strtotime($ban_info['start_date1']));
		} else {
			$data['sus_from'] = date('d-m-Y');
		}

		if (isset($this->request->post['suspension_to'])) {
			$data['sus_to'] = $this->request->post['suspension_to'];
		} elseif (!empty($ban_info)) {
			$data['sus_to'] = date('d-m-Y', strtotime($ban_info['end_date1']));
		} else {
			$data['sus_to'] = date('d-m-Y');
		}

		if (isset($this->request->post['wdr_lic'])) {
			$data['wdr_lic'] = $this->request->post['wdr_lic'];
		} elseif (!empty($ban_info)) {
			$data['wdr_lic'] = $ban_info['wdr_licence'];
		} else {
			$data['wdr_lic'] = '';
		}

		if (isset($this->request->post['ride_work'])) {
			$data['ride_work'] = $this->request->post['ride_work'];
		} elseif (!empty($ban_info)) {
			$data['ride_work'] = $ban_info['ride_work'];
		} else {
			$data['ride_work'] = '';
		}

		if (isset($this->request->post['mock_race'])) {
			$data['mock_race'] = $this->request->post['mock_race'];
		} elseif (!empty($ban_info)) {
			$data['mock_race'] = $ban_info['mock_race'];
		} else {
			$data['mock_race'] = '';
		}

		if (isset($this->request->post['remark'])) {
			$data['remark'] = $this->request->post['remark'];
		} elseif (!empty($ban_info)) {
			$data['remark'] = $ban_info['reason'];
		} else {
			$data['remark'] = '';
		}


		// echo'<pre>';
		// print_r($data['remark']);
		// exit;

		if (isset($this->request->post['remark_stewards'])) {
			$data['remark_stewards'] = $this->request->post['remark_stewards'];
		} elseif (!empty($ban_info)) {
			$data['remark_stewards'] = $ban_info['reason_stewards'];
		} else {
			$data['remark_stewards'] = '';
		}

		if (isset($this->request->post['remark_board_of_authority'])) {
			$data['remark_board_of_authority'] = $this->request->post['remark_board_of_authority'];
		} elseif (!empty($ban_info)) {
			$data['remark_board_of_authority'] = $ban_info['reason_board_of_authority'];
		} else {
			$data['remark_board_of_authority'] = '';
		}

		$data['user_log_grp_id'] = $this->user->getGroupId();

		$data['user_log_id'] = $this->user->getId();

		$data['token'] = $this->session->data['token'];

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/trainer_ban', $data));
	}


	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/trainer_ban')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	

	public function saveAllDatas() {
		// echo'<pre>';
		// print_r($this->request->post);
		// exit;
		$json = array();
		$e_trainer = 0;
		$e_horse = 0;
		$e_race = 0;
		$exist_trainer = $this->db->query("SELECT trainer_code FROM trainers WHERE trainer_code = '".$this->request->post['trainer_id']."'");
		$exist_race = $this->db->query("SELECT pros_id FROM prospectus WHERE pros_id = '".$this->request->post['race_no']."'");
		
		if($this->request->post['horse_name'] != ''){
			
			$exist_horse = $this->db->query("SELECT horseseq FROM horse1 WHERE horseseq = '".$this->request->post['horse_id']."'");
			if($exist_horse->num_rows == 0){
				$e_horse = 1;
			}
		}

		if($exist_trainer->num_rows == 0){
			$e_trainer = 1;
		}

		if($exist_race->num_rows == 0){
			$e_race = 1;
		}
		
		$json['status'] = 0;
		$json['e_horse'] = $e_horse;
		$json['e_trainer'] = $e_trainer;
		$json['e_race'] = $e_race;
		if($e_horse == 0 && $e_trainer == 0 && $e_race == 0)	{
			$wdr_lic = isset($this->request->post['wdr_lic']) ? $this->request->post['wdr_lic'] : 0;
			$ride_work = isset($this->request->post['ride_work'] ) ? $this->request->post['ride_work'] : 0 ; 
			$mock_race = isset($this->request->post['mock_race'] ) ? $this->request->post['mock_race'] : 0 ; 
			$remark_stewards = isset($this->request->post['remark_stewards'] ) ? $this->request->post['remark_stewards'] : "" ; 
			$remark_board_of_authority = isset($this->request->post['remark_board_of_authority'] ) ? $this->request->post['remark_board_of_authority'] : "" ; 

			if($this->request->get['ban_id'] != 0){
				if($this->request->get['editby'] == 'stipes'){
					$this->db->query("UPDATE trainer_ban SET 
						`authority` = '".$this->db->escape($this->request->post['authority'])."',
						`trainer_id` = '".$this->request->post['trainer_id']."',
						`trainer_code` = '".$this->db->escape($this->request->post['trainer_code'])."',
						`trainer_name` = '".$this->db->escape($this->request->post['trainer_name'])."',
						`club` = '".$this->db->escape($this->request->post['club'])."',
						`horse_id` = '".$this->request->post['horse_id']."',
						`horse_name` = '".$this->db->escape($this->request->post['horse_name'])."',
						`offences` = '".$this->db->escape($this->request->post['offences'])."',
						`race_no` = '".$this->request->post['race_no']."',
						`race_date` = '".date('Y-m-d',strtotime($this->request->post['race_date']))."',
						`start_date1` = '".date('Y-m-d',strtotime($this->request->post['suspension_from']))."',
						`end_date1` = '".date('Y-m-d',strtotime($this->request->post['suspension_to']))."',
						`amount` = '".$this->request->post['fine']."',
						`wdr_licence` = '".$wdr_lic."',
						`ride_work` = '".$ride_work."',
						`mock_race` = '".$mock_race."',
						`reason` = '".$this->db->escape($this->request->post['remark'])."',
						`reason_stewards` = '".$this->db->escape($remark_stewards)."',
						`reason_board_of_authority` = '".$this->db->escape($remark_board_of_authority)."',
						`stipes_entry_date` = '".date('Y-m-d')."'
						 WHERE ban_id = '".$this->request->get['ban_id']."'");
				} else if($this->request->get['editby'] == 'stewards') {
					$this->db->query("UPDATE trainer_ban SET
						`reason_stewards` = '".$this->db->escape($remark_stewards)."', 
						`stewards_offences` = '".$this->db->escape($this->request->post['offences'])."',
						`stewards_fine` = '".$this->db->escape($this->request->post['fine'])."',
						`start_date1` = '".date('Y-m-d',strtotime($this->request->post['suspension_from']))."',
						`end_date1` = '".date('Y-m-d',strtotime($this->request->post['suspension_to']))."',
						`stewards_entry_date` = '".date('Y-m-d')."'
						 WHERE ban_id = '".$this->request->get['ban_id']."'");
				} else {
					$this->db->query("UPDATE trainer_ban SET 
						`reason_board_of_authority` = '".$this->db->escape($remark_board_of_authority)."' ,
						`board_of_authority_offences` = '".$this->db->escape($this->request->post['offences'])."',
						`board_of_authority_fine` = '".$this->db->escape($this->request->post['fine'])."',
						`start_date1` = '".date('Y-m-d',strtotime($this->request->post['suspension_from']))."',
						`end_date1` = '".date('Y-m-d',strtotime($this->request->post['suspension_to']))."',
						`board_of_authority_entry_date` = '".date('Y-m-d')."'
						WHERE ban_id = '".$this->request->get['ban_id']."'");
				}

			} else {
				$this->db->query("INSERT INTO `trainer_ban` SET 
					`authority` = '".$this->db->escape($this->request->post['authority'])."',
					`trainer_id` = '".$this->request->post['trainer_id']."',
					`trainer_code` = '".$this->db->escape($this->request->post['trainer_code'])."',
					`trainer_name` = '".$this->db->escape($this->request->post['trainer_name'])."',
					`club` = '".$this->db->escape($this->request->post['club'])."',
					`horse_id` = '".$this->request->post['horse_id']."',
					`horse_name` = '".$this->db->escape($this->request->post['horse_name'])."',
					`offences` = '".$this->db->escape($this->request->post['offences'])."',
					`race_no` = '".$this->request->post['race_no']."',
					`race_date` = '".date('Y-m-d',strtotime($this->request->post['race_date']))."',
					`start_date1` = '".date('Y-m-d',strtotime($this->request->post['suspension_from']))."',
					`end_date1` = '".date('Y-m-d',strtotime($this->request->post['suspension_to']))."',
					`amount` = '".$this->request->post['fine']."',
					`wdr_licence` = '".$wdr_lic."',
					`ride_work` = '".$ride_work."',
					`mock_race` = '".$mock_race."',
					`reason` = '".$this->db->escape($this->request->post['remark'])."',
					`reason_stewards` = '".$this->db->escape($remark_stewards)."',
					`reason_board_of_authority` = '".$this->db->escape($remark_board_of_authority)."',
					`stipes_entry_date` = '".date('Y-m-d')."'

					");
			}

			$this->session->data['success'] = "trainer Ban Recorded Successfully!";
			$json['status'] = 1;
		}
			

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	


	public function autocompletetrainer() {
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		$json = array();
		$json['final_trainer'] = array();

		if( (isset($this->request->get['trainer_code']) && $this->request->get['trainer_code'] != '') ){
			$sql = "SELECT trainer_code, name, trainer_code_new FROM trainers WHERE 1=1 ";

			if($this->request->get['trainer_code'] != '') {
				$sql .= " AND trainer_code_new LIKE '%". $this->request->get['trainer_code'] ."%'";
			}

			$trainer_datas = $this->db->query($sql)->rows;
			foreach ($trainer_datas as $tkey => $tvalue) {
				$json['final_trainer'][] = array(
					'id' => $tvalue['trainer_code'],
					'name' => $tvalue['name'],
					'trainer_code_new' => $tvalue['trainer_code_new']
				); 
			}
		}

		if( (isset($this->request->get['trainer_name']) && $this->request->get['trainer_name'] != '') ){
			$sql = "SELECT trainer_code, name, trainer_code_new FROM trainers WHERE 1=1 ";

			
			if($this->request->get['trainer_name'] != '') {
				$sql .= " AND name LIKE '%". $this->request->get['trainer_name'] ."%'";
			}


			$trainer_datas = $this->db->query($sql)->rows;
			foreach ($trainer_datas as $tkey => $tvalue) {
				$json['final_trainer'][] = array(
					'id' => $tvalue['trainer_code'],
					'name' => $tvalue['name'],
					'trainer_code_new' => $tvalue['trainer_code_new']

				); 
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteHorse() {
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		$json = array();
		 $json['final_horse'] = array();

		if( (isset($this->request->get['horse_name']) && $this->request->get['horse_name'] != '') ){
			$sql = "SELECT official_name, horseseq FROM horse1 WHERE 1=1 AND horse_status = 1 ";

			
			if($this->request->get['horse_name'] != '') {
				$sql .= " AND official_name LIKE '%". $this->request->get['horse_name'] ."%'";
			}


			$horse_datas = $this->db->query($sql)->rows;
			foreach ($horse_datas as $tkey => $tvalue) {
				$json['final_horse'][] = array(
					'id' => $tvalue['horseseq'],
					'name' => $tvalue['official_name'],

				); 
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function autocompleteRaceNo() {
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		$json = array();


		if( (isset($this->request->get['race_no']) && $this->request->get['race_no'] != '') ){
			$sql = "SELECT pros_id, race_date FROM prospectus WHERE 1=1 ";

			
			if($this->request->get['race_no'] != '') {
				$sql .= " AND pros_id = '". $this->request->get['race_no'] ."'";
			}


			$race_datas = $this->db->query($sql)->rows;
			foreach ($race_datas as $tkey => $tvalue) {
				$json['final_race'][] = array(
					'id' => $tvalue['pros_id'],
					'race_date' => date('d-m-Y',strtotime($tvalue['race_date'])),

				); 
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
