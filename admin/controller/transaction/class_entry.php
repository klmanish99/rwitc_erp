<?php
class Controllertransactionclassentry extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('transaction/class_entry');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/class_entry');

		$this->getList();
	}

	public function add() {
		$this->load->language('transaction/class_entry');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/class_entry');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			$this->model_transaction_class_entry->addDoctors($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/class_entry', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('transaction/class_entry');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/class_entry');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			$this->model_transaction_class_entry->editDoctors($this->request->get['class_entry_id'], $this->request->post);
			// echo '<pre>';
			// print_r($this->request->post);
			
			// exit;
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/class_entry', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('transaction/class_entry');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/class_entry');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_transaction_class_entry->deleteDoctors($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/class_entry', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function repair() {
		$this->load->language('transaction/class_entry');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/class_entry');

		if ($this->validateRepair()) {
			$this->model_transaction_class_entry->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('transaction/class_entry', 'token=' . $this->session->data['token'], true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/class_entry', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/class_entry/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/class_entry/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/class_entry/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['filter_doctor_name'])) {
			$filter_doctor_name = $this->request->get['filter_doctor_name'];
			$data['filter_doctor_name'] = $this->request->get['filter_doctor_name'];
		}
		else{
			$filter_doctor_name = '';
			$data['filter_doctor_name'] = '';
		}

		if (isset($this->request->get['filter_doctor_code'])) {
			$filter_doctor_code = $this->request->get['filter_doctor_code'];
			$data['filter_doctor_code'] = $this->request->get['filter_doctor_code'];
		}
		else{
			$filter_doctor_code = '';
			$data['filter_doctor_code'] = '';
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = 'Active';
			$data['filter_status'] = 'Active';
		}

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_doctor_name'	=>	$filter_doctor_name,
			'filter_doctor_code'	=>	$filter_doctor_code,
			'filter_status'	=>	$filter_status
		);

		$data['status'] =array(
				'Active'  =>"Active",
				'In-Active'  =>'In-Active'
		);

		$category_total = $this->model_transaction_class_entry->getTotalDoctors();

		$results = $this->model_transaction_class_entry->getDoctor($filter_data);

		foreach ($results as $result) {
			$data['categories'][] = array(
				'class_entry_id' => $result['class_entry_id'],
				'distance'     => $result['distance'],
				'class'        => $result['class'],
				'sweepstake'        => $result['sweepstake'],
				'edit'        => $this->url->link('transaction/class_entry/edit', 'token=' . $this->session->data['token'] . '&class_entry_id=' . $result['class_entry_id'] . $url, true),
				'delete'      => $this->url->link('transaction/class_entry/delete', 'token=' . $this->session->data['token'] . '&class_entry_id=' . $result['class_entry_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/class_entry', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/class_entry', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('transaction/class_entry', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/class_entry_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_doctor_code'] = $this->language->get('entry_doctor_code');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['valierr_distance'])) {
			$data['valierr_distance'] = $this->error['valierr_distance'];
		} else {
			$data['valierr_distance'] = '';
		}

		if (isset($this->error['valierr_class'])) {
			$data['valierr_class'] = $this->error['valierr_class'];
		} else {
			$data['valierr_class'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/class_entry', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['distances'] = array(
			'1000'  =>'1000',
			'1100'  =>'1100',
			'1200'  =>'1200',
			'1400'  =>'1400',
			'1600'  =>'1600',
			'1800'  =>'1800',
			'2000'  =>'2000',
			'2200'  =>'2200',
			'2400'  =>'2400',
			'2800'  =>'2800',
			'3000'  =>'3000',
			'3200'  =>'3200',
		);

		$data['classs']= array(
			'Class I'  =>'Class I',
			'Class II'  =>'Class II',
			'Class III'  =>'Class III',
			'Class IV'  =>'Class IV',
			'Class V'  =>'Class V',
			'2/3 yrs old'  =>'2/3 yrs old',
		);

		if (!isset($this->request->get['class_entry_id'])) {
			$data['action'] = $this->url->link('transaction/class_entry/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/class_entry/edit', 'token=' . $this->session->data['token'] . '&class_entry_id=' . $this->request->get['class_entry_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('transaction/class_entry', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['class_entry_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_transaction_class_entry->getDoctors($this->request->get['class_entry_id']);
		}

		// if (isset($this->request->post['doctor_name'])) {
		// 	$data['doctor_name'] = $this->request->post['doctor_name'];
		// } elseif (!empty($category_info)) {
		// 	$data['doctor_name'] = $category_info['doctor_name'];
		// } else {
		// 	$data['doctor_name'] = '';
		// }

		// if (isset($this->request->post['doctor_code'])) {
		// 	$data['doctor_code'] = $this->request->post['doctor_code'];
		// } elseif (!empty($category_info)) {
		// 	$data['doctor_code'] = $category_info['doctor_code'];
		// } else {
		// 	$data['doctor_code'] = '';
		// }
		if (isset($this->request->post['distance'])) {
			$data['distance'] = $this->request->post['distance'];
		} elseif (!empty($category_info)) {
			$data['distance'] = $category_info['distance'];
		} else {
			$data['distance'] = '';
		}

		if (isset($this->request->post['class'])) {
			$data['class'] = $this->request->post['class'];
		} elseif (!empty($category_info)) {
			$data['class'] = $category_info['class'];
		} else {
			$data['class'] = '';
		}

		if (isset($this->request->post['entry'])) {
			$data['entry'] = $this->request->post['entry'];
		} elseif (!empty($category_info)) {
			$data['entry'] = $category_info['entry'];
		} else {
			$data['entry'] = '';
		}

		if (isset($this->request->post['acceptance'])) {
			$data['acceptance'] = $this->request->post['acceptance'];
		} elseif (!empty($category_info)) {
			$data['acceptance'] = $category_info['acceptance'];
		} else {
			$data['acceptance'] = '';
		}

		if (isset($this->request->post['division'])) {
			$data['division'] = $this->request->post['division'];
		} elseif (!empty($category_info)) {
			$data['division'] = $category_info['division'];
		} else {
			$data['division'] = '';
		}

		if (isset($this->request->post['two_nov_dec_entry'])) {
			$data['two_nov_dec_entry'] = $this->request->post['two_nov_dec_entry'];
		} elseif (!empty($category_info)) {
			$data['two_nov_dec_entry'] = $category_info['two_nov_dec_entry'];
		} else {
			$data['two_nov_dec_entry'] = '';
		}

		if (isset($this->request->post['two_nov_dec_acceptance'])) {
			$data['two_nov_dec_acceptance'] = $this->request->post['two_nov_dec_acceptance'];
		} elseif (!empty($category_info)) {
			$data['two_nov_dec_acceptance'] = $category_info['two_nov_dec_acceptance'];
		} else {
			$data['two_nov_dec_acceptance'] = '';
		}

		$data['Active'] = array('Yes'	=> 'Yes',
							  'No'	=> 'No');

		if (isset($this->request->post['sweepstake'])) {
			$data['sweepstake'] = $this->request->post['sweepstake'];
		} elseif (!empty($category_info)) {
			$data['sweepstake'] = $category_info['sweepstake'];
		} else {
			$data['sweepstake'] = 'Yes';
		}

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/class_entry_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/class_entry')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->request->post['distance'] == ''){
			$this->error['valierr_distance'] = 'Please Select Distance!';
		}

		if($this->request->post['class'] == ''){
			$this->error['valierr_class'] = 'Please Select Class!';
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'transaction/class_entry')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'transaction/class_entry')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_doctor_name'])) {
			$this->load->model('transaction/class_entry');

			$results = $this->model_transaction_class_entry->getDoctorAuto($this->request->get['filter_doctor_name']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name'     => strip_tags(html_entity_decode($result['doctor_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_name'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}

	public function autocompletes() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_doctor_code'])) {
			$this->load->model('transaction/class_entry');

			$results = $this->model_transaction_class_entry->getDoctorAutos($this->request->get['filter_doctor_code']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_code'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}
}
