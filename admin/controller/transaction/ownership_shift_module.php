<?php
class Controllertransactionownershipshiftmodule extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/ownership_shift_module');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/ownership_shift_module');
		$this->getList();
	}

	public function add() {
		$this->load->language('transaction/ownership_shift_module');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/ownership_shift_module');

		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')/* && $this->validateForm()*/) {
			$this->model_transaction_ownership_shift_module->addProspectus($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if($this->request->post['check_provi_owner'] == 'on'){
				if($this->request->post['charge_type'] == 'Arrival Charges'){
					echo "<script>window.close();</script>";
				} else {
					$this->response->redirect($this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'], true));
				}
			} else {
				$this->response->redirect($this->url->link('transaction/horse_datas_link', 'token=' . $this->session->data['token'] .'&horse_id=' . $this->request->post['filterHorseId'].'&color=1&order=1&provi_owner=' . $this->request->post['check_provi_owner'], true));
			}
		}

		$this->getForm();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['filter_race_name'])) {
			$data['filter_race_name'] =$this->request->get['filter_race_name'];
		} else{
			$data['filter_race_name']  ="";
		}

		if(isset($this->request->get['filter_acceptance_date'])){
			$data['filter_acceptance_date'] = $this ->request->get['filter_acceptance_date'];
		}
		 else{
			$data['filter_acceptance_date'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/ownership_shift_module/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/ownership_shift_module/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/ownership_shift_module/repair', 'token=' . $this->session->data['token'] . $url, true);


		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/ownership_shift_module/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/ownership_shift_module/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['categories'] = array();

		$data['ownerships_types'] = array(
			'Sale'  => 'Sale',
			'Lease'  => 'Lease',
			'Sub Lease'  => 'Sub Lease',
		);

		$data['allCharges'] = array(
			'Arrival Charges'  => 'Arrival Charges',
			'Name Registration'  => 'Name Registration',
			'Other'  => 'Other',
		);

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_race_name' => $data['filter_race_name'],
			'filter_acceptance_date' => $data['filter_acceptance_date']
		);
		

		if (isset($this->request->get['horse_id'])) {
			$horse_datass = $this->db->query("SELECT `official_name`, `horseseq` FROM `horse1` WHERE horseseq = '".$this->request->get['horse_id']."'")->row;
			$traniner_datass = $this->db->query("SELECT `trainer_name`, `trainer_id` FROM `horse_to_trainer` WHERE horse_id = '".$this->request->get['horse_id']."'");
			if($traniner_datass->num_rows > 0){
				$data['trainer_name'] = $traniner_datass->row['trainer_name'];
				$data['trainer_id'] = $traniner_datass->row['trainer_id'];
			} else {
				$data['trainer_name'] = "";
				$data['trainer_id'] = "";
			}

			// echo'<pre>';
			// print_r($traniner_datass);
			// exit;
			$data['horse_name'] = $horse_datass['official_name'];
			$data['horse_id'] = $horse_datass['horseseq'];
			
			$data['gt_id'] = 1;
			$data['arrival_charge'] = 0;
			$data['charge_type'] = '';
			if($this->request->get['pp'] == 'arrival_charge'){
				$data['check_provisonal'] = 1;
				$data['arrival_charge'] = 1;
				$data['charge_type'] = 'Arrival Charges';
			} elseif($this->request->get['pp'] == 'name_change') {
				$data['check_provisonal'] = 1;
				$data['arrival_charge'] = 1;
				$data['charge_type'] = 'Name Registration';

			} elseif($this->request->get['pp'] == 'Other') {
				$data['arrival_charge'] = 0;
				$data['charge_type'] = 'Other';

			}
		} else {
			$data['horse_name'] = '';
			$data['horse_id'] = '';
			$data['trainer_name'] = '';
			$data['trainer_id'] = '';
			$data['gt_id'] = 0;
			$data['arrival_charge'] = 0;
			$data['charge_type'] = '';
			$data['check_provisonal'] = 0;

		}

		if(isset($this->request->post['fromdate'])) {
			$data['fromdate'] = $this->request->post['fromdate'];
		} else {
			$data['fromdate'] = date('d-m-Y');
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/ownership_shift_module_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['pros_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');


		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_horse_name'])) {
			$filter_horse_name =$this->request->get['filter_horse_name'];
		} else{
			$filter_horse_name  ="";

		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$filter_data1 = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_horse_name' => $filter_horse_name
		);
		

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/ownership_shift_module/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/ownership_shift_module/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . $url, true);

		
		
		
		
		$data['token'] = $this->session->data['token'];
		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/ownership_shift_module_form', $data));
	}
	

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/ownership_shift_module')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['linkedOwnerName'] as $key => $value) {
			if ($value['linked_owner_id'] == '') {
				$this->error['linkedOwner'] = 'Please select valid linked owner.';
			}
		}

		if ($this->request->post['gstType'] == 'Registered' && $this->request->post['gstNo'] == '') {
			$this->error['gst_number'] = 'GST No can not be blank.';
		}

		foreach ($this->request->post['owner_shared_color'] as $key => $value) {
			if ($value['shared_owner_id'] == '') {
				$this->error['sharedColor'] = 'Please select valid owner to share color.';
			}
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'transaction/ownership_shift_module')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	
	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('transaction/ownership_shift_module');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'owner_Id'	  => $this->request->get['owner_Id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_ownership_shift_module->getOwners($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'owner_id' => $result['id'],
					'owner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['owner_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autoHorseToTrainer() {
		$json = array();
		if (isset($this->request->get['horse_id'])) {
			$this->load->model('transaction/ownership_shift_module');
			$filter_data = array(
				'horse_id' => $this->request->get['horse_id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_ownership_shift_module->gethorsetoTrainer($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'trainer_id' => $result['trainer_id'],
					'trainer_code' => $result['trainer_code'],
					'trainer_name'        => strip_tags(html_entity_decode($result['trainer_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getOwnerData() {
		$json = array();
		if (isset($this->request->get['filter_hourse_id']) && isset($this->request->get['ownership_type_id'])) {
			$this->load->model('transaction/ownership_shift_module');
			$filter_data = array(
				'horse_id' => $this->request->get['filter_hourse_id'],
				'ownership_type_id' => $this->request->get['ownership_type_id'],
			);
			$current_owners = $this->db->query("SELECT horse_to_owner_id, end_date_of_ownership, owner_percentage, grandpa_id, horse_id, entry_count FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' AND owner_share_status = 1 ");
			
			$his_status = 0;
			if($current_owners->num_rows > 0){
				foreach ($current_owners->rows as $ckey => $cvalue) {
					$date_added = date('Y-m-d');
					$current_date = date('Y-m-d');
					if(($current_date > $cvalue['end_date_of_ownership']) && $cvalue['end_date_of_ownership'] != '0000-00-00'){
						$his_status = 1;
						
						$owner_per = $cvalue['owner_percentage'];
						$this->db->query("UPDATE `horse_to_owner` SET owner_share_status = 0 ,cancel_lease_status = 'Cancel' WHERE horse_to_owner_id = '".$cvalue['horse_to_owner_id']."' AND horse_id = '".$cvalue['horse_id']."' ");

						$grand_parent_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id = '".$cvalue['grandpa_id']."'")->row;


						$pre_own_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE to_owner_id = '".$grand_parent_datas['to_owner_id']."' AND owner_share_status = 1 AND  horse_id = '".$cvalue['horse_id']."' AND owner_percentage < 100 AND ownership_type = 'Sale' ");

						if($pre_own_datas->num_rows > 0){
							$this->db->query("UPDATE `horse_to_owner` SET owner_share_status = 0 WHERE horse_to_owner_id = '".$pre_own_datas->row['horse_to_owner_id']."' AND horse_id = '".$cvalue['horse_id']."' ");
							$owner_per = $pre_own_datas->row['owner_percentage'] + $cvalue['owner_percentage'];
						}
						$entry_count = $cvalue['entry_count'] + 1;
						$this->db->query("INSERT INTO `horse_to_owner` SET  
						`horse_id` = '" . $this->db->escape($grand_parent_datas['horse_id']) . "',
						`horse_group_id` = '" . $this->db->escape($grand_parent_datas['horse_group_id']) . "',
						`trainer_id` = '" . $this->db->escape($grand_parent_datas['trainer_id']) . "',
						`ownership_type` = '".$this->db->escape($grand_parent_datas['ownership_type'])."',
						`parent_group_id` = '".$grand_parent_datas['horse_group_id']."',
						`date_of_ownership` = '" . $this->db->escape($grand_parent_datas['date_of_ownership']) . "',
						`end_date_of_ownership` = '" . $this->db->escape($grand_parent_datas['end_date_of_ownership']) . "',
						`remark_horse_to_owner` = '" . $this->db->escape($grand_parent_datas['remark_horse_to_owner']) . "',
						`owner_percentage` = '" . $this->db->escape($owner_per) . "',
						`to_owner` = '" . $this->db->escape($grand_parent_datas['to_owner']) . "',
						`to_owner_id` = '" . $this->db->escape($grand_parent_datas['to_owner_id']) . "',
						`contengency` = '" . $this->db->escape($grand_parent_datas['contengency']) . "',
						`cont_percentage` = '" . $this->db->escape($grand_parent_datas['cont_percentage']) . "',
						`cont_amount` = '" . $this->db->escape($grand_parent_datas['cont_amount']) . "',
						`cont_place` = '" . $this->db->escape($grand_parent_datas['cont_place']) . "',
						`win_gross_stake` = '" . $this->db->escape($grand_parent_datas['win_gross_stake']) . "',
						`win_net_stake` = '" . $this->db->escape($grand_parent_datas['win_net_stake']) . "',
						`millionover` = '" . $this->db->escape($grand_parent_datas['millionover']) . "',
						`millionover_amt` = '" . $this->db->escape($grand_parent_datas['millionover_amt']) . "',
						`grade1` = '" . $this->db->escape($grand_parent_datas['grade1']) . "',
						`grade2` = '" . $this->db->escape($grand_parent_datas['grade2']) . "',
						`grade3` = '" . $this->db->escape($grand_parent_datas['grade3']) . "',
						`owner_share_status` = '1',
						`entry_count` = '".$entry_count."',
						`grandpa_id` = '".$cvalue['grandpa_id']."',
						`date_added` = '".$date_added."',
						`user_id` = '".$this->session->data['user_id']."',
						`provisional_ownership` = '".$grand_parent_datas['provisional_ownership']."'
						");
						
					}
				}
				if($his_status == 1){
					$history_owners = $this->db->query("SELECT `batch_id` FROM `horse_to_owner_history` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' ORDER BY id DESC LIMIT 1 ");
					$batch_id = 1;
					if($history_owners->num_rows > 0){
						$batch_id = $history_owners->row['batch_id'] + 1;
						$this->db->query("UPDATE `horse_to_owner_history` SET status = 1 WHERE batch_id = '".$history_owners->row['batch_id']."' ");
					}

					$allCurrOwners = $this->db->query("SELECT horse_id, horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' AND owner_share_status = 1 ");
					if($allCurrOwners->num_rows > 0){
						foreach ($allCurrOwners->rows as $ohkey => $ohvalue) {
							$this->db->query("INSERT INTO `horse_to_owner_history` SET 
								`horse_id` = '" . $ohvalue['horse_id'] . "',
								`batch_id` = '" . $batch_id . "',
								`horse_to_owner_id` = '" . $ohvalue['horse_to_owner_id'] . "',
								`status` = 0
								");
						}
					}
				}
			}
			
			$results = $this->model_transaction_ownership_shift_module->gethorsetoOwner($filter_data);

			//echo "<pre>";print_r($results);exit;
			$html = '';
			
			if($results){
				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html .= '<table class="table table-bordered ownertable">';
				$html .= '<thead>';
				$html .= '<tr>';
					$html .= '<td style="width: 40px;text-align: center;"></td>';
					$html .= '<td style="text-align: center;vertical-align: middle;">Name</td>';
					$html .= '<td style="text-align: center;vertical-align: middle;">Type <br> (O / L / SL)</td>';
					$html .= '<td style="text-align: center;vertical-align: middle;">Period</td>';

					$html .= '<td style="width: 160px;text-align: center;vertical-align: middle;">Share</td>';
					
					$html .= '<td style="width: 100px;text-align: center;vertical-align: middle;">Color</td>';
					$html .= '<td style="width: 200px;text-align: center;vertical-align: middle;">Contingency</td>';
					$html .= '<td style="width: 300px;text-align: center;vertical-align: middle;">Ownership</td>';
					
				$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';

				$i=1;
				$parent_data =array();
				$sub_parent_data = array();
				foreach ($results as $result) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$result['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$result['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html .= '<tr>';
					$html .= '<td class="r_'.$i.'" style="text-align: center;">';
						if($result['ownership_type'] == 'Sale' && ($this->request->get['ownership_type_id'] == 'Lease' || $this->request->get['ownership_type_id'] == 'Sale')){
							$html .= '<input type="checkbox" name="owner_datas['.$i.'][share_saler]"  class="checkbox_value ent-evnt" id="fro_ow_ckper_'.$i.'" />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][grandpa_id]" value="'.$result['grandpa_id'].'"  id="grandpa_id_'.$i.'"  />';
						} else if($result['ownership_type'] == 'Lease' && $this->request->get['ownership_type_id'] == 'Sub Lease'){
							$html .= '<input type="checkbox" name="owner_datas['.$i.'][share_saler]"  class="checkbox_value ent-evnt" id="fro_ow_ckper_'.$i.'" />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][grandpa_id]" value="'.$result['grandpa_id'].'"  id="grandpa_id_'.$i.'"  />';
						}  else if($result['ownership_type'] == 'Sub Lease' && $this->request->get['ownership_type_id'] == 'Sub Lease'){
							$html .= '<input type="checkbox" name="owner_datas['.$i.'][share_saler]"  class="checkbox_value ent-evnt" id="fro_ow_ckper_'.$i.'" />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][grandpa_id]" value="'.$result['grandpa_id'].'"  id="grandpa_id_'.$i.'"  />';
						} else {
							$html .= '<input type="checkbox" name="owner_datas['.$i.'][share_saler]"  class="checkbox_value ent-evnt" id="fro_ow_ckper_'.$i.'" onclick="return false;" aria-disabled/>';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][grandpa_id]" value="'.$result['grandpa_id'].'"  id="grandpa_id_'.$i.'"  />';
						}
					$html .= '</td>';


					$from_date = date('d-m-Y', strtotime($result['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($result['end_date_of_ownership']));

					$html .= '<td class="r_'.$i.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html .= '<span style="cursor:pointer"><a id="partners_'.$result['to_owner_id'].'"  class="partners" >'.$result['to_owner'].'</a></span><br>';
						}else {
							$html .= '<span >'.$result['to_owner'].'</span><br>';
						}
							$html .= '<input type="hidden" name="owner_datas['.$i.'][from_owner_hidden]" value="'.$result['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$i.'"  />';
					$html .= '</td>';

					$html .= '<td class="r_'.$i.'" style="text-align: center;">';  
						$provisional_ownership  = ($result['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($result['ownership_type'] == 'Lease'){
							$html .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($result['ownership_type'] == 'Sub Lease'){
								
							$html .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html .= '</td>';

					$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							
						if($result['ownership_type'] == 'Lease'){
							$html .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($result['ownership_type'] == 'Sub Lease'){
								
							$html .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($result['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($result['end_date_of_ownership'])) : "";
							$html .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html .= '</td>';
					


					$html .= '<td class="r_'.$i.'" style="text-align: right;">';  
						$html .= '<span>'.$result['owner_percentage'].'%'.'</span>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_percentage]" value="'.$result['owner_percentage'].'" id="owner_percentage_'.$i.'"  />';
					$html .= '</td>';

					
					$html .= '<td class="r_'.$i.'" style="text-align: center;">';
					if($result['owner_color'] == 'Yes' && $last_color == $result['horse_to_owner_id']){
						$html .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'" checked="checked"  />';
					} elseif($result['owner_color'] == 'join'){
						$html .= '<i class="fa fa-link" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'" checked="checked"  />';
					} else {
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'"  />';
					} 
					$html .= '</td>';
					$contengency = ($result['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($result['cont_percentage'] != 0) ? $result['cont_percentage'] : "";
					$cont_amount = ($result['cont_amount'] != 0) ? $result['cont_amount'] : "0";
					$win_gross_stake = ($result['win_gross_stake'] == 1) ? "Yes" : "N/A";
					$win_net_stake = ($result['win_net_stake'] == 1) ? "Yes" : "N/A";
					$cont_place = ($result['cont_place'] == 1) ? "Yes" : "N/A";
					$millionover = ($result['millionover'] == 1) ? "Yes" : "N/A";
					$millionover_amt = ($result['millionover_amt'] != 0) ? $result['millionover_amt'] : "0";
					$grade1 = ($result['grade1'] == 1) ? "Yes" : "N/A";
					$grade2 = ($result['grade2'] == 1) ? "Yes" : "N/A";
					$grade3 = ($result['grade3'] == 1) ? "Yes" : "N/A";


					$html .= '<td class="r_'.$i.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$result['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][parent_cont_amt]" value="'.$cont_amount.'" class="parent_cont_amt ent-evnt" id="parent_cont_amt_'.$i.'"  />';
						}
					$html .= '</td>';
						
					if($parent_data){
						if($parent_data['to_owner'] != '' && $result['ownership_type'] != 'Sale'){
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								if($result['ownership_type'] == 'Lease'){
									$html .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else if($result['ownership_type'] == 'Sub Lease'){
									$html .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Sub-Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';
								} else {
									$html .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html .= '<input type="hidden" name="owner_datas['.$i.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$i.'"  />';
							$html .= '</td>';
						}else {
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span> </span>';
								$html .= '<input type="hidden" name="owner_datas['.$i.'][parent_to_owner]" value="0" id="parent_to_owner_'.$i.'"  />';
							$html .= '</td>';
						}
					} else {
						$html .= '<td class="r_'.$i.'" style="text-align: left;">';
						$html .= '</td>';
					}
					$i++;
				}
				$html .= '</tbody>';
				$html .= '</table>';
				$json['new_user'] = 0;
			} else {

				$json['new_user'] = 1;
			}
		}
		// echo'<pre>';
		// print_r($json);
		 //exit;
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getContaingencyData(){
		
		$html = '';
		$json = array();
		$containgency_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$this->request->get['horse_owner_id']."'");
		// echo'<pre>';
		// print_r($containgency_datas);
		// exit;
		if($containgency_datas->num_rows > 0){
			$horse_name = $this->db->query("SELECT official_name FROM `horse1` WHERE horseseq ='".$containgency_datas->row['horse_id']."'")->row;
			// echo'<pre>';
			// print_r($horse_name);
			// exit;
			$containgency_data = $containgency_datas->row;
			$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$this->request->get['horse_owner_id']."'");
			if($lease_table_data->num_rows > 0){
				$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
			} else {
				$parent_data['to_owner'] = '';
			}
			$contengency = ($containgency_data['contengency'] == 1) ? "Yes" : "N/A";
			$cont_percentage = ($containgency_data['cont_percentage'] != 0) ? $containgency_data['cont_percentage'] : "";
			$cont_amount = ($containgency_data['cont_amount'] != 0) ? $containgency_data['cont_amount'] : "0";
			$win_gross_stake = ($containgency_data['win_gross_stake'] == 1) ? "Yes" : "N/A";
			$win_net_stake = ($containgency_data['win_net_stake'] == 1) ? "Yes" : "N/A";
			$cont_place = ($containgency_data['cont_place'] == 1) ? "Yes" : "N/A";
			$millionover = ($containgency_data['millionover'] == 1) ? "Yes" : "N/A";
			$millionover_amt = ($containgency_data['millionover_amt'] != 0) ? $containgency_data['millionover_amt'] : "0";
			$grade1 = ($containgency_data['grade1'] == 1) ? "Yes" : "N/A";
			$grade2 = ($containgency_data['grade2'] == 1) ? "Yes" : "N/A";
			$grade3 = ($containgency_data['grade3'] == 1) ? "Yes" : "N/A";

			$html .= '<div class="col-sm-10>';
				$html .= '<div class="row">';
				$html .= '<span style="font-size:16px;"><b>Name : </b> '.$parent_data['to_owner'].'</span><br><br>';
				$html .= '<span style="font-size:16px;"><b>Horse Name : </b> '.$horse_name['official_name'].'</span><br><br>';

					$html .= '<div class="col-sm-6">';
						$html .= '<span style="font-size: 14px;"> <b> Contingency :</b> '.$contengency .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Amount :</b> Rs.'.$cont_amount .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Win Net Stake :</b> '.$win_net_stake .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Millionover :</b> '.$millionover .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Grade 1 :</b> '.$grade1 .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Grade 3 :</b> '.$grade3 .'</span> <br>';
					$html .= '</div>';

					$html .= '<div class="col-sm-6">';
						$html .= '<span style="font-size: 14px;"> <b> Percentage :</b> '.$cont_percentage .'% </span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Win Gross Stake :</b> '.$win_gross_stake .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Place :</b> '.$cont_place .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Millionover Amt :</b> Rs.'.$millionover_amt .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Grade 2 :</b> '.$grade2 .'</span> <br>';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
		}
		$json = $html;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getContaingencyDataForHidden(){
		$json = array();
		//$containgency_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$this->request->get['horse_owner_id']."' AND contengency = 1");
		$containgency_datas = $this->db->query("SELECT  *  FROM `horse_to_owner` WHERE horse_to_owner_id ='".$this->request->get['horse_owner_id']."' ");
		// echo'<pre>';
		// print_r($containgency_datas);
		// exit;
		if($containgency_datas->num_rows > 0){
			$json = $containgency_datas->row;
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getPartnersData(){

		$html = '';
		$json = array();
		$partners_datasss = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$this->request->get['parent_owner_id']."'");
		$owner_datasss = $this->db->query("SELECT `id`,`owner_name` FROM `owners` WHERE id ='".$this->request->get['parent_owner_id']."'")->row;

		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		if($partners_datasss->num_rows > 0){

			$html .= '<div class="modal fade" id="partners_data_'.$this->request->get['parent_owner_id'].'" role="dialog">';
                $html .= '<div class="modal-dialog">';
                     $html .= '<div class="modal-content">';
                        $html .= '<div class="modal-header">';
                            $html .= '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                            $html .= '<h4 class="modal-title">Partners Details</h4>';
                        $html .= '</div>';
                        $html .= '<div class="modal-body partners_div" style="height: 440px;">';
							$html .= '<table class="table table-bordered partner_tbl" id="tableid_'.$this->request->get['parent_owner_id'].'">';
								$html .= '<thead>';
								$html .= '<tr>';
									$html .= '<td style="width: 40px;text-align: center;"></td>';
									$html .= '<td style="text-align: center;vertical-align: middle;">Partners Name</td>';
								$html .= '</tr>';
								$html .= '</thead>';
								$html .= '<tbody>';
									$html .= '<span style="font-size:16px;"><b>Name : </b> '.$owner_datasss['owner_name'].'</span><br><br>';
									foreach ($partners_datasss->rows as $key => $value) {
										$represntative_datasss = $this->db->query("SELECT `represntative_id` FROM `horse_to_owner` WHERE `to_owner_id` ='".$this->request->get['parent_owner_id']."' AND `horse_id` ='".$this->request->get['horse_id']."' AND `owner_share_status` = '1' AND `represntative_id` = '".$value['partner_id']."' ");

										$repres_status = ($represntative_datasss->num_rows > 0) ? "1" : "0";

										$html .= '<tr>';
											$html .= '<td class="p_'.$key.'" style="text-align: center;">';
												if($repres_status == 1){
													$html .= '<input type="checkbox" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][is_reprentative]"  class="parent_value" id="owner_partner_id_'.$key.'" checked="checked />';

												} else {

													$html .= '<input type="checkbox" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][is_reprentative]"  class="parent_value" id="owner_partner_id_'.$key.'" />';
												}
												$html .= '<input type="hidden" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][partner_id]" value="'.$value['partner_id'].'"  id="partner_id_'.$key.'"  />';
												$html .= '<input type="hidden" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][owner_id]" value="'.$owner_datasss['id'].'"  id="partner_id_'.$key.'"  />';

											$html .= '</td>';
											$html .= '<td class="p_'.$key.'" style="text-align: left;">';
												$html .= '<span style="font-size: 14px;"> '.$value['partner_name'] .'</span>';
											$html .= '</td>';
										$html .= '</tr>';
									}
								$html .= '</tbody>';
							$html .= '</table>';
						$html .= '</div>';
                        $html .= '<div class="modal-footer">';
                            $html .= '<button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>';
                        $html .= '</div>';
                    $html .= '</div>';
                $html .= '</div>';
           $html .=  '</div>';
		}
		$json = $html;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));


	}

	public function autocompleteOwners(){
		$json = array();
		if (isset($this->request->get['owner_name'])) {
			$this->load->model('transaction/ownership_shift_module');
			$filter_data = array(
				'owner_name' => $this->request->get['owner_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);


			$results = $this->model_transaction_ownership_shift_module->getAllOwners($filter_data);

			$strings= '';
			$alluser_ids = explode(',',$this->request->get['pre_owners']);
			$added_ids = explode(',',$this->request->get['added_ids']);
			$updated_ids = '';
			foreach ($alluser_ids as $akey => $avalue) {
				if(!in_array($avalue, $added_ids)){
					if($avalue != ''){
						$updated_ids .="'" . $avalue. "'".','; 
					}
				}
			}

			$strings = rtrim($updated_ids, ',');
			if($strings != ''){
				$all_allowners = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_ids']."' AND owner_share_status = '1' AND to_owner_id NOT IN ($strings) ")->rows;
			} else {
				$all_allowners = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_ids']."' AND owner_share_status = '1' ")->rows;
			}
			
			$final_allowners = array();
			foreach ($all_allowners as $key => $value) {
				$final_allowners[] = $value['to_owner_id']; 
			}

			
			foreach ($results as $result) {
				if(!in_array($result['id'], $final_allowners) && !in_array($result['id'], $added_ids)){

					$partner_datasz = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id = '".$result['id']."'");
					$status = ($partner_datasz->num_rows > 0) ? "1" : "0";
					$json[] = array(
						'owner_id' => $result['id'],
						'owner_code' => $result['owner_code'],
						'status' => $status,
						'owner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteHorse() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['horse_name'])) {
			$this->load->model('transaction/ownership_shift_module');

			$results = $this->model_transaction_ownership_shift_module->getHorsesAuto($this->request->get['horse_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'horse_id' => $result['horseseq'],
						'horse_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		/*echo '<pre>';print_r($json);
			exit;*/
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['horse_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}
	
}
