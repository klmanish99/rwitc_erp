<?php
class ControllerTransactionProspectus extends Controller {
	private $error = array();
	public function index() {

		$this->load->language('transaction/prospectus');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/prospectus');

		// if(isset($this->session->data['race_date'])){
		// 	$pre_date = date('Y-m-d', strtotime($this->session->data['race_date']));
		// 	$current_date = date('Y-m-d');
		// 	$cur_date = date('Y-m-d', strtotime($current_date));

		// 	if($pre_date != $cur_date){
		// 		unset(	$this->session->data['race_date']);
		// 		unset(	$this->session->data['season']);
		// 		unset(	$this->session->data['year']); 
		// 		unset(	$this->session->data['race_day']); 
		// 		unset(	$this->session->data['evening_race']); 
		// 		unset(	$this->session->data['entries_close_date']); 
		// 		unset(	$this->session->data['handicaps_date']); 
		// 		unset(	$this->session->data['acceptance_date']); 
		// 		unset(	$this->session->data['declaration_date']); 
		// 	}

		// }
		$this->getList();

		
	}

	public function add() {
		$this->load->language('transaction/prospectus');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/prospectus');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_transaction_prospectus->addProspectus($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$post_datass = $this->request->post;
			$this->session->data['race_date'] = $post_datass['race_date'];
			$this->session->data['season'] = $post_datass['season'];
			$this->session->data['year'] = $post_datass['year'];
			$this->session->data['race_day'] = $post_datass['race_day'];

			$this->session->data['evening_race'] = isset($post_datass['evening_race']) ? "Yes" : "No";
			$this->session->data['entries_close_date'] = $post_datass['entries_close_date'];
			$this->session->data['handicaps_date'] = $post_datass['handicaps_date'];
			$this->session->data['acceptance_date'] = $post_datass['acceptance_date'];
			$this->session->data['declaration_date'] = $post_datass['declaration_date'];


			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('transaction/prospectus');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/prospectus');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
			$this->model_transaction_prospectus->editProspectus($this->request->get['pros_id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('transaction/prospectus');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/prospectus');
		if (isset($this->request->post['selected'])/* && $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $pros_id) {
				$this->model_transaction_prospectus->deleteprospectus($pros_id);
			}
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getList();
	}

	public function repair() {
		$this->load->language('transaction/prospectus');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/prospectus');
		if ($this->validateRepair()) {
			$this->model_transaction_prospectus->repairCategories();
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'], true));
		}
		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}


		if (isset($this->request->get['filter_race_name'])) {
			$data['filter_race_name'] =$this->request->get['filter_race_name'];
		} else{
			$data['filter_race_name']  ="";
		}

		if(isset($this->request->get['filter_race_date'])){
			$data['filter_race_date'] = $this ->request->get['filter_race_date'];
		}
		 else{
			$data['filter_race_date'] = '';
		}


		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/prospectus/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/prospectus/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/prospectus/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_race_name' => $data['filter_race_name'],
			'filter_race_date' => $data['filter_race_date']
		);

		$category_total = $this->model_transaction_prospectus->getTotalCategories($filter_data);
		$results = $this->model_transaction_prospectus->getprospectus($filter_data);
		foreach ($results as $result) {
			if ($result['race_type'] == 'Handicap Race') {
				$race_type = 'Handicap';
			} elseif ($result['race_type'] == 'Term Race') {
				$race_type = 'Term';
			}
			$data['prospectusdatas'][] = array(
				'pros_id' 	  	=> $result['pros_id'],
				'race_date'   	=> $result['race_date'],
				'race_name'	  	=> $result['race_name'],
				'race_description'	=> $result['race_description'],
				'race_type'	  	=> $race_type,
				'class'	  	=> $result['class'],
				'grade'	  	=> $result['grade'],
				'race_category'	  	=> $result['race_category'],
				'max_stakes_race'	  	=> $result['max_stakes_race'],
				'foreign_jockey_eligible'	  	=> $result['foreign_jockey_eligible'],
				'edit'        	=> $this->url->link('transaction/prospectus/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $result['pros_id'] . $url, true),
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/prospectus_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['pros_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');
		
		$data['evening_races'] = array(
			'No'	=> 'No',
			'Yes'	=> 'Yes'
		);

		$data['types'] = array(
			'No'	=> 'No',
			'Yes'	=> 'Yes'
		);

		$data['seasons'] = array(
			'PUNE'  =>'PUNE',
			'MUMBAI' =>'MUMBAI'

		);
		$data['race_types'] = array(
			'Handicap Race'  =>'Handicap Race',
			'Term Race'  => 'Term Race',

		);
		$data['distances'] = array(
			'1000'  =>'1000',
			'1100'  =>'1100',
			'1200'  =>'1200',
			'1400'  =>'1400',
			'1600'  =>'1600',
			'1800'  =>'1800',
			'2000'  =>'2000',
			'2200'  =>'2200',
			'2400'  =>'2400',
			'2800'  =>'2800',
			'3000'  =>'3000',
			'3200'  =>'3200',
		);

		$data['days'] = array(
			'FIRST DAY'    =>'FIRST DAY',
			'SECOND DAY'    =>'SECOND DAY',
			'THIRD DAY'    =>'THIRD DAY',
			'FOURTH DAY'    =>'FOURTH DAY',
			'FIFTH DAY'    =>'FIFTH DAY',
			'SIXTH DAY'    =>'SIXTH DAY',
			'SEVENTH DAY'    =>'SEVENTH DAY',
			'EIGHTH DAY'    =>'EIGHTH DAY',
			'NINTH DAY'    =>'NINTH DAY',
			'TENTH DAY'    =>'TENTH DAY',
			'ELEVENTH DAY'    =>'ELEVENTH DAY',
			'TWELVETH DAY'    =>'TWELVETH DAY',
			'THIRTEENTH DAY'    =>'THIRTHEENTH DAY',
			'FOURTEENTH DAY'    =>'FOURTEENTH DAY',
			'FIFTEETH DAY'    =>'FIFTEETH DAY',
			'SIXTEENTH DAY'    =>'SIXTEENTH DAY',
			'SEVENTEENTH DAY'    =>'SEVENTEENTH DAY',
			'EIGHTEENTH DAY'    =>'EIGHTEENTH DAY',
			'NINETEENTH DAY'    =>'NINETEENTH DAY',
			'TWENTIETH DAY'    =>'TWENTIETH DAY',
			'TWENTY FIRST DAY'    =>'FIRST DAY',
			'TWENTY SECOND DAY'    =>'TWENTY SECOND DAY',
			'TWENTY THIRD DAY'    =>'TWENTY THIRD DAY',
			'TWENTY FOURTH DAY'    =>'TWENTY FOURTH DAY',
			'TWENTY FIFTH DAY'    =>'TWENTY FIFTH DAY',
			'TWENTY SIXTH DAY'    =>'TWENTY SIXTH DAY',
			'TWENTY SEVENTH DAY'    =>'TWENTY SEVENTH DAY',
			'TWENTY EIGHTH DAY'    =>'TWENTY EIGHTH DAY',
			'TWENTY NINTH DAY'    =>'TWENTY NINTH DAY',
			'THIRTIETH DAY'    =>'THIRTIETH DAY',
			'THIRTY FIRST DAY'    =>'THIRTY FIRST DAY',
			'THIRTY SECOND DAY'    =>'THIRTY SECOND DAY',
			'THIRTY THIRD DAY'    =>'THIRTY THIRD DAY',
			'THIRTY FOURTH DAY'    =>'THIRTY FOURTH DAY',
			'THIRTY FIFTH DAY'    =>'THIRTY FIFTH DAY',
			'THIRTY SIXTH DAY'    =>'THIRTY SIXTH DAY',
			'THIRTY SEVENTH DAY'    =>'THIRTY SEVENTH DAY',
			'THIRTY EIGHTH DAY'    =>'THIRTY EIGHTH DAY',
			'THIRTY NINTH DAY'    =>'THIRTY NINTH DAY',
			'FORTIETH DAY'    =>'FORTIETH DAY',
			'SPECIAL RACE DAY'    =>'SPECIAL RACE DAY',
			'EXTRA RACE DAY'    =>'EXTRA RACE DAY',
			'MOCK RACE DAY'    =>'MOCK RACE DAY',
		);

		$data['times'] = array(
			'12.00 AM'  =>'12.00 AM',
			'12.15 AM'  =>'12.15 AM',
			'12.30 AM'  =>'12.30 AM',
			'12.45 AM'  =>'12.45 AM',

			'1.00 AM'  =>'1.00 AM',
			'1.15 AM'  =>'1.15 AM',
			'1.30 AM'  =>'1.30 AM',
			'1.45 AM'  =>'1.45 AM',

			'2.00 AM'  =>'2.00 AM',
			'2.15 AM'  =>'2.15 AM',
			'2.30 AM'  =>'2.30 AM',
			'2.45 AM'  =>'2.45 AM',

			'3.00 AM'  =>'3.00 AM',
			'3.15 AM'  =>'3.15 AM',
			'3.30 AM'  =>'3.30 AM',
			'3.45 AM'  =>'3.45 AM',

			'4.00 AM'  =>'4.00 AM',
			'4.15 AM'  =>'4.15 AM',
			'4.30 AM'  =>'4.30 AM',
			'4.45 AM'  =>'4.45 AM',

			'5.00 AM'  =>'5.00 AM',
			'5.15 AM'  =>'5.15 AM',
			'5.30 AM'  =>'5.30 AM',
			'5.45 AM'  =>'5.45 AM',

			'6.00 AM'  =>'6.00 AM',
			'6.15 AM'  =>'6.15 AM',
			'6.30 AM'  =>'6.30 AM',
			'6.45 AM'  =>'6.45 AM',

			'7.00 AM'  =>'7.00 AM',
			'7.15 AM'  =>'7.15 AM',
			'7.30 AM'  =>'7.30 AM',
			'7.45 AM'  =>'7.45 AM',

			'8.00 AM'  =>'8.00 AM',
			'8.15 AM'  =>'8.15 AM',
			'8.30 AM'  =>'8.30 AM',
			'8.45 AM'  =>'8.45 AM',

			'9.00 AM'  =>'9.00 AM',
			'9.15 AM'  =>'9.15 AM',
			'9.30 AM'  =>'9.30 AM',
			'9.45 AM'  =>'9.45 AM',

			'10.00 AM'  =>'10.00 AM',
			'10.15 AM'  =>'10.15 AM',
			'10.30 AM'  =>'10.30 AM',
			'10.45 AM'  =>'10.45 AM',

			'11.00 AM'  =>'11.00 AM',
			'11.15 AM'  =>'11.15 AM',
			'11.30 AM'  =>'11.30 AM',
			'11.45 AM'  =>'11.45 AM',

			'12.00 PM'  =>'12.00 PM',
			'12.15 PM'  =>'12.15 PM',
			'12.30 PM'  =>'12.30 PM',
			'12.45 PM'  =>'12.45 PM',

			'1.00 PM'  =>'1.00 PM',
			'1.15 PM'  =>'1.15 PM',
			'1.30 PM'  =>'1.30 PM',
			'1.45 PM'  =>'1.45 PM',

			'2.00 PM'  =>'2.00 PM',
			'2.15 PM'  =>'2.15 PM',
			'2.30 PM'  =>'2.30 PM',
			'2.45 PM'  =>'2.45 PM',

			'3.00 PM'  =>'3.00 PM',
			'3.15 PM'  =>'3.15 PM',
			'3.30 PM'  =>'3.30 PM',
			'3.45 PM'  =>'3.45 PM',

			'4.00 PM'  =>'4.00 PM',
			'4.15 PM'  =>'4.15 PM',
			'4.30 PM'  =>'4.30 PM',
			'4.45 PM'  =>'4.45 PM',

			'5.00 PM'  =>'5.00 PM',
			'5.15 PM'  =>'5.15 PM',
			'5.30 PM'  =>'5.30 PM',
			'5.45 PM'  =>'5.45 PM',

			'6.00 PM'  =>'6.00 PM',
			'6.15 PM'  =>'6.15 PM',
			'6.30 PM'  =>'6.30 PM',
			'6.45 PM'  =>'6.45 PM',

			'7.00 PM'  =>'7.00 PM',
			'7.15 PM'  =>'7.15 PM',
			'7.30 PM'  =>'7.30 PM',
			'7.45 PM'  =>'7.45 PM',

			'8.00 PM'  =>'8.00 PM',
			'8.15 PM'  =>'8.15 PM',
			'8.30 PM'  =>'8.30 PM',
			'8.45 PM'  =>'8.45 PM',

			'9.00 PM'  =>'9.00 PM',
			'9.15 PM'  =>'9.15 PM',
			'9.30 PM'  =>'9.30 PM',
			'9.45 PM'  =>'9.45 PM',

			'10.00 PM'  =>'10.00 PM',
			'10.15 PM'  =>'10.15 PM',
			'10.30 PM'  =>'10.30 PM',
			'10.45 PM'  =>'10.45 PM',

			'11.00 PM'  =>'11.00 PM',
			'11.15 PM'  =>'11.15 PM',
			'11.30 PM'  =>'11.30 PM',
			'11.45 PM'  =>'11.45 PM',
		);

		$data['type']= array(
			'No'  =>'No',
			'Yes'  =>'Yes',
		);
		$data['stakes']= array(
			'Rs'  =>'Rs',
			'%'  =>'%',
		);

		$data['sexs']= array(
			'All' => 'All',
			'm/f' => 'M/F',
			'c/g/h/r' => 'C/G/H/R',
		);

		$data['classs']= array(
			'Class I'  =>'Class I',
			'Class II'  =>'Class II',
			'Class III'  =>'Class III',
			'Class IV'  =>'Class IV',
			'Class V'  =>'Class V',
			'Mix Class'  =>'Mix Class',
		);

		$data['positionss']= array(
			'1'  =>'1',
			'2'  =>'2',
			'3'  =>'3',
			'4'  =>'4',
			'5'  =>'5',
			'6'  =>'6',
			'7'  =>'7',
			'8'  =>'8',
			'9'  =>'9',
			'10'  =>'10',

		);

		$data['terms']= array(
			'Grade I'  =>'Grade I',
			'Grade II'  =>'Grade II',
			'Grade III'  =>'Grade III',
		);
		 

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->error['valierr_race_date'])) {
			$data['valierr_race_date'] = $this->error['valierr_race_date'];
		}
		if (isset($this->error['valierr_greater_race_date'])) {
			$data['valierr_greater_race_date'] = $this->error['valierr_greater_race_date'];
		}
		if (isset($this->error['valierr_greater_handicaps_date'])) {
			$data['valierr_greater_handicaps_date'] = $this->error['valierr_greater_handicaps_date'];
		}
		if (isset($this->error['valierr_greater_acceptance_date'])) {
			$data['valierr_greater_acceptance_date'] = $this->error['valierr_greater_acceptance_date'];
		}
		if (isset($this->error['valierr_greater_declaration_date'])) {
			$data['valierr_greater_declaration_date'] = $this->error['valierr_greater_declaration_date'];
		}
		if (isset($this->error['valierr_race_name'])) {
			$data['valierr_race_name'] = $this->error['valierr_race_name'];
		}

		if (isset($this->error['gst_number'])) {
			$data['error_gst_number'] = $this->error['gst_number'];
		} else {
			$data['error_gst_number'] = '';
		}
		
		if (isset($this->error['sharedColor'])) {
			$data['error_sharedColor'] = $this->error['sharedColor'];
		} else {
			$data['error_sharedColor'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/prospectus/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/prospectus/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['pros_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_transaction_prospectus->getCategory($this->request->get['pros_id']);

		}

		if(isset($this->request->get['pros_id'])){
			$prize_distribution = $this->model_transaction_prospectus->getprizedistribution($this->request->get['pros_id']);
			
			
		}
		// echo '<pre>';
		// print_r($category_info);
		// exit;
		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();


		if (!empty($prize_distribution)) {
			$data['prize_distributions'] = $prize_distribution;
		} else {
      		$data['prize_distributions'] = array();
    	}

		if (isset($this->request->post['pros_id'])) {
			$data['pros_id'] = $this->request->post['pros_id'];
		} elseif (!empty($category_info)) {
			$data['pros_id'] = $category_info['pros_id'];
		} else {
			$data['pros_id'] = '';
		}
		if (isset($this->request->post['year'])) {
			$data['year'] = $this->request->post['year'];
		} elseif (!empty($category_info)) {
			$data['year'] = $category_info['year'];
		} else {
			//echo'innn';exit;
		// 	echo'<pre>';
		// print_r($this->session->data['year']);
		// exit;
			if(isset($this->session->data['year']) ){
				$data['year'] = $this->session->data['year'];
			} else {
				$data['year'] = '';
			}
		}

		if (isset($this->request->post['season'])) {
			$data['season'] = $this->request->post['season'];
		} elseif (!empty($category_info)) {
			$data['season'] = $category_info['season'];
		} else {
			if(isset($this->session->data['season']) && $this->session->data['season'] != ''){
				$data['season'] = $this->session->data['season'];
			} else {
				$data['season'] = '';
			}
		}
		if (isset($this->request->post['grade'])) {
			$data['grade'] = $this->request->post['grade'];
		} elseif (!empty($category_info)) {
			$data['grade'] = $category_info['grade'];
		} else {
			$data['grade'] = '';
		}

		if (isset($this->request->post['class'])) {
			$data['class'] = $this->request->post['class'];
		} elseif (!empty($category_info)) {
			$data['class'] = $category_info['class'];
		} else {
			$data['class'] = '';
		}

		if (isset($this->request->post['race_day'])) {
			$data['race_day'] = $this->request->post['race_day'];
		} elseif (!empty($category_info)) {
			$data['race_day'] = $category_info['race_day'];
		} else {
			if(isset($this->session->data['race_day']) && $this->session->data['race_day'] != ''){
				$data['race_day'] = $this->session->data['race_day'];
			} else {
				$data['race_day'] = '';
			}
		}
		if (isset($this->request->post['evening_race'])) {
			$data['evening_race'] = $this->request->post['evening_race'];
		} elseif (!empty($category_info)) {
			$data['evening_race'] = $category_info['evening_race'];
		} else {
			if(isset($this->session->data['evening_race']) && $this->session->data['evening_race'] != ''){

				$data['evening_race'] = $this->session->data['evening_race'];
			} else {
				$data['evening_race'] = '';
			}
		}
		if (isset($this->request->post['race_name'])) {
			$data['race_name'] = $this->request->post['race_name'];
		} elseif (!empty($category_info)) {
			$data['race_name'] = $category_info['race_name'];
		} else {
			$data['race_name'] = '';
		}
		if (isset($this->request->post['race_type'])) {
			$data['race_type'] = $this->request->post['race_type'];
		} elseif (!empty($category_info)) {
			$data['race_type'] = $category_info['race_type'];
		} else {
			$data['race_type'] = '';
		}
		if (isset($this->request->post['race_category'])) {
			$data['race_category'] = $this->request->post['race_category'];
		} elseif (!empty($category_info)) {
			$data['race_category'] = $category_info['race_category'];
		} else {
			$data['race_category'] = '';
		}
		if (isset($this->request->post['race_description'])) {
			$data['race_description'] = $this->request->post['race_description'];
		} elseif (!empty($category_info)) {
			$data['race_description'] = $category_info['race_description'];
		} else {
			$data['race_description'] = '';
		}

		if (isset($this->request->post['rating_from'])) {
            $data['rating_from'] = $this->request->post['rating_from'];
        } elseif (!empty($category_info)) {
            $data['rating_from'] = $category_info['rating_from'];
        } else {
            $data['rating_from'] = '';
        }

        if (isset($this->request->post['rating_to'])) {
            $data['rating_to'] = $this->request->post['rating_to'];
        } elseif (!empty($category_info)) {
            $data['rating_to'] = $category_info['rating_to'];
        } else {
            $data['rating_to'] = '';
        }
		if (isset($this->request->post['distance'])) {
			$data['distance'] = $this->request->post['distance'];
		} elseif (!empty($category_info)) {
			$data['distance'] = $category_info['distance'];
		} else {
			$data['distance'] = '';
		}

		if (isset($this->request->post['entries_close_date_time'])) {
			$data['entries_close_date_time'] = $this->request->post['entries_close_date_time'];
		} elseif (!empty($category_info)) {
			$data['entries_close_date_time'] = $category_info['entries_close_date_time'];
		} else {
			$data['entries_close_date_time'] = '10.30 AM';
		}
		if (isset($this->request->post['handicaps_date_time'])) {
			$data['handicaps_date_time'] = $this->request->post['handicaps_date_time'];
		} elseif (!empty($category_info)) {
			$data['handicaps_date_time'] = $category_info['handicaps_date_time'];
		} else {
			$data['handicaps_date_time'] = '4.00 PM';
		}
		if (isset($this->request->post['acceptance_date_time'])) {
			$data['acceptance_date_time'] = $this->request->post['acceptance_date_time'];
		} elseif (!empty($category_info)) {
			$data['acceptance_date_time'] = $category_info['acceptance_date_time'];
		} else {
			$data['acceptance_date_time'] = '10.30 AM';
		}

		if (isset($this->request->post['declaration_date_time'])) {
			$data['declaration_date_time'] = $this->request->post['declaration_date_time'];
		} elseif (!empty($category_info)) {
			$data['declaration_date_time'] = $category_info['declaration_date_time'];
		} else {
			$data['declaration_date_time'] = '10.30 AM';
		}
		/*if (isset($this->request->post['race_date'])) {
			$data['race_date'] = date('d-m-Y', strtotime($this->request->post['race_date']));
		} elseif (!empty($category_info)) {
			$data['race_date'] = date('d-m-Y', strtotime($category_info['race_date']));
		} else {
			$data['race_date'] = '';
		}*/


		if (isset($this->request->post['race_date'])) {
				$data['race_date'] = $this->request->post['race_date'];
		} elseif (!empty($category_info)) {
			if($category_info['race_date'] == '1970-01-01' || $category_info['race_date'] == '0000-00-00'){
				$data['race_date'] = '';
			} else {
				$data['race_date'] = date('d-m-Y', strtotime($category_info['race_date']));
			}
		} else {

			if(isset($this->session->data['race_date']) && $this->session->data['race_date'] != ''){
				$data['race_date'] = $this->session->data['race_date'];
			} else {
				$data['race_date'] = '';
			}
		}

		if (isset($this->request->post['serial_no'])) {
			$data['serial_no'] = $this->request->post['serial_no'];
		} elseif (!empty($category_info)) {
			$data['serial_no'] = $category_info['serial_no'];
		} else {
			$data['serial_no'] = '';
		}

		if (isset($this->request->post['entries_close_date'])) {
			$data['entries_close_date'] = $this->request->post['entries_close_date'];
		} elseif (!empty($category_info)) {
			if($category_info['entries_close_date'] != '0000-00-00' && $category_info['entries_close_date'] != '1970-01-01' ){
				$entries_close_date_dmy =date('d-m-Y', strtotime($category_info['entries_close_date']));
				$data['entries_close_date'] = $entries_close_date_dmy;
			} else {
				$data['entries_close_date'] = '';
			}

		} else {
			if(isset($this->session->data['entries_close_date']) && $this->session->data['entries_close_date'] != ''){
				$data['entries_close_date'] = $this->session->data['entries_close_date'];
			} else {
				$data['entries_close_date'] = '';
			}
			
		}

		if (isset($this->request->post['handicaps_date'])) {
			$data['handicaps_date'] = $this->request->post['handicaps_date'];
		} elseif (!empty($category_info)) {
			if($category_info['handicaps_date'] != '0000-00-00' && $category_info['handicaps_date'] != '1970-01-01' ){
				$handicaps_date_dmy =date('d-m-Y', strtotime($category_info['handicaps_date']));
				
				$data['handicaps_date'] = $handicaps_date_dmy;
			} else {
				$data['handicaps_date'] = '';
			}

		} else {
			if(isset($this->session->data['handicaps_date']) && $this->session->data['handicaps_date'] != ''){
				$data['handicaps_date'] = $this->session->data['handicaps_date'];
			} else {
				$data['handicaps_date'] = '';
			}
		}

		if (isset($this->request->post['acceptance_date'])) {
			$data['acceptance_date'] = $this->request->post['acceptance_date'];
		} elseif (!empty($category_info)) {
			if($category_info['acceptance_date'] != '0000-00-00' && $category_info['acceptance_date'] != '1970-01-01' ){
				$acceptance_date_dmy =date('d-m-Y', strtotime($category_info['acceptance_date']));
				$data['acceptance_date'] = $acceptance_date_dmy;
			} else {
				$data['acceptance_date'] = '';
			}

		} else {
			if(isset($this->session->data['acceptance_date']) && $this->session->data['acceptance_date'] != ''){
				$data['acceptance_date'] = $this->session->data['acceptance_date'];
			} else {
				$data['acceptance_date'] = '';
			}
		}

		if (isset($this->request->post['declaration_date'])) {
			$data['declaration_date'] = $this->request->post['declaration_date'];
		} elseif (!empty($category_info)) {
			if($category_info['declaration_date'] != '0000-00-00' && $category_info['declaration_date'] != '1970-01-01' ){
				$declaration_date_dmy =date('d-m-Y', strtotime($category_info['declaration_date']));
				
				$data['declaration_date'] = $declaration_date_dmy;
			} else {
				$data['declaration_date'] = '';
			}

		} else {
			if(isset($this->session->data['declaration_date']) && $this->session->data['declaration_date'] != ''){
				$data['declaration_date'] = $this->session->data['declaration_date'];
			} else {
				$data['declaration_date'] = '';
			}
		}
		if (isset($this->request->post['run_thrice_date'])) {
			$data['run_thrice_date'] = $this->request->post['run_thrice_date'];
		} elseif (!empty($category_info)) {
			if($category_info['run_thrice_date'] != '0000-00-00' && $category_info['run_thrice_date'] != '1970-01-01' ){
				$run_thrice_date_dmy =date('d-m-Y', strtotime($category_info['run_thrice_date']));
				$data['run_thrice_date'] = $run_thrice_date_dmy;
			} else {
				$data['run_thrice_date'] = '';
			}

		} else {
			$data['run_thrice_date'] = '';
		}

		if (isset($this->request->post['run_not_placed_date'])) {
			$data['run_not_placed_date'] = $this->request->post['run_not_placed_date'];
		} elseif (!empty($category_info)) {
			if($category_info['run_not_placed_date'] != '0000-00-00' && $category_info['run_not_placed_date'] != '1970-01-01' ){
				$run_not_placed_date_dmy =date('d-m-Y', strtotime($category_info['run_not_placed_date']));
				$data['run_not_placed_date'] = $run_not_placed_date_dmy;
			} else {
				$data['run_not_placed_date'] = '';
			}

		} else {
			$data['run_not_placed_date'] = '';
		}

		if (isset($this->request->post['run_not_won_date'])) {
			$data['run_not_won_date'] = $this->request->post['run_not_won_date'];
		} elseif (!empty($category_info)) {
			if($category_info['run_not_won_date'] != '0000-00-00' && $category_info['run_not_won_date'] != '1970-01-01' ){
				$run_not_won_date_dmy =date('d-m-Y', strtotime($category_info['run_not_won_date']));
				$data['run_not_won_date'] = $run_not_won_date_dmy;
			} else {
				$data['run_not_won_date'] = '';
			}

		} else {
			$data['run_not_won_date'] = '';
		}

		if (isset($this->request->post['run_and_not_won_during_mtg_date'])) {
			$data['run_and_not_won_during_mtg_date'] = $this->request->post['run_and_not_won_during_mtg_date'];
		} elseif (!empty($category_info)) {
			if($category_info['run_and_not_won_during_mtg_date'] != '0000-00-00' && $category_info['run_and_not_won_during_mtg_date'] != '1970-01-01' ){
				$run_and_not_won_during_mtg_date_dmy =date('d-m-Y', strtotime($category_info['run_and_not_won_during_mtg_date']));
				$data['run_and_not_won_during_mtg_date'] = $run_and_not_won_during_mtg_date_dmy;
			} else {
				$data['run_and_not_won_during_mtg_date'] = '';
			}

		} else {
			$data['run_and_not_won_during_mtg_date'] = '';
		}

		/*if (isset($this->request->post['entries_close_date'])) {
			$data['entries_close_date'] = date('d-m-Y', strtotime($this->request->post['entries_close_date']));
		} elseif (!empty($category_info)) {
			$data['entries_close_date'] = date('d-m-Y', strtotime($category_info['entries_close_date']));
		} else {
			$data['entries_close_date'] = '';
		}*/
		/*if (isset($this->request->post['handicaps_date'])) {
			$data['handicaps_date'] = date('d-m-Y', strtotime($this->request->post['handicaps_date']));
		} elseif (!empty($category_info)) {
			$data['handicaps_date'] = date('d-m-Y', strtotime($category_info['handicaps_date']));
		} else {
			$data['handicaps_date'] = '';
		}*/
		/*if (isset($this->request->post['acceptance_date'])) {
			$data['acceptance_date'] = date('d-m-Y', strtotime($this->request->post['acceptance_date']));
		} elseif (!empty($category_info)) {
			$data['acceptance_date'] = date('d-m-Y', strtotime($category_info['acceptance_date']));
		} else {
			$data['acceptance_date'] = '';
		}*/
		/*if (isset($this->request->post['declaration_date'])) {
			$data['declaration_date'] = date('d-m-Y', strtotime($this->request->post['declaration_date']));
		} elseif (!empty($category_info)) {
			$data['declaration_date'] = date('d-m-Y', strtotime($category_info['declaration_date']));
		} else {
			$data['declaration_date'] = '';
		}*/
		/*if (isset($this->request->post['run_thrice_date'])) {
			$data['run_thrice_date'] = date('d-m-Y', strtotime($this->request->post['run_thrice_date']));
		} elseif (!empty($category_info)) {
			$data['run_thrice_date'] = date('d-m-Y', strtotime($category_info['run_thrice_date']));
		} else {
			$data['run_thrice_date'] = '';
		}*/
		/*if (isset($this->request->post['run_not_placed_date'])) {
			$data['run_not_placed_date'] = date('d-m-Y', strtotime($this->request->post['run_not_placed_date']));
		} elseif (!empty($category_info)) {
			$data['run_not_placed_date'] = date('d-m-Y', strtotime($category_info['run_not_placed_date']));
		} else {
			$data['run_not_placed_date'] = '';
		}*/
		/*if (isset($this->request->post['run_not_won_date'])) {
			$data['run_not_won_date'] = date('d-m-Y', strtotime($this->request->post['run_not_won_date']));
		} elseif (!empty($category_info)) {
			$data['run_not_won_date'] = date('d-m-Y', strtotime($category_info['run_not_won_date']));
		} else {
			$data['run_not_won_date'] = '';
		}*/
		/*if (isset($this->request->post['run_and_not_won_during_mtg_date'])) {
			$data['run_and_not_won_during_mtg_date'] = date('d-m-Y', strtotime($this->request->post['run_and_not_won_during_mtg_date']));
		} elseif (!empty($category_info)) {
			$data['run_and_not_won_during_mtg_date'] = date('d-m-Y', strtotime($category_info['run_and_not_won_during_mtg_date']));
		} else {
			$data['run_and_not_won_during_mtg_date'] = '';
		}*/
		if (isset($this->request->post['age_from'])) {
			$data['age_from'] = $this->request->post['age_from'];
		} elseif (!empty($category_info)) {
			$data['age_from'] = $category_info['age_from'];
		} else {
			$data['age_from'] = '';
		}

		if (isset($this->request->post['age_to'])) {
			$data['age_to'] = $this->request->post['age_to'];
		} elseif (!empty($category_info)) {
			$data['age_to'] = $category_info['age_to'];
		} else {
			$data['age_to'] = '';
		}
		if (isset($this->request->post['max_win'])) {
			$data['max_win'] = $this->request->post['max_win'];
		} elseif (!empty($category_info)) {
			$data['max_win'] = $category_info['max_win'];
		} else {
			$data['max_win'] = '';
		}
		if (isset($this->request->post['sex'])) {
			$data['sex'] = $this->request->post['sex'];
		} elseif (!empty($category_info)) {
			$data['sex'] = $category_info['sex'];
		} else {
			$data['sex'] = '';
		}
		if (isset($this->request->post['maidens'])) {
			$data['maidens'] = $this->request->post['maidens'];
		} elseif (!empty($category_info)) {
			$data['maidens'] = $category_info['maidens'];
		} else {
			$data['maidens'] = '';
		}
		if (isset($this->request->post['unraced'])) {
			$data['unraced'] = $this->request->post['unraced'];
		} elseif (!empty($category_info)) {
			$data['unraced'] = $category_info['unraced'];
		} else {
			$data['unraced'] = '';
		}
		if (isset($this->request->post['not1_2_3'])) {
			$data['not1_2_3'] = $this->request->post['not1_2_3'];
		} elseif (!empty($category_info)) {
			$data['not1_2_3'] = $category_info['not1_2_3'];
		} else {
			$data['not1_2_3'] = '';
		}
		if (isset($this->request->post['unplaced'])) {
			$data['unplaced'] = $this->request->post['unplaced'];
		} elseif (!empty($category_info)) {
			$data['unplaced'] = $category_info['unplaced'];
		} else {
			$data['unplaced'] = '';
		}
		if (isset($this->request->post['short_not'])) {
			$data['short_not'] = $this->request->post['short_not'];
		} elseif (!empty($category_info)) {
			$data['short_not'] = $category_info['short_not'];
		} else {
			$data['short_not'] = '';
		}
		if (isset($this->request->post['max_stakes_race'])) {
			$data['max_stakes_race'] = $this->request->post['max_stakes_race'];
		} elseif (!empty($category_info)) {
			$data['max_stakes_race'] = $category_info['max_stakes_race'];
		} else {
			$data['max_stakes_race'] = '';
		}
		if (isset($this->request->post['run_thrice'])) {
			$data['run_thrice'] = $this->request->post['run_thrice'];
		} elseif (!empty($category_info)) {
			$data['run_thrice'] = $category_info['run_thrice'];
		} else {
			$data['run_thrice'] = '';
		}
		if (isset($this->request->post['run_not_placed'])) {
			$data['run_not_placed'] = $this->request->post['run_not_placed'];
		} elseif (!empty($category_info)) {
			$data['run_not_placed'] = $category_info['run_not_placed'];
		} else {
			$data['run_not_placed'] = '';
		}
		if (isset($this->request->post['run_not_won'])) {
			$data['run_not_won'] = $this->request->post['run_not_won'];
		} elseif (!empty($category_info)) {
			$data['run_not_won'] = $category_info['run_not_won'];
		} else {
			$data['run_not_won'] = '';
		}
		if (isset($this->request->post['run_and_not_won_during_mtg'])) {
			$data['run_and_not_won_during_mtg'] = $this->request->post['run_and_not_won_during_mtg'];
		} elseif (!empty($category_info)) {
			$data['run_and_not_won_during_mtg'] = $category_info['run_and_not_won_during_mtg'];
		} else {
			$data['run_and_not_won_during_mtg'] = '';
		}
		if (isset($this->request->post['no_whip'])) {
			$data['no_whip'] = $this->request->post['no_whip'];
		} elseif (!empty($category_info)) {
			$data['no_whip'] = $category_info['no_whip'];
		} else {
			$data['no_whip'] = '';
		}
		if (isset($this->request->post['foreign_jockey_eligible'])) {
			$data['foreign_jockey_eligible'] = $this->request->post['foreign_jockey_eligible'];
		} elseif (!empty($category_info)) {
			$data['foreign_jockey_eligible'] = $category_info['foreign_jockey_eligible'];
		} else {
			$data['foreign_jockey_eligible'] = '';
		}
		if (isset($this->request->post['foreign_horse_eligible'])) {
			$data['foreign_horse_eligible'] = $this->request->post['foreign_horse_eligible'];
		} elseif (!empty($category_info)) {
			$data['foreign_horse_eligible'] = $category_info['foreign_horse_eligible'];
		} else {
			$data['foreign_horse_eligible'] = '';
		}
		if (isset($this->request->post['sire_dam'])) {
			$data['sire_dam'] = $this->request->post['sire_dam'];
		} elseif (!empty($category_info)) {
			$data['sire_dam'] = $category_info['sire_dam'];
		} else {
			$data['sire_dam'] = '';
		}
		if (isset($this->request->post['stakes_in'])) {
			$data['stakes_in'] = $this->request->post['stakes_in'];
		} elseif (!empty($category_info)) {
			$data['stakes_in'] = $category_info['stakes_in'];
		} else {
			$data['stakes_in'] = '';
		}
		if (isset($this->request->post['trophy_value_amount'])) {
			$data['trophy_value_amount'] = $this->request->post['trophy_value_amount'];
		} elseif (!empty($category_info)) {
			$data['trophy_value_amount'] = $category_info['trophy_value_amount'];
		} else {
			$data['trophy_value_amount'] = '';
		}
		if (isset($this->request->post['positions'])) {
			$data['positions'] = $this->request->post['positions'];
		} elseif (!empty($category_info)) {
			$data['positions'] = $category_info['positions'];
		} else {
			$data['positions'] = '';
		}
		if (isset($this->request->post['stakes_money'])) {
			$data['stakes_money'] = $this->request->post['stakes_money'];
		} elseif (!empty($category_info)) {
			$data['stakes_money'] = $category_info['stakes_money'];
		} else {
			$data['stakes_money'] = '';
		}
		if (isset($this->request->post['bonus_applicable'])) {
			$data['bonus_applicable'] = $this->request->post['bonus_applicable'];
		} elseif (!empty($category_info)) {
			$data['bonus_applicable'] = $category_info['bonus_applicable'];
		} else {
			$data['bonus_applicable'] = '';
		}

		// echo'<pre>';
		// print_r($category_info);
		// exit;
		if (isset($this->request->post['sweepstake_race'])) {
			$data['sweepstake_race'] = $this->request->post['sweepstake_race'];
		} elseif (!empty($category_info)) {
			$data['sweepstake_race'] = $category_info['sweepstake_race'];
		} else {
			$data['sweepstake_race'] = '';
		}
		if (isset($this->request->post['sweepstake_of'])) {
			$data['sweepstake_of'] = $this->request->post['sweepstake_of'];
		} elseif (!empty($category_info)) {
			$data['sweepstake_of'] = $category_info['sweepstake_of'];
		} else {
			$data['sweepstake_of'] = '';
		}
		if (isset($this->request->post['no_of_forfiet'])) {
			$data['no_of_forfiet'] = $this->request->post['no_of_forfiet'];
		} elseif (!empty($category_info)) {
			$data['no_of_forfiet'] = $category_info['no_of_forfiet'];
		} else {
			$data['no_of_forfiet'] = '';
		}
		if (isset($this->request->post['entry_date'])) {
			$data['entry_date'] = $this->request->post['entry_date'];
		} elseif (!empty($category_info)) {
			$data['entry_date'] = $category_info['entry_date'];
		} else {
			$data['entry_date'] = '';
		}
		if (isset($this->request->post['entry_time'])) {
			$data['entry_time'] = $this->request->post['entry_time'];
		} elseif (!empty($category_info)) {
			$data['entry_time'] = $category_info['entry_time'];
		} else {
			$data['entry_time'] = '';
		}
		if (isset($this->request->post['entry_fees'])) {
			$data['entry_fees'] = $this->request->post['entry_fees'];
		} elseif (!empty($category_info)) {
			$data['entry_fees'] = $category_info['entry_fees'];
		} else {
			$data['entry_fees'] = '';
		}

		if (isset($this->request->post['forefit_date'])) {
			$data['forefit_date'] = $this->request->post['forefit_date'];
		} elseif (!empty($category_info)) {
			$data['forefit_date'] = $category_info['forefit_date'];
		} else {
			$data['forefit_date'] = '';
		}

		if (isset($this->request->post['forefit_time'])) {
			$data['forefit_time'] = $this->request->post['forefit_time'];
		} elseif (!empty($category_info)) {
			$data['forefit_time'] = $category_info['forefit_time'];
		} else {
			$data['forefit_time'] = '';
		}

		if (isset($this->request->post['forefit_amt'])) {
			$data['forefit_amt'] = $this->request->post['forefit_amt'];
		} elseif (!empty($category_info)) {
			$data['forefit_amt'] = $category_info['forefit_amt'];
		} else {
			$data['forefit_amt'] = '';
		}



		if (isset($this->request->post['additional_entry'])) {
			$data['additional_entry'] = $this->request->post['additional_entry'];
		} elseif (!empty($category_info)) {
			$data['additional_entry'] = $category_info['additional_entry'];
		} else {
			$data['additional_entry'] = '';
		}
		if (isset($this->request->post['bonus_to_breeder'])) {
			$data['bonus_to_breeder'] = $this->request->post['bonus_to_breeder'];
		} elseif (!empty($category_info)) {
			$data['bonus_to_breeder'] = $category_info['bonus_to_breeder'];
		} else {
			$data['bonus_to_breeder'] = '';
		}
		if (isset($this->request->post['remarks'])) {
			$data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($category_info)) {
			$data['remarks'] = $category_info['remarks'];
		} else {
			$data['remarks'] = '';
		}

		if (isset($this->request->post['lower_class_aligible'])) {
			$data['lower_class_aligible'] = $this->request->post['lower_class_aligible'];
		} elseif (!empty($category_info)) {
			$data['lower_class_aligible'] = $category_info['lower_class_aligible'];
		} else {
			$data['lower_class_aligible'] = '';
		}


		if (isset($this->request->post['stakes_per'])) {
			$data['stakes_per'] = $this->request->post['stakes_per'];
		} elseif (!empty($category_info)) {
			$data['stakes_per'] = $category_info['stacks_per'];
		} else {
			$data['stakes_per'] = 100;
		}

		


		$data['stacks_value'] = array();//$this->db->query("SELECT * FROM `stack_value` WHERE 1=1")->rows;


		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/prospectus_form', $data));
	}
	

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/prospectus')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		if($this->request->post['race_date'] == ''){
			
			$this->error['valierr_race_date'] = 'Please Enter Race Date';
		}
		if ($this->request->post['entries_close_date'] != '' && $this->request->post['race_date'] != '') {
			$s_dates = explode('-', $this->request->post['race_date']);
			$s_day = $s_dates[0];
			$s_month = $s_dates[1];
			$s_year = $s_dates[2];
			

			$e_dates = explode('-', $this->request->post['entries_close_date']);
			$e_day = $e_dates[0];
			$e_month = $e_dates[1];
			$e_year = $e_dates[2];

			if($s_year < $e_year){
				$this->error['valierr_greater_race_date'] = 'Date must not be less than Race Date!';
			} else if ($e_month > $s_month) {
                if ($s_day > $e_day || $e_day > $s_day) {
                    /*$this->error['valierr_greater_race_date'] = '';*/
                } 
            } else if($s_month < $e_month) {
                $this->error['valierr_greater_race_date'] = 'Date must not be less than Race Date!';
            } else if($s_month == $e_month) {
                if ($s_day < $e_day) {
                    $this->error['valierr_greater_race_date'] = 'Date must not be less than Race Date!';
                }
            }
		}
		if ($this->request->post['handicaps_date'] != '' && $this->request->post['race_date'] != '') {

			$s_date2 = explode('-', $this->request->post['race_date']);
			$s_day2 = $s_date2[0];
			$s_month2 = $s_date2[1];
			$s_year2 = $s_date2[2];

			$e_date2 = explode('-', $this->request->post['handicaps_date']);
			$e_day2 = $e_date2[0];
			$e_month2 = $e_date2[1];
			$e_year2 = $e_date2[2];

			if($s_year2 < $e_year2){
				$this->error['valierr_greater_handicaps_date'] = 'Date must not be less than Handicap\'s Date!';
			} else if ($e_month2 > $s_month2) {
                if ($s_day2 > $e_day2 || $e_day2 > $s_day2) {
                   /* $this->error['valierr_greater_handicaps_date'] = '';*/
                } 
            } else if($s_month2 < $e_month2) {
                $this->error['valierr_greater_handicaps_date'] = 'Date must not be less than Handicap\'s Date!';
            } else if($s_month2 == $e_month2) {
                if ($s_day2 < $e_day2) {
                    $this->error['valierr_greater_handicaps_date'] = 'Date must not be less than Handicap\'s Date!';
                }
            }
		}
		if ($this->request->post['acceptance_date'] != '' && $this->request->post['race_date'] != '') {

			$s_dates = explode('-', $this->request->post['race_date']);
			$s_day = $s_dates[0];
			$s_month = $s_dates[1];
			$s_year = $s_dates[2];

			$e_dates = explode('-', $this->request->post['acceptance_date']);
			$e_day = $e_dates[0];
			$e_month = $e_dates[1];
			$e_year = $e_dates[2];

			if($s_year < $e_year){
				$this->error['valierr_greater_acceptance_date'] = 'Date must not be less than Acceptance\'s Date!';
			} else if ($e_month > $s_month) {
                if ($s_day > $e_day || $e_day > $s_day) {
                   /* $this->error['valierr_greater_acceptance_date'] = '';*/
                } 
            } else if($s_month < $e_month) {
                $this->error['valierr_greater_acceptance_date'] = 'Date must not be less than Acceptance\'s Date!';
            } else if($s_month == $e_month) {
                if ($s_day < $e_day) {
                    $this->error['valierr_greater_acceptance_date'] = 'Date must not be less than Acceptance\'s Date!';
                }
            }
		}
		if ($this->request->post['declaration_date'] != '' && $this->request->post['race_date'] != '') {

			$s_dates = explode('-', $this->request->post['race_date']);
			$s_day = $s_dates[0];
			$s_month = $s_dates[1];
			$s_year = $s_dates[2];

			$e_dates = explode('-', $this->request->post['declaration_date']);
			$e_day = $e_dates[0];
			$e_month = $e_dates[1];
			$e_year = $e_dates[2];

			if($s_year < $e_year){
				$this->error['valierr_greater_declaration_date'] = 'Date must not be less than Declaration Date!';
			} else if ($e_month > $s_month) {
                if ($s_day > $e_day || $e_day > $s_day) {
                   /* $this->error['valierr_greater_declaration_date'] = '';*/
                } 
            } else if($s_month < $e_month) {
                $this->error['valierr_greater_declaration_date'] = 'Date must not be less than Declaration Date!';
            } else if($s_month == $e_month) {
                if ($s_day < $e_day) {
                    $this->error['valierr_greater_declaration_date'] = 'Date must not be less than Declaration Date!';
                }
            }
		}
		if($this->request->post['race_name'] == ''){
			
			$this->error['valierr_race_name'] = 'Please Enter Race Name';
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'transaction/prospectus')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'transaction/prospectus')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}


	public function autocompleteracename() {
        $json = array();
        if (isset($this->request->get['race_name'])) {
            $this->load->model('transaction/prospectus');
            $results = $this->model_transaction_prospectus->getRacenameauto($this->request->get['race_name']);
            if($results){
                foreach ($results as $result) {
                    $json[] = array(
                        'race_name'        => strip_tags(html_entity_decode($result['race_name'], ENT_QUOTES, 'UTF-8')),
                       
                    );
                }
            }
        }
        $sort_order = array();
        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['race_name'];
            
        }
        array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }


	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('transaction/prospectus');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'owner_Id'	  => $this->request->get['owner_Id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_prospectus->getOwners($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'owner_id' => $result['id'],
					'owner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['owner_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getCateDescOfClass(){
		$json = array();
		if(isset($this->request->get['race_type'])){
			//$status = ($this->request->get['lower_check_status'] == 1 ) ? "Yes" : "No";
			$race_dataas = $this->db->query("SELECT * FROM `racetypes` WHERE race_type = '".$this->request->get['race_type']."' AND race_class = '".$this->request->get['class_type']."' ");
			// echo "<pre>";
			// print_r($race_dataas);
			// exit;
			if($race_dataas->num_rows > 0){
				if ($this->request->get['lower_check_status'] == 1) {
					$json['race_description'] = $race_dataas->row['l_description'];
					$json['race_category'] = $race_dataas->row['l_category'];
					$json['rating_from'] = $race_dataas->row['l_rating_from'];
                    $json['rating_to'] = $race_dataas->row['l_rating_to'];
				} else {
					$json['race_description'] = $race_dataas->row['description'];
					$json['race_category'] = $race_dataas->row['category'];
					$json['rating_from'] = $race_dataas->row['rating_from'];
                    $json['rating_to'] = $race_dataas->row['rating_to'];
				}
				//$json['low_class_alig'] = $race_dataas->row['lower_class_eligible'];
			} else {
				$json['race_description'] ='';
				$json['race_category'] = '';
				$json['rating_from'] = '';
                $json['rating_to'] = '';
				$json['low_class_alig'] = '';
			}

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));


		}
	}

	public function getStackDatas(){
		if($this->request->get['race_type'] != ''){
			
			$places = array("Winner", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth");
			$place = '';
			$place_str = '';
			for ($i=0; $i < $this->request->get['positions'] ; $i++) { 
				$place .= $places[$i].',';
			} 

			$place_str = rtrim($place , ',');
			$result_string = "'" . str_replace(",", "','", $place_str) . "'";
			
			$race_class_datas = $this->db->query("SELECT * FROM `stacks` WHERE race_type = '".$this->request->get['race_type']."' AND class = '".$this->request->get['class_type']."' ");
			
			$html = '';
			if($race_class_datas->num_rows > 0){
				$stacks_value = $this->db->query("SELECT * FROM `stacks_value` WHERE stacks_id = '".$race_class_datas->row['stacks_id']."' AND stacks_type IN($result_string) ")->rows;
				if($stacks_value){
                	foreach($stacks_value as $key => $result) {
                    	$html .= '<tr id="id'.$key.'">';
                            $html .= '<td class="text-left">';
                                $html .= '<span>'.$result['stacks_type'] .'</span>';
                                $html .= '<input type="hidden" id="placing_'.$key.'" name="race_rs['. $key.'][placing]" value="'.$result['stacks_type'].'"  />';
                            $html .= '</td>';

                            $html .= '<td class="text-right">';
                                $html .= '<span>'.$result['owner'] .'</span>';
                                $html .= '<input type="hidden" id="owner_'.$key.'" name="race_rs['. $key.'][owner]" value="'.$result['owner'].'"  />';
                            $html .= '</td>';

                            $html .= '<td class="text-right">';
                                $html .= '<span>'.$result['trainer'] .'</span>';
                                $html .= '<input type="hidden" id="trainer_'.$key.'" name="race_rs['. $key.'][trainer]" value="'.$result['trainer'].'"  />';
                            $html .= '</td>';

                            $html .= '<td class="text-right">';
                                $html .= '<span>'.$result['jockey'] .'</span>';
                                $html .= '<input type="hidden" id="jockey_'.$key.'" name="race_rs['. $key.'][jockey]" value="'.$result['jockey'].'"  />';
                            $html .= '</td>';

                             $html .= '<td class="text-right">';
                                $html .= '<span>'.$result['total'] .'</span>';
                                $html .= '<input type="hidden" id="total_'.$key.'" name="race_rs['. $key.'][total]" value="'.$result['total'].'"  />';
                            $html .= '</td>';

                              $html .= '<td class="text-right">';
                                $html .= '<span>'.$result['incentive'] .'</span>';
                                $html .= '<input type="hidden" id="performance_incentive_'.$key.'" name="race_rs['. $key.'][performance_incentive]" value="'.$result['incentive'].'"  />';
                            $html .= '</td>';
                        $html .= '</tr>';
                    }
	                       
	            }
			} 
		}
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getManupulatedStackDatas(){
		if($this->request->get['race_type'] != ''){
			$per_of_decress = $this->request->get['decress_per'];
			
			$places = array("Winner", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth");
			$place = '';
			$place_str = '';
			for ($i=0; $i < $this->request->get['positions'] ; $i++) { 
				$place .= $places[$i].',';
			} 

			$place_str = rtrim($place , ',');
			$result_string = "'" . str_replace(",", "','", $place_str) . "'";
			
			$race_class_datas = $this->db->query("SELECT * FROM `stacks` WHERE race_type = '".$this->request->get['race_type']."' AND class = '".$this->request->get['class_type']."' ");
			
			$html = '';
			if($race_class_datas->num_rows > 0){
				$stacks_value = $this->db->query("SELECT * FROM `stacks_value` WHERE stacks_id = '".$race_class_datas->row['stacks_id']."' AND stacks_type IN($result_string) ")->rows;
				if($stacks_value){
                	foreach($stacks_value as $key => $result) {
                		$total_owner_pay = round((($result['owner'] * $per_of_decress) / 100),0,PHP_ROUND_HALF_UP);
                		$total_trainer_pay = round((($result['trainer'] * $per_of_decress) / 100),0,PHP_ROUND_HALF_UP);
                		$total_jockey_pay = round((($result['jockey'] * $per_of_decress) / 100),0,PHP_ROUND_HALF_UP);
                		$total_amt_pay = $total_owner_pay + $total_trainer_pay + $total_jockey_pay;
                		
                		$total_performance_incentive_pay = round((($result['incentive'] * $per_of_decress) / 100),0,PHP_ROUND_HALF_UP);

                    	$html .= '<tr id="id'.$key.'">';
                            $html .= '<td class="text-left">';
                                $html .= '<span>'.$result['stacks_type'] .'</span>';
                                $html .= '<input type="hidden" id="placing_'.$key.'" name="race_rs['. $key.'][placing]" value="'.$result['stacks_type'].'"  />';
                            $html .= '</td>';

                            $html .= '<td class="text-right">';
                                $html .= '<span>'.$total_owner_pay .'</span>';
                                $html .= '<input type="hidden" id="owner_'.$key.'" name="race_rs['. $key.'][owner]" value="'.$total_owner_pay.'"  />';
                            $html .= '</td>';

                            $html .= '<td class="text-right">';
                                $html .= '<span>'.$total_trainer_pay  .'</span>';
                                $html .= '<input type="hidden" id="trainer_'.$key.'" name="race_rs['. $key.'][trainer]" value="'.$total_trainer_pay .'"  />';
                            $html .= '</td>';

                            $html .= '<td class="text-right">';
                                $html .= '<span>'.$total_jockey_pay .'</span>';
                                $html .= '<input type="hidden" id="jockey_'.$key.'" name="race_rs['. $key.'][jockey]" value="'.$total_jockey_pay.'"  />';
                            $html .= '</td>';

                             $html .= '<td class="text-right">';
                                $html .= '<span>'.$total_amt_pay .'</span>';
                                $html .= '<input type="hidden" id="total_'.$key.'" name="race_rs['. $key.'][total]" value="'.$total_amt_pay.'"  />';
                            $html .= '</td>';

                              $html .= '<td class="text-right">';
                                $html .= '<span>'.$total_performance_incentive_pay .'</span>';
                                $html .= '<input type="hidden" id="performance_incentive_'.$key.'" name="race_rs['. $key.'][performance_incentive]" value="'.$total_performance_incentive_pay.'"  />';
                            $html .= '</td>';
                        $html .= '</tr>';
                    }
	            }
			} 
		}
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function getRaceData(){
		$json = array();
		// echo "<pre>";
		// print_r($this->request->get);
		// exit;
		$race_date = date('Y-m-d', strtotime($this->request->get['race_date']));

		$prospectus_datas = $this->db->query("SELECT * FROM `prospectus` WHERE race_date = '".$race_date."' ");

		// echo "<pre>";
		// print_r($prospectus_datas);
		// exit;

		if ($prospectus_datas->num_rows > 0) {
			$serial_no = $prospectus_datas->num_rows;
			// echo "<pre>";
			// print_r($serial_no);
			// exit;
			foreach ($prospectus_datas->rows as $prospectus_data) {
				$json = array(
					'pros_id' 				=> $prospectus_data['pros_id'],
					'serial_no' 			=> $serial_no,
					'season'        		=> $prospectus_data['season'],
					'year'        			=> $prospectus_data['year'],
					'race_day'        		=> $prospectus_data['race_day'],
					'entries_close_date'    => date('d-m-Y', strtotime($prospectus_data['entries_close_date'])),
					'handicaps_date'        => date('d-m-Y', strtotime($prospectus_data['handicaps_date'])),
					'acceptance_date'       => date('d-m-Y', strtotime($prospectus_data['acceptance_date'])),
					'declaration_date'      => date('d-m-Y', strtotime($prospectus_data['declaration_date'])),
				);
			}
		}
		/*$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['declaration_date'];
		}
		array_multisort($sort_order, SORT_ASC, $json);*/
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
