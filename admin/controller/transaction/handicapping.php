<?php
// require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
class Controllertransactionhandicapping extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/handicapping');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/handicapping');
		$this->getList();
		//$this->rating();
	}

	public function add() {
		$this->load->language('transaction/handicapping');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/handicapping');
		if (($this->request->server['REQUEST_METHOD'] == 'POST')/* && $this->validateForm()*/) {
			$this->model_transaction_handicapping->addProspectus($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function rating() { 
		$sql = "SELECT * FROM entry_horse WHERE pros_id = 2 ORDER BY `horse_rating`  DESC ";
		$horse_data = $this->db->query($sql)->rows;
		
		$max ="SELECT MAX(horse_rating) AS max FROM entry_horse WHERE pros_id = 2 ";
		$max1 = $this->db->query($max)->row;
		$min ="SELECT horse_rating  FROM entry_horse WHERE pros_id = 2 ORDER BY horse_rating ASc";
		$min1 = $this->db->query($min)->rows;
		foreach ($min1 as $key => $value) {
			$sql =("SELECT Rating From horse_weight WHERE Class ='Class II' AND Rating ='".$value['horse_rating']."' ");
			$query = $this->db->query($sql);
			if($query->num_rows > 0){
				$min_value= $value['horse_rating'];
				break;
			} 
		}
		$class1 ='Class II';
		$top_rating=$max1['max'];
		$bot_rating=$min_value;
		// $class1 ='Class V';
		// $top_rating=8;
		// $bot_rating=12;
		$this->load->model('transaction/handicapping');
		$result =$this->model_transaction_handicapping->rating($class1,$top_rating,$bot_rating);
		/*echo '<pre>';
		print_r($result);
		exit;*/
	}

	public function edit() {
		$this->load->language('transaction/handicapping');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/handicapping');
		if (($this->request->server['REQUEST_METHOD'] == 'POST')/* && $this->validateForm()*/) {
			$this->model_transaction_handicapping->editProspectus($this->request->get['pros_id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit1() { 
		$json = array();
		if (isset($this->request->get['pros_id'])) {
			$this->load->model('transaction/handicapping');
			$json = $this->model_transaction_handicapping->getpopupdata($this->request->get['pros_id']);
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function voidbutton() {
		if (isset($this->request->post)) {
			$void=$this->request->post;
			if(isset($void['shift_void_data'])){
				foreach ($void['shift_void_data'] as $key => $value) {
					$this->db->query("UPDATE prospectus SET race_status = 0 WHERE pros_id='".$value['pros_id']."'");
				}
			}
		}
		$json='';
		// $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function shiftbutton() {
		$date = date('Y-m-d', strtotime($this->request->post['date_changed']));
		// 	//$date=$this->request->post['date_changed'];
		// 	$date = date('Y-m-d', strtotime($this->request->post['date_changed']));
		if (isset($this->request->post)) {
			$date_shift=$this->request->post;
			if(isset($date_shift['shift_date'])){
				foreach ($date_shift['shift_date'] as $key => $value) {
					$this->db->query("UPDATE prospectus SET race_date = '".$date."' WHERE pros_id='".$value['pros_id']."'");
				}
			}
		}
		$json='';
		// $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function shiftracebutton() {
		if (isset($this->request->post)) {
			foreach ($this->request->post['shift_void_data'] as $skey => $svalue) {
				$data1  = ("SELECT * FROM `prospectus` WHERE pros_id='".$svalue['pros_id']."'");
				$final_data = $this->db->query($data1)->row;
				$data2[] = array(
					'pros_id' => $final_data['pros_id'],
					'race_name'        => $final_data['race_name'],
					'race_type'        => $final_data['race_type'],
					'race_date'        => date('d-m-Y', strtotime($final_data['race_date'])),
				);
			}
			$json = $data2;
			$this->response->setOutput(json_encode($json));
		}
	}
		
	public function selectedfunction11() {
		$string_from_array =$this->request->get['pros_id'];
		$data= preg_split("/[,]/",$string_from_array);
		if (isset($this->request->get['pros_id'])) {
			foreach ($data as $key => $value) {
				$data1  = ("SELECT * FROM `prospectus` WHERE pros_id='".$value."'");
				$final_data = $this->db->query($data1)->row;
				$data2[] = array(
					'pros_id' => $final_data['pros_id'],
					'race_name'        => $final_data['race_name'],
					'race_type'        => $final_data['race_type']
				);
			}
			$json = $data2;
			$this->response->setOutput(json_encode($json));
		}
	}

	public function delete() {
		$this->load->language('transaction/handicapping');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/handicapping');
		
		if (isset($this->request->post['selected'])/* && $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $pros_id) {
				$this->model_transaction_handicapping->deleteprospectus($pros_id);
			}
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getList();
	}

	public function repair() {
		$this->load->language('transaction/handicapping');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/handicapping');
		if ($this->validateRepair()) {
			$this->model_transaction_handicapping->repairCategories();
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'], true));
		}
		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['filter_race_name'])) {
			$data['filter_race_name'] =$this->request->get['filter_race_name'];
		} else{
			$data['filter_race_name']  ="";
		}

		if(isset($this->request->get['filter_race_date'])){
			$data['filter_race_date'] = $this ->request->get['filter_race_date'];
		}
		 else{
			$data['filter_race_date'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/handicapping/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/handicapping/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/handicapping/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_race_name' => $data['filter_race_name'],
			'filter_race_date' => $data['filter_race_date']
		);
		$category_total = $this->model_transaction_handicapping->getTotalCategories($filter_data);
		$results = $this->model_transaction_handicapping->getprospectus($filter_data);
		foreach ($results as $result) {
			$data['handicappingdatas'][] = array(
				'entry_id' 	  	=> $result['entry_id'],
				'pros_id' 	  	=> $result['pros_id'],
				'race_description' 	  	=> $result['race_description'],
				'race_name'	  	=> $result['race_name'],
				'race_type'	  	=> $result['race_type'],
				'handicap_status' =>$result['handicap_status'],
				'race_date'	  	=> date('d-m-Y', strtotime($result['race_date'])),
				'class'	  	=> $result['class'],
				'distance' =>$result['distance'],
				'foreign_jockey'	  	=> $result['foreign_jockey_eligible'],
				'edit'        	=> $this->url->link('transaction/handicapping/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $result['pros_id'] . $url, true),
			);
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/handicapping_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['pros_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');


		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_horse_name'])) {
			$filter_horse_name =$this->request->get['filter_horse_name'];
		} else{
			$filter_horse_name  ="";

		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$filter_data1 = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_horse_name' => $filter_horse_name
		);
		if (isset($this->request->get['pros_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_transaction_handicapping->getCategory($this->request->get['pros_id']);

		}

		$data['horseDatas'] =array();

		$results = $this->model_transaction_handicapping->getHorses($category_info['age_from'],$category_info['age_to'],$category_info['sex']);
		//echo "<pre>";print_r($results);exit;
		
		foreach ($results as $result) {
			$data['horseDatas'][] = array(
				'horseID' 	  			=> $result['horseseq'],
				'horseName'   			=> $result['official_name'],
				'horseColor'  			=> $result['color'],
				'horseSex'  			=> $result['sex'],
				'horseAge'  			=> $result['age'],
				'horseRating'  			=> $result['rating'],
				'trainer_name'  			=> $result['trainer_name'],
				'trainer_id'   =>$result['trainer_id']
			);
		}
		 

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['linkedOwner'])) {
			$data['error_linkedOwner'] = $this->error['linkedOwner'];
		} else {
			$data['error_linkedOwner'] = '';
		}

		if (isset($this->error['gst_number'])) {
			$data['error_gst_number'] = $this->error['gst_number'];
		} else {
			$data['error_gst_number'] = '';
		}
		
		if (isset($this->error['sharedColor'])) {
			$data['error_sharedColor'] = $this->error['sharedColor'];
		} else {
			$data['error_sharedColor'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/handicapping/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/handicapping/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'] . $url, true);

		
		
		if(isset($this->request->get['pros_id'])){
			$prize_distribution = $this->model_transaction_handicapping->getprizedistribution($this->request->get['pros_id']);
			
		}
		if(isset($this->request->get['pros_id'])){
			
			$entrydata1 =$this->model_transaction_handicapping->getentrydatas1($this->request->get['pros_id']);
			
		}
		if(isset($this->request->get['pros_id'])){
			
			$entrydata =$this->model_transaction_handicapping->getentrydatas($this->request->get['pros_id']);
			
		}
		if(isset($this->request->get['pros_id'])){
			
			$entrydata_id =$this->model_transaction_handicapping->getentrydata_id($this->request->get['pros_id']);
			
		}
		
		$data['token'] = $this->session->data['token'];
		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['count'])) {
			$data['count'] = $this->request->post['count'];
		} elseif (!empty($entrydata)) {
			$data['count'] = $entrydata['count'];
		} else {
			$data['count'] = '';
		}


		if (isset($this->request->post['race_description'])) {
			$data['race_description'] = $this->request->post['race_description'];
		} elseif(!empty($entrydata)){
			if($entrydata['race_description'] != ''){
				$data['race_description'] = $entrydata['race_description'];
			} elseif($entrydata['race_description'] == ''){
				$data['race_description'] = $category_info['race_description'];
			}
		} else {
			$data['race_description'] = '';
		}

		if (!empty($entrydata1)) {
			$data['entrydata1s'] = $entrydata1;
		} else {
      		$data['entrydata1s'] = array();
    	}

    	if (!empty($entrydata_id)) {
			$data['entrydata_id'] = $entrydata_id;
		} else {
      		$data['entrydata_id'] = array();
    	}
    	$abc = '';

    	foreach ($data['entrydata_id'] as $tnamekey => $tnamevalue) {
    		//$abc .= $tnamevalue['horse_id'].',';
    		$abc []= $tnamevalue['horse_id'];
		}
		/*echo '<pre>';
    	print_r($abc);
    	exit;*/
		if (!empty($abc)) {
			$data['abc'] = $abc;
		} else {
      		$data['abc'] = array();
    	}
		
		if (!empty($prize_distribution)) {
			$data['prize_distributions'] = $prize_distribution;
		} else {
      		$data['prize_distributions'] = array();
    	}

		if (isset($this->request->post['pros_id'])) {
			$data['pros_id'] = $this->request->post['pros_id'];
		} elseif (!empty($category_info)) {
			$data['pros_id'] = $category_info['pros_id'];
		} else {
			$data['pros_id'] = '';
		}
		if (isset($this->request->post['year'])) {
			$data['year'] = $this->request->post['year'];
		} elseif (!empty($category_info)) {
			$data['year'] = $category_info['year'];
		} else {
			$data['year'] = '';
		}
		if (isset($this->request->post['season'])) {
			$data['season'] = $this->request->post['season'];
		} elseif (!empty($category_info)) {
			$data['season'] = $category_info['season'];
		} else {
			$data['season'] = '';
		}
		if (isset($this->request->post['grade'])) {
			$data['grade'] = $this->request->post['grade'];
		} elseif (!empty($category_info)) {
			$data['grade'] = $category_info['grade'];
		} else {
			$data['grade'] = '';
		}
		if (isset($this->request->post['class'])) {
			$data['class'] = $this->request->post['class'];
		} elseif (!empty($category_info)) {
			$data['class'] = $category_info['class'];
		} else {
			$data['class'] = '';
		}
		if (isset($this->request->post['race_day'])) {
			$data['race_day'] = $this->request->post['race_day'];
		} elseif (!empty($category_info)) {
			$data['race_day'] = $category_info['race_day'];
		} else {
			$data['race_day'] = '';
		}
		if (isset($this->request->post['evening_race'])) {
			$data['evening_race'] = $this->request->post['evening_race'];
		} elseif (!empty($category_info)) {
			$data['evening_race'] = $category_info['evening_race'];
		} else {
			$data['evening_race'] = '';
		}
		if (isset($this->request->post['race_name'])) {
			$data['race_name'] = $this->request->post['race_name'];
		} elseif (!empty($category_info)) {
			$data['race_name'] = $category_info['race_name'];
		} else {
			$data['race_name'] = '';
		}
		if (isset($this->request->post['race_type'])) {
			$data['race_type'] = $this->request->post['race_type'];
		} elseif (!empty($category_info)) {
			$data['race_type'] = $category_info['race_type'];
		} else {
			$data['race_type'] = '';
		}
		if (isset($this->request->post['race_category'])) {
			$data['race_category'] = $this->request->post['race_category'];
		} elseif (!empty($category_info)) {
			$data['race_category'] = $category_info['race_category'];
		} else {
			$data['race_category'] = '';
		}
		if (isset($this->request->post['distance'])) {
			$data['distance'] = $this->request->post['distance'];
		} elseif (!empty($category_info)) {
			$data['distance'] = $category_info['distance'];
		} else {
			$data['distance'] = '';
		}
		if (isset($this->request->post['entries_close_date_time'])) {
			$data['entries_close_date_time'] = $this->request->post['entries_close_date_time'];
		} elseif (!empty($category_info)) {
			$data['entries_close_date_time'] = $category_info['entries_close_date_time'];
		} else {
			$data['entries_close_date_time'] = '';
		}
		if (isset($this->request->post['handicaps_date_time'])) {
			$data['handicaps_date_time'] = $this->request->post['handicaps_date_time'];
		} elseif (!empty($category_info)) {
			$data['handicaps_date_time'] = $category_info['handicaps_date_time'];
		} else {
			$data['handicaps_date_time'] = '';
		}
		if (isset($this->request->post['acceptance_date_time'])) {
			$data['acceptance_date_time'] = $this->request->post['acceptance_date_time'];
		} elseif (!empty($category_info)) {
			$data['acceptance_date_time'] = $category_info['acceptance_date_time'];
		} else {
			$data['acceptance_date_time'] = '';
		}

		if (isset($this->request->post['declaration_date_time'])) {
			$data['declaration_date_time'] = $this->request->post['declaration_date_time'];
		} elseif (!empty($category_info)) {
			$data['declaration_date_time'] = $category_info['declaration_date_time'];
		} else {
			$data['declaration_date_time'] = '';
		}
		if (isset($this->request->post['race_date'])) {
			$data['race_date'] = date('d-m-Y', strtotime($this->request->post['race_date']));
		} elseif (!empty($category_info)) {
			$data['race_date'] = date('d-m-Y', strtotime($category_info['race_date']));
		} else {
			$data['race_date'] = '';
		}
		if (isset($this->request->post['entries_close_date'])) {
			$data['entries_close_date'] = date('d-m-Y', strtotime($this->request->post['entries_close_date']));
		} elseif (!empty($category_info)) {
			$data['entries_close_date'] = date('d-m-Y', strtotime($category_info['entries_close_date']));
		} else {
			$data['entries_close_date'] = '';
		}
		if (isset($this->request->post['handicaps_date'])) {
			$data['handicaps_date'] = date('d-m-Y', strtotime($this->request->post['handicaps_date']));
		} elseif (!empty($category_info)) {
			$data['handicaps_date'] = date('d-m-Y', strtotime($category_info['handicaps_date']));
		} else {
			$data['handicaps_date'] = '';
		}
		if (isset($this->request->post['acceptance_date'])) {
			$data['acceptance_date'] = date('d-m-Y', strtotime($this->request->post['acceptance_date']));
		} elseif (!empty($category_info)) {
			$data['acceptance_date'] = date('d-m-Y', strtotime($category_info['acceptance_date']));
		} else {
			$data['acceptance_date'] = '';
		}
		if (isset($this->request->post['declaration_date'])) {
			$data['declaration_date'] = date('d-m-Y', strtotime($this->request->post['declaration_date']));
		} elseif (!empty($category_info)) {
			$data['declaration_date'] = date('d-m-Y', strtotime($category_info['declaration_date']));
		} else {
			$data['declaration_date'] = '';
		}
		if (isset($this->request->post['run_thrice_date'])) {
			$data['run_thrice_date'] = date('d-m-Y', strtotime($this->request->post['run_thrice_date']));
		} elseif (!empty($category_info)) {
			$data['run_thrice_date'] = date('d-m-Y', strtotime($category_info['run_thrice_date']));
		} else {
			$data['run_thrice_date'] = '';
		}
		if (isset($this->request->post['run_not_placed_date'])) {
			$data['run_not_placed_date'] = date('d-m-Y', strtotime($this->request->post['run_not_placed_date']));
		} elseif (!empty($category_info)) {
			$data['run_not_placed_date'] = date('d-m-Y', strtotime($category_info['run_not_placed_date']));
		} else {
			$data['run_not_placed_date'] = '';
		}
		if (isset($this->request->post['run_not_won_date'])) {
			$data['run_not_won_date'] = date('d-m-Y', strtotime($this->request->post['run_not_won_date']));
		} elseif (!empty($category_info)) {
			$data['run_not_won_date'] = date('d-m-Y', strtotime($category_info['run_not_won_date']));
		} else {
			$data['run_not_won_date'] = '';
		}
		if (isset($this->request->post['run_and_not_won_during_mtg_date'])) {
			$data['run_and_not_won_during_mtg_date'] = date('d-m-Y', strtotime($this->request->post['run_and_not_won_during_mtg_date']));
		} elseif (!empty($category_info)) {
			$data['run_and_not_won_during_mtg_date'] = date('d-m-Y', strtotime($category_info['run_and_not_won_during_mtg_date']));
		} else {
			$data['run_and_not_won_during_mtg_date'] = '';
		}
		if (isset($this->request->post['age_from'])) {
			$data['age_from'] = $this->request->post['age_from'];
		} elseif (!empty($category_info)) {
			$data['age_from'] = $category_info['age_from'];
		} else {
			$data['age_from'] = '';
		}

		if (isset($this->request->post['age_to'])) {
			$data['age_to'] = $this->request->post['age_to'];
		} elseif (!empty($category_info)) {
			$data['age_to'] = $category_info['age_to'];
		} else {
			$data['age_to'] = '';
		}
		if (isset($this->request->post['max_win'])) {
			$data['max_win'] = $this->request->post['max_win'];
		} elseif (!empty($category_info)) {
			$data['max_win'] = $category_info['max_win'];
		} else {
			$data['max_win'] = '';
		}
		if (isset($this->request->post['sex'])) {
			$data['sex'] = $this->request->post['sex'];
		} elseif (!empty($category_info)) {
			$data['sex'] = $category_info['sex'];
		} else {
			$data['sex'] = '';
		}
		if (isset($this->request->post['maidens'])) {
			$data['maidens'] = $this->request->post['maidens'];
		} elseif (!empty($category_info)) {
			$data['maidens'] = $category_info['maidens'];
		} else {
			$data['maidens'] = '';
		}
		if (isset($this->request->post['unraced'])) {
			$data['unraced'] = $this->request->post['unraced'];
		} elseif (!empty($category_info)) {
			$data['unraced'] = $category_info['unraced'];
		} else {
			$data['unraced'] = '';
		}
		if (isset($this->request->post['not1_2_3'])) {
			$data['not1_2_3'] = $this->request->post['not1_2_3'];
		} elseif (!empty($category_info)) {
			$data['not1_2_3'] = $category_info['not1_2_3'];
		} else {
			$data['not1_2_3'] = '';
		}
		if (isset($this->request->post['unplaced'])) {
			$data['unplaced'] = $this->request->post['unplaced'];
		} elseif (!empty($category_info)) {
			$data['unplaced'] = $category_info['unplaced'];
		} else {
			$data['unplaced'] = '';
		}
		if (isset($this->request->post['short_not'])) {
			$data['short_not'] = $this->request->post['short_not'];
		} elseif (!empty($category_info)) {
			$data['short_not'] = $category_info['short_not'];
		} else {
			$data['short_not'] = '';
		}
		if (isset($this->request->post['max_stakes_race'])) {
			$data['max_stakes_race'] = $this->request->post['max_stakes_race'];
		} elseif (!empty($category_info)) {
			$data['max_stakes_race'] = $category_info['max_stakes_race'];
		} else {
			$data['max_stakes_race'] = '';
		}
		if (isset($this->request->post['run_thrice'])) {
			$data['run_thrice'] = $this->request->post['run_thrice'];
		} elseif (!empty($category_info)) {
			$data['run_thrice'] = $category_info['run_thrice'];
		} else {
			$data['run_thrice'] = '';
		}
		if (isset($this->request->post['run_not_placed'])) {
			$data['run_not_placed'] = $this->request->post['run_not_placed'];
		} elseif (!empty($category_info)) {
			$data['run_not_placed'] = $category_info['run_not_placed'];
		} else {
			$data['run_not_placed'] = '';
		}
		if (isset($this->request->post['run_not_won'])) {
			$data['run_not_won'] = $this->request->post['run_not_won'];
		} elseif (!empty($category_info)) {
			$data['run_not_won'] = $category_info['run_not_won'];
		} else {
			$data['run_not_won'] = '';
		}
		if (isset($this->request->post['run_and_not_won_during_mtg'])) {
			$data['run_and_not_won_during_mtg'] = $this->request->post['run_and_not_won_during_mtg'];
		} elseif (!empty($category_info)) {
			$data['run_and_not_won_during_mtg'] = $category_info['run_and_not_won_during_mtg'];
		} else {
			$data['run_and_not_won_during_mtg'] = '';
		}
		if (isset($this->request->post['no_whip'])) {
			$data['no_whip'] = $this->request->post['no_whip'];
		} elseif (!empty($category_info)) {
			$data['no_whip'] = $category_info['no_whip'];
		} else {
			$data['no_whip'] = '';
		}
		if (isset($this->request->post['foreign_jockey_eligible'])) {
			$data['foreign_jockey_eligible'] = $this->request->post['foreign_jockey_eligible'];
		} elseif (!empty($category_info)) {
			$data['foreign_jockey_eligible'] = $category_info['foreign_jockey_eligible'];
		} else {
			$data['foreign_jockey_eligible'] = '';
		}
		if (isset($this->request->post['foreign_horse_eligible'])) {
			$data['foreign_horse_eligible'] = $this->request->post['foreign_horse_eligible'];
		} elseif (!empty($category_info)) {
			$data['foreign_horse_eligible'] = $category_info['foreign_horse_eligible'];
		} else {
			$data['foreign_horse_eligible'] = '';
		}
		if (isset($this->request->post['sire_dam'])) {
			$data['sire_dam'] = $this->request->post['sire_dam'];
		} elseif (!empty($category_info)) {
			$data['sire_dam'] = $category_info['sire_dam'];
		} else {
			$data['sire_dam'] = '';
		}
		if (isset($this->request->post['stakes_in'])) {
			$data['stakes_in'] = $this->request->post['stakes_in'];
		} elseif (!empty($category_info)) {
			$data['stakes_in'] = $category_info['stakes_in'];
		} else {
			$data['stakes_in'] = '';
		}
		if (isset($this->request->post['trophy_value_amount'])) {
			$data['trophy_value_amount'] = $this->request->post['trophy_value_amount'];
		} elseif (!empty($category_info)) {
			$data['trophy_value_amount'] = $category_info['trophy_value_amount'];
		} else {
			$data['trophy_value_amount'] = '';
		}
		if (isset($this->request->post['positions'])) {
			$data['positions'] = $this->request->post['positions'];
		} elseif (!empty($category_info)) {
			$data['positions'] = $category_info['positions'];
		} else {
			$data['positions'] = '';
		}
		if (isset($this->request->post['stakes_money'])) {
			$data['stakes_money'] = $this->request->post['stakes_money'];
		} elseif (!empty($category_info)) {
			$data['stakes_money'] = $category_info['stakes_money'];
		} else {
			$data['stakes_money'] = '';
		}
		if (isset($this->request->post['bonus_applicable'])) {
			$data['bonus_applicable'] = $this->request->post['bonus_applicable'];
		} elseif (!empty($category_info)) {
			$data['bonus_applicable'] = $category_info['bonus_applicable'];
		} else {
			$data['bonus_applicable'] = '';
		}
		if (isset($this->request->post['sweepstake_race'])) {
			$data['sweepstake_race'] = $this->request->post['sweepstake_race'];
		} elseif (!empty($category_info)) {
			$data['sweepstake_race'] = $category_info['sweepstake_race'];
		} else {
			$data['sweepstake_race'] = '';
		}
		if (isset($this->request->post['sweepstake_of'])) {
			$data['sweepstake_of'] = $this->request->post['sweepstake_of'];
		} elseif (!empty($category_info)) {
			$data['sweepstake_of'] = $category_info['sweepstake_of'];
		} else {
			$data['sweepstake_of'] = '';
		}
		if (isset($this->request->post['no_of_forfiet'])) {
			$data['no_of_forfiet'] = $this->request->post['no_of_forfiet'];
		} elseif (!empty($category_info)) {
			$data['no_of_forfiet'] = $category_info['no_of_forfiet'];
		} else {
			$data['no_of_forfiet'] = '';
		}
		if (isset($this->request->post['entry_date'])) {
			$data['entry_date'] = $this->request->post['entry_date'];
		} elseif (!empty($category_info)) {
			$data['entry_date'] = $category_info['entry_date'];
		} else {
			$data['entry_date'] = '';
		}
		if (isset($this->request->post['entry_time'])) {
			$data['entry_time'] = $this->request->post['entry_time'];
		} elseif (!empty($category_info)) {
			$data['entry_time'] = $category_info['entry_time'];
		} else {
			$data['entry_time'] = '';
		}
		if (isset($this->request->post['entry_fees'])) {
			$data['entry_fees'] = $this->request->post['entry_fees'];
		} elseif (!empty($category_info)) {
			$data['entry_fees'] = $category_info['entry_fees'];
		} else {
			$data['entry_fees'] = '';
		}
		if (isset($this->request->post['additional_entry'])) {
			$data['additional_entry'] = $this->request->post['additional_entry'];
		} elseif (!empty($category_info)) {
			$data['additional_entry'] = $category_info['additional_entry'];
		} else {
			$data['additional_entry'] = '';
		}
		if (isset($this->request->post['bonus_to_breeder'])) {
			$data['bonus_to_breeder'] = $this->request->post['bonus_to_breeder'];
		} elseif (!empty($category_info)) {
			$data['bonus_to_breeder'] = $category_info['bonus_to_breeder'];
		} else {
			$data['bonus_to_breeder'] = '';
		}
		if (isset($this->request->post['remarks'])) {
			$data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($category_info)) {
			$data['remarks'] = $category_info['remarks'];
		} else {
			$data['remarks'] = '';
		}

		if (isset($this->request->post['lower_class_aligible'])) {
			$data['lower_class_aligible'] = $this->request->post['lower_class_aligible'];
		} elseif (!empty($category_info)) {
			$data['lower_class_aligible'] = $category_info['lower_class_aligible'];
		} else {
			$data['lower_class_aligible'] = '';
		}

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/handicapping_form', $data));
	}
	

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/handicapping')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['linkedOwnerName'] as $key => $value) {
			if ($value['linked_owner_id'] == '') {
				$this->error['linkedOwner'] = 'Please select valid linked owner.';
			}
		}

		if ($this->request->post['gstType'] == 'Registered' && $this->request->post['gstNo'] == '') {
			$this->error['gst_number'] = 'GST No can not be blank.';
		}

		foreach ($this->request->post['owner_shared_color'] as $key => $value) {
			if ($value['shared_owner_id'] == '') {
				$this->error['sharedColor'] = 'Please select valid owner to share color.';
			}
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'transaction/handicapping')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'transaction/handicapping')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('transaction/handicapping');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'owner_Id'	  => $this->request->get['owner_Id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_handicapping->getOwners($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'owner_id' => $result['id'],
					'owner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['owner_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function report() {
		if (isset($this->request->get['filter_race_date'])) {
			$race_date = date('Y-m-d', strtotime($this->request->get['filter_race_date']));
		}

		if (isset($this->request->get['filter_race_name'])) {
			$race_name = $this->request->get['filter_race_name'];
		}

		$data['report_data'] = array();

		$data['horse_data'] = array();

		$sql = "SELECT * FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id) WHERE entry.entry_id <> '' ";

		if (!empty($race_name)) {
			$sql .= " AND prospectus.race_name LIKE '%" . $race_name . "%'";
		}

		if (!empty($race_date)) {
			$sql .= " AND prospectus.race_date LIKE '%" . $race_date . "%'";
		}

		$report_data = $this->db->query($sql)->rows;

		if (!empty($report_data)) {

			foreach ($report_data as $key => $value) {
				$min_value = '';
				$this->db->query("UPDATE `prospectus` SET `handicap_status` = '1' WHERE `pros_id` = '".$value['pros_id']."' ");

				$race_date = $value['race_date'];
				$race_dates = date('d-F-Y', strtotime($race_date));
				$final_race_date = explode('-', $race_dates);
				$race_date_day  = $final_race_date[0];
				$race_date_month = $final_race_date[1];
				$race_date_year  = $final_race_date[2];
				$race_day = date('l-F-Y', strtotime($race_date));
				$final_race_day = explode('-', $race_day);
				$race_date_days  = $final_race_day[0];
				$sql = "SELECT * FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ORDER BY `horse_rating`  DESC ";
				$horse_data = $this->db->query($sql)->rows;
				
				$max ="SELECT MAX(horse_rating) AS max FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ";
				$max1 = $this->db->query($max)->row;
				$min ="SELECT horse_rating  FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ORDER BY horse_rating ASC";
				$min1 = $this->db->query($min)->rows;
				foreach ($min1 as $mkey => $mvalue) {
					$sql =("SELECT Rating From horse_weight WHERE Class ='".$value['class']."' AND Rating ='".$mvalue['horse_rating']."' ");
					$query = $this->db->query($sql);
					if($query->num_rows > 0){
						$min_value = $mvalue['horse_rating'];
						break;
					} else {
						$min_value = '';
					}
				}
				$class1 = $value['class'];
				$top_rating=$max1['max'];
				$bot_rating=$min_value;
				
				$this->load->model('transaction/handicapping');
				$result =$this->model_transaction_handicapping->rating($class1,$top_rating,$bot_rating);

				if($result['res_n'] !=''){
					$final_min_value=$result['res_n'];
				}else if($result['res_p'] !=''){
					$final_min_value=$result['res_p'];
				}
				else {
					$final_min_value='';
				}
				
				$acceptance_date = $value['acceptance_date'];
				$acceptance_dates = date('d-F-Y', strtotime($acceptance_date));
				$final_acceptance_date = explode('-', $acceptance_dates);
				$acceptance_date_day  = $final_acceptance_date[0];
				$acceptance_date_month = $final_acceptance_date[1];
				$acceptance_date_year  = $final_acceptance_date[2];
				$acceptance_day= date('l-F-Y', strtotime($acceptance_date));
				$final_acceptance_day = explode('-', $acceptance_day);
				$acceptance_date_days  = $final_acceptance_day[0];
				$acceptance_date_times = $value['acceptance_date_time'];
				$final_acceptance_date_times = str_replace(".", ":", $acceptance_date_times);

				$declaration_date = $value['declaration_date'];
				$declaration_dates = date('d-F-Y', strtotime($declaration_date));
				$final_declaration_date = explode('-', $declaration_dates);
				$declaration_date_day  = $final_declaration_date[0];
				$declaration_date_month = $final_declaration_date[1];
				$declaration_date_year  = $final_declaration_date[2];
				$declaration_day= date('l-F-Y', strtotime($declaration_date));
				$final_declaration_day = explode('-', $declaration_day);
				$declaration_date_days  = $final_declaration_day[0];
				$declaration_date_times = $value['declaration_date_time'];
				$final_declaration_date_times = str_replace(".", ":", $declaration_date_times);
				
				$data['report_data'][] = array(
					'race_name' => $value['race_name'],
					'race_description' => $value['race_description'],
					'distance' => $value['distance'],
					'season' => $value['season'],
					'year' => $value['year'],
					'race_day' => $value['race_day'],
					'race_date_days' => $race_date_days,
					'race_date_day' => $race_date_day,
					'race_date_month' => $race_date_month,
					'race_date_year' => $race_date_year,
					'race_name' => $value['race_name'],
					'race_description' => $value['race_description'],
					'final_acceptance_date_times' => $final_acceptance_date_times,
					'acceptance_date_days' => $acceptance_date_days,
					'acceptance_date_day' => $acceptance_date_day,
					'acceptance_date_month' => $acceptance_date_month,
					'acceptance_date_year' => $acceptance_date_year,
					'final_declaration_date_times' => $final_declaration_date_times,
					'declaration_date_days' => $declaration_date_days,
					'declaration_date_day' => $declaration_date_day,
					'declaration_date_month' => $declaration_date_month,
					'declaration_date_year' => $declaration_date_year,
				);
				foreach ($horse_data as $hkey => $hvalue) {
					if($hvalue['weight'] !=''){
						$weight = $hvalue['weight'] + $final_min_value;
					}else{
						$weight = '';
					}
					$data['horse_data'][] = array(
						'horse_rating' => $hvalue['horse_rating'],
						'horse_name' => $hvalue['horse_name'],
						'weight' => $weight,
					);
				}
			}
		}

		$data['base'] = HTTP_SERVER;
 		$html = $this->load->view('catalog/all_report_html', $data);
		$filename = 'Handicap Report.html';
		header('Content-type: text/html');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $html;exit;
	}


	public function reports() {

		$sql = "SELECT * FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id) WHERE entry.entry_id = '".$this->request->get['entry_id']."' ";

		$report_data = $this->db->query($sql)->rows;

		if (!empty($report_data)) {

			foreach ($report_data as $key => $value) {
				$this->db->query("UPDATE `prospectus` SET `handicap_status` = '1' WHERE `pros_id` = '".$value['pros_id']."' ");
				$race_date = $value['race_date'];
				$race_dates = date('d-F-Y', strtotime($race_date));
				$final_race_date = explode('-', $race_dates);
				$race_date_day  = $final_race_date[0];
				$race_date_month = $final_race_date[1];
				$race_date_year  = $final_race_date[2];
				$race_day = date('l-F-Y', strtotime($race_date));
				$final_race_day = explode('-', $race_day);
				$race_date_days  = $final_race_day[0];
				$sql = "SELECT * FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ORDER BY `horse_rating`  DESC ";
				$horse_data = $this->db->query($sql)->rows;

				$max ="SELECT MAX(horse_rating) AS max FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ";
				$max1 = $this->db->query($max)->row;
				$min ="SELECT horse_rating  FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ORDER BY horse_rating ASC";
				$min1 = $this->db->query($min)->rows;
				foreach ($min1 as $mkey => $mvalue) {
					$sql =("SELECT Rating From horse_weight WHERE Class ='".$value['class']."' AND Rating ='".$mvalue['horse_rating']."' ");
					$query = $this->db->query($sql);
					if($query->num_rows > 0){
						$min_value= $mvalue['horse_rating'];
						break;
					} 
				}
				$class1 = $value['class'];
				$top_rating=$max1['max'];
				$bot_rating=$min_value;
				
				$this->load->model('transaction/handicapping');
				$result =$this->model_transaction_handicapping->rating($class1,$top_rating,$bot_rating);

				if($result['res_n'] !=''){
					$final_min_value=$result['res_n'];
				}else if($result['res_p'] !=''){
					$final_min_value=$result['res_p'];
				}
				else {
					$final_min_value='';
				}

				$acceptance_date = $value['acceptance_date'];
				$acceptance_dates = date('d-F-Y', strtotime($acceptance_date));
				$final_acceptance_date = explode('-', $acceptance_dates);
				$acceptance_date_day  = $final_acceptance_date[0];
				$acceptance_date_month = $final_acceptance_date[1];
				$acceptance_date_year  = $final_acceptance_date[2];
				$acceptance_day= date('l-F-Y', strtotime($acceptance_date));
				$final_acceptance_day = explode('-', $acceptance_day);
				$acceptance_date_days  = $final_acceptance_day[0];
				$acceptance_date_times = $value['acceptance_date_time'];
				$final_acceptance_date_times = str_replace(".", ":", $acceptance_date_times);

				$declaration_date = $value['declaration_date'];
				$declaration_dates = date('d-F-Y', strtotime($declaration_date));
				$final_declaration_date = explode('-', $declaration_dates);
				$declaration_date_day  = $final_declaration_date[0];
				$declaration_date_month = $final_declaration_date[1];
				$declaration_date_year  = $final_declaration_date[2];
				$declaration_day= date('l-F-Y', strtotime($declaration_date));
				$final_declaration_day = explode('-', $declaration_day);
				$declaration_date_days  = $final_declaration_day[0];
				$declaration_date_times = $value['declaration_date_time'];
				$final_declaration_date_times = str_replace(".", ":", $declaration_date_times);
				
				$data['report_data'][] = array(
					'race_name' => $value['race_name'],
					'race_description' => $value['race_description'],
					'distance' => $value['distance'],
					'season' => $value['season'],
					'year' => $value['year'],
					'race_day' => $value['race_day'],
					'race_date_days' => $race_date_days,
					'race_date_day' => $race_date_day,
					'race_date_month' => $race_date_month,
					'race_date_year' => $race_date_year,
					'race_name' => $value['race_name'],
					'race_description' => $value['race_description'],
					'final_acceptance_date_times' => $final_acceptance_date_times,
					'acceptance_date_days' => $acceptance_date_days,
					'acceptance_date_day' => $acceptance_date_day,
					'acceptance_date_month' => $acceptance_date_month,
					'acceptance_date_year' => $acceptance_date_year,
					'final_declaration_date_times' => $final_declaration_date_times,
					'declaration_date_days' => $declaration_date_days,
					'declaration_date_day' => $declaration_date_day,
					'declaration_date_month' => $declaration_date_month,
					'declaration_date_year' => $declaration_date_year,
				);

				foreach ($horse_data as $hkey => $hvalue) {
					if($hvalue['weight'] !=''){
						$weight = $hvalue['weight'] + $final_min_value;
					}else{
						$weight = '';
					}
					$data['horse_data'][] = array(
						'horse_rating' => $hvalue['horse_rating'],
						'horse_name' => $hvalue['horse_name'],
						'weight' => $weight,
					);
				}
			}
		}
			
 		$data['base'] = HTTP_SERVER;
 		$html = $this->load->view('catalog/all_report_html', $data);
		$filename = 'Handicap Report.html';
		header('Content-type: text/html');	
		header('Content-Disposition: attachment; filename='.$filename);
		echo $html;exit;
	}

	public function autocompleteRace() {
		/*echo 'in';exit;*/
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['race_name'])) {
			$this->load->model('transaction/handicapping');

			$results = $this->model_transaction_handicapping->getRaceAuto($this->request->get['race_name']);

			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'pros_id' => $result['pros_id'],
						'race_name'        => strip_tags(html_entity_decode($result['race_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['race_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}
}
