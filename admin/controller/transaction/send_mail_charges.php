<?php
class ControllerTransactionSendMailCharges extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/send_mail_charges');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	

	protected function getList() {
	
		if(isset($this->request->get['filter_acceptance_date'])){
			$data['filter_acceptance_date'] = $this ->request->get['filter_acceptance_date'];
		}
		 else{
			$data['filter_acceptance_date'] = '';
		}
		$url = '';


		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/send_mail_charges', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/send_mail_charges/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/send_mail_charges/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/send_mail_charges/repair', 'token=' . $this->session->data['token'] . $url, true);


		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/send_mail_charges/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/send_mail_charges/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['arrival'] = $this->url->link('transaction/arrival_charges', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();
		$final_datas = array();
		$is_filter = 1;
		$jockey_datas = array();
		$trainer_datas = array();

		if(isset($this->request->get['pp'])){
			if($this->request->get['pp'] == 'jLTrans'){
				$latest_datas = $this->db->query("SELECT * FROM `jockey_renewal_history` WHERE entry_status = 1")->rows;

				foreach ($latest_datas as $key => $value) {
					$jockey_datas[] = array(
						'trans_id' => $value['id'],
						'jockey_id' => $value['trainer_id'],
						'jockey_name' => $value['jockey_name'],
						'fees_type' => $value['fees'],
						'amount' => $value['amount'],
						'license_start_date' => date('jS M Y', strtotime($value['license_start_date'])),
						'license_end_date' => date('jS M Y', strtotime($value['license_end_date'])),
						'license_type' => $value['license_type'],
						'apprenties' => $value['apprenties'],
					); 
				}
			} elseif($this->request->get['pp'] == 'TLTrans') {
				$latest_datas = $this->db->query("SELECT * FROM `trainer_renewal_history` WHERE entry_status = 1")->rows;

				foreach ($latest_datas as $key => $value) {
					$trainer_datas[] = array(
						'trans_id' => $value['id'],
						'trainer_id' => $value['trainer_id'],
						'trainer_name' => $value['trainer_name'],
						'fees_type' => $value['fees'],
						'amount' => $value['amount'],
						'license_start_date' => date('jS M Y', strtotime($value['license_start_date'])),
						'license_end_date' => date('jS M Y', strtotime($value['license_end_date'])),
						'license_type' => $value['license_type'],
						'apprenties' => $value['apprenties'],
					); 
				}

			}

		} else {

			$latest_datas = $this->db->query("SELECT * FROM `arrival_charges` WHERE entry_status = 1")->rows;
			foreach ($latest_datas as $lkey => $lvalue) {
				$owners_datas = $this->db->query("SELECT * FROM `arrival_charge_owners` WHERE arrival_charge_id = '".$lvalue['id']."'")->rows;
				$owners_name_str = '';
				$owner_name = '';
				foreach ($owners_datas as $key => $value) {
					$owners_name_str .= $value['owner_name'].'   <b> /  AMT '.$value['charge_amt'].'</b>'.', <br />';
				}

				$owner_name = rtrim($owners_name_str, ',');
				$charge = ($lvalue['arrival_charge'] > 0) ? "Arrival Charge" : "1 Time Lavy Charge";
				$charge_amt = ($lvalue['arrival_charge'] > 0) ? $lvalue['arrival_charge'] : $lvalue['levy_charge'];

				$final_datas[] = array(
					'trans_id' => $lvalue['id'],
					'horse_id' => $lvalue['horse_id'],
					'horse_name' => $lvalue['horse_name'],
					'trainer_id' => $lvalue['trainer_id'],
					'trainer_name' => $lvalue['trainer_name'],
					'charge_type' => $charge,
					'charge_amt' => $charge_amt,
					'owner_name' => $owner_name
				); 
			}
		}
		$data['jockey_datas'] = $jockey_datas;
		$data['trainer_datas'] = $trainer_datas;

		$data['final_datas'] = $final_datas;
		// echo '<pre>';
		// print_r($final_datas);
		// exit;
			

		$data['is_filter'] = $is_filter;

		if(isset($this->request->post['fromdate'])) {
			$data['fromdate'] = $this->request->post['fromdate'];
		} else {
			$data['fromdate'] = date('d-m-Y');
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		

		$url = '';

		

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		// /$data['charge_type'] = 

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/send_mail_charges', $data));
	}


	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/send_mail_charges')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	public function insert_datas()
	{
		$json = array();
		if(isset($this->request->post['trans_id'])){
			if($this->request->post['submit_type'] == 'arrival_charge' ){

				$this->db->query("INSERT INTO `schedule_mail` SET description ='Arrival Charge' ");
				$ids = $this->db->getLastId();
				foreach ($this->request->post['trans_id'] as $key => $value) {
					$this->db->query("INSERT INTO `schedule_mail_trans` SET schedule_mail_id  ='".$ids."' , description_id = '".$value."'  ");
					
				}
				$json['status'] = 1;
				$json['pp'] = 'AC';

				$this->session->data['success'] = "Mail Sending in Progress... ";
				$this->db->query("UPDATE arrival_charges SET entry_status = '0' WHERE entry_status = '1' ");
			} else if($this->request->post['submit_type'] == 'jockey_license_trans' ){

				$this->db->query("INSERT INTO `schedule_mail` SET description ='Jockey License Transaction'");
				$ids = $this->db->getLastId();
				foreach ($this->request->post['trans_id'] as $key => $value) {
					$this->db->query("INSERT INTO `schedule_mail_trans` SET schedule_mail_id  ='".$ids."' , description_id = '".$value."'  ");
					
				}
				$json['status'] = 1;
				$json['pp'] = 'JLT';

				$this->session->data['success'] = "Mail Sending in Progress... ";
				$this->db->query("UPDATE jockey_renewal_history SET entry_status = '0' WHERE entry_status = '1' ");

			}  else if($this->request->post['submit_type'] == 'trainer_license_trans' ){

				$this->db->query("INSERT INTO `schedule_mail` SET description ='Trainer License Transaction'");
				$ids = $this->db->getLastId();
				foreach ($this->request->post['trans_id'] as $key => $value) {
					$this->db->query("INSERT INTO `schedule_mail_trans` SET schedule_mail_id  ='".$ids."' , description_id = '".$value."'  ");
					
				}
				$json['status'] = 1;
				$json['pp'] = 'TLT';

				$this->session->data['success'] = "Mail Sending in Progress... ";
				$this->db->query("UPDATE trainer_renewal_history SET entry_status = '0' WHERE entry_status = '1' ");

			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		// echo "<pre>";
		// print_r($this->request->post);
		// exit;
	}

	

	
	

	
	public function autocompleteHorse() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['horse_name'])) {
			$this->load->model('transaction/send_mail_charges');

			$results = $this->model_transaction_multi_arrival_charges->getHorsesAuto($this->request->get['horse_name']);


			if($results){
				foreach ($results as $result) {
					$foal_date = date('Y',strtotime($result['foal_date']));

					$snat = ($result['sire_nationality'] != '') ? '['.$result['sire_nationality'].']' : "";
					$mnat = ($result['dam_nationality'] != '') ? '['.$result['dam_nationality'].']' : "";


					$json[] = array(
						'horse_id' => $result['horseseq'],
						'pedegree' => $result['color'].' '.$result['sex'].'  '.$foal_date.' by '.$result['sire_name'].$snat.' ex '.$result['dam_name'].$mnat,
						'horse_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		/*echo '<pre>';print_r($json);
			exit;*/
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['horse_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	
	
}
