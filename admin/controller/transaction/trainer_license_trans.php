<?php
class ControllerTransactionTrainerLicenseTrans extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/trainer_license_trans');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/trainer_license_trans');
		$this->getList();
	}

	public function add() {
		$this->load->language('transaction/trainer_license_trans');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/trainer_license_trans');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_transaction_trainer_license_trans->addTrainerTrans($this->request->post);
			$this->session->data['success'] = "Trainer Licence Amount Accepted Successfully !";
			
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/send_mail_charges', 'token=' . $this->session->data['token'] . '&pp=TLTrans'. $url, true));
		}

		$this->getList();
	}


	protected function getList() {
		if (isset($this->request->get['filter_trainer'])) {
			$filter_trainer = $this->request->get['filter_trainer'];
		} else {
			$filter_trainer = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['filter_race_name'])) {
			$data['filter_race_name'] =$this->request->get['filter_race_name'];
		} else{
			$data['filter_race_name']  ="";
		}

		if(isset($this->request->get['filter_acceptance_date'])){
			$data['filter_acceptance_date'] = $this ->request->get['filter_acceptance_date'];
		}
		 else{
			$data['filter_acceptance_date'] = '';
		}
		$url = '';

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . urlencode(html_entity_decode($this->request->get['filter_trainer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/trainer_license_trans', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/trainer_license_trans/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/trainer_license_trans/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/trainer_license_trans/repair', 'token=' . $this->session->data['token'] . $url, true);


		
			$data['action'] = $this->url->link('transaction/trainer_license_trans/add', 'token=' . $this->session->data['token'] . $url, true);
		

		//$data['multi_arrival'] = $this->url->link('transaction/multi_trainer_license_trans', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['lisence_types'] = array(
			'A'  => 'A',
			'B'  => 'B',
		);

		$data['fees_types'] = array(
			'Cash' => 'Cash',
			'Debit' => 'Debit',
			'Wita' => 'Wita',
			'Internet' => 'Internet',
		);


		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_race_name' => $data['filter_race_name'],
			'filter_acceptance_date' => $data['filter_acceptance_date'],
			'filter_trainer'	  => $filter_trainer,
		);
		
		$newEndingDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . " + 1 year"));
		$nxt_yr = date('Y' , strtotime($newEndingDate));
		$data['season_start'] =  '1st May '.date('Y');
		//echo date('Y-m-d', strtotime($data['season_start']));exit;
		$data['season_end'] = '30th April '.$nxt_yr;
		$data['current_date'] = date('jS M Y');
		$data['fees_type'] = 'Debit';


		$trainers_data = array();
		$trainers_datas = $this->model_transaction_trainer_license_trans->getTrainersDatas($filter_data);
		//$this->db->query("select * from trainers where active = 1")->rows;
		foreach ($trainers_datas as $key => $value) {
			$horse = $this->db->query("SELECT COUNT(*) AS total FROM `horse_to_trainer` WHERE trainer_id='".$value['id']."'");

			//echo "<pre>";print_r($horse);exit;
			$last_trans = $this->db->query("SELECT * FROM `trainer_renewal_history` WHERE trainer_id = '".$value['trainer_code']."' AND season_start = '".date('Y-m-d', strtotime($data['season_start']))."' AND season_end = '".date('Y-m-d', strtotime($data['season_end']))."' ");
			if($last_trans->num_rows == 0){
				$lisence_start_date = date('d-m-Y');
				$current_month = date('m');
				//echo $current_month; exit;
				if($current_month > 04){
					$end_date = '30-04-'.$nxt_yr;
				} else {
					$end_date = '30-04-'.date('Y');
				}

				if ($horse->row['total'] <= 29) {
					$tamount = '5000';
				} else{
					$tamount = '10000';
				}
				$trainers_data[] = array(
					'trainer_id' => $value['trainer_code'],
					'trainer_name' => $value['name'],
					'lisence_type' => $value['license_type'],
					'lisence_end_date' => $end_date,
					'lisence_start_date' => $lisence_start_date,
					'no_horse' => $horse->row['total'],
					'amount' => $tamount,
				);
			}
		}

		$data['trainers_data'] = $trainers_data;
				
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . urlencode(html_entity_decode($this->request->get['filter_trainer'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/trainer_license_trans', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/trainer_license_trans', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . urlencode(html_entity_decode($this->request->get['filter_trainer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		

		
		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['filter_trainer'] = $filter_trainer;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/trainer_license_trans', $data));
	}


	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/trainer_license_trans')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		// echo '<pre>';
		// print_r($this->request->post);
		// exit;
		$inn = 0;
		foreach ($this->request->post['trainer_datas'] as $key => $value) {
			if(isset($value['selected_trainer'])){
				$inn = 1;
				break;
			}
		}

		if($inn == 0){
			$this->error['warning'] = 'Please Select Atleast One Trainer To Take Charge';
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	
	
}
