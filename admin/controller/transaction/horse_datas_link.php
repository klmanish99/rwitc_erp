<?php
class Controllertransactionhorsedataslink extends Controller {
	public function index() {
		$this->load->language('transaction/horse_datas_link');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['horse_id'])) {
			$hourse_id = $this->request->get['horse_id'];
		} else {
			$hourse_id = '';
		}

		

		if (isset($this->request->get['color'])) {
			$data['color_link'] = 1;
		} else {
			$data['color_link'] = 0;
		}



		if (isset($this->request->get['order'])) {
			$data['order_link'] = 1;
		} else {
			$data['order_link'] = 0;
		}


		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}


		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_hourse_name']) ) {
			$url .= '&filter_hourse_name=' . $this->request->get['filter_hourse_name'];
		}

		if (isset($this->request->get['horse_id']) ) {
			$url .= '&horse_id=' . $this->request->get['horse_id'];
		}

		if (isset($this->request->get['color'])) {
			$url .= '&color=' . $this->request->get['color'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		/*if (isset($this->request->get['filter_trainer_name']) ) {
			$url .= '&filter_trainer_name=' . $this->request->get['filter_trainer_name'];
		}

		if (isset($this->request->get['filter_trainer_id']) ) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}*/

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/horse_undertaking_charge', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('transaction/horse_datas_link');

		$data['undertakingcharges_datas'] = array();
		if (isset($this->request->get['provi_owner'])) {
			$provi = $this->request->get['provi_owner'];
		} else{
			$provi = '';
		}
		//$hourse_id = 3;
		$filter_data = array(
			'filter_date_end'	     => $filter_date_end,
			'provi_owner'			 => $provi,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin'),
			'hourse_id' => $hourse_id,
			
		);
		$data['print_letter'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] , true);

		if (isset($this->request->get['horse_id'])) {
			$data['action'] = $this->url->link('transaction/horse_datas_link/edit', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/horse_datas_link', 'token=' . $this->session->data['token'] . $url, true);
		}


		$order_total ='';
		$data['final_datas'] = array();
		$horse_info = $this->model_transaction_horse_datas_link->gettrainer($filter_data);

		//ECHO "<pre>";print_r($horse_info);exit;
		if (!empty($horse_info)) {
			$data['horse_id'] = $horse_info['horse_id'];
		} else {
			$data['horse_id'] = '';
		}

		if (!empty($horse_info)) {
			$data['official_name'] = $horse_info['official_name'];
		} else {
			$data['official_name'] = '';
		}

		if (!empty($horse_info)) {
			$data['trainer_name'] = $horse_info['trainer_name'];
		} else {
			$data['trainer_name'] = '';
		}

		if (!empty($horse_info)) {
			$data['trainer_id'] = $horse_info['trainer_id'];
		} else {
			$data['trainer_id'] = '';
		}

		$final_owner_data = $this->model_transaction_horse_datas_link->getowner($filter_data);
		$parent_data = array();
		$sub_parent_data = array();

		foreach ($final_owner_data as $okey => $ovalue) {
			$final_owner_data[$okey]['sub_parent_owner_name'] = '';
					$final_owner_data[$okey]['sub_parent_owner_id'] = '';

			$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$ovalue['horse_to_owner_id']."'");
			if($lease_table_data->num_rows > 0){
				$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
				$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
				if($lease_parent_data->num_rows > 0){
					$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
				}

			}

			if($parent_data){
				$final_owner_data[$okey]['parent_owner_name'] = $parent_data['to_owner'];
				$final_owner_data[$okey]['parent_owner_id'] = $parent_data['to_owner_id'];
				if($sub_parent_data){
					$final_owner_data[$okey]['sub_parent_owner_name'] = $sub_parent_data['to_owner'];
					$final_owner_data[$okey]['sub_parent_owner_id'] = $sub_parent_data['to_owner_id'];
				}
			}
			$owners_color = $this->db->query("SELECT * FROM `owners_color` WHERE owner_id ='".$ovalue['to_owner_id']."' ");
			if($owners_color->num_rows > 0){
				$final_owner_data[$okey]['parent_owner_color'] = $owners_color->row['color'];
			} else {
				$final_owner_data[$okey]['parent_owner_color'] = 'No color';
			}
		}//exit;
		// echo'<pre>';
		// 	print_r($this->session->data);
		// 	exit;

		$data['final_owner'] = $final_owner_data;

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['error'])) {
			$data['error'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} else {
			$data['error'] = '';
		}

		//echo "<pre>";print_r($data['final_owner']);exit;

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_date_start'] = $this->language->get('column_date_start');
		$data['column_date_end'] = $this->language->get('column_date_end');
		$data['column_orders'] = $this->language->get('column_orders');
		$data['column_products'] = $this->language->get('column_products');
		$data['column_tax'] = $this->language->get('column_tax');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_group'] = $this->language->get('entry_group');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		

		
		$data['filter_date_end'] = $filter_date_end;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		

		$this->response->setOutput($this->load->view('transaction/horse_datas_link', $data));
	}

	

	public function edit() {
		//echo "<pre>";print_r($this->request->post);exit;
		$this->load->language('transaction/ownership_shift_module');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/horse_datas_link');
		$inn = 0;
		if (isset($this->request->post['owner_datas'])) {
			foreach ($this->request->post['owner_datas'] as $key => $value) {
				if($value['horse_color_selected'] != 0){
					$inn = 1;
					break;
				}
			}
		}


		//echo '<pre>'; print_r($this->has_dupes($this->request->post['owner_datas'])); exit;



		//echo"<pre>";print_r($inn);exit;
			//echo "<pre>";print_r($this->request->post);exit;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $inn == 1) {
			
			 $this->model_transaction_horse_datas_link->updateOwnerDatas($this->request->post);
			
			//$this->session->data['success'] = 'Owner Updated' ;
			

			$url = '';
			$in = 0;
			$in1 = 0;
			if (isset($this->request->get['horse_id'])) {
				$url .= '&horse_id=' . $this->request->get['horse_id'];
			}

			if (isset($this->request->get['color'])) {
				$url .= '&color=' . $this->request->get['color'];
				$in = 1;
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
				$in1 = 1;
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(($in == 1) && ($in1 == 1) && isset($this->request->post['print_letter']) ){
				$this->response->redirect($this->url->link('transaction/amount_tran', 'token=' . $this->session->data['token'] . $url, true));
			} else if(($in == 1) && ($in1 == 1) && isset($this->request->post['only_update'])){
				$this->session->data['success'] = 'Only Color Updated Done' ;
				$this->response->redirect($this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'], true));
			

			} else {
				$this->response->redirect($this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'], true));
			}

		}  else {
			$this->session->data['error'] = "Please select color to proceed next step";
			$this->index();
		}

	}

	

	public function autocompleteHorse() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['horse_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesAuto($this->request->get['horse_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'horse_id' => $result['horseseq'],
						'horse_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		/*echo '<pre>';print_r($json);
			exit;*/
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['horse_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteJoinColor() {
		$json = array();
		 // echo '<pre>';
		 // print_r($this->request->get);
		 // exit;
		if (isset($this->request->get['join_color'])) {
			$this->load->model('transaction/horse_datas_link');

			$filter_data = array(
				'filter_join_color' => $this->request->get['join_color'],
				//'start'       => 0,
				//'limit'       => 5
			);
			$results = $this->model_transaction_horse_datas_link->getsupplier($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'doctor_name'     => strip_tags(html_entity_decode($result['color'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function UpdateColor(){
		$json = array();
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		if (isset($this->request->get['horse_id'])) {
		 	$this->db->query("UPDATE horse_to_owner SET join_color = '".$this->request->get['color']."' WHERE `horse_id`= '".$this->request->get['horse_id']."' AND owner_share_status = '1' AND provisional_ownership = 'No' ");
		}
		$json['success']= 1;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}