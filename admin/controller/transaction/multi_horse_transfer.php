<?php
class ControllerTransactionMultiHorseTransfer extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/multi_horse_transfer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/multi_horse_transfer');
		$this->getList();
	}

	

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['charge_type'])) {
			$data['charge_type'] =$this->request->get['charge_type'];
		} else{
			$data['charge_type']  ="";
		}

		if(isset($this->request->get['filter_acceptance_date'])){
			$data['filter_acceptance_date'] = $this ->request->get['filter_acceptance_date'];
		}
		 else{
			$data['filter_acceptance_date'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/multi_horse_transfer', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/multi_horse_transfer/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/multi_horse_transfer/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/multi_horse_transfer/repair', 'token=' . $this->session->data['token'] . $url, true);


		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/multi_horse_transfer/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/multi_horse_transfer/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['arrival'] = $this->url->link('transaction/arrival_charges', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['registration_type'] = array(
			'Name Registration'  => 'Name Registration',
			'Rename'  => 'Rename',
		);

		$data['arrival_time_trainer'] = array(
			'AM'  => 'AM',
			'PM'  => 'PM',
		);

		$data['licence_typess'] = array(
			'A'  => 'A',
			'B'  => 'B',
		);

		$data['charge_types'] = array(
			'Arrival Charge'  => 'Arrival Charge',
			'1 Time Lavy'  => '1 Time Lavy',
		);

		// echo'<pre>';
		// print_r($this->request->get);
		// exit;


		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_acceptance_date' => $data['filter_acceptance_date']
		);
		

		
		$horse_data = array();
		$is_filter = 0;
		if(isset($this->request->get['charge_type'])){
			$sql = "SELECT * FROM horse_to_trainer ht LEFT JOIN trainers t ON(ht.trainer_id = t.id) WHERE 1=1 AND trainer_status = '1' AND provisional = '0'";
			if(isset($this->request->get['charge_type']) && $this->request->get['charge_type'] == '1 Time Lavy'){
				$sql .= " AND t.license_type = 'A' ";
			} elseif(isset($this->request->get['charge_type']) && $this->request->get['charge_type'] == 'Arrival Charge') {
				$sql .= " AND t.license_type = 'B' ";
			}
			$sql .= " Group By ht.horse_id";
			$trainer_datas  = $this->db->query($sql)->rows;
			foreach ($trainer_datas as $hkey => $hvalue) {
				$horse_datass = $this->db->query("SELECT * FROM horse1 WHERE horseseq = '".$hvalue['horse_id']."' AND horse_status = 1");
				if($horse_datass->num_rows > 0){
					$exist_arrival  = $this->db->query("SELECT * FROM arrival_charges WHERE horse_id = '".$hvalue['horse_id']."' AND trainer_id = '".$hvalue['trainer_id']."' ");
					if($exist_arrival->num_rows == 0) {
						$owners_datas  = $this->db->query("SELECT * FROM horse_to_owner WHERE horse_id = '".$hvalue['horse_id']."' AND owner_share_status = 1");
						$owner_name = '';
						$owner_string = '';
						if($owners_datas->num_rows > 0){
							foreach ($owners_datas->rows as $okey => $ovalue) {
								$owner_name .= $ovalue['to_owner'].', <br / >';
							}
						}
						$owner_string = rtrim($owner_name, ',');

						$horse_data[] = array(
							'horse_id' => $hvalue['horse_id'],
							'horse_name' => $horse_datass->row['official_name'],
							'trainer_name' => $hvalue['trainer_name'],
							'trainer_id' => $hvalue['trainer_id'],
							'owner_name' => $owner_string,
							'change_ownership' => $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . '&horse_id=' . $hvalue['horse_id'] . '&pp=' . 'arrival_charge'. $url, true),
						);
					}
				}
			}
			$is_filter = 1;
		}

			$data['horse_datas'] = $horse_data;
		$data['is_filter'] = $is_filter;

		if(isset($this->request->post['fromdate'])) {
			$data['fromdate'] = $this->request->post['fromdate'];
		} else {
			$data['fromdate'] = date('d-m-Y');
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/multi_horse_transfer', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/multi_horse_transfer', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		// /$data['charge_type'] = 

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/multi_horse_transfer', $data));
	}


	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/multi_horse_transfer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['linkedOwnerName'] as $key => $value) {
			if ($value['linked_owner_id'] == '') {
				$this->error['linkedOwner'] = 'Please select valid linked owner.';
			}
		}

		if ($this->request->post['gstType'] == 'Registered' && $this->request->post['gstNo'] == '') {
			$this->error['gst_number'] = 'GST No can not be blank.';
		}

		foreach ($this->request->post['owner_shared_color'] as $key => $value) {
			if ($value['shared_owner_id'] == '') {
				$this->error['sharedColor'] = 'Please select valid owner to share color.';
			}
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	

	public function saveAllDatas() {
		// echo'<pre>';
		// print_r($this->request->post);
		// exit;
		$json = array();
		$exist_form_trainer = 0;
		$exist_to_trainer = 0;
		$exist_form_trainers = $this->db->query("SELECT id FROM trainers WHERE id = '".$this->request->post['from_trainer_id']."'");
		$exist_to_trainers = $this->db->query("SELECT id FROM trainers WHERE id = '".$this->request->post['to_trainer_id']."'");
		if($exist_form_trainers->num_rows == 0){
			$exist_form_trainer = 1;
		}

		if($exist_to_trainers->num_rows == 0){
			$exist_to_trainer = 1;
		}
		$json['status'] = 0;
		if(isset($this->request->post['horse_datas']) && $exist_to_trainer == 0 && $exist_form_trainer == 0)	{
			foreach ($this->request->post['horse_datas'] as $key => $value) {
				if(isset($value['trans_id']) && $value['trans_id'] != '' ){
					$narration = ($this->request->post['narration'] != "") ? $this->request->post['narration'] : $value['extra_narration'] ;
					$provisional = isset($this->request->post['provisional'] ) ? $this->request->post['provisional'] : $value['provisional'] ; 
					$undertaking_charge = isset($this->request->post['undertaking_charge'] ) ? $this->request->post['undertaking_charge'] : $value['undertaking_charge'] ; 

					$exist_trainer = $this->db->query("SELECT * FROM horse_to_trainer WHERE horse_id = '".$value['horse_id']."' AND trainer_status = 1 ")->row;
					$this->db->query("UPDATE horse_to_trainer SET trainer_status = 0, undertaking_charge = '".$undertaking_charge."', left_date_of_charge = '".date('Y-m-d')."' WHERE horse_to_trainer_id = '".$exist_trainer['horse_to_trainer_id']."' ");

					$this->db->query("INSERT INTO `horse_to_trainer` SET 
						`horse_id` = '".$value['horse_id']."',
						`trainer_name` = '".$this->db->escape($this->request->post['to_trainer'])."',
						`trainer_id` = '".$this->request->post['to_trainer_id']."',
						`trainer_code` = '".$this->request->post['to_trainer_code']."',
						`date_of_charge` = '".date('Y-m-d',strtotime($this->request->post['date_of_charge']))."',
						`trainer_status` = '1',
						`arrival_time` = '".$this->request->post['arrival_time_trainers']."',
						`extra_narration` = '".$narration."',
						`provisional` = '".$provisional."',
						`bulk_transfer` = '1'
						");

					$this->session->data['success'] = "Transfer Success!";
					$json['status'] = 1;
				}
			}
		} 

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	


	public function autocompleteFromTrainer() {
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		$json = array();

		if(isset($this->request->get['trainer_name']) && $this->request->get['trainer_name'] != ''){
			$trainers_datas = $this->db->query("SELECT id, name, license_type FROM trainers WHERE name LIKE '%". $this->request->get['trainer_name'] ."%' ")->rows;
			foreach ($trainers_datas as $tkey => $tvalue) {
				$json['final_trainers'][] = array(
					'id' => $tvalue['id'],
					'name' => $tvalue['name'],
					'license_type' => $tvalue['license_type']
				); 
			}
		}


		if(isset($this->request->get['trainer_code']) && $this->request->get['trainer_code'] != ''){
			$trainers_datas = $this->db->query("SELECT id, name, license_type, trainer_code_new FROM trainers WHERE trainer_code_new LIKE '%". $this->request->get['trainer_code'] ."%' ")->rows;
			foreach ($trainers_datas as $tkey => $tvalue) {
				$json['final_trainers'][] = array(
					'id' => $tvalue['id'],
					'name' => $tvalue['name'],
					'license_type' => $tvalue['license_type'],
					'trainer_code' => $tvalue['trainer_code_new']
				); 
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	

	public function allTrainerHorses() {
		$json = array();
		$json['status'] = 0;
		if($this->request->get['trainer_id'] != ''){
			$all_horses = $this->db->query("SELECT * FROM horse_to_trainer WHERE trainer_id = '". $this->request->get['trainer_id'] ."' AND trainer_status = 1");
		// 	echo '<pre>';
		// print_r($all_horses);
		// exit;
			$html  = '';
			$owners_present = 0;
			if($all_horses->num_rows > 0){
				$json['status'] = 1;
				$html .= '<div style="height:300px;overflow:auto;">';
					$html .= '<table class="table table-bordered ownertable">';
					$html .= '<thead>';
					$html .= '<tr>';
						$html .= '<td style="width: 40px;text-align: center;"><input type="checkbox" class="all_checked" /></td>';
						$html .= '<td style="text-align: center;vertical-align: middle;">Horse Name</td>';
						$html .= '<td style="text-align: center;vertical-align: middle;">Trainer Name</td>';
						$html .= '<td style="text-align: center;vertical-align: middle;">Ownership</td>';

						$html .= '<td style="width: 100px;text-align: center;vertical-align: middle;">Date of Charge</td>';
						
						$html .= '<td style="width: 50px;text-align: center;vertical-align: middle;">Arrival Time</td>';
						$html .= '<td style="width: 200px;text-align: center;vertical-align: middle;">Extra Narration</td>';
						$html .= '<td style="width: 50px;text-align: center;vertical-align: middle;">Undertaking Charge</td>';
						$html .= '<td style="width: 50px;text-align: center;vertical-align: middle;">Provisional</td>';
						
					$html .= '</tr>';
					$html .= '</thead>';
					$html .= '<tbody>';
					$i=1;
					foreach ($all_horses->rows as $hkey => $hvalue) {
						$horse_name = $this->db->query("SELECT official_name FROM horse1 WHERE horseseq = '". $hvalue['horse_id'] ."' ")->row['official_name'];
						$all_owners = $this->db->query("SELECT * FROM horse_to_owner WHERE horse_id = '". $hvalue['horse_id'] ."' AND owner_share_status = 1");
						$owners_name = '';
						if($all_owners->num_rows > 0){
							foreach ($all_owners->rows as $akey => $avalue) {
								$owners_name .= $avalue['to_owner'].' ('.$avalue['owner_percentage'].'% ) ,<br/>'; 
							}
						}
						if($owners_name != ''){
							$owners_present = 1;
							$html .= '<tr>';
							$html .= '<td class="r_'.$i.'" style="text-align: center;">';
							$html .= '<input type="checkbox" name="horse_datas['.$i.'][trans_id]"  class="checkbox_value ent-evnt" id="fro_ow_ckper_'.$i.'" />';
									$html .= '<input type="hidden" name="horse_datas['.$i.'][checked_horse_id]" value="'.$hvalue['horse_id'].'"  id="fro_ow_ck_'.$i.'"  />';
							$html .= '</td>';

							$html .= '<td class="r_'.$i.'" style="text-align: left;">'; 
								$html .= '<span >'.$horse_name.'</span><br>';
								$html .= '<input type="hidden" name="horse_datas['.$i.'][horse_name]" value="'.$horse_name.'" class="ent-evnt" id="horse_name_'.$i.'"  />';
								$html .= '<input type="hidden" name="horse_datas['.$i.'][horse_id]" value="'.$hvalue['horse_id'].'" class="ent-evnt" id="horse_id_'.$i.'"  />';
							$html .= '</td>';

							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span>'.$hvalue['trainer_name'].'</span>';
								$html .= '<input type="hidden" name="horse_datas['.$i.'][trainer_name]" value="'.$hvalue['trainer_name'].'" id="trainer_name_'.$i.'"  />';
								$html .= '<input type="hidden" name="horse_datas['.$i.'][trainer_id]" value="'.$hvalue['trainer_id'].'" id="trainer_id_'.$i.'"  />';
							$html .= '</td>';


							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span>'.$owners_name.'</span>';
							$html .= '</td>';

							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span>'.date('d-M-Y',strtotime($hvalue['date_of_charge'])).'</span>';
								$html .= '<input type="hidden" name="horse_datas['.$i.'][date_of_charge]" value="'.$hvalue['date_of_charge'].'" id="date_of_charge_'.$i.'"  />';
							$html .= '</td>';

							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span>'.$hvalue['arrival_time'].'</span>';
								$html .= '<input type="hidden" name="horse_datas['.$i.'][arrival_time]" value="'.$hvalue['arrival_time'].'" id="arrival_time_'.$i.'"  />';
							$html .= '</td>';

							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span>'.$hvalue['extra_narration'].'</span>';
								$html .= '<input type="hidden" name="horse_datas['.$i.'][extra_narration]" value="'.$hvalue['extra_narration'].'" id="extra_narration_'.$i.'"  />';
							$html .= '</td>';
							$undertaking_charge = ($hvalue['undertaking_charge'] == 1) ? "Yes": "No" ;
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span>'.$undertaking_charge.'</span>';
								$html .= '<input type="hidden" name="horse_datas['.$i.'][undertaking_charge]" value="'.$hvalue['undertaking_charge'].'" id="undertaking_charge_'.$i.'"  />';
							$html .= '</td>';
							$provisional = ($hvalue['provisional'] == 1) ? "Yes": "No" ;

							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span>'.$provisional.'</span>';
								$html .= '<input type="hidden" name="horse_datas['.$i.'][provisional]" value="'.$hvalue['provisional'].'" id="provisional_'.$i.'"  />';
							$html .= '</td>';
							$html .='</tr>';
							
							$i++;
						}
					}
					$html .= '</tbody>';
				$html .= '</table>';
				$html .= '</div>';


			}
		}
		$json['owners_present'] = $owners_present;
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
