<?php
class ControllerTransactionArrivalCharges extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/arrival_charges');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/arrival_charges');
		$this->getList();
	}

	

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['filter_race_name'])) {
			$data['filter_race_name'] =$this->request->get['filter_race_name'];
		} else{
			$data['filter_race_name']  ="";
		}

		if(isset($this->request->get['filter_acceptance_date'])){
			$data['filter_acceptance_date'] = $this ->request->get['filter_acceptance_date'];
		}
		 else{
			$data['filter_acceptance_date'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/arrival_charges', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/arrival_charges/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/arrival_charges/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/arrival_charges/repair', 'token=' . $this->session->data['token'] . $url, true);


		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/arrival_charges/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/arrival_charges/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['multi_arrival'] = $this->url->link('transaction/multi_arrival_charges', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['registration_type'] = array(
			'Name Registration'  => 'Name Registration',
			'Rename'  => 'Rename',
		);




		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_race_name' => $data['filter_race_name'],
			'filter_acceptance_date' => $data['filter_acceptance_date']
		);
		$category_total = $this->model_transaction_arrival_charges->getTotalCategories($filter_data);
		$results = $this->model_transaction_arrival_charges->getprospectus($filter_data);
		// echo "<pre>";
		// print_r($results);
		// exit;
		foreach ($results as $result) {
			$data['acceptancedatas'][] = array(
				'entry_id' 	  	=> $result['entry_id'],
				'pros_id' 	  	=> $result['pros_id'],
				'race_description' 	  	=> $result['race_description'],
				'race_name'	  	=> $result['race_name'],
				'race_type'	  	=> $result['race_type'],
				'count'	  		=> $result['count'],
				'handicap_status' =>$result['handicap_status'],
				'race_date'	  	=> date('d-m-Y', strtotime($result['race_date'])),
				'class'	  	=> $result['class'],
				'distance' =>$result['distance'],
				'foreign_jockey'	  	=> $result['foreign_jockey_eligible'],
				'edit'        	=> $this->url->link('transaction/arrival_charges/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $result['pros_id'] . $url, true),
			);
		}

		if (isset($this->request->get['horse_id'])) {
			$horse_datass = $this->db->query("SELECT `official_name`, `horseseq` FROM `horse1` WHERE horseseq = '".$this->request->get['horse_id']."'")->row;
			$traniner_datass = $this->db->query("SELECT `trainer_name`, `trainer_id` FROM `horse_to_trainer` WHERE horse_id = '".$this->request->get['horse_id']."'")->row;

			$data['horse_name'] = $horse_datass['official_name'];
			$data['horse_id'] = $horse_datass['horseseq'];
			$data['trainer_name'] = $traniner_datass['trainer_name'];
			$data['trainer_id'] = $traniner_datass['trainer_id'];
			$data['gt_id'] = 1;
		} else {
			$data['horse_name'] = '';
			$data['horse_id'] = '';
			$data['trainer_name'] = '';
			$data['trainer_id'] = '';
			$data['gt_id'] = 0;
		}

		$data['user_id'] = $this->session->data['user_id'];

		if(isset($this->request->post['fromdate'])) {
			$data['fromdate'] = $this->request->post['fromdate'];
		} else {
			$data['fromdate'] = date('d-m-Y');
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/arrival_charges', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/arrival_charges', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('transaction/arrival_charges', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/arrival_charges', $data));
	}


	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/arrival_charges')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['linkedOwnerName'] as $key => $value) {
			if ($value['linked_owner_id'] == '') {
				$this->error['linkedOwner'] = 'Please select valid linked owner.';
			}
		}

		if ($this->request->post['gstType'] == 'Registered' && $this->request->post['gstNo'] == '') {
			$this->error['gst_number'] = 'GST No can not be blank.';
		}

		foreach ($this->request->post['owner_shared_color'] as $key => $value) {
			if ($value['shared_owner_id'] == '') {
				$this->error['sharedColor'] = 'Please select valid owner to share color.';
			}
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	public function autoHorseToTrainer() {
		$json = array();
		$html1 = '';
		if (isset($this->request->get['horse_id'])) {
			$this->load->model('transaction/arrival_charges');
			$filter_data = array(
				'horse_id' => $this->request->get['horse_id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_arrival_charges->gethorsetoTrainer($filter_data);
			foreach ($results as $result) {
				$licence_typess = $this->db->query("SELECT license_type FROM trainers WHERE trainer_code = '" . (int)$result['trainer_code'] . "'");
				$license_type = ($licence_typess->num_rows > 0) ? $licence_typess->row['license_type'] : '';
				$json = array(
					'trainer_id' => $result['trainer_id'],
					'trainer_code' => $result['trainer_code'],
					'license_type' => $license_type,
					'trainer_name'        => strip_tags(html_entity_decode($result['trainer_name'], ENT_QUOTES, 'UTF-8'))
				);
			}

			$pre_datass = $this->db->query("SELECT * FROM arrival_charges WHERE horse_id = '" . (int)$this->request->get['horse_id'] . "'");
			$html = '';
			$html .= '<label class="col-sm-2 control-label old_arrival_label" style= "margin-left: 1%;font-size:14px;" for="input-arrival" >Arrival Charge History:</label><br/>';
			$html .= '<table class="table table-bordered ownertable" style= "margin-top: 2%;width: 95%;margin-left: 2%;">';
				$html .= '<thead>';
				$html .= '<tr>';
					$html .= '<td style="text-align: center;">Horse Name</td>';
					$html .= '<td style="width: 160px;text-align: center;">Trainer Name</td>';
					$html .= '<td style="width: 160px;text-align: center;">License Type</td>';
					$html .= '<td style="width: 160px;text-align: center;">Date</td>';
					$html .= '<td style="width: 160px;text-align: center;">Amount</td>';
					$html .= '<td style="width: 160px;text-align: center;">Charge Type</td>';

				$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';

			$json['pre_exist'] = 0;
			$json['is_atype'] = 0;
			if($pre_datass->num_rows > 0){
				$json['pre_exist'] = 1;
				
				$i = 0;
				foreach ($pre_datass->rows as $result) {
					if($result['license_type'] == 'A'){
						$json['is_atype'] = 1;
					}
					$date = ($result['date'] != '0000-00-00') ? date('d-m-Y', strtotime($result['date'])) : '';
					$charge = ($result['arrival_charge'] != 0) ? $result['arrival_charge'] : $result['levy_charge'];

					$html .= '<tr>';
						$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							//$html .= '<span>'.$result['horse_name'].'</span>';
							$html .= '<span style="cursor:pointer"><a id="chargehis_'.$result['id'].'"  class="chargehis" >'.$result['horse_name'].'</a></span>';
						$html .= '</td>';

						$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							$html .= '<span>'.$result['trainer_name'].'</span>';
						$html .= '</td>';

						$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							$html .= '<span>'.$result['license_type'].'</span>';
						$html .= '</td>';

						$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							$html .= '<span>'.$date.'</span>';
						$html .= '</td>';

						$html .= '<td class="r_'.$i.'" style="text-align: right;">';  
							$html .= '<span>'.$charge.'</span>';
						$html .= '</td>';

						if($result['arrival_charge'] > 0){
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span>Arrival Charge</span>';
							$html .= '</td>';
						} else {
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span>1 Time Levy</span>';
							$html .= '</td>';
						}
					$html .= '</tr>';
					$i++;
				}
				$html .= '</tbody>';
				$html .= '</table>';
			}


			$resultz = $this->model_transaction_arrival_charges->gethorsetoOwner($filter_data);
			$provi_resultz = $this->model_transaction_arrival_charges->gethorsetoOwnerProvi($filter_data);

			//echo "<pre>";print_r($results);exit;
			$html1 = '';

			$json['is_owners'] = 0;
			if($resultz){
				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="col-sm-2 control-label current_owner_label" style= "margin-left: 1%;font-size:14px;" for="input-trainer_name" >Current Owners:</label><br/>';

					$html1 .= '<a target="_blank" style="font-size:14px;margin-left:50%;" class="btn btn-primary" href="index.php?route=transaction/ownership_shift_module&token='.$this->session->data['token'].'&horse_id='.$this->request->get['horse_id'].'&pp=arrival_charge">Change Ownership</a>';
                    $html1 .= '<a id="ref-owners" style="margin-left:15px;font-size:14px;" class="btn btn-primary">Refresh Owners</a>';

				$html1 .= '</div>';
				$html1 .= '<table class="table table-bordered ownertable" style= "margin-top: 2%;width: 95%;margin-left: 2%;">';
				$html1 .= '<thead>';
				$html1 .= '<tr>';
						$html1 .= '<td class="text-center;" style="vertical-align: middle;">Name</td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Period </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Share </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Color </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Contingency </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Ownership </td>';
				$html1 .= '</tr>';
				$html1 .= '</thead>';
				$html1 .= '<tbody>';

				$j=1;
				$parent_data =array();
				foreach ($resultz as $resulta) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$resulta['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$resulta['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html1  .= '<tr>';
					

					$from_date = date('d-m-Y', strtotime($resulta['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($resulta['end_date_of_ownership']));

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html1  .= '<span style="cursor:pointer"><a id="partners_'.$resulta['to_owner_id'].'"  class="partners" >'.$resulta['to_owner'].'</a></span><br>';
						}else {
							$html1  .= '<span >'.$resulta['to_owner'].'</span><br>';
						}
							$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_hidden]" value="'.$resulta['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$j.'"  />';
							$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_name_hidden]" value="'.$resulta['to_owner'].'"  />';
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';  
						$provisional_ownership  = ($resulta['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';  
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($resulta['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($resulta['end_date_of_ownership'])) : "";
							$html1 .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html1  .= '</td>';
					


					$html1  .= '<td class="r_'.$j.'" style="text-align: right;">';  
						$html1  .= '<span>'.$resulta['owner_percentage'].'%'.'</span>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_percentage]" value="'.$resulta['owner_percentage'].'" id="owner_percentage_'.$j.'"  />';
					$html1  .= '</td>';
					
					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
					if($resulta['owner_color'] == 'Yes' && $last_color == $resulta['horse_to_owner_id']){
						$html1  .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'" checked="checked"  />';
					} else {
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'"  />';
					} 
					$html1  .= '</td>';
					$contengency = ($resulta['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($resulta['cont_percentage'] != 0) ? $resulta['cont_percentage'] : "";
					$cont_amount = ($resulta['cont_amount'] != 0) ? $resulta['cont_amount'] : "0";

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html1  .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$resulta['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
						}
					$html1  .= '</td>';
						
					if($parent_data){
						if($parent_data['to_owner'] != '' && $resulta['ownership_type'] != 'Sale'){
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								if($resulta['ownership_type'] == 'Lease'){
									$html1 .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else {
									$html1 .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}else {
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								$html1 .= '<span> </span>';
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="0" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}
					} else {
						$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';
						$html1  .= '</td>';
					}
					$j++;
				}
				$html1 .= '</tbody>';
				$html1 .= '</table>';








				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="col-sm-2 control-label current_owner_label" style= "margin-left: 1%;font-size:14px;" for="input-trainer_name" >Provisional ownership:</label><br/>';

					// $html1 .= '<a target="_blank" style="font-size:14px;margin-left:50%;" class="btn btn-primary" href="index.php?route=transaction/ownership_shift_module&token='.$this->session->data['token'].'&horse_id='.$this->request->get['horse_id'].'&pp=arrival_charge">Change Ownership</a>';
     //                $html1 .= '<a id="ref-owners" style="margin-left:15px;font-size:14px;" class="btn btn-primary">Refresh Owners</a>';

				$html1 .= '</div>';
				$html1 .= '<table class="table table-bordered ownertable" style= "margin-top: 2%;width: 95%;margin-left: 2%;">';
				$html1 .= '<thead>';
				$html1 .= '<tr>';
						$html1 .= '<td class="text-center;" style="vertical-align: middle;">Name</td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Period </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Share </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Color </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Contingency </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Ownership </td>';
				$html1 .= '</tr>';
				$html1 .= '</thead>';
				$html1 .= '<tbody>';

				$j=1;
				$parent_data =array();
				foreach ($provi_resultz as $resulta) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$resulta['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$resulta['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html1  .= '<tr>';
					

					$from_date = date('d-m-Y', strtotime($resulta['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($resulta['end_date_of_ownership']));

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html1  .= '<span style="cursor:pointer"><a id="partners_'.$resulta['to_owner_id'].'"  class="partners" >'.$resulta['to_owner'].'</a></span><br>';
						}else {
							$html1  .= '<span >'.$resulta['to_owner'].'</span><br>';
						}
							$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_hidden]" value="'.$resulta['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$j.'"  />';
							$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_name_hidden]" value="'.$resulta['to_owner'].'"  />';
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';  
						$provisional_ownership  = ($resulta['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';  
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($resulta['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($resulta['end_date_of_ownership'])) : "";
							$html1 .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html1  .= '</td>';
					


					$html1  .= '<td class="r_'.$j.'" style="text-align: right;">';  
						$html1  .= '<span>'.$resulta['owner_percentage'].'%'.'</span>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_percentage]" value="'.$resulta['owner_percentage'].'" id="owner_percentage_'.$j.'"  />';
					$html1  .= '</td>';
					
					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
					if($resulta['owner_color'] == 'Yes' && $last_color == $resulta['horse_to_owner_id']){
						$html1  .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'" checked="checked"  />';
					} else {
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'"  />';
					} 
					$html1  .= '</td>';
					$contengency = ($resulta['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($resulta['cont_percentage'] != 0) ? $resulta['cont_percentage'] : "";
					$cont_amount = ($resulta['cont_amount'] != 0) ? $resulta['cont_amount'] : "0";

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html1  .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$resulta['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
						}
					$html1  .= '</td>';
						
					if($parent_data){
						if($parent_data['to_owner'] != '' && $resulta['ownership_type'] != 'Sale'){
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								if($resulta['ownership_type'] == 'Lease'){
									$html1 .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else {
									$html1 .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}else {
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								$html1 .= '<span> </span>';
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="0" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}
					} else {
						$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';
						$html1  .= '</td>';
					}
					$j++;
				}
				$html1 .= '</tbody>';
				$html1 .= '</table>';
				$json['is_owners'] = 1;
			}  else {
				$html1 .= '<div class="form-group col-sm-12">';
					$html1  .= '<label class="control-label current_owner_label" style= "margin-left: 4%;font-size:14px;" for="input-trainer_name" >No Owners assign to current Horse</label><br/>';
					$html1  .= '<a target="_blank" style="font-size:14px;margin-left:70%;" class="btn btn-primary" href="index.php?route=transaction/ownership_shift_module&token='.$this->session->data['token'].'&horse_id='.$this->request->get['horse_id'].'&pp=arrival_charge">Add Ownership</a>';
                    $html1 .= '<a id="ref-owners" style="margin-left:15px;font-size:14px;" class="btn btn-primary">Refresh Owners</a>';
				$html1 .= '</div>';
			}
			$json['html_owner'] = $html1;
		}
		$json['html'] = $html;

		// echo'<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function refreshOwners(){
		$this->load->model('transaction/arrival_charges');

		$filter_data = array(
				'horse_id' => $this->request->get['filterHorseId'],
				
			);


		$resultz = $this->model_transaction_arrival_charges->gethorsetoOwner($filter_data);
		$provi_resultz = $this->model_transaction_arrival_charges->gethorsetoOwnerProvi($filter_data);
			$html1 = '';
			if($resultz){
				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filterHorseId']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="col-sm-2 control-label current_owner_label" style= "margin-left: 1%;font-size:14px;" for="input-trainer_name" >Current Owners:</label><br/>';

					$html1 .= '<a target="_blank" style="font-size:14px;margin-left:50%;" class="btn btn-primary" href="index.php?route=transaction/ownership_shift_module&token='.$this->session->data['token'].'&horse_id='.$this->request->get['filterHorseId'].'&pp=name_change">Change Ownership</a>';
                    $html1 .= '<a id="ref-owners" style="margin-left:15px;font-size:14px;" class="btn btn-primary">Refresh Owners</a>';

				$html1 .= '</div>';
				
				$html1 .= '<table class="table table-bordered ownertable" style= "margin-top: 2%;width: 98%;margin-left: 1%;">';
				$html1 .= '<thead>';
				$html1 .= '<tr>';
						$html1 .= '<td class="text-center;" style="vertical-align: middle;">Name</td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Period </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Share </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Color </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Contingency </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Ownership </td>';
				$html1 .= '</tr>';
				$html1 .= '</thead>';
				$html1 .= '<tbody>';

				$j=1;
				
				$parent_data =array();
				foreach ($resultz as $resulta) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$resulta['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$resulta['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html1  .= '<tr>';
					

					$from_date = date('d-m-Y', strtotime($resulta['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($resulta['end_date_of_ownership']));

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html1  .= '<span style="cursor:pointer"><a id="partners_'.$resulta['to_owner_id'].'"  class="partners" >'.$resulta['to_owner'].'</a></span><br>';
						}else {
							$html1  .= '<span >'.$resulta['to_owner'].'</span><br>';
						}
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_hidden]" value="'.$resulta['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$j.'"  />';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_name_hidden]" value="'.$resulta['to_owner'].'"  />';
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';  
						$provisional_ownership  = ($resulta['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';  
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($resulta['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($resulta['end_date_of_ownership'])) : "";
							$html1 .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html1  .= '</td>';
					


					$html1  .= '<td class="r_'.$j.'" style="text-align: right;">';  
						$html1  .= '<span>'.$resulta['owner_percentage'].'%'.'</span>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_percentage]" value="'.$resulta['owner_percentage'].'" id="owner_percentage_'.$j.'"  />';
					$html1  .= '</td>';
					
					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
					if($resulta['owner_color'] == 'Yes'  && $last_color == $resulta['horse_to_owner_id']){
						$html1  .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'" checked="checked"  />';
					} else {
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'"  />';
					} 
					$html1  .= '</td>';
					$contengency = ($resulta['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($resulta['cont_percentage'] != 0) ? $resulta['cont_percentage'] : "";
					$cont_amount = ($resulta['cont_amount'] != 0) ? $resulta['cont_amount'] : "0";

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html1  .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$resulta['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
						}
					$html1  .= '</td>';
						

					if($parent_data){
						if($parent_data['to_owner'] != '' && $resulta['ownership_type'] != 'Sale'){
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								if($resulta['ownership_type'] == 'Lease'){
									$html1 .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else {
									$html1 .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}else {
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								$html1 .= '<span> </span>';
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="0" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}
					} else {
						$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';
						$html1  .= '</td>';
					}
					$j++;
				}
				$html1 .= '</tbody>';
				$html1 .= '</table>';









				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filterHorseId']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="col-sm-2 control-label current_owner_label" style= "margin-left: 1%;font-size:14px;" for="input-trainer_name" >Provisional ownership:</label><br/>';

					// $html1 .= '<a target="_blank" style="font-size:14px;margin-left:50%;" class="btn btn-primary" href="index.php?route=transaction/ownership_shift_module&token='.$this->session->data['token'].'&horse_id='.$this->request->get['filterHorseId'].'&pp=name_change">Change Ownership</a>';
     //                $html1 .= '<a id="ref-owners" style="margin-left:15px;font-size:14px;" class="btn btn-primary">Refresh Owners</a>';

				$html1 .= '</div>';
				
				$html1 .= '<table class="table table-bordered ownertable" style= "margin-top: 2%;width: 98%;margin-left: 1%;">';
				$html1 .= '<thead>';
				$html1 .= '<tr>';
						$html1 .= '<td class="text-center;" style="vertical-align: middle;">Name</td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Period </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Share </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Color </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Contingency </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Ownership </td>';
				$html1 .= '</tr>';
				$html1 .= '</thead>';
				$html1 .= '<tbody>';

				$j=1;
				
				$parent_data =array();
				foreach ($provi_resultz as $resulta) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$resulta['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$resulta['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html1  .= '<tr>';
					

					$from_date = date('d-m-Y', strtotime($resulta['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($resulta['end_date_of_ownership']));

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html1  .= '<span style="cursor:pointer"><a id="partners_'.$resulta['to_owner_id'].'"  class="partners" >'.$resulta['to_owner'].'</a></span><br>';
						}else {
							$html1  .= '<span >'.$resulta['to_owner'].'</span><br>';
						}
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_hidden]" value="'.$resulta['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$j.'"  />';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_name_hidden]" value="'.$resulta['to_owner'].'"  />';
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';  
						$provisional_ownership  = ($resulta['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';  
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($resulta['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($resulta['end_date_of_ownership'])) : "";
							$html1 .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html1  .= '</td>';
					


					$html1  .= '<td class="r_'.$j.'" style="text-align: right;">';  
						$html1  .= '<span>'.$resulta['owner_percentage'].'%'.'</span>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_percentage]" value="'.$resulta['owner_percentage'].'" id="owner_percentage_'.$j.'"  />';
					$html1  .= '</td>';
					
					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
					if($resulta['owner_color'] == 'Yes'  && $last_color == $resulta['horse_to_owner_id']){
						$html1  .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'" checked="checked"  />';
					} else {
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'"  />';
					} 
					$html1  .= '</td>';
					$contengency = ($resulta['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($resulta['cont_percentage'] != 0) ? $resulta['cont_percentage'] : "";
					$cont_amount = ($resulta['cont_amount'] != 0) ? $resulta['cont_amount'] : "0";

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html1  .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$resulta['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
						}
					$html1  .= '</td>';
						

					if($parent_data){
						if($parent_data['to_owner'] != '' && $resulta['ownership_type'] != 'Sale'){
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								if($resulta['ownership_type'] == 'Lease'){
									$html1 .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else {
									$html1 .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}else {
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								$html1 .= '<span> </span>';
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="0" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}
					} else {
						$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';
						$html1  .= '</td>';
					}
					$j++;
				}
				$html1 .= '</tbody>';
				$html1 .= '</table>';
				$json['is_owners'] = 1;
			}else {
				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="control-label current_owner_label" style= "margin-left: 4%;font-size:14px;" for="input-trainer_name" >No Owners assign to current Horse</label><br/>';

					$html1 .= '<a target="_blank" style="font-size:14px;margin-left:70%;" class="btn btn-primary" href="index.php?route=transaction/ownership_shift_module&token='.$this->session->data['token'].'&horse_id='.$this->request->get['filterHorseId'].'&pp=name_change">Add Ownership</a>';
                    $html1 .= '<a id="ref-owners" style="margin-left:15px;font-size:14px;" class="btn btn-primary">Refresh Owners</a>';

				$html1 .= '</div>';

			}
			$json['status'] = 1;

			$json['html_owner'] = $html1;
			$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	
public function getPartnersData(){

		$html = '';
		$json = array();
		$partners_datasss = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$this->request->get['parent_owner_id']."'");
		$owner_datasss = $this->db->query("SELECT `id`,`owner_name` FROM `owners` WHERE id ='".$this->request->get['parent_owner_id']."'")->row;

		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		if($partners_datasss->num_rows > 0){

			$html .= '<div class="modal fade" id="partners_data_'.$this->request->get['parent_owner_id'].'" role="dialog">';
                $html .= '<div class="modal-dialog">';
                     $html .= '<div class="modal-content">';
                        $html .= '<div class="modal-header">';
                            $html .= '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                            $html .= '<h4 class="modal-title">Partners Details</h4>';
                        $html .= '</div>';
                        $html .= '<div class="modal-body partners_div" style="height: 440px;">';
							$html .= '<table class="table table-bordered partner_tbl" id="tableid_'.$this->request->get['parent_owner_id'].'">';
								$html .= '<thead>';
								$html .= '<tr>';
									$html .= '<td style="width: 40px;text-align: center;"></td>';
									$html .= '<td style="text-align: center;vertical-align: middle;">Partners Name</td>';
								$html .= '</tr>';
								$html .= '</thead>';
								$html .= '<tbody>';
									$html .= '<span style="font-size:16px;"><b>Name : </b> '.$owner_datasss['owner_name'].'</span><br><br>';
									foreach ($partners_datasss->rows as $key => $value) {
										$represntative_datasss = $this->db->query("SELECT `represntative_id` FROM `horse_to_owner` WHERE `to_owner_id` ='".$this->request->get['parent_owner_id']."' AND `horse_id` ='".$this->request->get['horse_id']."' AND `owner_share_status` = '1' AND `represntative_id` = '".$value['partner_id']."' ");

										$repres_status = ($represntative_datasss->num_rows > 0) ? "1" : "0";

										$html .= '<tr>';
											$html .= '<td class="p_'.$key.'" style="text-align: center;">';
												if($repres_status == 1){
													$html .= '<input type="checkbox" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][is_reprentative]"  class="parent_value" id="owner_partner_id_'.$key.'" checked="checked />';

												} else {

													$html .= '<input type="checkbox" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][is_reprentative]"  class="parent_value" id="owner_partner_id_'.$key.'" />';
												}
												$html .= '<input type="hidden" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][partner_id]" value="'.$value['partner_id'].'"  id="partner_id_'.$key.'"  />';
												$html .= '<input type="hidden" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][owner_id]" value="'.$owner_datasss['id'].'"  id="partner_id_'.$key.'"  />';

											$html .= '</td>';
											$html .= '<td class="p_'.$key.'" style="text-align: left;">';
												$html .= '<span style="font-size: 14px;"> '.$value['partner_name'] .'</span>';
											$html .= '</td>';
										$html .= '</tr>';
									}
								$html .= '</tbody>';
							$html .= '</table>';
						$html .= '</div>';
                    $html .= '</div>';
                $html .= '</div>';
           $html .=  '</div>';
		}
		$json = $html;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getChargeOwnersHistory(){


		$html = '';
		$json = array();
		$arrival_charge_owners_data = $this->db->query("SELECT * FROM `arrival_charge_owners` WHERE arrival_charge_id ='".$this->request->get['charge_history_id']."'");
		//$owner_datasss = $this->db->query("SELECT `id`,`owner_name` FROM `owners` WHERE id ='".$this->request->get['parent_owner_id']."'")->row;

		// echo'<pre>';
		// print_r($arrival_charge_owners_data->rows);
		// exit;
		if($arrival_charge_owners_data->num_rows > 0){

			$html .= '<div class="modal fade" id="chargeownershis_data_'.$this->request->get['charge_history_id'].'" role="dialog">';
                $html .= '<div class="modal-dialog">';
                     $html .= '<div class="modal-content">';
                        $html .= '<div class="modal-header">';
                            $html .= '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                            $html .= '<h4 class="modal-title">Owners Charge Details</h4>';
                        $html .= '</div>';
                        $html .= '<div class="modal-body partners_div" style="height: 440px;">';
							$html .= '<table class="table table-bordered partner_tbl" id="tableid_'.$this->request->get['charge_history_id'].'">';
								$html .= '<thead>';
								$html .= '<tr>';
									$html .= '<td style="text-align: center;vertical-align: middle;">Owner Name</td>';
									$html .= '<td style="text-align: center;vertical-align: middle;">Charge Amount</td>';

								$html .= '</tr>';
								$html .= '</thead>';
								$html .= '<tbody>';
									//$html .= '<span style="font-size:16px;"><b>Name : </b> '.$owner_datasss['owner_name'].'</span><br><br>';
									foreach ($arrival_charge_owners_data->rows as $key => $value) {
										$html .= '<tr>';
											$html .= '<td class="p_'.$key.'" style="text-align: left;">';
												$html .= '<span style="font-size: 14px;"> '.$value['owner_name'] .'</span>';
											$html .= '</td>';
											$html .= '<td class="p_'.$key.'" style="text-align: right;">';
												$html .= '<span style="font-size: 14px;"> '.$value['charge_amt'] .'</span>';
											$html .= '</td>';
										$html .= '</tr>';
									}
								$html .= '</tbody>';
							$html .= '</table>';
						$html .= '</div>';
                    $html .= '</div>';
                $html .= '</div>';
           $html .=  '</div>';
		}
		$json = $html;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
	
	public function autocompleteHorse() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['horse_name'])) {
			$this->load->model('transaction/arrival_charges');

			$results = $this->model_transaction_arrival_charges->getHorsesAuto($this->request->get['horse_name']);


			if($results){
				foreach ($results as $result) {
					$foal_date = date('Y',strtotime($result['foal_date']));

					$snat = ($result['sire_nationality'] != '') ? '['.$result['sire_nationality'].']' : "";
					$mnat = ($result['dam_nationality'] != '') ? '['.$result['dam_nationality'].']' : "";


					$json[] = array(
						'horse_id' => $result['horseseq'],
						'pedegree' => $result['color'].' '.$result['sex'].'  '.$foal_date.' by '.$result['sire_name'].$snat.' ex '.$result['dam_name'].$mnat,
						'horse_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		/*echo '<pre>';print_r($json);
			exit;*/
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['horse_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function arrival_datas() {
		$json = array();
		// echo '<pre>';
		// print_r($this->request->post);
		// exit;
		$json['status'] = 0;

		if($this->request->post['license_type'] == 'A'){
			$levy_charge = $this->request->post['levy_charge'];
			$arrival_charge = 0;
		} else {
			$arrival_charge = $this->request->post['arrival_charge'];
			$levy_charge = 0;
		}

		if(isset($this->request->post['filterHorseName'])){
			$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
			$last_update_date = $date->format('Y-m-d h:i:s');
			$dates = date('Y-m-d', strtotime($this->request->post['reg_date']));
			$this->db->query("INSERT INTO `arrival_charges` SET 
				`horse_name` = '".$this->request->post['filterHorseName']."',
				`horse_id` = '".$this->request->post['filterHorseId']."',
				`trainer_name` = '".$this->request->post['trainer_name']."',
				`trainer_id` = '".$this->request->post['trainer_id']."',
				`license_type` = '".$this->request->post['license_type']."',
				`date` = '".$dates."',
				`arrival_charge` = '".$arrival_charge."',
				`levy_charge` = '".$levy_charge."',
				`entry_status` = '1',
				`user_id` = '".$this->request->post['user_id']."',
				`last_update_date` = '".$last_update_date."' 
				");

			$arrival_charge_id = $this->db->getLastId();

			$total_amt = ($levy_charge > 0) ? $levy_charge : $arrival_charge;
			
			$amt = 0;
			foreach ($this->request->post['owner_datas'] as $okey => $ovalue) {
				$amt = ($total_amt * $ovalue['owner_percentage'] / 100); 
				$this->db->query("INSERT INTO `arrival_charge_owners` SET 
				`arrival_charge_id` = '".$arrival_charge_id."',
				`horse_id` = '".$this->request->post['filterHorseId']."',
				`owner_id` = '".$ovalue['to_owner_hidden']."',
				`owner_name` = '".$ovalue['to_owner_name_hidden']."',
				`entry_date` = '".$dates."',
				`charge_amt` = '".$amt."'
				");
			}

			$this->session->data['success'] = "Arrival Charge Accepted Successfully !";
			$json['status'] = 1;

		}

		

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
}
