<?php
class Controllertransactionentries extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/entries');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/entries');
		$this->getList();
	}

	public function add() {
		$this->load->language('transaction/entries');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/entries');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
			$this->model_transaction_entries->addProspectus($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/entries', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('transaction/entries');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/entries');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
			$this->model_transaction_entries->editProspectus($this->request->get['pros_id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/entries', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('transaction/entries');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/entries');
		if (isset($this->request->post['selected'])/* && $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $pros_id) {
				$this->model_transaction_entries->deleteprospectus($pros_id);
			}
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/entries', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getList();
	}

	public function repair() {
		$this->load->language('transaction/entries');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/entries');
		if ($this->validateRepair()) {
			$this->model_transaction_entries->repairCategories();
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('transaction/entries', 'token=' . $this->session->data['token'], true));
		}
		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['filter_race_name'])) {
			$data['filter_race_name'] =$this->request->get['filter_race_name'];
		} else{
			$data['filter_race_name']  ="";
		}

		if(isset($this->request->get['filter_race_date'])){
			$data['filter_race_date'] = $this ->request->get['filter_race_date'];
		}
		 else{
			$data['filter_race_date'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/entries', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/entries/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/entries/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/entries/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_race_name' => $data['filter_race_name'],
			'filter_race_date' => $data['filter_race_date']
		);

		$category_total = $this->model_transaction_entries->getTotalCategories($filter_data);
		$results = $this->model_transaction_entries->getprospectus($filter_data);
		foreach ($results as $result) {
			if ($result['race_type'] == 'Handicap Race') {
				$race_type = 'Handicap';
			} elseif ($result['race_type'] == 'Term Race') {
				$race_type = 'Term';
			}

			if ($result['race_type'] == 'Handicap Race') {
				$class_grade = $result['class'];
			} elseif ($result['race_type'] == 'Term Race') {
				$class_grade = $result['grade'];
			}
			$final_class = urlencode(html_entity_decode($result['class'], ENT_QUOTES, 'UTF-8'));
			$data['prospectusdatas'][] = array(
				'pros_id' 	  	=> $result['pros_id'],
				'race_date'   	=> $result['race_date'],
				'race_name'	  	=> $result['race_name'],
				'race_type'	  	=> $race_type,
				'class_grade'	  	=> $class_grade,
				//'grade'	  	=> $result['grade'],
				'distance' =>$result['distance'],
				'race_category'	  	=> $result['race_category'],
				'max_stakes_race'	  	=> $result['max_stakes_race'],
				'foreign_jockey_eligible'	  	=> $result['foreign_jockey_eligible'],
				'edit'        	=> $this->url->link('transaction/entries/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $result['pros_id'] . '&class=' . $final_class . $url, true),
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/entries', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/entries', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('transaction/entries', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/entries_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['pros_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');


		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}


		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_horse_name'])) {
			$filter_horse_name =$this->request->get['filter_horse_name'];
		} else{
			$filter_horse_name  ="";

		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$filter_data1 = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_horse_name' => $filter_horse_name
		);
		if (isset($this->request->get['pros_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_transaction_entries->getCategory($this->request->get['pros_id']);

		} else {
			$category_info = $this->model_transaction_entries->getCategory($this->request->get['pros_id']);

		}
		$data['horseDatas'] =array();
		$horseDatas =array();

		$results = $this->model_transaction_entries->getHorses($category_info);
		//echo "<pre>";print_r($category_info);exit;
		$current_date=date('Y-m-d');

		foreach ($results as $result) {
			$trainer  =("SELECT trainer_name,trainer_id,left_date_of_charge from `horse_to_trainer`  WHERE horse_id ='".$result['horseseq']."' AND (left_date_of_charge >='".$current_date."' OR left_date_of_charge='1970-01-01') ");
			// echo '<pre>';
		
			$final_trainer = $this->db->query($trainer);
			if($final_trainer->num_rows > 0){
				$final_trainer1 = $final_trainer->row;
				$trainer_name = $final_trainer1['trainer_name'];
				$trainer_id = $final_trainer1['trainer_id'];
			} else {
				$trainer_name = '';
				$trainer_id ='';
			}
			//echo "<pre>";print_r($final_trainer);exit;
			$horseDatas[] = array(
				'horseID' 	  			=> $result['horseseq'],
				'horseName'   			=> $result['official_name'],
				'horseColor'  			=> $result['color'],
				'horseSex'  			=> $result['sex'],
				'horseAge'  			=> $result['age'],
				'horseRating'  			=> $result['rating'],
				'trainer_name'  		=> $trainer_name,
				'trainer_id'  			=>$trainer_id
			);
		}

		$data['horseDatas'] = $horseDatas;

		//exit;
		 

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['linkedOwner'])) {
			$data['error_linkedOwner'] = $this->error['linkedOwner'];
		} else {
			$data['error_linkedOwner'] = '';
		}


		if (isset($this->error['entry_error'])) {
			$data['entry_error'] = $this->error['entry_error'];
		} else {
			$data['entry_error'] = '';
		}

		

		if (isset($this->error['gst_number'])) {
			$data['error_gst_number'] = $this->error['gst_number'];
		} else {
			$data['error_gst_number'] = '';
		}
		
		if (isset($this->error['sharedColor'])) {
			$data['error_sharedColor'] = $this->error['sharedColor'];
		} else {
			$data['error_sharedColor'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/entries', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/entries/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/entries/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('transaction/entries', 'token=' . $this->session->data['token'] . $url, true);
		
		if(isset($this->request->get['pros_id'])){
			$prize_distribution = $this->model_transaction_entries->getprizedistribution($this->request->get['pros_id']);
		}
		if(isset($this->request->get['pros_id'])){
			$entrydata1 =$this->model_transaction_entries->getentrydatas1($this->request->get['pros_id']);
		}
		if(isset($this->request->get['pros_id'])){
			$entrydata =$this->model_transaction_entries->getentrydatas($this->request->get['pros_id']);
		}
		if(isset($this->request->get['pros_id'])){
			$entrydata_id =$this->model_transaction_entries->getentrydata_id($this->request->get['pros_id']);
		}
		// echo '<pre>';
		// print_r($entrydata);
		// exit;
		$data['token'] = $this->session->data['token'];
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['count'])) {
			$data['count'] = $this->request->post['count'];
		} elseif (!empty($entrydata)) {
			$data['count'] = $entrydata['count'];
		} else {
			$data['count'] = '';
		}
		if (isset($this->request->post['race_description'])) {
			$data['race_description'] = $this->request->post['race_description'];
		} elseif(!empty($entrydata)){
			if($entrydata['race_description'] != ''){
				$data['race_description'] = $entrydata['race_description'];
			} 
		} elseif(!empty($category_info)){
				$data['race_description'] = $category_info['race_description'];
			}

		 else {
			$data['race_description'] = '';
		}

		// echo '<pre>';
		// print_r($category_info);
		// 	exit;

		if (isset($this->request->post['class'])) {
			$data['class'] = $this->request->post['class'];
		} elseif ($this->request->get['class'] != '') {
			$data['class'] = $this->request->get['class'];
		} else {
			$data['class'] = '';
		}
		if (!empty($entrydata1)) {
			$data['entrydata1s'] = $entrydata1;
		} else {
      		$data['entrydata1s'] = array();
    	}

    	if (!empty($entrydata_id)) {
			$data['entrydata_id'] = $entrydata_id;
		} else {
      		$data['entrydata_id'] = array();
    	}
    	$abc = '';
    	foreach ($data['entrydata_id'] as $tnamekey => $tnamevalue) {
    		//$abc .= $tnamevalue['horse_id'].',';
    		$abc []= $tnamevalue['horse_id'];
		}
		/*echo '<pre>';
    	print_r($abc);
    	exit;*/
		if (!empty($abc)) {
			$data['abc'] = $abc;
		} else {
      		$data['abc'] = array();
    	}

		if (!empty($prize_distribution)) {
			$data['prize_distributions'] = $prize_distribution;
		} else {
      		$data['prize_distributions'] = array();
    	}

		if (isset($this->request->post['pros_id'])) {
			$data['pros_id'] = $this->request->post['pros_id'];
		} elseif (!empty($category_info)) {
			$data['pros_id'] = $category_info['pros_id'];
		} else {
			$data['pros_id'] = '';
		}
		if (isset($this->request->post['year'])) {
			$data['year'] = $this->request->post['year'];
		} elseif (!empty($category_info)) {
			$data['year'] = $category_info['year'];
		} else {
			$data['year'] = '';
		}
		if (isset($this->request->post['season'])) {
			$data['season'] = $this->request->post['season'];
		} elseif (!empty($category_info)) {
			$data['season'] = $category_info['season'];
		} else {
			$data['season'] = '';
		}

		if (isset($this->request->post['grade'])) {
			$data['grade'] = $this->request->post['grade'];
		} elseif (!empty($category_info)) {
			$data['grade'] = $category_info['grade'];
		} else {
			$data['grade'] = '';
		}

		if (isset($this->request->post['class'])) {
			$data['class'] = $this->request->post['class'];
		} elseif (!empty($category_info)) {
			$data['class'] = $category_info['class'];
		} else {
			$data['class'] = '';
		}

		if (isset($this->request->post['bonafite_cnts'])) {
			$data['bonafite_cnts'] = $this->request->post['bonafite_cnts'];
		} elseif (!empty($entrydata)) {
			$data['bonafite_cnts'] = $entrydata['bonafide_count'];
		} else {
			$data['bonafite_cnts'] = 0;
		}

		if (isset($this->request->post['race_day'])) {
			$data['race_day'] = $this->request->post['race_day'];
		} elseif (!empty($category_info)) {
			$data['race_day'] = $category_info['race_day'];
		} else {
			$data['race_day'] = '';
		}
		if (isset($this->request->post['evening_race'])) {
			$data['evening_race'] = $this->request->post['evening_race'];
		} elseif (!empty($category_info)) {
			$data['evening_race'] = $category_info['evening_race'];
		} else {
			$data['evening_race'] = '';
		}
		if (isset($this->request->post['race_name'])) {
			$data['race_name'] = $this->request->post['race_name'];
		} elseif (!empty($category_info)) {
			$data['race_name'] = $category_info['race_name'];
		} else {
			$data['race_name'] = '';
		}
		if (isset($this->request->post['race_type'])) {
			$data['race_type'] = $this->request->post['race_type'];
		} elseif (!empty($category_info)) {
			$data['race_type'] = $category_info['race_type'];
		} else {
			$data['race_type'] = '';
		}
		if (isset($this->request->post['race_category'])) {
			$data['race_category'] = $this->request->post['race_category'];
		} elseif (!empty($category_info)) {
			$data['race_category'] = $category_info['race_category'];
		} else {
			$data['race_category'] = '';
		}
		if (isset($this->request->post['distance'])) {
			$data['distance'] = $this->request->post['distance'];
		} elseif (!empty($category_info)) {
			$data['distance'] = $category_info['distance'];
		} else {
			$data['distance'] = '';
		}
		if (isset($this->request->post['entries_close_date_time'])) {
			$data['entries_close_date_time'] = $this->request->post['entries_close_date_time'];
		} elseif (!empty($category_info)) {
			$data['entries_close_date_time'] = $category_info['entries_close_date_time'];
		} else {
			$data['entries_close_date_time'] = '';
		}
		if (isset($this->request->post['handicaps_date_time'])) {
			$data['handicaps_date_time'] = $this->request->post['handicaps_date_time'];
		} elseif (!empty($category_info)) {
			$data['handicaps_date_time'] = $category_info['handicaps_date_time'];
		} else {
			$data['handicaps_date_time'] = '';
		}
		if (isset($this->request->post['acceptance_date_time'])) {
			$data['acceptance_date_time'] = $this->request->post['acceptance_date_time'];
		} elseif (!empty($category_info)) {
			$data['acceptance_date_time'] = $category_info['acceptance_date_time'];
		} else {
			$data['acceptance_date_time'] = '';
		}

		if (isset($this->request->post['declaration_date_time'])) {
			$data['declaration_date_time'] = $this->request->post['declaration_date_time'];
		} elseif (!empty($category_info)) {
			$data['declaration_date_time'] = $category_info['declaration_date_time'];
		} else {
			$data['declaration_date_time'] = '';
		}
		if (isset($this->request->post['race_date'])) {
			$data['race_date'] = date('d-m-Y', strtotime($this->request->post['race_date']));
		} elseif (!empty($category_info)) {
			$data['race_date'] = date('d-m-Y', strtotime($category_info['race_date']));
		} else {
			$data['race_date'] = '';
		}
		if (isset($this->request->post['entries_close_date'])) {
			$data['entries_close_date'] = date('d-m-Y', strtotime($this->request->post['entries_close_date']));
		} elseif (!empty($category_info)) {
			$data['entries_close_date'] = date('d-m-Y', strtotime($category_info['entries_close_date']));
		} else {
			$data['entries_close_date'] = '';
		}
		if (isset($this->request->post['handicaps_date'])) {
			$data['handicaps_date'] = date('d-m-Y', strtotime($this->request->post['handicaps_date']));
		} elseif (!empty($category_info)) {
			$data['handicaps_date'] = date('d-m-Y', strtotime($category_info['handicaps_date']));
		} else {
			$data['handicaps_date'] = '';
		}
		if (isset($this->request->post['acceptance_date'])) {
			$data['acceptance_date'] = date('d-m-Y', strtotime($this->request->post['acceptance_date']));
		} elseif (!empty($category_info)) {
			$data['acceptance_date'] = date('d-m-Y', strtotime($category_info['acceptance_date']));
		} else {
			$data['acceptance_date'] = '';
		}
		if (isset($this->request->post['declaration_date'])) {
			$data['declaration_date'] = date('d-m-Y', strtotime($this->request->post['declaration_date']));
		} elseif (!empty($category_info)) {
			$data['declaration_date'] = date('d-m-Y', strtotime($category_info['declaration_date']));
		} else {
			$data['declaration_date'] = '';
		}
		if (isset($this->request->post['run_thrice_date'])) {
			$data['run_thrice_date'] = date('d-m-Y', strtotime($this->request->post['run_thrice_date']));
		} elseif (!empty($category_info)) {
			$data['run_thrice_date'] = date('d-m-Y', strtotime($category_info['run_thrice_date']));
		} else {
			$data['run_thrice_date'] = '';
		}
		if (isset($this->request->post['run_not_placed_date'])) {
			$data['run_not_placed_date'] = date('d-m-Y', strtotime($this->request->post['run_not_placed_date']));
		} elseif (!empty($category_info)) {
			$data['run_not_placed_date'] = date('d-m-Y', strtotime($category_info['run_not_placed_date']));
		} else {
			$data['run_not_placed_date'] = '';
		}
		if (isset($this->request->post['run_not_won_date'])) {
			$data['run_not_won_date'] = date('d-m-Y', strtotime($this->request->post['run_not_won_date']));
		} elseif (!empty($category_info)) {
			$data['run_not_won_date'] = date('d-m-Y', strtotime($category_info['run_not_won_date']));
		} else {
			$data['run_not_won_date'] = '';
		}
		if (isset($this->request->post['run_and_not_won_during_mtg_date'])) {
			$data['run_and_not_won_during_mtg_date'] = date('d-m-Y', strtotime($this->request->post['run_and_not_won_during_mtg_date']));
		} elseif (!empty($category_info)) {
			$data['run_and_not_won_during_mtg_date'] = date('d-m-Y', strtotime($category_info['run_and_not_won_during_mtg_date']));
		} else {
			$data['run_and_not_won_during_mtg_date'] = '';
		}
		if (isset($this->request->post['age_from'])) {
			$data['age_from'] = $this->request->post['age_from'];
		} elseif (!empty($category_info)) {
			$data['age_from'] = $category_info['age_from'];
		} else {
			$data['age_from'] = '';
		}

		if (isset($this->request->post['age_to'])) {
			$data['age_to'] = $this->request->post['age_to'];
		} elseif (!empty($category_info)) {
			$data['age_to'] = $category_info['age_to'];
		} else {
			$data['age_to'] = '';
		}
		if (isset($this->request->post['max_win'])) {
			$data['max_win'] = $this->request->post['max_win'];
		} elseif (!empty($category_info)) {
			$data['max_win'] = $category_info['max_win'];
		} else {
			$data['max_win'] = '';
		}
		if (isset($this->request->post['sex'])) {
			$data['sex'] = $this->request->post['sex'];
		} elseif (!empty($category_info)) {
			$data['sex'] = $category_info['sex'];
		} else {
			$data['sex'] = '';
		}
		if (isset($this->request->post['maidens'])) {
			$data['maidens'] = $this->request->post['maidens'];
		} elseif (!empty($category_info)) {
			$data['maidens'] = $category_info['maidens'];
		} else {
			$data['maidens'] = '';
		}
		if (isset($this->request->post['unraced'])) {
			$data['unraced'] = $this->request->post['unraced'];
		} elseif (!empty($category_info)) {
			$data['unraced'] = $category_info['unraced'];
		} else {
			$data['unraced'] = '';
		}
		if (isset($this->request->post['not1_2_3'])) {
			$data['not1_2_3'] = $this->request->post['not1_2_3'];
		} elseif (!empty($category_info)) {
			$data['not1_2_3'] = $category_info['not1_2_3'];
		} else {
			$data['not1_2_3'] = '';
		}
		if (isset($this->request->post['unplaced'])) {
			$data['unplaced'] = $this->request->post['unplaced'];
		} elseif (!empty($category_info)) {
			$data['unplaced'] = $category_info['unplaced'];
		} else {
			$data['unplaced'] = '';
		}
		if (isset($this->request->post['short_not'])) {
			$data['short_not'] = $this->request->post['short_not'];
		} elseif (!empty($category_info)) {
			$data['short_not'] = $category_info['short_not'];
		} else {
			$data['short_not'] = '';
		}
		if (isset($this->request->post['max_stakes_race'])) {
			$data['max_stakes_race'] = $this->request->post['max_stakes_race'];
		} elseif (!empty($category_info)) {
			$data['max_stakes_race'] = $category_info['max_stakes_race'];
		} else {
			$data['max_stakes_race'] = '';
		}
		if (isset($this->request->post['run_thrice'])) {
			$data['run_thrice'] = $this->request->post['run_thrice'];
		} elseif (!empty($category_info)) {
			$data['run_thrice'] = $category_info['run_thrice'];
		} else {
			$data['run_thrice'] = '';
		}
		if (isset($this->request->post['run_not_placed'])) {
			$data['run_not_placed'] = $this->request->post['run_not_placed'];
		} elseif (!empty($category_info)) {
			$data['run_not_placed'] = $category_info['run_not_placed'];
		} else {
			$data['run_not_placed'] = '';
		}
		if (isset($this->request->post['run_not_won'])) {
			$data['run_not_won'] = $this->request->post['run_not_won'];
		} elseif (!empty($category_info)) {
			$data['run_not_won'] = $category_info['run_not_won'];
		} else {
			$data['run_not_won'] = '';
		}
		if (isset($this->request->post['run_and_not_won_during_mtg'])) {
			$data['run_and_not_won_during_mtg'] = $this->request->post['run_and_not_won_during_mtg'];
		} elseif (!empty($category_info)) {
			$data['run_and_not_won_during_mtg'] = $category_info['run_and_not_won_during_mtg'];
		} else {
			$data['run_and_not_won_during_mtg'] = '';
		}
		if (isset($this->request->post['no_whip'])) {
			$data['no_whip'] = $this->request->post['no_whip'];
		} elseif (!empty($category_info)) {
			$data['no_whip'] = $category_info['no_whip'];
		} else {
			$data['no_whip'] = '';
		}
		if (isset($this->request->post['foreign_jockey_eligible'])) {
			$data['foreign_jockey_eligible'] = $this->request->post['foreign_jockey_eligible'];
		} elseif (!empty($category_info)) {
			$data['foreign_jockey_eligible'] = $category_info['foreign_jockey_eligible'];
		} else {
			$data['foreign_jockey_eligible'] = '';
		}
		if (isset($this->request->post['foreign_horse_eligible'])) {
			$data['foreign_horse_eligible'] = $this->request->post['foreign_horse_eligible'];
		} elseif (!empty($category_info)) {
			$data['foreign_horse_eligible'] = $category_info['foreign_horse_eligible'];
		} else {
			$data['foreign_horse_eligible'] = '';
		}
		if (isset($this->request->post['sire_dam'])) {
			$data['sire_dam'] = $this->request->post['sire_dam'];
		} elseif (!empty($category_info)) {
			$data['sire_dam'] = $category_info['sire_dam'];
		} else {
			$data['sire_dam'] = '';
		}
		if (isset($this->request->post['stakes_in'])) {
			$data['stakes_in'] = $this->request->post['stakes_in'];
		} elseif (!empty($category_info)) {
			$data['stakes_in'] = $category_info['stakes_in'];
		} else {
			$data['stakes_in'] = '';
		}
		if (isset($this->request->post['trophy_value_amount'])) {
			$data['trophy_value_amount'] = $this->request->post['trophy_value_amount'];
		} elseif (!empty($category_info)) {
			$data['trophy_value_amount'] = $category_info['trophy_value_amount'];
		} else {
			$data['trophy_value_amount'] = '';
		}
		if (isset($this->request->post['positions'])) {
			$data['positions'] = $this->request->post['positions'];
		} elseif (!empty($category_info)) {
			$data['positions'] = $category_info['positions'];
		} else {
			$data['positions'] = '';
		}
		if (isset($this->request->post['stakes_money'])) {
			$data['stakes_money'] = $this->request->post['stakes_money'];
		} elseif (!empty($category_info)) {
			$data['stakes_money'] = $category_info['stakes_money'];
		} else {
			$data['stakes_money'] = '';
		}
		if (isset($this->request->post['bonus_applicable'])) {
			$data['bonus_applicable'] = $this->request->post['bonus_applicable'];
		} elseif (!empty($category_info)) {
			$data['bonus_applicable'] = $category_info['bonus_applicable'];
		} else {
			$data['bonus_applicable'] = '';
		}
		if (isset($this->request->post['sweepstake_race'])) {
			$data['sweepstake_race'] = $this->request->post['sweepstake_race'];
		} elseif (!empty($category_info)) {
			$data['sweepstake_race'] = $category_info['sweepstake_race'];
		} else {
			$data['sweepstake_race'] = '';
		}


		


		if (isset($this->request->post['sweepstake_of'])) {
			$data['sweepstake_of'] = $this->request->post['sweepstake_of'];
		} elseif (!empty($category_info)) {
			$data['sweepstake_of'] = $category_info['sweepstake_of'];
		} else {
			$data['sweepstake_of'] = '';
		}
		if (isset($this->request->post['no_of_forfiet'])) {
			$data['no_of_forfiet'] = $this->request->post['no_of_forfiet'];
		} elseif (!empty($category_info)) {
			$data['no_of_forfiet'] = $category_info['no_of_forfiet'];
		} else {
			$data['no_of_forfiet'] = '';
		}
		if (isset($this->request->post['entry_date'])) {
			$data['entry_date'] = $this->request->post['entry_date'];
		} elseif (!empty($category_info)) {
			$data['entry_date'] = $category_info['entry_date'];
		} else {
			$data['entry_date'] = '';
		}
		if (isset($this->request->post['entry_time'])) {
			$data['entry_time'] = $this->request->post['entry_time'];
		} elseif (!empty($category_info)) {
			$data['entry_time'] = $category_info['entry_time'];
		} else {
			$data['entry_time'] = '';
		}
		if (isset($this->request->post['entry_fees'])) {
			$data['entry_fees'] = $this->request->post['entry_fees'];
		} elseif (!empty($category_info)) {
			$data['entry_fees'] = $category_info['entry_fees'];
		} else {
			$data['entry_fees'] = '';
		}
		if (isset($this->request->post['additional_entry'])) {
			$data['additional_entry'] = $this->request->post['additional_entry'];
		} elseif (!empty($category_info)) {
			$data['additional_entry'] = $category_info['additional_entry'];
		} else {
			$data['additional_entry'] = '';
		}
		if (isset($this->request->post['bonus_to_breeder'])) {
			$data['bonus_to_breeder'] = $this->request->post['bonus_to_breeder'];
		} elseif (!empty($category_info)) {
			$data['bonus_to_breeder'] = $category_info['bonus_to_breeder'];
		} else {
			$data['bonus_to_breeder'] = '';
		}
		if (isset($this->request->post['remarks'])) {
			$data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($category_info)) {
			$data['remarks'] = $category_info['remarks'];
		} else {
			$data['remarks'] = '';
		}

		if (isset($this->request->post['lower_class_aligible'])) {
			$data['lower_class_aligible'] = $this->request->post['lower_class_aligible'];
		} elseif (!empty($category_info)) {
			$data['lower_class_aligible'] = $category_info['lower_class_aligible'];
		} else {
			$data['lower_class_aligible'] = '';
		}

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/entries_form', $data));
	}
	

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/entries')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		// echo'<pre>';
		// print_r($this->request->post);
		// exit;
		$class = $this->request->post['class'];
		$distance = $this->request->post['distance'];
		$bonafite_cnts = $this->request->post['bonafite_cnts'];


		$entry_datas = $this->db->query("SELECT entry FROM class_entry WHERE class = '".$class."' AND distance = '".$distance."' ")->row;
		if($entry_datas['entry'] > $bonafite_cnts){
			$this->error['entry_error'] = " Total Horse Entry is less ";
		}


		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'transaction/entries')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'transaction/entries')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('transaction/entries');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'owner_Id'	  => $this->request->get['owner_Id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_entries->getOwners($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'owner_id' => $result['id'],
					'owner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['owner_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function bonaFiteDatasCount(){
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;

		$json = array();
		$current_hid =  $this->request->get['current_horse_id'];
		$owners_idss = '';
		$current_horse_owners_id = array();
		$owners_datas = $this->db->query("SELECT to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$current_hid."' AND owner_share_status = '1'")->rows;
		foreach ($owners_datas as $okey => $ovalue) {
			$owners_idss .= "'" . $ovalue['to_owner_id']. "'".','; 
		}
		$current_horse_owners_idss = rtrim($owners_idss,',');
		//$current_horse_owners_id = explode(',' , $current_horse_owners_idss);
		$bon_cnt = 1;
		if($this->request->get['pre_horse_idss'] != ''){
			$pre_horse_ids = explode(',' , $this->request->get['pre_horse_idss']);
			// echo '<pre>';
			// print_r(($pre_horse_ids));
			// exit;
			foreach ($pre_horse_ids as $key => $value) {
				if($value != ''){
					$owners_dataz = $this->db->query("SELECT to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$value."' AND to_owner_id NOT IN ($current_horse_owners_idss)  AND owner_share_status = '1'");
					   //echo "SELECT to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$value."' AND to_owner_id NOT IN ($current_horse_owners_idss)  AND owner_share_status = '1'";
					if($owners_dataz->num_rows > 0){
						// echo'<pre>';
						// print_r($owners_dataz);
						$bon_cnt = $this->request->get['bona_cnt'] + 1;
					} 
						// echo '<pre>';
						// print_r(($owners_dataz));

				}
			}

		}

					// exit;

		
		$json['bonafite_cnts'] = $bon_cnt;
		//$json['preowners_id'] = $this->request->post['data'];

		$json['owners_id'] = $current_horse_owners_idss;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
