<?php
class Controllertransactionregisterationmodule extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/registeration_module');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/registeration_module');
		$this->getList();
	}

	

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['filter_race_name'])) {
			$data['filter_race_name'] =$this->request->get['filter_race_name'];
		} else{
			$data['filter_race_name']  ="";
		}

		if(isset($this->request->get['filter_acceptance_date'])){
			$data['filter_acceptance_date'] = $this ->request->get['filter_acceptance_date'];
		}
		 else{
			$data['filter_acceptance_date'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/registeration_module', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/registeration_module/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/registeration_module/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/registeration_module/repair', 'token=' . $this->session->data['token'] . $url, true);


		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/registeration_module/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/registeration_module/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['categories'] = array();

		$data['registration_type'] = array(
			'Name Registration'  => 'Initial Name Registration',
			'Rename'  => 'Change Of Name',
			'SBD'  => 'SBD',
		);




		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_race_name' => $data['filter_race_name'],
			'filter_acceptance_date' => $data['filter_acceptance_date']
		);
		$category_total = $this->model_transaction_registeration_module->getTotalCategories($filter_data);
		$results = $this->model_transaction_registeration_module->getprospectus($filter_data);
		// echo "<pre>";
		// print_r($results);
		// exit;
		foreach ($results as $result) {
			$data['acceptancedatas'][] = array(
				'entry_id' 	  	=> $result['entry_id'],
				'pros_id' 	  	=> $result['pros_id'],
				'race_description' 	  	=> $result['race_description'],
				'race_name'	  	=> $result['race_name'],
				'race_type'	  	=> $result['race_type'],
				'count'	  		=> $result['count'],
				'handicap_status' =>$result['handicap_status'],
				'race_date'	  	=> date('d-m-Y', strtotime($result['race_date'])),
				'class'	  	=> $result['class'],
				'distance' =>$result['distance'],
				'foreign_jockey'	  	=> $result['foreign_jockey_eligible'],
				'edit'        	=> $this->url->link('transaction/registeration_module/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $result['pros_id'] . $url, true),
			);
		}

		if (isset($this->request->get['horse_id'])) {
			$horse_datass = $this->db->query("SELECT `official_name`, `horseseq` FROM `horse1` WHERE horseseq = '".$this->request->get['horse_id']."'")->row;
			$traniner_datass = $this->db->query("SELECT `trainer_name`, `trainer_id` FROM `horse_to_trainer` WHERE horse_id = '".$this->request->get['horse_id']."'")->row;

			$data['horse_name'] = $horse_datass['official_name'];
			$data['horse_id'] = $horse_datass['horseseq'];
			$data['trainer_name'] = $traniner_datass['trainer_name'];
			$data['trainer_id'] = $traniner_datass['trainer_id'];
			$data['gt_id'] = 1;
		} else {
			$data['horse_name'] = '';
			$data['horse_id'] = '';
			$data['trainer_name'] = '';
			$data['trainer_id'] = '';
			$data['gt_id'] = 0;
		}

		if(isset($this->request->post['fromdate'])) {
			$data['fromdate'] = $this->request->post['fromdate'];
		} else {
			$data['fromdate'] = date('d-m-Y');
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/registeration_module', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/registeration_module', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('transaction/registeration_module', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('transaction/registeration_module_list', $data));
	}


	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/registeration_module')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['linkedOwnerName'] as $key => $value) {
			if ($value['linked_owner_id'] == '') {
				$this->error['linkedOwner'] = 'Please select valid linked owner.';
			}
		}

		if ($this->request->post['gstType'] == 'Registered' && $this->request->post['gstNo'] == '') {
			$this->error['gst_number'] = 'GST No can not be blank.';
		}

		foreach ($this->request->post['owner_shared_color'] as $key => $value) {
			if ($value['shared_owner_id'] == '') {
				$this->error['sharedColor'] = 'Please select valid owner to share color.';
			}
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	public function autoHorseToTrainer() {
		$json = array();
		if (isset($this->request->get['horse_id'])) {
			$this->load->model('transaction/registeration_module');
			$filter_data = array(
				'horse_id' => $this->request->get['horse_id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_registeration_module->gethorsetoTrainer($filter_data);
			foreach ($results as $result) {
				$licence_typess = $this->db->query("SELECT license_type FROM trainers WHERE trainer_code = '" . (int)$result['trainer_id'] . "'")->row;
				//echo "<pre>";print_r("SELECT license_type FROM trainers WHERE trainer_code = '" . (int)$result['trainer_id'] . "'");exit;

				$json[] = array(
					'trainer_id' => $result['trainer_id'],
					'trainer_code' => $result['trainer_code'],
					'licence_type' => $licence_typess['license_type'],
					'trainer_name'        => strip_tags(html_entity_decode($result['trainer_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getOwnerData() {
		$json = '';
		$json['is_owners'] = 0;

		//echo "<pre>";print_r($this->request->get);
		if (isset($this->request->get['horse_id'])) {
			$this->load->model('transaction/registeration_module');
			$filter_data = array(
				'horse_id' => $this->request->get['horse_id'],
			);
			$results = $this->model_transaction_registeration_module->gethorsetoOwner($filter_data);

			$results_provi = $this->model_transaction_registeration_module->gethorsetoOwnerprovi($filter_data);

			//echo "<pre>";print_r($results);exit;
			$html1 = '';
			if($results || $results_provi){
				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="col-sm-2 control-label current_owner_label" style= "margin-left: 1%;font-size:14px;" for="input-trainer_name" >Provisional ownership:</label><br/>';
					$html1 .= '<a target="_blank" style="font-size:14px;margin-left:50%;" class="btn btn-primary" href="index.php?route=transaction/ownership_shift_module&token='.$this->session->data['token'].'&horse_id='.$this->request->get['horse_id'].'&pp=name_change">Change Ownership</a>';
                    $html1 .= '<a id="ref-owners" style="margin-left:15px;font-size:14px;" class="btn btn-primary">Refresh Owners</a>';

				$html1 .= '</div>';
				
				$html1 .= '<table class="table table-bordered ownertable" style= "margin-top: 2%;width: 98%;margin-left: 1%;">';
				$html1 .= '<thead>';
				$html1 .= '<tr>';
						$html1 .= '<td class="text-center;" style="vertical-align: middle;">Name</td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Period </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Share </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Color </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Contingency </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Ownership </td>';
				$html1 .= '</tr>';
				$html1 .= '</thead>';
				$html1 .= '<tbody>';

				$j=1;
				
				$parent_data =array();
				foreach ($results_provi as $resulta) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$resulta['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$resulta['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html1  .= '<tr>';
					

					$from_date = date('d-m-Y', strtotime($resulta['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($resulta['end_date_of_ownership']));

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html1  .= '<span style="cursor:pointer"><a id="partners_'.$resulta['to_owner_id'].'"  class="partners" >'.$resulta['to_owner'].'</a></span><br>';
						}else {
							$html1  .= '<span >'.$resulta['to_owner'].'</span><br>';
						}
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_hidden]" value="'.$resulta['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$j.'"  />';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_name_hidden]" value="'.$resulta['to_owner'].'"  />';
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';  
						$provisional_ownership  = ($resulta['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';  
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($resulta['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($resulta['end_date_of_ownership'])) : "";
							$html1 .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html1  .= '</td>';
					


					$html1  .= '<td class="r_'.$j.'" style="text-align: right;">';  
						$html1  .= '<span>'.$resulta['owner_percentage'].'%'.'</span>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_percentage]" value="'.$resulta['owner_percentage'].'" id="owner_percentage_'.$j.'"  />';
					$html1  .= '</td>';
					
					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
					if($resulta['owner_color'] == 'Yes'  && $last_color == $resulta['horse_to_owner_id']){
						$html1  .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'" checked="checked"  />';
					} else {
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'"  />';
					} 
					$html1  .= '</td>';
					$contengency = ($resulta['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($resulta['cont_percentage'] != 0) ? $resulta['cont_percentage'] : "";
					$cont_amount = ($resulta['cont_amount'] != 0) ? $resulta['cont_amount'] : "0";

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html1  .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$resulta['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
						}
					$html1  .= '</td>';
						

					if($parent_data){
						if($parent_data['to_owner'] != '' && $resulta['ownership_type'] != 'Sale'){
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								if($resulta['ownership_type'] == 'Lease'){
									$html1 .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else {
									$html1 .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}else {
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								$html1 .= '<span> </span>';
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="0" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}
					} else {
						$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';
						$html1  .= '</td>';
					}
					$j++;
				}
				$html1 .= '</tbody>';
				$html1 .= '</table>';









				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="col-sm-2 control-label current_owner_label" style= "margin-left: 1%;font-size:14px;" for="input-trainer_name" >Current Owners:</label><br/>';

				$html1 .= '</div>';
				
				$html1 .= '<table class="table table-bordered ownertable" style= "margin-top: 2%;width: 98%;margin-left: 1%;">';
				$html1 .= '<thead>';
				$html1 .= '<tr>';
						$html1 .= '<td class="text-center;" style="vertical-align: middle;">Name</td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Period </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Share </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Color </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Contingency </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Ownership </td>';
				$html1 .= '</tr>';
				$html1 .= '</thead>';
				$html1 .= '<tbody>';

				$j=1;
				
				$parent_data =array();
				foreach ($results as $resulta) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$resulta['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$resulta['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html1  .= '<tr>';
					

					$from_date = date('d-m-Y', strtotime($resulta['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($resulta['end_date_of_ownership']));

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html1  .= '<span style="cursor:pointer"><a id="partners_'.$resulta['to_owner_id'].'"  class="partners" >'.$resulta['to_owner'].'</a></span><br>';
						}else {
							$html1  .= '<span >'.$resulta['to_owner'].'</span><br>';
						}
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_hidden]" value="'.$resulta['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$j.'"  />';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_name_hidden]" value="'.$resulta['to_owner'].'"  />';
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';  
						$provisional_ownership  = ($resulta['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';  
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($resulta['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($resulta['end_date_of_ownership'])) : "";
							$html1 .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html1  .= '</td>';
					


					$html1  .= '<td class="r_'.$j.'" style="text-align: right;">';  
						$html1  .= '<span>'.$resulta['owner_percentage'].'%'.'</span>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_percentage]" value="'.$resulta['owner_percentage'].'" id="owner_percentage_'.$j.'"  />';
					$html1  .= '</td>';
					
					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
					if($resulta['owner_color'] == 'Yes'  && $last_color == $resulta['horse_to_owner_id']){
						$html1  .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'" checked="checked"  />';
					} else {
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'"  />';
					} 
					$html1  .= '</td>';
					$contengency = ($resulta['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($resulta['cont_percentage'] != 0) ? $resulta['cont_percentage'] : "";
					$cont_amount = ($resulta['cont_amount'] != 0) ? $resulta['cont_amount'] : "0";

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html1  .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$resulta['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
						}
					$html1  .= '</td>';
						

					if($parent_data){
						if($parent_data['to_owner'] != '' && $resulta['ownership_type'] != 'Sale'){
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								if($resulta['ownership_type'] == 'Lease'){
									$html1 .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else {
									$html1 .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}else {
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								$html1 .= '<span> </span>';
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="0" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}
					} else {
						$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';
						$html1  .= '</td>';
					}
					$j++;
				}
				$html1 .= '</tbody>';
				$html1 .= '</table>';
				$json['is_owners'] = 1;
			}else {
				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="control-label current_owner_label" style= "margin-left: 4%;font-size:14px;" for="input-trainer_name" >No Owners assign to current Horse</label><br/>';

					$html1 .= '<a target="_blank" style="font-size:14px;margin-left:70%;" class="btn btn-primary" href="index.php?route=transaction/ownership_shift_module&token='.$this->session->data['token'].'&horse_id='.$this->request->get['horse_id'].'&pp=name_change">Add Ownership</a>';
                    $html1 .= '<a id="ref-owners" style="margin-left:15px;font-size:14px;" class="btn btn-primary">Refresh Owners</a>';

				$html1 .= '</div>';

			}
		}

		$json['status'] = 1;

		$json['html_owner'] = $html1;


		//$json = $html1;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function refreshOwners(){
		$this->load->model('transaction/registeration_module');

		$filter_data = array(
				'horse_id' => $this->request->get['filterHorseId'],
				
			);


		$resultz = $this->model_transaction_registeration_module->gethorsetoOwner($filter_data);

		$results_provi = $this->model_transaction_registeration_module->gethorsetoOwnerprovi($filter_data);
			$html1 = '';
			$json['is_owners'] = 0;
			if($resultz || $results_provi){
				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filterHorseId']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="col-sm-2 control-label current_owner_label" style= "margin-left: 1%;font-size:14px;" for="input-trainer_name" >Provisional ownership:</label><br/>';

					$html1 .= '<a target="_blank" style="font-size:14px;margin-left:50%;" class="btn btn-primary" href="index.php?route=transaction/ownership_shift_module&token='.$this->session->data['token'].'&horse_id='.$this->request->get['filterHorseId'].'&pp=name_change">Change Ownership</a>';
                    $html1 .= '<a id="ref-owners" style="margin-left:15px;font-size:14px;" class="btn btn-primary">Refresh Owners</a>';
                    
				$html1 .= '</div>';
				
				$html1 .= '<table class="table table-bordered ownertable" style= "margin-top: 2%;width: 98%;margin-left: 1%;">';
				$html1 .= '<thead>';
				$html1 .= '<tr>';
						$html1 .= '<td class="text-center;" style="vertical-align: middle;">Name</td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Period </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Share </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Color </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Contingency </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Ownership </td>';
				$html1 .= '</tr>';
				$html1 .= '</thead>';
				$html1 .= '<tbody>';

				$j=1;
				
				$parent_data =array();
				foreach ($results_provi as $resulta) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$resulta['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$resulta['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html1  .= '<tr>';
					

					$from_date = date('d-m-Y', strtotime($resulta['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($resulta['end_date_of_ownership']));

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html1  .= '<span style="cursor:pointer"><a id="partners_'.$resulta['to_owner_id'].'"  class="partners" >'.$resulta['to_owner'].'</a></span><br>';
						}else {
							$html1  .= '<span >'.$resulta['to_owner'].'</span><br>';
						}
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_hidden]" value="'.$resulta['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$j.'"  />';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_name_hidden]" value="'.$resulta['to_owner'].'"  />';
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';  
						$provisional_ownership  = ($resulta['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';  
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($resulta['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($resulta['end_date_of_ownership'])) : "";
							$html1 .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html1  .= '</td>';
					


					$html1  .= '<td class="r_'.$j.'" style="text-align: right;">';  
						$html1  .= '<span>'.$resulta['owner_percentage'].'%'.'</span>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_percentage]" value="'.$resulta['owner_percentage'].'" id="owner_percentage_'.$j.'"  />';
					$html1  .= '</td>';
					
					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
					if($resulta['owner_color'] == 'Yes'  && $last_color == $resulta['horse_to_owner_id']){
						$html1  .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'" checked="checked"  />';
					} else {
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'"  />';
					} 
					$html1  .= '</td>';
					$contengency = ($resulta['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($resulta['cont_percentage'] != 0) ? $resulta['cont_percentage'] : "";
					$cont_amount = ($resulta['cont_amount'] != 0) ? $resulta['cont_amount'] : "0";

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html1  .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$resulta['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
						}
					$html1  .= '</td>';
						

					if($parent_data){
						if($parent_data['to_owner'] != '' && $resulta['ownership_type'] != 'Sale'){
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								if($resulta['ownership_type'] == 'Lease'){
									$html1 .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else {
									$html1 .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}else {
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								$html1 .= '<span> </span>';
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="0" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}
					} else {
						$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';
						$html1  .= '</td>';
					}
					$j++;
				}
				$html1 .= '</tbody>';
				$html1 .= '</table>';







				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filterHorseId']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="col-sm-2 control-label current_owner_label" style= "margin-left: 1%;font-size:14px;" for="input-trainer_name" >Current Owners:</label><br/>';

				$html1 .= '</div>';
				
				$html1 .= '<table class="table table-bordered ownertable" style= "margin-top: 2%;width: 98%;margin-left: 1%;">';
				$html1 .= '<thead>';
				$html1 .= '<tr>';
						$html1 .= '<td class="text-center;" style="vertical-align: middle;">Name</td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Type <br>  (O / L / SL) </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Period </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Share </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Color </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Contingency </td>';
					  	$html1 .= '<td class="text-center" style="vertical-align: middle;">Ownership </td>';
				$html1 .= '</tr>';
				$html1 .= '</thead>';
				$html1 .= '<tbody>';

				$j=1;
				
				$parent_data =array();
				foreach ($resultz as $resulta) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$resulta['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$resulta['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html1  .= '<tr>';
					

					$from_date = date('d-m-Y', strtotime($resulta['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($resulta['end_date_of_ownership']));

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html1  .= '<span style="cursor:pointer"><a id="partners_'.$resulta['to_owner_id'].'"  class="partners" >'.$resulta['to_owner'].'</a></span><br>';
						}else {
							$html1  .= '<span >'.$resulta['to_owner'].'</span><br>';
						}
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_hidden]" value="'.$resulta['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$j.'"  />';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][to_owner_name_hidden]" value="'.$resulta['to_owner'].'"  />';
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';  
						$provisional_ownership  = ($resulta['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html1  .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html1  .= '</td>';

					$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';  
							
						if($resulta['ownership_type'] == 'Lease'){
							$html1  .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($resulta['ownership_type'] == 'Sub Lease'){
								
							$html1  .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($resulta['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($resulta['end_date_of_ownership'])) : "";
							$html1 .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html1  .= '</td>';
					


					$html1  .= '<td class="r_'.$j.'" style="text-align: right;">';  
						$html1  .= '<span>'.$resulta['owner_percentage'].'%'.'</span>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_percentage]" value="'.$resulta['owner_percentage'].'" id="owner_percentage_'.$j.'"  />';
					$html1  .= '</td>';
					
					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
					if($resulta['owner_color'] == 'Yes'  && $last_color == $resulta['horse_to_owner_id']){
						$html1  .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'" checked="checked"  />';
					} else {
						$html1  .= '<input type="hidden" name="owner_datas['.$j.'][owner_color]" value="'.$resulta['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$j.'"  />';
					} 
					$html1  .= '</td>';
					$contengency = ($resulta['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($resulta['cont_percentage'] != 0) ? $resulta['cont_percentage'] : "";
					$cont_amount = ($resulta['cont_amount'] != 0) ? $resulta['cont_amount'] : "0";

					$html1  .= '<td class="r_'.$j.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html1  .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$resulta['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
						}
					$html1  .= '</td>';
						

					if($parent_data){
						if($parent_data['to_owner'] != '' && $resulta['ownership_type'] != 'Sale'){
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								if($resulta['ownership_type'] == 'Lease'){
									$html1 .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else {
									$html1 .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}else {
							$html1 .= '<td class="r_'.$j.'" style="text-align: left;">';  
								$html1 .= '<span> </span>';
								$html1 .= '<input type="hidden" name="owner_datas['.$j.'][parent_to_owner]" value="0" id="parent_to_owner_'.$j.'"  />';
							$html1 .= '</td>';
						}
					} else {
						$html1  .= '<td class="r_'.$j.'" style="text-align: left;">';
						$html1  .= '</td>';
					}
					$j++;
				}
				$html1 .= '</tbody>';
				$html1 .= '</table>';
				$json['is_owners'] = 1;
			}else {
				$html1 .= '<div class="form-group col-sm-12">';
					$html1 .= '<label class="control-label current_owner_label" style= "margin-left: 4%;font-size:14px;" for="input-trainer_name" >No Owners assign to current Horse</label><br/>';

					$html1 .= '<a target="_blank" style="font-size:14px;margin-left:70%;" class="btn btn-primary" href="index.php?route=transaction/ownership_shift_module&token='.$this->session->data['token'].'&horse_id='.$this->request->get['filterHorseId'].'&pp=name_change">Add Ownership</a>';
                    $html1 .= '<a id="ref-owners" style="margin-left:15px;font-size:14px;" class="btn btn-primary">Refresh Owners</a>';

				$html1 .= '</div>';

			}
			$json['status'] = 1;

			$json['html_owner'] = $html1;
			$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}


	public function getRegisteredHorse() {
		$json = '';

		//echo "<pre>";print_r($this->request->get);
		if (isset($this->request->get['horse_id'])) {
			$this->load->model('transaction/registeration_module');
			$filter_data = array(
				'horse_id' => $this->request->get['horse_id'],
			);
			$results = $this->model_transaction_registeration_module->getRegiserationmodule($filter_data);

			//echo "<pre>";print_r($results);exit;
			$html = '';
			
			if($results){
				
					$html .= '<label class="col-sm-2 control-label name_change_label" for="input-trainer_name" >Name Change History:</label><br/>';


				$html .= '<table class="table table-bordered regitabless" style= "margin-top: 2%;">';
				$html .= '<thead>';
				$html .= '<tr>';
					$html .= '<td style="text-align: center;">Horse Name</td>';
					$html .= '<td style="width: 160px;text-align: center;">Registration Type</td>';
					$html .= '<td style="text-align: center;">Registration Date</td>';
					$html .= '<td style="text-align: center;">Reason</td>';
				$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';

				$i=1;
				$parent_data =array();
				foreach ($results as $result) {
					if ($result['modification'] == '1') {
						$modify = '(modification)';
					} else {
						$modify = '';
					}
					if ($result['registration_date'] == '1970-01-01') {
						$reg_date = '';
					} else {
						$reg_date = date('d-m-Y', strtotime($result['registration_date']));
					}
					$html .= '<tr>';
						$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							$html .= '<span>'.$result['horse_name'].'</span>';
						$html .= '</td>';
						$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							$html .= '<span>'.$result['registeration_type'].' '.$modify.'</span>';
						$html .= '</td>';

						$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							$html .= '<span>'.$reg_date.'</span>';
						$html .= '</td>';

						$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							$html .= '<span>'.$result['remark'].'</span>';
						$html .= '</td>';
					$html .= '</tr>';
					$i++;
				}
				$html .= '</tbody>';
				$html .= '</table>';
			} 
		}

		$json = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function sumbit_registeration_form() {
		//echo "inn";
		//echo "<pre>";print_r($this->request->post);exit;
		$json = array();
		$results = array();

		if (isset($this->request->post)) {
			// echo'inn';
			// exit;
			$this->load->model('transaction/registeration_module');
			

			$last_id = $this->model_transaction_registeration_module->insert_registration($this->request->post);
			$results = $this->model_transaction_registeration_module->getdetails($last_id);

			$this->session->data['success'] = "Proposed Name Successfully  Registered !";
		}
		$json = $results;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function autocompleteHorse() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['horse_name'])) {
			$this->load->model('transaction/registeration_module');

			$results = $this->model_transaction_registeration_module->getHorsesAuto($this->request->get['horse_name']);


			if($results){
				foreach ($results as $result) {
					$foal_date = date('Y',strtotime($result['foal_date']));

					$snat = ($result['sire_nationality'] != '') ? '['.$result['sire_nationality'].']' : "";
					$mnat = ($result['dam_nationality'] != '') ? '['.$result['dam_nationality'].']' : "";


					$json[] = array(
						'horse_id' => $result['horseseq'],
						'pedegree' => $result['color'].' '.$result['sex'].'  '.$foal_date.' by '.$result['sire_name'].$snat.' ex '.$result['dam_name'].$mnat,
						'horse_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		/*echo '<pre>';print_r($json);
			exit;*/
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['horse_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function reg_datas() {
		$json = array();

		//echo "<pre>"; print_r($this->request->get);exit;
		if (isset($this->request->get['type'])) {
			$this->load->model('transaction/registeration_module');

			$filter_data = array(
				'id' => $this->request->get['id'],
				'type' => $this->request->get['type'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_registeration_module->getregdatas($filter_data);
			//echo "<pre>"; print_r($results); exit;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['id'];
		}

		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
}
