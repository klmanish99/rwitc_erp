<?php
class Controllertransactionamounttran extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/amount_tran');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['horse_id'])) {
			$hourse_id = $this->request->get['horse_id'];
		} else {
			$hourse_id = '';
		}

		

		if (isset($this->request->get['color'])) {
			$data['color_link'] = 1;
		} else {
			$data['color_link'] = 0;
		}



		if (isset($this->request->get['order'])) {
			$data['order_link'] = 1;
		} else {
			$data['order_link'] = 0;
		}


		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}


		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_hourse_name']) ) {
			$url .= '&filter_hourse_name=' . $this->request->get['filter_hourse_name'];
		}

		if (isset($this->request->get['horse_id']) ) {
			$url .= '&horse_id=' . $this->request->get['horse_id'];
		}

		if (isset($this->request->get['color'])) {
			$url .= '&color=' . $this->request->get['color'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		/*if (isset($this->request->get['filter_trainer_name']) ) {
			$url .= '&filter_trainer_name=' . $this->request->get['filter_trainer_name'];
		}

		if (isset($this->request->get['filter_trainer_id']) ) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}*/

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/horse_undertaking_charge', 'token=' . $this->session->data['token'] . $url, true)
		);

		

		$data['undertakingcharges_datas'] = array();
		//$hourse_id = 3;
		$filter_data = array(
			'filter_date_end'	     => $filter_date_end,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin'),
			'hourse_id' => $hourse_id,
			
		);

		if (isset($this->request->get['horse_id']) && (isset($this->request->get['order']) || isset($this->request->get['color']))) {
			$data['action'] = $this->url->link('transaction/amount_tran/edit', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/amount_tran/edit', 'token=' . $this->session->data['token'] . $url, true);
		}

		if (isset($this->request->post['amount'])) {
			$data['amount'] = $this->request->post['amount'];
		} else {
			$data['amount'] = '';
		}

		if (isset($this->request->post['letter_date'])) {
			$data['letter_date'] = $this->request->post['letter_date'];
		} else {
			$data['letter_date'] = '';
		}

		if (isset($this->request->post['received_date'])) {
			$data['received_date'] = $this->request->post['received_date'];
		} else {
			$data['received_date'] = '';
		}
		

		$order_total ='';
	
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['valierr_amount'])) {
			$data['valierr_amount'] = $this->error['valierr_amount'];
		}

		if (isset($this->error['valierr_letter_date'])) {
			$data['valierr_letter_date'] = $this->error['valierr_letter_date'];
		}

		if (isset($this->error['valierr_received_date'])) {
			$data['valierr_received_date'] = $this->error['valierr_received_date'];
		}

		//echo "<pre>";print_r($data['final_owner']);exit;

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_date_start'] = $this->language->get('column_date_start');
		$data['column_date_end'] = $this->language->get('column_date_end');
		$data['column_orders'] = $this->language->get('column_orders');
		$data['column_products'] = $this->language->get('column_products');
		$data['column_tax'] = $this->language->get('column_tax');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_group'] = $this->language->get('entry_group');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		

		if (isset($this->request->get['horse_id'])) {
			$horse_name = $this->db->query("SELECT official_name FROM `horse1` WHERE horseseq = '".$this->request->get['horse_id']."' ")->row;

			$trainer_name = $this->db->query("SELECT trainer_name FROM `horse_to_trainer` WHERE horse_id = '".$this->request->get['horse_id']."' order by horse_to_trainer_id DESC ")->row;
			
			$data['horse_name'] = $horse_name['official_name'];
			$data['trainer_name'] = $trainer_name['trainer_name'];
		}


		
		$data['filter_date_end'] = $filter_date_end;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		

		$this->response->setOutput($this->load->view('transaction/amount_tran', $data));
	}

	public function edit() {
		$this->load->language('transaction/ownership_shift_module');
		$this->document->setTitle($this->language->get('heading_title'));
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo "<pre>";print_r($this->request->post);exit;
			
			 //$this->model_transaction_amount_tran->updateTransDatas($this->request->post);
			$letter_date = date('Y-m-d', strtotime($this->request->post['letter_date']));
			$received_date = date('Y-m-d', strtotime($this->request->post['received_date']));

			if(isset($this->request->get['horse_id'])){
				if($this->request->get['horse_id'] > 0){
					$horse_id = $this->request->get['horse_id'];
				} else {
					$horse_id = 0;	
				}
			}
			$gst = (($this->request->post['amount'] * 18)/100);



			$this->db->query("INSERT INTO `account_ownership_transfer` SET amount ='".(int)$this->request->post['amount']."', gst = '".($gst)."' , letter_date = '".$letter_date."' , received_date = '".$received_date."' ,date_added = NOW() ,horse_id ='".($horse_id)."' ");
			$trans_id = $this->db->getLastId();
			
			$this->log->write("INSERT INTO `account_ownership_transfer` SET amount ='".(int)$this->request->post['amount']."', gst = '".($gst)."' , letter_date = '".$letter_date."' , received_date = '".$received_date."' ,date_added = NOW() ,horse_id ='".($horse_id)."' ");
			
			$active_owners = $this->db->query("SELECT horse_to_owner_id, to_owner_id, to_owner, owner_percentage FROM `horse_to_owner` WHERE horse_id = '".$horse_id."' AND owner_share_status = 1 ")->rows;
			foreach ($active_owners as $akey => $avalue) {
				$amt = (($this->request->post['amount'] * $avalue['owner_percentage']) / 100);
				$new_owners = $this->db->query("SELECT id, share FROM `horse_to_owner_charges` WHERE horse_id = '".$horse_id."' AND horse_to_owner_id = '".$avalue['horse_to_owner_id']."' AND amount = 0 ");
				if($new_owners->num_rows > 0){
					$this->db->query("UPDATE `horse_to_owner_charges` SET amount = '".$amt."', trans_id = '".$trans_id."' WHERE id = '".$new_owners->row['id']."'");
				} else {
					$this->db->query("INSERT INTO `horse_to_owner_charges` SET horse_to_owner_id = '".$avalue['horse_to_owner_id']."', trans_id = '".$trans_id."',  horse_id ='".($horse_id)."', owner_id = '".$avalue['to_owner_id']."', owner_name = '".$avalue['to_owner']."', share = '".$avalue['owner_percentage']."', amount ='".$amt."'");
				}
			}
			
			$this->session->data['success'] = 'Transaction Added' ;
			

			$url = '';

			if (isset($this->request->get['horse_id'])) {
				$url .= '&horse_id=' . $this->request->get['horse_id'];
			}

			if (isset($this->request->get['color'])) {
				$url .= '&color=' . $this->request->get['color'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'], true));

		}
		$this->index();
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/amount_tran')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		//echo '<pre>';print_r($this->request->post);exit;

		if($this->request->post['amount'] == ''){
			$this->error['valierr_amount'] = 'Please Enter Amount!';
		}


		if($this->request->post['letter_date'] == ''){
			$this->error['valierr_letter_date'] = 'Please Select Date!';
		}

		if($this->request->post['received_date'] == ''){
			$this->error['valierr_received_date'] = 'Please Select Date!';
		}

		$letter_date = date('Y-m-d', strtotime($this->request->post['letter_date']));
		$received_date = date('Y-m-d', strtotime($this->request->post['received_date']));
		if($letter_date > $received_date){
			$this->error['valierr_received_date'] = 'Please Select Valid Date!';
		}
		

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		/*echo '<pre>';
		print_r($this->error);exit;*/
		return !$this->error;
		//exit;
	}

	public function autocompleteHorse() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['horse_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesAuto($this->request->get['horse_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'horse_id' => $result['horseseq'],
						'horse_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		/*echo '<pre>';print_r($json);
			exit;*/
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['horse_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}
}