<?php
class Controllertransactioncancelownership extends Controller {
	private $error = array();
	public function index() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		//$this->load->language('transaction/cancel_ownership');

		$this->document->setTitle('Cancel Lease Ownership');

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = date('Y-m-d');
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date']) ) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/cancel_ownership', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('transaction/cancel_ownership');
		$data['action'] = $this->url->link('transaction/cancel_ownership/add', 'token=' . $this->session->data['token'] , true);

		$data['back_monthly_daily_reconsilation'] = $this->url->link('report/monthly_medicine_transaction', 'token=' . $this->session->data['token'] , true);

		$data['ownerships_types'] = array(
			'Sale'  => 'Sale',
			'Lease'  => 'Lease',
			'Sub Lease'  => 'Sub Lease',
		);

		if (isset($this->request->get['horse_id'])) {
			$horse_datass = $this->db->query("SELECT `official_name`, `horseseq` FROM `horse1` WHERE horseseq = '".$this->request->get['horse_id']."'")->row;
			$traniner_datass = $this->db->query("SELECT `trainer_name`, `trainer_id` FROM `horse_to_trainer` WHERE horse_id = '".$this->request->get['horse_id']."'");
			if($traniner_datass->num_rows > 0){
				$data['trainer_name'] = $traniner_datass->row['trainer_name'];
				$data['trainer_id'] = $traniner_datass->row['trainer_id'];
			} else {
				$data['trainer_name'] = "";
				$data['trainer_id'] = "";
			}

			// echo'<pre>';
			// print_r($traniner_datass);
			// exit;
			$data['horse_name'] = $horse_datass['official_name'];
			$data['horse_id'] = $horse_datass['horseseq'];
			
			$data['gt_id'] = 1;
			$data['arrival_charge'] = 0;
			$data['charge_type'] = '';
			if($this->request->get['pp'] == 'arrival_charge'){
				$data['check_provisonal'] = 1;
				$data['arrival_charge'] = 1;
				$data['charge_type'] = 'Arrival Charges';
			} elseif($this->request->get['pp'] == 'name_change') {
				$data['check_provisonal'] = 1;
				$data['arrival_charge'] = 1;
				$data['charge_type'] = 'Name Registration';

			} elseif($this->request->get['pp'] == 'Other') {
				$data['arrival_charge'] = 0;
				$data['charge_type'] = 'Other';

			}
		} else {
			$data['horse_name'] = '';
			$data['horse_id'] = '';
			$data['trainer_name'] = '';
			$data['trainer_id'] = '';
			$data['gt_id'] = 0;
			$data['arrival_charge'] = 0;
			$data['charge_type'] = '';
			$data['check_provisonal'] = 0;

		}

		if(isset($this->request->post['fromdate'])) {
			$data['fromdate'] = $this->request->post['fromdate'];
		} else {
			$data['fromdate'] = date('d-m-Y');
		}
		

		$filter_data = array(
			'filter_date'	     => $filter_date,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin'),
		);

		$order_total = 0;
		$data['results'] = 0;

		//echo "<pre>";print_r($filter_data);exit;

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('Cancel Lease Own');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_date'] = $this->language->get('column_date');
		$data['column_Trainer'] = $this->language->get('column_Trainer');
		$data['column_Amount'] = $this->language->get('column_Amount');
		$data['column_Charge'] = $this->language->get('column_Charge');
		$data['column_horse_name'] = $this->language->get('column_horse_name');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_group'] = $this->language->get('entry_group');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['error_medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['error_medicine_error'] = '';
		}

		if (isset($this->error['filter_date'])) {
			$data['filter_date'] = $this->error['filter_date'];
		} else {
			$data['filter_date'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['medicine_error'] = '';
		}

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		/*$data['med_transaction'] = array();

		$data['med_transaction'] = $this->model_transaction_cancel_ownership->getMedicineTranscation($filter_data);*/

		/*if ($results) {
			foreach ($results as $key => $result) {
				$data['med_transaction'][] = array(
					'product_name' 	  			=> $result['product_name'],
					'total_trans'   			=> $result['total_trans'],
				);
	
			}
		}*/

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('transaction/cancel_ownership', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['filter_date'] = date('d-m-Y', strtotime($filter_date));

		$this->response->setOutput($this->load->view('transaction/cancel_ownership', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/cancel_ownership')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		/*echo '<pre>';
		print_r($this->request->post);
		exit;*/
		

		if ((utf8_strlen($this->request->post['filter_date']) == '')) {
			$this->error['filter_date'] = "Please Select date";
		}


		if(!isset($this->request->post['productraw_datas'])){
			$this->error['medicine_error'] = 'Please Enter Atleast Entry';
		} 
		$datas = $this->request->post;
		return !$this->error;
	}

	public function getOwnerData() {
			//echo "<pre>";print_r($this->request->get);exit;
		$json = array();
		if (isset($this->request->get['filter_hourse_id'])) {
			$this->load->model('transaction/cancel_ownership');
			$filter_data = array(
				'horse_id' => $this->request->get['filter_hourse_id'],
			);

			$current_owners = $this->db->query("SELECT horse_to_owner_id, end_date_of_ownership, owner_percentage, grandpa_id, horse_id, entry_count FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' AND owner_share_status = 1 ");
			
			$his_status = 0;
			if($current_owners->num_rows > 0){
				foreach ($current_owners->rows as $ckey => $cvalue) {
					$current_date = date('Y-m-d');
					if(($current_date > $cvalue['end_date_of_ownership']) && $cvalue['end_date_of_ownership'] != '0000-00-00'){
					$his_status = 1;
						
					$owner_per = $cvalue['owner_percentage'];
						$this->db->query("UPDATE `horse_to_owner` SET owner_share_status = 0 ,cancel_lease_status = 'Cancel' WHERE horse_to_owner_id = '".$cvalue['horse_to_owner_id']."' AND horse_id = '".$cvalue['horse_id']."' ");

						$grand_parent_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id = '".$cvalue['grandpa_id']."'")->row;


						$pre_own_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE to_owner_id = '".$grand_parent_datas['to_owner_id']."' AND owner_share_status = 1 AND  horse_id = '".$cvalue['horse_id']."' AND owner_percentage < 100 AND ownership_type = 'Sale' ");

						if($pre_own_datas->num_rows > 0){
							$this->db->query("UPDATE `horse_to_owner` SET owner_share_status = 0 WHERE horse_to_owner_id = '".$pre_own_datas->row['horse_to_owner_id']."' AND horse_id = '".$cvalue['horse_id']."' ");
							$owner_per = $pre_own_datas->row['owner_percentage'] + $cvalue['owner_percentage'];
						}
						$entry_count = $cvalue['entry_count'] + 1;
						$this->db->query("INSERT INTO `horse_to_owner` SET  
						`horse_id` = '" . $this->db->escape($grand_parent_datas['horse_id']) . "',
						`horse_group_id` = '" . $this->db->escape($grand_parent_datas['horse_group_id']) . "',
						`trainer_id` = '" . $this->db->escape($grand_parent_datas['trainer_id']) . "',
						`ownership_type` = '".$this->db->escape($grand_parent_datas['ownership_type'])."',
						`parent_group_id` = '".$grand_parent_datas['horse_group_id']."',
						`date_of_ownership` = '" . $this->db->escape($grand_parent_datas['date_of_ownership']) . "',
						`end_date_of_ownership` = '" . $this->db->escape($grand_parent_datas['end_date_of_ownership']) . "',
						`remark_horse_to_owner` = '" . $this->db->escape($grand_parent_datas['remark_horse_to_owner']) . "',
						`owner_percentage` = '" . $this->db->escape($owner_per) . "',
						`to_owner` = '" . $this->db->escape($grand_parent_datas['to_owner']) . "',
						`to_owner_id` = '" . $this->db->escape($grand_parent_datas['to_owner_id']) . "',
						`contengency` = '" . $this->db->escape($grand_parent_datas['contengency']) . "',
						`cont_percentage` = '" . $this->db->escape($grand_parent_datas['cont_percentage']) . "',
						`cont_amount` = '" . $this->db->escape($grand_parent_datas['cont_amount']) . "',
						`cont_place` = '" . $this->db->escape($grand_parent_datas['cont_place']) . "',
						`win_gross_stake` = '" . $this->db->escape($grand_parent_datas['win_gross_stake']) . "',
						`win_net_stake` = '" . $this->db->escape($grand_parent_datas['win_net_stake']) . "',
						`millionover` = '" . $this->db->escape($grand_parent_datas['millionover']) . "',
						`millionover_amt` = '" . $this->db->escape($grand_parent_datas['millionover_amt']) . "',
						`grade1` = '" . $this->db->escape($grand_parent_datas['grade1']) . "',
						`grade2` = '" . $this->db->escape($grand_parent_datas['grade2']) . "',
						`grade3` = '" . $this->db->escape($grand_parent_datas['grade3']) . "',
						`owner_share_status` = '1',
						`entry_count` = '".$entry_count."',
						`grandpa_id` = '".$cvalue['grandpa_id']."',
						`provisional_ownership` = '".$grand_parent_datas['provisional_ownership']."'
						");
						
					}
				}
				if($his_status == 1){
					$history_owners = $this->db->query("SELECT `batch_id` FROM `horse_to_owner_history` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' ORDER BY id DESC LIMIT 1 ");
					$batch_id = 1;
					if($history_owners->num_rows > 0){
						$batch_id = $history_owners->row['batch_id'] + 1;
						$this->db->query("UPDATE `horse_to_owner_history` SET status = 1 WHERE batch_id = '".$history_owners->row['batch_id']."' ");
					}

					$allCurrOwners = $this->db->query("SELECT horse_id, horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' AND owner_share_status = 1 ");
					if($allCurrOwners->num_rows > 0){
						foreach ($allCurrOwners->rows as $ohkey => $ohvalue) {
							$this->db->query("INSERT INTO `horse_to_owner_history` SET 
								`horse_id` = '" . $ohvalue['horse_id'] . "',
								`batch_id` = '" . $batch_id . "',
								`horse_to_owner_id` = '" . $ohvalue['horse_to_owner_id'] . "',
								`status` = 0
								");
						}
					}
				}
			}
			
			$results = $this->model_transaction_cancel_ownership->gethorsetoOwner($filter_data);

			//echo "<pre>";print_r($results);exit;
			$html = '';
			
			if($results){
				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html .= '<table class="table table-bordered ownertable">';
				$html .= '<thead>';
				$html .= '<tr>';
					
					$html .= '<td style="text-align: center;vertical-align: middle;">Name</td>';
					$html .= '<td style="text-align: center;vertical-align: middle;">Type <br> (O / L / SL)</td>';
					$html .= '<td style="text-align: center;vertical-align: middle;">Period</td>';

					$html .= '<td style="width: 160px;text-align: center;vertical-align: middle;">Share</td>';
					
					$html .= '<td style="width: 100px;text-align: center;vertical-align: middle;">Color</td>';
					$html .= '<td style="width: 200px;text-align: center;vertical-align: middle;">Contingency</td>';
					$html .= '<td style="width: 300px;text-align: center;vertical-align: middle;">Ownership</td>';
					$html .= '<td style="width: 300px;text-align: center;vertical-align: middle;">Cancel Lease</td>';

				$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';

				$i=1;
				$parent_data =array();
				$sub_parent_data = array();
				foreach ($results as $result) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$result['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$result['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html .= '<tr>';
					


					$from_date = date('d-m-Y', strtotime($result['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($result['end_date_of_ownership']));

					$html .= '<td class="r_'.$i.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html .= '<span style="cursor:pointer"><a id="partners_'.$result['to_owner_id'].'"  class="partners" >'.$result['to_owner'].'</a></span><br>';
						}else {
							$html .= '<span >'.$result['to_owner'].'</span><br>';
						}
							$html .= '<input type="hidden" name="owner_datas['.$i.'][from_owner_hidden]" value="'.$result['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$i.'"  />';
					$html .= '</td>';

					$html .= '<td class="r_'.$i.'" style="text-align: center;">';  
						$provisional_ownership  = ($result['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($result['ownership_type'] == 'Lease'){
							$html .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($result['ownership_type'] == 'Sub Lease'){
								
							$html .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html .= '</td>';

					$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							
						if($result['ownership_type'] == 'Lease'){
							$html .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($result['ownership_type'] == 'Sub Lease'){
								
							$html .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($result['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($result['end_date_of_ownership'])) : "";
							$html .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html .= '</td>';
					


					$html .= '<td class="r_'.$i.'" style="text-align: right;">';  
						$html .= '<span>'.$result['owner_percentage'].'%'.'</span>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_percentage]" value="'.$result['owner_percentage'].'" id="owner_percentage_'.$i.'"  />';
					$html .= '</td>';

					
					$html .= '<td class="r_'.$i.'" style="text-align: center;">';
					if($result['owner_color'] == 'Yes' && $last_color == $result['horse_to_owner_id']){
						$html .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'" checked="checked"  />';
					} else {
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'"  />';
					} 
					$html .= '</td>';
					$contengency = ($result['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($result['cont_percentage'] != 0) ? $result['cont_percentage'] : "";
					$cont_amount = ($result['cont_amount'] != 0) ? $result['cont_amount'] : "0";
					$win_gross_stake = ($result['win_gross_stake'] == 1) ? "Yes" : "N/A";
					$win_net_stake = ($result['win_net_stake'] == 1) ? "Yes" : "N/A";
					$cont_place = ($result['cont_place'] == 1) ? "Yes" : "N/A";
					$millionover = ($result['millionover'] == 1) ? "Yes" : "N/A";
					$millionover_amt = ($result['millionover_amt'] != 0) ? $result['millionover_amt'] : "0";
					$grade1 = ($result['grade1'] == 1) ? "Yes" : "N/A";
					$grade2 = ($result['grade2'] == 1) ? "Yes" : "N/A";
					$grade3 = ($result['grade3'] == 1) ? "Yes" : "N/A";

					$styles = 'text-align: center;';
					$html .= '<td class="r_'.$i.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$result['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
							$styles = 'text-align: center;display: flex;justify-content: center;';
						}
					$html .= '</td>';

					if($parent_data){
						if($parent_data['to_owner'] != '' && $result['ownership_type'] != 'Sale'){
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								if($result['ownership_type'] == 'Lease'){
									$html .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else if($result['ownership_type'] == 'Sub Lease'){
									$html .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Sub-Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';
								} else {
									$html .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html .= '<input type="hidden" name="owner_datas['.$i.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$i.'"  />';
							$html .= '</td>';
						}else {
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span> </span>';
								$html .= '<input type="hidden" name="owner_datas['.$i.'][parent_to_owner]" value="0" id="parent_to_owner_'.$i.'"  />';
							$html .= '</td>';
						}
					} else {
						$html .= '<td class="r_'.$i.'" style="text-align: left;">';
						$html .= '</td>';
					}

					$html .= '<td class="r_'.$i.'" style="'.$styles.'">';
						if($contengency == 'Yes'){
							$html .= '<a  class="btn btn-warning rmcontigencey"  id="'.$result['horse_to_owner_id'].'" style = "background-color: #008a2b; border-color: #008a2b;">Contingency Cancel</a>&nbsp;&nbsp;';
						}
						if($result['ownership_type'] == 'Lease'){
							$html .= '<input type="button"  value="'.$result['ownership_type'].' Cancel'.'"  class="checkbox_value ent-evnt btn btn-warning" id="fro_ow_ckper_'.$i.'" style = "background-color: #f33333; border-color: #f33333;color: white;"/>';
							/*$html .= '<a onclick="updateLease("'.$result['horse_to_owner_id'].'","'.$result['ownership_type'].'" );" class="btn btn-warning" style = "background-color: #f33333; border-color: #f33333;color: white;">'.$result['ownership_type'].' Cancel'.' Cancel</a>';*/
							$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][ownership_types]" value="'.$result['ownership_type'].'"  id="own_type_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][batch_ids]" value="'.$result['entry_count'].'"  id="batch_ids_'.$i.'"  />';
						} 
						else if($result['ownership_type'] == 'Sub Lease'){
							/*$html .= '<a onclick="updateLease("'.$result['horse_to_owner_id'].'","'.$result['ownership_type'].'" );" class="btn btn-warning" style = "background-color: #f33333; border-color: #f33333;color: white;">'.$result['ownership_type'].' Cancel'.' Cancel</a>';*/
						$html .= '<input type="button"  value="'.$result['ownership_type'].' Cancel'.'"  class="checkbox_value ent-evnt btn btn-warning" id="fro_ow_ckper_'.$i.'" style = "background-color: #f33333; border-color: #f33333;color: white;"/>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][ownership_types]" value="'.$result['ownership_type'].'"  id="own_type_'.$i.'"  />';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][batch_ids]" value="'.$result['entry_count'].'"  id="batch_ids_'.$i.'"  />';
						} 
					$html .= '</td>';
						
					
					$i++;
				}
				$html .= '</tbody>';
				$html .= '</table>';
				$json['new_user'] = 0;
			} else {

				$json['new_user'] = 1;
			}
		}
		// echo'<pre>';
		// print_r($json);
		 //exit;
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getOwnersParent(){
		
		$html = '';
		$json = array();
		if (isset($this->request->get['filter_hourse_id']) && isset($this->request->get['ownership_type_id'])) {
			$owners_datas_sql = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$this->request->get['ownership_type_id']."'");
			// echo'<pre>';
			// print_r($owners_datas_sql);
			// exit;
			if($owners_datas_sql->num_rows > 0){
				
				// $lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$this->request->get['ownership_type_id']."'");
				// if($lease_table_data->num_rows > 0){
					$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
					if($last_colorss->num_rows > 0){
						$last_color = $last_colorss->row['horse_to_owner_id'];
					} else {
						$last_color = 0;
					}
					$i=1;
					$parent_data =array();
					//if($lease_table_data->num_rows > 0){
						$html .= '<table class="table table-bordered ownertable">';
						$html .= '<thead>';
						$html .= '<tr>';
							$html .= '<td style="text-align: center;vertical-align: middle;">Name</td>';
							$html .= '<td style="text-align: center;vertical-align: middle;">Type <br> (O / L / SL)</td>';
							$html .= '<td style="text-align: center;vertical-align: middle;">Period</td>';
							$html .= '<td style="width: 160px;text-align: center;vertical-align: middle;">Share</td>';
							$html .= '<td style="width: 100px;text-align: center;vertical-align: middle;">Color</td>';
						$html .= '</tr>';
						$html .= '</thead>';
						$html .= '<tbody>';
						//foreach($lease_table_data->rows as $lkey => $lvalue){
							//$parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE parent_trans_id ='".$lvalue['parent_trans_id']."' GROUP BY `parent_trans_id` ");
							$parent_data = $this->db->query("SELECT grandpa_id, owner_percentage,contengency FROM `horse_to_owner` WHERE horse_to_owner_id ='".$this->request->get['ownership_type_id']."' ");
							
							if($parent_data->num_rows > 0){
								$json['containgency_status'] = $parent_data->row['contengency'];
									$hourse_to_owner_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$parent_data->row['grandpa_id']."'");
									//echo "SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$pvalue['parent_trans_id']."'";
									if($hourse_to_owner_data->num_rows > 0){
										$result = $hourse_to_owner_data->row;
										$from_date = date('d-m-Y', strtotime($result['date_of_ownership']));
										$to_date = date('d-m-Y', strtotime($result['end_date_of_ownership']));
										$html .= '<tr>';
										//-----------------ownership name-------------------
										$html .= '<td class="r_'.$i.'" style="text-align: left;">'; 
											$html .= '<span >'.$result['to_owner'].'</span><br>';
										$html .= '</td>';
										//-----------------ownership type-------------------
										$html .= '<td class="r_'.$i.'" style="text-align: center;">';  
											$provisional_ownership  = ($result['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
												
											if($result['ownership_type'] == 'Lease'){
												$html .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
											} elseif($result['ownership_type'] == 'Sub Lease'){
													
												$html .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
											} else {
												$html .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
											}
										$html .= '</td>';
										//-----------------ownership period-------------------

										$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
											
										if($result['ownership_type'] == 'Lease'){
											$html .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
										} elseif($result['ownership_type'] == 'Sub Lease'){
												
											$html .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
										} else {
											$end_date = ($result['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($result['end_date_of_ownership'])) : "";
											$html .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
										}
										$html .= '</td>';
										//-----------------ownership percentage-------------------

										$html .= '<td class="r_'.$i.'" style="text-align: right;">';  
											$html .= '<span>'.$parent_data->row['owner_percentage'].'%'.'</span>';
											$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_percentage]" value="'.$parent_data->row['owner_percentage'].'" id="owner_percentage_'.$i.'"  />';
										$html .= '</td>';
										//-----------------ownership color-------------------
										$html .= '<td class="r_'.$i.'" style="text-align: center;">';
											if($result['owner_color'] == 'Yes' && $last_color == $result['horse_to_owner_id']){
												$html .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
												$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'" checked="checked"  />';
											} else {
												$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'"  />';
											} 
										$html .= '</td>';
										$i++;
										$html .= '</tr>';
										
									}
								
							}
						//}
						$html .= '</tbody>';
						$html .= '</table>';
					//}
				//}  
			}
		}
		//echo "<pre>";print_r($html);exit;
		$json['html'] = $html;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function updateown() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit();
		*/
		$json['status'] = 0;
		$json = array();
		if (isset($this->request->get['filter_hourse_id']) && isset($this->request->get['ownership_type_id'])) {
			$json['status'] = 1;
			
			if(isset($this->request->get['from_contigencey']) && $this->request->get['from_contigencey'] == 1){
				$this->db->query("UPDATE `horse_to_owner` SET contengency = '0',cont_percentage = 0,cont_amount=0,win_gross_stake=0,win_net_stake = 0,cont_place =0,millionover=0,millionover_amt =0,grade1 =0,grade2 = 0,grade3 = 0 ,cancel_contigencey = 'Cancel Contingency' WHERE horse_to_owner_id = '".$this->request->get['ownership_type_id']."' ");
				$this->log->write("UPDATE `horse_to_owner` SET contengency = '0',cont_percentage = 0,cont_amount=0,win_gross_stake=0,win_net_stake = 0,cont_place =0,millionover=0,millionover_amt =0,grade1 =0,grade2 = 0,grade3 = 0 ,cancel_contigencey = 'Cancel Contingency' WHERE horse_to_owner_id = '".$this->request->get['ownership_type_id']."' ");
			}
			$this->load->model('transaction/cancel_ownership');
			$filter_data = array(
				'horse_id' => $this->request->get['filter_hourse_id'],
			);

			if(isset($this->request->get['from_contigencey']) && $this->request->get['from_contigencey'] == 0){

				$sub_owners = $this->db->query("SELECT grandpa_id, owner_percentage, entry_count FROM `horse_to_owner` WHERE horse_to_owner_id = '".$this->request->get['ownership_type_id']."'");
				if($sub_owners->num_rows > 0){
					$owner_per= $sub_owners->row['owner_percentage'];
					$this->db->query("UPDATE `horse_to_owner` SET owner_share_status = 0 ,cancel_lease_status = 'Cancel' WHERE horse_to_owner_id = '".$this->request->get['ownership_type_id']."' AND horse_id = '".$this->request->get['filter_hourse_id']."' ");
					
					$grand_parent_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id = '".$sub_owners->row['grandpa_id']."'")->row;

					$pre_own_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE to_owner_id = '".$grand_parent_datas['to_owner_id']."' AND owner_share_status = 1 AND  horse_id = '".$this->request->get['filter_hourse_id']."' AND owner_percentage < 100 ");
					if($pre_own_datas->num_rows > 0){
						$this->db->query("UPDATE `horse_to_owner` SET owner_share_status = 0 WHERE horse_to_owner_id = '".$pre_own_datas->row['horse_to_owner_id']."' AND horse_id = '".$this->request->get['filter_hourse_id']."' ");
						$owner_per = $pre_own_datas->row['owner_percentage'] + $sub_owners->row['owner_percentage'];
					}
					$entry_count = $sub_owners->row['entry_count'] + 1;

					$this->db->query("INSERT INTO `horse_to_owner` SET  
						`horse_id` = '" . $this->db->escape($grand_parent_datas['horse_id']) . "',
						`horse_group_id` = '" . $this->db->escape($grand_parent_datas['horse_group_id']) . "',
						`trainer_id` = '" . $this->db->escape($grand_parent_datas['trainer_id']) . "',
						`ownership_type` = '".$this->db->escape($grand_parent_datas['ownership_type'])."',
						`parent_group_id` = '".$grand_parent_datas['horse_group_id']."',
						`date_of_ownership` = '" . $this->db->escape($grand_parent_datas['date_of_ownership']) . "',
						`end_date_of_ownership` = '" . $this->db->escape($grand_parent_datas['end_date_of_ownership']) . "',
						`remark_horse_to_owner` = '" . $this->db->escape($grand_parent_datas['remark_horse_to_owner']) . "',
						`owner_percentage` = '" . $this->db->escape($owner_per) . "',
						`to_owner` = '" . $this->db->escape($grand_parent_datas['to_owner']) . "',
						`to_owner_id` = '" . $this->db->escape($grand_parent_datas['to_owner_id']) . "',
						`contengency` = '" . $this->db->escape($grand_parent_datas['contengency']) . "',
						`cont_percentage` = '" . $this->db->escape($grand_parent_datas['cont_percentage']) . "',
						`cont_amount` = '" . $this->db->escape($grand_parent_datas['cont_amount']) . "',
						`cont_place` = '" . $this->db->escape($grand_parent_datas['cont_place']) . "',
						`win_gross_stake` = '" . $this->db->escape($grand_parent_datas['win_gross_stake']) . "',
						`win_net_stake` = '" . $this->db->escape($grand_parent_datas['win_net_stake']) . "',
						`millionover` = '" . $this->db->escape($grand_parent_datas['millionover']) . "',
						`millionover_amt` = '" . $this->db->escape($grand_parent_datas['millionover_amt']) . "',
						`grade1` = '" . $this->db->escape($grand_parent_datas['grade1']) . "',
						`grade2` = '" . $this->db->escape($grand_parent_datas['grade2']) . "',
						`grade3` = '" . $this->db->escape($grand_parent_datas['grade3']) . "',
						`owner_share_status` = '1',
						`entry_count` = '".$entry_count."',
						`grandpa_id` = '".$sub_owners->row['grandpa_id']."',
						`provisional_ownership` = '".$grand_parent_datas['provisional_ownership']."'
					");
				}

				$history_owners = $this->db->query("SELECT `batch_id` FROM `horse_to_owner_history` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' ORDER BY id DESC LIMIT 1 ");
				$batch_id = 1;
				if($history_owners->num_rows > 0){
					$batch_id = $history_owners->row['batch_id'] + 1;
					$this->db->query("UPDATE `horse_to_owner_history` SET status = 1 WHERE batch_id = '".$history_owners->row['batch_id']."' ");
				}

				$allCurrOwners = $this->db->query("SELECT horse_id, horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' AND owner_share_status = 1 ");
				if($allCurrOwners->num_rows > 0){
					foreach ($allCurrOwners->rows as $ohkey => $ohvalue) {
						$this->db->query("INSERT INTO `horse_to_owner_history` SET 
							`horse_id` = '" . $ohvalue['horse_id'] . "',
							`batch_id` = '" . $batch_id . "',
							`horse_to_owner_id` = '" . $ohvalue['horse_to_owner_id'] . "',
							`status` = 0
							");
					}
				}
			} 
			$results = $this->model_transaction_cancel_ownership->gethorsetoOwner($filter_data);

			//echo "<pre>";print_r($results);exit;
			$html = '';
			
			if($results){
				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['filter_hourse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$last_color = $last_colorss->row['horse_to_owner_id'];
				} else {
					$last_color = 0;
				}

				$html .= '<table class="table table-bordered ownertable">';
				$html .= '<thead>';
				$html .= '<tr>';
					
					$html .= '<td style="text-align: center;vertical-align: middle;">Name</td>';
					$html .= '<td style="text-align: center;vertical-align: middle;">Type <br> (O / L / SL)</td>';
					$html .= '<td style="text-align: center;vertical-align: middle;">Period</td>';

					$html .= '<td style="width: 160px;text-align: center;vertical-align: middle;">Share</td>';
					
					$html .= '<td style="width: 100px;text-align: center;vertical-align: middle;">Color</td>';
					$html .= '<td style="width: 200px;text-align: center;vertical-align: middle;">Contingency</td>';
					$html .= '<td style="width: 300px;text-align: center;vertical-align: middle;">Ownership</td>';
					$html .= '<td style="width: 300px;text-align: center;vertical-align: middle;">Cancel Lease</td>';
					
				$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';

				$i=1;
				$parent_data =array();
				$sub_parent_data = array();
				foreach ($results as $result) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$result['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
						
						$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
						if($lease_parent_data->num_rows > 0){
							$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
						}

					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$result['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html .= '<tr>';
					


					$from_date = date('d-m-Y', strtotime($result['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($result['end_date_of_ownership']));

					$html .= '<td class="r_'.$i.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html .= '<span style="cursor:pointer"><a id="partners_'.$result['to_owner_id'].'"  class="partners" >'.$result['to_owner'].'</a></span><br>';
						}else {
							$html .= '<span >'.$result['to_owner'].'</span><br>';
						}
							$html .= '<input type="hidden" name="owner_datas['.$i.'][from_owner_hidden]" value="'.$result['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$i.'"  />';
					$html .= '</td>';

					$html .= '<td class="r_'.$i.'" style="text-align: center;">';  
						$provisional_ownership  = ($result['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($result['ownership_type'] == 'Lease'){
							$html .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($result['ownership_type'] == 'Sub Lease'){
								
							$html .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html .= '</td>';

					$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							
						if($result['ownership_type'] == 'Lease'){
							$html .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($result['ownership_type'] == 'Sub Lease'){
								
							$html .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$end_date = ($result['end_date_of_ownership'] != '0000-00-00') ? ' - '.date('Y-m-d', strtotime($result['end_date_of_ownership'])) : "";
							$html .= '<span style="font-size:12px;"> '.$from_date.''.$end_date.'</span>';
						}
					$html .= '</td>';
					


					$html .= '<td class="r_'.$i.'" style="text-align: right;">';  
						$html .= '<span>'.$result['owner_percentage'].'%'.'</span>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_percentage]" value="'.$result['owner_percentage'].'" id="owner_percentage_'.$i.'"  />';
					$html .= '</td>';

					
					$html .= '<td class="r_'.$i.'" style="text-align: center;">';
					if($result['owner_color'] == 'Yes' && $last_color == $result['horse_to_owner_id']){
						$html .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'" checked="checked"  />';
					} else {
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'"  />';
					} 
					$html .= '</td>';
					$contengency = ($result['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($result['cont_percentage'] != 0) ? $result['cont_percentage'] : "";
					$cont_amount = ($result['cont_amount'] != 0) ? $result['cont_amount'] : "0";
					$win_gross_stake = ($result['win_gross_stake'] == 1) ? "Yes" : "N/A";
					$win_net_stake = ($result['win_net_stake'] == 1) ? "Yes" : "N/A";
					$cont_place = ($result['cont_place'] == 1) ? "Yes" : "N/A";
					$millionover = ($result['millionover'] == 1) ? "Yes" : "N/A";
					$millionover_amt = ($result['millionover_amt'] != 0) ? $result['millionover_amt'] : "0";
					$grade1 = ($result['grade1'] == 1) ? "Yes" : "N/A";
					$grade2 = ($result['grade2'] == 1) ? "Yes" : "N/A";
					$grade3 = ($result['grade3'] == 1) ? "Yes" : "N/A";

					$styles = 'text-align: center;';
					$html .= '<td class="r_'.$i.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$result['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
							$styles = 'text-align: center;display: flex;justify-content: center;';
						}
					$html .= '</td>';

					if($parent_data){
						if($parent_data['to_owner'] != '' && $result['ownership_type'] != 'Sale'){
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								if($result['ownership_type'] == 'Lease'){
									$html .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else if($result['ownership_type'] == 'Sub Lease'){
									$html .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Sub-Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';
								} else {
									$html .= '<span>'.$sub_parent_data['to_owner'].' ( In the case of Lease )</span> => <span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html .= '<input type="hidden" name="owner_datas['.$i.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$i.'"  />';
							$html .= '</td>';
						}else {
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span> </span>';
								$html .= '<input type="hidden" name="owner_datas['.$i.'][parent_to_owner]" value="0" id="parent_to_owner_'.$i.'"  />';
							$html .= '</td>';
						}
					} else {
						$html .= '<td class="r_'.$i.'" style="text-align: left;">';
						$html .= '</td>';
					}

					$html .= '<td class="r_'.$i.'" style="'.$styles.'">';
						if($contengency == 'Yes'){
							$html .= '<a  class="btn btn-warning rmcontigencey"  id="'.$result['horse_to_owner_id'].'" style = "background-color: #008a2b; border-color: #008a2b;">Contingency Cancel</a>&nbsp;&nbsp;';
						}
						if($result['ownership_type'] == 'Lease'){
							$html .= '<input type="button"  value="'.$result['ownership_type'].' Cancel'.'"  class="checkbox_value ent-evnt btn btn-warning" id="fro_ow_ckper_'.$i.'" style = "background-color: #f33333; border-color: #f33333;color: white;"/>';
							/*$html .= '<a onclick="updateLease("'.$result['horse_to_owner_id'].'","'.$result['ownership_type'].'" );" class="btn btn-warning" style = "background-color: #f33333; border-color: #f33333;color: white;">'.$result['ownership_type'].' Cancel'.' Cancel</a>';*/
							$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][ownership_types]" value="'.$result['ownership_type'].'"  id="own_type_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][batch_ids]" value="'.$result['entry_count'].'"  id="batch_ids_'.$i.'"  />';
						} 
						else if($result['ownership_type'] == 'Sub Lease'){
							/*$html .= '<a onclick="updateLease("'.$result['horse_to_owner_id'].'","'.$result['ownership_type'].'" );" class="btn btn-warning" style = "background-color: #f33333; border-color: #f33333;color: white;">'.$result['ownership_type'].' Cancel'.' Cancel</a>';*/
						$html .= '<input type="button"  value="'.$result['ownership_type'].' Cancel'.'"  class="checkbox_value ent-evnt btn btn-warning" id="fro_ow_ckper_'.$i.'" style = "background-color: #f33333; border-color: #f33333;color: white;"/>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][ownership_types]" value="'.$result['ownership_type'].'"  id="own_type_'.$i.'"  />';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][batch_ids]" value="'.$result['entry_count'].'"  id="batch_ids_'.$i.'"  />';
						} 
					$html .= '</td>';
						
					
					$i++;
				}
				$html .= '</tbody>';
				$html .= '</table>';
				$json['new_user'] = 0;
			} else {

				$json['new_user'] = 1;
			}
		}
		/*echo'<pre>';
		print_r($json);
		 exit;*/
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function getContaingencyData(){
		
		$html = '';
		$json = array();
		$containgency_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$this->request->get['horse_owner_id']."'");
		// echo'<pre>';
		// print_r($containgency_datas);
		// exit;
		if($containgency_datas->num_rows > 0){
			$horse_name = $this->db->query("SELECT official_name FROM `horse1` WHERE horseseq ='".$containgency_datas->row['horse_id']."'")->row;
			$containgency_data = $containgency_datas->row;
			$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$this->request->get['horse_owner_id']."'");
			if($lease_table_data->num_rows > 0){
				$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
			} else {
				$parent_data['to_owner'] = '';
			}
			$contengency = ($containgency_data['contengency'] == 1) ? "Yes" : "N/A";
			$cont_percentage = ($containgency_data['cont_percentage'] != 0) ? $containgency_data['cont_percentage'] : "";
			$cont_amount = ($containgency_data['cont_amount'] != 0) ? $containgency_data['cont_amount'] : "0";
			$win_gross_stake = ($containgency_data['win_gross_stake'] == 1) ? "Yes" : "N/A";
			$win_net_stake = ($containgency_data['win_net_stake'] == 1) ? "Yes" : "N/A";
			$cont_place = ($containgency_data['cont_place'] == 1) ? "Yes" : "N/A";
			$millionover = ($containgency_data['millionover'] == 1) ? "Yes" : "N/A";
			$millionover_amt = ($containgency_data['millionover_amt'] != 0) ? $containgency_data['millionover_amt'] : "0";
			$grade1 = ($containgency_data['grade1'] == 1) ? "Yes" : "N/A";
			$grade2 = ($containgency_data['grade2'] == 1) ? "Yes" : "N/A";
			$grade3 = ($containgency_data['grade3'] == 1) ? "Yes" : "N/A";

			$html .= '<div class="col-sm-10>';
				$html .= '<div class="row">';
				$html .= '<span style="font-size:16px;"><b>Name : </b> '.$parent_data['to_owner'].'</span><br><br>';
				$html .= '<span style="font-size:16px;"><b>Horse Name : </b> '.$horse_name['official_name'].'</span><br><br>';

					$html .= '<div class="col-sm-6">';
						$html .= '<span style="font-size: 14px;"> <b> Contingency :</b> '.$contengency .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Amount :</b> Rs.'.$cont_amount .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Win Net Stake :</b> '.$win_net_stake .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Millionover :</b> '.$millionover .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Grade 1 :</b> '.$grade1 .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Grade 3 :</b> '.$grade3 .'</span> <br>';
					$html .= '</div>';

					$html .= '<div class="col-sm-6">';
						$html .= '<span style="font-size: 14px;"> <b> Percentage :</b> '.$cont_percentage .'% </span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Win Gross Stake :</b> '.$win_gross_stake .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Place :</b> '.$cont_place .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Millionover Amt :</b> Rs.'.$millionover_amt .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Grade 2 :</b> '.$grade2 .'</span> <br>';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
		}
		$json = $html;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function CancelContigengetHorseSubLease($filter_hourse_id,$in) {
		if($in == 1){
			$check_horse = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_id = '".$filter_hourse_id."' AND contengency = 1  AND ownership_type = 'Sub Lease' ");
		} else if($in == 3){
			$check_horse = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_id = '".$filter_hourse_id."' AND contengency = 1  AND ownership_type = 'Lease' ");
		}
		 else {
			$check_horse = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id = '".$filter_hourse_id."' AND contengency = 1 ");
		}
		if($check_horse->num_rows > 0){
			foreach($check_horse->rows as $conkey => $convalue){
				$this->db->query("UPDATE `horse_to_owner` SET contengency = '0',cont_percentage = 0,cont_amount=0,win_gross_stake=0,win_net_stake = 0,cont_place =0,millionover=0,millionover_amt =0,grade1 =0,grade2 = 0,grade3 = 0 ,cancel_contigencey = 'Cancel Contingency' WHERE horse_to_owner_id = '".$convalue['horse_to_owner_id']."' ");
			}
		}
	}
}