<?php
require DIR_SYSTEM.'library/PhpSpreadsheet/vendor/autoload.php';
//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Controllerutilityimporttallypo extends Controller {
	private $error = array();

	public function index() {
		/*header("Content-Type: text/html; charset=ISO-8859-1");*/
		// $this->load->language('utility/import_tally_po');
		// $this->document->setTitle($this->language->get('heading_title'));
		// if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->user->hasPermission('modify', 'utility/import_tally_po')) {
		// 	if (is_uploaded_file($this->request->files['import']['tmp_name'])) {
		// 		$content = file_get_contents($this->request->files['import']['tmp_name']);
		// 	} else {
		// 		$content = false;
		// 	}
		// 	if ($content) {
		// 		$this->import_data($content);
		// 		$this->session->data['success'] = 'Data Imported Successfully';
		// 		$this->redirect($this->url->link('utility/import_tally_po', 'token=' . $this->session->data['token'], 'SSL'));
		// 	} else {
		// 		$this->error['warning'] = 'File Is Empty';
		// 	}
		// }

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');
		$data['text_list'] = "Import Tally PO";


		
		//echo "<pre>";print_r($this->error);exit;

		/*if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
*/
		if (isset($this->session->data['warning'])) {
			$data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['valierr_amount'])) {
			$data['valierr_amount'] = $this->error['valierr_amount'];
		}

		if (isset($this->error['valierr_letter_date'])) {
			$data['valierr_letter_date'] = $this->error['valierr_letter_date'];
		}

		if (isset($this->error['valierr_received_date'])) {
			$data['valierr_received_date'] = $this->error['valierr_received_date'];
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('utility/import_tally_po', 'token=' . $this->session->data['token'] . $url, true)
		);

		
		$data['action'] = $this->url->link('utility/import_tally_po/import_data', 'token=' . $this->session->data['token'] . $url, true);
		

		//echo "<pre>";print_r($this->request->post['trainer_info_history']);exit;
		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

	
		
		$data['path'] = '';

		$this->load->model('catalog/filter');

		if (isset($this->request->post['category_filter'])) {
			$filters = $this->request->post['category_filter'];
		} elseif (isset($this->request->get['category_id'])) {
		} else {
			$filters = array();
		}

		$data['category_filters'] = array();

		foreach ($filters as $filter_id) {
			$filter_info = $this->model_catalog_filter->getFilter($filter_id);

			if ($filter_info) {
				$data['category_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}

		$this->load->model('setting/store');
		$product_specials = array();
		$data['product_specials'] = $product_specials;

		$product_recurrings = array();
		$data['product_recurrings'] = $product_recurrings;

		$data['recurrings'] = array();
		$data['customer_groups'] = array();
		$data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['category_store'])) {
			$data['category_store'] = $this->request->post['category_store'];
		} elseif (isset($this->request->get['category_id'])) {
		} else {
			$data['category_store'] = array(0);
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($category_info)) {
			$data['keyword'] = $category_info['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($category_info)) {
			$data['image'] = $category_info['image'];
		} else {
			$data['image'] = '';
		}

		$data['cancel'] = $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($category_info) && is_file(DIR_IMAGE . $category_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($category_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['top'])) {
			$data['top'] = $this->request->post['top'];
		} elseif (!empty($category_info)) {
			$data['top'] = $category_info['top'];
		} else {
			$data['top'] = 0;
		}

		if (isset($this->request->post['column'])) {
			$data['column'] = $this->request->post['column'];
		} elseif (!empty($category_info)) {
			$data['column'] = $category_info['column'];
		} else {
			$data['column'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($category_info)) {
			$data['sort_order'] = $category_info['sort_order'];
		} else {
			$data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = true;
		}

		if (isset($this->request->post['category_layout'])) {
			$data['category_layout'] = $this->request->post['category_layout'];
		} elseif (isset($this->request->get['category_id'])) {
		} else {
			$data['category_layout'] = array();
		}



		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('utility/import_tally_po', $data));

	}
	public function import_data() {
		//echo "<pre>";print_r($this->session->data['user_id']);exit;
	    //echo "<pre>";print_r($_FILES);exit;
		if(isset($_FILES['import_data']['name']) && $_FILES['import_data']['name'] != "") {
	        $allowedExtensions = array("xls","xlsx");
	        $ext = pathinfo($_FILES['import_data']['name'], PATHINFO_EXTENSION);
	        if(in_array($ext, $allowedExtensions)) {
	           	$file_size = $_FILES['import_data']['size'] / 1024;
	           	if($file_size < 50) {
	               	if($ext == 'csv'){
	                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
	                } elseif($ext == 'xlsx') {
	                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
	                } else {
	                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
	                }
	                // file path
	                $spreadsheet = $reader->load($_FILES['import_data']['tmp_name']);
	                $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true);
	                // array Count
	                $arrayCount = count($allDataInSheet);
	                $flag = 0;
	                $createArray = 	array('Voucher No',
                 					'Voucher Type',
                 					'Date of Posting',
                 					'P O No', 
                 					'PI No',
                 					'PI Date', 
                 					'Supplier PO/Ref No',
                 					'Indent No',
                 					'Credit Ledger Name- Supplier Name',
                 					'Credit Ledger Code- Supplier Ledger Code',
                 					'Debit Ledger Name',
                 					'Debit Ledger Code',
                 					'Name of the Items', 
                 					'Qty', 
                 					'UOM', 
                 					'Rate', 
                 					'Amount', 
                 					'Godown Name', 
                 					'Narration',
                 					'Locations',
                 					'Divisions',
                 					'INPUT SGST (NOT ELIGIBLE FOR ITC)',
                 					'INPUT CGST (NOT ELIGIBLE FOR ITC)',
                 					'INPUT IGST (NOT ELIGIBLE FOR ITC)',
                 					'Total'
	                 				);
		            $makeArray = 	array('VoucherNo' => 'VoucherNo', 
	                				'VoucherType' => 'VoucherType',
	                				'DateofPosting' => 'DateofPosting',
	                				'PONo' => 'PONo',
	                				'PINo' => 'PINo',
	                				'PIDate' => 'PIDate',
	                				'SupplierPO/RefNo' => 'SupplierPO/RefNo',
	                				'IndentNo' => 'IndentNo',
	                				'CreditLedgerName-SupplierName' => 'CreditLedgerName-SupplierName',
	                				'CreditLedgerCode-SupplierLedgerCode' => 'CreditLedgerCode-SupplierLedgerCode',
	                				'DebitLedgerName' => 'DebitLedgerName',
	                				'DebitLedgerCode' => 'DebitLedgerCode',
	                				'NameoftheItems' => 'NameoftheItems',
	                				'Qty' => 'Qty',
	                				'UOM' => 'UOM',
	                				'Rate' => 'Rate',
	                				'Amount' => 'Amount',
	                				'GodownName' => 'GodownName',
	                				'Narration' => 'Narration',
	                				'Locations' => 'Locations',
	                				'Divisions' => 'Divisions',
	                				'INPUTSGST(NOTELIGIBLEFORITC)' => 'INPUTSGST(NOTELIGIBLEFORITC)',
	                				'INPUTCGST(NOTELIGIBLEFORITC)' => 'INPUTCGST(NOTELIGIBLEFORITC)',
	                				'INPUTIGST(NOTELIGIBLEFORITC)' => 'INPUTIGST(NOTELIGIBLEFORITC)',
	                				'Total' => 'Total',
		                			);
	                $SheetDataKey = array();
	                foreach ($allDataInSheet as $dataInSheet) {
	                    foreach ($dataInSheet as $key => $value) {
	                        if (in_array(trim($value), $createArray)) {
	                            $value = preg_replace('/\s+/', '', $value);
	                            $SheetDataKey[trim($value)] = $key;
	                        } 
	                    }
	                }
	                $dataDiff = array_diff_key($makeArray, $SheetDataKey);
	                //echo "<pre>";print_r($dataDiff);exit;
	                if (empty($dataDiff)) {
	                    $flag = 1;
	                }
	                if ($flag == 1) {
	                    $valid = 1;
	                    for ($i = 1; $i < $arrayCount; $i++) {
	                        $addresses = array();
	                        $voucher_no = $SheetDataKey['VoucherNo'];
	                        $voucher_no = filter_var(trim($allDataInSheet[$i][$voucher_no]), FILTER_SANITIZE_STRING);
	                        $voucher_type = $SheetDataKey['VoucherType'];
	                        $voucher_type = filter_var(trim($allDataInSheet[$i][$voucher_type]), FILTER_SANITIZE_STRING);
	                        $date_of_posting = $SheetDataKey['DateofPosting'];
	                        $date_of_posting = filter_var(trim($allDataInSheet[$i][$date_of_posting]), FILTER_SANITIZE_STRING);
	                        $po_no = $SheetDataKey['PONo'];
	                        $po_no = filter_var(trim($allDataInSheet[$i][$po_no]), FILTER_SANITIZE_STRING);
	                        $pi_no = $SheetDataKey['PINo'];
	                        $pi_no = filter_var(trim($allDataInSheet[$i][$pi_no]), FILTER_SANITIZE_STRING);
	                        $pi_date = $SheetDataKey['PIDate'];
	                        $pi_date = filter_var(trim($allDataInSheet[$i][$pi_date]), FILTER_SANITIZE_STRING);
	                        $supplier_po_ref_no = $SheetDataKey['SupplierPO/RefNo'];
	                        $supplier_po_ref_no = filter_var(trim($allDataInSheet[$i][$supplier_po_ref_no]), FILTER_SANITIZE_STRING);
	                        $indent_no = $SheetDataKey['IndentNo'];
	                        $indent_no = filter_var(trim($allDataInSheet[$i][$indent_no]), FILTER_SANITIZE_STRING);
	                        $credit_ledger_name_Supplier_name = $SheetDataKey['CreditLedgerName-SupplierName'];
	                        $credit_ledger_name_Supplier_name = filter_var(trim($allDataInSheet[$i][$credit_ledger_name_Supplier_name]), FILTER_SANITIZE_STRING);
	                        $credit_ledger_code_Supplier_ledger_code = $SheetDataKey['CreditLedgerCode-SupplierLedgerCode'];
	                        $credit_ledger_code_Supplier_ledger_code = filter_var(trim($allDataInSheet[$i][$credit_ledger_code_Supplier_ledger_code]), FILTER_SANITIZE_STRING);
	                        $debit_ledger_name = $SheetDataKey['DebitLedgerName'];
	                        $debit_ledger_name = filter_var(trim($allDataInSheet[$i][$debit_ledger_name]), FILTER_SANITIZE_STRING);
	                        $debit_ledger_code = $SheetDataKey['DebitLedgerCode'];
	                        $debit_ledger_code = filter_var(trim($allDataInSheet[$i][$debit_ledger_code]), FILTER_SANITIZE_STRING);
	                        $name_of_the_items = $SheetDataKey['NameoftheItems'];
	                        $name_of_the_items = filter_var(trim($allDataInSheet[$i][$name_of_the_items]), FILTER_SANITIZE_STRING);
	                        $Qty = $SheetDataKey['Qty'];
	                        $Qty = filter_var(trim($allDataInSheet[$i][$Qty]), FILTER_SANITIZE_STRING);
	                        $UOM = $SheetDataKey['UOM'];
	                        $UOM = filter_var(trim($allDataInSheet[$i][$UOM]), FILTER_SANITIZE_STRING);
	                        $Rate = $SheetDataKey['Rate'];
	                        $Rate = filter_var(trim($allDataInSheet[$i][$Rate]), FILTER_SANITIZE_STRING);
	                        $Amount = $SheetDataKey['Amount'];
	                        $Amount = filter_var(trim($allDataInSheet[$i][$Amount]), FILTER_SANITIZE_STRING);
	                        $godown_name = $SheetDataKey['GodownName'];
	                        $godown_name = filter_var(trim($allDataInSheet[$i][$godown_name]), FILTER_SANITIZE_STRING);
	                        $Narration = $SheetDataKey['Narration'];
	                        $Narration = filter_var(trim($allDataInSheet[$i][$Narration]), FILTER_SANITIZE_STRING);
	                        $Locations = $SheetDataKey['Locations'];
	                        $Locations = filter_var(trim($allDataInSheet[$i][$Locations]), FILTER_SANITIZE_STRING);
	                        $Divisions = $SheetDataKey['Divisions'];
	                        $Divisions = filter_var(trim($allDataInSheet[$i][$Divisions]), FILTER_SANITIZE_STRING);
	                        $INPUT_SGST_NOT_ELIGIBLE_FOR_ITC = $SheetDataKey['INPUTSGST(NOTELIGIBLEFORITC)'];
	                        $INPUT_SGST_NOT_ELIGIBLE_FOR_ITC = filter_var(trim($allDataInSheet[$i][$INPUT_SGST_NOT_ELIGIBLE_FOR_ITC]), FILTER_SANITIZE_STRING);
	                        $INPUT_CGST_NOT_ELIGIBLE_FOR_ITC = $SheetDataKey['INPUTCGST(NOTELIGIBLEFORITC)'];
	                        $INPUT_CGST_NOT_ELIGIBLE_FOR_ITC = filter_var(trim($allDataInSheet[$i][$INPUT_CGST_NOT_ELIGIBLE_FOR_ITC]), FILTER_SANITIZE_STRING);
	                        $INPUT_IGST_NOT_ELIGIBLE_FOR_ITC = $SheetDataKey['INPUTIGST(NOTELIGIBLEFORITC)'];
	                        $INPUT_IGST_NOT_ELIGIBLE_FOR_ITC = filter_var(trim($allDataInSheet[$i][$INPUT_IGST_NOT_ELIGIBLE_FOR_ITC]), FILTER_SANITIZE_STRING);
	                        $Total = $SheetDataKey['Total'];
	                        $Total = filter_var(trim($allDataInSheet[$i][$Total]), FILTER_SANITIZE_STRING);

	                        $date_of_posting1 = date('Y-m-d', strtotime($date_of_posting));

	                        $user_id = $this->session->data['user_id'];

	                        $current_date = date("Y-m-d");

	                   		$pi_date1 = date('Y-m-d', strtotime($pi_date));

	                   		$po_num = $this->db->query("SELECT * FROM oc_tally_po WHERE po_no='".$po_no."'");
	                   		
	                   		if ($po_num->num_rows > 0) {
	                   			$is_po_no = 1;
	                   		} else {
	                   			$is_po_no = 0;
	                   		}

	                   		if ($voucher_no != '' && $is_po_no == 0) {
	                   			$this->db->query("INSERT INTO `oc_tally_po` SET voucher_no = '".$voucher_no."',
		       					voucher_type = '".$voucher_type."',
		       					date_of_posting = '".$date_of_posting1 ."',
		       					po_no  = '".$po_no."'  , 
		       					pi_no = '".$pi_no."' ,
		       					pi_date = '".$pi_date1."',
		       					supplier_po = '".$supplier_po_ref_no."',
		       					indent_no = '".$indent_no."',
								cln_supplier_name = '".$credit_ledger_name_Supplier_name."',
								clc_supplier_ledger_code = '".$credit_ledger_code_Supplier_ledger_code."',
								debit_ledger_name = '".$debit_ledger_name."',
								debit_ledger_code = '".$debit_ledger_code."',
								name_of_the_items = '".$name_of_the_items."',
								qty = '".$Qty."',
								uom = '".$UOM."',
								rate = '".$Rate."',
								amount = '".$Amount."',
								godown_name = '".$godown_name."',
								narration = '".$Narration."',
								locations = '".$Locations."',
								divisions = '".$Divisions."',
								input_sgst_not_eligible_for_itc = '".$INPUT_SGST_NOT_ELIGIBLE_FOR_ITC."',
								input_cgst_not_eligible_for_itc = '".$INPUT_CGST_NOT_ELIGIBLE_FOR_ITC."',
								input_igst_not_eligible_for_itc = '".$INPUT_IGST_NOT_ELIGIBLE_FOR_ITC."',
								total = '".$Total."',
								date_added = '".$current_date."',
								user_id = '".$user_id."'
								");
	                   		}

								// $this->log->write("UPDATE " . DB_PREFIX . "participant SET `bip_no` = '".$this->db->escape($bibnumber)."' WHERE `participant_id` = '".$this->db->escape($participant_id)."' ");
								// $this->db->query("UPDATE " . DB_PREFIX . "participant SET `bip_no` = '".$this->db->escape($bibnumber)."' WHERE `participant_id` = '".$this->db->escape($participant_id)."' ");
						
	                    }   
	                } else {
	                	$valid = 0;
	                    $this->session->data['warning'] = 'Please import correct file, did not match excel sheet column';
	                }
	            } else {
	                $valid = 0;
	                $this->session->data['warning'] = '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
	            }
	        } else {
	            $valid = 0;
	            $this->session->data['warning'] = '<span class="msg">This type of file not allowed!</span>';
	        }
	    } else {
	        $valid = 0;
	        $this->session->data['warning'] = '<span class="msg">Select an excel file first!</span>';
	    }
	    if($valid == 1){
	    	$url = $this->url->link('utility/import_tally_po', 'token=' . $this->session->data['token'].$url1, true);
			$url = str_replace('&amp;', '&', $url);
			$this->session->data['success'] = "Success: You have Imported Successfully!";
			$this->response->redirect($url);
	    } else {
	    	$url = $this->url->link('utility/import_tally_po', 'token=' . $this->session->data['token'].$url1, true);
			$url = str_replace('&amp;', '&', $url);
			$this->response->redirect($url);
	    }
	}
	

	

	protected function validateForm() {
		$this->error = array();
		if (!$this->user->hasPermission('modify', 'utility/import_tally_po')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		//echo '<pre>';print_r($this->request->post);exit;
		if($this->request->post['amount'] == ''){
			$this->error['valierr_amount'] = 'Please Enter Amount!';
		}


		if($this->request->post['letter_date'] == ''){
			$this->error['valierr_letter_date'] = 'Please Select Date!';
		}

		if($this->request->post['received_date'] == ''){
			$this->error['valierr_received_date'] = 'Please Select Date!';
		}

		

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		echo '<pre>';
		print_r($this->error['warning']);exit;
		return !$this->error;
		//exit;
	}

	
}
