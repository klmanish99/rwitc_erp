<?php
class Controllerutilityimporthorse extends Controller {
	private $error = array();

	public function index() {
		header("Content-Type: text/html; charset=ISO-8859-1");
		$this->load->language('utility/import_horse');
		$this->document->setTitle($this->language->get('heading_title'));
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->user->hasPermission('modify', 'utility/import_horse')) {
			if (is_uploaded_file($this->request->files['import']['tmp_name'])) {
				$content = file_get_contents($this->request->files['import']['tmp_name']);
			} else {
				$content = false;
			}
			if ($content) {
				$this->import_data($content);
				$this->session->data['success'] = 'Data Imported Successfully';
				$this->redirect($this->url->link('utility/import_horse', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->error['warning'] = 'File Is Empty';
			}
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');
		$data['text_list'] = $this->language->get('text_list');


		
		//echo "<pre>";print_r($this->error);exit;

		/*if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}*/

		if (isset($this->session->data['warning'])) {
			$data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['valierr_amount'])) {
			$data['valierr_amount'] = $this->error['valierr_amount'];
		}

		if (isset($this->error['valierr_letter_date'])) {
			$data['valierr_letter_date'] = $this->error['valierr_letter_date'];
		}

		if (isset($this->error['valierr_received_date'])) {
			$data['valierr_received_date'] = $this->error['valierr_received_date'];
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('utility/import_horse', 'token=' . $this->session->data['token'] . $url, true)
		);

		
		$data['action'] = $this->url->link('utility/import_horse', 'token=' . $this->session->data['token'] . $url, true);
		

		//echo "<pre>";print_r($this->request->post['trainer_info_history']);exit;
		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

	
		
		$data['path'] = '';

		$this->load->model('catalog/filter');

		if (isset($this->request->post['category_filter'])) {
			$filters = $this->request->post['category_filter'];
		} elseif (isset($this->request->get['category_id'])) {
		} else {
			$filters = array();
		}

		$data['category_filters'] = array();

		foreach ($filters as $filter_id) {
			$filter_info = $this->model_catalog_filter->getFilter($filter_id);

			if ($filter_info) {
				$data['category_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}

		$this->load->model('setting/store');
		$product_specials = array();
		$data['product_specials'] = $product_specials;

		$product_recurrings = array();
		$data['product_recurrings'] = $product_recurrings;

		$data['recurrings'] = array();
		$data['customer_groups'] = array();
		$data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['category_store'])) {
			$data['category_store'] = $this->request->post['category_store'];
		} elseif (isset($this->request->get['category_id'])) {
		} else {
			$data['category_store'] = array(0);
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($category_info)) {
			$data['keyword'] = $category_info['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($category_info)) {
			$data['image'] = $category_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($category_info) && is_file(DIR_IMAGE . $category_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($category_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['top'])) {
			$data['top'] = $this->request->post['top'];
		} elseif (!empty($category_info)) {
			$data['top'] = $category_info['top'];
		} else {
			$data['top'] = 0;
		}

		if (isset($this->request->post['column'])) {
			$data['column'] = $this->request->post['column'];
		} elseif (!empty($category_info)) {
			$data['column'] = $category_info['column'];
		} else {
			$data['column'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($category_info)) {
			$data['sort_order'] = $category_info['sort_order'];
		} else {
			$data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = true;
		}

		if (isset($this->request->post['category_layout'])) {
			$data['category_layout'] = $this->request->post['category_layout'];
		} elseif (isset($this->request->get['category_id'])) {
		} else {
			$data['category_layout'] = array();
		}

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('utility/import_horse', $data));

	}

		public function import_data($content) {
		$valid = 0;

		 
		// exit;
		
		if(isset($this->request->files['import']['name'])){
			$file_name = $this->request->files['import']['name'];
			$file_name_exp = explode('.', $file_name);
			if(strtoupper($file_name_exp[1]) == 'DBF'){
				$valid = 1;
			} 
		}
	
		if(isset($this->request->files['import']['tmp_name']) && $this->request->files['import']['tmp_name'] != '' && $valid == 1){
			//$file=fopen($this->request->files['import']['tmp_name'],"r");

			

			 if (!file_exists(DIR_DOWNLOAD.'horse_dbf/'.$this->request->files['import']['name'])) {
                move_uploaded_file($this->request->files['import']['tmp_name'], DIR_DOWNLOAD.'horse_dbf/'.$this->request->files['import']['name']);
             }

			$db = dbase_open(DIR_DOWNLOAD.'horse_dbf/'.$this->request->files['import']['name'], 0);
		
		

			if ($db) {
				$record_numbers = dbase_numrecords($db);
  	 			//echo $record_numbers;exit;
  	 			$start = 1;
			    for ($i = $start; $i <= $record_numbers; $i++) {
			   		if($i <= $record_numbers){
		       			$row = dbase_get_record_with_names($db, $i);
		       			 	/*echo "<pre>";
		       				print_r($row);
		       				exit;*/

		       				if(isset($row['SIRENAME']) && (trim($row['SIRENAME']) != '')) {
								$horse_name = $this->db->escape(trim($row['SIRENAME']));
							} else {
								$horse_name = '';
							}

							if(isset($row['HORSECOLOR']) && (trim($row['HORSECOLOR']) != '')) {
								$horse_color = $this->db->escape(trim($row['HORSECOLOR']));
							} else {
								$horse_color = '';
							}

							if(isset($row['HORSESEX']) && (trim($row['HORSESEX']) != '')) {
								$sex = $this->db->escape(trim($row['HORSESEX']));
							} else {
								$sex = '';
							}

							if(isset($row['MICROCHIP1']) && (trim($row['MICROCHIP1']) != '')) {
								$chip_no1 = $this->db->escape(trim($row['MICROCHIP1']));
							} else {
								$chip_no1 = '';
							}

							if(isset($row['MICROCHIP2']) && (trim($row['MICROCHIP2']) != '')) {
								$chip_no2 = $this->db->escape(trim($row['MICROCHIP2']));
							} else {
								$chip_no2 = '';
							}

							if(isset($row['FOALDATE']) && (trim($row['FOALDATE']) != '')) {
								$date_foal_date = date('Y-m-d', strtotime($row['FOALDATE']));
							} else {
								$date_foal_date = '';
							}

							if(isset($row['YROFFLNG']) && (trim($row['YROFFLNG']) != '')) {
								$YROFFLNG = $this->db->escape(trim($row['YROFFLNG']));
							} else {
								$YROFFLNG = 0;
							}

							if(isset($row['SIRENAT']) && (trim($row['SIRENAT']) != '')) {
								$SIRENAT = $this->db->escape(trim($row['SIRENAT']));
							} else {
								$SIRENAT = '';
							}

							if(isset($row['MARENAME']) && (trim($row['MARENAME']) != '')) {
								$MARENAME = $this->db->escape(trim($row['MARENAME']));
							} else {
								$MARENAME = '';
							}

							if(isset($row['MARENAT']) && (trim($row['MARENAT']) != '')) {
								$MARENAT = $this->db->escape(trim($row['MARENAT']));
							} else {
								$MARENAT = '';
							}

							if(isset($row['FOALNAME']) && (trim($row['FOALNAME']) != '')) {
								$FOALNAME = $this->db->escape(trim($row['FOALNAME']));
							} else {
								$FOALNAME = '';
							}

							if(isset($row['MNTHDATE']) && (trim($row['MNTHDATE']) != '')) {
								$MNTHDATE = $this->db->escape(trim($row['MNTHDATE']));
							} else {
								$MNTHDATE = '';
							}

							if(isset($row['STUDNAME']) && (trim($row['STUDNAME']) != '')) {
								$STUDNAME = $this->db->escape(trim($row['STUDNAME']));
							} else {
								$STUDNAME = '';
							}

							if(isset($row['BRAND1']) && (trim($row['BRAND1']) != '')) {
								$BRAND1 = $this->db->escape(trim($row['BRAND1']));
							} else {
								$BRAND1 = '';
							}

							if(isset($row['BRAND2']) && (trim($row['BRAND2']) != '')) {
								$BRAND2 = $this->db->escape(trim($row['BRAND2']));
							} else {
								$BRAND2 = '';
							}

							if(isset($row['PASSP_NO']) && (trim($row['PASSP_NO']) != '')) {
								$PASSP_NO = $this->db->escape(trim($row['PASSP_NO']));
							} else {
								$PASSP_NO = '';
							}

							if(isset($row['LIFENUMBER']) && (trim($row['LIFENUMBER']) != '')) {
								$LIFENUMBER = $this->db->escape(trim($row['LIFENUMBER']));
							} else {
								$LIFENUMBER = '';
							}

							if(isset($row['D_BRAND1']) && (trim($row['D_BRAND1']) != '')) {
								$D_BRAND1 = $this->db->escape(trim($row['D_BRAND1']));
							} else {
								$D_BRAND1 = '';
							}

							if(isset($row['D_BRAND2']) && (trim($row['D_BRAND2']) != '')) {
								$D_BRAND2 = $this->db->escape(trim($row['D_BRAND2']));
							} else {
								$D_BRAND2 = '';
							}

							if(isset($row['BR1']) && (trim($row['BR1']) != '')) {
								$BR1 = $this->db->escape(trim($row['BR1']));
							} else {
								$BR1 = '';
							}

							if(isset($row['BR2']) && (trim($row['BR2']) != '')) {
								$BR2 = $this->db->escape(trim($row['BR2']));
							} else {
								$BR2 = '';
							}

							if(isset($row['BR3']) && (trim($row['BR3']) != '')) {
								$BR3 = $this->db->escape(trim($row['BR3']));
							} else {
								$BR3 = '';
							}

							if(isset($row['BR4']) && (trim($row['BR4']) != '')) {
								$BR4 = $this->db->escape(trim($row['BR4']));
							} else {
								$BR4 = '';
							}

							if(isset($row['BR5']) && (trim($row['BR5']) != '')) {
								$BR5 = $this->db->escape(trim($row['BR5']));
							} else {
								$BR5 = '';
							}

							if(isset($row['BR6']) && (trim($row['BR6']) != '')) {
								$BR6 = $this->db->escape(trim($row['BR6']));
							} else {
								$BR6 = '';
							}

							if(isset($row['BR7']) && (trim($row['BR7']) != '')) {
								$BR7 = $this->db->escape(trim($row['BR7']));
							} else {
								$BR7 = '';
							}

							if(isset($row['BR8']) && (trim($row['BR8']) != '')) {
								$BR8 = $this->db->escape(trim($row['BR8']));
							} else {
								$BR8 = '';
							}

							if(isset($row['BR9']) && (trim($row['BR9']) != '')) {
								$BR9 = $this->db->escape(trim($row['BR9']));
							} else {
								$BR9 = '';
							}

							if(isset($row['BR10']) && (trim($row['BR10']) != '')) {
								$BR10 = $this->db->escape(trim($row['BR10']));
							} else {
								$BR10 = '';
							}

							if(isset($row['ON1']) && (trim($row['ON1']) != '')) {
								$ON1 = $this->db->escape(trim($row['ON1']));
							} else {
								$ON1 = '';
							}

							if(isset($row['ON2']) && (trim($row['ON2']) != '')) {
								$ON2 = $this->db->escape(trim($row['ON2']));
							} else {
								$ON2 = '';
							}

							if(isset($row['ON3']) && (trim($row['ON3']) != '')) {
								$ON3 = $this->db->escape(trim($row['ON3']));
							} else {
								$ON3 = '';
							}

							if(isset($row['ON4']) && (trim($row['ON4']) != '')) {
								$ON4 = $this->db->escape(trim($row['ON4']));
							} else {
								$ON4 = '';
							}

							if(isset($row['ON5']) && (trim($row['ON5']) != '')) {
								$ON5 = $this->db->escape(trim($row['ON5']));
							} else {
								$ON5 = '';
							}

							if(isset($row['ON6']) && (trim($row['ON6']) != '')) {
								$ON6 = $this->db->escape(trim($row['ON6']));
							} else {
								$ON6 = '';
							}

							if(isset($row['ON7']) && (trim($row['ON7']) != '')) {
								$ON7 = $this->db->escape(trim($row['ON7']));
							} else {
								$ON7 = '';
							}

							if(isset($row['ON8']) && (trim($row['ON8']) != '')) {
								$ON8 = $this->db->escape(trim($row['ON8']));
							} else {
								$ON8 = '';
							}

							if(isset($row['ON9']) && (trim($row['ON9']) != '')) {
								$ON9 = $this->db->escape(trim($row['ON9']));
							} else {
								$ON9 = '';
							}

							if(isset($row['ON10']) && (trim($row['ON10']) != '')) {
								$ON10 = $this->db->escape(trim($row['ON10']));
							} else {
								$ON10 = '';
							}

							if(isset($row['BSHR1']) && (trim($row['BSHR1']) != '')) {
								$BSHR1 = $this->db->escape(trim($row['BSHR1']));
							} else {
								$BSHR1 = '';
							}

							if(isset($row['BSHR2']) && (trim($row['BSHR2']) != '')) {
								$BSHR2 = $this->db->escape(trim($row['BSHR2']));
							} else {
								$BSHR2 = '';
							}

							if(isset($row['BSHR3']) && (trim($row['BSHR3']) != '')) {
								$BSHR3 = $this->db->escape(trim($row['BSHR3']));
							} else {
								$BSHR3 = '';
							}

							if(isset($row['BSHR4']) && (trim($row['BSHR4']) != '')) {
								$BSHR4 = $this->db->escape(trim($row['BSHR4']));
							} else {
								$BSHR4 = '';
							}

							if(isset($row['BSHR5']) && (trim($row['BSHR5']) != '')) {
								$BSHR5 = $this->db->escape(trim($row['BSHR5']));
							} else {
								$BSHR5 = '';
							}

							if(isset($row['BSHR6']) && (trim($row['BSHR6']) != '')) {
								$BSHR6 = $this->db->escape(trim($row['BSHR6']));
							} else {
								$BSHR6 = '';
							}

							if(isset($row['BSHR7']) && (trim($row['BSHR7']) != '')) {
								$BSHR7 = $this->db->escape(trim($row['BSHR7']));
							} else {
								$BSHR7 = '';
							}

							if(isset($row['BSHR8']) && (trim($row['BSHR8']) != '')) {
								$BSHR8 = $this->db->escape(trim($row['BSHR8']));
							} else {
								$BSHR8 = '';
							}

							if(isset($row['BSHR9']) && (trim($row['BSHR9']) != '')) {
								$BSHR9 = $this->db->escape(trim($row['BSHR9']));
							} else {
								$BSHR9 = '';
							}

							if(isset($row['BSHR10']) && (trim($row['BSHR10']) != '')) {
								$BSHR10 = $this->db->escape(trim($row['BSHR10']));
							} else {
								$BSHR10 = '';
							}

							if(isset($row['OSHR1']) && (trim($row['OSHR1']) != '')) {
								$OSHR1 = $this->db->escape(trim($row['OSHR1']));
							} else {
								$OSHR1 = '';
							}

							if(isset($row['OSHR2']) && (trim($row['OSHR2']) != '')) {
								$OSHR2 = $this->db->escape(trim($row['OSHR2']));
							} else {
								$OSHR2 = '';
							}

							if(isset($row['OSHR3']) && (trim($row['OSHR3']) != '')) {
								$OSHR3 = $this->db->escape(trim($row['OSHR3']));
							} else {
								$OSHR3 = '';
							}

							if(isset($row['OSHR4']) && (trim($row['OSHR4']) != '')) {
								$OSHR4 = '';
							} else {
								$OSHR4 = $this->db->escape(trim($row['OSHR4']));
							}

							if(isset($row['OSHR5']) && (trim($row['OSHR5']) != '')) {
								$OSHR5 = $this->db->escape(trim($row['OSHR5']));
							} else {
								$OSHR5 = '';
							}

							if(isset($row['OSHR6']) && (trim($row['OSHR6']) != '')) {
								$OSHR6 = $this->db->escape(trim($row['OSHR6']));
							} else {
								$OSHR6 = '';
							}

							if(isset($row['OSHR7']) && (trim($row['OSHR7']) != '')) {
								$OSHR7 = $this->db->escape(trim($row['OSHR7']));
							} else {
								$OSHR7 = '';
							}

							if(isset($row['OSHR8']) && (trim($row['OSHR8']) != '')) {
								$OSHR8 = $this->db->escape(trim($row['OSHR8']));
							} else {
								$OSHR8 = '';
							}

							if(isset($row['OSHR9']) && (trim($row['OSHR9']) != '')) {
								$OSHR9 = $this->db->escape(trim($row['OSHR9']));
							} else {
								$OSHR9 = '';
							}

							if(isset($row['OSHR10']) && (trim($row['OSHR10']) != '')) {
								$OSHR10 = $this->db->escape(trim($row['OSHR10']));
							} else {
								$OSHR10 = '';
							}

							$this->db->query("INSERT INTO `foal_isb` SET SIRENAME = '".$horse_name."',
		       					MICROCHIP1 = '".$chip_no1."',
		       					MICROCHIP2 = '".$chip_no2."',
		       					FOALDATE = '".$date_foal_date ."',
		       					HORSECOLOR  = '".$horse_color."'  , 
		       					HORSESEX = '".$sex."' ,
		       					YROFFLNG = '".$YROFFLNG."',
		       					SIRENAT = '".$SIRENAT."',
		       					MARENAME = '".$MARENAME."',
								MARENAT = '".$MARENAT."',
								MNTHDATE = '".$MNTHDATE."',
								STUDNAME = '".$STUDNAME."',
								BRAND1 = '".$BRAND1."',
								BRAND2 = '".$BRAND2."',
								PASSP_NO = '".$PASSP_NO."',
								LIFENUMBER = '".$LIFENUMBER."',
								D_BRAND1 = '".$D_BRAND1."',
								D_BRAND2 = '".$D_BRAND2."',
								BR1 = '".$BR1."',
								BR2 = '".$BR2."',
								BR3 = '".$BR3."',
								BR4 = '".$BR4."',
								BR5 = '".$BR5."',
								BR6 = '".$BR6."',
								BR7 = '".$BR7."',
								BR8 = '".$BR8."',
								BR9 = '".$BR9."',
								BR10 = '".$BR10."',
								ON1 = '".$ON1."',
								ON2 = '".$ON2."',
								ON3 = '".$ON3."',
								ON4 = '".$ON4."',
								ON5 = '".$ON5."',
								ON6 = '".$ON6."',
								ON7 = '".$ON7."',
								ON8 = '".$ON8."',
								ON9 = '".$ON9."',
								ON10 = '".$ON10."',
								BSHR1 = '".$BSHR1."',
								BSHR2 = '".$BSHR2."',
								BSHR3 = '".$BSHR3."',
								BSHR4 = '".$BSHR4."',
								BSHR5 = '".$BSHR5."',
								BSHR6 = '".$BSHR6."',
								BSHR7 = '".$BSHR7."',
								BSHR8 = '".$BSHR8."',
								BSHR9 = '".$BSHR9."',
								BSHR10 = '".$BSHR10."',
								OSHR1 = '".$OSHR1."',
								OSHR2 = '".$OSHR2."',
								OSHR3 = '".$OSHR3."',
								OSHR4 = '".$OSHR4."',
								OSHR5 = '".$OSHR5."',
								OSHR6 = '".$OSHR6."',
								OSHR7 = '".$OSHR7."',
								OSHR8 = '".$OSHR8."',
								OSHR9 = '".$OSHR9."',
								OSHR10 = '".$OSHR10."',
								status = 1
								");
		       				
		       		}
			   	}
	 		}
			 	
		
			$url = $this->url->link('utility/import_horse', 'token=' . $this->session->data['token'], true);
			$url = str_replace('&amp;', '&', $url);
			$this->session->data['success'] = 'Date Imported Successfully';
			$this->response->redirect($url);
			fclose($file);
		} else {
		/*	echo 'in';*/
			$url = $this->url->link('utility/import_horse', 'token=' . $this->session->data['token'], true);
			$url = str_replace('&amp;', '&', $url);
			/*echo $url;*/
			$this->session->data['warning'] = 'Please Select the Valid DBF File';
			/*exit;*/
			/*echo '<pre>';
			print_r($this->session);
			exit;*/
			$this->response->redirect($url);
		}
	}
	

	

	protected function validateForm() {
		$this->error = array();
		if (!$this->user->hasPermission('modify', 'utility/import_horse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		//echo '<pre>';print_r($this->request->post);exit;
		if($this->request->post['amount'] == ''){
			$this->error['valierr_amount'] = 'Please Enter Amount!';
		}


		if($this->request->post['letter_date'] == ''){
			$this->error['valierr_letter_date'] = 'Please Select Date!';
		}

		if($this->request->post['received_date'] == ''){
			$this->error['valierr_received_date'] = 'Please Select Date!';
		}

		

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		echo '<pre>';
		print_r($this->error['warning']);exit;
		return !$this->error;
		//exit;
	}

	
}
