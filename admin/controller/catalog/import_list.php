<?php
class ControllerCatalogImportList extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/import_list');

		$this->document->setTitle('PO List');

		$this->load->model('catalog/import_list');
		$this->load->model('catalog/product');

		$this->getList();
	}

	protected function getList() {
		
		if (isset($this->request->get['filter_po_no'])) {
			$filter_po_no = $this->request->get['filter_po_no'];
		} else {
			$filter_po_no = null;
		}

		if (isset($this->request->get['filter_item'])) {
			$filter_item = $this->request->get['filter_item'];
		} else {
			$filter_item = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$filter_vendor_id = $this->request->get['filter_vendor_id'];
		} else {
			$filter_vendor_id = null;
		}

		if (isset($this->request->get['filter_vendorsort'])) {
			$filter_vendorsort = $this->request->get['filter_vendorsort'];
		} else {
			$filter_vendorsort = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_po_no'])) {
			$url .= '&filter_po_no=' . urlencode(html_entity_decode($this->request->get['filter_po_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_item'])) {
			$url .= '&filter_item=' . urlencode(html_entity_decode($this->request->get['filter_item'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES, 'UTF-8'));
		}

		// if (isset($this->request->get['filter_link'])) {
		// 	$url .= '&filter_link=' . $this->request->get['filter_link'];
		// }

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$url .= '&filter_vendor_id=' . urlencode(html_entity_decode($this->request->get['filter_vendor_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendorsort'])) {
			$url .= '&filter_vendorsort=' . urlencode(html_entity_decode($this->request->get['filter_vendorsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = '3';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'PO List',
			'href' => $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['status'] =array(
				'3'  =>'All',
				'0' =>'Open',
				'1' =>'Partial',
				'2' =>'Complete'
		);

		$data['import_lists'] = array();

		$filter_data = array(
			'filter_po_no'	  => $filter_po_no,
			'filter_item'	  => $filter_item,
			'filter_status'	  => $filter_status,
			'filter_date'	  => $filter_date,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$vendor_total = $this->model_catalog_import_list->getTotalTally($filter_data);

		$results = $this->model_catalog_import_list->getTally($filter_data);

		foreach ($results as $result) {

			$user = $result['user_id'];
			$user_data = $this->db->query("SELECT * FROM oc_user WHERE user_id = '".$user."' ");
			if ($user_data->num_rows > 0) {
				$user_name = $user_data->row['firstname'].' '.$user_data->row['lastname'];
			} else {
				$user_name = '';
			}

			if ($result['status'] == 2) {
				$status = 'Complete';
			} elseif ($result['status'] == 1) {
				$status = 'Partial';
			} elseif ($result['status'] == 0) {
				$status = 'Open';
			} else {
				$status = 'All';
			}

			$data['import_lists'][] = array(
				'tally_po_id' => $result['tally_po_id'],
				'date'     => $result['date_added'],
				'user_name'     => $user_name,
				'po_no'     => $result['po_no'],
				'item'     => $result['name_of_the_items'],
				'qty'     => $result['qty'],
				'price'     => $result['rate'],
				'indent_no'     => $result['indent_no'],
				'status' => $status
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_po_no'])) {
			$url .= '&filter_po_no=' . urlencode(html_entity_decode($this->request->get['filter_po_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_item'])) {
			$url .= '&filter_item=' . urlencode(html_entity_decode($this->request->get['filter_item'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$url .= '&filter_vendor_id=' . urlencode(html_entity_decode($this->request->get['filter_vendor_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendorsort'])) {
			$url .= '&filter_vendorsort=' . urlencode(html_entity_decode($this->request->get['filter_vendorsort'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . $url, true);
		$data['import_tally'] = $this->url->link('utility/import_tally_po', 'token=' . $this->session->data['token'] , true);

		$data['sort_name'] = $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'] . '&sort=vendor_name' . $url, true);

		$data['sort_place'] = $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'] . '&sort=vendor_place' . $url, true);

		$data['sort_phone_no'] = $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'] . '&sort=vendor_phone_no' . $url, true);
		$data['sort_contact_person'] = $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'] . '&sort=vendor_contact_person' . $url, true);
		$data['sort_email_id'] = $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'] . '&sort=vendor_email_id' . $url, true);
		$data['sort_items'] = $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'] . '&sort=vendor_items' . $url, true);


		$data['sort_sort_order'] = $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_po_no'])) {
			$url .= '&filter_po_no=' . urlencode(html_entity_decode($this->request->get['filter_po_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_item'])) {
			$url .= '&filter_item=' . urlencode(html_entity_decode($this->request->get['filter_item'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$url .= '&filter_vendor_id=' . urlencode(html_entity_decode($this->request->get['filter_vendor_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendorsort'])) {
			$url .= '&filter_vendorsort=' . urlencode(html_entity_decode($this->request->get['filter_vendorsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $vendor_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($vendor_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($vendor_total - $this->config->get('config_limit_admin'))) ? $vendor_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $vendor_total, ceil($vendor_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_po_no'] = $filter_po_no;
		$data['filter_item'] = $filter_item;
		$data['filter_status'] = $filter_status;
		$data['filter_date'] = $filter_date;
		$data['filter_vendor_id'] = $filter_vendor_id;
		$data['filter_vendorsort'] = $filter_vendorsort;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];

		$this->response->setOutput($this->load->view('catalog/import_list', $data));
	}

	public function autocomplete() {

		$json = array();

		if (isset($this->request->get['filter_po_no'])) {
			$this->load->model('catalog/import_list');

			$filter_data = array(
				'filter_po_no' => $this->request->get['filter_po_no'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_import_list->getTallys($filter_data);

			foreach ($results as $result) {

				$json[] = array(
					'po_no' => $result['po_no'],
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['po_no'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}