<?php
class Controllercataloghorse extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo '<pre>';print_r($this->request->post);exit;
			$horse_id = $this->model_catalog_horse->addHorse($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] .'&horse_id='.$horse_id . '&pp=' .'Horse_Master' .$url, true));
			
			//$this->response->redirect($this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true));


		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo '<pre>';print_r($this->request->post);exit;
			$this->model_catalog_horse->editCategory($this->request->get['horse_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function view() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo '<pre>';print_r($this->request->post);exit;
			$this->model_catalog_horse->editCategory($this->request->get['horse_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getFormview();
	}

	public function delete() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $category_id) {
				$this->model_catalog_horse->deleteCategory($category_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function repair() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		if ($this->validateRepair()) {
			$this->model_catalog_horse->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('catalog/horse', 'token=' . $this->session->data['token'], true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_hourse_name'])) {
			$filter_hourse_name = $this->request->get['filter_hourse_name'];
		} else {
			$filter_hourse_name = '';
		}

		if (isset($this->request->get['filter_hourse_id'])) {
			$filter_hourse_ids =$this->request->get['filter_hourse_id'];
			$filter_hourse_id =substr($filter_hourse_ids,1);
		} else {
			$filter_hourse_ids ="";
			$filter_hourse_id ='';
		}

		if (isset($this->request->get['filter_trainer_name'])) {
			$filter_trainer_name = $this->request->get['filter_trainer_name'];
		} else {
			$filter_trainer_name = '';
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_hourse_name']) ) {
			$url .= '&filter_hourse_name=' . $this->request->get['filter_hourse_name'];
		}

		if (isset($this->request->get['filter_hourse_id']) ) {
			$url .= '&filter_hourse_id=' . $this->request->get['filter_hourse_id'];
		}

		if (isset($this->request->get['filter_trainer_name']) ) {
			$url .= '&filter_trainer_name=' . $this->request->get['filter_trainer_name'];
		}

		if (isset($this->request->get['filter_status']) ) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		$user_group_id = $this->user->getGroupId();
		$user_id = $this->session->data['user_id'];
		$group_ids = $this->db->query("SELECT user_group_id FROM oc_user WHERE user_id = '".$user_id."' ");
		if ($group_ids->num_rows > 0) {
			$data['group_id'] = $group_ids->row['user_group_id'];
		}

		//echo $filter_hourse_name;exit;

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/horse/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/horse/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/horse/repair', 'token=' . $this->session->data['token'] . $url, true);
		$data['import_horse'] = $this->url->link('catalog/isb_horse', 'token=' . $this->session->data['token'], true);

		$data['categories'] = array();

		$data['status'] =array(
				'1'  =>"In",
				'0'  =>'Exit',
				'3' => 'Temp-horse'
		);

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_hourse_name' => $filter_hourse_name,
			'filter_hourse_id' => $filter_hourse_id,
			'filter_trainer_name' => $filter_trainer_name,
			'filter_status'	=>	$filter_status
		);
		//echo '<pre>';print_r($filter_data);exit;
		$data['horseDatas'] =array();
		$category_total = $this->model_catalog_horse->getHourseCount($filter_data);

		$results = $this->model_catalog_horse->getHorses($filter_data);

		foreach ($results as $result) {
			$trainer_name_sql = "SELECT trainer_name FROM `horse_to_trainer` WHERE trainer_status = 1 ";
			if (!empty($filter_data['filter_trainer_name'])) {
				$trainer_name_sql .= " AND LOWER(trainer_name) LIKE '%" . $this->db->escape(strtolower($filter_data['filter_trainer_name'])) . "%' ";
			} else {
				$trainer_name_sql .= " AND horse_id = '".$result['horseseq']."' ";
			}
			$trainer_name_sql .= "ORDER BY `horse_to_trainer_id` DESC LIMIT 1"; 
			//echo $trainer_name_sql;exit;
			$trainer_name = $this->db->query($trainer_name_sql);
			if($trainer_name->num_rows > 0){
				$final_trainer_name = $trainer_name->row['trainer_name'];			
			} else {
				$final_trainer_name = '';
			}

			$equipment_name_sql = "SELECT equipment_name FROM `horse_equipments` WHERE horse_id = '".$result['horseseq']."' AND equipment_status = '1' ";
			$equipment_name = $this->db->query($equipment_name_sql);
			
			$final_equipment_name = '';
			if ($equipment_name->num_rows >0) {
				foreach ($equipment_name->rows as $akey => $avalue) {
					$short_name = $this->db->query("SELECT short_name FROM equipment WHERE equipment_name = '".$avalue['equipment_name']."' ");
					// echo'<pre>';
					// print_r("SELECT short_name FROM equipment WHERE equipment_name = '".$avalue['equipment_name']."' ");
					if ($short_name->num_rows > 0) {
						$final_equipment_name .= '</b>'.$short_name->row['short_name'].', '.'<b>';
					} else {
						$final_equipment_name .= '</b>'.$avalue['equipment_name'].', '.'<b>';
					}
				}//exit;
				
			}

			$shoe_name_sql = "SELECT * FROM `horse_shoeing` WHERE horse_id = '".$result['horseseq']."' ORDER BY `horse_shoeing_id` DESC LIMIT 1 ";
			$shoe_name = $this->db->query($shoe_name_sql);
			//echo "<pre>";print_r($shoe_name);
			$bit_name_sql = "SELECT * FROM `horse_bits` WHERE horse_id = '".$result['horseseq']."' ORDER BY `id` DESC LIMIT 1 ";

			$bit_name = $this->db->query($bit_name_sql);
			//echo "<pre>";print_r($bit_name);

			$final_shoe_name = '';
			if ($shoe_name->num_rows >0  && $bit_name->num_rows >0) {
				//foreach ($shoe_name->rows as $akey => $avalue) {
					if ($shoe_name->row['type'] == 'Aluminium') {
						$type = 'A';
					} elseif ($shoe_name->row['type'] == 'Steel') {
						$type = 'S';
					} else{
						$type = '';
					}
					$final_shoe_name .= '</b>'.$type.' '.$shoe_name->row['shoe_description'].' '.$bit_name->row['short_name'].'<br>'.'<b>';
				//}
				
			}

			$current_date = date("Y-m-d");

			$ban_sql = "SELECT * FROM `horse_ban` WHERE horse_id = '".$result['horseseq']."' ";
			$ban = $this->db->query($ban_sql);
			//echo "<pre>";print_r($ban);
			$final_ban = '';
			if ($ban->num_rows >0) {
				foreach ($ban->rows as $key => $value) {
					// echo "<pre>";print_r($current_date);
					// echo "<pre>";print_r($avalue['enddate_ban']);exit;
					if ($value['enddate_ban'] == '0000-00-00') {
						$final_ban .= '</b>'.$value['club_ban'].'-'.$value['authority'].'<br>'.'<b>';
					}else{
						$final_ban = '';
					}
				}
			} 
			//echo "<pre>";print_r($final_equipment_name);exit;
			$data['horseDatas'][] = array(
				'horseID' 	  			=> $result['horse_code'],
				'horseName'   			=> $result['official_name'],
				'horseColor'  			=> $result['color'] .'/'.$result['sex'],
				// 'horseSex'  			=> $result['sex'],
				'horseRegDate'  		=> $result['reg_date'],
				'horseFoalDate'  		=> $result['foal_date'],
				'horseOrigin'  			=> $result['orgin'],
				'edit'        			=> $this->url->link('catalog/horse/edit', 'token=' . $this->session->data['token'] . '&horse_id=' . $result['horseseq'] . $url, true),
				'view'        			=> $this->url->link('catalog/horse/view', 'token=' . $this->session->data['token'] . '&horse_id=' . $result['horseseq'] . $url, true),
				'horseAge'  			=> $result['age'],
				'horseRating'  			=> $result['rating'],
				'horse_status'  		=> $result['horse_status'],
				'trainer_name'  		=> $final_trainer_name,
				'equipment_name'  		=> $final_equipment_name,
				'shoe_name'  			=> $final_shoe_name,
				'ban'  					=> $final_ban
				//'delete'      => $this->url->link('catalog/horse/delete', 'token=' . $this->session->data['token'] . '&horse_id=' . $result['HORSESEQ'] . $url, true)
			);
		}//exit;
		//echo "<pre>";print_r($data['horseDatas']);exit;
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_view'] = $this->language->get('button_view');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=official_name' . $url, true);
		$data['horseseq'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=horseseq' . $url, true);

		$data['horse_code'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=horse_code' . $url, true);
		$data['sex'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=sex' . $url, true);
		$data['color'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=color' . $url, true);
		$data['age'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=age' . $url, true);
		$data['rating'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=rating' . $url, true);
		$data['horse_status'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=horse_status' . $url, true);

		$data['sort_sort_order'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['filter_hourse_name'] = $filter_hourse_name;
		$data['filter_hourse_id'] = $filter_hourse_id;
		$data['filter_trainer_name'] = $filter_trainer_name;
		$data['filter_status'] = $filter_status;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/horse_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		//echo "<pre>";print_r($this->error);exit;

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['remarks_errors'])) {
			$data['remarks_errors'] = $this->error['remarks_errors'];
		} else {
			$data['remarks_errors'] = '';
		}

		if (isset($this->error['valierr_horse_name'])) {
			$data['valierr_horse_name'] = $this->error['valierr_horse_name'];
		}

		if (isset($this->error['valierr_code'])) {
			$data['valierr_code'] = $this->error['valierr_code'];
		}

		if (isset($this->error['valierr_undertaking_Charge'])) {
			$data['valierr_undertaking_Charge'] = $this->error['valierr_undertaking_Charge'];
		} 


		if (isset($this->error['valierr_date_ownership'])) {
			$data['valierr_date_ownership'] = $this->error['valierr_date_ownership'];
		} 

		if (isset($this->error['valierr_registeration_date'])) {
			$data['valierr_registeration_date'] = $this->error['valierr_registeration_date'];
		} 

		if (isset($this->error['valierr_month_registeration'])) {
			$data['valierr_month_registeration'] = $this->error['valierr_month_registeration'];
		}

		if (isset($this->error['valierr_year_registeration'])) {
			$data['valierr_year_registeration'] = $this->error['valierr_year_registeration'];
		} 

		if (isset($this->error['valierr_foal_date'])) {
			$data['valierr_foal_date'] = $this->error['valierr_foal_date'];
		}

		if (isset($this->error['valierr_month_foal_date'])) {
			$data['valierr_month_foal_date'] = $this->error['valierr_month_foal_date'];
		} 

		if (isset($this->error['valierr_year_foal_date'])) {
			$data['valierr_year_foal_date'] = $this->error['valierr_year_foal_date'];
		} 

		if (isset($this->error['valierr_namehave_btdatenot'])) {
			$data['valierr_namehave_btdatenot'] = $this->error['valierr_namehave_btdatenot'];
		} 

		if (isset($this->error['valierr_datehave_btnamenot'])) {
			$data['valierr_datehave_btnamenot'] = $this->error['valierr_datehave_btnamenot'];
		} 

		if (isset($this->error['valierr_change_horse_name'])) {
			$data['valierr_change_horse_name'] = $this->error['valierr_change_horse_name'];
		} 

		if (isset($this->error['valierr_change_horse_date'])) {
			$data['valierr_change_horse_date'] = $this->error['valierr_change_horse_date'];
		} 

		if (isset($this->error['valierr_month_change_horse'])) {
			$data['valierr_month_change_horse'] = $this->error['valierr_month_change_horse'];
		} 

		if (isset($this->error['valierr_year_change_horse'])) {
			$data['valierr_year_change_horse'] = $this->error['valierr_year_change_horse'];
		} 

		if (isset($this->error['valierr_std_stall_certificate_date'])) {
			$data['valierr_std_stall_certificate_date'] = $this->error['valierr_std_stall_certificate_date'];
		} 

		if (isset($this->error['valierr_month_std_stall_certificate'])) {
			$data['valierr_month_std_stall_certificate'] = $this->error['valierr_month_std_stall_certificate'];
		} 

		if (isset($this->error['valierr_year_std_stall_certificate'])) {
			$data['valierr_year_std_stall_certificate'] = $this->error['valierr_year_std_stall_certificate'];
		} 

		if (isset($this->error['valierr_passport_no'])) {
			$data['valierr_passport_no'] = $this->error['valierr_passport_no'];
		} 

		if (isset($this->error['valierr_registerarton_authentication'])) {
			$data['valierr_registerarton_authentication'] = $this->error['valierr_registerarton_authentication'];
		} 

		if (isset($this->error['valierr_sire'])) {
			$data['valierr_sire'] = $this->error['valierr_sire'];
		} 

		if (isset($this->error['valierr_dam'])) {
			$data['valierr_dam'] = $this->error['valierr_dam'];
		} 

		if (isset($this->error['valierr_chip_no_one'])) {
			$data['valierr_chip_no_one'] = $this->error['valierr_chip_no_one'];
		} 

		if (isset($this->error['valierr_arrival_charges_to_be_paid'])) {
			$data['valierr_arrival_charges_to_be_paid'] = $this->error['valierr_arrival_charges_to_be_paid'];
		} 

		if (isset($this->error['valierr_color'])) {
			$data['valierr_color'] = $this->error['valierr_color'];
		} 

		if (isset($this->error['valierr_origin'])) {
			$data['valierr_origin'] = $this->error['valierr_origin'];
		} 

		if (isset($this->error['valierr_sex'])) {
			$data['valierr_sex'] = $this->error['valierr_sex'];
		} 

		if (isset($this->error['valierr_age'])) {
			$data['valierr_age'] = $this->error['valierr_age'];
		} 

		if (isset($this->error['valierr_stud_form'])) {
			$data['valierr_stud_form'] = $this->error['valierr_stud_form'];
		} 

		if (isset($this->error['valierr_breeder'])) {
			$data['valierr_breeder'] = $this->error['valierr_breeder'];
		} 

		if (isset($this->error['valierr_saddle_no'])) {
			$data['valierr_saddle_no'] = $this->error['valierr_saddle_no'];
		} 

		if (isset($this->error['valierr_octroi_details'])) {
			$data['valierr_octroi_details'] = $this->error['valierr_octroi_details'];
		} 

		if (isset($this->error['valierr_awbi_registration_no'])) {
			$data['valierr_awbi_registration_no'] = $this->error['valierr_awbi_registration_no'];
		} 

		if (isset($this->error['valierr_Id_by_brand'])) {
			$data['valierr_Id_by_brand'] = $this->error['valierr_Id_by_brand'];
		} 

		if (isset($this->error['valierr_std_stall_remarks'])) {
			$data['valierr_std_stall_remarks'] = $this->error['valierr_std_stall_remarks'];
		} 

		if (isset($this->error['valierr_horse_remarks'])) {
			$data['valierr_horse_remarks'] = $this->error['valierr_horse_remarks'];
		} 

		if (isset($this->error['valierr_trainer_name'])) {
			$data['valierr_trainer_name'] = $this->error['valierr_trainer_name'];
		} 

		if (isset($this->error['valierr_date_charge_trainer'])) {
			$data['valierr_date_charge_trainer'] = $this->error['valierr_date_charge_trainer'];
		} 

		if (isset($this->error['valierr_month_charge_trainer'])) {
			$data['valierr_month_charge_trainer'] = $this->error['valierr_month_charge_trainer'];
		} 

		if (isset($this->error['valierr_year_charge_trainer'])) {
			$data['valierr_year_charge_trainer'] = $this->error['valierr_year_charge_trainer'];
		} 

		if (isset($this->error['valierr_arrival_time_trainer'])) {
			$data['valierr_arrival_time_trainer'] = $this->error['valierr_arrival_time_trainer'];
		} 

		if (isset($this->error['valierr_extra_narrration_trainer'])) {
			$data['valierr_extra_narrration_trainer'] = $this->error['valierr_extra_narrration_trainer'];
		} 

		if (isset($this->error['valierr_extra_narrration_two_trainer'])) {
			$data['valierr_extra_narrration_two_trainer'] = $this->error['valierr_extra_narrration_two_trainer'];
		} 

		if (isset($this->error['valierr_date_left_trainer'])) {
			$data['valierr_date_left_trainer'] = $this->error['valierr_date_left_trainer'];
		} 

		if (isset($this->error['valierr_month_left_trainer'])) {
			$data['valierr_month_left_trainer'] = $this->error['valierr_month_left_trainer'];
		} 

		if (isset($this->error['valierr_year_left_trainer'])) {
			$data['valierr_year_left_trainer'] = $this->error['valierr_year_left_trainer'];
		} 

		if (isset($this->error['valierr_undertaking_Charge'])) {
			$data['valierr_undertaking_Charge'] = $this->error['valierr_undertaking_Charge'];
		} 

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['horse_id'])) {
			$data['action'] = $this->url->link('catalog/horse/add', 'token=' . $this->session->data['token'] . $url, true);
			$data['owners_horse_ids'] = 0;
		} else {
			$data['action'] = $this->url->link('catalog/horse/edit', 'token=' . $this->session->data['token'] . '&horse_id=' . $this->request->get['horse_id'] .  $url, true);
			$data['owners_horse_ids'] = $this->request->get['horse_id'];
			$data['change_owner_link'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . '&horse_id=' . $this->request->get['horse_id'] . '&pp=' .'Other' . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true);

		$data['arrival_time_trainer'] = array(
			'AM'  => 'AM',
			'PM'  => 'PM',
		);

		$data['regiteration_authenticaton_datas'] = array(
			'MyRC'  => 'MyRC',
			'RWITC'  => 'RWITC',
			'BTC'  => 'BTC',
			'DRC'  => 'DRC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'RCTC'  => 'RCTC',
			'OTHER'  => 'OTHER',
		);
		$data['origins'] = array(
			'Indian'  => 'Indian',
			'Foreign'  => 'Foreign',
		);

		$data['sexs'] = array(
			'm'  => 'm',
			'g'  => 'g',
			'h'  => 'h',
			'r'  => 'r',
			'f'  => 'f',
			'c'  => 'c',

		);

		$data['authorty_bans'] = array(
			'VO'  => 'VO',
			'SS'  => 'SS',
			'STIPES'  => 'STIPES',
			'BLEEDER 1'  => 'BLEEDER 1',
			'BLEEDER 2'  => 'BLEEDER 2',
			'BLEEDER 3'  => 'BLEEDER 3',
		);


		$data['clubs'] = array(
			'RWITC'  => 'RWITC',
			'RCTC'  => 'RCTC',
			'DRC'  => 'DRC',
			'BTC'  => 'BTC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'MYRC'  => 'MYRC',
		);

		$data['venus'] = array(
			'RWITC'  => 'RWITC',
			'RCTC'  => 'RCTC',
			'DRC'  => 'DRC',
			'BTC'  => 'BTC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'MYRC'  => 'MYRC',
		);

		$data['grads'] = array(
			'Grade 1'  => 'Grade 1',
			'Grade 2'  => 'Grade 2',
			'Grade 3'  => 'Grade 3',
			'No Grade' => '-',
		);

		$data['ownerships_types'] = array(
			'Sale'  => 'Sale',
			'Lease'  => 'Lease',
			'Sale with Contingency'  => 'Sale with Contingency',
			'Lease with Contingency'  => 'Lease with Contingency',
		);

		$data['Active'] = array('0'	=> 'Exit',
							  '1'	=> 'In');

		$types = $this->db->query("SELECT * FROM shoe WHERE 1=1 ")->rows;
		/*echo'<pre>';
		print_r($types);
		exit;*/

		$data['Type'] = array(
			'None' => 'None',
			'Aluminium' => 'Aluminium',
			'Steel' => 'Steel',
			'Unshod' => 'Unshod'
		);

		// $data['license_type1'] =  array(
		// 	'A' => 'A',
		// 	'B'   => 'B',
		// 	'' =>'None',
		// );



		if (isset($this->request->get['horse_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$horse_info = $this->model_catalog_horse->getHorsesform($this->request->get['horse_id']);
			$trainer_info_all = $this->model_catalog_horse->getTrainerAll($this->request->get['horse_id']);
			
			$trainer_info_current = $this->model_catalog_horse->getTrainerCurrent($this->request->get['horse_id']);
			$ownerinfo =  $this->model_catalog_horse->getOwnerform($this->request->get['horse_id']);
			$ownerprovis =  $this->model_catalog_horse->getOwnerprovis($this->request->get['horse_id']);
			$baninfo_with_date_end =  $this->model_catalog_horse->getBanWithdateEnd($this->request->get['horse_id']);//FOR BAN HISTORY
			$baninfo_witout_date_end =  $this->model_catalog_horse->getBanWithoutdateEnd($this->request->get['horse_id']);//FOR BAN DETAILS
			$equipment_info =  $this->model_catalog_horse->getEquipment($this->request->get['horse_id']);
			$equipment_history_info =  $this->model_catalog_horse->getEquipmentHistroyData($this->request->get['horse_id']);

			$shoe_info =  $this->model_catalog_horse->getShoie($this->request->get['horse_id']);
			$current_shoe_info =  $this->model_catalog_horse->getCurrentShoie($this->request->get['horse_id']);

			$stack_info =  $this->model_catalog_horse->getStackOt($this->request->get['horse_id']);

			$ownerhistory =  $this->model_catalog_horse->getOwnerHistoryDatas($this->request->get['horse_id']);
		}

		//echo "<pre>";print_r($this->request->post['trainer_info_history']);exit;
		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		
    	if (!empty($trainer_info_all)) {
			$data['trainer_info_all'] = $trainer_info_all;
		} 
		else {
			$data['trainer_info_all'] = array();
		}

		if (isset($this->request->post['stackesdatas'])) {
      		$data['stackoutstation_datas'] = $this->request->post['stackesdatas'];
    	} elseif (!empty($stack_info)) {
			$data['stackoutstation_datas'] = $stack_info;
		} else {
			$data['stackoutstation_datas'] = array();
		}

		if (isset($this->request->post['stacked_hidden_outstation_id'])) {
      		$data['stacked_hidden_outstation_id'] = $this->request->post['stacked_hidden_outstation_id'];
    	} elseif (!empty($stack_info)) {
			$stack_info_counts = count($stack_info);
			$data['stacked_hidden_outstation_id'] = $stack_info_counts;
		} else {
      		$data['stacked_hidden_outstation_id'] = 1;
    	}

    	if (isset($this->request->post['shoe_datas'])) {
      		$data['new_shoe_dats'] = $this->request->post['shoe_datas'];
    	} elseif (!empty($current_shoe_info)) {
			$data['new_shoe_dats'] = $current_shoe_info;
		} else {
			$data['new_shoe_dats'] = array();
		}

		$horse_codes = $this->db->query("SELECT horse_code FROM horse1 WHERE 1=1 ORDER BY horse_code DESC LIMIT 1 ");
		/*echo'<pre>';
		print_r("SELECT med_code FROM medicine WHERE 1=1 ORDER BY id DESC LIMIT 1 ");
		exit;*/
		if ($horse_codes->num_rows > 0) {
			$horse_code = $horse_codes->row['horse_code'];
		} else {
			$horse_code = '';
		}

		if (isset($this->request->post['horse_code'])) {
			$data['horse_code'] = $this->request->post['horse_code'];
		} elseif (!empty($horse_info)) {
			$data['horse_code'] = $horse_info['horse_code'];
		} elseif (isset($this->request->post['hidden_horse_code'])) {
			$data['horse_code'] = $this->request->post['hidden_horse_code'];
		} else {
			$addHcode = $horse_code + 1;
			$data['horse_code'] = $addHcode;
		}


		if (isset($this->request->post['shoe_datas_history'])) {
      		$data['history_shoe'] = $this->request->post['shoe_datas_history'];
    	} elseif (!empty($shoe_info)) {
			$data['history_shoe'] = $shoe_info;
		} else {
			$data['history_shoe'] = array();
		}

		if (isset($this->request->post['equipment_dtas'])) {
      		$data['equipment_datas'] = $this->request->post['equipment_dtas'];
    	} elseif (!empty($equipment_info)) {
			$data['equipment_datas'] = $equipment_info;
		} else {
			$data['equipment_datas'] = array();
		}

		//echo "<pre>";print_r($equipment_info);exit;

		if (isset($this->request->post['equipment_datas'])) {
      		$data['equipmentdatas'] = $this->request->post['equipment_datas'];
    	} elseif (!empty($equipment_history_info)) {
			$data['equipmentdatas'] = $equipment_history_info;
		} else {
			$data['equipmentdatas'] = array();
		}

		if (isset($this->request->post['ownerdatas'])) {
      		$data['owners_olddatas'] = $this->request->post['ownerdatas'];
    	} elseif (!empty($ownerinfo)) {
    		$parent_data = array();
    		$sub_parent_data = array();
			foreach ($ownerinfo as $okey => $ovalue) {
				$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$ovalue['horse_to_owner_id']."'");
				if($lease_table_data->num_rows > 0){
					$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;

					$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
					if($lease_parent_data->num_rows > 0){
						$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
					}
				}
				$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$ovalue['to_owner_id']."'");
				$ownerinfo[$okey]['partner_status'] = ($owners_partner->num_rows > 0) ? "1" : "0";

				if($parent_data){
					$ownerinfo[$okey]['parent_owner_name'] = $parent_data['to_owner'];
					$ownerinfo[$okey]['parent_owner_id'] = $parent_data['to_owner_id'];
				}

				if($sub_parent_data){
					$ownerinfo[$okey]['sub_parent_owner_name'] = $sub_parent_data['to_owner'];
				}

				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$ownerinfo[$okey]['last_color'] = $last_colorss->row['horse_to_owner_id'];
				} else {
					$ownerinfo[$okey]['last_color'] = 0;
				}

			}
			$data['owners_olddatas'] = $ownerinfo;
		} else {
      		$data['owners_olddatas'] = array();
    	}

    	if (!empty($ownerprovis)) {
    		$parent_data = array();
    		$sub_parent_data = array();
			foreach ($ownerprovis as $okey => $ovalue) {
				$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$ovalue['horse_to_owner_id']."'");
				if($lease_table_data->num_rows > 0){
					$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;

					$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
					if($lease_parent_data->num_rows > 0){
						$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
					}
				}
				$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$ovalue['to_owner_id']."'");
				$ownerprovis[$okey]['partner_status'] = ($owners_partner->num_rows > 0) ? "1" : "0";

				if($parent_data){
					$ownerprovis[$okey]['parent_owner_name'] = $parent_data['to_owner'];
					$ownerprovis[$okey]['parent_owner_id'] = $parent_data['to_owner_id'];
				}

				if($sub_parent_data){
					$ownerprovis[$okey]['sub_parent_owner_name'] = $sub_parent_data['to_owner'];
				}

				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$ownerprovis[$okey]['last_color'] = $last_colorss->row['horse_to_owner_id'];
				} else {
					$ownerprovis[$okey]['last_color'] = 0;
				}

			}
			$data['owners_provisional'] = $ownerprovis;
		} else {
      		$data['owners_provisional'] = array();
    	}

    	if (!empty($ownerhistory)) {
    		$parent_data = array();
			foreach ($ownerhistory as $okey => $ovalue) {
				$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$ovalue['horse_to_owner_id']."'");
				if($lease_table_data->num_rows > 0){
					$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
				}

				$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$ovalue['to_owner_id']."'");
				$ownerhistory[$okey]['partner_status'] = ($owners_partner->num_rows > 0) ? "1" : "0";


				if($parent_data){
					$ownerhistory[$okey]['parent_owner_name'] = $parent_data['to_owner'];
					$ownerhistory[$okey]['parent_owner_id'] = $parent_data['to_owner_id'];
				}
			}
			$data['ownerhistory'] = $ownerhistory;
		} else {
      		$data['ownerhistory'] = array();
    	}

    	

    	if (isset($this->request->post['ownership_hidden_id'])) {
      		$data['ownership_hidden_id'] = $this->request->post['ownership_hidden_id'];
    	} elseif (!empty($ownerinfo)) {
			$owner_counts = count($ownerinfo);
			$data['ownership_hidden_id'] = $owner_counts;
		} else {
      		$data['ownership_hidden_id'] = 1;
    	}

		if (isset($this->request->post['banhistorydatas'])) {
      		$data['bandatas'] = $this->request->post['banhistorydatas'];
    	} elseif (!empty($baninfo_with_date_end)) {
			$data['bandatas'] = $baninfo_with_date_end;
		} else {
      		$data['bandatas'] = array();
    	}

    	if (isset($this->request->post['bandats'])) {
      		$data['bandeatils'] = $this->request->post['bandats'];
    	} elseif (!empty($baninfo_witout_date_end)) {
			$data['bandeatils'] = $baninfo_witout_date_end;
		} else {
      		$data['bandeatils'] = array();
    	}

    	if (isset($this->request->post['id_hidden_band'])) {
      		$data['id_hidden_band'] = $this->request->post['id_hidden_band'];
    	} elseif (!empty($baninfo_witout_date_end)) {
			$ban_counts = count($baninfo_witout_date_end);
			$data['id_hidden_band'] = $ban_counts;
		} else {
      		$data['id_hidden_band'] = 1;
    	}

		if (isset($this->request->post['horse_name'])) {
			$data['horse_name'] = $this->request->post['horse_name'];
		} elseif (!empty($horse_info)) {
			$data['horse_name'] = $horse_info['official_name'];
		} else {
			$data['horse_name'] = '';
		}

		if (isset($this->request->post['horse_name'])) {
			$data['horse_name'] = $this->request->post['horse_name'];
		} elseif (!empty($horse_info)) {
			$data['horse_name'] = $horse_info['official_name'];
		} else {
			$data['horse_name'] = '';
		}

		if (isset($this->request->post['isActive'])) {
			$data['isActive'] = $this->request->post['isActive'];
		} elseif (!empty($horse_info)) {
			$data['isActive'] = $horse_info['horse_status'];
		} else {
			$data['isActive'] = '1';
		}

		if (isset($this->request->post['hidden_active'])) {
			$data['isActive1'] = $this->request->post['hidden_active'];
		} elseif (!empty($horse_info)) {
			$data['isActive1'] = $horse_info['horse_status'];
		} else {
			$data['isActive1'] = '';
		}

		if (isset($this->request->post['changehorse_name'])) {
			$data['changehorse_name'] = $this->request->post['changehorse_name'];
		} elseif (!empty($horse_info)) {
			$data['changehorse_name'] = $horse_info['changehorse_name'];
		} else {
			$data['changehorse_name'] = '';
		}

		if (isset($this->request->post['official_name_change'])) {
			$data['official_name_change'] = $this->request->post['official_name_change'];
		} elseif (!empty($horse_info)) {
			$data['official_name_change'] = $horse_info['official_name_change_status'];
		} else {
			$data['official_name_change'] = 0;
		}

		if (isset($this->request->post['horse_id'])) {
			$data['horse_id'] = $this->request->post['horse_id'];
		} elseif (!empty($horse_info)) {
			$data['horse_id'] = $horse_info['horseseq'];
		} else {
			$data['horse_id'] = '';
		}

		if (isset($this->request->post['passport_no'])) {
			$data['passport_no'] = $this->request->post['passport_no'];
		} elseif (!empty($horse_info)) {
			$data['passport_no'] = $horse_info['life_no'];
		} else {
			$data['passport_no'] = '';
		}

		if (isset($this->request->post['change_horse_dates'])) {
			$data['change_horse_dates'] = $this->request->post['change_horse_dates'];
		} elseif (!empty($horse_info)) {
			if($horse_info['date_of_changehorse_name'] != '1970-01-01' && $horse_info['date_of_changehorse_name'] != '0000-00-00'){
				$data['change_horse_dates']= date('d-m-Y', strtotime($horse_info['date_of_changehorse_name']));
			} else {
			$data['change_horse_dates'] = '';
			}
		} else {
			$data['change_horse_dates'] = '';
		}

		if (isset($this->request->post['provisional'])) {
			$data['provisional'] = $this->request->post['provisional'];
		} elseif (!empty($trainer_info_current)) {
			$data['provisional'] = $trainer_info_current['provisional'];
		} else {
			$data['provisional'] = 0;
		}

		if (isset($this->request->post['registeration_date'])) {
			$data['registeration_date'] = $this->request->post['registeration_date'];
		} elseif (!empty($horse_info)) {
			if($horse_info['reg_date'] != '1970-01-01' && $horse_info['reg_date'] != '0000-00-00'){
				$data['registeration_date']= date('d-m-Y', strtotime($horse_info['reg_date']));
			} else {
			$data['registeration_date'] = '';
			}
		} else {
			$data['registeration_date'] = '';
		}

		if (isset($this->request->post['date_foal_date'])) {
			$data['date_foal_date'] = $this->request->post['date_foal_date'];
		} elseif (!empty($horse_info)) {
			if($horse_info['foal_date'] != '1970-01-01' && $horse_info['foal_date'] != '0000-00-00'){
				$data['date_foal_date']= date('d-m-Y', strtotime($horse_info['foal_date']));
			} else {
			$data['date_foal_date'] = '';
			}
		} else {
			$data['date_foal_date'] = '';
		}

		if (isset($this->request->post['date_std_stall_certificate'])) {
			$data['date_std_stall_certificate'] = $this->request->post['date_std_stall_certificate'];
		} elseif (!empty($horse_info)) {

			if($horse_info['std_stall_certificate_date'] != '1970-01-01' && $horse_info['std_stall_certificate_date'] != '0000-00-00'){
				$data['date_std_stall_certificate']= date('d-m-Y', strtotime($horse_info['std_stall_certificate_date']));
			} else {
				$data['date_std_stall_certificate'] = '';
			}
		} else {
			$data['date_std_stall_certificate'] = '';
		}

		if (isset($this->request->post['date_charge_trainer'])) {
			$data['date_charge_trainer'] = $this->request->post['date_charge_trainer'];
		} elseif (!empty($trainer_info_current)) {
			if($trainer_info_current['date_of_charge'] != '1970-01-01' && $trainer_info_current['date_of_charge'] != '0000-00-00'){
				$data['date_charge_trainer']= date('d-m-Y', strtotime($trainer_info_current['date_of_charge']));
			} else {
			$data['date_charge_trainer'] = '';
			}
		} else {
			$data['date_charge_trainer'] = '';
		}

		if (isset($this->request->post['date_left_trainer'])) {
			$data['date_left_trainer'] = $this->request->post['date_left_trainer'];
		} elseif (!empty($trainer_info_current)) {
			if($trainer_info_current['left_date_of_charge'] != '1970-01-01' && $trainer_info_current['left_date_of_charge'] != '0000-00-00'){
				$data['date_left_trainer']= date('d-m-Y', strtotime($trainer_info_current['left_date_of_charge']));
			} else {
			$data['date_left_trainer'] = '';
			}
		} else {
			$data['date_left_trainer'] = '';
		}

		if (isset($this->request->post['registerarton_authentication'])) {
			$data['registerarton_authentication'] = $this->request->post['registerarton_authentication'];
		} elseif (!empty($horse_info)) {
			$data['registerarton_authentication'] = $horse_info['reg_auth_id'];
		} else {
			$data['registerarton_authentication'] = '';
		}

		if (isset($this->request->post['sire'])) {
			$data['sire'] = $this->request->post['sire'];
		} elseif (!empty($horse_info)) {
			$data['sire'] = $horse_info['sire_name'];
		} else {
			$data['sire'] = '';
		}

		if (isset($this->request->post['dam'])) {
			$data['dam'] = $this->request->post['dam'];
		} elseif (!empty($horse_info)) {
			$data['dam'] = $horse_info['dam_name'];
		} else {
			$data['dam'] = '';
		}


		if (isset($this->request->post['color'])) {
			$data['color'] = $this->request->post['color'];
		} elseif (!empty($horse_info)) {
			$data['color'] = $horse_info['color'];
		} else {
			$data['color'] = '';
		}

		if (isset($this->request->post['origin'])) {
			$data['origin'] = $this->request->post['origin'];
		} elseif (!empty($horse_info)) {
			$data['origin'] = $horse_info['orgin'];
		} else {
			$data['origin'] = '';
		}

		// echo'<pre>';
		// print_r($horse_info['orgin']);
		// exit;
		if (isset($this->request->post['sex'])) {
			$data['sex'] = $this->request->post['sex'];
		} elseif (!empty($horse_info)) {
			$data['sex'] = $horse_info['sex'];
		} else {
			$data['sex'] = '';
		}

		/*if (isset($this->request->post['castration_date'])) {
			$data['castration_date'] = $this->request->post['castration_date'];
		} elseif (!empty($horse_info)) {
			$data['castration_date'] = date('d-m-Y', strtotime($horse_info['castration_date']));
		} else {
			$data['castration_date'] = '';
		}*/

		if (isset($this->request->post['castration_date'])) {
			$data['castration_date'] = $this->request->post['castration_date'];
		} elseif (!empty($horse_info)) {
			if($horse_info['castration_date'] != '1970-01-01' && $horse_info['castration_date'] != '0000-00-00'){
				$data['castration_date']= date('d-m-Y', strtotime($horse_info['castration_date']));
			} else {
			$data['castration_date'] = '';
			}
		} else {
			$data['castration_date'] = '';
		}

		if (isset($this->request->post['stud_form'])) {
			$data['stud_form'] = $this->request->post['stud_form'];
		} elseif (!empty($horse_info)) {
			$data['stud_form'] = $horse_info['stud_form'];
		} else {
			$data['stud_form'] = '';
		}

		if (isset($this->request->post['breeder'])) {
			$data['breeder'] = $this->request->post['breeder'];
		} elseif (!empty($horse_info)) {
			$data['breeder'] = $horse_info['breeder_name'];
		} else {
			$data['breeder'] = '';
		}

		if (isset($this->request->post['saddle_no'])) {
			$data['saddle_no'] = $this->request->post['saddle_no'];
		} elseif (!empty($horse_info)) {
			$data['saddle_no'] = $horse_info['saddle_no'];
		} else {
			$data['saddle_no'] = '';
		}

		if (isset($this->request->post['saddle_no'])) {
			$data['saddle_no'] = $this->request->post['saddle_no'];
		} elseif (!empty($horse_info)) {
			$data['saddle_no'] = $horse_info['saddle_no'];
		} else {
			$data['saddle_no'] = '';
		}

		if (isset($this->request->post['chip_no_one'])) {
			$data['chip_no_one'] = $this->request->post['chip_no_one'];
		} elseif (!empty($horse_info)) {
			$data['chip_no_one'] = $horse_info['chip_no1'];
		} else {
			$data['chip_no_one'] = '';
		}

		if (isset($this->request->post['chip_no_two'])) {
			$data['chip_no_two'] = $this->request->post['chip_no_two'];
		} elseif (!empty($horse_info)) {
			$data['chip_no_two'] = $horse_info['chip_no2'];
		} else {
			$data['chip_no_two'] = '';
		}

		if (isset($this->request->post['arrival_charges_to_be_paid'])) {
			$data['arrival_charges_to_be_paid'] = $this->request->post['arrival_charges_to_be_paid'];
		} elseif (!empty($horse_info)) {
			if($horse_info['arrival_charges_to_be_paid'] != '1970-01-01' && $horse_info['arrival_charges_to_be_paid'] != '0000-00-00'){
				$data['arrival_charges_to_be_paid']= date('d-m-Y', strtotime($horse_info['arrival_charges_to_be_paid']));
			} else {
				$data['arrival_charges_to_be_paid'] = '';
			}
		} else {
			$data['arrival_charges_to_be_paid'] = '';
		}

		if (isset($this->request->post['one_time_lavy'])) {
			$data['one_time_lavy'] = $this->request->post['one_time_lavy'];
		} elseif (!empty($horse_info)) {
			$data['one_time_lavy'] = $horse_info['one_time_lavy'];
		} else {
			$data['one_time_lavy'] = '';
		}

		if (isset($this->request->post['age'])) {
			$data['age'] = $this->request->post['age'];
		} elseif (!empty($horse_info)) {
			$data['age'] = $horse_info['age'];
		} else {
			$data['age'] = '';
		}

		if (isset($this->request->post['rating'])) {
			$data['rating'] = $this->request->post['rating'];
		} elseif (!empty($horse_info)) {
			$data['rating'] = $horse_info['rating'];
		} else {
			$data['rating'] = '';
		}

		if (isset($this->request->post['octroi_details'])) {
			$data['octroi_details'] = $this->request->post['octroi_details'];
		} elseif (!empty($horse_info)) {
			$data['octroi_details'] = $horse_info['octroi_details'];
		} else {
			$data['octroi_details'] = '';
		}

		if (isset($this->request->post['awbi_registration_no'])) {
			$data['awbi_registration_no'] = $this->request->post['awbi_registration_no'];
		} elseif (!empty($horse_info)) {
			$data['awbi_registration_no'] = $horse_info['awbi_registration_no'];
		} else {
			$data['awbi_registration_no'] = '';
		}

		if (isset($this->request->post['awbi_registration_file'])) {
			$data['awbi_registration_file'] = $this->request->post['awbi_registration_file'];
		} elseif (!empty($horse_info)) {
			$data['awbi_registration_file'] = $horse_info['awbi_registration_file'];
		} else {
			$data['awbi_registration_file'] = '';
		}

		if (isset($this->request->post['awbi_registration_file'])) {
			$data['awbi_registration_file'] = $this->request->post['awbi_registration_file'];
			$data['awbi_registration_file_source'] = $this->request->post['awbi_registration_file_source'];
		} elseif (!empty($horse_info)) {
			$data['awbi_registration_file'] = $horse_info['awbi_registration_file'];
			$data['awbi_registration_file_source'] = $horse_info['awbi_registration_file_source'];
		} else {	
			$data['awbi_registration_file'] = '';
			$data['awbi_registration_file_source'] = '';
		}

		if (isset($this->request->post['Id_by_brand'])) {
			$data['Id_by_brand'] = $this->request->post['Id_by_brand'];
		} elseif (!empty($horse_info)) {
			$data['Id_by_brand'] = $horse_info['Id_by_brand'];
		} else {
			$data['Id_by_brand'] = '';
		}

		if (isset($this->request->post['std_stall_remarks'])) {
			$data['std_stall_remarks'] = $this->request->post['std_stall_remarks'];
		} elseif (!empty($horse_info)) {
			$data['std_stall_remarks'] = $horse_info['std_stall_remarks'];
		} else {
			$data['std_stall_remarks'] = '';
		}

		if (isset($this->request->post['horse_remarks'])) {
			$data['horse_remarks'] = $this->request->post['horse_remarks'];
		} elseif (!empty($horse_info)) {
			$data['horse_remarks'] = $horse_info['horse_remarks'];
		} else {
			$data['horse_remarks'] = '';
		}

		if(isset($this->request->get['show_owner_tab'])){
			$data['show_owner_tab'] = $this->request->get['show_owner_tab'];
		} else {
			$data['show_owner_tab'] = 0;
		}

		if (isset($this->request->post['trainer_name'])) {
			$data['trainer_name'] = $this->request->post['trainer_name'];
		} elseif (!empty($trainer_info_current)) {
			$data['trainer_name'] = $trainer_info_current['trainer_name'];
		} else {
			$data['trainer_name'] = '';
		}

		if (isset($this->request->post['new_trainer_name'])) {
			$data['new_trainer_name'] = $this->request->post['new_trainer_name'];
		} else {
			$data['new_trainer_name'] = '';
		}

		if (isset($this->request->post['new_trainer_code'])) {
			$data['new_trainer_code'] = $this->request->post['new_trainer_code'];
		} else {
			$data['new_trainer_code'] = '';
		}


		if (isset($this->request->post['trainer_id'])) {
			$data['trainer_id'] = $this->request->post['trainer_id'];
		} elseif (!empty($trainer_info_current)) {
			$data['trainer_id'] = $trainer_info_current['trainer_id'];
		} else {
			$data['trainer_id'] = '';
		}

		if (isset($this->request->post['horse_to_trainer_id'])) {
			$data['horse_to_trainer_id'] = $this->request->post['horse_to_trainer_id'];
		} elseif (!empty($trainer_info_current)) {
			$data['horse_to_trainer_id'] = $trainer_info_current['horse_to_trainer_id'];
		} else {
			$data['horse_to_trainer_id'] = '';
		}

		if (isset($this->request->post['trainer_codes_id'])) {
			$data['trainer_codes_id'] = $this->request->post['trainer_codes_id'];
		} elseif (!empty($trainer_info_current)) {
			$data['trainer_codes_id'] = $trainer_info_current['trainer_code'];
		} else {
			$data['trainer_codes_id'] = '';
		}

		if (isset($this->request->post['arrival_time_trainers'])) {
			$data['arrival_time_trainers'] = $this->request->post['arrival_time_trainers'];
		} elseif (!empty($trainer_info_current)) {
			$data['arrival_time_trainers'] = $trainer_info_current['arrival_time'];
		} else {
			$data['arrival_time_trainers'] = '';
		}

		if (isset($this->request->post['extra_narrration_trainer'])) {
			$data['extra_narrration_trainer'] = $this->request->post['extra_narrration_trainer'];
		} elseif (!empty($trainer_info_current)) {
			$data['extra_narrration_trainer'] = $trainer_info_current['extra_narration'];
		} else {
			$data['extra_narrration_trainer'] = '';
		}

		if (isset($this->request->post['extra_narrration_two_trainer'])) {
			$data['extra_narrration_two_trainer'] = $this->request->post['extra_narrration_two_trainer'];
		} elseif (!empty($trainer_info_current)) {
			$data['extra_narrration_two_trainer'] = $trainer_info_current['extra_narration_two'];
		} else {
			$data['extra_narrration_two_trainer'] = '';
		}

		if (isset($this->request->post['undertaking_charge'])) {
			$data['undertaking_charge'] = $this->request->post['undertaking_charge'];
		} elseif (!empty($trainer_info_current)) {
			$data['undertaking_charge'] = $trainer_info_current['undertaking_charge'];
		} else {
			$data['undertaking_charge'] = 0;
		}

		if (isset($this->request->post['hidden_isb_horse'])) {
			$data['hidden_isb_horse'] = $this->request->post['hidden_isb_horse'];
		} else {
			$data['hidden_isb_horse'] = '';
		}
		
		$data['path'] = '';

		$this->load->model('catalog/filter');

		if (isset($this->request->post['category_filter'])) {
			$filters = $this->request->post['category_filter'];
		} elseif (isset($this->request->get['category_id'])) {
			$filters = $this->model_catalog_horse->getCategoryFilters($this->request->get['category_id']);
		} else {
			$filters = array();
		}

		$data['category_filters'] = array();

		foreach ($filters as $filter_id) {
			$filter_info = $this->model_catalog_filter->getFilter($filter_id);

			if ($filter_info) {
				$data['category_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}

		$this->load->model('setting/store');
		$product_specials = array();
		$data['product_specials'] = $product_specials;

		$product_recurrings = array();
		$data['product_recurrings'] = $product_recurrings;

		$data['recurrings'] = array();
		$data['customer_groups'] = array();
		$data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['category_store'])) {
			$data['category_store'] = $this->request->post['category_store'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_store'] = $this->model_catalog_horse->getCategoryStores($this->request->get['category_id']);
		} else {
			$data['category_store'] = array(0);
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($category_info)) {
			$data['keyword'] = $category_info['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($category_info)) {
			$data['image'] = $category_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($category_info) && is_file(DIR_IMAGE . $category_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($category_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['top'])) {
			$data['top'] = $this->request->post['top'];
		} elseif (!empty($category_info)) {
			$data['top'] = $category_info['top'];
		} else {
			$data['top'] = 0;
		}

		if (isset($this->request->post['column'])) {
			$data['column'] = $this->request->post['column'];
		} elseif (!empty($category_info)) {
			$data['column'] = $category_info['column'];
		} else {
			$data['column'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($category_info)) {
			$data['sort_order'] = $category_info['sort_order'];
		} else {
			$data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = true;
		}

		if (isset($this->request->post['category_layout'])) {
			$data['category_layout'] = $this->request->post['category_layout'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_layout'] = $this->model_catalog_horse->getCategoryLayouts($this->request->get['category_id']);
		} else {
			$data['category_layout'] = array();
		}

		if (isset($this->request->post['bit_datas'])) {
			$data['bit_datas'] = $this->request->post['bit_datas'];
		} elseif (!empty($horse_info)) {
			$bit_datass = $this->db->query("SELECT * FROM `horse_bits` WHERE `horse_id` = '".$horse_info['horseseq']."' AND end_date <> '0000-00-00' ORDER BY start_date ASC ")->rows;
			$bit_datas = array();

			foreach($bit_datass as $pkeys => $pvalues){
				
				$bit_datas[] = array(
					'short_name' => $pvalues['short_name'],
					'long_name' => $pvalues['long_name'],
					'start_date' => date("d-m-Y", strtotime($pvalues['start_date'])),
					'end_date' => date("d-m-Y", strtotime($pvalues['end_date'])),

					'status' => $pvalues['status'],
				);	
			}
			$data['bit_datas'] = $bit_datas;


			$current_bit_datass = $this->db->query("SELECT * FROM `horse_bits` WHERE `horse_id` = '".$this->request->get['horse_id']."' AND end_date = '0000-00-00' ")->rows;
			$current_bit_datas = array();

			foreach($current_bit_datass as $pkeys => $pvalues){
				
				$current_bit_datas[] = array(
					'id' =>  $pvalues['id'],
					'short_name' => $pvalues['short_name'],
					'long_name' => $pvalues['long_name'],
					'start_date' => date("d-m-Y", strtotime($pvalues['start_date'])),
					'status' => $pvalues['status'],
				);	
			}
			$data['current_bit_datass'] = $current_bit_datas;
		} else {
			$data['bit_datas'] = array();
			$data['current_bit_datass'] = array();
		}

		if (isset($this->request->get['horse_id'])) {
			$horse_to_trainer_query = $this->db->query("SELECT * FROM `horse_to_trainer`WHERE horse_id = '".$this->request->get['horse_id']."' AND trainer_status = '1' ORDER BY `horse_to_trainer_id` DESC LIMIT 1 ");
			if ($horse_to_trainer_query->num_rows > 0) {
				$trainer_id_ht = $horse_to_trainer_query->row['trainer_id'];
				$data_license_type = $this->db->query("SELECT id,license_type FROM `trainers` WHERE id='".$horse_to_trainer_query->row['trainer_id']."' ");
				if ($data_license_type->num_rows > 0) {
					$data['license_type'] = $data_license_type->row['license_type'];
				} else{
					$data['license_type'] = '';
				}
				//echo "<pre>";print_r($license_type->rows);exit;
			} else{
				$data['license_type'] = '';
			}
		} else{
			$data['license_type'] = '';
		}

		if (isset($this->request->get['horse_id'])) {
			$name_registrations = $this->db->query("SELECT * FROM `oc_registration_module`WHERE horse_id = '".$this->request->get['horse_id']."' ORDER BY `id` DESC LIMIT 1 ");
			if ($name_registrations->num_rows > 0 && $name_registrations->row['modification'] == '2') {
				$data['name_registration'] = $name_registrations->row['registeration_type'];
				//echo "<pre>";print_r($data['name_registration']);exit;
			} else{
				$data['name_registration'] = '';
			}
		} else{
			$data['name_registration'] = '';
		}


		//$license_type = $this->db->query("SELECT id,license_type FROM `trainers` WHERE 1=1 ");



		


		
		// echo'<pre>';
		// print_r($data['bit_datas']);
		// exit;

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/horse_form', $data));
	}

	protected function getFormview() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		//echo "<pre>";print_r($this->error);exit;

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['remarks_errors'])) {
			$data['remarks_errors'] = $this->error['remarks_errors'];
		} else {
			$data['remarks_errors'] = '';
		}

		if (isset($this->error['valierr_horse_name'])) {
			$data['valierr_horse_name'] = $this->error['valierr_horse_name'];
		}

		if (isset($this->error['valierr_code'])) {
			$data['valierr_code'] = $this->error['valierr_code'];
		}

		if (isset($this->error['valierr_undertaking_Charge'])) {
			$data['valierr_undertaking_Charge'] = $this->error['valierr_undertaking_Charge'];
		} 


		if (isset($this->error['valierr_date_ownership'])) {
			$data['valierr_date_ownership'] = $this->error['valierr_date_ownership'];
		} 

		if (isset($this->error['valierr_registeration_date'])) {
			$data['valierr_registeration_date'] = $this->error['valierr_registeration_date'];
		} 

		if (isset($this->error['valierr_month_registeration'])) {
			$data['valierr_month_registeration'] = $this->error['valierr_month_registeration'];
		}

		if (isset($this->error['valierr_year_registeration'])) {
			$data['valierr_year_registeration'] = $this->error['valierr_year_registeration'];
		} 

		if (isset($this->error['valierr_foal_date'])) {
			$data['valierr_foal_date'] = $this->error['valierr_foal_date'];
		}

		if (isset($this->error['valierr_month_foal_date'])) {
			$data['valierr_month_foal_date'] = $this->error['valierr_month_foal_date'];
		} 

		if (isset($this->error['valierr_year_foal_date'])) {
			$data['valierr_year_foal_date'] = $this->error['valierr_year_foal_date'];
		} 

		if (isset($this->error['valierr_namehave_btdatenot'])) {
			$data['valierr_namehave_btdatenot'] = $this->error['valierr_namehave_btdatenot'];
		} 

		if (isset($this->error['valierr_datehave_btnamenot'])) {
			$data['valierr_datehave_btnamenot'] = $this->error['valierr_datehave_btnamenot'];
		} 

		if (isset($this->error['valierr_change_horse_name'])) {
			$data['valierr_change_horse_name'] = $this->error['valierr_change_horse_name'];
		} 

		if (isset($this->error['valierr_change_horse_date'])) {
			$data['valierr_change_horse_date'] = $this->error['valierr_change_horse_date'];
		} 

		if (isset($this->error['valierr_month_change_horse'])) {
			$data['valierr_month_change_horse'] = $this->error['valierr_month_change_horse'];
		} 

		if (isset($this->error['valierr_year_change_horse'])) {
			$data['valierr_year_change_horse'] = $this->error['valierr_year_change_horse'];
		} 

		if (isset($this->error['valierr_std_stall_certificate_date'])) {
			$data['valierr_std_stall_certificate_date'] = $this->error['valierr_std_stall_certificate_date'];
		} 

		if (isset($this->error['valierr_month_std_stall_certificate'])) {
			$data['valierr_month_std_stall_certificate'] = $this->error['valierr_month_std_stall_certificate'];
		} 

		if (isset($this->error['valierr_year_std_stall_certificate'])) {
			$data['valierr_year_std_stall_certificate'] = $this->error['valierr_year_std_stall_certificate'];
		} 

		if (isset($this->error['valierr_passport_no'])) {
			$data['valierr_passport_no'] = $this->error['valierr_passport_no'];
		} 

		if (isset($this->error['valierr_registerarton_authentication'])) {
			$data['valierr_registerarton_authentication'] = $this->error['valierr_registerarton_authentication'];
		} 

		if (isset($this->error['valierr_sire'])) {
			$data['valierr_sire'] = $this->error['valierr_sire'];
		} 

		if (isset($this->error['valierr_dam'])) {
			$data['valierr_dam'] = $this->error['valierr_dam'];
		} 

		if (isset($this->error['valierr_chip_no_one'])) {
			$data['valierr_chip_no_one'] = $this->error['valierr_chip_no_one'];
		} 

		if (isset($this->error['valierr_arrival_charges_to_be_paid'])) {
			$data['valierr_arrival_charges_to_be_paid'] = $this->error['valierr_arrival_charges_to_be_paid'];
		} 

		if (isset($this->error['valierr_color'])) {
			$data['valierr_color'] = $this->error['valierr_color'];
		} 

		if (isset($this->error['valierr_origin'])) {
			$data['valierr_origin'] = $this->error['valierr_origin'];
		} 

		if (isset($this->error['valierr_sex'])) {
			$data['valierr_sex'] = $this->error['valierr_sex'];
		} 

		if (isset($this->error['valierr_age'])) {
			$data['valierr_age'] = $this->error['valierr_age'];
		} 

		if (isset($this->error['valierr_stud_form'])) {
			$data['valierr_stud_form'] = $this->error['valierr_stud_form'];
		} 

		if (isset($this->error['valierr_breeder'])) {
			$data['valierr_breeder'] = $this->error['valierr_breeder'];
		} 

		if (isset($this->error['valierr_saddle_no'])) {
			$data['valierr_saddle_no'] = $this->error['valierr_saddle_no'];
		} 

		if (isset($this->error['valierr_octroi_details'])) {
			$data['valierr_octroi_details'] = $this->error['valierr_octroi_details'];
		} 

		if (isset($this->error['valierr_awbi_registration_no'])) {
			$data['valierr_awbi_registration_no'] = $this->error['valierr_awbi_registration_no'];
		} 

		if (isset($this->error['valierr_Id_by_brand'])) {
			$data['valierr_Id_by_brand'] = $this->error['valierr_Id_by_brand'];
		} 

		if (isset($this->error['valierr_std_stall_remarks'])) {
			$data['valierr_std_stall_remarks'] = $this->error['valierr_std_stall_remarks'];
		} 

		if (isset($this->error['valierr_horse_remarks'])) {
			$data['valierr_horse_remarks'] = $this->error['valierr_horse_remarks'];
		} 

		if (isset($this->error['valierr_trainer_name'])) {
			$data['valierr_trainer_name'] = $this->error['valierr_trainer_name'];
		} 

		if (isset($this->error['valierr_date_charge_trainer'])) {
			$data['valierr_date_charge_trainer'] = $this->error['valierr_date_charge_trainer'];
		} 

		if (isset($this->error['valierr_month_charge_trainer'])) {
			$data['valierr_month_charge_trainer'] = $this->error['valierr_month_charge_trainer'];
		} 

		if (isset($this->error['valierr_year_charge_trainer'])) {
			$data['valierr_year_charge_trainer'] = $this->error['valierr_year_charge_trainer'];
		} 

		if (isset($this->error['valierr_arrival_time_trainer'])) {
			$data['valierr_arrival_time_trainer'] = $this->error['valierr_arrival_time_trainer'];
		} 

		if (isset($this->error['valierr_extra_narrration_trainer'])) {
			$data['valierr_extra_narrration_trainer'] = $this->error['valierr_extra_narrration_trainer'];
		} 

		if (isset($this->error['valierr_extra_narrration_two_trainer'])) {
			$data['valierr_extra_narrration_two_trainer'] = $this->error['valierr_extra_narrration_two_trainer'];
		} 

		if (isset($this->error['valierr_date_left_trainer'])) {
			$data['valierr_date_left_trainer'] = $this->error['valierr_date_left_trainer'];
		} 

		if (isset($this->error['valierr_month_left_trainer'])) {
			$data['valierr_month_left_trainer'] = $this->error['valierr_month_left_trainer'];
		} 

		if (isset($this->error['valierr_year_left_trainer'])) {
			$data['valierr_year_left_trainer'] = $this->error['valierr_year_left_trainer'];
		} 

		if (isset($this->error['valierr_undertaking_Charge'])) {
			$data['valierr_undertaking_Charge'] = $this->error['valierr_undertaking_Charge'];
		} 

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['horse_id'])) {
			$data['action'] = $this->url->link('catalog/horse/add', 'token=' . $this->session->data['token'] . $url, true);
			$data['owners_horse_ids'] = 0;
		} else {
			$data['action'] = $this->url->link('catalog/horse/edit', 'token=' . $this->session->data['token'] . '&horse_id=' . $this->request->get['horse_id'] .  $url, true);
			$data['owners_horse_ids'] = $this->request->get['horse_id'];
			$data['change_owner_link'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . '&horse_id=' . $this->request->get['horse_id'] . '&pp=' .'Other' . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true);

		$data['arrival_time_trainer'] = array(
			'AM'  => 'AM',
			'PM'  => 'PM',
		);

		$data['regiteration_authenticaton_datas'] = array(
			'MyRC'  => 'MyRC',
			'RWITC'  => 'RWITC',
			'BTC'  => 'BTC',
			'DRC'  => 'DRC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'RCTC'  => 'RCTC',
			'OTHER'  => 'OTHER',
		);
		$data['origins'] = array(
			'Indian'  => 'Indian',
			'Foreign'  => 'Foreign',
		);

		$data['sexs'] = array(
			'm'  => 'm',
			'g'  => 'g',
			'h'  => 'h',
			'r'  => 'r',
			'f'  => 'f',
			'c'  => 'c',

		);

		$data['authorty_bans'] = array(
			'VO'  => 'VO',
			'SS'  => 'SS',
			'STIPES'  => 'STIPES',
			'BLEEDER 1'  => 'BLEEDER 1',
			'BLEEDER 2'  => 'BLEEDER 2',
			'BLEEDER 3'  => 'BLEEDER 3',
		);


		$data['clubs'] = array(
			'RWITC'  => 'RWITC',
			'RCTC'  => 'RCTC',
			'DRC'  => 'DRC',
			'BTC'  => 'BTC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'MYRC'  => 'MYRC',
		);

		$data['venus'] = array(
			'CAL'  => 'CAL',
			'DEL'  => 'DEL',
			'BNG'  => 'BNG',
			'MYS'  => 'MYS',
			'HYD'  => 'HYD',
			'MAD'  => 'MAD',
			'OTY'  => 'OTY',
		);

		$data['grads'] = array(
			'Grade 1'  => 'Grade 1',
			'Grade 2'  => 'Grade 2',
			'Grade 3'  => 'Grade 3',
		);

		$data['ownerships_types'] = array(
			'Sale'  => 'Sale',
			'Lease'  => 'Lease',
			'Sale with Contingency'  => 'Sale with Contingency',
			'Lease with Contingency'  => 'Lease with Contingency',
		);

		$data['Active'] = array('0'	=> 'Exit',
							  '1'	=> 'In');

		$types = $this->db->query("SELECT * FROM shoe WHERE 1=1 ")->rows;
		/*echo'<pre>';
		print_r($types);
		exit;*/

		$data['Type'] = array(
			'None' => 'None',
			'Aluminium' => 'Aluminium',
			'Steel' => 'Steel',
			'Unshod' => 'Unshod'
		);

		// $data['license_type1'] =  array(
		// 	'A' => 'A',
		// 	'B'   => 'B',
		// 	'' =>'None',
		// );



		if (isset($this->request->get['horse_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$horse_info = $this->model_catalog_horse->getHorsesform($this->request->get['horse_id']);
			$trainer_info_all = $this->model_catalog_horse->getTrainerAll($this->request->get['horse_id']);
			
			$trainer_info_current = $this->model_catalog_horse->getTrainerCurrent($this->request->get['horse_id']);
			$ownerinfo =  $this->model_catalog_horse->getOwnerform($this->request->get['horse_id']);
			$ownerprovis =  $this->model_catalog_horse->getOwnerprovis($this->request->get['horse_id']);
			$baninfo_with_date_end =  $this->model_catalog_horse->getBanWithdateEnd($this->request->get['horse_id']);//FOR BAN HISTORY
			$baninfo_witout_date_end =  $this->model_catalog_horse->getBanWithoutdateEnd($this->request->get['horse_id']);//FOR BAN DETAILS
			$equipment_info =  $this->model_catalog_horse->getEquipment($this->request->get['horse_id']);
			$equipment_history_info =  $this->model_catalog_horse->getEquipmentHistroyData($this->request->get['horse_id']);

			$shoe_info =  $this->model_catalog_horse->getShoie($this->request->get['horse_id']);
			$current_shoe_info =  $this->model_catalog_horse->getCurrentShoie($this->request->get['horse_id']);

			$stack_info =  $this->model_catalog_horse->getStackOt($this->request->get['horse_id']);

			$ownerhistory =  $this->model_catalog_horse->getOwnerHistoryDatas($this->request->get['horse_id']);
		}

		//echo "<pre>";print_r($this->request->post['trainer_info_history']);exit;
		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		
    	if (!empty($trainer_info_all)) {
			$data['trainer_info_all'] = $trainer_info_all;
		} 
		else {
			$data['trainer_info_all'] = array();
		}

		if (isset($this->request->post['stackesdatas'])) {
      		$data['stackoutstation_datas'] = $this->request->post['stackesdatas'];
    	} elseif (!empty($stack_info)) {
			$data['stackoutstation_datas'] = $stack_info;
		} else {
			$data['stackoutstation_datas'] = array();
		}

		if (isset($this->request->post['stacked_hidden_outstation_id'])) {
      		$data['stacked_hidden_outstation_id'] = $this->request->post['stacked_hidden_outstation_id'];
    	} elseif (!empty($stack_info)) {
			$stack_info_counts = count($stack_info);
			$data['stacked_hidden_outstation_id'] = $stack_info_counts;
		} else {
      		$data['stacked_hidden_outstation_id'] = 1;
    	}

    	if (isset($this->request->post['shoe_datas'])) {
      		$data['new_shoe_dats'] = $this->request->post['shoe_datas'];
    	} elseif (!empty($current_shoe_info)) {
			$data['new_shoe_dats'] = $current_shoe_info;
		} else {
			$data['new_shoe_dats'] = array();
		}

		$horse_codes = $this->db->query("SELECT horse_code FROM horse1 WHERE 1=1 ORDER BY horse_code DESC LIMIT 1 ");
		/*echo'<pre>';
		print_r("SELECT med_code FROM medicine WHERE 1=1 ORDER BY id DESC LIMIT 1 ");
		exit;*/
		if ($horse_codes->num_rows > 0) {
			$horse_code = $horse_codes->row['horse_code'];
		} else {
			$horse_code = '';
		}

		if (isset($this->request->post['horse_code'])) {
			$data['horse_code'] = $this->request->post['horse_code'];
		} elseif (!empty($horse_info)) {
			$data['horse_code'] = $horse_info['horse_code'];
		} elseif (isset($this->request->post['hidden_horse_code'])) {
			$data['horse_code'] = $this->request->post['hidden_horse_code'];
		} else {
			$addHcode = $horse_code + 1;
			$data['horse_code'] = $addHcode;
		}


		if (isset($this->request->post['shoe_datas_history'])) {
      		$data['history_shoe'] = $this->request->post['shoe_datas_history'];
    	} elseif (!empty($shoe_info)) {
			$data['history_shoe'] = $shoe_info;
		} else {
			$data['history_shoe'] = array();
		}

		if (isset($this->request->post['equipment_dtas'])) {
      		$data['equipment_datas'] = $this->request->post['equipment_dtas'];
    	} elseif (!empty($equipment_info)) {
			$data['equipment_datas'] = $equipment_info;
		} else {
			$data['equipment_datas'] = array();
		}

		//echo "<pre>";print_r($equipment_info);exit;

		if (isset($this->request->post['equipment_datas'])) {
      		$data['equipmentdatas'] = $this->request->post['equipment_datas'];
    	} elseif (!empty($equipment_history_info)) {
			$data['equipmentdatas'] = $equipment_history_info;
		} else {
			$data['equipmentdatas'] = array();
		}

		if (isset($this->request->post['ownerdatas'])) {
      		$data['owners_olddatas'] = $this->request->post['ownerdatas'];
    	} elseif (!empty($ownerinfo)) {
    		$parent_data = array();
    		$sub_parent_data = array();
    		$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
			if($last_colorss->num_rows > 0){
				$data['last_color'] = $last_colorss->row['horse_to_owner_id'];
			} else {
				$data['last_color'] = 0;
			}
			foreach ($ownerinfo as $okey => $ovalue) {
				$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$ovalue['horse_to_owner_id']."'");
				if($lease_table_data->num_rows > 0){
					$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;

					$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
					if($lease_parent_data->num_rows > 0){
						$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
					}
				}
				$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$ovalue['to_owner_id']."'");
				$ownerinfo[$okey]['partner_status'] = ($owners_partner->num_rows > 0) ? "1" : "0";

				if($parent_data){
					$ownerinfo[$okey]['parent_owner_name'] = $parent_data['to_owner'];
					$ownerinfo[$okey]['parent_owner_id'] = $parent_data['to_owner_id'];
				}

				if($sub_parent_data){
					$ownerinfo[$okey]['sub_parent_owner_name'] = $sub_parent_data['to_owner'];
				}

			}
			$data['owners_olddatas'] = $ownerinfo;
		} else {
      		$data['owners_olddatas'] = array();
    	}

    	if (!empty($ownerprovis)) {
    		$parent_data = array();
    		$sub_parent_data = array();
			foreach ($ownerprovis as $okey => $ovalue) {
				$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$ovalue['horse_to_owner_id']."'");
				if($lease_table_data->num_rows > 0){
					$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;

					$lease_parent_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$parent_data['horse_to_owner_id']."'");
					if($lease_parent_data->num_rows > 0){
						$sub_parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_parent_data->row['parent_trans_id']."'")->row;
					}
				}
				$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$ovalue['to_owner_id']."'");
				$ownerprovis[$okey]['partner_status'] = ($owners_partner->num_rows > 0) ? "1" : "0";

				if($parent_data){
					$ownerprovis[$okey]['parent_owner_name'] = $parent_data['to_owner'];
					$ownerprovis[$okey]['parent_owner_id'] = $parent_data['to_owner_id'];
				}

				if($sub_parent_data){
					$ownerprovis[$okey]['sub_parent_owner_name'] = $sub_parent_data['to_owner'];
				}

				$last_colorss = $this->db->query("SELECT horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_id']."' AND owner_share_status = 1 AND owner_color = 'Yes' order by horse_to_owner_id DESC ");
				if($last_colorss->num_rows > 0){
					$ownerprovis[$okey]['last_color'] = $last_colorss->row['horse_to_owner_id'];
				} else {
					$ownerprovis[$okey]['last_color'] = 0;
				}

			}
			$data['owners_provisional'] = $ownerprovis;
		} else {
      		$data['owners_provisional'] = array();
    	}

    	if (!empty($ownerhistory)) {
    		$parent_data = array();
			foreach ($ownerhistory as $okey => $ovalue) {
				$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$ovalue['horse_to_owner_id']."'");
				if($lease_table_data->num_rows > 0){
					$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
				}

				$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$ovalue['to_owner_id']."'");
				$ownerhistory[$okey]['partner_status'] = ($owners_partner->num_rows > 0) ? "1" : "0";


				if($parent_data){
					$ownerhistory[$okey]['parent_owner_name'] = $parent_data['to_owner'];
					$ownerhistory[$okey]['parent_owner_id'] = $parent_data['to_owner_id'];
				}
			}
			$data['ownerhistory'] = $ownerhistory;
		} else {
      		$data['ownerhistory'] = array();
    	}

    	

    	if (isset($this->request->post['ownership_hidden_id'])) {
      		$data['ownership_hidden_id'] = $this->request->post['ownership_hidden_id'];
    	} elseif (!empty($ownerinfo)) {
			$owner_counts = count($ownerinfo);
			$data['ownership_hidden_id'] = $owner_counts;
		} else {
      		$data['ownership_hidden_id'] = 1;
    	}

		if (isset($this->request->post['banhistorydatas'])) {
      		$data['bandatas'] = $this->request->post['banhistorydatas'];
    	} elseif (!empty($baninfo_with_date_end)) {
			$data['bandatas'] = $baninfo_with_date_end;
		} else {
      		$data['bandatas'] = array();
    	}

    	if (isset($this->request->post['bandats'])) {
      		$data['bandeatils'] = $this->request->post['bandats'];
    	} elseif (!empty($baninfo_witout_date_end)) {
			$data['bandeatils'] = $baninfo_witout_date_end;
		} else {
      		$data['bandeatils'] = array();
    	}

    	if (isset($this->request->post['id_hidden_band'])) {
      		$data['id_hidden_band'] = $this->request->post['id_hidden_band'];
    	} elseif (!empty($baninfo_witout_date_end)) {
			$ban_counts = count($baninfo_witout_date_end);
			$data['id_hidden_band'] = $ban_counts;
		} else {
      		$data['id_hidden_band'] = 1;
    	}

		if (isset($this->request->post['horse_name'])) {
			$data['horse_name'] = $this->request->post['horse_name'];
		} elseif (!empty($horse_info)) {
			$data['horse_name'] = $horse_info['official_name'];
		} else {
			$data['horse_name'] = '';
		}

		if (isset($this->request->post['horse_name'])) {
			$data['horse_name'] = $this->request->post['horse_name'];
		} elseif (!empty($horse_info)) {
			$data['horse_name'] = $horse_info['official_name'];
		} else {
			$data['horse_name'] = '';
		}

		if (isset($this->request->post['isActive'])) {
			$data['isActive'] = $this->request->post['isActive'];
		} elseif (!empty($horse_info)) {
			$data['isActive'] = $horse_info['horse_status'];
		} else {
			$data['isActive'] = '1';
		}

		if (!empty($horse_info)) {
			$data['last_upadate'] = date("d-m-Y h:i:s", strtotime($horse_info['last_update_date']));
		} else {
			$data['last_upadate'] = '';
		}

		if (!empty($horse_info) && $horse_info['user_id'] != 0) {
			$user_name = $this->db->query("SELECT username FROM oc_user WHERE user_id = '".$horse_info['user_id']."' ")->row;
			$data['user'] = $user_name['username'] ;
		} else {
			$data['user'] = '';
		}

		if (isset($this->request->post['hidden_active'])) {
			$data['isActive1'] = $this->request->post['hidden_active'];
		} elseif (!empty($horse_info)) {
			$data['isActive1'] = $horse_info['horse_status'];
		} else {
			$data['isActive1'] = '';
		}

		if (isset($this->request->post['changehorse_name'])) {
			$data['changehorse_name'] = $this->request->post['changehorse_name'];
		} elseif (!empty($horse_info)) {
			$data['changehorse_name'] = $horse_info['changehorse_name'];
		} else {
			$data['changehorse_name'] = '';
		}

		if (isset($this->request->post['official_name_change'])) {
			$data['official_name_change'] = $this->request->post['official_name_change'];
		} elseif (!empty($horse_info)) {
			$data['official_name_change'] = $horse_info['official_name_change_status'];
		} else {
			$data['official_name_change'] = 0;
		}

		if (isset($this->request->post['horse_id'])) {
			$data['horse_id'] = $this->request->post['horse_id'];
		} elseif (!empty($horse_info)) {
			$data['horse_id'] = $horse_info['horseseq'];
		} else {
			$data['horse_id'] = '';
		}

		if (isset($this->request->post['passport_no'])) {
			$data['passport_no'] = $this->request->post['passport_no'];
		} elseif (!empty($horse_info)) {
			$data['passport_no'] = $horse_info['life_no'];
		} else {
			$data['passport_no'] = '';
		}

		if (isset($this->request->post['change_horse_dates'])) {
			$data['change_horse_dates'] = $this->request->post['change_horse_dates'];
		} elseif (!empty($horse_info)) {
			if($horse_info['date_of_changehorse_name'] != '1970-01-01' && $horse_info['date_of_changehorse_name'] != '0000-00-00'){
				$data['change_horse_dates']= date('d-m-Y', strtotime($horse_info['date_of_changehorse_name']));
			} else {
			$data['change_horse_dates'] = '';
			}
		} else {
			$data['change_horse_dates'] = '';
		}

		if (isset($this->request->post['provisional'])) {
			$data['provisional'] = $this->request->post['provisional'];
		} elseif (!empty($trainer_info_current)) {
			$data['provisional'] = $trainer_info_current['provisional'];
		} else {
			$data['provisional'] = 0;
		}

		if (isset($this->request->post['registeration_date'])) {
			$data['registeration_date'] = $this->request->post['registeration_date'];
		} elseif (!empty($horse_info)) {
			if($horse_info['reg_date'] != '1970-01-01' && $horse_info['reg_date'] != '0000-00-00'){
				$data['registeration_date']= date('d-m-Y', strtotime($horse_info['reg_date']));
			} else {
			$data['registeration_date'] = '';
			}
		} else {
			$data['registeration_date'] = '';
		}

		if (isset($this->request->post['date_foal_date'])) {
			$data['date_foal_date'] = $this->request->post['date_foal_date'];
		} elseif (!empty($horse_info)) {
			if($horse_info['foal_date'] != '1970-01-01' && $horse_info['foal_date'] != '0000-00-00'){
				$data['date_foal_date']= date('d-m-Y', strtotime($horse_info['foal_date']));
			} else {
			$data['date_foal_date'] = '';
			}
		} else {
			$data['date_foal_date'] = '';
		}

		if (isset($this->request->post['date_std_stall_certificate'])) {
			$data['date_std_stall_certificate'] = $this->request->post['date_std_stall_certificate'];
		} elseif (!empty($horse_info)) {

			if($horse_info['std_stall_certificate_date'] != '1970-01-01' && $horse_info['std_stall_certificate_date'] != '0000-00-00'){
				$data['date_std_stall_certificate']= date('d-m-Y', strtotime($horse_info['std_stall_certificate_date']));
			} else {
				$data['date_std_stall_certificate'] = '';
			}
		} else {
			$data['date_std_stall_certificate'] = '';
		}

		if (isset($this->request->post['date_charge_trainer'])) {
			$data['date_charge_trainer'] = $this->request->post['date_charge_trainer'];
		} elseif (!empty($trainer_info_current)) {
			if($trainer_info_current['date_of_charge'] != '1970-01-01' && $trainer_info_current['date_of_charge'] != '0000-00-00'){
				$data['date_charge_trainer']= date('d-m-Y', strtotime($trainer_info_current['date_of_charge']));
			} else {
			$data['date_charge_trainer'] = '';
			}
		} else {
			$data['date_charge_trainer'] = '';
		}

		if (isset($this->request->post['date_left_trainer'])) {
			$data['date_left_trainer'] = $this->request->post['date_left_trainer'];
		} elseif (!empty($trainer_info_current)) {
			if($trainer_info_current['left_date_of_charge'] != '1970-01-01' && $trainer_info_current['left_date_of_charge'] != '0000-00-00'){
				$data['date_left_trainer']= date('d-m-Y', strtotime($trainer_info_current['left_date_of_charge']));
			} else {
			$data['date_left_trainer'] = '';
			}
		} else {
			$data['date_left_trainer'] = '';
		}

		if (isset($this->request->post['registerarton_authentication'])) {
			$data['registerarton_authentication'] = $this->request->post['registerarton_authentication'];
		} elseif (!empty($horse_info)) {
			$data['registerarton_authentication'] = $horse_info['reg_auth_id'];
		} else {
			$data['registerarton_authentication'] = '';
		}

		if (isset($this->request->post['sire'])) {
			$data['sire'] = $this->request->post['sire'];
		} elseif (!empty($horse_info)) {
			$data['sire'] = $horse_info['sire_name'];
		} else {
			$data['sire'] = '';
		}

		if (isset($this->request->post['dam'])) {
			$data['dam'] = $this->request->post['dam'];
		} elseif (!empty($horse_info)) {
			$data['dam'] = $horse_info['dam_name'];
		} else {
			$data['dam'] = '';
		}


		if (isset($this->request->post['color'])) {
			$data['color'] = $this->request->post['color'];
		} elseif (!empty($horse_info)) {
			$data['color'] = $horse_info['color'];
		} else {
			$data['color'] = '';
		}

		if (isset($this->request->post['origin'])) {
			$data['origin'] = $this->request->post['origin'];
		} elseif (!empty($horse_info)) {
			$data['origin'] = $horse_info['orgin'];
		} else {
			$data['origin'] = '';
		}

		// echo'<pre>';
		// print_r($horse_info['orgin']);
		// exit;
		if (isset($this->request->post['sex'])) {
			$data['sex'] = $this->request->post['sex'];
		} elseif (!empty($horse_info)) {
			$data['sex'] = $horse_info['sex'];
		} else {
			$data['sex'] = '';
		}

		/*if (isset($this->request->post['castration_date'])) {
			$data['castration_date'] = $this->request->post['castration_date'];
		} elseif (!empty($horse_info)) {
			$data['castration_date'] = date('d-m-Y', strtotime($horse_info['castration_date']));
		} else {
			$data['castration_date'] = '';
		}*/

		if (isset($this->request->post['castration_date'])) {
			$data['castration_date'] = $this->request->post['castration_date'];
		} elseif (!empty($horse_info)) {
			if($horse_info['castration_date'] != '1970-01-01' && $horse_info['castration_date'] != '0000-00-00'){
				$data['castration_date']= date('d-m-Y', strtotime($horse_info['castration_date']));
			} else {
			$data['castration_date'] = '';
			}
		} else {
			$data['castration_date'] = '';
		}

		if (isset($this->request->post['stud_form'])) {
			$data['stud_form'] = $this->request->post['stud_form'];
		} elseif (!empty($horse_info)) {
			$data['stud_form'] = $horse_info['stud_form'];
		} else {
			$data['stud_form'] = '';
		}

		if (isset($this->request->post['breeder'])) {
			$data['breeder'] = $this->request->post['breeder'];
		} elseif (!empty($horse_info)) {
			$data['breeder'] = $horse_info['breeder_name'];
		} else {
			$data['breeder'] = '';
		}

		if (isset($this->request->post['saddle_no'])) {
			$data['saddle_no'] = $this->request->post['saddle_no'];
		} elseif (!empty($horse_info)) {
			$data['saddle_no'] = $horse_info['saddle_no'];
		} else {
			$data['saddle_no'] = '';
		}

		if (isset($this->request->post['saddle_no'])) {
			$data['saddle_no'] = $this->request->post['saddle_no'];
		} elseif (!empty($horse_info)) {
			$data['saddle_no'] = $horse_info['saddle_no'];
		} else {
			$data['saddle_no'] = '';
		}

		if (isset($this->request->post['chip_no_one'])) {
			$data['chip_no_one'] = $this->request->post['chip_no_one'];
		} elseif (!empty($horse_info)) {
			$data['chip_no_one'] = $horse_info['chip_no1'];
		} else {
			$data['chip_no_one'] = '';
		}

		if (isset($this->request->post['chip_no_two'])) {
			$data['chip_no_two'] = $this->request->post['chip_no_two'];
		} elseif (!empty($horse_info)) {
			$data['chip_no_two'] = $horse_info['chip_no2'];
		} else {
			$data['chip_no_two'] = '';
		}

		if (isset($this->request->post['arrival_charges_to_be_paid'])) {
			$data['arrival_charges_to_be_paid'] = $this->request->post['arrival_charges_to_be_paid'];
		} elseif (!empty($horse_info)) {
			if($horse_info['arrival_charges_to_be_paid'] != '1970-01-01' && $horse_info['arrival_charges_to_be_paid'] != '0000-00-00'){
				$data['arrival_charges_to_be_paid']= date('d-m-Y', strtotime($horse_info['arrival_charges_to_be_paid']));
			} else {
				$data['arrival_charges_to_be_paid'] = '';
			}
		} else {
			$data['arrival_charges_to_be_paid'] = '';
		}

		if (isset($this->request->post['one_time_lavy'])) {
			$data['one_time_lavy'] = $this->request->post['one_time_lavy'];
		} elseif (!empty($horse_info)) {
			$data['one_time_lavy'] = $horse_info['one_time_lavy'];
		} else {
			$data['one_time_lavy'] = '';
		}

		if (isset($this->request->post['age'])) {
			$data['age'] = $this->request->post['age'];
		} elseif (!empty($horse_info)) {
			$data['age'] = $horse_info['age'];
		} else {
			$data['age'] = '';
		}

		if (isset($this->request->post['rating'])) {
			$data['rating'] = $this->request->post['rating'];
		} elseif (!empty($horse_info)) {
			$data['rating'] = $horse_info['rating'];
		} else {
			$data['rating'] = '';
		}

		if (isset($this->request->post['octroi_details'])) {
			$data['octroi_details'] = $this->request->post['octroi_details'];
		} elseif (!empty($horse_info)) {
			$data['octroi_details'] = $horse_info['octroi_details'];
		} else {
			$data['octroi_details'] = '';
		}

		if (isset($this->request->post['awbi_registration_no'])) {
			$data['awbi_registration_no'] = $this->request->post['awbi_registration_no'];
		} elseif (!empty($horse_info)) {
			$data['awbi_registration_no'] = $horse_info['awbi_registration_no'];
		} else {
			$data['awbi_registration_no'] = '';
		}

		if (isset($this->request->post['awbi_registration_file'])) {
			$data['awbi_registration_file'] = $this->request->post['awbi_registration_file'];
		} elseif (!empty($horse_info)) {
			$data['awbi_registration_file'] = $horse_info['awbi_registration_file'];
		} else {
			$data['awbi_registration_file'] = '';
		}

		if (isset($this->request->post['awbi_registration_file'])) {
			$data['awbi_registration_file'] = $this->request->post['awbi_registration_file'];
			$data['awbi_registration_file_source'] = $this->request->post['awbi_registration_file_source'];
		} elseif (!empty($horse_info)) {
			$data['awbi_registration_file'] = $horse_info['awbi_registration_file'];
			$data['awbi_registration_file_source'] = $horse_info['awbi_registration_file_source'];
		} else {	
			$data['awbi_registration_file'] = '';
			$data['awbi_registration_file_source'] = '';
		}

		if (isset($this->request->post['Id_by_brand'])) {
			$data['Id_by_brand'] = $this->request->post['Id_by_brand'];
		} elseif (!empty($horse_info)) {
			$data['Id_by_brand'] = $horse_info['Id_by_brand'];
		} else {
			$data['Id_by_brand'] = '';
		}

		if (isset($this->request->post['std_stall_remarks'])) {
			$data['std_stall_remarks'] = $this->request->post['std_stall_remarks'];
		} elseif (!empty($horse_info)) {
			$data['std_stall_remarks'] = $horse_info['std_stall_remarks'];
		} else {
			$data['std_stall_remarks'] = '';
		}

		if (isset($this->request->post['horse_remarks'])) {
			$data['horse_remarks'] = $this->request->post['horse_remarks'];
		} elseif (!empty($horse_info)) {
			$data['horse_remarks'] = $horse_info['horse_remarks'];
		} else {
			$data['horse_remarks'] = '';
		}

		if(isset($this->request->get['show_owner_tab'])){
			$data['show_owner_tab'] = $this->request->get['show_owner_tab'];
		} else {
			$data['show_owner_tab'] = 0;
		}

		if (isset($this->request->post['trainer_name'])) {
			$data['trainer_name'] = $this->request->post['trainer_name'];
		} elseif (!empty($trainer_info_current)) {
			$data['trainer_name'] = $trainer_info_current['trainer_name'];
		} else {
			$data['trainer_name'] = '';
		}

		if (isset($this->request->post['new_trainer_name'])) {
			$data['new_trainer_name'] = $this->request->post['new_trainer_name'];
		} else {
			$data['new_trainer_name'] = '';
		}

		if (isset($this->request->post['new_trainer_code'])) {
			$data['new_trainer_code'] = $this->request->post['new_trainer_code'];
		} else {
			$data['new_trainer_code'] = '';
		}


		if (isset($this->request->post['trainer_id'])) {
			$data['trainer_id'] = $this->request->post['trainer_id'];
		} elseif (!empty($trainer_info_current)) {
			$data['trainer_id'] = $trainer_info_current['trainer_id'];
		} else {
			$data['trainer_id'] = '';
		}

		if (isset($this->request->post['horse_to_trainer_id'])) {
			$data['horse_to_trainer_id'] = $this->request->post['horse_to_trainer_id'];
		} elseif (!empty($trainer_info_current)) {
			$data['horse_to_trainer_id'] = $trainer_info_current['horse_to_trainer_id'];
		} else {
			$data['horse_to_trainer_id'] = '';
		}

		if (isset($this->request->post['trainer_codes_id'])) {
			$data['trainer_codes_id'] = $this->request->post['trainer_codes_id'];
		} elseif (!empty($trainer_info_current)) {
			$data['trainer_codes_id'] = $trainer_info_current['trainer_code'];
		} else {
			$data['trainer_codes_id'] = '';
		}

		if (isset($this->request->post['arrival_time_trainers'])) {
			$data['arrival_time_trainers'] = $this->request->post['arrival_time_trainers'];
		} elseif (!empty($trainer_info_current)) {
			$data['arrival_time_trainers'] = $trainer_info_current['arrival_time'];
		} else {
			$data['arrival_time_trainers'] = '';
		}

		if (isset($this->request->post['extra_narrration_trainer'])) {
			$data['extra_narrration_trainer'] = $this->request->post['extra_narrration_trainer'];
		} elseif (!empty($trainer_info_current)) {
			$data['extra_narrration_trainer'] = $trainer_info_current['extra_narration'];
		} else {
			$data['extra_narrration_trainer'] = '';
		}

		if (isset($this->request->post['extra_narrration_two_trainer'])) {
			$data['extra_narrration_two_trainer'] = $this->request->post['extra_narrration_two_trainer'];
		} elseif (!empty($trainer_info_current)) {
			$data['extra_narrration_two_trainer'] = $trainer_info_current['extra_narration_two'];
		} else {
			$data['extra_narrration_two_trainer'] = '';
		}

		if (isset($this->request->post['undertaking_charge'])) {
			$data['undertaking_charge'] = $this->request->post['undertaking_charge'];
		} elseif (!empty($trainer_info_current)) {
			$data['undertaking_charge'] = $trainer_info_current['undertaking_charge'];
		} else {
			$data['undertaking_charge'] = 0;
		}

		if (isset($this->request->post['hidden_isb_horse'])) {
			$data['hidden_isb_horse'] = $this->request->post['hidden_isb_horse'];
		} else {
			$data['hidden_isb_horse'] = '';
		}
		
		$data['path'] = '';

		$this->load->model('catalog/filter');

		if (isset($this->request->post['category_filter'])) {
			$filters = $this->request->post['category_filter'];
		} elseif (isset($this->request->get['category_id'])) {
			$filters = $this->model_catalog_horse->getCategoryFilters($this->request->get['category_id']);
		} else {
			$filters = array();
		}

		$data['category_filters'] = array();

		foreach ($filters as $filter_id) {
			$filter_info = $this->model_catalog_filter->getFilter($filter_id);

			if ($filter_info) {
				$data['category_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}

		$this->load->model('setting/store');
		$product_specials = array();
		$data['product_specials'] = $product_specials;

		$product_recurrings = array();
		$data['product_recurrings'] = $product_recurrings;

		$data['recurrings'] = array();
		$data['customer_groups'] = array();
		$data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['category_store'])) {
			$data['category_store'] = $this->request->post['category_store'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_store'] = $this->model_catalog_horse->getCategoryStores($this->request->get['category_id']);
		} else {
			$data['category_store'] = array(0);
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($category_info)) {
			$data['keyword'] = $category_info['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($category_info)) {
			$data['image'] = $category_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($category_info) && is_file(DIR_IMAGE . $category_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($category_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['top'])) {
			$data['top'] = $this->request->post['top'];
		} elseif (!empty($category_info)) {
			$data['top'] = $category_info['top'];
		} else {
			$data['top'] = 0;
		}

		if (isset($this->request->post['column'])) {
			$data['column'] = $this->request->post['column'];
		} elseif (!empty($category_info)) {
			$data['column'] = $category_info['column'];
		} else {
			$data['column'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($category_info)) {
			$data['sort_order'] = $category_info['sort_order'];
		} else {
			$data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = true;
		}

		if (isset($this->request->post['category_layout'])) {
			$data['category_layout'] = $this->request->post['category_layout'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_layout'] = $this->model_catalog_horse->getCategoryLayouts($this->request->get['category_id']);
		} else {
			$data['category_layout'] = array();
		}

		if (isset($this->request->post['bit_datas'])) {
			$data['bit_datas'] = $this->request->post['bit_datas'];
		} elseif (!empty($horse_info)) {
			$bit_datass = $this->db->query("SELECT * FROM `horse_bits` WHERE `horse_id` = '".$horse_info['horseseq']."' AND end_date <> '0000-00-00' ORDER BY start_date ASC ")->rows;
			$bit_datas = array();

			foreach($bit_datass as $pkeys => $pvalues){
				
				$bit_datas[] = array(
					'short_name' => $pvalues['short_name'],
					'long_name' => $pvalues['long_name'],
					'start_date' => date("d-m-Y", strtotime($pvalues['start_date'])),
					'end_date' => date("d-m-Y", strtotime($pvalues['end_date'])),

					'status' => $pvalues['status'],
				);	
			}
			$data['bit_datas'] = $bit_datas;


			$current_bit_datass = $this->db->query("SELECT * FROM `horse_bits` WHERE `horse_id` = '".$this->request->get['horse_id']."' AND end_date = '0000-00-00' ")->rows;
			$current_bit_datas = array();

			foreach($current_bit_datass as $pkeys => $pvalues){
				
				$current_bit_datas[] = array(
					'short_name' => $pvalues['short_name'],
					'long_name' => $pvalues['long_name'],
					'start_date' => date("d-m-Y", strtotime($pvalues['start_date'])),
					'status' => $pvalues['status'],
				);	
			}
			$data['current_bit_datass'] = $current_bit_datas;
		} else {
			$data['bit_datas'] = array();
			$data['current_bit_datass'] = array();
		}

		if (isset($this->request->get['horse_id'])) {
			$horse_to_trainer_query = $this->db->query("SELECT * FROM `horse_to_trainer`WHERE horse_id = '".$this->request->get['horse_id']."' AND trainer_status = '1' ORDER BY `horse_to_trainer_id` DESC LIMIT 1 ");
			if ($horse_to_trainer_query->num_rows > 0) {
				$trainer_id_ht = $horse_to_trainer_query->row['trainer_id'];
				$data_license_type = $this->db->query("SELECT id,license_type FROM `trainers` WHERE id='".$horse_to_trainer_query->row['trainer_id']."' ");
				if ($data_license_type->num_rows > 0) {
					$data['license_type'] = $data_license_type->row['license_type'];
				} else{
					$data['license_type'] = '';
				}
				//echo "<pre>";print_r($license_type->rows);exit;
			} else{
				$data['license_type'] = '';
			}
		} else{
			$data['license_type'] = '';
		}

		if (isset($this->request->get['horse_id'])) {
			$name_registrations = $this->db->query("SELECT * FROM `oc_registration_module`WHERE horse_id = '".$this->request->get['horse_id']."' ORDER BY `id` DESC LIMIT 1 ");
			if ($name_registrations->num_rows > 0 && $name_registrations->row['modification'] == '2') {
				$data['name_registration'] = $name_registrations->row['registeration_type'];
				//echo "<pre>";print_r($data['name_registration']);exit;
			} else{
				$data['name_registration'] = '';
			}
		} else{
			$data['name_registration'] = '';
		}


		//$license_type = $this->db->query("SELECT id,license_type FROM `trainers` WHERE 1=1 ");



		


		
		// echo'<pre>';
		// print_r($data['bit_datas']);
		// exit;

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/horse_form_view', $data));
	}

	function ischecktDate($string) { 
		$matches = array();
	    $pattern = '/^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/';
	    if (!preg_match($pattern, $string, $matches)) return false;
	    if (!checkdate($matches[2], $matches[1], $matches[3])) return false;
	    return true;
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/horse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		//echo '<pre>';print_r($this->request->post);exit;
		if($this->request->post['horse_name'] == ''){ 
			$this->error['valierr_horse_name'] = 'Please Enter Hourse Name';
		}

		if($this->request->post['isActive'] == 0 && $this->request->post['horse_remarks'] == ''){ 
			$this->error['remarks_errors'] = 'Please Enter Remark';
		}

		if($this->request->post['trainer_name'] != ''){
			if($this->request->post['date_foal_date'] == '') {
				$this->error['valierr_foal_date'] = 'Please Enter Valid Date';
			}
		}

		if (!$this->ischecktDate($this->request->post['date_foal_date'])) {
			$this->error['valierr_foal_date'] = 'Please Enter Valid Date';
			$this->request->post['date_foal_date'] = '';
		}

		if($this->request->post['official_name_change'] == 1){
			if (isset($this->request->post['changehorse_name'])) {
				if($this->request->post['changehorse_name'] == ''){
					$this->error['valierr_change_horse_name'] = 'Please Enter Changes Horse Name';
				} 
			}
		}

		if($this->request->post['registerarton_authentication'] == ''){
			$this->error['valierr_registerarton_authentication'] = 'Please Select Registration Auth';
		}

		if (!isset($this->request->post['horse_id'])) {
			$sire_name = $this->db->query("SELECT sire_name FROM horse1 WHERE 1=1 ")->rows;
			foreach ($sire_name as $key => $value) {
				if ($this->request->post['sire'] == $value['sire_name']) {
					$this->error['warning'] ='This Horse is Already Exist';
					$this->error['valierr_sire'] = 'Already Exist Sire/ Nat';
				}
			}

			if($this->request->post['sire'] == ''){
				$this->error['valierr_sire'] = 'Please Enter Sire/ Nat';
			} 

			$dam_name = $this->db->query("SELECT dam_name FROM horse1 WHERE 1=1 ")->rows;

			foreach ($dam_name as $key => $value) {
				if ($this->request->post['dam'] == $value['dam_name']) {
					$this->error['warning'] ='This Horse is Already Exist';
					$this->error['valierr_dam'] = 'Already Exist Dam/ Nat';
				}
			}

			if($this->request->post['dam'] == ''){
				$this->error['valierr_dam'] = 'Please Enter Dam/ Nat';
			}
		}


		if($this->request->post['color'] == ''){
			$this->error['valierr_color'] = 'Please Enter Color';
		}

		if($this->request->post['origin'] == ''){
			$this->error['valierr_origin'] = 'Please Select Origin';
		} 

  		if($this->request->post['sex'] == ''){
			$this->error['valierr_sex'] = 'Please Select Sex';
		}

		if($this->request->post['trainer_name'] != ''){
			if($this->request->post['trainer_id'] == ''){
				$this->error['valierr_trainer_name'] = 'Please Choose Trainer Name';
			} elseif ($this->request->post['trainer_name'] != '') {
				$equipment_name = $this->db->query("SELECT name FROM `trainers` WHERE name='".$this->request->post['trainer_name']."' ");
				if ($equipment_name->num_rows == 0) {
					$this->error['valierr_trainer_name'] = 'Can\'t Select '.$this->request->post['trainer_name'].'! It is not Equipment';
				}
			}
		}

		if($this->request->post['date_charge_trainer'] != '') {
			if($this->request->post['date_charge_trainer'] == '') {
				$this->error['valierr_date_charge_trainer'] = 'Please Select Valid Date';
			}

			if (!$this->ischecktDate($this->request->post['date_charge_trainer'])) {
				$this->error['valierr_date_charge_trainer'] = 'Please Enter Valid Date';
				$this->request->post['date_charge_trainer'] = '';
			}
		} 
		

		if($this->request->post['arrival_time_trainers'] == ''){
			$this->error['valierr_arrival_time_trainer'] = 'Please Select Arrival Time';
		}

		if(isset($this->request->post['date_charge_trainer']) && ($this->ischecktDate($this->request->post['date_charge_trainer']) == 'true') && isset($this->request->post['date_left_trainer']) && $this->request->post['date_left_trainer'] != ''){
			$today1 = date('Y-m-d', strtotime($this->request->post['date_charge_trainer']));
			$today2 = date('Y-m-d', strtotime($this->request->post['date_left_trainer']));
			if($today2 < $today1){ 
				$this->error['valierr_date_left_trainer'] = 'Left Charge Must Be Greater';
			} elseif($today1 == $today2){
				$this->error['valierr_date_charge_trainer'] = 'Date Of Charge Not Be Same';
				$this->error['valierr_date_left_trainer'] = 'Left Charge Not Be Same';
			}
			if(!isset($this->request->post['undertaking_charge'])){
				$this->error['valierr_undertaking_Charge'] = 'Please Tick Undertaking Charge';
			}
		}


		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		// echo '<pre>';
		// print_r($this->error);
		return !$this->error;
		//exit;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/horse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/horse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocompleteFromOwner() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['from_owner_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesFromOwnerAuto($this->request->get['from_owner_name']);

			foreach ($results as $result) {
				$json[] = array(
					'fromowner_code' => $result['id'],
					'fromowner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['fromowner_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	

	public function autocompleteToOwner() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['to_owner_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesToOwnerAuto($this->request->get['to_owner_name']);

			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'toowner_code' => $result['id'],
						'toowner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['toowner_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function autocompleteTrainer() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['trainer_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesToTrainerAuto($this->request->get['trainer_name']);


			if($results){
				foreach ($results as $result) {
					//echo "<pre>";print_r($result);exit;
					$json[] = array(
						'trainer_code' => $result['trainer_code_new'],
						'trainer_id' => $result['id'],
						'trainer_name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
						'license_type' => $result['license_type'],
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteTrainercode(){
		//echo "<pre>";print_r($this->request->get);exit;
		$json = array();

		if (isset($this->request->get['trainer_code'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesToTrainerCode($this->request->get['trainer_code']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'trainer_code' => $result['trainer_code_new'],
						'trainer_id' => $result['id'],
						'trainer_name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		//echo '<pre>';print_r($json);exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_code'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteEquipement() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['equipment_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesequipments($this->request->get['equipment_name']);

			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'equipment_name'        => strip_tags(html_entity_decode($result['equipment_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['equipment_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}
	

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/horse');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_horse->getHorses($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function upload() {
		//$this->load->language('catalog/employee');
		$json = array();
		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/horse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$file_show_path = HTTP_CATALOG.'system/storage/download/horse/';
		$file_upload_path = DIR_DOWNLOAD.'horse/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				$this->log->write(print_r($this->request->files, true));
				$this->log->write($image_name);
				$this->log->write($img_extension);
				$this->log->write($filename);

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';
				

				$this->log->write(print_r($allowed, true));

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] ='Please upload valid file!'. $this->request->files['file']['error'];
				}
			} else {
				$json['error'] ='Please upload valid file!';
			}
		}

		if (!$json) {
			$file = $filename;
			if(move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file)){
				$destFile = $file_upload_path . $file;
				//chmod($destFile, 0777);
				$json['filename'] = $file;
				$json['link_href'] = $file_show_path . $file;

				$json['success'] = 'Your file was successfully uploaded!';
			} else {
				$json['error'] = 'Please upload valid file!';
			}
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function deletebandeatils() {
		$json = array();

		if (isset($this->request->get['hourse_id']) && isset($this->request->get['hourse_ban_id'])) {

			$this->db->query("DELETE FROM `horse_ban` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_ban_id = '".$this->request->get['hourse_ban_id']."'");
			$this->log->write("DELETE FROM `horse_ban` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_ban_id = '".$this->request->get['hourse_ban_id']."'");

			$json['success'] = 'Delete Record Sucessfully';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function deleteShoeDeatils() {
		$json = array();

		if (isset($this->request->get['hourse_id']) && isset($this->request->get['hourse_shoeing_id'])) {

			$this->db->query("DELETE FROM `horse_shoeing` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_shoeing_id = '".$this->request->get['hourse_shoeing_id']."'");
			$this->log->write("DELETE FROM `horse_shoeing` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_shoeing_id = '".$this->request->get['hourse_shoeing_id']."'");

			$json['success'] = 'Delete Record Sucessfully';
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function deleteStackDeatils() {
		$json = array();

		if (isset($this->request->get['hourse_id']) && isset($this->request->get['hourse_stack_id'])) {

			$this->db->query("DELETE FROM `horse_stackoutstation` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_stackoutstation_id = '".$this->request->get['hourse_stack_id']."'");
			$this->log->write("DELETE FROM `horse_stackoutstation` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_stackoutstation_id = '".$this->request->get['hourse_stack_id']."'");

			$json['success'] = 'Delete Record Sucessfully';
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function UpdateStatusIsbHorse() {
		$json = array();

		if (isset($this->request->get['hidden_horse_isb_id'])) {

			$this->db->query("UPDATE foal_isb SET status = '0' WHERE id = '" .(int)$this->request->get['hidden_horse_isb_id'] . "'");
			$this->log->write("UPDATE foal_isb SET status = '0' WHERE id = '" .(int)$this->request->get['hidden_horse_isb_id'] . "'");

			$json['success'] = 'Update Record Sucessfully';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getHorseIsb() {
			$json = array();

		if (isset($this->request->get['horse_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesIsbAuto($this->request->get['horse_name']);


			if($results){
				foreach ($results as $result) {
					if($result['FOALNAME']){
						$hourse_name = $result['FOALNAME'];
					} else {
						if($result['MARENAT']){
							$hourse_name =  $result['MARENAME'].'['.$result['MARENAT'].']'.' '.'/'.$result['YROFFLNG'];
						} else {
							$hourse_name =  $result['MARENAME'].'/'.$result['YROFFLNG'];
						}
					}

					if($result['SIRENAT']){
						$sir_name =  $result['SIRENAME'].'['.$result['SIRENAT'].']'.' '.'/'.$result['YROFFLNG'];
					} else {
						$sir_name =  $result['SIRENAME'].'/'.$result['YROFFLNG'];
					}

					if($result['MARENAT']){
						$dam_name =  $result['MARENAME'].'['.$result['MARENAT'].']'.' '.'/'.$result['YROFFLNG'];
					} else {
						$dam_name =  $result['MARENAME'].'/'.$result['YROFFLNG'];
					}

					$foal_date =($result['FOALDATE'] != '' && $result['FOALDATE'] != '0000-00-00') ? date('d-m-Y', strtotime($result['FOALDATE'])) : '';
					
					$json[] = array(
						'hore_isb_id' => $result['id'],
						'YROFFLNG' => $result['YROFFLNG'],
						'horse_name' => $hourse_name,
						'SIRENAME'        => $sir_name,
						'country'        => strip_tags(html_entity_decode($result['SIRENAT'], ENT_QUOTES, 'UTF-8')),
						'mother'        => $dam_name,
						'mother_country'        => strip_tags(html_entity_decode($result['MARENAT'], ENT_QUOTES, 'UTF-8')),
						'horse_color'        => strip_tags(html_entity_decode($result['HORSECOLOR'], ENT_QUOTES, 'UTF-8')),
						'horse_gender'        => strip_tags(html_entity_decode($result['HORSESEX'], ENT_QUOTES, 'UTF-8')),
						'STUDNAME'        => strip_tags(html_entity_decode($result['STUDNAME'], ENT_QUOTES, 'UTF-8')),
						'FOALDATE'        => $foal_date, 
						'MICROCHIP1'        => strip_tags(html_entity_decode($result['MICROCHIP1'], ENT_QUOTES, 'UTF-8')),
						'MICROCHIP2'        => strip_tags(html_entity_decode($result['MICROCHIP2'], ENT_QUOTES, 'UTF-8')),
						'LIFENUMBER'        => strip_tags(html_entity_decode($result['LIFENUMBER'], ENT_QUOTES, 'UTF-8')),
						'BRAND1'        => strip_tags(html_entity_decode($result['BRAND1'], ENT_QUOTES, 'UTF-8'))

					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['SIRENAME'];
		}



		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteHorse() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['trainer_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesAuto($this->request->get['trainer_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'trainer_id' => $result['horseseq'],
						'trainer_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}
	public function owner_status() {
		$datas = $this->request->post;
 	    if($datas['owners_horse_id'] != 0){
 	    	$ownership_from_date = date("Y-m-d", strtotime($datas['date_ownership']));
 	    	if($datas['end_date_ownership'] != ''){
				$ownership_date_end = date("Y-m-d", strtotime($datas['end_date_ownership']));
			} else {
				$ownership_date_end = '0000-00-00';
			}
			$parent_owner_id = 0;
			$parent_owner_id_else = 0;
			$fresh_horse_owner = 0;
			if(!isset($datas['frm_owner']) && !isset($datas['frm_owner'])){
				if(($datas['ownership_type_name'] ==  'Lease') || ($datas['ownership_type_name'] ==  'Lease with Contingency')){
					$parent_owner_id_else = $datas['to_owner_hidden'];
				}
				$fresh_horse_owner = 1;
			}elseif(($datas['ownership_type_name'] ==  'Lease') || ($datas['ownership_type_name'] ==  'Lease with Contingency')){
				$parent_owner_id_else = $datas['frm_owner_hidden'];
			}

			if($fresh_horse_owner != 1){
				$owner_exist_share_status_query = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_id = '".$datas['owners_horse_id']."'  AND from_owner_id = '".$datas['frm_owner_hidden']."' AND  owner_share_status = 1  ORDER BY horse_to_owner_id desc LIMIT 1");
	 	    	$this->log->write("SELECT * FROM `horse_to_owner` WHERE horse_id = '".$datas['owners_horse_id']."'  AND from_owner_id = '".$datas['frm_owner_hidden']."' AND  owner_share_status = 1  ORDER BY horse_to_owner_id desc LIMIT 1");
				if($owner_exist_share_status_query->num_rows > 0){
					$owner_exist_share_status = $owner_exist_share_status_query->row; 

					if(($owner_exist_share_status['owner_percentage'] >= $datas['owner_percentage'])){
						$owner_percentage = ($owner_exist_share_status['owner_percentage'] - $datas['owner_percentage']);
						if($owner_percentage >= 0) {
							if($owner_exist_share_status['from_owner_id'] == $owner_exist_share_status['to_owner_id']){
								$this->db->query("UPDATE `horse_to_owner` SET owner_share_status = '1' ,owner_percentage ='".$owner_percentage."',date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."' ,remark_horse_to_owner = '".$this->db->escape($datas['remark_description_owner'])."' , owner_color = '".$this->db->escape($datas['color_owner_jk_id'])."' WHERE horse_id = '" .(int)$owner_exist_share_status['horse_id'] . "' AND from_owner_id = '".$owner_exist_share_status['from_owner_id']."' AND to_owner_id = '".$owner_exist_share_status['to_owner_id']."'");
								$this->log->write("UPDATE `horse_to_owner` SET owner_share_status = '1' ,owner_percentage ='".$owner_percentage."',date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."' ,remark_horse_to_owner = '".$this->db->escape($datas['remark_description_owner'])."' , owner_color = '".$this->db->escape($datas['color_owner_jk_id'])."' WHERE horse_id = '" .(int)$owner_exist_share_status['horse_id'] . "' AND from_owner_id = '".$owner_exist_share_status['from_owner_id']."' AND to_owner_id = '".$owner_exist_share_status['to_owner_id']."'");
							} else {
								$ownership_date_end_minus_1_day = date('Y-m-d', strtotime('-1 day', strtotime($ownership_date_end)));
								$this->db->query("UPDATE `horse_to_owner` SET owner_share_status = '0',parent_owner_id = 0 , end_date_of_ownership ='".$ownership_date_end_minus_1_day."' WHERE horse_id = '" .(int)$owner_exist_share_status['horse_id'] . "' AND from_owner_id = '".$owner_exist_share_status['from_owner_id']."' AND to_owner_id = '".$owner_exist_share_status['to_owner_id']."'");
								$this->log->write("UPDATE `horse_to_owner` SET owner_share_status = '0',parent_owner_id = 0 , end_date_of_ownership ='".$ownership_date_end_minus_1_day."'WHERE horse_id = '" .(int)$owner_exist_share_status['horse_id'] . "' AND from_owner_id = '".$owner_exist_share_status['from_owner_id']."' AND to_owner_id = '".$owner_exist_share_status['to_owner_id']."'");


								if(($datas['ownership_type_name'] ==  'Lease') || ($datas['ownership_type_name'] ==  'Lease with Contingency')){
									$parent_owner_id = $owner_exist_share_status['from_owner_id']; 
								}
								$this->db->query("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$datas['owners_horse_id']."', from_owner = '".$this->db->escape($datas['frm_owner'])."',from_owner_id = '".$datas['frm_owner_hidden']."', to_owner = '".$this->db->escape($datas['to_owner'])."',to_owner_id = '".$datas['to_owner_hidden']."',owner_percentage ='".$datas['owner_percentage']."',ownership_type ='".$datas['ownership_type_name']."' ,date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."',remark_horse_to_owner = '".$this->db->escape($datas['remark_description_owner'])."' , owner_color = '".$this->db->escape($datas['color_owner_jk_id'])."',parent_owner_id = '".$parent_owner_id."' , owner_share_status = 1");	
								$this->db->query("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$owner_exist_share_status['horse_id']."', from_owner = '".$this->db->escape($owner_exist_share_status['from_owner'])."',from_owner_id = '".$owner_exist_share_status['from_owner_id']."', to_owner = '".$this->db->escape($owner_exist_share_status['from_owner'])."',to_owner_id = '".$owner_exist_share_status['from_owner_id']."',owner_percentage ='".$owner_percentage."',ownership_type ='".$owner_exist_share_status['ownership_type']."' ,date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."' ,remark_horse_to_owner = '".$this->db->escape($owner_exist_share_status['remark_horse_to_owner'])."' , owner_color = '".$this->db->escape($owner_exist_share_status['owner_color'])."' ,owner_share_status = 1");	
							}
						}
					} else {
						$json['warning'] = 1;
					}

				} else {
					$this->db->query("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$datas['owners_horse_id']."', from_owner = '".$this->db->escape($datas['frm_owner'])."',from_owner_id = '".$datas['frm_owner_hidden']."', to_owner = '".$this->db->escape($datas['to_owner'])."',to_owner_id = '".$datas['to_owner_hidden']."',owner_percentage ='".$datas['owner_percentage']."',ownership_type ='".$datas['ownership_type_name']."' ,date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."',remark_horse_to_owner = '".$this->db->escape($datas['remark_description_owner'])."' , owner_color = '".$this->db->escape($datas['color_owner_jk_id'])."',parent_owner_id = '".$parent_owner_id_else."' , owner_share_status = 1 ");
					$this->log->write("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$datas['owners_horse_id']."', from_owner = '".$this->db->escape($datas['frm_owner'])."',from_owner_id = '".$datas['frm_owner_hidden']."', to_owner = '".$this->db->escape($datas['to_owner'])."',to_owner_id = '".$datas['to_owner_hidden']."',owner_percentage ='".$datas['owner_percentage']."',ownership_type ='".$datas['ownership_type_name']."' ,date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."',remark_horse_to_owner = '".$this->db->escape($datas['remark_description_owner'])."' , owner_color = '".$this->db->escape($datas['color_owner_jk_id'])."',parent_owner_id = '".$parent_owner_id_else."' , owner_share_status = 1");
					
				}
			} else {
				$this->db->query("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$datas['owners_horse_id']."', from_owner = '',from_owner_id = 0, to_owner = '".$this->db->escape($datas['to_owner'])."',to_owner_id = '".$datas['to_owner_hidden']."',owner_percentage ='".$datas['owner_percentage']."',ownership_type ='".$datas['ownership_type_name']."' ,date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."',remark_horse_to_owner = '".$this->db->escape($datas['remark_description_owner'])."' , owner_color = '".$this->db->escape($datas['color_owner_jk_id'])."',parent_owner_id = '".$parent_owner_id_else."' , owner_share_status = 1 ");
				$this->log->write("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$datas['owners_horse_id']."', from_owner = '',from_owner_id = 0, to_owner = '".$this->db->escape($datas['to_owner'])."',to_owner_id = '".$datas['to_owner_hidden']."',owner_percentage ='".$datas['owner_percentage']."',ownership_type ='".$datas['ownership_type_name']."' ,date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."',remark_horse_to_owner = '".$this->db->escape($datas['remark_description_owner'])."' , owner_color = '".$this->db->escape($datas['color_owner_jk_id'])."',parent_owner_id = '".$parent_owner_id_else."' , owner_share_status = 1");
			}

 	    
		} 	
		
 	   	$json['success']= 1;
		$this->response->setOutput(json_encode($json));
	}

	public function getShoes() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		
			$json = array();

		if (isset($this->request->get['shoe_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getShoesdata($this->request->get['shoe_name']);

			if($results){
				foreach ($results as $result) {
					
					$json[] = array(
						'shoe_id' => $result['id'],
						'shoe_name' => $result['shoe_name'],
						'shoe_details' => $result['shoe_details'],
					);
				}
			}
		}
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['shoe_details'];
		}

		// echo'<pre>';
		// print_r($json);
		// exit;

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getBits() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		
			$json = array();

		if (isset($this->request->get['bits_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getBitsdata($this->request->get['bits_name']);

			if($results){
				foreach ($results as $result) {
					
					$json[] = array(
						'bit_id' => $result['id'],
						'short_name' => $result['short_name'],
						'long_name' => $result['long_name'],
					);
				}
			}
		}
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['long_name'];
		}

		// echo'<pre>';
		// print_r($json);
		// exit;

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function TrainerDetails(){
		//$json['alert'] = 0;
		//echo "<pre>";print_r($this->request->post);exit;

	 	if (isset($this->request->post) && $this->request->post['trainer_name'] != '') {
			$trainer_name = $this->db->query("SELECT name FROM `trainers` WHERE name='".$this->request->post['trainer_name']."' ");
			//echo "<pre>";print_r($trainer_name);exit;
			if ($trainer_name->num_rows == 0) {
				$json['alert'] = 'Can\'t Select trainer name!';
			} else {
				$last_trainers = $this->db->query("SELECT * FROM horse_to_trainer WHERE `horse_id` = '".$this->request->post['horse_id']."' ORDER BY horse_to_trainer_id DESC LIMIT 1 ");
				if ($last_trainers->num_rows > 0) {
					$this->db->query("UPDATE horse_to_trainer SET left_date_of_charge = '".date('Y-m-d', strtotime($this->request->post['charge_trainer']))."',undertaking_charge = '".$this->request->post['undertaking_charge']."', trainer_status = 0 WHERE horse_to_trainer_id = '".$last_trainers->row['horse_to_trainer_id']."' ");
				}

				$this->db->query("INSERT INTO `horse_to_trainer` SET
					horse_id = '" . $this->request->post['horse_id'] . "',
					trainer_name = '".$this->request->post['trainer_name']."',
					trainer_id = '".$this->request->post['trainer_id']."',
					trainer_code = '".$this->request->post['code']."',
					date_of_charge = '".date('Y-m-d', strtotime($this->request->post['charge_trainer']))."', 
					arrival_time = '".$this->request->post['arrival_time']."',
					extra_narration = '".$this->request->post['extra_narrration']."',  
					provisional = '".$this->request->post['provisional']."',
					trainer_status = '1'  
				");

				$horse_to_tr = $this->db->query("SELECT * FROM `horse_to_trainer` WHERE horse_id = '" . $this->request->post['horse_id'] . "' AND trainer_status = '0' ")->rows;
				//echo "<pre>";print_r($horse_to_tr);exit;
				$html = '';
				foreach ($horse_to_tr as $hkey => $hvalue) {
					$html .= '<tr class="tr_pre_trainers_'.$hvalue['horse_to_trainer_id'].'">';
						$html .= '<td class="text-center">'.$hvalue['trainer_name'].'</td>';
		                $html .= '<td class="text-center">'.$hvalue['date_of_charge'].'</td>';
		                $html .= '<td class="text-center">'.$hvalue['left_date_of_charge'].'</td>';
		                $html .= '<td class="text-center"></td>';
		            $html .= '</tr>';

				}

				$json['alert'] = '';
				$json['html'] = $html;
				//$json = $horse_to_tr;
			}
		} 

		//echo "<pre>";print_r($json);exit;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}




	public function getPartnersData(){

		$html = '';
		$json = array();
		$partners_datasss = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$this->request->get['parent_owner_id']."'");
		$owner_datasss = $this->db->query("SELECT `id`,`owner_name` FROM `owners` WHERE id ='".$this->request->get['parent_owner_id']."'")->row;

		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		if($partners_datasss->num_rows > 0){

			$html .= '<div class="modal fade" id="partners_data_'.$this->request->get['parent_owner_id'].'" role="dialog">';
                $html .= '<div class="modal-dialog">';
                     $html .= '<div class="modal-content">';
                        $html .= '<div class="modal-header">';
                            $html .= '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                            $html .= '<h4 class="modal-title">Partners Details</h4>';
                        $html .= '</div>';
                        $html .= '<div class="modal-body partners_div" style="height: 440px;">';
							$html .= '<table class="table table-bordered partner_tbl" id="tableid_'.$this->request->get['parent_owner_id'].'">';
								$html .= '<thead>';
								$html .= '<tr>';
									$html .= '<td style="width: 40px;text-align: center;"></td>';
									$html .= '<td style="text-align: center;vertical-align: middle;">Partners Name</td>';
								$html .= '</tr>';
								$html .= '</thead>';
								$html .= '<tbody>';
									$html .= '<span style="font-size:16px;"><b>Name : </b> '.$owner_datasss['owner_name'].'</span><br><br>';
									foreach ($partners_datasss->rows as $key => $value) {
										$represntative_datasss = $this->db->query("SELECT `represntative_id` FROM `horse_to_owner` WHERE `to_owner_id` ='".$this->request->get['parent_owner_id']."' AND `horse_id` ='".$this->request->get['horse_id']."'  AND `represntative_id` = '".$value['partner_id']."' ");
										
										$repres_status = ($represntative_datasss->num_rows > 0) ? "1" : "0";

										$html .= '<tr>';
											$html .= '<td class="p_'.$key.'" style="text-align: center;">';
												if($repres_status == 1){
													$html .= '<input type="checkbox" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][is_reprentative]"  class="parent_value" id="owner_partner_id_'.$key.'" checked="checked />';

												} else {

													$html .= '<input type="checkbox" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][is_reprentative]"  class="parent_value" id="owner_partner_id_'.$key.'" />';
												}
												$html .= '<input type="hidden" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][partner_id]" value="'.$value['partner_id'].'"  id="partner_id_'.$key.'"  />';
												$html .= '<input type="hidden" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][owner_id]" value="'.$owner_datasss['id'].'"  id="partner_ids_'.$key.'"  />';

											$html .= '</td>';
											$html .= '<td class="p_'.$key.'" style="text-align: left;">';
												$html .= '<span style="font-size: 14px;"> '.$value['partner_name'] .'</span>';
											$html .= '</td>';
										$html .= '</tr>';
									}
								$html .= '</tbody>';
							$html .= '</table>';
						$html .= '</div>';
                        $html .= '<div class="modal-footer">';
                            $html .= '<button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>';
                        $html .= '</div>';
                    $html .= '</div>';
                $html .= '</div>';
           $html .=  '</div>';
		}
		$json = $html;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));


	}


	public function getEquipmentData() {

		$html = '';
		$json = array();
		$equ_datasss = $this->db->query("SELECT * FROM `horse_equipments` WHERE `horse_equipments_id` ='".$this->request->get['equ_id']."'")->row;
		
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$html .= '<div class="modal fade" id="equ_data_'.$this->request->get['equ_id'].'" role="dialog">';
            $html .= '<div class="modal-dialog">';
               	$html .= '<div class="modal-content">';
                    $html .= '<div class="modal-header">';
                        $html .= '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                        $html .= '<h4 class="modal-title">Edit Equipment</h4>';
                    $html .= '</div>';
                    $html .= '<div class="modal-body equip_div" >';
							
						$html .= '<div class="form-group">';
							$html .= '<label class="col-sm-2 control-label" for="input-club">Equipment Name</label>';
							$html .= '<div class="col-sm-10">';
								$html .= '<input type="text" name="equipment"  placeholder="Equipment" id="equipment_'.$this->request->get['equ_id'].'" value="'.$equ_datasss['equipment_name'].'" class="form-control" />';
								$html .= '<span style="display: none;color: red;font-weight: bold;" id="error_equipement" class="error">Please Enter Equipment</span>';
							$html .= '</div>';
						$html .= '</div>';

						$equ_start_date = date('d-m-Y', strtotime($equ_datasss['equipment_date']));
						// echo'<pre>';
						// print_r($equ_start_date);exit;
						$html .= '<div class="form-group">';
							$html .= '<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b>Start Date</label>';
							$html .= '<div class="col-sm-8">';
								$html .= '<div class="input-group date input-equ_start_date">';
									$html .= '<input type="text" name="change_horse_date"  data-index="4"  value="'.$equ_start_date.'" placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="equ_start_date_'.$this->request->get['equ_id'].'" class="form-control" />';
									$html .= '<span class="input-group-btn">';
										$html .= '<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>';
									$html .= '</span>';
								$html .= '</div>';
								$html .= '<span style="display: none;color: red;font-weight: bold;" id="error_equ_start_date" class="error">Please Select Valid Date</span>';
							$html .= '</div>';
						$html .= '</div>';

						if($equ_datasss['equipment_end_date'] != '0000-00-00'){
							$equ_end_date = date('d-m-Y', strtotime($equ_datasss['equipment_end_date']));
						} else {
							$equ_end_date = '';
						}


						$html .= '<div class="form-group">';
							$html .= '<label class="col-sm-2 control-label" for="input-horse"><b style="color: red">*</b>End Date</label>';
							$html .= '<div class="col-sm-8">';
								$html .= '<div class="input-group date input-equ_end_date">';
									$html .= '<input type="text" name="equ_end_date" value="'.$equ_end_date.'" data-index="4"  placeholder="DD-MM-YYYY" data-date-format="DD-MM-YYYY" id="equ_end_date_'.$this->request->get['equ_id'].'" class="form-control" />';
									$html .= '<span class="input-group-btn">';
										$html .= '<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>';
									$html .= '</span>';
								$html .= '</div>';
								$html .= '<span style="display: none;color: red;font-weight: bold;" id="error_date_choose_date" class="error">Please Select Valid Date</span>';
							$html .= '</div>';
						$html .= '</div>';

						$html .= '<div class="form-group">';
							$html .= '<label class="col-sm-2 control-label" for="input-club">Reason</label>';
							$html .= '<div class="col-sm-10">';
								$html .= '<input type="text" name="equ_reason"  placeholder="Reason" id="equ-reason_'.$this->request->get['equ_id'].'" value="'.$equ_datasss['reason'].'" class="form-control" />';
							$html .= '</div>';
						$html .= '</div>';

					$html .= '</div>';
                        $html .= '<div class="modal-footer">';
                            $html .= '<button type="button" class="btn btn-primary" onclick="updateEquipmentData('.$this->request->get['equ_id'].')">Save</button>';
                        $html .= '</div>';
                    $html .= '</div>';
                $html .= '</div>';
           $html .=  '</div>';
        $html .=  '</div>';

		$json = $html;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));


	}

	public function getEquipmentName(){
		$json = array();
		$name = array();
		$json['status'] = 0;
		$html = '';
		if($this->request->get['equ_name'] != ''){
			$e_names = $this->db->query("SELECT * FROM `horse_equipments` WHERE horse_id = '".$this->request->get['horse_id']."'AND equipment_status = '1' ");
			//echo "<pre>";print_r($e_names);exit;
			if($e_names->num_rows > 0){
				foreach ($e_names->rows as $key => $value) {
					if($value['equipment_name'] == 'VISOR'){
						$json['isequipment'] = 1;
					} elseif ($value['equipment_name'] == 'BLINKERS') {
						$json['isequipment'] = 1;
					} else {
						$json['isequipment'] = 0;
					}
				}
			} else {
				$json['isequipment'] = 0;
			}

			if($e_names->num_rows > 0){
				foreach ($e_names->rows as $key => $value) {
					if($value['equipment_name'] == 'EYE SHEILD (LEFT)'){
						$json['eyesheild'] = 1;
					} elseif ($value['equipment_name'] == 'EYE SHEILD (RIGHT)') {
						$json['eyesheild'] = 1;
					} else {
						$json['eyesheild'] = 0;
					}
				}
			} else {
				$json['eyesheild'] = 0;
			}

			// $e_names = $this->db->query("SELECT * FROM `equipment` WHERE 1=1  ");
			// if($e_names->num_rows > 0){
			// 	foreach ($e_names->rows as $key => $value) {
			// 		if($value['equipment_name'] == $this->request->get['equ_name']){
			// 			$equ_names = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` ='".$this->request->get['equ_name']."' AND horse_id = '".$this->request->get['horse_id']."' AND equipment_status = '1' ");

			// 			//echo "<pre>";print_r($equ_names);exit;
			// 			$json['status'] = ($equ_names->num_rows > 0) ? '1' : '0';
			// 		} else {
			// 			$json['isequipment'] = 0;
			// 		}
			// 	}
			// }


			//echo "<pre>";print_r($e_names);
			
			
			$equ_names = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` ='".$this->request->get['equ_name']."' AND horse_id = '".$this->request->get['horse_id']."' AND equipment_status = '1' ");

			//echo "<pre>";print_r($equ_names);exit;
			$json['status'] = ($equ_names->num_rows > 0) ? '1' : '0';

			//$json['e_name'] = $equ_names->row['equipment_name']; 	

			$all_prevs_equipments = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` ='".$this->request->get['equ_name']."' AND horse_id = '".$this->request->get['horse_id']."' AND equipment_status = '0' AND `equipment_end_date` <> '0000-00-00' ORDER BY `equipment_end_date` DESC ");

			if($all_prevs_equipments->num_rows > 0){
				$html .= '<table class="table table-bordered pre_equip">';
					$html .= '<thead>';
					$html .= '<tr>';
						$html .= '<td style="text-align: center;">Equipment Name</td>';
						$html .= '<td style="text-align: center;vertical-align: middle;">Start Date</td>';
						$html .= '<td style="text-align: center;vertical-align: middle;">End Date</td>';
					$html .= '</tr>';
					$html .= '</thead>';
					$html .= '<tbody>';
						foreach ($all_prevs_equipments->rows as $key => $value) {
							$date_start = date('d-m-Y', strtotime($value['equipment_date']));
							$date_end = date('d-m-Y', strtotime($value['equipment_end_date']));
							$html .= '<tr>';
								$html .= '<td style="text-align: left;">'.$value['equipment_name'].'</td>';
								$html .= '<td style="text-align: center;">'.$date_start.'</td>';
								$html .= '<td style="text-align: center;">'.$date_end.'</td>';
									
							$html .= '</tr>';
						}
						//exit;
					$html .= '</tbody>';
				$html .= '</table>';
			}
		}
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function updateEquipment(){
		$json['status'] = 0;
		$current_dates = date('Y-m-d');
		if($this->request->get['end_date'] != '' && $this->request->get['end_date'] != '0000-00-00'){
			$date_end = date('Y-m-d', strtotime($this->request->get['end_date']));
			if($date_end <= $current_dates){
				 $equipment_status = 0;
			} else {
				 $equipment_status = 1;

			}
			$this->db->query("UPDATE `horse_equipments` SET equipment_end_date = '".$this->db->escape($date_end)."', equipment_status = '".$equipment_status."'  WHERE `horse_equipments_id` = '".$this->request->get['equ_id']."'");

			$json['status'] = 1;
		}
		//echo $status;exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addNewEquipment(){
		//echo "<pre>";print_r($this->request->post);exit;
		$json['status'] = 0;
		if($this->request->post['start_date'] != '' && $this->request->post['name'] != ''){
			// $enames = $this->db->query("SELECT * FROM `horse_equipments` WHERE horse_id = '" .$this->request->get['horse_id'] . "' ");

			// echo "<pre>";print_r($enames);exit;
			$date_start = date('Y-m-d', strtotime($this->request->post['start_date']));

			$this->db->query("INSERT INTO `horse_equipments` SET horse_id = '".$this->request->get['horse_id']."', equipment_name = '". $this->db->escape($this->request->post['name']) ."',  equipment_status = '1', equipment_date = '".$this->db->escape($date_start)."',reason = '". $this->db->escape($this->request->post['reason']) ."' ");

			$json['status'] = 1;
		}
		//echo $status;exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addNewBan(){
		
		//echo "<pre>";print_r($this->request->post);exit;

		if (isset($this->request->post)) {
			$authority_exist = $this->db->query("SELECT authority FROM `horse_ban` WHERE horse_id = '" .$this->request->get['horse_id'] . "' AND authority = '".$this->request->post['authority_ban']."' ");
			$club_exist = $this->db->query("SELECT authority FROM `horse_ban` WHERE horse_id = '" .$this->request->get['horse_id'] . "' AND club_ban =  '".$this->request->post['club']."' ");
			if($authority_exist->num_rows > 0 || $club_exist->num_rows > 0){ 
				if (isset($this->request->post['idban_increment_id'])) {
					$json['alert'] = '';
					$start_date_bans = date("Y-m-d", strtotime($this->request->post['date_start_date_ban']));
					if($this->request->post['date_end_date_ban'] != ''){
						$end_date_ban = date("Y-m-d", strtotime($this->request->post['date_end_date_ban']));
					} else {
						$end_date_ban = '0000-00-00';
					}

					if(isset($this->request->post['idban_increment_id'])){
						if($this->request->post['idban_increment_id'] != '0'){
							$ban_exist_sql = $this->db->query("SELECT * FROM `horse_ban` WHERE horse_id = '" .$this->request->get['horse_id'] . "' AND horse_ban_id = '".$this->request->post['idban_increment_id']."' ");
							$authority_exist = $this->db->query("SELECT authority FROM `horse_ban` WHERE horse_id = '" .$this->request->get['horse_id'] . "' AND authority = '".$this->request->post['authority_ban']."' AND club_ban =  '".$this->request->post['club']."' ");

							if($authority_exist->num_rows > 0 ){ 
								//$json['alert'] = 'Can\'t Select Authority! Already Exist';
								$this->db->query("UPDATE `horse_ban` SET horse_id = '".$this->request->get['horse_id']."',startdate_ban = '".$start_date_bans."',enddate_ban = '".$end_date_ban."',reason_ban = '".$this->db->escape($this->request->post['reason_bans'])."' WHERE horse_ban_id = '".$this->request->post['idban_increment_id']."' ");
							} else {
								$this->db->query("UPDATE `horse_ban` SET horse_id = '".$this->request->get['horse_id']."',startdate_ban = '".$start_date_bans."',enddate_ban = '".$end_date_ban."', reason_ban = '".$this->db->escape($this->request->post['reason_bans'])."' WHERE horse_ban_id = '".$this->request->post['idban_increment_id']."' ");
							}
						}
					}
				} else{
					$json['alert'] = 'Can\'t Select Club Or Authority! Already Exist';
				}
			} else{
				$json['alert'] = '';
				$start_date_bans = date("Y-m-d", strtotime($this->request->post['date_start_date_ban']));
				if($this->request->post['date_end_date_ban'] != ''){
					$end_date_ban = date("Y-m-d", strtotime($this->request->post['date_end_date_ban']));
				} else {
					$end_date_ban = '0000-00-00';
				}

				// if(isset($this->request->post['idban_increment_id'])){
				// 	if($this->request->post['idban_increment_id'] != '0'){
				// 		$ban_exist_sql = $this->db->query("SELECT * FROM `horse_ban` WHERE horse_id = '" .$this->request->get['horse_id'] . "' AND horse_ban_id = '".$this->request->post['idban_increment_id']."' ");
				// 		if($ban_exist_sql->num_rows > 0){ 
				// 			$this->db->query("DELETE FROM `horse_ban` WHERE horse_id = '" .$this->request->get['horse_id'] . "' AND horse_ban_id = '".$this->request->post['idban_increment_id']."'");
				// 			$this->log->write("DELETE FROM `horse_ban` WHERE horse_id = '" .$this->request->get['horse_id'] . "' AND horse_ban_id = '".$this->request->post['idban_increment_id']."'");
				// 		}
				// 	}
				// }
				if(!isset($this->request->post['idban_increment_id'])){
					$this->db->query("INSERT INTO `horse_ban` SET horse_id = '".$this->request->get['horse_id']."', club_ban = '".$this->db->escape($this->request->post['club'])."',startdate_ban = '".$start_date_bans."',enddate_ban = '".$end_date_ban."', authority = '".$this->db->escape($this->request->post['authority_ban'])."', reason_ban = '".$this->db->escape($this->request->post['reason_bans'])."'");
				} else {
					$json['alert'] = 'Can\'t Change club Or Authority!';
				}
				// $json['status'] = 0;
				// if($this->request->post['start_date'] != '' && $this->request->post['name'] != ''){
				// 	$date_start = date('Y-m-d', strtotime($this->request->post['start_date']));

				// 	$this->db->query("INSERT INTO `horse_equipments` SET horse_id = '".$this->request->get['horse_id']."', equipment_name = '". $this->db->escape($this->request->post['name']) ."',  equipment_status = '1', equipment_date = '".$this->db->escape($date_start)."'");

				// 	$json['status'] = 1;
				// }
				// //echo $status;exit;
				$json['success']= 1;
			}

			//echo "<pre>";print_r($authority_exist);exit;
		}

		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function UpdateBanData(){
		$json['id'] = '';
		// echo "<pre>";print_r($this->request->post);
		// echo "<pre>";print_r($this->request->get);exit;
		$start_date_bans = date("Y-m-d", strtotime($this->request->post['start_date_ban']));
		if($this->request->post['end_date_ban'] != ''){
			$end_date_ban = date("Y-m-d", strtotime($this->request->post['end_date_ban']));
		} else {
			$end_date_ban = '0000-00-00';
		}
		if(isset($this->request->post['increment_ban_id'])){
			if($this->request->post['increment_ban_id'] != '0'){
				$ban_exist_sql = $this->db->query("SELECT * FROM `horse_ban` WHERE horse_id = '" .$this->request->get['horse_id'] . "' AND horse_ban_id = '".$this->request->post['increment_ban_id']."' ");
				// if($ban_exist_sql->num_rows > 0){ 
				// 	$this->db->query("DELETE FROM `horse_ban` WHERE horse_id = '" .$this->request->get['horse_id'] . "' AND horse_ban_id = '".$this->request->post['increment_ban_id']."'");
				// 	$this->log->write("DELETE FROM `horse_ban` WHERE horse_id = '" .$this->request->get['horse_id'] . "' AND horse_ban_id = '".$this->request->post['increment_ban_id']."'");
				// }
				$json['id'] = $this->request->post['increment_ban_id'];
			}
		}
		// $this->db->query("INSERT INTO `horse_ban` SET horse_id = '".$this->request->get['horse_id']."', club_ban = '".$this->db->escape($this->request->post['club_ban'])."',startdate_ban = '".$start_date_bans."',enddate_ban = '".$end_date_ban."', authority = '".$this->db->escape($this->request->post['authority_ban'])."', reason_ban = '".$this->db->escape($this->request->post['reason_ban'])."'");
		// $this->log->write("INSERT INTO `horse_ban` SET horse_id = '".$this->request->get['horse_id']."', club_ban = '".$this->db->escape($this->request->post['club_ban'])."',startdate_ban = '".$start_date_bans."',enddate_ban = '".$end_date_ban."', authority = '".$this->db->escape($this->request->post['authority_ban'])."', reason_ban = '".$this->db->escape($this->request->post['reason_ban'])."'");
		// $start_date_bans = date("Y-m-d", strtotime($this->request->post['date_start_date_ban']));
		// if($this->request->post['date_end_date_ban'] != ''){
		// 	$end_date_ban = date("Y-m-d", strtotime($this->request->post['date_end_date_ban']));
		// } else {
		// 	$end_date_ban = '0000-00-00';
		// }

		// $this->db->query("INSERT INTO `horse_ban` SET horse_id = '".$this->request->get['horse_id']."', club_ban = '".$this->db->escape($this->request->post['club'])."',startdate_ban = '".$start_date_bans."',enddate_ban = '".$end_date_ban."', authority = '".$this->db->escape($this->request->post['authority_ban'])."', reason_ban = '".$this->db->escape($this->request->post['reason_bans'])."'");
		// $json['status'] = 0;
		// if($this->request->post['start_date'] != '' && $this->request->post['name'] != ''){
		// 	$date_start = date('Y-m-d', strtotime($this->request->post['start_date']));

		// 	$this->db->query("INSERT INTO `horse_equipments` SET horse_id = '".$this->request->get['horse_id']."', equipment_name = '". $this->db->escape($this->request->post['name']) ."',  equipment_status = '1', equipment_date = '".$this->db->escape($date_start)."'");

		// 	$json['status'] = 1;
		// }
		// //echo $status;exit;
		//$json['success']= 1;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function addNewShoes(){

		$json['status'] = 0;
		if($this->request->post['start_date'] != '' && $this->request->post['type_of_shoe'] != ''){
			//echo "<pre>";print_r($this->request->post);exit;
			$shoe_name = $this->db->query("SELECT shoe_name FROM `shoe` WHERE shoe_name='".$this->request->post['description_of_shoe']."' ");
			//echo "<pre>";print_r($shoe_name);exit;
			if ($shoe_name->num_rows == 0) {
				$json['shoeName'] = 0;
			} else{
				$json['shoeName'] = 1;
				$date_start = date('Y-m-d', strtotime($this->request->post['start_date']));

				$old_shoe_datas = $this->db->query("SELECT horse_shoeing_id FROM `horse_shoeing` WHERE `horse_id` ='".$this->request->get['horse_id']."' AND shoe_end_date = '0000-00-00' ");
				$old_shoe_data_id = ($old_shoe_datas->num_rows > 0) ? $old_shoe_datas->row['horse_shoeing_id'] : 0;

				if($old_shoe_data_id != 0){
					$this->db->query("UPDATE `horse_shoeing` SET shoe_end_date = '".$this->db->escape($date_start)."'  WHERE `horse_shoeing_id` = '".$old_shoe_data_id."'");
				}

				$this->db->query("INSERT INTO `horse_shoeing` SET horse_id = '".$this->request->get['horse_id']."', type = '". $this->db->escape($this->request->post['type_of_shoe']) ."',  full_form = '".$this->db->escape($this->request->post['fullform'])."', shoe_description_id = '".$this->db->escape($this->request->post['shoe_description_id'])."', shoe_description = '".$this->db->escape($this->request->post['description_of_shoe'])."',  shoe_start_date = '".$this->db->escape($date_start)."'");

				$json['status'] = 1;
			}
		}
		//echo $status;exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function UpdateShoes(){

		$json['status'] = 0;
		if($this->request->post['start_date'] != '' && $this->request->post['type_of_shoe'] != ''){
			//echo "<pre>";print_r($this->request->post);exit;
			$shoe_name = $this->db->query("SELECT shoe_name FROM `shoe` WHERE shoe_name='".$this->request->post['description_of_shoe']."' ");
			//echo "<pre>";print_r($shoe_name);exit;
			if ($shoe_name->num_rows == 0) {
				$json['shoeName'] = 0;
			} else{
				$json['shoeName'] = 1;
				$date_start = date('Y-m-d', strtotime($this->request->post['start_date']));

				$old_shoe_datas = $this->db->query("SELECT * FROM `horse_shoeing` WHERE `horse_shoeing_id` ='".$this->request->get['horse_shoeing_id']."' ");
				//echo "<pre>";print_r($old_shoe_datas);exit;
			
				$this->db->query("UPDATE `horse_shoeing` SET 
					type = '".$this->request->post['type_of_shoe']."',
					full_form = '".$this->request->post['fullform']."',
					shoe_description = '".$this->request->post['description_of_shoe']."',
					shoe_description_id = '".$this->request->post['shoe_description_id']."',
					shoe_start_date = '".$this->db->escape($date_start)."'
					WHERE `horse_shoeing_id` = '".$this->request->get['horse_shoeing_id']."'");
				$json['status'] = 1;
			}
		}
		//echo $status;exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addNewBit(){
		$json['status'] = 0;
		if($this->request->post['start_date'] != '' && $this->request->post['bit_name'] != ''){
			//echo "<pre>";print_r($this->request->post);exit;
			$bit_name = $this->db->query("SELECT short_name FROM `bit` WHERE short_name='".$this->request->post['bit_name']."' ");
			//echo "<pre>";print_r($bit_name);exit;
			if ($bit_name->num_rows == 0) {
				$json['bitName'] = 0;
			} else {
				$json['bitName'] = 1;
				$date_start = date('Y-m-d', strtotime($this->request->post['start_date']));

				$old_bit_datas = $this->db->query("SELECT id FROM `horse_bits` WHERE `horse_id` ='".$this->request->get['horse_id']."' AND end_date = '0000-00-00' ");
				$old_bit_data_id = ($old_bit_datas->num_rows > 0) ? $old_bit_datas->row['id'] : 0;

				if($old_bit_data_id != 0){
					$this->db->query("UPDATE `horse_bits` SET end_date = '".$this->db->escape($date_start)."'  WHERE `id` = '".$old_bit_data_id."'");
				}

				$this->db->query("INSERT INTO `horse_bits` SET horse_id = '".$this->request->get['horse_id']."', short_name = '". $this->db->escape($this->request->post['bit_name']) ."',  long_name = '".$this->db->escape($this->request->post['description_bit'])."', bit_id = '".$this->db->escape($this->request->post['bit_id'])."', start_date = '".$this->db->escape($date_start)."'");

				$json['status'] = 1;
			}

			
		}
		//echo $status;exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function UpdateBits(){

		$json['status'] = 0;
		if($this->request->post['start_date'] != '' && $this->request->post['short_name'] != ''){
			//echo "<pre>";print_r($this->request->post);exit;
			$bit_name = $this->db->query("SELECT short_name FROM `bit` WHERE short_name='".$this->request->post['short_name']."' ");
			//echo "<pre>";print_r($bit_name);exit;
			if ($bit_name->num_rows == 0) {
				$json['bitName'] = 0;
			} else{
				$json['bitName'] = 1;
				$date_start = date('Y-m-d', strtotime($this->request->post['start_date']));

				$old_bit_datas = $this->db->query("SELECT * FROM `horse_bits` WHERE `id` ='".$this->request->get['auto_id']."' ");
				//echo "<pre>";print_r($old_bit_datas);exit;
			
				$this->db->query("UPDATE `horse_bits` SET 
					bit_id = '".$this->request->post['bit_id']."',
					short_name = '".$this->request->post['short_name']."',
					long_name = '".$this->request->post['long_name']."',
					start_date = '".$this->db->escape($date_start)."'
					WHERE `id` = '".$this->request->get['auto_id']."'");
				$json['status'] = 1;
			}
		}
		//echo $status;exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function saveGeneralDatas(){
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$this->load->model('catalog/horse');

		if ($this->request->get['horse_id'] != '') {
			$this->model_catalog_horse->editCategory($this->request->get['horse_id'] ,$this->request->post);
		} else{
			$this->model_catalog_horse->addHorse($this->request->post);
		}
		
		$json['success']= 1;


		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function checkEquip(){
		//echo "<pre>";print_r($this->request->get);exit;

		if ($this->request->get['check_equip']) {
			$check_equip = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` = '".$this->request->get['check_equip']."'AND horse_id = '".$this->request->get['horse_id']."' AND `equipment_status` = 1 ");
			if ($check_equip->num_rows > 0) {
				$json['duplicate'] = 1;
			} else {
				$json['duplicate'] = 0;
			}
			

			if ($this->request->get['check_equip'] == 'BLINKERS') {
				$check_alternate = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` = 'PACIFIER BLINKER' AND `equipment_status` = 1 AND horse_id = '".$this->request->get['horse_id']."' ");
				if ($check_alternate->num_rows > 0) {
					$json['alert'] = 'Can\'t Select BLINKERS! PACIFIER BLINKER Already Exist';
				} else {
					$json['alert'] = '';
				}
			} elseif ($this->request->get['check_equip'] == 'PACIFIER BLINKER') {
				$check_alternate = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` = 'PACIFIER BLINKER' AND `equipment_status` = 1 AND horse_id = '".$this->request->get['horse_id']."' ");

				$check_alternate_1 = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` = 'PACIFIER' AND `equipment_status` = 1 AND horse_id = '".$this->request->get['horse_id']."' ");

				$check_alternate_2 = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` = 'BLINKERS' AND `equipment_status` = 1 AND horse_id = '".$this->request->get['horse_id']."' ");

				if ($check_alternate->num_rows > 0) {
					$json['alert'] = 'Can\'t Select PACIFIER BLINKER! PACIFIER BLINKER Already Exist';
				} elseif ($check_alternate_1->num_rows > 0) {
					$json['alert'] = 'Can\'t Select PACIFIER BLINKER! PACIFIER Already Exist';
				} elseif ($check_alternate_2->num_rows > 0) {
					$json['alert'] = 'Can\'t Select PACIFIER BLINKER! BLINKERS Already Exist';
				} else {
					$json['alert'] = '';
				}
			}
			elseif ($this->request->get['check_equip'] == 'PACIFIER') {
				$check_alternate = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` = 'PACIFIER BLINKER' AND `equipment_status` = 1 AND horse_id = '".$this->request->get['horse_id']."' ");
				if ($check_alternate->num_rows > 0) {
					$json['alert'] = 'Can\'t Select PACIFIER! PACIFIER BLINKERS Already Exist';
				} else {
					$json['alert'] = '';
				}
			}  elseif ($this->request->get['check_equip'] == 'EYE SHEILD (LEFT)') {
				$check_alternate = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` = 'EYE SHEILD (RIGHT)' AND `equipment_status` = 1 AND horse_id = '".$this->request->get['horse_id']."' ");
				if ($check_alternate->num_rows > 0) {
					$json['alert'] = 'Can\'t Select EYE SHEILD (LEFT)! EYE SHEILD (RIGHT) Already Exist';
				} else {
					$json['alert'] = '';
				}
			} elseif ($this->request->get['check_equip'] == 'EYE SHEILD (RIGHT)') {
				$check_alternate = $this->db->query("SELECT * FROM `horse_equipments` WHERE `equipment_name` = 'EYE SHEILD (LEFT)' AND `equipment_status` = 1 AND horse_id = '".$this->request->get['horse_id']."' ");
				if ($check_alternate->num_rows > 0) {
					$json['alert'] = 'Can\'t Select EYE SHEILD (RIGHT)! EYE SHEILD (LEFT) Already Exist';
				} else {
					$json['alert'] = '';
				}
			} elseif ($this->request->get['check_equip'] != '') {
				$equipment_name = $this->db->query("SELECT equipment_name FROM `equipment` WHERE equipment_name='".$this->request->get['check_equip']."' ");
				if ($equipment_name->num_rows == 0) {
					$json['alert'] = 'Can\'t Select '.$this->request->get['check_equip'].'! It is not Equipment';
				} else {
					$json['alert'] = '';
				}
			} else {

				$json['alert'] = '';
			}

			
		}

		//echo $status;exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function UpdateShoesData(){
		//echo "<pre>";print_r($this->request->get);exit;

		$json = array();

		if (isset($this->request->get['shoeing_id'])) {
			$results = $this->db->query("SELECT * FROM `horse_shoeing` WHERE horse_shoeing_id='".$this->request->get['shoeing_id']."' ")->row;
			//echo "<pre>";print_r($results);exit;
			if($results){
				$json = array(
					'horse_shoeing_id' => $results['horse_shoeing_id'],
					'horse_id' => $results['horse_id'],
					'type' => $results['type'],
					'full_form' => $results['full_form'],
					'shoe_description' => $results['shoe_description'],
					'shoe_description_id' => $results['shoe_description_id'],
					'shoe_start_date' => date('d-m-Y', strtotime($results['shoe_start_date'])),
					'shoe_end_date' => $results['shoe_end_date'],
					'bit_name' => $results['bit_name'],
					'bit_description' => $results['bit_description'],
					'shoe_and_bit_date' => $results['shoe_and_bit_date'],
					'shoe_and_bit_status' => $results['shoe_and_bit_status'],
				);
			}
		}//echo "<pre>";print_r($json);exit;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function UpdateBitData(){
		//echo "<pre>";print_r($this->request->get);exit;

		$json = array();

		if (isset($this->request->get['bit_id'])) {
			$results = $this->db->query("SELECT * FROM `horse_bits` WHERE id='".$this->request->get['bit_id']."' ")->row;
			//echo "<pre>";print_r($results);exit;
			if($results){
				$json = array(
					'id' => $results['id'],
					'horse_id' => $results['horse_id'],
					'bit_id' => $results['bit_id'],
					'short_name' => $results['short_name'],
					'long_name' => $results['long_name'],
					'start_date' => date('d-m-Y', strtotime($results['start_date'])),
					'end_date' => $results['end_date'],
					'status' => $results['status'],
				);
			}
		}//echo "<pre>";print_r($json);exit;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function UpdateTrainerData(){
		$json = array();
		//echo "<pre>";print_r($this->request->get);exit;


		if (isset($this->request->get['horse_trainer_id'])) {
			$results = $this->db->query("SELECT * FROM `horse_to_trainer` WHERE horse_to_trainer_id='".$this->request->get['horse_trainer_id']."' ")->row;
			//echo "<pre>";print_r($results);exit;
			if($results){
				$json = array(
					'horse_to_trainer_id' => $results['horse_to_trainer_id'],
					'date_of_charge' => date('d-m-Y', strtotime($results['date_of_charge'])),
					'left_date_of_charge' => date('d-m-Y', strtotime($results['left_date_of_charge'])),
					'extra_narration' => $results['extra_narration'],
				);
			}
		}//echo "<pre>";print_r($json);exit;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function Updatetrainers(){

		$json['status'] = 0;
		if($this->request->post['date_of_charge'] != '' && $this->request->post['left_date_of_charge'] != ''){
			//echo "<pre>";print_r($this->request->post);exit;

			$date_start = date('Y-m-d', strtotime($this->request->post['date_of_charge']));
			$date_end = date('Y-m-d', strtotime($this->request->post['left_date_of_charge']));

			//$old_bit_datas = $this->db->query("SELECT * FROM `horse_bits` WHERE `id` ='".$this->request->get['auto_id']."' ");
			//echo "<pre>";print_r($old_bit_datas);exit;
		
			$this->db->query("UPDATE `horse_to_trainer` SET 
				date_of_charge = '".$this->db->escape($date_start)."',
				left_date_of_charge = '".$this->db->escape($date_end)."',
				extra_narration = '".$this->request->post['extra_narration']."'
				WHERE `horse_to_trainer_id` = '".$this->request->get['horse_to_trainer_id']."'");
			$json['status'] = 1;
		}
		//echo $status;exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
