<?php
date_default_timezone_set("Asia/Kolkata");
class ControllerCatalogVendor extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/vendor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/vendor');
		$this->load->model('catalog/product');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/vendor');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/vendor');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_vendor->addVendor($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/vendor');
		$this->load->model('catalog/product');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/vendor');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_vendor->editVendor($this->request->get['vendor_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/vendor');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/vendor');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $vendor_id) {
				$this->model_catalog_vendor->deleteVendor($vendor_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}
		
		if (isset($this->request->get['filter_vendor'])) {
			$filter_vendor = $this->request->get['filter_vendor'];
		} else {
			$filter_vendor = null;
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$filter_vendor_id = $this->request->get['filter_vendor_id'];
		} else {
			$filter_vendor_id = null;
		}

		if (isset($this->request->get['filter_vendorsort'])) {
			$filter_vendorsort = $this->request->get['filter_vendorsort'];
		} else {
			$filter_vendorsort = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . urlencode(html_entity_decode($this->request->get['filter_vendor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$url .= '&filter_vendor_id=' . urlencode(html_entity_decode($this->request->get['filter_vendor_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendorsort'])) {
			$url .= '&filter_vendorsort=' . urlencode(html_entity_decode($this->request->get['filter_vendorsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = 'Active';
			$data['filter_status'] = 'Active';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['status'] =array(
				'Active'  =>"Active",
				'In-Active'  =>'In-Active'
		);

		$data['add'] = $this->url->link('catalog/vendor/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/vendor/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['vendors'] = array();

		$filter_data = array(
			'filter_vendor'	  => $filter_vendor,
			'filter_vendor_id'  => $filter_vendor_id,
			'filter_vendorsort' => $filter_vendorsort,
			'filter_status'	=>	$filter_status,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$vendor_total = $this->model_catalog_vendor->getTotalVendors($filter_data);

		$results = $this->model_catalog_vendor->getVendors($filter_data);

		foreach ($results as $result) {
			$logs_info = $this->db->query("SELECT * FROM `supplier_logs` WHERE `supp_id` = '".$result['vendor_id']."' ORDER BY `id` DESC LIMIT 1 ");
			// echo'<pre>';
			// print_r("SELECT * FROM `supplier_logs` WHERE `supp_id` = '".$result['vendor_id']."' ");
			if ($logs_info->num_rows > 0) {
				$fname = $logs_info->row['fname'];
				$lname = $logs_info->row['lname'];
				$log_date = date('d-m-Y', strtotime($logs_info->row['log_date']));
				$log_time = $logs_info->row['log_time'];
				$log_datas = '<span>'.'Name: '.$fname.' '.$lname.'</span>'.'<br>'.'<span>'.'Date: '.$log_date.'</span>'.'<br>'.'<span>'.'Time: '.$log_time.'</span>';
			} else {
				$log_datas = '';
			}

			$anchor_styles = "color: #666666 !important; cursor:pointer;";

			$data['vendors'][] = array(
				'vendor_id' => $result['vendor_id'],
				'vendor_name'     => $result['vendor_name'],
				'code'     => $result['vendor_code'],
				'log_datas'     => $log_datas,
				'status'     => $result['isActive'],
				'anchor_styles'     => $anchor_styles,
				'edit'     => $this->url->link('catalog/vendor/edit', 'token=' . $this->session->data['token'] . '&vendor_id=' . $result['vendor_id'] . $url, true),
				'export'     => $this->url->link('catalog/vendor/export_vendor', 'token=' . $this->session->data['token'] . '&filter_name_id=' . $result['vendor_id'] . $url, true)
			);
		}//exit;

		$data['vendorsorts'] = array(
			'a'=>'A','b'=>'B','c'=>'C','d'=>'D','e'=>'E','f'=>'F','g'=>'G','h'=>'H',
			'i'=>'I','j'=>'J','k'=>'K','l'=>'L','m'=>'M','n'=>'N','o'=>'O','p'=>'P',
			'q'=>'Q','r'=>'R','s'=>'S','t'=>'T','u'=>'U','v'=>'V','w'=>'W','x'=>'X',
			'y'=>'Y','z'=>'Z','Other'
		);

		$data['base_link_1'] = $this->url->link('catalog/vendor/edit', 'token=' . $this->session->data['token']);
		$data['base_link'] = $this->url->link('catalog/vendor/edit', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . urlencode(html_entity_decode($this->request->get['filter_vendor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$url .= '&filter_vendor_id=' . urlencode(html_entity_decode($this->request->get['filter_vendor_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendorsort'])) {
			$url .= '&filter_vendorsort=' . urlencode(html_entity_decode($this->request->get['filter_vendorsort'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . '&sort=vendor_name' . $url, true);

		$data['sort_place'] = $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . '&sort=vendor_place' . $url, true);

		$data['sort_phone_no'] = $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . '&sort=vendor_phone_no' . $url, true);
		$data['sort_contact_person'] = $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . '&sort=vendor_contact_person' . $url, true);
		$data['sort_email_id'] = $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . '&sort=vendor_email_id' . $url, true);
		$data['sort_items'] = $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . '&sort=vendor_items' . $url, true);


		$data['sort_sort_order'] = $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . urlencode(html_entity_decode($this->request->get['filter_vendor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$url .= '&filter_vendor_id=' . urlencode(html_entity_decode($this->request->get['filter_vendor_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendorsort'])) {
			$url .= '&filter_vendorsort=' . urlencode(html_entity_decode($this->request->get['filter_vendorsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $vendor_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($vendor_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($vendor_total - $this->config->get('config_limit_admin'))) ? $vendor_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $vendor_total, ceil($vendor_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_vendor'] = $filter_vendor;
		$data['filter_vendor_id'] = $filter_vendor_id;
		$data['filter_vendorsort'] = $filter_vendorsort;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];

		$this->response->setOutput($this->load->view('catalog/vendor_list', $data));
	}

	protected function getForm() {
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['vendor_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['text_loading'] = $this->language->get('text_loading');
		
		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['Active'] = array('In-Active'	=> 'In-Active',
							  'Active'	=> 'Active');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		if (isset($this->error['filename'])) {
			$data['error_filename'] = $this->error['filename'];
		} else {
			$data['error_filename'] = '';
		}

		if (isset($this->error['mask'])) {
			$data['error_mask'] = $this->error['mask'];
		} else {
			$data['error_mask'] = '';
		}

		if (isset($this->error['filename2'])) {
			$data['error_filename2'] = $this->error['filename2'];
		} else {
			$data['error_filename2'] = '';
		}

		if (isset($this->error['mask2'])) {
			$data['error_mask2'] = $this->error['mask2'];
		} else {
			$data['error_mask2'] = '';
		}

		if (isset($this->error['filename3'])) {
			$data['error_filename3'] = $this->error['filename3'];
		} else {
			$data['error_filename3'] = '';
		}

		if (isset($this->error['mask3'])) {
			$data['error_mask3'] = $this->error['mask3'];
		} else {
			$data['error_mask3'] = '';
		}

		if (isset($this->error['filename4'])) {
			$data['error_filename4'] = $this->error['filename4'];
		} else {
			$data['error_filename4'] = '';
		}

		if (isset($this->error['mask4'])) {
			$data['error_mask4'] = $this->error['mask4'];
		} else {
			$data['error_mask4'] = '';
		}

		if (isset($this->error['filename5'])) {
			$data['error_filename5'] = $this->error['filename5'];
		} else {
			$data['error_filename5'] = '';
		}

		if (isset($this->error['mask5'])) {
			$data['error_mask5'] = $this->error['mask5'];
		} else {
			$data['error_mask5'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . urlencode(html_entity_decode($this->request->get['filter_vendor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$url .= '&filter_vendor_id=' . urlencode(html_entity_decode($this->request->get['filter_vendor_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['vendor_id'])) {
			$data['action'] = $this->url->link('catalog/vendor/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/vendor/edit', 'token=' . $this->session->data['token'] . '&vendor_id=' . $this->request->get['vendor_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['vendor_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$vendor_info = $this->model_catalog_vendor->getVendor($this->request->get['vendor_id']);
		}

		$data['token'] = $this->session->data['token'];

		$data['products'] = array(
			'0' => array(
				'product_id' => '1',
				'name' => 'Test Product 1',
			),
			'1' => array(
				'product_id' => '2',
				'name' => 'Test Product 2',
			),
			'2' => array(
				'product_id' => '3',
				'name' => 'Test Product 3',
			),
			'3' => array(
				'product_id' => '4',
				'name' => 'Test Product 4',
			),
			'4' => array(
				'product_id' => '5',
				'name' => 'Test Product 5',
			)
		);

		if (isset($this->request->post['vendor_name'])) {
			$data['vendor_name'] = $this->request->post['vendor_name'];
		} elseif (!empty($vendor_info)) {
			$data['vendor_name'] = $vendor_info['vendor_name'];
		} else {
			$data['vendor_name'] = '';
		}
		if (isset($this->request->post['place'])) {
			$data['place'] = $this->request->post['place'];
		} elseif (!empty($vendor_info)) {
			$data['place'] = $vendor_info['place'];
		} else {
			$data['place'] = '';
		}

		if (isset($this->request->post['gst_no'])) {
			$data['gst_no'] = $this->request->post['gst_no'];
		} elseif (!empty($vendor_info)) {
			$data['gst_no'] = $vendor_info['gst_no'];
		} else {
			$data['gst_no'] = '';
		}
		
		if (isset($this->request->post['phone_no'])) {
			$data['phone_no'] = $this->request->post['phone_no'];
		} elseif (!empty($vendor_info)) {
			$data['phone_no'] = $vendor_info['phone_no'];
		} else {
			$data['phone_no'] = '';
		}

		if (isset($this->request->post['vendor_code'])) {
			$data['vendor_code'] = $this->request->post['vendor_code'];
		} elseif (!empty($vendor_info)) {
			$data['vendor_code'] = $vendor_info['vendor_code'];
		} else {
			$data['vendor_code'] = '';
		}

		if (isset($this->request->post['filename'])) {
			$data['filename'] = $this->request->post['filename'];
		} elseif (!empty($vendor_info)) {
			$data['filename'] = $vendor_info['filename'];
		} else {
			$data['filename'] = '';
		}

		if (isset($this->request->post['mask'])) {
			$data['mask'] = $this->request->post['mask'];
		} elseif (!empty($vendor_info)) {
			$data['mask'] = $vendor_info['mask'];
		} else {
			$data['mask'] = '';
		}

		if (isset($this->request->post['uploaded_file_source'])) {
			$data['uploaded_file_source'] = $this->request->post['uploaded_file_source'];
		} elseif (!empty($vendor_info)) {
			$data['uploaded_file_source'] = $vendor_info['uploaded_file_source'];
		} else {
			$data['uploaded_file_source'] = '';
		}

		if (isset($this->request->post['filename2'])) {
			$data['filename2'] = $this->request->post['filename2'];
		} elseif (!empty($vendor_info)) {
			$data['filename2'] = $vendor_info['filename2'];
		} else {
			$data['filename2'] = '';
		}

		if (isset($this->request->post['mask2'])) {
			$data['mask2'] = $this->request->post['mask2'];
		} elseif (!empty($vendor_info)) {
			$data['mask2'] = $vendor_info['mask2'];
		} else {
			$data['mask2'] = '';
		}

		if (isset($this->request->post['uploaded_file_source2'])) {
			$data['uploaded_file_source2'] = $this->request->post['uploaded_file_source2'];
		} elseif (!empty($vendor_info)) {
			$data['uploaded_file_source2'] = $vendor_info['uploaded_file_source2'];
		} else {
			$data['uploaded_file_source2'] = '';
		}

		if (isset($this->request->post['filename3'])) {
			$data['filename3'] = $this->request->post['filename3'];
		} elseif (!empty($vendor_info)) {
			$data['filename3'] = $vendor_info['filename3'];
		} else {
			$data['filename3'] = '';
		}

		if (isset($this->request->post['mask3'])) {
			$data['mask3'] = $this->request->post['mask3'];
		} elseif (!empty($vendor_info)) {
			$data['mask3'] = $vendor_info['mask3'];
		} else {
			$data['mask3'] = '';
		}

		if (isset($this->request->post['uploaded_file_source3'])) {
			$data['uploaded_file_source3'] = $this->request->post['uploaded_file_source3'];
		} elseif (!empty($vendor_info)) {
			$data['uploaded_file_source3'] = $vendor_info['uploaded_file_source3'];
		} else {
			$data['uploaded_file_source3'] = '';
		}

		if (isset($this->request->post['filename4'])) {
			$data['filename4'] = $this->request->post['filename4'];
		} elseif (!empty($vendor_info)) {
			$data['filename4'] = $vendor_info['filename4'];
		} else {
			$data['filename4'] = '';
		}

		if (isset($this->request->post['mask4'])) {
			$data['mask4'] = $this->request->post['mask4'];
		} elseif (!empty($vendor_info)) {
			$data['mask4'] = $vendor_info['mask4'];
		} else {
			$data['mask4'] = '';
		}

		if (isset($this->request->post['uploaded_file_source4'])) {
			$data['uploaded_file_source4'] = $this->request->post['uploaded_file_source4'];
		} elseif (!empty($vendor_info)) {
			$data['uploaded_file_source4'] = $vendor_info['uploaded_file_source4'];
		} else {
			$data['uploaded_file_source4'] = '';
		}

		if (isset($this->request->post['filename5'])) {
			$data['filename5'] = $this->request->post['filename5'];
		} elseif (!empty($vendor_info)) {
			$data['filename5'] = $vendor_info['filename5'];
		} else {
			$data['filename5'] = '';
		}

		$data['user_log_grp_id'] = $this->user->getGroupId();

		$data['user_log_id'] = $this->user->getId();
		// echo $data['user_log_id'];
		// exit;

		if (isset($this->request->post['mask5'])) {
			$data['mask5'] = $this->request->post['mask5'];
		} elseif (!empty($vendor_info)) {
			$data['mask5'] = $vendor_info['mask5'];
		} else {
			$data['mask5'] = '';
		}

		if (isset($this->request->post['contact_datas'])) {
			$data['contact_datas'] = $this->request->post['contact_datas'];
		} elseif (!empty($vendor_info)) {
			$contact_datas = $this->model_catalog_vendor->getcontact_data($vendor_info['vendor_id']);
			$data['contact_datas'] = $contact_datas;
		} else {
			$data['contact_datas'] = array();
		}

		if (isset($this->request->post['isActive'])) {
			$data['isActive'] = $this->request->post['isActive'];
		} elseif (!empty($vendor_info)) {
			$data['isActive'] = $vendor_info['isActive'];
		} else {
			$data['isActive'] = 'Active';
		}

		$data['payment_types'] = array(
			'Advance' => 'Advance',
			'Against Delivery' => 'Against Delivery',
			'Credit' => 'Credit',
			'Pdc' => 'Post Dated Check',
		);


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/vendor_form', $data));
	}

	protected function validateForm() {
		//echo "<pre>";print_r($this->request->post);exit;

		if($this->request->post['contact_datas']){
			foreach ($this->request->post['contact_datas'] as $key => $value) {
				if ((utf8_strlen($value['email_id']) > 96) || !filter_var($value['email_id'], FILTER_VALIDATE_EMAIL) && ($value['email_id'] != '')) {
					$this->error['warning'] = 'Please Enter valid Email Id';
				}
				if ($value['mobile2'] != '') {
					if ((utf8_strlen($value['mobile2']) > 10) || (utf8_strlen($value['mobile2']) < 10)) {
						$this->error['warning'] = 'Please Enter valid Mobile 2';
					} else {
						if(!preg_match('/^[0-9]{10}+$/',$value['mobile2'])){
							$this->error['warning'] = 'Please Enter valid Mobile 2';
						}
					}
				}
				if ($value['mobile1'] != '') {
					if ((utf8_strlen($value['mobile1']) > 10) || (utf8_strlen($value['mobile1']) < 10)) {
						$this->error['warning'] = 'Please Enter valid Mobile 1';
					} else {
						if(!preg_match('/^[0-9]{10}+$/',$value['mobile1'])){
							$this->error['warning'] = 'Please Enter valid Mobile 1';
						}
					}
				}
				if ($value['phone_no'] != '') {
					if ((utf8_strlen($value['phone_no']) > 10) || (utf8_strlen($value['phone_no']) < 10)) {
						$this->error['warning'] = 'Please Enter valid Phone No';
					} else {
						echo'inn';
						if(!preg_match('/^[0-9]{10}+$/',$value['phone_no'])){
							$this->error['warning'] = 'Please Enter valid Phone No';
						}
					}
				}
			}
		}

		if ((utf8_strlen($this->request->post['vendor_name']) < 2) || (utf8_strlen($this->request->post['vendor_name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (($this->request->post['vendor_code'] != '') && (!isset($this->request->get['vendor_id'])) ) {
			$avail_code = $this->db->query("SELECT * FROM is_vendor WHERE `vendor_code` = '".$this->request->post['vendor_code']."' ");
			if ($avail_code->num_rows > 0) {
				$this->error['code'] = 'This Code Is Already In Used! Please Select Another!';
			}
		}

		if ($this->request->post['vendor_code'] == '' ) {
			$this->error['code'] = 'Please Select Supplier Code!';
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/vendor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $this->load->model('catalog/product');

		// foreach ($this->request->post['selected'] as $vendor_id) {
		// 	$product_total = $this->model_catalog_product->getTotalProductsByVendorId($vendor_id);

		// 	if ($product_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
		// 	}
		// }

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/vendor');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_vendor->getVendors($filter_data);

			foreach ($results as $result) {

				$json[] = array(
					'vendor_id' => $result['vendor_id'],
					'vendor_name'     => strip_tags(html_entity_decode($result['vendor_name'], ENT_QUOTES, 'UTF-8'))
					/*'place'     => strip_tags(html_entity_decode($result['place'], ENT_QUOTES, 'UTF-8'))*/
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['vendor_name'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function upload() {
		$this->load->language('catalog/vendor');

		$json = array();

		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/vendor')) {
			$json['error'] = $this->language->get('error_permission');
		}
		

		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				$file_size = $this->request->files['file']['size'] / 1024;
          		if($file_size < 5000) {

					// Allowed file extension types
					$allowed = array();

					$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

					$filetypes = explode("\n", $extension_allowed);

					foreach ($filetypes as $filetype) {
						$allowed[] = trim($filetype);
					}
					$allowed[] = 'jpg';
					$allowed[] = 'jpeg';
					$allowed[] = 'png';
					$allowed[] = 'pdf';
					$this->log->write(print_r($allowed, true));
					if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
						$json['error'] = $this->language->get('error_filetype');
					}

					// Allowed file mime types
					$allowed = array();

					$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

					$filetypes = explode("\n", $mime_allowed);

					foreach ($filetypes as $filetype) {
						$allowed[] = trim($filetype);
					}

					//$this->log->write(print_r($this->request->files,true));

					if (!in_array($this->request->files['file']['type'], $allowed)) {
						$json['error'] = $this->language->get('error_filetype');
					}

					// Check to see if any PHP files are trying to be uploaded
					$content = file_get_contents($this->request->files['file']['tmp_name']);

					if (preg_match('/\<\?php/i', $content)) {
						$json['error'] = $this->language->get('error_filetype');
					}

					// Return any upload error
					if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
						$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
					}
				} else {
					$json['error'] = 'it is more than 5 mb';
				}
			}
		}

		$file_show_path = HTTP_CATALOG.'system/storage/download/';

		if (!$json) {
			//$file = $filename . '.' . token(32);
			$file = $filename;

			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);

			$json['filename'] = $file;
			$json['mask'] = $filename;
			$json['link_href'] = $file_show_path . $file;

			$json['success'] = $this->language->get('text_upload');
		}


		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function export() {
		$this->load->language('catalog/vendor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/vendor');
		$this->load->model('catalog/product');

		if (isset($this->request->get['filter_vendor'])) {
			$filter_vendor = $this->request->get['filter_vendor'];
		} else {
			$filter_vendor = null;
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$filter_vendor_id = $this->request->get['filter_vendor_id'];
		} else {
			$filter_vendor_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . urlencode(html_entity_decode($this->request->get['filter_vendor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor_id'])) {
			$url .= '&filter_vendor_id=' . urlencode(html_entity_decode($this->request->get['filter_vendor_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$vendors = array();
		$filter_data = array(
			'filter_vendor'	  => $filter_vendor,
			'filter_vendor_id'  => $filter_vendor_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		//$vendor_total = $this->model_catalog_vendor->getTotalVendors($filter_data);
		$results = $this->model_catalog_vendor->getVendors($filter_data);
		foreach ($results as $result) {

			$vendors[] = array(
				'vendor_id' => $result['vendor_id'],
				'vendor_name'     => $result['vendor_name'],
				'place'     => $result['place'],
				'phone_no'     => $result['phone_no'],
				'contact_person'     => $result['contact_person'],
				'email_id'     => $result['email_id'],
				'items'     => $result['items'],
				'vendor_status'     => $result['vendor_status'],
				'state_of_reg'     => $result['state_of_reg'],
				'territory_handled'     => $result['territory_handled'],
				'transportation_details'     => $result['transportation_details'],
				'charged_to'     => $result['charged_to'],
				'payment_details'     => $result['payment_details'],
				'territory_handled'     => $result['territory_handled'],
				'territory_handled'     => $result['territory_handled'],
				'territory_handled'     => $result['territory_handled'],
				'territory_handled'     => $result['territory_handled'],
				'territory_handled'     => $result['territory_handled'],
				'territory_handled'     => $result['territory_handled'],
			);
		}
	}

	public function getdata() {
		$json = array();
		$html = '';
		if (isset($this->request->get['filter_name_id'])) {
			$this->load->model('catalog/vendor');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name_id'],
			);
			$filter_name_id = $this->request->get['filter_name_id'];

			$vendor_datass = $this->db->query("SELECT * FROM `is_vendor` WHERE `vendor_id` = '".$filter_name_id."' ")->rows;
			$vendor_datas = array();
			foreach($vendor_datass as $ckeys => $cvalues){
				if($cvalues['payment_details'] == 'Advance'){
					$payment_text = $cvalues['percent_text'].'% Advance and Balance after ' . $cvalues['days_text'] . ' days of delivery & submission of Invoice';
				} elseif($cvalues['payment_details'] == 'Credit') {
					$payment_text = $cvalues['days_text'] . ' days of delivery & submission of Invoice';
				} elseif($cvalues['payment_details'] == 'Pdc'){
					$payment_text = $cvalues['days_text'] . ' days from date of order';
				}
				$vendor_datas[] = array(
					'vendor_name' => $cvalues['vendor_name'],
					'place' => $cvalues['place'],
					'phone_no' => $cvalues['phone_no'],
					'contact_person' => $cvalues['contact_person'],
					'email_id' => $cvalues['email_id'],
					'items' => $cvalues['items'],
					
					'vendor_id' => $cvalues['vendor_id'],
					'vendor_status' => $cvalues['vendor_status'],
					'territory_handled' => $cvalues['territory_handled'],
					'payment_details' => $cvalues['payment_details'],
					'payment_text' => $payment_text,
					'c_blockno' => $cvalues['c_blockno'],
					'c_street' => $cvalues['c_street'],
					'c_area' => $cvalues['c_area'],
					'c_district' => $cvalues['c_district'],
					'c_city' => $cvalues['c_city'],
					'c_pincode' => $cvalues['c_pincode'],
					'c_state' => $cvalues['c_state'],
					'c_country' => $cvalues['c_country'],
				); 			
			}


			$vendor_contact = $this->db->query("SELECT * FROM `is_vendor_contact` WHERE `vendor_id` = '".$filter_name_id."' ")->rows;

			//echo '<pre>';
			//print_r($vendor_datas);
			//exit();

			$vendor_contact_data = array();
			foreach($vendor_contact as $ckeys => $cvalues){

				/*if($cvalues['payment_details'] == 'Advance'){
					$payment_text = $cvalues['percent_text'].'% Advance and Balance after ' . $cvalues['days_text'] . ' days of delivery & submission of Invoice';
				} elseif($cvalues['payment_details'] == 'Credit') {
					$payment_text = $cvalues['days_text'] . ' days of delivery & submission of Invoice';
				} elseif($cvalues['payment_details'] == 'Pdc'){
					$payment_text = $cvalues['days_text'] . ' days from date of order';
				}*/
				$vendor_contact_data[] = array(
					'name' => $cvalues['name'],
					'vendor_id' => $cvalues['vendor_id'],
					'id' => $cvalues['id'],
					'designation' => $cvalues['designation'],
					'phone_no' => $cvalues['phone_no'],
					'mobile1' => $cvalues['mobile1'],
					'mobile2' => $cvalues['mobile2'],
					'email_id' => $cvalues['email_id'],
				); 			
			}
			//echo '<pre>';
			//print_r($vendor_contact_data);
			//exit();

			$extra_field = 1;
			$html = '<table style="margin-top: 15px;" class="table table-hover">';
				$html .= '<tbody style="border-top:0px !important;">';
					foreach ($vendor_datas as $result) {
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>Vendor Name</b> : ' . $result['vendor_name'];
							$html .= '</td>';
						$html .= '</tr>';	
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>place</b> : ' . $result['place'];
							$html .= '</td>';
						$html .= '</tr>';	
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>phone_no</b> : ' . $result['phone_no'];
							$html .= '</td>';
						$html .= '</tr>';
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>contact_person</b> : ' . $result['contact_person'];
							$html .= '</td>';
						$html .= '</tr>';
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>email_id</b> : ' . $result['email_id'];
							$html .= '</td>';
						$html .= '</tr>';
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>items</b> : ' . $result['items'];
							$html .= '</td>';
						$html .= '</tr>';
						/*$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>Vendor Code</b> : ISB/V-' . $result['vendor_id'];
							$html .= '</td>';
						$html .= '</tr>';
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>Status</b> : ' . $result['vendor_status'];
							$html .= '</td>';
						$html .= '</tr>';
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>Territory Handled</b> : ' . $result['territory_handled'];
							$html .= '</td>';	
						$html .= '</tr>';	
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>Payment Terms</b> : ' . $result['payment_details'] . '(' . $result['payment_text'] . ')<hr>';
							$html .= '</td>';
						$html .= '</tr>';
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>Address</b> : ' . $result['c_blockno'].', '.$result['c_street'].', '.$result['c_area'].', '.$result['c_district'].', '.$result['c_city'].', '.$result['c_pincode'].', '.$result['c_state'].', '.$result['c_country'];
							$html .= '</td>';
						$html .= '</tr>';*/	

					}
					foreach ($vendor_contact_data as $result1) {
					//	$html .= '<tr>';	
					//		$html .= '<td style="border-top: 0px !important;">';
					//			$html .= '<b>Vendor Name</b> : ' . $result1['name'];
					//	$html .= '</tr>';	
					//	$html .= '<tr>';	
					//		$html .= '</td>';
					//		$html .= '<td style="border-top: 0px !important;">';
					//			$html .= '<b>Vendor Code</b> : ISB/V-' . $result1['vendor_id'];
					//		$html .= '</td>';
					//	$html .= '</tr>';
					//	$html .= '<tr>';	
					//		$html .= '<td style="border-top: 0px !important;">';
					//			$html .= '<b>Designation</b> : ' . $result1['designation'];
					//		$html .= '</td>';
					//	$html .= '</tr>';
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>Phone No</b> : ' . $result1['phone_no'];
							$html .= '</td>';
						$html .= '</tr>';	
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>Mobile</b> : ' . $result1['mobile1'] . ' / ' .$result1['mobile2'] . '';
							$html .= '</td>';
						$html .= '</tr>';
						$html .= '<tr>';	
							$html .= '<td style="border-top: 0px !important;">';
								$html .= '<b>Email</b> : ' . $result1['email_id'] ;
							$html .= '</td>';
						$html .= '</tr>';						
					}
				$html .= '</tbody>';
			$html .= '</table>';
		}
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function export_vendor() {
		$json = array();
		$html = '';
		if (isset($this->request->get['filter_name_id'])) {
			$this->load->model('catalog/vendor');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name_id'],
			);
			$filter_name_id = $this->request->get['filter_name_id'];
			$vendor_datass = $this->db->query("SELECT * FROM `is_vendor` WHERE `vendor_id` = '".$filter_name_id."' ")->rows;
			$vendor_datas = array();
			$vendor_name = '';
			$place = '';
			foreach($vendor_datass as $ckeys => $cvalues){
				if($cvalues['payment_details'] == 'Advance'){
					$payment_text = $cvalues['percent_text'].'% Advance and Balance after ' . $cvalues['days_text'] . ' days of delivery & submission of Invoice';
				} elseif($cvalues['payment_details'] == 'Credit') {
					$payment_text = $cvalues['days_text'] . ' days of delivery & submission of Invoice';
				} elseif($cvalues['payment_details'] == 'Pdc'){
					$payment_text = $cvalues['days_text'] . ' days from date of order';
				}
				$vendor_name = $cvalues['vendor_name'];
				$place = $cvalues['place'];
				$vendor_datas[] = array(
					'vendor_name' => $cvalues['vendor_name'],
					'place' => $cvalues['place'],
					'phone_no'     => $result['phone_no'],
			 		'contact_person'     => $result['contact_person'],
					'email_id'     => $result['email_id'],
					'items'     => $result['items'],
					'vendor_id' => $cvalues['vendor_id'],
					'vendor_status' => $cvalues['vendor_status'],
					'territory_handled' => $cvalues['territory_handled'],
					'payment_details' => $cvalues['payment_details'],
					'payment_text' => $payment_text,
					'c_blockno' => $cvalues['c_blockno'],
					'c_street' => $cvalues['c_street'],
					'c_area' => $cvalues['c_area'],
					'c_district' => $cvalues['c_district'],
					'c_city' => $cvalues['c_city'],
					'c_pincode' => $cvalues['c_pincode'],
					'c_state' => $cvalues['c_state'],
					'c_country' => $cvalues['c_country'],
				); 			
			}
			$vendor_contact = $this->db->query("SELECT * FROM `is_vendor_contact` WHERE `vendor_id` = '".$filter_name_id."' ")->rows;
			$vendor_contact_data = array();
			foreach($vendor_contact as $ckeys => $cvalues){
				$vendor_contact_data[] = array(
					'name' => $cvalues['name'],
					'vendor_id' => $cvalues['vendor_id'],
					'id' => $cvalues['id'],
					'designation' => $cvalues['designation'],
					'phone_no' => $cvalues['phone_no'],
					'mobile1' => $cvalues['mobile1'],
					'mobile2' => $cvalues['mobile2'],
					'email_id' => $cvalues['email_id'],
				); 			
			}
			$data['vendor_datas'] = $vendor_datas;
			$data['vendor_contact_data'] = $vendor_contact_data;
			$data['title'] = 'G.R.N';
			if ($this->request->server['HTTPS']) {
				$data['base'] = HTTPS_SERVER;
			} else {
				$data['base'] = HTTP_SERVER;
			}
			$data['direction'] = $this->language->get('direction');
			$data['lang'] = $this->language->get('code');

			$data['header_image'] = DIR_APPLICATION.'view/image/image_logo.png';
			$html = $this->load->view('sale/vendoroverview', $data);
			//echo $html;exit;
			$vendor_name = str_replace(array(' ', ',', '&', '.'), '_', $vendor_name);
			$filename = 'VENDOR_OVERVIEW_'.$vendor_name;
			//echo $filename;exit;		
			define("DOMPDF_ENABLE_REMOTE", false);
		    $dompdf = new Dompdf();
		    $dompdf->loadHtml($html);
		    $dompdf->setPaper('A4', 'portrait');
		    $dompdf->render();
		    $dompdf->stream($filename);
		}
	}
}