<?php
class Controllercataloghorse extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo '<pre>';print_r($this->request->post);exit;
			$this->model_catalog_horse->addHorse($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo '<pre>';print_r($this->request->post);exit;
			$this->model_catalog_horse->editCategory($this->request->get['horse_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $category_id) {
				$this->model_catalog_horse->deleteCategory($category_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function repair() {
		$this->load->language('catalog/horse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/horse');

		if ($this->validateRepair()) {
			$this->model_catalog_horse->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('catalog/horse', 'token=' . $this->session->data['token'], true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_hourse_name'])) {
			$filter_hourse_name = $this->request->get['filter_hourse_name'];
		} else {
			$filter_hourse_name = '';
		}

		if (isset($this->request->get['filter_hourse_id'])) {
			$filter_hourse_id = $this->request->get['filter_hourse_id'];
		} else {
			$filter_hourse_id = '';
		}

		if (isset($this->request->get['filter_trainer_name'])) {
			$filter_trainer_name = $this->request->get['filter_trainer_name'];
		} else {
			$filter_trainer_name = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_hourse_name']) ) {
			$url .= '&filter_hourse_name=' . $this->request->get['filter_hourse_name'];
		}

		if (isset($this->request->get['filter_hourse_id']) ) {
			$url .= '&filter_hourse_id=' . $this->request->get['filter_hourse_id'];
		}

		if (isset($this->request->get['filter_trainer_name']) ) {
			$url .= '&filter_trainer_name=' . $this->request->get['filter_trainer_name'];
		}

		//echo $filter_hourse_name;exit;

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/horse/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/horse/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/horse/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_hourse_name' => $filter_hourse_name,
			'filter_hourse_id' => $filter_hourse_id,
			'filter_trainer_name' => $filter_trainer_name
		);
		//echo '<pre>';print_r($filter_data);exit;
		$data['horseDatas'] =array();
		$category_total = $this->model_catalog_horse->getHourseCount($filter_data);

		$results = $this->model_catalog_horse->getHorses($filter_data);

		foreach ($results as $result) {
			$data['horseDatas'][] = array(
				'horseID' 	  			=> $result['horseseq'],
				'horseName'   			=> $result['official_name'],
				'horseColor'  			=> $result['color'],
				'horseSex'  			=> $result['sex'],
				'horseRegDate'  		=> $result['reg_date'],
				'horseFoalDate'  		=> $result['foal_date'],
				'horseOrigin'  			=> $result['orgin'],
				'edit'        => $this->url->link('catalog/horse/edit', 'token=' . $this->session->data['token'] . '&horse_id=' . $result['horseseq'] . $url, true),
				'horseAge'  			=> $result['age'],
				'horseRating'  			=> $result['rating'],
				'trainer_name'  			=> $result['trainer_name']
				//'delete'      => $this->url->link('catalog/horse/delete', 'token=' . $this->session->data['token'] . '&horse_id=' . $result['HORSESEQ'] . $url, true)
			);
		}
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['filter_hourse_name'] = $filter_hourse_name;
		$data['filter_hourse_id'] = $filter_hourse_id;
		$data['filter_trainer_name'] = $filter_trainer_name;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/horse_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		//echo "<pre>";print_r($this->error);exit;

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['valierr_horse_name'])) {
			$data['valierr_horse_name'] = $this->error['valierr_horse_name'];
		} 

		if (isset($this->error['valierr_registeration_date'])) {
			$data['valierr_registeration_date'] = $this->error['valierr_registeration_date'];
		} 

		if (isset($this->error['valierr_month_registeration'])) {
			$data['valierr_month_registeration'] = $this->error['valierr_month_registeration'];
		}

		if (isset($this->error['valierr_year_registeration'])) {
			$data['valierr_year_registeration'] = $this->error['valierr_year_registeration'];
		} 

		if (isset($this->error['valierr_foal_date'])) {
			$data['valierr_foal_date'] = $this->error['valierr_foal_date'];
		}

		if (isset($this->error['valierr_month_foal_date'])) {
			$data['valierr_month_foal_date'] = $this->error['valierr_month_foal_date'];
		} 

		if (isset($this->error['valierr_year_foal_date'])) {
			$data['valierr_year_foal_date'] = $this->error['valierr_year_foal_date'];
		} 

		if (isset($this->error['valierr_namehave_btdatenot'])) {
			$data['valierr_namehave_btdatenot'] = $this->error['valierr_namehave_btdatenot'];
		} 

		if (isset($this->error['valierr_datehave_btnamenot'])) {
			$data['valierr_datehave_btnamenot'] = $this->error['valierr_datehave_btnamenot'];
		} 

		if (isset($this->error['valierr_change_horse_name'])) {
			$data['valierr_change_horse_name'] = $this->error['valierr_change_horse_name'];
		} 

		if (isset($this->error['valierr_change_horse_date'])) {
			$data['valierr_change_horse_date'] = $this->error['valierr_change_horse_date'];
		} 

		if (isset($this->error['valierr_month_change_horse'])) {
			$data['valierr_month_change_horse'] = $this->error['valierr_month_change_horse'];
		} 

		if (isset($this->error['valierr_year_change_horse'])) {
			$data['valierr_year_change_horse'] = $this->error['valierr_year_change_horse'];
		} 

		if (isset($this->error['valierr_std_stall_certificate_date'])) {
			$data['valierr_std_stall_certificate_date'] = $this->error['valierr_std_stall_certificate_date'];
		} 

		if (isset($this->error['valierr_month_std_stall_certificate'])) {
			$data['valierr_month_std_stall_certificate'] = $this->error['valierr_month_std_stall_certificate'];
		} 

		if (isset($this->error['valierr_year_std_stall_certificate'])) {
			$data['valierr_year_std_stall_certificate'] = $this->error['valierr_year_std_stall_certificate'];
		} 

		if (isset($this->error['valierr_passport_no'])) {
			$data['valierr_passport_no'] = $this->error['valierr_passport_no'];
		} 

		if (isset($this->error['valierr_registerarton_authentication'])) {
			$data['valierr_registerarton_authentication'] = $this->error['valierr_registerarton_authentication'];
		} 

		if (isset($this->error['valierr_sire'])) {
			$data['valierr_sire'] = $this->error['valierr_sire'];
		} 

		if (isset($this->error['valierr_dam'])) {
			$data['valierr_dam'] = $this->error['valierr_dam'];
		} 

		if (isset($this->error['valierr_chip_no_one'])) {
			$data['valierr_chip_no_one'] = $this->error['valierr_chip_no_one'];
		} 

		if (isset($this->error['valierr_arrival_charges_to_be_paid'])) {
			$data['valierr_arrival_charges_to_be_paid'] = $this->error['valierr_arrival_charges_to_be_paid'];
		} 

		if (isset($this->error['valierr_color'])) {
			$data['valierr_color'] = $this->error['valierr_color'];
		} 

		if (isset($this->error['valierr_origin'])) {
			$data['valierr_origin'] = $this->error['valierr_origin'];
		} 

		if (isset($this->error['valierr_sex'])) {
			$data['valierr_sex'] = $this->error['valierr_sex'];
		} 

		if (isset($this->error['valierr_age'])) {
			$data['valierr_age'] = $this->error['valierr_age'];
		} 

		if (isset($this->error['valierr_stud_form'])) {
			$data['valierr_stud_form'] = $this->error['valierr_stud_form'];
		} 

		if (isset($this->error['valierr_breeder'])) {
			$data['valierr_breeder'] = $this->error['valierr_breeder'];
		} 

		if (isset($this->error['valierr_saddle_no'])) {
			$data['valierr_saddle_no'] = $this->error['valierr_saddle_no'];
		} 

		if (isset($this->error['valierr_octroi_details'])) {
			$data['valierr_octroi_details'] = $this->error['valierr_octroi_details'];
		} 

		if (isset($this->error['valierr_awbi_registration_no'])) {
			$data['valierr_awbi_registration_no'] = $this->error['valierr_awbi_registration_no'];
		} 

		if (isset($this->error['valierr_Id_by_brand'])) {
			$data['valierr_Id_by_brand'] = $this->error['valierr_Id_by_brand'];
		} 

		if (isset($this->error['valierr_std_stall_remarks'])) {
			$data['valierr_std_stall_remarks'] = $this->error['valierr_std_stall_remarks'];
		} 

		if (isset($this->error['valierr_horse_remarks'])) {
			$data['valierr_horse_remarks'] = $this->error['valierr_horse_remarks'];
		} 

		if (isset($this->error['valierr_trainer_name'])) {
			$data['valierr_trainer_name'] = $this->error['valierr_trainer_name'];
		} 

		if (isset($this->error['valierr_date_charge_trainer'])) {
			$data['valierr_date_charge_trainer'] = $this->error['valierr_date_charge_trainer'];
		} 

		if (isset($this->error['valierr_month_charge_trainer'])) {
			$data['valierr_month_charge_trainer'] = $this->error['valierr_month_charge_trainer'];
		} 

		if (isset($this->error['valierr_year_charge_trainer'])) {
			$data['valierr_year_charge_trainer'] = $this->error['valierr_year_charge_trainer'];
		} 

		if (isset($this->error['valierr_arrival_time_trainer'])) {
			$data['valierr_arrival_time_trainer'] = $this->error['valierr_arrival_time_trainer'];
		} 

		if (isset($this->error['valierr_extra_narrration_trainer'])) {
			$data['valierr_extra_narrration_trainer'] = $this->error['valierr_extra_narrration_trainer'];
		} 

		if (isset($this->error['valierr_extra_narrration_two_trainer'])) {
			$data['valierr_extra_narrration_two_trainer'] = $this->error['valierr_extra_narrration_two_trainer'];
		} 

		if (isset($this->error['valierr_date_left_trainer'])) {
			$data['valierr_date_left_trainer'] = $this->error['valierr_date_left_trainer'];
		} 

		if (isset($this->error['valierr_month_left_trainer'])) {
			$data['valierr_month_left_trainer'] = $this->error['valierr_month_left_trainer'];
		} 

		if (isset($this->error['valierr_year_left_trainer'])) {
			$data['valierr_year_left_trainer'] = $this->error['valierr_year_left_trainer'];
		} 

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['horse_id'])) {
			$data['action'] = $this->url->link('catalog/horse/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/horse/edit', 'token=' . $this->session->data['token'] . '&horse_id=' . $this->request->get['horse_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'] . $url, true);

		$data['arrival_time_trainer'] = array(
			'AM'  => 'AM',
			'PM'  => 'PM',
		);

		$data['regiteration_authenticaton_datas'] = array(
			'MyRC'  => 'MyRC',
			'RWITC'  => 'RWITC',
			'BTC'  => 'BTC',
			'DRC'  => 'DRC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'RCTC'  => 'RCTC',
			'OTHER'  => 'OTHER',
		);
		$data['origins'] = array(
			'India'  => 'India',
			'Foreign'  => 'Foreign',
		);

		$data['sexs'] = array(
			'm'  => 'm',
			'g'  => 'g',
			'h'  => 'h',
			'r'  => 'r',
			'f'  => 'f',

		);

		$data['authorty_bans'] = array(
			'VO'  => 'VO',
			'SS'  => 'SS',
			'STIPES'  => 'STIPES',
			'BLEEDER 1'  => 'BLEEDER 1',
			'BLEEDER 2'  => 'BLEEDER 2',
			'BLEEDER 3'  => 'BLEEDER 3',
		);


		$data['clubs'] = array(
			'RWITC'  => 'RWITC',
			'CAL'  => 'CAL',
			'DEL'  => 'DEL',
			'BNG'  => 'BNG',
			'MYS'  => 'MYS',
			'HYD'  => 'HYD',
		);

		$data['venus'] = array(
			'CAL'  => 'CAL',
			'DEL'  => 'DEL',
			'BNG'  => 'BNG',
			'MYS'  => 'MYS',
			'HYD'  => 'HYD',
			'MAD'  => 'MAD',
		);

		$data['grads'] = array(
			'Grade 1'  => 'Grade 1',
			'Grade 1'  => 'Grade 1',
			'Grade 1'  => 'Grade 1',
			'Mixed Class'  => 'Mixed Class',
			'Class I'  => 'Class I',
			'Class II'  => 'Class II',
			'Class III'  => 'Class III',
			'Class IV'  => 'Class IV',
			'Class V'  => 'Class V',
			'Mixed Class'  => 'Mixed Class',
		);

		$data['ownerships_types'] = array(
			'Sale'  => 'Sale',
			'Lease'  => 'Lease',
			'Sale with Contingency'  => 'Sale with Contingency',
			'Lease with Contingency'  => 'Lease with Contingency',
		);



		if (isset($this->request->get['horse_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$horse_info = $this->model_catalog_horse->getHorsesform($this->request->get['horse_id']);
			$trainer_info_all = $this->model_catalog_horse->getTrainerAll($this->request->get['horse_id']);
			$trainer_info_current = $this->model_catalog_horse->getTrainerCurrent($this->request->get['horse_id']);
			$ownerinfo =  $this->model_catalog_horse->getOwnerform($this->request->get['horse_id']);
			$baninfo_with_date_end =  $this->model_catalog_horse->getBanWithdateEnd($this->request->get['horse_id']);//FOR BAN HISTORY
			$baninfo_witout_date_end =  $this->model_catalog_horse->getBanWithoutdateEnd($this->request->get['horse_id']);//FOR BAN DETAILS
			$equipment_info =  $this->model_catalog_horse->getEquipment($this->request->get['horse_id']);
			$shoe_info =  $this->model_catalog_horse->getShoie($this->request->get['horse_id']);
			$stack_info =  $this->model_catalog_horse->getStackOt($this->request->get['horse_id']);
		}
		//echo "<pre>";print_r($this->request->post['trainer_info_history']);exit;
		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['trainer_info_history'])) {
      		$data['trainer_info_all'] = $this->request->post['trainer_info_history'];
    	} elseif (!empty($trainer_info_all)) {
			$data['trainer_info_all'] = $trainer_info_all;
		} else {
			$data['trainer_info_all'] = array();
		}

		if (isset($this->request->post['stackesdatas'])) {
      		$data['stackoutstation_datas'] = $this->request->post['stackesdatas'];
    	} elseif (!empty($stack_info)) {
			$data['stackoutstation_datas'] = $stack_info;
		} else {
			$data['stackoutstation_datas'] = array();
		}

		if (isset($this->request->post['stacked_hidden_outstation_id'])) {
      		$data['stacked_hidden_outstation_id'] = $this->request->post['stacked_hidden_outstation_id'];
    	} elseif (!empty($stack_info)) {
			$stack_info_counts = count($stack_info);
			$data['stacked_hidden_outstation_id'] = $stack_info_counts;
		} else {
      		$data['stacked_hidden_outstation_id'] = 1;
    	}

    	if (isset($this->request->post['shoe_datas'])) {
      		$data['new_shoe_dats'] = $this->request->post['shoe_datas'];
    	} else {
			$data['new_shoe_dats'] = array();
		}


		if (isset($this->request->post['shoe_datas_history'])) {
      		$data['history_shoe'] = $this->request->post['shoe_datas_history'];
    	} elseif (!empty($shoe_info)) {
			$data['history_shoe'] = $shoe_info;
		} else {
			$data['history_shoe'] = array();
		}

		if (isset($this->request->post['equipment_dtas'])) {
      		$data['equipment_datas'] = $this->request->post['equipment_dtas'];
    	} else {
			$data['equipment_datas'] = array();
		}

		//echo "<pre>";print_r($equipment_info);exit;

		if (isset($this->request->post['equipment_datas'])) {
      		$data['equipmentdatas'] = $this->request->post['equipment_datas'];
    	} elseif (!empty($equipment_info)) {
			$data['equipmentdatas'] = $equipment_info;
		} else {
			$data['equipmentdatas'] = array();
		}

		if (isset($this->request->post['ownerdatas'])) {
      		$data['owners_olddatas'] = $this->request->post['ownerdatas'];
    	} elseif (!empty($ownerinfo)) {
			$data['owners_olddatas'] = $ownerinfo;
		} else {
      		$data['owners_olddatas'] = array();
    	}

    	if (isset($this->request->post['ownership_hidden_id'])) {
      		$data['ownership_hidden_id'] = $this->request->post['ownership_hidden_id'];
    	} elseif (!empty($ownerinfo)) {
			$owner_counts = count($ownerinfo);
			$data['ownership_hidden_id'] = $owner_counts;
		} else {
      		$data['ownership_hidden_id'] = 1;
    	}

		if (isset($this->request->post['banhistorydatas'])) {
      		$data['bandatas'] = $this->request->post['banhistorydatas'];
    	} elseif (!empty($baninfo_with_date_end)) {
			$data['bandatas'] = $baninfo_with_date_end;
		} else {
      		$data['bandatas'] = array();
    	}

    	if (isset($this->request->post['bandats'])) {
      		$data['bandeatils'] = $this->request->post['bandats'];
    	} elseif (!empty($baninfo_witout_date_end)) {
			$data['bandeatils'] = $baninfo_witout_date_end;
		} else {
      		$data['bandeatils'] = array();
    	}

    	if (isset($this->request->post['id_hidden_band'])) {
      		$data['id_hidden_band'] = $this->request->post['id_hidden_band'];
    	} elseif (!empty($baninfo_witout_date_end)) {
			$ban_counts = count($baninfo_witout_date_end);
			$data['id_hidden_band'] = $ban_counts;
		} else {
      		$data['id_hidden_band'] = 1;
    	}

		if (isset($this->request->post['horse_name'])) {
			$data['horse_name'] = $this->request->post['horse_name'];
		} elseif (!empty($horse_info)) {
			$data['horse_name'] = $horse_info['official_name'];
		} else {
			$data['horse_name'] = '';
		}

		if (isset($this->request->post['horse_name'])) {
			$data['horse_name'] = $this->request->post['horse_name'];
		} elseif (!empty($horse_info)) {
			$data['horse_name'] = $horse_info['official_name'];
		} else {
			$data['horse_name'] = '';
		}

		if (isset($this->request->post['changehorse_name'])) {
			$data['changehorse_name'] = $this->request->post['changehorse_name'];
		} elseif (!empty($horse_info)) {
			$data['changehorse_name'] = $horse_info['official_name'];
		} else {
			$data['changehorse_name'] = '';
		}

		if (isset($this->request->post['official_name_change'])) {
			$data['official_name_change'] = $this->request->post['official_name_change'];
		} elseif (!empty($horse_info)) {
			$data['official_name_change'] = $horse_info['official_name_change_status'];
		} else {
			$data['official_name_change'] = 0;
		}

		if (isset($this->request->post['horse_id'])) {
			$data['horse_id'] = $this->request->post['horse_id'];
		} elseif (!empty($horse_info)) {
			$data['horse_id'] = $horse_info['horseseq'];
		} else {
			$data['horse_id'] = '';
		}

		if (isset($this->request->post['passport_no'])) {
			$data['passport_no'] = $this->request->post['passport_no'];
		} elseif (!empty($horse_info)) {
			$data['passport_no'] = $horse_info['life_no'];
		} else {
			$data['passport_no'] = '';
		}

		if (isset($this->request->post['change_horse_dates'])) {
			$data['change_horse_dates'] = $this->request->post['change_horse_dates'];
		} elseif (!empty($horse_info)) {
			if($horse_info['date_of_changehorse_name'] != '1970-01-01' && $horse_info['date_of_changehorse_name'] != '0000-00-00'){
				$data['change_horse_dates']= date('d-m-Y', strtotime($horse_info['date_of_changehorse_name']));
			} else {
			$data['change_horse_dates'] = '';
			}
		} else {
			$data['change_horse_dates'] = '';
		}

		if (isset($this->request->post['registeration_date'])) {
			$data['registeration_date'] = $this->request->post['registeration_date'];
		} elseif (!empty($horse_info)) {
			if($horse_info['reg_date'] != '1970-01-01' && $horse_info['reg_date'] != '0000-00-00'){
				$data['registeration_date']= date('d-m-Y', strtotime($horse_info['reg_date']));
			} else {
			$data['registeration_date'] = '';
			}
		} else {
			$data['registeration_date'] = '';
		}

		if (isset($this->request->post['date_foal_date'])) {
			$data['date_foal_date'] = $this->request->post['date_foal_date'];
		} elseif (!empty($horse_info)) {
			if($horse_info['foal_date'] != '1970-01-01' && $horse_info['foal_date'] != '0000-00-00'){
				$data['date_foal_date']= date('d-m-Y', strtotime($horse_info['foal_date']));
			} else {
			$data['date_foal_date'] = '';
			}
		} else {
			$data['date_foal_date'] = '';
		}

		if (isset($this->request->post['date_std_stall_certificate'])) {
			$data['date_std_stall_certificate'] = $this->request->post['date_std_stall_certificate'];
		} elseif (!empty($horse_info)) {

			if($horse_info['std_stall_certificate_date'] != '1970-01-01' && $horse_info['std_stall_certificate_date'] != '0000-00-00'){
				$data['date_std_stall_certificate']= date('d-m-Y', strtotime($horse_info['std_stall_certificate_date']));
			} else {
				$data['date_std_stall_certificate'] = '';
			}
		} else {
			$data['date_std_stall_certificate'] = '';
		}

		if (isset($this->request->post['date_charge_trainer'])) {
			$data['date_charge_trainer'] = $this->request->post['date_charge_trainer'];
		} elseif (!empty($trainer_info_current)) {
			if($trainer_info_current['date_of_charge'] != '1970-01-01' && $trainer_info_current['date_of_charge'] != '0000-00-00'){
				$data['date_charge_trainer']= date('d-m-Y', strtotime($trainer_info_current['date_of_charge']));
			} else {
			$data['date_charge_trainer'] = '';
			}
		} else {
			$data['date_charge_trainer'] = '';
		}

		if (isset($this->request->post['date_left_trainer'])) {
			$data['date_left_trainer'] = $this->request->post['date_left_trainer'];
		} elseif (!empty($trainer_info_current)) {
			if($trainer_info_current['left_date_of_charge'] != '1970-01-01' && $trainer_info_current['left_date_of_charge'] != '0000-00-00'){
				$data['date_left_trainer']= date('d-m-Y', strtotime($trainer_info_current['left_date_of_charge']));
			} else {
			$data['date_left_trainer'] = '';
			}
		} else {
			$data['date_left_trainer'] = '';
		}

		if (isset($this->request->post['registerarton_authentication'])) {
			$data['registerarton_authentication'] = $this->request->post['registerarton_authentication'];
		} elseif (!empty($horse_info)) {
			$data['registerarton_authentication'] = $horse_info['reg_auth_id'];
		} else {
			$data['registerarton_authentication'] = '';
		}

		if (isset($this->request->post['sire'])) {
			$data['sire'] = $this->request->post['sire'];
		} elseif (!empty($horse_info)) {
			$data['sire'] = $horse_info['sire_name'];
		} else {
			$data['sire'] = '';
		}

		if (isset($this->request->post['dam'])) {
			$data['dam'] = $this->request->post['dam'];
		} elseif (!empty($horse_info)) {
			$data['dam'] = $horse_info['dam_name'];
		} else {
			$data['dam'] = '';
		}


		if (isset($this->request->post['color'])) {
			$data['color'] = $this->request->post['color'];
		} elseif (!empty($horse_info)) {
			$data['color'] = $horse_info['color'];
		} else {
			$data['color'] = '';
		}

		if (isset($this->request->post['origin'])) {
			$data['origin'] = $this->request->post['origin'];
		} elseif (!empty($horse_info)) {
			$data['origin'] = $horse_info['orgin'];
		} else {
			$data['origin'] = '';
		}

		if (isset($this->request->post['sex'])) {
			$data['sex'] = $this->request->post['sex'];
		} elseif (!empty($horse_info)) {
			$data['sex'] = $horse_info['sex'];
		} else {
			$data['sex'] = '';
		}

		if (isset($this->request->post['stud_form'])) {
			$data['stud_form'] = $this->request->post['stud_form'];
		} elseif (!empty($horse_info)) {
			$data['stud_form'] = $horse_info['stud_form'];
		} else {
			$data['stud_form'] = '';
		}

		if (isset($this->request->post['breeder'])) {
			$data['breeder'] = $this->request->post['breeder'];
		} elseif (!empty($horse_info)) {
			$data['breeder'] = $horse_info['breeder_name'];
		} else {
			$data['breeder'] = '';
		}

		if (isset($this->request->post['saddle_no'])) {
			$data['saddle_no'] = $this->request->post['saddle_no'];
		} elseif (!empty($horse_info)) {
			$data['saddle_no'] = $horse_info['saddle_no'];
		} else {
			$data['saddle_no'] = '';
		}

		if (isset($this->request->post['saddle_no'])) {
			$data['saddle_no'] = $this->request->post['saddle_no'];
		} elseif (!empty($horse_info)) {
			$data['saddle_no'] = $horse_info['saddle_no'];
		} else {
			$data['saddle_no'] = '';
		}

		if (isset($this->request->post['chip_no_one'])) {
			$data['chip_no_one'] = $this->request->post['chip_no_one'];
		} elseif (!empty($horse_info)) {
			$data['chip_no_one'] = $horse_info['chip_no1'];
		} else {
			$data['chip_no_one'] = '';
		}

		if (isset($this->request->post['chip_no_two'])) {
			$data['chip_no_two'] = $this->request->post['chip_no_two'];
		} elseif (!empty($horse_info)) {
			$data['chip_no_two'] = $horse_info['chip_no2'];
		} else {
			$data['chip_no_two'] = '';
		}

		if (isset($this->request->post['arrival_charges_to_be_paid'])) {
			$data['arrival_charges_to_be_paid'] = $this->request->post['arrival_charges_to_be_paid'];
		} elseif (!empty($horse_info)) {
			$data['arrival_charges_to_be_paid'] = $horse_info['arrival_charges_to_be_paid'];
		} else {
			$data['arrival_charges_to_be_paid'] = '';
		}

		if (isset($this->request->post['age'])) {
			$data['age'] = $this->request->post['age'];
		} elseif (!empty($horse_info)) {
			$data['age'] = $horse_info['age'];
		} else {
			$data['age'] = '';
		}

		//echo "<pre>";print_r($this->request->post);exit;

		/*if (isset($this->request->post['age'])) {
			$data['age'] = $this->request->post['age'];
		} else {
			$data['age'] = '';
		}*/

		if (isset($this->request->post['rating'])) {
			$data['rating'] = $this->request->post['rating'];
		} elseif (!empty($horse_info)) {
			$data['rating'] = $horse_info['rating'];
		} else {
			$data['rating'] = '';
		}

		if (isset($this->request->post['octroi_details'])) {
			$data['octroi_details'] = $this->request->post['octroi_details'];
		} elseif (!empty($horse_info)) {
			$data['octroi_details'] = $horse_info['octroi_details'];
		} else {
			$data['octroi_details'] = '';
		}

		if (isset($this->request->post['awbi_registration_no'])) {
			$data['awbi_registration_no'] = $this->request->post['awbi_registration_no'];
		} elseif (!empty($horse_info)) {
			$data['awbi_registration_no'] = $horse_info['awbi_registration_no'];
		} else {
			$data['awbi_registration_no'] = '';
		}

		if (isset($this->request->post['awbi_registration_file'])) {
			$data['awbi_registration_file'] = $this->request->post['awbi_registration_file'];
		} elseif (!empty($horse_info)) {
			$data['awbi_registration_file'] = $horse_info['awbi_registration_file'];
		} else {
			$data['awbi_registration_file'] = '';
		}

		if (isset($this->request->post['awbi_registration_file'])) {
			$data['awbi_registration_file'] = $this->request->post['awbi_registration_file'];
			$data['awbi_registration_file_source'] = $this->request->post['awbi_registration_file_source'];
		} elseif (!empty($horse_info)) {
			$data['awbi_registration_file'] = $horse_info['awbi_registration_file'];
			$data['awbi_registration_file_source'] = $horse_info['awbi_registration_file_source'];
		} else {	
			$data['awbi_registration_file'] = '';
			$data['awbi_registration_file_source'] = '';
		}

		if (isset($this->request->post['Id_by_brand'])) {
			$data['Id_by_brand'] = $this->request->post['Id_by_brand'];
		} elseif (!empty($horse_info)) {
			$data['Id_by_brand'] = $horse_info['Id_by_brand'];
		} else {
			$data['Id_by_brand'] = '';
		}

		if (isset($this->request->post['std_stall_remarks'])) {
			$data['std_stall_remarks'] = $this->request->post['std_stall_remarks'];
		} elseif (!empty($horse_info)) {
			$data['std_stall_remarks'] = $horse_info['std_stall_remarks'];
		} else {
			$data['std_stall_remarks'] = '';
		}

		if (isset($this->request->post['horse_remarks'])) {
			$data['horse_remarks'] = $this->request->post['horse_remarks'];
		} elseif (!empty($horse_info)) {
			$data['horse_remarks'] = $horse_info['horse_remarks'];
		} else {
			$data['horse_remarks'] = '';
		}

		if(isset($this->request->get['show_owner_tab'])){
			$data['show_owner_tab'] = $this->request->get['show_owner_tab'];
		} else {
			$data['show_owner_tab'] = 0;
		}

		if (isset($this->request->post['trainer_name'])) {
			$data['trainer_name'] = $this->request->post['trainer_name'];
		} elseif (!empty($trainer_info_current)) {
			$data['trainer_name'] = $trainer_info_current['trainer_name'];
		} else {
			$data['trainer_name'] = '';
		}


		if (isset($this->request->post['trainer_id'])) {
			$data['trainer_id'] = $this->request->post['trainer_id'];
		} elseif (!empty($trainer_info_current)) {
			$data['trainer_id'] = $trainer_info_current['trainer_id'];
		} else {
			$data['trainer_id'] = '';
		}

		if (isset($this->request->post['trainer_codes_id'])) {
			$data['trainer_codes_id'] = $this->request->post['trainer_codes_id'];
		} elseif (!empty($trainer_info_current)) {
			$data['trainer_codes_id'] = $trainer_info_current['trainer_code'];
		} else {
			$data['trainer_codes_id'] = '';
		}

		if (isset($this->request->post['arrival_time_trainers'])) {
			$data['arrival_time_trainers'] = $this->request->post['arrival_time_trainers'];
		} elseif (!empty($trainer_info_current)) {
			$data['arrival_time_trainers'] = $trainer_info_current['arrival_time'];
		} else {
			$data['arrival_time_trainers'] = '';
		}

		if (isset($this->request->post['extra_narrration_trainer'])) {
			$data['extra_narrration_trainer'] = $this->request->post['extra_narrration_trainer'];
		} elseif (!empty($trainer_info_current)) {
			$data['extra_narrration_trainer'] = $trainer_info_current['extra_narration'];
		} else {
			$data['extra_narrration_trainer'] = '';
		}

		if (isset($this->request->post['extra_narrration_two_trainer'])) {
			$data['extra_narrration_two_trainer'] = $this->request->post['extra_narrration_two_trainer'];
		} elseif (!empty($trainer_info_current)) {
			$data['extra_narrration_two_trainer'] = $trainer_info_current['extra_narration_two'];
		} else {
			$data['extra_narrration_two_trainer'] = '';
		}
		
		$data['path'] = '';

		$this->load->model('catalog/filter');

		if (isset($this->request->post['category_filter'])) {
			$filters = $this->request->post['category_filter'];
		} elseif (isset($this->request->get['category_id'])) {
			$filters = $this->model_catalog_horse->getCategoryFilters($this->request->get['category_id']);
		} else {
			$filters = array();
		}

		$data['category_filters'] = array();

		foreach ($filters as $filter_id) {
			$filter_info = $this->model_catalog_filter->getFilter($filter_id);

			if ($filter_info) {
				$data['category_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}

		$this->load->model('setting/store');
		$product_specials = array();
		$data['product_specials'] = $product_specials;

		$product_recurrings = array();
		$data['product_recurrings'] = $product_recurrings;

		$data['recurrings'] = array();
		$data['customer_groups'] = array();
		$data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['category_store'])) {
			$data['category_store'] = $this->request->post['category_store'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_store'] = $this->model_catalog_horse->getCategoryStores($this->request->get['category_id']);
		} else {
			$data['category_store'] = array(0);
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($category_info)) {
			$data['keyword'] = $category_info['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($category_info)) {
			$data['image'] = $category_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($category_info) && is_file(DIR_IMAGE . $category_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($category_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['top'])) {
			$data['top'] = $this->request->post['top'];
		} elseif (!empty($category_info)) {
			$data['top'] = $category_info['top'];
		} else {
			$data['top'] = 0;
		}

		if (isset($this->request->post['column'])) {
			$data['column'] = $this->request->post['column'];
		} elseif (!empty($category_info)) {
			$data['column'] = $category_info['column'];
		} else {
			$data['column'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($category_info)) {
			$data['sort_order'] = $category_info['sort_order'];
		} else {
			$data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = true;
		}

		if (isset($this->request->post['category_layout'])) {
			$data['category_layout'] = $this->request->post['category_layout'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_layout'] = $this->model_catalog_horse->getCategoryLayouts($this->request->get['category_id']);
		} else {
			$data['category_layout'] = array();
		}

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/horse_form', $data));
	}

	function ischecktDate($string) { 
		$matches = array();
	    $pattern = '/^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/';
	    if (!preg_match($pattern, $string, $matches)) return false;
	    if (!checkdate($matches[2], $matches[1], $matches[3])) return false;
	    return true;
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/horse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		//echo '<pre>';print_r($this->request->post);exit;
		if($this->request->post['horse_name'] == ''){ 
			$this->error['valierr_horse_name'] = 'Please Enter Hourse Name';
		} 

		if($this->request->post['registeration_date'] == '') {
			$this->error['valierr_registeration_date'] = 'Please Enter Valid Date';
		}

		if (!$this->ischecktDate($this->request->post['registeration_date'])) { //echo "innn";exit;
			$this->error['valierr_registeration_date'] = 'Please Enter Valid Date';
			$this->request->post['registeration_date'] = '';
		}

		if($this->request->post['date_foal_date'] == '') {
			$this->error['valierr_foal_date'] = 'Please Enter Valid Date';
		}

		if (!$this->ischecktDate($this->request->post['date_foal_date'])) {
			$this->error['valierr_foal_date'] = 'Please Enter Valid Date';
			$this->request->post['date_foal_date'] = '';
		}

		if($this->request->post['official_name_change'] == 1){
			if($this->request->post['changehorse_name'] == ''){
				$this->error['valierr_change_horse_name'] = 'Please Enter Changes Hourse Name';
			} 
			if(($this->request->post['change_horse_dates'] == '')) {
				$this->error['valierr_change_horse_date'] = 'Please Select Valid Date';
			}
			if (!$this->ischecktDate($this->request->post['change_horse_dates'])) {
				$this->error['valierr_change_horse_date'] = 'Please Enter Valid Date';
				$this->request->post['change_horse_dates'] = '';
			}
		}

		if($this->request->post['date_std_stall_certificate'] == '') {
			$this->error['valierr_std_stall_certificate_date'] = 'Please Enter Valid Date';
		}

		if (!$this->ischecktDate($this->request->post['date_std_stall_certificate'])) {
			$this->error['valierr_std_stall_certificate_date'] = 'Please Enter Valid Date';
			$this->request->post['date_std_stall_certificate'] = '';
		}

		if($this->request->post['passport_no'] == ''){  
			$this->error['valierr_passport_no'] = 'Please Enter Passport/Life Number';
		} 

		if($this->request->post['registerarton_authentication'] == ''){
			$this->error['valierr_registerarton_authentication'] = 'Please Select Registration Auth';
		} 

		if($this->request->post['sire'] == ''){
			$this->error['valierr_sire'] = 'Please Enter Sire/ Nat';
		} 

		if($this->request->post['dam'] == ''){
			$this->error['valierr_dam'] = 'Please Enter Dam/ Nat';
		} 

		if($this->request->post['chip_no_one'] == ''){
			$this->error['valierr_chip_no_one'] = 'Please Enter Micro Chip no 1';
		}

		if($this->request->post['arrival_charges_to_be_paid'] == ''){
			$this->error['valierr_arrival_charges_to_be_paid'] = 'Please Enter Arrival Charges to be Paid';
		} 

		if($this->request->post['color'] == ''){
			$this->error['valierr_color'] = 'Please Enter Color';
		}

		if($this->request->post['origin'] == ''){
			$this->error['valierr_origin'] = 'Please Select Origin';
		} 

  		if($this->request->post['sex'] == ''){
			$this->error['valierr_sex'] = 'Please Select Sex';
		} 

		/*if($this->request->post['age'] == ''){
			$this->error['valierr_age'] = 'Please Enter Age';
		} */

		if($this->request->post['stud_form'] == ''){
			$this->error['valierr_stud_form'] = 'Please Enter Stud Farm';
		} 

		if($this->request->post['breeder'] == ''){
			$this->error['valierr_breeder'] = 'Please Enter Breeder';
		} 

		if($this->request->post['saddle_no'] == ''){
			$this->error['valierr_saddle_no'] = 'Please Enter Saddle No';
		} 

		if($this->request->post['octroi_details'] == ''){
			$this->error['valierr_octroi_details'] = 'Please Enter Octroi Details';
		} 

		if($this->request->post['awbi_registration_no'] == ''){
			$this->error['valierr_awbi_registration_no'] = 'Please Enter AWBI Registration No';
		} 

		if($this->request->post['Id_by_brand'] == ''){
			$this->error['valierr_Id_by_brand'] = 'Please Enter ID By Brand';
		} 

		if($this->request->post['std_stall_remarks'] == ''){
			$this->error['valierr_std_stall_remarks'] = 'Please Enter St Stall Remark';
		} 

		if($this->request->post['horse_remarks'] == ''){
			$this->error['valierr_horse_remarks'] = 'Please Enter Remarks';
		} 

		if($this->request->post['trainer_name'] == ''){
			$this->error['valierr_trainer_name'] = 'Please Choose Trainer Name';
		} 

		if($this->request->post['date_charge_trainer'] == '') {
			$this->error['valierr_date_charge_trainer'] = 'Please Select Valid Date';
		}

		if (!$this->ischecktDate($this->request->post['date_charge_trainer'])) {
			$this->error['valierr_date_charge_trainer'] = 'Please Enter Valid Date';
			$this->request->post['date_charge_trainer'] = '';
		} 
		

		if($this->request->post['arrival_time_trainers'] == ''){
			$this->error['valierr_arrival_time_trainer'] = 'Please Select Arrival Time';
		}

		if($this->request->post['extra_narrration_trainer'] == ''){
			$this->error['valierr_extra_narrration_trainer'] = 'Please Enter Extra Narration';
		} 

		if($this->request->post['extra_narrration_two_trainer'] == ''){
			$this->error['valierr_extra_narrration_two_trainer'] = 'Please Enter Extra Narration 2';
		} 
		
		if($this->request->post['date_left_trainer'] == '') {
			$this->error['valierr_date_left_trainer'] = 'Please Select Valid Date';
		}

		if (!$this->ischecktDate($this->request->post['date_left_trainer'])) {
			$this->error['valierr_date_left_trainer'] = 'Please Enter Valid Date';
			$this->request->post['date_left_trainer'] = '';
		}

		if($this->ischecktDate($this->request->post['date_charge_trainer']) == 'true' && $this->request->post['date_left_trainer'] != ''){
			$today1 = date('Y-m-d', strtotime($this->request->post['date_charge_trainer']));
			$today2 = date('Y-m-d', strtotime($this->request->post['date_left_trainer']));
			if($today2 < $today1){ 
				$this->error['valierr_date_left_trainer'] = 'Left Charge Must Be Greater';
			} elseif($today1 == $today2){
				$this->error['valierr_date_charge_trainer'] = 'Date Of Charge Not Be Same';
				$this->error['valierr_date_left_trainer'] = 'Left Charge Not Be Same';
			}
		}


		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/horse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/horse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocompleteFromOwner() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['from_owner_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesFromOwnerAuto($this->request->get['from_owner_name']);

			foreach ($results as $result) {
				$json[] = array(
					'fromowner_code' => $result['id'],
					'fromowner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['fromowner_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	

	public function autocompleteToOwner() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['to_owner_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesToOwnerAuto($this->request->get['to_owner_name']);

			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'toowner_code' => $result['id'],
						'toowner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['toowner_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function autocompleteTrainer() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['trainer_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesToTrainerAuto($this->request->get['trainer_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'trainer_code' => $result['trainer_code'],
						'trainer_id' => $result['id'],
						'trainer_name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteEquipement() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['equipment_name'])) {
			$this->load->model('catalog/horse');

			$results = $this->model_catalog_horse->getHorsesequipments($this->request->get['equipment_name']);

			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'equipment_name'        => strip_tags(html_entity_decode($result['equipment_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['equipment_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}
	

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/horse');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_horse->getHorses($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function upload() {
		//$this->load->language('catalog/employee');
		$json = array();
		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/horse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$file_show_path = HTTP_CATALOG.'system/storage/download/horse/';
		$file_upload_path = DIR_DOWNLOAD.'horse/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				$this->log->write(print_r($this->request->files, true));
				$this->log->write($image_name);
				$this->log->write($img_extension);
				$this->log->write($filename);

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';
				$allowed[] = 'docx';
				$allowed[] = 'doc';

				$this->log->write(print_r($allowed, true));

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] ='Please upload valid file!'. $this->request->files['file']['error'];
				}
			} else {
				$json['error'] ='Please upload valid file!';
			}
		}

		if (!$json) {
			$file = $filename;
			if(move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file)){
				$destFile = $file_upload_path . $file;
				//chmod($destFile, 0777);
				$json['filename'] = $file;
				$json['link_href'] = $file_show_path . $file;

				$json['success'] = 'Your file was successfully uploaded!';
			} else {
				$json['error'] = 'Please upload valid file!';
			}
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function deletebandeatils() {
		$json = array();

		if (isset($this->request->get['hourse_id']) && isset($this->request->get['hourse_ban_id'])) {

			$this->db->query("DELETE FROM `horse_ban` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_ban_id = '".$this->request->get['hourse_ban_id']."'");
			$this->log->write("DELETE FROM `horse_ban` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_ban_id = '".$this->request->get['hourse_ban_id']."'");

			$json['success'] = 'Delete Record Sucessfully';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function deleteShoeDeatils() {
		$json = array();

		if (isset($this->request->get['hourse_id']) && isset($this->request->get['hourse_shoeing_id'])) {

			$this->db->query("DELETE FROM `horse_shoeing` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_shoeing_id = '".$this->request->get['hourse_shoeing_id']."'");
			$this->log->write("DELETE FROM `horse_shoeing` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_shoeing_id = '".$this->request->get['hourse_shoeing_id']."'");

			$json['success'] = 'Delete Record Sucessfully';
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function deleteStackDeatils() {
		$json = array();

		if (isset($this->request->get['hourse_id']) && isset($this->request->get['hourse_stack_id'])) {

			$this->db->query("DELETE FROM `horse_stackoutstation` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_stackoutstation_id = '".$this->request->get['hourse_stack_id']."'");
			$this->log->write("DELETE FROM `horse_stackoutstation` WHERE horse_id = '" .(int)$this->request->get['hourse_id'] . "' AND horse_stackoutstation_id = '".$this->request->get['hourse_stack_id']."'");

			$json['success'] = 'Delete Record Sucessfully';
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
