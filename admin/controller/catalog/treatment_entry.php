<?php
/*require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;*/
date_default_timezone_set("Asia/Kolkata");
class Controllercatalogtreatmententry extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/treatment_entry');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/treatment_entry');
		$this->load->model('catalog/product');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/treatment_entry');
		/*$this->load->model('catalog/product');*/

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/treatment_entry');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_treatment_entry->addinward($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	
	protected function getList() {
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		if (isset($this->request->get['filter_inward'])) {
			$filter_inward = $this->request->get['filter_inward'];
		} else {
			$filter_inward = null;
		}

		if (isset($this->request->get['filter_order_no'])) {
			$filter_order_no = $this->request->get['filter_order_no'];
		} else {
			$filter_order_no = null;
		}

		if (isset($this->request->get['filter_medicine'])) {
			$filter_medicine = $this->request->get['filter_medicine'];
		} else {
			$filter_medicine = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['filter_dates'])) {
			$filter_dates = $this->request->get['filter_dates'];
		} else {
			$filter_dates = null;
		}

		if (isset($this->request->get['filter_productsort'])) {
			$filter_productsort = $this->request->get['filter_productsort'];
		} else {
			$filter_productsort = null;
		}

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$filter_inward_category = $this->request->get['filter_inward_category'];
		} else {
			$filter_inward_category = null;
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$filter_inward_category_id = $this->request->get['filter_inward_category_id'];
		} else {
			$filter_inward_category_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}


		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/treatment_entry', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/treatment_entry/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['stock'] = $this->url->link('catalog/treatment_entry/stock', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/treatment_entry/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['inwards'] = array();

		$filter_data = array(
			'filter_inward'	  => $filter_inward,
			'filter_order_no'	  => $filter_order_no,
			'filter_medicine'	  => $filter_medicine,
			'filter_date'	  => $filter_date,
			'filter_dates'	  => $filter_dates,
			'filter_order_id'  => $filter_order_id,
			'filter_inward_category'	  => $filter_inward_category,
			'filter_inward_category_id'  => $filter_inward_category_id,
			'filter_productsort'	=> $filter_productsort,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
			//echo '<pre>';
			 //print_r($filter_data);
			//exit;
		$results = array();
		//$inward_total = $this->model_catalog_inward->getTotalinwards($filter_data);
		$inward_total = 0;
		//$results = $this->model_catalog_inward->getinwards($filter_data);
		$user_group_id = $this->user->getGroupId();

			 //echo '<pre>';
			 //print_r($results);
			 //echo $user_group_id;
			 //exit;
			
	/*	foreach ($results as $result) {

			$user_group_id = $this->user->getGroupId();
			if ($user_group_id == '14') {
				if ($result['status'] != '0') {
					$anchor_styles = "color: #666666 !important; cursor:pointer;";
				$data['inwards'][] = array(
				
					'order_id' => $result['order_id'],
					'order_no' => $result['order_no'],
					'supplier' => $result['supplier'],
					'date' => $result['date'],
					
					'status' => $result['status'],
				
					'view_status' => $result['view_status'],
					'approve'     => $this->url->link('catalog/treatment_entry/approve', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
					'edit'     => $this->url->link('catalog/treatment_entry/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
					'edit1'     => $this->url->link('catalog/treatment_entry/edit1', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
					'print'     => $this->url->link('catalog/treatment_entry/prints', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
					'calculate'     => $this->url->link('catalog/treatment_entry/calculate', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true)
				);
			}
			} else {
				$anchor_styles = "color: #666666 !important; cursor:pointer;";
				$data['inwards'][] = array(
					
					'order_id' => $result['order_id'],
					'order_no' => $result['order_no'],
					'supplier' => $result['supplier'],
					'date' => $result['date'],
					//'indent_no' => $result['indent_no'],
					'status' => $result['status'],
					//'pose_no' => $result['pose_no'],
					'view_status' => $result['view_status'],
					'approve'     => $this->url->link('catalog/treatment_entry/approve', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
					'edit'     => $this->url->link('catalog/treatment_entry/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
					'edit1'     => $this->url->link('catalog/treatment_entry/edit1', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
					'print'     => $this->url->link('catalog/treatment_entry/prints', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
					'calculate'     => $this->url->link('catalog/treatment_entry/calculate', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true)
				);
			}
			
		}*/

		$issue_nos = $this->db->query("SELECT issue_no FROM oc_treatment_entry WHERE 1=1 ORDER BY issue_no DESC LIMIT 1 ");
		/*echo '<pre>';
		print_r($issue_nos->row);
		exit;*/
		if ($issue_nos->num_rows > 0) {
			$data['input_isse_number'] = $issue_nos->row['issue_no'] + 1;
		} else {
			$data['input_isse_number'] = 1;
		}

		$data['productsorts'] = array(
			'a'=>'A','b'=>'B','c'=>'C','d'=>'D','e'=>'E','f'=>'F','g'=>'G','h'=>'H',
			'i'=>'I','j'=>'J','k'=>'K','l'=>'L','m'=>'M','n'=>'N','o'=>'O','p'=>'P',
			'q'=>'Q','r'=>'R','s'=>'S','t'=>'T','u'=>'U','v'=>'V','w'=>'W','x'=>'X',
			'y'=>'Y','z'=>'Z','Other'
		);
			// echo '<pre>';
			// print_r($data['productsorts']);
			// exit;
		$data['user_group_id'] = $this->user->getGroupId();
		//$data['user_type'] = $this->user->getUserType();	
		$data['base_link_1'] = $this->url->link('catalog/treatment_entry/edit', 'token=' . $this->session->data['token']);
		$data['base_link'] = $this->url->link('catalog/treatment_entry/edit', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/treatment_entry', 'token=' . $this->session->data['token'] . '&sort=inward_name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/treatment_entry', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $inward_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/treatment_entry', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($inward_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($inward_total - $this->config->get('config_limit_admin'))) ? $inward_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $inward_total, ceil($inward_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_inward_category'] = $filter_inward_category;
		$data['filter_inward_category_id'] = $filter_inward_category_id;
		$data['filter_inward'] = $filter_inward;
		$data['filter_order_no'] = $filter_order_no;
		$data['filter_medicine'] = $filter_medicine;
		$data['filter_date'] = $filter_date;
		$data['filter_dates'] = $filter_dates;
		$data['filter_productsort'] = $filter_productsort;
		$data['filter_order_id'] = $filter_order_id;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];
		
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		$this->response->setOutput($this->load->view('catalog/treatment_entry_list', $data));
	}

	protected function getForm() {
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['error_medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['error_medicine_error'] = '';
		}

		if (isset($this->error['day_close_error'])) {
			$data['day_close_error'] = $this->error['day_close_error'];
		} else {
			$data['day_close_error'] = '';
		}


		if (isset($this->error['valierr_trainer'])) {
			$data['valierr_trainer'] = $this->error['valierr_trainer'];
		} else {
			$data['valierr_trainer'] = '';
		}

		if (isset($this->error['valierr_horse'])) {
			$data['valierr_horse'] = $this->error['valierr_horse'];
		} else {
			$data['valierr_horse'] = '';
		}

		if (isset($this->error['valierr_doctor'])) {
			$data['valierr_doctor'] = $this->error['valierr_doctor'];
		} else {
			$data['valierr_doctor'] = '';
		}

		if (isset($this->error['valierr_clinic'])) {
			$data['valierr_clinic'] = $this->error['valierr_clinic'];
		} else {
			$data['valierr_clinic'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_quantity'])) {
			$url .= '&filter_inward_quantity=' . urlencode(html_entity_decode($this->request->get['filter_inward_quantity'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['previous'] = $this->url->link('catalog/treatment_entry', 'token=' . $this->session->data['token'], true);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/treatment_entry', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/treatment_entry/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/treatment_entry/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/treatment_entry', 'token=' . $this->session->data['token'] . $url, true);
		$data['treatment_entry'] = $this->url->link('catalog/treatment_entry_single/add', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['order_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$inward_info = $this->model_catalog_inward->getinward($this->request->get['order_id']);
		}

		$data['token'] = $this->session->data['token'];

		

		$issue_nos = $this->db->query("SELECT issue_no FROM oc_treatment_entry WHERE 1=1 ORDER BY issue_no DESC LIMIT 1 ");
		/*echo '<pre>';
		print_r($issue_nos->row);
		exit;*/
		if ($issue_nos->num_rows > 0) {
			$data['input_isse_number'] = $issue_nos->row['issue_no'] + 1;
		} else {
			$data['input_isse_number'] = 1;
		}


		
		if (isset($this->request->post['filter_parent_doctor'])) {
			$data['filter_parent_doctor'] = $this->request->post['filter_parent_doctor'];
		} elseif (!empty($transection_info_parent)) {
			$data['filter_parent_doctor'] = $transection_info_parent['clinic_id'];
		} else {
			$data['filter_parent_doctor'] = '';
		}

		if (isset($this->request->post['filterParentId'])) {
			$data['filter_parent_doctor_id'] = $this->request->post['filterParentId'];
		} elseif (!empty($transection_info_parent)) {
			$data['filter_parent_doctor_id'] = $transection_info_parent['clinic_name'];
		} else {
			$data['filter_parent_doctor_id'] = '';
		}

		if (isset($this->request->post['horse_datas'])) {
			$data['post_horses'] = $this->request->post['horse_datas'];
			$results = $this->model_catalog_treatment_entry->getHorseAutos($this->request->post['hidden_trainer_name_id']);
			
			$json = array();
			foreach ($results as $result) {
				if($result['horse_id']){
					$horse_sql =  $this->db->query("SELECT official_name FROM `horse1` WHERE horseseq = '".$result['horse_id']."'");
					if ($horse_sql->num_rows > 0) {
						$horse_name = $horse_sql->row['official_name'];
						$json[] = array(
							'horse_id' => $result['horse_id'],
							'horse_name' => $horse_name,
						);
					}
				}
			}

			// echo'<pre>';
			// print_r($json);
			// exit;
			$data['horse_datas'] = $json;
		}  else {
			$data['horse_datas'] = array();
			$data['post_horses'] = array();
		}

		

		if (isset($this->request->post['trainer_name'])) {
			$data['trainer_name'] = $this->request->post['trainer_name'];
		} elseif (!empty($transection_info_parent)) {
			$data['trainer_name'] = $transection_info_parent['trainer_name'];
		} else {
			$data['trainer_name'] = '';
		}

		if (isset($this->request->post['hidden_trainer_name_id'])) {
			$data['trainer_name_id'] = $this->request->post['hidden_trainer_name_id'];
		} elseif (!empty($transection_info_parent)) {
			$data['trainer_name_id'] = $transection_info_parent['trainer_name_id'];
		} else {
			$data['trainer_name_id'] = '';
		}

		if (isset($this->request->post['date'])) {
			$data['date'] = $this->request->post['date'];
		} elseif (!empty($transection_info_parent)) {
			$data['date'] = $transection_info_parent['entry_date'];
		} else {
			$data['date'] = date('d-m-Y');
		}
		
		if (isset($this->request->post['total_item'])) {
			$data['total_item'] = $this->request->post['total_item'];
		} elseif (!empty($transection_info_parent)) {
			$data['total_item'] = $transection_info_parent['total_item'];
		} else {
			$data['total_item'] =0;
		}

		if (isset($this->request->post['total_qty'])) {
			$data['total_qty'] = $this->request->post['total_qty'];
		} elseif (!empty($transection_info_parent)) {
			$data['total_qty'] = $transection_info_parent['total_qty'];
		} else {
			$data['total_qty'] =0;
		}

		if (isset($this->request->post['total'])) {
			$data['total_amt'] = $this->request->post['total'];
		} elseif (!empty($transection_info_parent)) {
			$data['total_amt'] = $transection_info_parent['total_amt'];
		} else {
			$data['total_amt'] =0;
		}

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} else {
			$data['productraw_datas'] = array();
		}

		if (isset($this->request->post['to_doc']) && $this->request->post['to_doc'] != '') {
			$data['to_doc'] = $this->request->post['to_doc'];
			$data['child_doctor_name'] = $this->db->query("SELECT * FROM doctor WHERE id = '".$this->request->post['to_doc']."'	")->row['doctor_name'];
			$results =$this->db->query("SELECT * FROM  doctor WHERE isActive = 'Active' AND is_parent = 0 AND `parent_id` = '" . ($this->request->post['filterParentId']) . "' ")->rows;
			
			$json = array();
			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}

			$data['all_docs'] = $json;
		} elseif (!empty($transection_info_parent)) {
			$data['to_doc'] = $transection_info_parent['doctor_id'];
			$data['child_doctor_name'] = $this->db->query("SELECT * FROM doctor WHERE id = '".$transection_info_parent['doctor_id']."'	")->row['doctor_name'];
			$results =$this->db->query("SELECT * FROM  doctor WHERE isActive = 'Active' AND is_parent = 0 AND `parent_id` = '" . ($transection_info_parent['clinic_id']) . "' ")->rows;
			
			$json = array();
			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}

			$data['all_docs'] = $json;

		} else {
			$data['to_doc'] = '';
			$data['all_docs'] = array();
		}

		$data['user_log_grp_id'] = $this->user->getGroupId();

		$data['user_log_id'] = $this->user->getId();

		// echo $data['to_doc'];
		// exit;
		//$data['user_type'] = $this->user->getUserType();	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/treatment_entry_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/treatment_entry')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		// echo '<pre>';
		// print_r($this->request->post);
		// exit;
		$datas = $this->request->post;

		if ((utf8_strlen($this->request->post['filter_parent_doctor']) == '')) {
			$this->error['valierr_clinic'] = "Please Enter Clinic Name";
		}

		// if (isset($this->request->post['horse_datas']) && (($this->request->post['horse_datas']))) {
			
		// 	$this->error['valierr_horse'] = "Please Select Horse";
				
		// }

		// if (isset($this->request->post['to_doc']) && (utf8_strlen($this->request->post['to_doc']) == '')) {
		// 	$this->error['valierr_doctor'] = "Please Select Doctor";
		// }

		if ((utf8_strlen($this->request->post['trainer_name']) == '')) {
			$this->error['valierr_trainer'] = "Please Select Trainer";
		}
		$chceck_day_close_product = $this->db->query("SELECT * FROM daily_reconsilation WHERE entry_date = '".$this->db->escape(date('Y-m-d', strtotime($this->request->post['date'])))."'  ");
		
		if($chceck_day_close_product->num_rows > 0){
			$this->error['day_close_error'] = "Medicine Not Transfer Because Day Is Not Closed!";
		}

		if(!isset($this->request->post['productraw_datas'])){
			$this->error['medicine_error'] = ''." Please Enter Atleast Transfer Entry";
		} 
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/treatment_entry')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
	public function autocomplete() {
		$json = array();
		 /*echo '<pre>';
		 print_r($this->request->get);
		 exit;*/
		if (isset($this->request->get['filter_order_id'])) {
			$this->load->model('catalog/treatment_entry');

			$filter_data = array(
				'filter_order_id' => $this->request->get['filter_order_id'],
				//'start'       => 0,
				//'limit'       => 5
			);
			$results = $this->model_catalog_inward->getinwards($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'order_id' => $result['order_id'],
					'order_no'     => strip_tags(html_entity_decode($result['order_no'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['order_no'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	

	public function autocomplete1() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/treatment_entry');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
			);
			$results = $this->model_catalog_inward->getinwards1($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'order_id' => $result['order_id'],
					'order_no'     => strip_tags(html_entity_decode($result['order_no'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['order_no'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		}

	public function autocomplete_raw() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/treatment_entry');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_inward->getProductnews($filter_data);

			foreach ($results as $result) {
				//echo "<pre>";print_r($result);exit;
				$quantity = 1;
				$json[] = array(
					'id' => $result['id'],
					'med_code' => strip_tags(html_entity_decode($result['med_code'], ENT_QUOTES, 'UTF-8')),
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8')),
					'price'     => strip_tags(html_entity_decode($result['unit_cost'], ENT_QUOTES, 'UTF-8')),
					'store_unit' => strip_tags(html_entity_decode($result['store_unit'], ENT_QUOTES, 'UTF-8')),
					'unit'     => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8')),
					'packing'     => strip_tags(html_entity_decode($result['pack_type'], ENT_QUOTES, 'UTF-8')),
					'volume'     => strip_tags(html_entity_decode($result['volume'], ENT_QUOTES, 'UTF-8')),
					'gst_rate'     => strip_tags(html_entity_decode($result['gst_rate'], ENT_QUOTES, 'UTF-8')),
					'quantity' => 1,
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['med_name'];
			
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		// echo'<pre>';
		// print_r($json);
		// exit;
	}

	public function autocomplete_raw_code() {
		$json = array();

		if (isset($this->request->get['med_code'])) {
			$this->load->model('catalog/treatment_entry');

			$filter_data = array(
				'med_code' => $this->request->get['med_code'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_inward->getProductcode($filter_data);

			foreach ($results as $result) {
				$quantity = 1;
				$json = array(
					'id' => $result['id'],
					'med_code' => strip_tags(html_entity_decode($result['med_code'], ENT_QUOTES, 'UTF-8')),
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8')),
					'quantity' => 1,
					'price'     => strip_tags(html_entity_decode($result['unit_cost'], ENT_QUOTES, 'UTF-8')),
					'store_unit' => strip_tags(html_entity_decode($result['store_unit'], ENT_QUOTES, 'UTF-8')),
					'unit'     => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8')),
					'packing'     => strip_tags(html_entity_decode($result['pack_type'], ENT_QUOTES, 'UTF-8')),
					'volume'     => strip_tags(html_entity_decode($result['volume'], ENT_QUOTES, 'UTF-8')),
					'gst_rate'     => strip_tags(html_entity_decode($result['gst_rate'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['med_name'];
			
		// }

		// array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	
	
	
	public function autocompleteParentDoc() {
		//echo 'in';
		$json = array();
		//echo '<pre>';print_r($this->request->get);exit;

		if (isset($this->request->get['filter_doctor_code'])) {
			$this->load->model('catalog/treatment_entry');

			$results = $this->model_catalog_treatment_entry->getDoctorAutos($this->request->get['filter_doctor_code']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'is_parent' => $result['is_parent'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_code'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteChildDoc() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['parent_doc_id'])) {
			$this->load->model('catalog/treatment_entry');

			$results = $this->model_catalog_treatment_entry->getChildDoctorAutos($this->request->get['parent_doc_id']);
			/*echo'<pre>';
			print_r($this->request->get['parent_doc_id']);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_code'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletehorse() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['trainer_id'])) {
			$this->load->model('catalog/treatment_entry');

			$results = $this->model_catalog_treatment_entry->getHorseAutos($this->request->get['trainer_id']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {
				if($result['horse_id']){
					$horse_sql =  $this->db->query("SELECT official_name FROM `horse1` WHERE horseseq = '".$result['horse_id']."'");
					if ($horse_sql->num_rows > 0) {
						$horse_name = $horse_sql->row['official_name'];
						$json[] = array(
							'horse_id' => $result['horse_id'],
							'horse_name' => $horse_name,
						);
					}
				}
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['horse_id'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addMedicineTrans() {
		echo "<pre>"; print_r($this->request->post);exit;
		
	}

	public function clinic_search() {
		$json = array();
		$check_valid = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' ");
		if ($check_valid->num_rows > 0) {
			$json['success'] = 0;
			$code_datas = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' ");
			if ($code_datas->num_rows > 0) {
				$doctor_id = $code_datas->row['id'];
			} else {
				$doctor_id = '';
			}
			if ($doctor_id != '') {
				$doctor_datas = $this->db->query("SELECT * FROM `doctor` WHERE `parent_id` = '".$doctor_id."' AND `is_parent` = 0 ");

				if ($doctor_datas->num_rows > 0) {
					foreach ($doctor_datas->rows as $value) {
						$json['docs'][] = array(
							'doctor_id' => $value['id'],
							'doctor_name' => $value['doctor_name'],
						);
					}
					$json['parent'] = $value['parent'];
					$json['parent_id'] = $value['parent_id'];
					$json['success'] = 1;
				}
			}
			if ($json['success'] == 0) {
				$final_data = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' AND `is_parent` = 0 ");
				if ($final_data->num_rows > 0) {
					$parent_id = $final_data->row['parent_id'];
					if ($parent_id == 0) {
						$json = array(
							'parent_id' => $final_data->row['id'],
							'parent' => $final_data->row['doctor_name'],
							'doctor_id' => '',
							'doctor_name' => '',
						);
						$json['success'] = 3;
					} else {
						$json = array(
							'parent_id' => $final_data->row['parent_id'],
							'parent' => $final_data->row['parent'],
							'doctor_id' => $final_data->row['id'],
							'doctor_name' => $final_data->row['doctor_name'],
						);
					}
				} else {
					$final_datas = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' ");
					if ($final_datas->num_rows > 0) {
						$json = array(
							'parent_id' => $final_datas->row['parent_id'],
							'parent' => $final_datas->row['parent'],
							'doctor_id' => '',
							'doctor_name' => '',
						);
						$json['success'] = 4;
					}
				}
			}
			$json['alert'] = '';
		} else {
			$json['alert'] = 'Please Enter Valid Code!';
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
