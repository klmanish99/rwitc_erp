<?php
/*require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;*/
date_default_timezone_set("Asia/Kolkata");
class Controllercatalogmedicinetransfer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/medicine_transfer');

		$this->document->setTitle('Medicine Transfer');

		$this->load->model('catalog/medicine_transfer');
		$this->load->model('catalog/product');

		$this->getForm();
	}

	public function add() {
		$this->load->language('catalog/medicine_transfer');
		/*$this->load->model('catalog/product');*/

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/medicine_transfer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_medicine_transfer->addMedicineTrans($this->request->post);

			$this->session->data['success'] = "Medicine Transfer Successfully !!!";

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url, true));
			//$this->print_medicine($this->request->post);
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/medicine_transfer');
		$this->load->model('catalog/product');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/medicine_transfer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_inward->editinward($this->request->get['order_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

		public function edit1() {
		$this->load->language('catalog/medicine_transfer');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/medicine_transfer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') /*&& $this->validateForm()*/) {
			$this->model_catalog_inward->editinward1($this->request->get['order_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/medicine_transfer/getForm', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getFormView();
	}

	public function getFormView() {
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['req'])) {
			$data['error_req'] = $this->error['req'];
		} else {
			$data['error_req'] = '';
		}

		if (isset($this->error['reason'])) {
			$data['error_reason'] = $this->error['reason'];
		} else {
			$data['error_reason'] = array();
		}


		if (isset($this->error['date'])) {
			$data['error_date'] = $this->error['date'];
		} else {
			$data['error_date'] = array();
		}

		// echo '<pre>';
		// print_r($data['error_reason']);
		// exit;

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_quantity'])) {
			$url .= '&filter_inward_quantity=' . urlencode(html_entity_decode($this->request->get['filter_inward_quantity'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/medicine_transfer/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/medicine_transfer/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . $url, true);
		}

		

		$data['cancel'] = $this->url->link('catalog/medicine_transfer/getList', 'token=' . $this->session->data['token'] . $url, true);
		

		if (isset($this->request->get['order_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			//$med_trans_info = $this->model_catalog_medicine_transfer->test($this->request->get['order_id']);
			$med_trans_info = $this->db->query("SELECT DISTINCT * FROM medicine_transfer WHERE id = '" . (int)$this->request->get['order_id'] . "'")->row;
		//return $query->row;
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['parent_doctor_name'])) {
			$data['parent_doctor_name'] = $this->request->post['parent_doctor_name'];
		} elseif (!empty($med_trans_info)) {
			$data['parent_doctor_name'] = $med_trans_info['parent_doctor_name'];
		} else {
			$data['parent_doctor_name'] = '';
		}
		
		if (isset($this->request->post['parent_doctor_id'])) {
			$data['parent_doctor_id'] = $this->request->post['parent_doctor_id'];
		} elseif (!empty($med_trans_info)) {
			$data['parent_doctor_id'] = $med_trans_info['parent_doctor_id'];
		} else {
			$data['parent_doctor_id'] = '';
		}


		if (isset($this->request->get['issue_no'])) {
			$data['issue_no'] = $this->request->get['issue_no'];
		} elseif (!empty($med_trans_info)) {
			$data['issue_no'] = $med_trans_info['issue_no'];
		} else {
			$data['issue_no'] = '';
		}

		if (isset($this->request->post['entry_date'])) {
			$data['entry_date'] = $this->request->post['entry_date'];
		} elseif (!empty($med_trans_info)) {
			if($med_trans_info['entry_date'] != '0000-00-00'){
				$data['entry_date'] = date('d-m-Y',strtotime($med_trans_info['entry_date']));
			} else {
				$data['entry_date'] = '';
			}
		} else {
			$data['entry_date'] = '';
		}

		if (isset($this->request->post['total_item'])) {
			$data['total_item'] = $this->request->post['total_item'];
		} elseif (!empty($med_trans_info)) {
			$data['total_item'] = $med_trans_info['total_item'];
		} else {
			$data['total_item'] = '';
		}


		if (isset($this->request->post['total_qty'])) {
			$data['total_qty'] = $this->request->post['total_qty'];
		} elseif (!empty($med_trans_info)) {
			$data['total_qty'] = $med_trans_info['total_qty'];
		} else {
			$data['total_qty'] = '';
		}


		if (isset($this->request->post['total_amt'])) {
			$data['total_amt'] = $this->request->post['total_amt'];
		} elseif (!empty($med_trans_info)) {
			$data['total_amt'] = $med_trans_info['total_amt'];
		} else {
			$data['total_amt'] = '';
		}

		if (isset($this->request->post['child_doctor_id'])) {
			$data['child_doctor_id'] = $this->request->post['child_doctor_id'];
		} elseif (!empty($med_trans_info)) {
			$doctor_name = $this->db->query("SELECT * FROM doctor WHERE id = '".$med_trans_info['child_doctor_id']."'	");
			if ($doctor_name->num_rows > 0) {
				$data['child_doctor_id'] = $doctor_name->row['doctor_name'];
			} else {
				$data['child_doctor_id'] = '';
			}
			//$data['child_doctor_id'] = $med_trans_info['child_doctor_id'];
		} else {
			$data['child_doctor_id'] = '';
		}


		$types = array(
			
		);

		$data['types'] = $types;

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} elseif (!empty($med_trans_info)) {
			$product_raw_datass = $this->db->query("SELECT * FROM `medicine_trans_items` WHERE `parent_id` = '".$med_trans_info['id']."' ")->rows;
			//echo "<pre>";print_r($product_raw_datass);exit;
			$productraw_datas = array();

			foreach($product_raw_datass as $pkeys => $pvalues){
				if($pvalues['expire_date'] != '0000-00-00' && $pvalues['expire_date'] != '1970-01-01'){
					$exp_date = date('d-m-Y',strtotime($pvalues['expire_date']));
				} else {
					$exp_date = '';
				}
				$productraw_datas[] = array(
					'product_id' => $pvalues['product_id'],
					'product_name' => $pvalues['product_name'],
					'expire_date' => $exp_date,
					'product_qty' => $pvalues['product_qty'],
					
				);	
			}
			$data['productraw_datas'] = $productraw_datas;
		} else {
			$data['productraw_datas'] = array();
		}

		//$data['user_type'] = $this->user->getUserType();	
		$data['user_group_id'] = $this->user->getGroupId();	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/medicine_transfer_view', $data));
	}

	public function stock() {
		$this->load->language('catalog/medicine_transfer');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/medicine_transfer');


		$datas = $this->model_catalog_inward->daterawmaterial();
		foreach ($datas as $key => $value) {
			$data['datas'][] = array(
				'date' => $value['date']
			);

		$is_date = $this->db->query("SELECT * FROM `oc_inwarditem` WHERE `order_id` = '".$value['order_id']."' AND date = '".$value['date']."' ");
			if ($is_date->num_rows == 0){
					$this->db->query("INSERT INTO oc_inwarditem SET `order_id` = '".$value['order_id']."',date = '".$value['date']."' ");
				}
				
		}
		// echo '<pre>';print_r($is_date);exit;

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url, true));
		

		$this->getList();
	}

	public function delete() {
		$this->load->language('catalog/medicine_transfer');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/medicine_transfer');

		if (isset($this->request->post['selected']) /*&& $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $order_id) {
				$this->model_catalog_inward->deleteinward($order_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}
	public function approve() {
		$order_id1=$_GET['order_id'];
		$user_group_id=$this->db->query("UPDATE oc_inward SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$user_group=$this->db->query("UPDATE oc_inwarditem SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$this->response->redirect($this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function approve_onform() {
		$order_id1=$_GET['order_id'];
		$user_group_id=$this->db->query("UPDATE oc_inward SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$user_group=$this->db->query("UPDATE oc_inwarditem SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$this->response->redirect($this->url->link('catalog/medicine_transfer/edit1', 'token=' . $this->session->data['token'] . '&order_id=' .$order_id1. $url, true));
	}

	public function getList() {


		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		if (isset($this->request->get['filter_clinic_name'])) {
			$filter_clinic_name = $this->request->get['filter_clinic_name'];
		} else {
			$filter_clinic_name = null;
		}

		if (isset($this->request->get['filter_clinic_id'])) {
			$filter_clinic_id = $this->request->get['filter_clinic_id'];
		} else {
			$filter_clinic_id = null;
		}

		if (isset($this->request->get['filter_doctor'])) {
			$filter_doctor = $this->request->get['filter_doctor'];
		} else {
			$filter_doctor = null;
		}

		if (isset($this->request->get['filter_doctor_id'])) {
			$filter_doctor_id = $this->request->get['filter_doctor_id'];
		} else {
			$filter_doctor_id = null;
		}

		if (isset($this->request->get['filter_dates'])) {
			$filter_dates = $this->request->get['filter_dates'];
		} else {
			$filter_dates = null;
		}

		if (isset($this->request->get['filter_productsort'])) {
			$filter_productsort = $this->request->get['filter_productsort'];
		} else {
			$filter_productsort = null;
		}

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$filter_inward_category = $this->request->get['filter_inward_category'];
		} else {
			$filter_inward_category = null;
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$filter_inward_category_id = $this->request->get['filter_inward_category_id'];
		} else {
			$filter_inward_category_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}


		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url, true)
		);
		
		$data['medicine_report'] = $this->url->link('report/med_transfer_report', 'token=' . $this->session->data['token'] . $url, true);
		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . $url, true);
		$data['add'] = $this->url->link('catalog/medicine_transfer/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['stock'] = $this->url->link('catalog/medicine_transfer/stock', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/medicine_transfer/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['medicine_trans_datas'] = array();

		$filter_data = array(
			'filter_clinic_name'	  => $filter_clinic_name,
			'filter_clinic_id'	  => $filter_clinic_id,
			'filter_doctor'	  => $filter_doctor,
			'filter_doctor_id'	  => $filter_doctor_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
			// echo '<pre>';
			//  print_r($filter_data);
			// exit;
		$results = array();
		//$inward_total = $this->model_catalog_inward->getTotalinwards($filter_data);
		$inward_total = 0;
		$user_group_id = $this->user->getGroupId();
		$medi_datas = array();
		$sql = "SELECT * FROM medicine_transfer WHERE 1=1";
		if($filter_clinic_id != ''){
			$sql .= " AND parent_doctor_id = '".$filter_clinic_id."' ";
		}

		if($filter_doctor_id != ''){
			$sql .= " AND child_doctor_id = '".$filter_doctor_id."' ";
		}

		$sql .= " ORDER BY id DESC";
		// echo $sql ;
		// exit;
		$medicine_trans_datas = $this->db->query($sql)->rows;
		foreach ($medicine_trans_datas as $key => $value) {
			$doc_datas = $this->db->query("SELECT * FROM doctor WHERE id = '".$value['child_doctor_id']."'");
			if ($doc_datas->num_rows > 0) {
				$doctor_name = $doc_datas->row['doctor_name'];
			} else {
				$doctor_name = '';
			}

			$logs_info = $this->db->query("SELECT * FROM `med_trans_logs` WHERE `medt_id` = '".$value['id']."' ORDER BY `id` DESC LIMIT 1 ");
			// echo'<pre>';
			// print_r("SELECT * FROM `supplier_logs` WHERE `supp_id` = '".$result['vendor_id']."' ");
			if ($logs_info->num_rows > 0) {
				$fname = $logs_info->row['fname'];
				$lname = $logs_info->row['lname'];
				$log_date = date('d-m-Y', strtotime($logs_info->row['log_date']));
				$log_time = $logs_info->row['log_time'];
				$log_datas = '<span>'.'Name: '.$fname.' '.$lname.'</span>'.'<br>'.'<span>'.'Date: '.$log_date.'</span>'.'<br>'.'<span>'.'Time: '.$log_time.'</span>';
			} else {
				$log_datas = '';
			}

			$medicine_types = $this->db->query("SELECT unit FROM medicine WHERE id = '".$value['id']."' ");
			if ($medicine_types->num_rows > 0) {
				$medicine_type = $medicine_types->row['unit'];
			} else {
				$medicine_type = '';
			}

			$medi_datas[] = array(
					'id' => $value['id'],
					'issue_no' => $value['issue_no'],
					'entry_date' => $value['entry_date'],
					'clinic_name' => $value['parent_doctor_name'],
					'doctor_name' => $doctor_name,
					'total_item' => $value['total_item'],
					'total_qty' => $value['total_qty'].' '.$medicine_type,
					'total_amt' => $value['total_amt'],
					'log_datas'     => $log_datas,
					'edit'     => $this->url->link('catalog/medicine_transfer/getFormView', 'token=' . $this->session->data['token'] . '&order_id=' . $value['id'] . $url, true),
					'print'     => $this->url->link('catalog/medicine_transfer/prints', 'token=' . $this->session->data['token'] . '&id=' . $value['id'] . $url, true),
			);
		}
		$data['medicine_trans_datas'] = $medi_datas;

		

		$data['productsorts'] = array(
			'a'=>'A','b'=>'B','c'=>'C','d'=>'D','e'=>'E','f'=>'F','g'=>'G','h'=>'H',
			'i'=>'I','j'=>'J','k'=>'K','l'=>'L','m'=>'M','n'=>'N','o'=>'O','p'=>'P',
			'q'=>'Q','r'=>'R','s'=>'S','t'=>'T','u'=>'U','v'=>'V','w'=>'W','x'=>'X',
			'y'=>'Y','z'=>'Z','Other'
		);
			// echo '<pre>';
			// print_r($data['productsorts']);
			// exit;
		$data['user_group_id'] = $this->user->getGroupId();
		//$data['user_type'] = $this->user->getUserType();	
		$data['base_link_1'] = $this->url->link('catalog/medicine_transfer/edit', 'token=' . $this->session->data['token']);
		$data['base_link'] = $this->url->link('catalog/medicine_transfer/edit', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . '&sort=inward_name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $inward_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($inward_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($inward_total - $this->config->get('config_limit_admin'))) ? $inward_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $inward_total, ceil($inward_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		
		$data['filter_clinic_name'] = $filter_clinic_name;
		$data['filter_clinic_id'] = $filter_clinic_id;
		$data['filter_doctor'] = $filter_doctor;
		$data['filter_doctor_id'] = $filter_doctor_id;
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];
		
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		$this->response->setOutput($this->load->view('catalog/medicine_transfer_list', $data));
	}

	protected function getForm() {
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['error_medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['error_medicine_error'] = '';
		}

		if (isset($this->error['day_close_error'])) {
			$data['day_close_error'] = $this->error['day_close_error'];
		} else {
			$data['day_close_error'] = '';
		}

		

		// echo'<pre>';
		// print_r($this->error);
		// exit;

		if (isset($this->error['req'])) {
			$data['error_req'] = $this->error['req'];
		} else {
			$data['error_req'] = '';
		}

		if (isset($this->error['reason'])) {
			$data['error_reason'] = $this->error['reason'];
		} else {
			$data['error_reason'] = array();
		}


		if (isset($this->error['date'])) {
			$data['error_date'] = $this->error['date'];
		} else {
			$data['error_date'] = array();
		}

		// echo '<pre>';
		// print_r($data['error_reason']);
		// exit;

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_quantity'])) {
			$url .= '&filter_inward_quantity=' . urlencode(html_entity_decode($this->request->get['filter_inward_quantity'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['previous'] = $this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'], true);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['user_log_grp_id'] = $this->user->getGroupId();

		$data['user_log_id'] = $this->user->getId();

		if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/medicine_transfer/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/medicine_transfer/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/medicine_transfer/getList', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['order_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$med_trans_info = $this->model_catalog_inward->getinward($this->request->get['order_id']);
		}

		$data['token'] = $this->session->data['token'];

		$order_nos = $this->db->query("SELECT issue_no FROM medicine_transfer WHERE 1=1 ORDER BY issue_no DESC LIMIT 1 ");
		// echo '<pre>';
		// print_r($this->request->post);
		// exit;
		if ($order_nos->num_rows > 0) {
			$data['input_isse_number'] = $order_nos->row['issue_no'] + 1;
		} else {
			$data['input_isse_number'] = 1;
		}

		
		if (isset($this->request->post['filter_parent_doctor'])) {
			$data['filter_parent_doctor'] = $this->request->post['filter_parent_doctor'];
		} elseif (!empty($med_trans_info)) {
			$data['filter_parent_doctor'] = $med_trans_info['parent_doctor_name'];
		} else {
			$data['filter_parent_doctor'] = '';
		}

		if (isset($this->request->post['filterParentId'])) {
			$data['filter_parent_doctor_id'] = $this->request->post['filterParentId'];
		} elseif (!empty($med_trans_info)) {
			$data['filter_parent_doctor_id'] = $med_trans_info['parent_doctor_id'];
		} else {
			$data['filter_parent_doctor_id'] = '';
		}

		if (isset($this->request->post['to_doc'])) {
			$data['to_doc'] = $this->request->post['to_doc'];
			$data['child_doctor_name'] = $this->db->query("SELECT * FROM doctor WHERE id = '".$this->request->post['to_doc']."'	")->row['doctor_name'];
			$results =$this->db->query("SELECT * FROM  doctor WHERE isActive = 'Active' AND is_parent = 0 AND `parent_id` = '" . ($this->request->post['filterParentId']) . "' ")->rows;
			
			$json = array();
			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}

			$data['all_docs'] = $json;
		} elseif (!empty($med_trans_info)) {
			$data['to_doc'] = $med_trans_info['child_doctor_id'];
			$data['child_doctor_name'] = $this->db->query("SELECT * FROM doctor WHERE id = '".$med_trans_info['child_doctor_id']."'	")->row['doctor_name'];
			$results =$this->db->query("SELECT * FROM  doctor WHERE isActive = 'Active' AND is_parent = 0 ")->rows;
			
			$json = array();
			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}

			$data['all_docs'] = $json;

		} else {
			$data['to_doc'] = '';
			$data['all_docs'] = array();
		}

		if (isset($this->request->post['expire_date'])) {
			$data['expire_date'] = $this->request->post['expire_date'];
		} elseif (!empty($med_trans_info)) {
			$data['expire_date'] = $med_trans_info['expire_date'];
		} else {
			//$data['expire_date'] = date('d-m-Y');
			if(isset($this->request->get['date_reconsilation'])){ 
				$data['expire_date'] = $this->request->get['date_reconsilation'];
			} else {
				$data['expire_date'] = date('d-m-Y');
			}
		}
		
		if (isset($this->request->post['total_item'])) {
			$data['total_item'] = $this->request->post['total_item'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['total_item'] = $rawmaterialreq_info['total_item'];
		} else {
			$data['total_item'] =0;
		}

		if (isset($this->request->post['total_qty'])) {
			$data['total_qty'] = $this->request->post['total_qty'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['total_qty'] = $rawmaterialreq_info['total_qty'];
		} else {
			$data['total_qty'] =0;
		}

		if (isset($this->request->post['total_amt'])) {
			$data['total_amt'] = $this->request->post['total_amt'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['total_amt'] = $rawmaterialreq_info['total_amt'];
		} else {
			$data['total_amt'] =0;
		}

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} else {
			$data['productraw_datas'] = array();
		}
		//$data['user_type'] = $this->user->getUserType();	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/medicine_transfer_form', $data));
	}


	public function prints() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$medicine_transfer = $this->db->query("SELECT * FROM medicine_transfer WHERE id = '".$this->request->get['id']."' ")->rows;

		foreach ($medicine_transfer as $pvalue) {

			$doctors = $this->db->query("SELECT * FROM doctor WHERE id = '".$pvalue['child_doctor_id']."' ");
			if ($doctors->num_rows > 0) {
				$data['doctor_name'] = $doctors->row['doctor_name'];
			} else {
				$data['doctor_name'] = '';
			}

			if (($pvalue['entry_date'] > '1970-01-01')) {
				$entry_date = date('d-m-Y', strtotime($pvalue['entry_date']));
			} else {
				$entry_date = '';
			}

			$data['medicine_transfers'][] = array(
				'issue_no'=> $pvalue['issue_no'],
				'clinic'=> $pvalue['parent_doctor_name'],
				'entry_date'=> $entry_date,
			);

			$data['total_amount'] = $pvalue['total_amt'];
		}


		$medicine_transfer_items = $this->db->query("SELECT * FROM medicine_trans_items WHERE parent_id = '".$this->request->get['id']."' ")->rows;
		
		foreach ($medicine_transfer_items as $value) {

			if (($value['expire_date'] > '1970-01-01')) {
				$expire_date = date('d-m-Y', strtotime($value['expire_date']));
			} else {
				$expire_date = '';
			}

			$medicine_types = $this->db->query("SELECT unit, med_type FROM medicine WHERE med_code LIKE '".$value['product_id']."%' ");
			if ($medicine_types->num_rows > 0) {
				$medicine_type = $medicine_types->row['med_type'];
				$medicine_unit = $medicine_types->row['unit'];
			} else {
				$medicine_type = '';
				$medicine_unit = '';
			}

			$data['medicine_transfer_item'][] = array(
				'med_code'=> $value['product_id'],
				'med_name'=> $value['product_name'],
				'expire_date'=> $expire_date,
				'product_qty'=> $value['product_qty'],
				'medicine_type'=> $medicine_type,
				'medicine_unit'=> $medicine_unit,
			);
		}

		
		
		$html = $this->load->view('report/medicine_transfer_html', $data);
		//echo $html;exit;
		$filename = 'Medicine Transfer.html';
		//file_put_contents(DIR_DOWNLOAD.Indent, $html);
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;
	}

	public function calculate() {
		$this->load->language('catalog/medicine_transfer');

		$this->document->setTitle('Product Costing');

		$this->load->model('catalog/medicine_transfer');
		$this->load->model('catalog/product');
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Product Costing',
			'href' => $this->url->link('catalog/medicine_transfer/calculate', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['cancel'] = $this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url, true);
		$data['action'] = $this->url->link('catalog/medicine_transfer/make', 'token=' . $this->session->data['token'] . $url.'&finished_product_id='.$this->request->get['order_id'], true);

		if (isset($this->request->get['order_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$inward_info = $this->model_catalog_inward->getinward($this->request->get['order_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['inward_name'])) {
			$data['inward_name'] = $this->request->post['inward_name'];
		} elseif (!empty($inward_info)) {
			$data['inward_name'] = $inward_info['inward_name'];
		} else {
			$data['inward_name'] = '';
		}

		if (isset($this->request->post['cost_price'])) {
			$data['cost_price'] = $this->request->post['cost_price'];
		} elseif (!empty($inward_info)) {
			$data['cost_price'] = $inward_info['cost_price'];
		} else {
			$data['cost_price'] = '';
		}

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} elseif (!empty($inward_info)) {
			$product_raw_datass = $this->db->query("SELECT * FROM `oc_inwarditem` WHERE `order_id` = '".$inward_info['order_id']."' ")->rows;
			$productraw_datas = array();
			foreach($product_raw_datass as $pkeys => $pvalues){
				$product_datas = $this->db->query("SELECT `quantity` FROM `is_productnew` WHERE `productnew_id` = '".$pvalues['product_id']."' ")->row;
				if(isset($product_datas['quantity'])){
					$quantity_in_stock = $product_datas['quantity'];
				} else {
					$quantity_in_stock = 0;
				}
				$productraw_datas[] = array(
					'productraw_name' => $pvalues['productraw_name'],
					'productraw_id' => $pvalues['productraw_id'],
					'product_id' => $pvalues['product_id'],
					'order_no' => $pvalues['order_no'],
					'price' => $pvalues['price'],
					'quantity' => $pvalues['quantity'],
					'gst_type' => $pvalues['gst_type'],
					/*'outside_color' => $pvalues['outside_color'],
					'inside_color' => $pvalues['inside_color'],
					'raw_material' => $pvalues['raw_material'],
					'productnew_id' => $pvalues['productnew_id'],*/
					'gst_type_id' => $pvalues['gst_type_id'],
					/*'quantity_in_stock' => $quantity_in_stock,*/
					'total' => $pvalues['total'],
				); 			
			}
			$data['productraw_datas'] = $productraw_datas;
		} else {
			$data['productraw_datas'] = array();
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/medicine_transfer_calculate', $data));
	}

	public function make() {
		$this->load->model('catalog/medicine_transfer');
		$this->model_catalog_inward->makeProduct($this->request->get['finished_product_id'], $this->request->post);
		$this->response->redirect($this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'] . $url, true));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/medicine_transfer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		// $this->error['name'] = '';
		if ((utf8_strlen($this->request->post['filter_parent_doctor']) == '')) {
			$this->error['name'] = "Please Enter Clinic Name";
		}

		if(!isset($this->request->post['productraw_datas'])){
			$this->error['medicine_error'] = ''." Please Enter Atleast one medicine to transfer";
		}
		$chceck_day_close_product = $this->db->query("SELECT * FROM daily_reconsilation WHERE entry_date = '".$this->db->escape(date('Y-m-d', strtotime($this->request->post['date'])))."'  ");
		
		if($chceck_day_close_product->num_rows > 0){
			$this->error['day_close_error'] = "Medicine Not Transfer Because Day Is Not Closed!";
		}	
		 // else{
		// 	foreach ($this->request->post['productraw_datas'] as $key => $value) {
		// 		$total_inward_qty = $this->db->query("SELECT  productraw_id ,quantity FROM `oc_inwarditem` WHERE product_id = '".$value['input_item_code']."' AND cons_status = 0 order by ex_date ASC ");
		// 		if($total_inward_qty->num_rows > 0){
		// 			$pre_transfer_qty = $this->db->query("SELECT sum(qty) as qty FROM `inward_medicine_transfer` WHERE product_id = '".$value['input_item_code']."' AND inward_id ='".$total_inward_qty->row['productraw_id']."' ");
		// 			if($pre_transfer_qty->num_rows > 0){
		// 				$bal_qty = $total_inward_qty->row['quantity'] - $pre_transfer_qty->row['qty'];
		// 				if($value['input_qty'] > $bal_qty){
		// 					if($bal_qty > 0){
		// 						$total_new_inward_qty = $this->db->query("SELECT  productraw_id ,quantity FROM `oc_inwarditem` WHERE product_id = '".$value['input_item_code']."' AND cons_status = 0 AND quantity >= '".$bal_qty."' AND productraw_id <> '".$total_inward_qty->row['productraw_id']."' order by ex_date ASC ");
		// 						if ($total_new_inward_qty->num_rows == 0){
		// 							$this->error['medicine_error'] = ' Balance Qty is - <b>'.$bal_qty.' For '.$value['item_name'].' </b> <br /> Please Inward some Quantity';
		// 						}
		// 					} else {
		// 						$this->error['medicine_error'] = ' Balance Qty is - <b> 0 </b> <br /> Please Inward some Quantity';
		// 					}
		// 				} else {
		// 					if($value['input_qty'] > $total_inward_qty->row['quantity']){
		// 						$this->error['medicine_error'] = ' Balance Qty is - <b>'.$bal_qty.' For '.$value['item_name'].' </b> <br /> Please Inward some Quantity';
		// 					}
		// 				} 
		// 			}  else {
		// 				if($value['input_qty'] > $total_inward_qty->row['quantity']){
		// 					$this->error['medicine_error'] = ' Balance Qty is - <b>'.$total_inward_qty->row['quantity'].' For '.$value['item_name'].' </b> <br /> Please Inward some Quantity';
		// 				}
		// 			} 
		// 		} else {
		// 			$this->error['medicine_error'] = ' Balance Qty is - <b>0  For '.$value['item_name'].' </b>';
		// 		}
		// 	}
		// }

		// echo '<pre>';
		// print_r($this->request->post);
		//  exit;
		//$datas = $this->request->post;
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/medicine_transfer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
	public function autocomplete() {
		$json = array();
		 /*echo '<pre>';
		 print_r($this->request->get);
		 exit;*/
		if (isset($this->request->get['filter_order_id'])) {
			$this->load->model('catalog/medicine_transfer');

			$filter_data = array(
				'filter_order_id' => $this->request->get['filter_order_id'],
				//'start'       => 0,
				//'limit'       => 5
			);


			//$results = $this->model_catalog_medicine_transfer->geMediTransDatas($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'order_id' => $result['order_id'],
					'order_no'     => strip_tags(html_entity_decode($result['order_no'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['order_no'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletesupplier() {
		$json = array();
		 // echo '<pre>';
		 // print_r($this->request->get);
		 // exit;
		if (isset($this->request->get['supplier'])) {
			$this->load->model('catalog/medicine_transfer');

			$filter_data = array(
				'filter_supplier' => $this->request->get['supplier'],
				//'start'       => 0,
				//'limit'       => 5
			);
			$results = $this->model_catalog_inward->getsupplier($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'vendor_id' => $result['vendor_id'],
					'vendor_name'     => strip_tags(html_entity_decode($result['vendor_name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['vendor_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete1() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/medicine_transfer');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
			);
			$results = $this->model_catalog_inward->getinwards1($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'order_id' => $result['order_id'],
					'order_no'     => strip_tags(html_entity_decode($result['order_no'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['order_no'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		}

	public function autocomplete_raw() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/medicine_transfer');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_inward->getProductnews($filter_data);

			foreach ($results as $result) {
				//echo "<pre>";print_r($result);exit;
				$quantity = 1;
				$json[] = array(
					'id' => $result['id'],
					'med_code' => strip_tags(html_entity_decode($result['med_code'], ENT_QUOTES, 'UTF-8')),
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8')),
					'price'     => strip_tags(html_entity_decode($result['unit_cost'], ENT_QUOTES, 'UTF-8')),
					'store_unit' => strip_tags(html_entity_decode($result['store_unit'], ENT_QUOTES, 'UTF-8')),
					'unit'     => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8')),
					'packing'     => strip_tags(html_entity_decode($result['pack_type'], ENT_QUOTES, 'UTF-8')),
					'volume'     => strip_tags(html_entity_decode($result['volume'], ENT_QUOTES, 'UTF-8')),
					'gst_rate'     => strip_tags(html_entity_decode($result['gst_rate'], ENT_QUOTES, 'UTF-8')),
					'quantity' => 1,
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['med_name'];
			
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		// echo'<pre>';
		// print_r($json);
		// exit;
	}

	public function autocomplete_raw_code() {
		$json = array();

		if (isset($this->request->get['med_code'])) {
			$this->load->model('catalog/medicine_transfer');

			$filter_data = array(
				'med_code' => $this->request->get['med_code'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_inward->getProductcode($filter_data);

			foreach ($results as $result) {
				$quantity = 1;
				$json = array(
					'id' => $result['id'],
					'med_code' => strip_tags(html_entity_decode($result['med_code'], ENT_QUOTES, 'UTF-8')),
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8')),
					'quantity' => 1,
					'price'     => strip_tags(html_entity_decode($result['unit_cost'], ENT_QUOTES, 'UTF-8')),
					'store_unit' => strip_tags(html_entity_decode($result['store_unit'], ENT_QUOTES, 'UTF-8')),
					'unit'     => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8')),
					'packing'     => strip_tags(html_entity_decode($result['pack_type'], ENT_QUOTES, 'UTF-8')),
					'volume'     => strip_tags(html_entity_decode($result['volume'], ENT_QUOTES, 'UTF-8')),
					'gst_rate'     => strip_tags(html_entity_decode($result['gst_rate'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['med_name'];
			
		// }

		// array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getdata() {
		$json = array();
		$html = '';
		if (isset($this->request->get['filter_name_id'])) {
			$this->load->model('catalog/medicine_transfer');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name_id'],
			);
			$filter_name_id = $this->request->get['filter_name_id'];

			$product_finished = $this->db->query("SELECT * FROM `oc_inward` WHERE `order_id` = '".$filter_name_id."'")->row;
			
			$product_finished_raw = $this->db->query("SELECT * FROM `oc_inwarditem` WHERE `order_id` = '".$filter_name_id."'")->rows;

			$html = '<table style="margin-top: 15px;" class="table table-hover">';
				$html .= '<tbody style="border-top:0px !important;">';
					$html .= '<tr>';	
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Name</b> : ';
							$html .= $product_finished['inward_name'];
						$html .= '</td>';
						
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Description</b> : ';
							$html .= $product_finished['inward_description'];
						$html .= '</td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Cost Price</b> : ';
							$html .= $product_finished['cost_price'];
						$html .= '</td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Selling Price</b> : ';
							$html .= $product_finished['selling_price'];
						$html .= '</td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>GST</b> : ';
							$html .= $product_finished['gst_type'];
						$html .= '</td>';
					$html .= '</tr>';

					/*$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Outside_color</b> : ';
							$html .= $product_finished['outside_color'];
						$html .= '</td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Inside_color</b> : ';
							$html .= $product_finished['inside_color'];
						$html .= '</td>';
					$html .= '</tr>';

					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Raw_Material</b> : ';
							$html .= $product_finished['raw_material'];
						$html .= '</td>';
					$html .= '</tr>';*/
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Assigned Category</b> : ';
							$html .= $product_finished['inward_category'];
						$html .= '</td>';
					$html .= '</tr>';
				$html .= '</tbody>';
			$html .= '</table>';
			$html .= '<table style="margin-top: 15px;" class="table table-hover">';
				$html .= '<tbody style="border-top:0px !important;">';
					$html .= '<tr style="border-top: 1px solid black;">';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Raw Materials</b>';
						$html .= '</td>';	
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Materials</b>';
						$html .= '</td>';	
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Quantity</b>';
						$html .= '</td>';	
					$html .= '</tr>';
					if($product_finished_raw) {
						foreach ($product_finished_raw as $result) {
							$html .= '<tr>';	
								$html .= '<td style="border-top: 0px !important;">';
									$html .= $result['productraw_name'];
								$html .= '</td>';

								/*$html .= '<tr>';	
								$html .= '<td style="border-top: 0px !important;">';
									$html .= $result['order_no'];
								$html .= '</td>';*/
								$html .= '<td style="border-top: 0px !important;">';
									$html .= $result['quantity'];
								$html .= '</td>';
							$html .= '</tr>';
						}
					}
				$html .= '</tbody>';
			$html .= '</table>';
		}
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function PoDatas() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$json = array();

		// $quantity = $this->db->query("SELECT SUM(quantity) AS quantitys FROM `oc_inwarditem` WHERE `productraw_name` = '".$this->request->get['med_name']."' ")->row;


		// $inword_qty = $quantity['quantitys'];
		//echo "<pre>";print_r($inword_qty);exit;

		$results = $this->db->query("SELECT * FROM `oc_tally_po` WHERE `name_of_the_items` = '".$this->request->get['med_name']."' AND (`status` = 0 OR `status` = 1) ")->rows;
		
		foreach($results as $result){
			$json[] = array(
				'po_no' => $result['po_no'],
				'qty' => $result['qty']
			);//echo "<pre>";print_r($json);	
		}//exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function PoQty() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$json = array();
		$results = $this->db->query("SELECT * FROM `oc_tally_po` WHERE `po_no` = '".$this->request->get['po']."' ")->rows;
		// echo'<pre>';
		// print_r("SELECT * FROM `oc_tally_po` WHERE `po_no` = '".$this->request->get['po']."' ");
		// exit;
		foreach($results as $result){
			$inward_po = $this->db->query("SELECT reduce_po_qty FROM `oc_inwarditem` WHERE `po_no` = '".$result['po_no']."' ORDER BY `productraw_id` DESC LIMIT 1 ");
			if ($inward_po->num_rows > 0) {
				$reduce_po_qty = $inward_po->row['reduce_po_qty'];
			} else{
				$reduce_po_qty = $result['qty'];
			}

			$json = array(
				'reduce_po_qty' => $reduce_po_qty,
				'qty' => $result['qty']
			);//echo "<pre>";print_r($json);	
		}//exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteParentDoc() {
		//echo 'in';
		$json = array();
		//echo '<pre>';print_r($this->request->get);exit;

		if (isset($this->request->get['filter_doctor_code'])) {
			$this->load->model('catalog/medicine_transfer');

			$results = $this->model_catalog_medicine_transfer->getDoctorAutos($this->request->get['filter_doctor_code']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'is_parent' => $result['is_parent'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
			//echo '<pre>';print_r($json);exit;
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_code'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteChildDoc() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['parent_horse_id'])) {
			$this->load->model('catalog/medicine_transfer');

			$results = $this->model_catalog_medicine_transfer->getChildDoctorAutos($this->request->get['parent_horse_id']);
		/*	echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_code'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	


	public function medicineCodeSearch(){
		$json = array();

		if (isset($this->request->get['filter_medicine_code'])) {
			$sql =("SELECT * FROM  medicine WHERE 1 = 1");
			if($this->request->get['filter_medicine_code'] != ''){
				$sql .= " AND `med_code` = '" . $this->db->escape($this->request->get['filter_medicine_code']) . "'";
			}

			$sql .= " ORDER BY `med_code` ";
			$query = $this->db->query($sql);
			if($query->num_rows > 0){
				$result = $query->row;

				$inward_exp_date = $this->db->query("SELECT quantity, ex_date FROM `oc_inwarditem` WHERE product_id = '".$result['med_code']."' AND cons_status = 0 ORDER BY ex_date ASC");
				if($inward_exp_date->num_rows > 0){
					$ex_date = date('d-m-Y', strtotime($inward_exp_date->row['ex_date']));
				} else {
					$ex_date = '';
				}

				$final_price = 0;
				$final_price = $result['purchase_price'] * (int)$result['normal'];
				$json = array(
					'id' => $result['id'],
					'med_code' => $result['med_code'],
					'purchase_price' => $result['purchase_price'],
					'final_price' =>$final_price,
					'normal' => 0,
					'expiry_date' => $ex_date,
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8'))
				);
				$json['success'] = 1;
			}else {
				$json = array();
				$json['success'] = 0;
			}
		} 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteClinic(){
		$json = array();

		if (isset($this->request->get['filter_clinic_name'])) {

			$clinic_namess = $this->db->query("SELECT * FROM doctor WHERE doctor_name LIKE '%".$this->request->get['filter_clinic_name']."%' AND is_parent = 1 AND isActive = 'Active'")->rows;
			foreach ($clinic_namess as $key => $value) {
				$json[] = array(
					'clinic_name' => $value['doctor_name'],
					'clinic_id' => $value['id'],
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function autocompleteDoctor(){

		$json = array();

		if (isset($this->request->get['filter_doctor_name'])) {

			$doctor_namess = $this->db->query("SELECT * FROM doctor WHERE doctor_name LIKE '%".$this->request->get['filter_doctor_name']."%' AND is_parent = 0 AND isActive = 'Active'")->rows;
			foreach ($doctor_namess as $key => $value) {
				$json[] = array(
					'doctor_name' => $value['doctor_name'],
					'doctor_id' => $value['id'],
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function clinic_search() {
		$json = array();
		$check_valid = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' ");
		if ($check_valid->num_rows > 0) {
			$json['success'] = 0;
			$code_datas = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' ");
			if ($code_datas->num_rows > 0) {
				$doctor_id = $code_datas->row['id'];
			} else {
				$doctor_id = '';
			}
			if ($doctor_id != '') {
				$doctor_datas = $this->db->query("SELECT * FROM `doctor` WHERE `parent_id` = '".$doctor_id."' AND `is_parent` = 0 ");

				if ($doctor_datas->num_rows > 0) {
					foreach ($doctor_datas->rows as $value) {
						$json['docs'][] = array(
							'doctor_id' => $value['id'],
							'doctor_name' => $value['doctor_name'],
						);
					}
					$json['parent'] = $value['parent'];
					$json['parent_id'] = $value['parent_id'];
					$json['success'] = 1;
				}
			}
			if ($json['success'] == 0) {
				$final_data = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' AND `is_parent` = 0 ");
				if ($final_data->num_rows > 0) {
					$parent_id = $final_data->row['parent_id'];
					if ($parent_id == 0) {
						$json = array(
							'parent_id' => $final_data->row['id'],
							'parent' => $final_data->row['doctor_name'],
							'doctor_id' => '',
							'doctor_name' => '',
						);
						$json['success'] = 3;
					} else {
						$json = array(
							'parent_id' => $final_data->row['parent_id'],
							'parent' => $final_data->row['parent'],
							'doctor_id' => $final_data->row['id'],
							'doctor_name' => $final_data->row['doctor_name'],
						);
					}
				} else {
					$final_datas = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' ");
					if ($final_datas->num_rows > 0) {
						$json = array(
							'parent_id' => $final_datas->row['parent_id'],
							'parent' => $final_datas->row['parent'],
							'doctor_id' => '',
							'doctor_name' => '',
						);
						$json['success'] = 4;
					}
				}
			}
			$json['alert'] = '';
		} else {
			$json['alert'] = 'Please Enter Valid Code!';
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	// function print_medicine() {
	// 	// echo'<pre>';
	// 	// print_r($this->request->post);
	// 	// exit;

	// 	$post_datas = $this->db->query("SELECT * FROM medicine_transfer WHERE 1=1 ORDER BY id DESC LIMIT 1 ")->rows;

	// 	foreach ($post_datas as $pvalue) {

	// 		$doctors = $this->db->query("SELECT * FROM doctor WHERE id = '".$pvalue['child_doctor_id']."' ");
	// 		if ($doctors->num_rows > 0) {
	// 			$data['doctor_name'] = $doctors->row['doctor_name'];
	// 		} else {
	// 			$data['doctor_name'] = '';
	// 		}

	// 		if (($pvalue['entry_date'] > '1970-01-01')) {
	// 			$entry_date = date('d-m-Y', strtotime($pvalue['entry_date']));
	// 		} else {
	// 			$entry_date = '';
	// 		}

	// 		$data['medicine_transfers'][] = array(
	// 			'issue_no'=> $pvalue['issue_no'],
	// 			'clinic'=> $pvalue['parent_doctor_name'],
	// 			'entry_date'=> $entry_date,
	// 		);

	// 		$data['total_amount'] = $pvalue['total_amt'];
	// 	}


	// 	$medicine_transfer_items = $this->db->query("SELECT * FROM medicine_trans_items WHERE parent_id = '".$pvalue['id']."' ")->rows;
		
	// 	foreach ($medicine_transfer_items as $value) {

	// 		if (($value['expire_date'] > '1970-01-01')) {
	// 			$expire_date = date('d-m-Y', strtotime($value['expire_date']));
	// 		} else {
	// 			$expire_date = '';
	// 		}

	// 		$medicine_types = $this->db->query("SELECT unit, med_type FROM medicine WHERE med_code LIKE '".$value['product_id']."%' ");
	// 		if ($medicine_types->num_rows > 0) {
	// 			$medicine_type = $medicine_types->row['med_type'];
	// 			$medicine_unit = $medicine_types->row['unit'];
	// 		} else {
	// 			$medicine_type = '';
	// 			$medicine_unit = '';
	// 		}

	// 		$data['medicine_transfer_item'][] = array(
	// 			'med_code'=> $value['product_id'],
	// 			'med_name'=> $value['product_name'],
	// 			'expire_date'=> $expire_date,
	// 			'product_qty'=> $value['product_qty'],
	// 			'medicine_type'=> $medicine_type,
	// 			'medicine_unit'=> $medicine_unit
	// 		);
	// 	}

		
		
	// 	$html = $this->load->view('report/medicine_transfer_html', $data);
	// 	//echo $html;exit;
	// 	$filename = 'Medicine Transfer.html';
	// 	//file_put_contents(DIR_DOWNLOAD.Indent, $html);
	// 	header('Content-disposition: attachment; filename=' . $filename);
	// 	header('Content-type: text/html');
	// 	echo $html;exit;
	// }
}
