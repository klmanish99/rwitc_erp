<?php
class Controllercatalogisbhorse extends Controller {
  public function index() {
	$this->load->language('catalog/isb_horse');

	$this->document->setTitle($this->language->get('heading_title'));

	if (isset($this->request->get['filter_hourse_name'])) {

	  $filter_hourse_name = $this->request->get['filter_hourse_name'];
	} else {
	  $filter_hourse_name = '';
	}

	if (isset($this->request->get['filtermare_name'])) {
	  $filtermare_name = $this->request->get['filtermare_name'];
	} else {
	  $filtermare_name = '';
	}

	if (isset($this->request->get['filter_status'])) {
	  $filter_status = $this->request->get['filter_status'];
	} else {
	  $filter_status = '-3';
	}

	if (isset($this->request->get['filtersire'])) {
	  $filtersire = $this->request->get['filtersire'];
	} else {
	  $filtersire = '';
	}

	if (isset($this->request->get['filterstudname'])) {
	  $filterstudname = $this->request->get['filterstudname'];
	} else {
	  $filterstudname = '';
	}

	if (isset($this->request->get['year_of_foaling'])) {
	  $year_of_foaling = $this->request->get['year_of_foaling'];
	} else {
	  $year_of_foaling = '0';
	}


	if (isset($this->request->get['page'])) {
	  $page = $this->request->get['page'];
	} else {
	  $page = 1;
	}

	$url = '';

	if (isset($this->request->get['filter_hourse_name']) ) {
	  $url .= '&filter_hourse_name=' . $this->request->get['filter_hourse_name'];
	}

	if (isset($this->request->get['filtermare_name']) ) {
	  $url .= '&filtermare_name=' . $this->request->get['filtermare_name'];
	}

	if (isset($this->request->get['filter_status']) ) {
	  $url .= '&filter_status=' . $this->request->get['filter_status'];
	}

	 if (isset($this->request->get['filtersire']) ) {
	  $url .= '&filtersire=' . $this->request->get['filtersire'];
	}

	if (isset($this->request->get['filterstudname']) ) {
	  $url .= '&filterstudname=' . $this->request->get['filterstudname'];
	}

	if (isset($this->request->get['year_of_foaling']) ) {
	  $url .= '&year_of_foaling=' . $this->request->get['year_of_foaling'];
	}

	
	if (isset($this->request->get['page'])) {
	  $url .= '&page=' . $this->request->get['page'];
	}

	$data['breadcrumbs'] = array();

	$data['breadcrumbs'][] = array(
	  'text' => $this->language->get('text_home'),
	  'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
	);

	$data['breadcrumbs'][] = array(
	  'text' => $this->language->get('heading_title'),
	  'href' => $this->url->link('report/horse_undertaking_charge', 'token=' . $this->session->data['token'] . $url, true)
	);

	$this->load->model('catalog/isb_horse');

	$data['undertakingcharges_datas'] = array();


	$data['status_horse'] = array(
	  1 => 'Not Created',
	  0 => 'Created',
	  3 => 'Temp-Horse'
	);
	 $data['import_horse'] = $this->url->link('utility/import_horse', 'token=' . $this->session->data['token'], true);

	$filter_data = array(
	  'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
	  'limit'                  => $this->config->get('config_limit_admin'),
	  'filtermare_name' => $filtermare_name,
	  'filter_status' =>  $filter_status,
	  'filtersire' =>  $filtersire,
	  'filterstudname' =>  $filterstudname,
	  'year_of_foaling' =>  $year_of_foaling
	);
	$data['year_of_foalings'] = $this->model_catalog_isb_horse->getyearfoaling($filter_data);
	 $data['back_horse'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'], true);

  //echo "<pre>";print_r($filter_data);exit;

	$order_total = $this->model_catalog_isb_horse->getTotalIsbHorses($filter_data);
	$data['isb_horse_datas'] =array();

	$data['isb_horse_datas'] = $this->model_catalog_isb_horse->getIsbHorses($filter_data);

	//echo "<pre>";print_r($data['isb_horse_datas']);exit;

   

	$data['heading_title'] = $this->language->get('heading_title');

	$data['text_list'] = $this->language->get('text_list');
	$data['text_no_results'] = $this->language->get('text_no_results');
	$data['text_confirm'] = $this->language->get('text_confirm');
	$data['text_all_status'] = $this->language->get('text_all_status');

	$data['column_date_start'] = $this->language->get('column_date_start');
	$data['column_date_end'] = $this->language->get('column_date_end');
	$data['column_orders'] = $this->language->get('column_orders');
	$data['column_products'] = $this->language->get('column_products');
	$data['column_tax'] = $this->language->get('column_tax');
	$data['column_total'] = $this->language->get('column_total');

	$data['entry_date_start'] = $this->language->get('entry_date_start');
	$data['entry_date_end'] = $this->language->get('entry_date_end');
	$data['entry_group'] = $this->language->get('entry_group');
	$data['entry_status'] = $this->language->get('entry_status');

	$data['button_filter'] = $this->language->get('button_filter');

	$data['token'] = $this->session->data['token'];

	$this->load->model('localisation/order_status');

	$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

	$data['groups'] = array();

	$data['groups'][] = array(
	  'text'  => $this->language->get('text_year'),
	  'value' => 'year',
	);

	$data['groups'][] = array(
	  'text'  => $this->language->get('text_month'),
	  'value' => 'month',
	);

	$data['groups'][] = array(
	  'text'  => $this->language->get('text_week'),
	  'value' => 'week',
	);

	$data['groups'][] = array(
	  'text'  => $this->language->get('text_day'),
	  'value' => 'day',
	);

	$url = '';

	if (isset($this->request->get['filter_hourse_name']) ) {
	  $url .= '&filter_hourse_name=' . $this->request->get['filter_hourse_name'];
	}

	if (isset($this->request->get['filtermare_name']) ) {
	  $url .= '&filtermare_name=' . $this->request->get['filtermare_name'];
	}

	if (isset($this->request->get['filter_status']) ) {
	  $url .= '&filter_status=' . $this->request->get['filter_status'];
	}

	 if (isset($this->request->get['filtersire']) ) {
	  $url .= '&filtersire=' . $this->request->get['filtersire'];
	}

	if (isset($this->request->get['filterstudname']) ) {
	  $url .= '&filterstudname=' . $this->request->get['filterstudname'];
	}

	if (isset($this->request->get['year_of_foaling']) ) {
	  $url .= '&year_of_foaling=' . $this->request->get['year_of_foaling'];
	}

	if (isset($this->request->get['filter_group'])) {
	  $url .= '&filter_group=' . $this->request->get['filter_group'];
	}

	if (isset($this->request->get['filter_order_status_id'])) {
	  $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
	}

	$pagination = new Pagination();
	$pagination->total = $order_total;
	$pagination->page = $page;
	$pagination->limit = $this->config->get('config_limit_admin');
	$pagination->url = $this->url->link('catalog/isb_horse', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

	$data['pagination'] = $pagination->render();

	$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));
	
	$data['header'] = $this->load->controller('common/header');
	$data['column_left'] = $this->load->controller('common/column_left');
	$data['footer'] = $this->load->controller('common/footer');

	$data['filter_hourse_name'] = $filter_hourse_name;
	$data['filtermare_name'] = $filtermare_name;
	$data['filtersire'] = $filtersire;
	$data['filterstudname'] = $filterstudname;
	 $data['year_of_foaling'] = $year_of_foaling;
	$data['filter_status'] = $filter_status;
	

	$this->response->setOutput($this->load->view('catalog/isb_horse', $data));
  }

  public function autocompleteHorse() {
   $json = array();

	if (isset($this->request->get['horse_name'])) {
	  $this->load->model('catalog/isb_horse');

	  $results = $this->model_catalog_isb_horse->getHorsesIsbAuto($this->request->get['horse_name']);


	  if($results){
		foreach ($results as $result) {
		 if($result['FOALNAME']){
			$hourse_name = $result['FOALNAME'];
		  } else {
			if($result['MARENAT']){
			  $hourse_name =  $result['MARENAME'].'/'.$result['YROFFLNG'].'['.$result['MARENAT'].']';
			} else {
			  $hourse_name =  $result['MARENAME'].'/'.$result['YROFFLNG'];
			}
		  }

		  $json[] = array(
			'hore_isb_id' => $result['id'],
			'horse_name' => $hourse_name,
			'MARENAME' => $result['MARENAME'],
		  );
		}
	  }
	}

	/*echo '<pre>';print_r($json);
	  exit;*/
	$sort_order = array();

	foreach ($json as $key => $value) {
	  $sort_order[$key] = $value['hore_isb_id'];
	}



	array_multisort($sort_order, SORT_ASC, $json);

	$this->response->addHeader('Content-Type: application/json');
	//echo '<pre>';print_r($json);
	$this->response->setOutput(json_encode($json));
  }
  public function autocompletesire() {
   $json = array();

	if (isset($this->request->get['sire_name'])) {
	  $this->load->model('catalog/isb_horse');

	  $results = $this->model_catalog_isb_horse->getsire_namebAuto($this->request->get['sire_name']);


	  if($results){
		foreach ($results as $result) {
		 

		  $json[] = array(
		   'SIRENAME' => strip_tags(html_entity_decode($result['SIRENAME'], ENT_QUOTES, 'UTF-8')),
		  );
		}
	  }
	}

	/*echo '<pre>';print_r($json);
	  exit;*/
	$sort_order = array();

	foreach ($json as $key => $value) {
	  $sort_order[$key] = $value['SIRENAME'];
	}



	array_multisort($sort_order, SORT_ASC, $json);

	$this->response->addHeader('Content-Type: application/json');
	//echo '<pre>';print_r($json);
	$this->response->setOutput(json_encode($json));
  }

  public function autocompletestudname() {
   $json = array();

	if (isset($this->request->get['stud_name'])) {
	  $this->load->model('catalog/isb_horse');

	  $results = $this->model_catalog_isb_horse->getstud_nameAuto($this->request->get['stud_name']);


	  if($results){
		foreach ($results as $result) {

		  $json[] = array(
			'STUDNAME' => strip_tags(html_entity_decode($result['STUDNAME'], ENT_QUOTES, 'UTF-8')),
		  );
		}
	  }
	}

	/*echo '<pre>';print_r($json);
	  exit;*/
	$sort_order = array();

	foreach ($json as $key => $value) {
	  $sort_order[$key] = $value['STUDNAME'];
	}



	array_multisort($sort_order, SORT_ASC, $json);

	$this->response->addHeader('Content-Type: application/json');
	//echo '<pre>';print_r($json);
	$this->response->setOutput(json_encode($json));
  }

  	public function insertISbhorse() {
		$json = array();
		if (isset($this->request->post) ) {

			
			$horse_codes = $this->db->query("SELECT horse_code FROM horse1 WHERE 1=1 ORDER BY horse_code DESC LIMIT 1 ");
			if ($horse_codes->num_rows > 0) {
			  $horse_code = $horse_codes->row['horse_code'] + 1;;
			} else {
			  $horse_code = 1;
			}

			$isb_horse = $this->db->query("SELECT * FROM foal_isb WHERE id = '".$this->request->post['isb_horse_id']."'")->row;

            $foal_date = ($isb_horse['FOALDATE'] != '' && $isb_horse['FOALDATE'] != '0000-00-00') ? date('Y-m-d', strtotime($isb_horse['FOALDATE'])) : '';
            if($foal_date != ''){
				$foal_date = date('Y-m-d', strtotime($foal_date));
				$age = date_diff(date_create($foal_date), date_create('today'))->y;
			} else {
				$foal_date = '0000-00-00';
				$age = 0;
			}

            if($isb_horse['FOALNAME']){
                $horse_name = $isb_horse['FOALNAME'];
            } else {
                if($isb_horse['MARENAT']){
                    $horse_name =  $isb_horse['MARENAME'].'['.$isb_horse['MARENAT'].']'.' '.'/'.$isb_horse['YROFFLNG'];
                } else {
                    $horse_name =  $isb_horse['MARENAME'].'/'.$isb_horse['YROFFLNG'];
                }
            }

            if($isb_horse['SIRENAT']){
				$sir_name =  $isb_horse['SIRENAME'].'['.$isb_horse['SIRENAT'].']'.' '.'/'.$isb_horse['YROFFLNG'];
			} else {
				$sir_name =  $isb_horse['SIRENAME'].'/'.$isb_horse['YROFFLNG'];
			}

			if($isb_horse['MARENAT']){
				$dam_name =  $isb_horse['MARENAME'].'['.$isb_horse['MARENAT'].']'.' '.'/'.$isb_horse['YROFFLNG'];
			} else {
				$dam_name =  $isb_horse['MARENAME'].'/'.$isb_horse['YROFFLNG'];
			}

            $breeder_namez = $isb_horse['BR1'].','.$isb_horse['BR2'].','.$isb_horse['BR3'].','.$isb_horse['BR4'].','.$isb_horse['BR5'].','.$isb_horse['BR6'].','.$isb_horse['BR7'].','.$isb_horse['BR8'].','.$isb_horse['BR9'].','.$isb_horse['BR10'];

            $breeder_namezz = explode(',', $breeder_namez);
            $breeder_nameaa = '';
            foreach ($breeder_namezz as $key => $value) {
            	if(($value != '')){
            		$breeder_nameaa .=  $value.',' ;
            	}
            }

            
            $breeder_name = rtrim($breeder_nameaa, ',');

            // echo  $breeder_name;
            // exit;

            $passp_no = $isb_horse['PASSP_NO'];
            $life_no = $isb_horse['LIFENUMBER'];

			$last_upadate = date('Y-m-d');
			$user_ids = $this->session->data['user_id'];

		 	$insert =  $this->db->query("INSERT INTO `horse1` SET 
		 		horse_isb_id = '".(int)$isb_horse['id']."',
		 		official_name = '".$this->db->escape($horse_name)."',
		 		sire_name = '".$this->db->escape($sir_name)."', 
		 		sire_nationality = '".$this->db->escape($isb_horse['SIRENAT'])."', 
		 		dam_name = '". $this->db->escape($dam_name)."',
		 		dam_nationality = '".$this->db->escape($isb_horse['MARENAT'])."', 
		 		sex = '".$this->db->escape($isb_horse['HORSESEX'])."',
		 		color = '".$this->db->escape($isb_horse['HORSECOLOR'])."',
		 		age = '".$age."' ,
		 		foal_date = '".$foal_date."' ,
		 		user_id = '".$user_ids."' ,
		 		last_update_date = '".$last_upadate."' ,
		 		horse_status = '3' ,
		 		chip_no1 = '".$isb_horse['MICROCHIP1']."' ,
		 		chip_no2 = '".$isb_horse['MICROCHIP2']."',
		 		stud_form = '".$this->db->escape($isb_horse['STUDNAME'])."', 
		 		breeder_name = '".$this->db->escape($breeder_name)."', 
		 		passp_no = '".$this->db->escape($passp_no)."', 
		 		life_no = '".$this->db->escape($life_no)."', 
		 		reg_auth_id = 'RWITC' ,
		 		horse_code = '".$horse_code."',
		 		Id_by_brand = '".$isb_horse['BRAND1']."' ,
		 		orgin = 'Indian'

		    ");

		   //  echo'<pre>';
		   //  print_r("INSERT INTO `horse1` SET 
		 		// horse_isb_id = '".(int)$isb_horse['id']."',
		 		// official_name = '".$this->db->escape($horse_name)."',
		 		// sire_name = '".$this->db->escape($sir_name)."', 
		 		// sire_nationality = '".$this->db->escape($isb_horse['SIRENAT'])."', 
		 		// dam_name = '". $this->db->escape($dam_name)."',
		 		// dam_nationality = '".$this->db->escape($isb_horse['MARENAT'])."', 
		 		// sex = '".$this->db->escape($isb_horse['HORSESEX'])."',
		 		// color = '".$this->db->escape($isb_horse['HORSECOLOR'])."',
		 		// age = '".$age."' ,
		 		// foal_date = '".$foal_date."' ,
		 		// horse_status = '3' ,
		 		// chip_no1 = '".$isb_horse['MICROCHIP1']."' ,
		 		// chip_no2 = '".$isb_horse['MICROCHIP2']."',
		 		// stud_form = '".$this->db->escape($isb_horse['STUDNAME'])."', 
		 		// breeder_name = '".$this->db->escape($breeder_name)."', 
		 		// passp_no = '".$this->db->escape($passp_no)."', 
		 		// life_no = '".$this->db->escape($life_no)."', 
		 		// reg_auth_id = 'RWITC' ,
		 		// horse_code = '".$horse_code."',
		 		// Id_by_brand = '".$isb_horse['BRAND1']."' ,
		 		// orgin = 'Indian'

		   //  ");
		   //  exit;


		  	$update = $this->db->query("UPDATE `foal_isb` SET  status = '3' WHERE id = '" . (int)$this->request->post['isb_horse_id'] . "'  "); 



		 	if($insert && $update){
				$json['success']= '1';
			}  else {
				 $json['success']= '0';
			}
	 
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
  	}

  public function update_status() {
		$json = array();

		if (isset($this->request->get['hourse_id']) ) {

			$insert =  $this->db->query("DELETE FROM `horse1` WHERE horse_isb_id = '" .(int)$this->request->get['hourse_id'] . "' ");
			$update = $this->db->query("UPDATE `foal_isb` SET  status = '1' WHERE id = '" . (int)$this->request->get['hourse_id'] . "'  "); 

			 if($insert && $update){
				$json['success']= '1';
			}  else {
				 $json['success']= '0';
			}
	 
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}