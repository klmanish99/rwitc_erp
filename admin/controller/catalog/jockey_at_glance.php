<?php
class Controllercatalogjockeyatglance extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/ownership_shift_module');
		$this->document->setTitle('Jockey At Glance');
		$this->load->model('transaction/ownership_shift_module');
		$this->getList();
		//$this->rating();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['filter_race_name'])) {
			$data['filter_race_name'] =$this->request->get['filter_race_name'];
		} else{
			$data['filter_race_name']  ="";
		}

		if(isset($this->request->get['filter_acceptance_date'])){
			$data['filter_acceptance_date'] = $this ->request->get['filter_acceptance_date'];
		}
		 else{
			$data['filter_acceptance_date'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/ownership_shift_module/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/ownership_shift_module/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/ownership_shift_module/repair', 'token=' . $this->session->data['token'] . $url, true);


		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/ownership_shift_module/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/ownership_shift_module/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['categories'] = array();

		$data['ownerships_types'] = array(
			'Sale'  => 'Sale',
			'Lease'  => 'Lease',
			'Sub Lease'  => 'Sub Lease',
		);

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_race_name' => $data['filter_race_name'],
			'filter_acceptance_date' => $data['filter_acceptance_date']
		);

		if (isset($this->request->get['horse_id'])) {
			$horse_datass = $this->db->query("SELECT `official_name`, `horseseq` FROM `horse1` WHERE horseseq = '".$this->request->get['horse_id']."'")->row;
			$traniner_datass = $this->db->query("SELECT `trainer_name`, `trainer_id` FROM `horse_to_trainer` WHERE horse_id = '".$this->request->get['horse_id']."'")->row;

			$data['horse_name'] = $horse_datass['official_name'];
			$data['horse_id'] = $horse_datass['horseseq'];
			$data['trainer_name'] = $traniner_datass['trainer_name'];
			$data['trainer_id'] = $traniner_datass['trainer_id'];
			$data['gt_id'] = 1;
		} else {
			$data['horse_name'] = '';
			$data['horse_id'] = '';
			$data['trainer_name'] = '';
			$data['trainer_id'] = '';
			$data['gt_id'] = 0;
		}

		if(isset($this->request->post['fromdate'])) {
			$data['fromdate'] = $this->request->post['fromdate'];
		} else {
			$data['fromdate'] = date('d-m-Y');
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/jockey_at_glance', $data));
	}
	

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/ownership_shift_module')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['linkedOwnerName'] as $key => $value) {
			if ($value['linked_owner_id'] == '') {
				$this->error['linkedOwner'] = 'Please select valid linked owner.';
			}
		}

		if ($this->request->post['gstType'] == 'Registered' && $this->request->post['gstNo'] == '') {
			$this->error['gst_number'] = 'GST No can not be blank.';
		}

		foreach ($this->request->post['owner_shared_color'] as $key => $value) {
			if ($value['shared_owner_id'] == '') {
				$this->error['sharedColor'] = 'Please select valid owner to share color.';
			}
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	public function autocompleteJockey(){

		$json = array();
		if (isset($this->request->get['jockey_name'])) {
			$this->load->model('transaction/ownership_shift_module');
			$filter_data = array(
				'jockey_name' => $this->request->get['jockey_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$sql = "SELECT * FROM jockey WHERE 1=1 ";

			if (!empty($this->request->get['jockey_name'])) {
				$sql .= " AND `jockey_name` LIKE '%" . ($this->request->get['jockey_name']) . "%'";
			}

			$results = $this->db->query($sql)->rows;


			if ($results) {
				foreach ($results as $result) {

					$json[] = array(
						'jockey_id' => $result['jockey_id'],
						'jockey_code' => $result['jockey_code'],
						'jockey_name'        => strip_tags(html_entity_decode($result['jockey_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
