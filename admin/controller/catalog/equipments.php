<?php
class ControllerCatalogEquipments extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/equipments');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/equipments');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/equipments');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/equipments');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			$this->model_catalog_equipments->addEquipments($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/equipments', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/equipments');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/equipments');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			$this->model_catalog_equipments->editEquipments($this->request->get['equipment_id'], $this->request->post);
			// echo '<pre>';
			// print_r($this->request->post);
			
			// exit;
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/equipments', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/equipments');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/equipments');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $equipment_id) {
				$this->model_catalog_equipments->deleteCategory($equipment_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/equipments', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function repair() {
		$this->load->language('catalog/equipments');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/equipments');

		if ($this->validateRepair()) {
			$this->model_catalog_equipments->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('catalog/equipments', 'token=' . $this->session->data['token'], true));
		}

		$this->getList();
	}

	protected function getList() {

		if (isset($this->request->get['filter_equipment_name'])) {
			$filter_equipment_name =$this->request->get['filter_equipment_name'];
		} else{
			$filter_equipment_name ="";
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = '1';
			$data['filter_status'] = '1';
		}

		// if (isset($this->request->get['filter_equipment_name'])) {
		// 	$url .= '&filter_equipment_name=' . $this->request->get['filter_equipment_name'];
		// }

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/equipments', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/equipments/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/equipments/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/equipments/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_equipment_name' => $filter_equipment_name,
			'filter_status' => $filter_status
		);

		$category_total = $this->model_catalog_equipments->getTotalCategories();

		$results = $this->model_catalog_equipments->getCategories($filter_data);

		foreach ($results as $result) {
			$data['categories'][] = array(
				'equipment_id' => $result['equipment_id'],
				'short_name'        => $result['short_name'],
				'equipment_name'        => $result['equipment_name'],
				'equipment_status'        => $result['equipment_status'],
				
				'edit'        => $this->url->link('catalog/equipments/edit', 'token=' . $this->session->data['token'] . '&equipment_id=' . $result['equipment_id'] . $url, true),
				'delete'      => $this->url->link('catalog/equipments/delete', 'token=' . $this->session->data['token'] . '&equipment_id=' . $result['equipment_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/equipments', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/equipments', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/equipments', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/equipments_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['valierr_short_name'])) {
			$data['valierr_short_name'] = $this->error['valierr_short_name'];
		} else {
			$data['valierr_short_name'] = '';
		}

		if (isset($this->error['valierr_equipment_name'])) {
			$data['valierr_equipment_name'] = $this->error['valierr_equipment_name'];
		} else {
			$data['valierr_equipment_name'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/equipments', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['equipment_id'])) {
			$data['action'] = $this->url->link('catalog/equipments/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/equipments/edit', 'token=' . $this->session->data['token'] . '&equipment_id=' . $this->request->get['equipment_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/equipments', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['equipment_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_equipments->getEquipment($this->request->get['equipment_id']);
		}
	 

		if (isset($this->request->post['equipment_name'])) {
			$data['equipment_name'] = $this->request->post['equipment_name'];
		} elseif (!empty($category_info)) {
			$data['equipment_name'] = $category_info['equipment_name'];
		} else {
			$data['equipment_name'] = '';
		}

		if (isset($this->request->post['short_name'])) {
			$data['short_name'] = $this->request->post['short_name'];
		} elseif (!empty($category_info)) {
			$data['short_name'] = $category_info['short_name'];
		} else {
			$data['short_name'] = '';
		}

		if (isset($this->request->post['equipment_status'])) {
			$data['equipment_status'] = $this->request->post['equipment_status'];
		} elseif (!empty($category_info)) {
			$data['equipment_status'] = $category_info['equipment_status'];
		} else {
			$data['equipment_status'] = true;
		}

		if (isset($this->request->post['category_layout'])) {
			$data['category_layout'] = $this->request->post['category_layout'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_layout'] = $this->model_catalog_equipments->getCategoryLayouts($this->request->get['category_id']);
		} else {
			$data['category_layout'] = array();
		}

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/equipments_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/equipments')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->request->post['short_name'] == ''){
			$this->error['valierr_short_name'] = 'Please Select Short Name!';
		}

		if($this->request->post['equipment_name'] == ''){
			$this->error['valierr_equipment_name'] = 'Please Select Equipments Name!';
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/equipments')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/equipments')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/equipments');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_equipments->getCategories($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteEquipment() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['equipment_name'])) {
			$this->load->model('catalog/equipments');

			$results = $this->model_catalog_equipments->getEquipmentAuto($this->request->get['equipment_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'equipment_id' 		=> $result['equipment_id'],
						'equipment_name'    => strip_tags(html_entity_decode($result['equipment_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['equipment_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}
}
