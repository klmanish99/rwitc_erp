<?php
class ControllerCatalogAssignTrainer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/assign_trainer');

		$this->document->setTitle('Trainer To Staff');

		$this->load->model('catalog/trainer_staff');

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Trainer To Staff',
			'href' => $this->url->link('catalog/assign_trainer', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['categories'] = array();

		$data['token'] = $this->session->data['token'];
		
		$data['action'] = $this->url->link('catalog/assign_trainer/all_update_trainer', 'token=' . $this->session->data['token'] . $url, true);

		$category_total = $this->model_catalog_trainer_staff->getTotalTrainerStaff();

		$results = $this->model_catalog_trainer_staff->getTrainerStaffs();

		foreach ($results as $result) {
			$data['categories'][] = array(
				'id' => $result['id'],
				'staff'     => $result['staff'],
				'designation'        => $result['designation'],
				'trainer_id'        => $result['trainer_id'],
				'trainer_name'        => $result['trainer_name'],
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/assign_trainer', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/assign_trainer', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/assign_trainer', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/assign_trainer', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/assign_trainer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->request->post['adhar_number'] == ''){
			$this->error['valierr_adhar_number'] = 'Please Select Adhar Number!';
		}
		
		if($this->request->post['staff_name'] == ''){
			$this->error['valierr_staff_name'] = 'Please Select Staff Name!';
		}

		if($this->request->post['trainer_name'] == ''){
			$this->error['valierr_trainer_name'] = 'Please Select Trainer Name!';
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	public function update_trainer() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$this->db->query("UPDATE trainer_staff_entry SET trainer_name = '".$this->request->get['trainer_name']."', trainer_id = '".$this->request->get['trainer_id']."' WHERE id = '".$this->request->get['id']."' ");

		$this->response->redirect($this->url->link('catalog/assign_trainer', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function all_update_trainer() {
		if ($this->request->post['trainer']) {
			foreach ($this->request->post['trainer'] as $key => $value) {
				
				$this->db->query("UPDATE trainer_staff_entry SET trainer_name = '".$value['trainer_name']."', trainer_id = '".$value['trainer_id']."', designation = '".$value['designation']."' WHERE id = '".$value['staff_id']."' ");
			}
		}


		$this->response->redirect($this->url->link('catalog/assign_trainer', 'token=' . $this->session->data['token'] . $url, true));
	}
}
