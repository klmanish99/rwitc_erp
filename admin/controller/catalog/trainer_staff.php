<?php
class ControllerCatalogTrainerStaff extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/trainer_staff');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/trainer_staff');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/trainer_staff');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/trainer_staff');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			$this->model_catalog_trainer_staff->addTrainerStaff($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/trainer_staff');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/trainer_staff');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			$this->model_catalog_trainer_staff->editTrainerStaff($this->request->get['id'], $this->request->post);
			// echo '<pre>';
			// print_r($this->request->post);
			
			// exit;
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/trainer_staff');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/trainer_staff');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_trainer_staff->deleteTrainerStaff($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function repair() {
		$this->load->language('catalog/trainer_staff');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/trainer_staff');

		if ($this->validateRepair()) {
			$this->model_catalog_trainer_staff->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'], true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/trainer_staff/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/trainer_staff/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/trainer_staff/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['filter_doctor_name'])) {
			$filter_doctor_name = $this->request->get['filter_doctor_name'];
			$data['filter_doctor_name'] = $this->request->get['filter_doctor_name'];
		}
		else{
			$filter_doctor_name = '';
			$data['filter_doctor_name'] = '';
		}

		if (isset($this->request->get['filter_trainer_name'])) {
			$filter_trainer_name = $this->request->get['filter_trainer_name'];
			$data['filter_trainer_name'] = $this->request->get['filter_trainer_name'];
		}
		else{
			$filter_trainer_name = '';
			$data['filter_trainer_name'] = '';
		}

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_doctor_name'	=>	$filter_doctor_name,
			'filter_trainer_name'	=>	$filter_trainer_name,
		);

		$category_total = $this->model_catalog_trainer_staff->getTotalTrainerStaff();

		$results = $this->model_catalog_trainer_staff->getTrainerStaffs($filter_data);

		foreach ($results as $result) {
			$data['categories'][] = array(
				'id' => $result['id'],
				'staff'     => $result['staff'],
				'adhar_no'        => $result['adhar_no'],
				'trainer_name'        => $result['trainer_name'],
				'edit'        => $this->url->link('catalog/trainer_staff/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true),
				'delete'      => $this->url->link('catalog/trainer_staff/delete', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/trainer_staff_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_trainer_staff_code'] = $this->language->get('entry_trainer_staff_code');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['valierr_adhar_number'])) {
			$data['valierr_adhar_number'] = $this->error['valierr_adhar_number'];
		} else {
			$data['valierr_adhar_number'] = '';
		}

		if (isset($this->error['valierr_staff_name'])) {
			$data['valierr_staff_name'] = $this->error['valierr_staff_name'];
		} else {
			$data['valierr_staff_name'] = '';
		}

		if (isset($this->error['valierr_trainer_name'])) {
			$data['valierr_trainer_name'] = $this->error['valierr_trainer_name'];
		} else {
			$data['valierr_trainer_name'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/trainer_staff/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/trainer_staff/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_trainer_staff->getTrainerStaff($this->request->get['id']);
		}

		if (isset($this->request->post['adhar_number'])) {
			$data['adhar_number'] = $this->request->post['adhar_number'];
		} elseif (!empty($category_info)) {
			$data['adhar_number'] = $category_info['adhar_no'];
		} else {
			$data['adhar_number'] = '';
		}

		if (isset($this->request->post['designation'])) {
			$data['designation'] = $this->request->post['designation'];
		} elseif (!empty($category_info)) {
			$data['designation'] = $category_info['designation'];
		} else {
			$data['designation'] = '';
		}

		if (isset($this->request->post['staff_name'])) {
			$data['staff_name'] = $this->request->post['staff_name'];
		} elseif (!empty($category_info)) {
			$data['staff_name'] = $category_info['staff'];
		} else {
			$data['staff_name'] = '';
		}

		
		if (isset($this->request->post['file_number_upload'])) {
			$data['file_number_upload'] = $this->request->post['file_number_upload'];
		} elseif (!empty($category_info)) {
			$data['file_number_upload'] = $category_info['file_number_upload'];
		} else {
			$data['file_number_upload'] = '';
		}

		if (!empty($category_info)) {
    		// $img = ltrim($category_info['image_source'],"/");
    		if($category_info['uploaded_file_source'] != ''){
    			$data['img_path'] = $category_info['uploaded_file_source'];
    		} else {
    			$data['img_path'] = '1';
    		}
		} else {
      		$data['img_path'] = '#';
    	}
		
		if (isset($this->request->post['uploaded_file_source'])) {
			$data['uploaded_file_source'] = $this->request->post['uploaded_file_source'];
		} elseif (!empty($category_info)) {
			$data['uploaded_file_source'] = $category_info['uploaded_file_source'];
		} else {
			$data['uploaded_file_source'] = '';
		}

		if (isset($this->request->post['address'])) {
			$data['address'] = $this->request->post['address'];
		} elseif (!empty($category_info)) {
			$data['address'] = $category_info['address'];
		} else {
			$data['address'] = '';
		}

		if (isset($this->request->post['blood_group'])) {
			$data['blood_group'] = $this->request->post['blood_group'];
		} elseif (!empty($category_info)) {
			$data['blood_group'] = $category_info['blood_group'];
		} else {
			$data['blood_group'] = '';
		}

		if (isset($this->request->post['trainer_name'])) {
			$data['trainer_name'] = $this->request->post['trainer_name'];
		} elseif (!empty($category_info)) {
			$data['trainer_name'] = $category_info['trainer_name'];
		} else {
			$data['trainer_name'] = '';
		}

		if (isset($this->request->post['trainer_id'])) {
			$data['trainer_id'] = $this->request->post['trainer_id'];
		} elseif (!empty($category_info)) {
			$data['trainer_id'] = $category_info['trainer_id'];
		} else {
			$data['trainer_id'] = '';
		}

		$data['token'] = $this->session->data['token'];

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/trainer_staff_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/trainer_staff')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->request->post['adhar_number'] == ''){
			$this->error['valierr_adhar_number'] = 'Please Select Adhar Number!';
		}

		// if (($this->request->post['doctor_code'] != '')  && (!isset($this->request->get['id'])) ) {
		// 	$avail_code = $this->db->query("SELECT * FROM doctor WHERE `doctor_code` = '".$this->request->post['doctor_code']."' ");
		// 	if ($avail_code->num_rows > 0) {
		// 		$this->error['valierr_staff_name'] = 'This Code Is Already In Used! Please Select Another!';
		// 	}
		// }
		
		if($this->request->post['staff_name'] == ''){
			$this->error['valierr_staff_name'] = 'Please Select Staff Name!';
		}

		// if($this->request->post['trainer_name'] == ''){
		// 	$this->error['valierr_trainer_name'] = 'Please Select Trainer Name!';
		// }
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/trainer_staff')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/trainer_staff')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	// public function autocomplete() {
	// 	/*echo'<pre>';
	// 	print_r($this->request->get);
	// 	exit;*/
	// 	$json = array();

	// 	if (isset($this->request->get['filter_doctor_name'])) {
	// 		$this->load->model('catalog/trainer_staff');

	// 		$results = $this->model_catalog_trainer_staff->getDoctorAuto($this->request->get['filter_doctor_name']);
	// 		/*echo'<pre>';
	// 		print_r($results);
	// 		exit;*/

	// 		foreach ($results as $result) {

	// 			$json[] = array(
	// 				'id' => $result['id'],
	// 				'doctor_name'     => strip_tags(html_entity_decode($result['doctor_name'], ENT_QUOTES, 'UTF-8'))
	// 			);
	// 		}
	// 	}

	// 	$sort_order = array();

	// 	foreach ($json as $key => $value) {
	// 		$sort_order[$key] = $value['doctor_name'];
	// 		//$sort_order[$key] = $value['place'];
	// 	}

	// 	array_multisort($sort_order, SORT_ASC, $json);

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// 	/*echo'<pre>';
	// 	print_r($json);
	// 	exit;*/
	// }

	// public function autocompletes() {
	// 	/*echo'<pre>';
	// 	print_r($this->request->get);
	// 	exit;*/
	// 	$json = array();

	// 	if (isset($this->request->get['filter_doctor_code'])) {
	// 		$this->load->model('catalog/trainer_staff');

	// 		$results = $this->model_catalog_trainer_staff->getDoctorAutos($this->request->get['filter_doctor_code']);
	// 		/*echo'<pre>';
	// 		print_r($results);
	// 		exit;*/

	// 		foreach ($results as $result) {

	// 			$json[] = array(
	// 				'id' => $result['id'],
	// 				'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
	// 			);
	// 		}
	// 	}

	// 	$sort_order = array();

	// 	foreach ($json as $key => $value) {
	// 		$sort_order[$key] = $value['doctor_code'];
	// 		//$sort_order[$key] = $value['place'];
	// 	}

	// 	array_multisort($sort_order, SORT_ASC, $json);

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// 	/*echo'<pre>';
	// 	print_r($json);
	// 	exit;*/
	// }

	public function autocompleteparent() {
		$json = array();
		 // echo '<pre>';
		 // print_r($this->request->get);
		 // exit;
		if (isset($this->request->get['parent'])) {
			$this->load->model('catalog/trainer_staff');

			$filter_data = array(
				'filter_parent' => $this->request->get['parent'],
				//'start'       => 0,
				//'limit'       => 5
			);
			$results = $this->model_catalog_trainer_staff->getsupplier($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'trainer_name'     => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function upload_profile() {
		//$this->load->language('catalog/employee');
		$json = array();
		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/trainer_staff/profile_pic')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$file_show_path = HTTP_CATALOG.'system/storage/download/trainer_staff/profile_pic/';
		$file_upload_path = DIR_DOWNLOAD.'trainer_staff/profile_pic/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				$this->log->write(print_r($this->request->files, true));
				$this->log->write($image_name);
				$this->log->write($img_extension);
				$this->log->write($filename);

				
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';
				$this->log->write(print_r($allowed, true));
				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] ='Please upload valid file!'. $this->request->files['file']['error'];
				}
			} else {
				$json['error'] ='Please upload valid file!';
			}
		}

		if (!$json) {
			$file = $filename;
			if(move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file)){
				$destFile = $file_upload_path . $file;
				//chmod($destFile, 0777);
				$json['filename'] = $file;
				$json['link_href'] = $file_show_path . $file;

				$json['success'] = 'Your file was successfully uploaded!';
			} else {
				$json['error'] = 'Please upload valid file!';
			}
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
