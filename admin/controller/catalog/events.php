<?php
class ControllerCatalogEvents extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/events');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/events');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/events');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/events');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_events->addEvents($this->request->post);

			$this->session->data['success'] = 'Event Is Successfully Added';

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['date'])) {
				$url .= '&date=' . $this->request->get['date'];
			}

			$this->response->redirect($this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$this->load->language('catalog/events');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/events');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_events->editEvents($this->request->get['event_id'], $this->request->post);
			// echo '<pre>';
			// print_r($this->request->post);
			
			// exit;
			$this->session->data['success'] = 'Event Is Successfully Modified';

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['date'])) {
				$url .= '&date=' . $this->request->get['date'];
			}

			$this->response->redirect($this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	protected function getList() {
		/*echo'<pre>';
		print_r($this->request->get['date']);
		exit;*/

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['date'])) {
			$url .= '&date=' . $this->request->get['date'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Events',
			'href' => $this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/events/add', 'token=' . $this->session->data['token'] . '&date=' . $this->request->get['date'] . $url, true);

		$results['categories'] = array();
		if (isset($this->request->get['date'])) {
			$dates = $this->request->get['date'];
		} else {
			$dates = '';
		}
		/*echo'<pre>';
		print_r($dates);
		exit;*/
		$data['categories'] = array();
		$results = $this->model_catalog_events->getEvent($dates);
		if ($dates != '') {
			foreach ($results as $result) {
				$data['categories'][] = array(
					'event_id'=> $result['event_id'],
					'event_name'        => $result['event_name'],
					'description'        => $result['description'],
					'date_of_event'     => date('d-m-Y', strtotime($result['date_of_event'])),
					'edit'        => $this->url->link('catalog/events/edit', 'token=' . $this->session->data['token'] .'&date=' .$this->request->get['date'] .'&event_id=' .$result['event_id']. $url, true)
				);
			}
		} 
		/*echo'<pre>';
		print_r($data['categories']);
		exit;*/


		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['valierr_date_of_event'])) {
			$data['valierr_date_of_event'] = $this->error['valierr_date_of_event'];
		} 

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/events', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/events', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		//$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		//$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/events_list', $data));
	}

	protected function getForm() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Events',
			'href' => $this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['event_id'])) {
			$data['action'] = $this->url->link('catalog/events/add', 'token=' . $this->session->data['token'] . '&date=' . $this->request->get['date'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/events/edit', 'token=' . $this->session->data['token'] . '&date=' . $this->request->get['date'] . '&event_id=' . $this->request->get['event_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/events', 'token=' . $this->session->data['token'] . '&date=' . $this->request->get['date'] . $url, true);

		if (isset($this->request->get['event_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_events->getEvents($this->request->get['event_id']);
		}
	 

		if (isset($this->request->post['event_name'])) {
			$data['event_name'] = $this->request->post['event_name'];
		} elseif (!empty($category_info)) {
			$data['event_name'] = $category_info['event_name'];
		} else {
			$data['event_name'] = '';
		}

		if (isset($this->request->post['description'])) {
			$data['description'] = $this->request->post['description'];
		} elseif (!empty($category_info)) {
			$data['description'] = $category_info['description'];
		} else {
			$data['description'] = '';
		}

		if (isset($this->request->post['date_of_event'])) {
			$data['date_of_event'] = $this->request->post['date_of_event'];
		} elseif (!empty($category_info)) {
			$data['date_of_event'] = date('d-m-Y', strtotime($category_info['date_of_event']));
		} else {
			$data['date_of_event'] = '';
		}
		/*echo'<pre>';
		print_r($data['date_of_event']);
		exit;*/

		$this->load->model('catalog/events');


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/events_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/events')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if($this->request->post['date_of_event'] == '') {
			$this->error['valierr_date_of_event'] = 'Please Enter Valid Date';
		}
		
		return !$this->error;
	}

}