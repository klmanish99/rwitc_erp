<?php
/*require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;*/
date_default_timezone_set("Asia/Kolkata");
class ControllerCataloginward extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/inward');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/inward');
		$this->load->model('catalog/product');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/inward');
		/*$this->load->model('catalog/product');*/

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/inward');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_inward->addinward($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/inward');
		$this->load->model('catalog/product');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/inward');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_inward->editinward($this->request->get['order_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

		public function edit1() {
		$this->load->language('catalog/inward');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/inward');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_inward->editinward1($this->request->get['order_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/inward/getForm', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getFormView();
	}

	public function getFormView() {
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['req'])) {
			$data['error_req'] = $this->error['req'];
		} else {
			$data['error_req'] = '';
		}

		if (isset($this->error['reason'])) {
			$data['error_reason'] = $this->error['reason'];
		} else {
			$data['error_reason'] = array();
		}


		if (isset($this->error['date'])) {
			$data['error_date'] = $this->error['date'];
		} else {
			$data['error_date'] = array();
		}

		// echo '<pre>';
		// print_r($data['error_reason']);
		// exit;

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_quantity'])) {
			$url .= '&filter_inward_quantity=' . urlencode(html_entity_decode($this->request->get['filter_inward_quantity'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/inward/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/inward/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . $url, true);
		}

		$data['approve'] = $this->url->link('catalog/inward/approve_onform', 'token=' . $this->session->data['token'] . '&order_id=' .  $this->request->get['order_id'] . $url, true);


		$data['cancel'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true);
		

		if (isset($this->request->get['order_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$inward_info = $this->model_catalog_inward->getinward($this->request->get['order_id']);
		}

		$data['token'] = $this->session->data['token'];

				if (isset($this->request->post['order_no'])) {
			$data['order_no'] = $this->request->post['order_no'];
		} elseif (!empty($inward_info)) {
			$data['order_no'] = $inward_info['order_no'];
		} else {
			$data['order_no'] = '';
		}

		if (!empty($inward_info)) {
			$data['approval_status'] = $inward_info['approval_status'];
		} else {
			$data['approval_status'] = '';
		}

		if (!empty($inward_info)) {
			$data['approval'] = $this->url->link('catalog/inward/indent_approval', 'token=' . $this->session->data['token'] . '&order_id=' . $inward_info['order_id'] . $url, true);
		} else {
			$data['approval'] = '';
		}
		// 		if (isset($this->request->post['indent_no'])) {
		// 	$data['indent_no'] = $this->request->post['indent_no'];
		// } elseif (!empty($inward_info)) {
		// 	$data['indent_no'] = $inward_info['indent_no'];
		// } else {
		// 	$data['indent_no'] = '';
		// }
		if (isset($this->request->post['supplier'])) {
			$data['supplier'] = $this->request->post['supplier'];
		} elseif (!empty($inward_info)) {
			$data['supplier'] = $inward_info['supplier'];
		} else {
			$data['supplier'] = '';
		}

		if (isset($this->request->post['date'])) {
			$data['date'] = $this->request->post['date'];
		} elseif (!empty($inward_info)) {
			$data['date'] = $inward_info['date'];
		} else {
			$data['date'] = date('d-m-Y');
		}

		// if (isset($this->request->post['pose_no'])) {
		// 	$data['pose_no'] = $this->request->post['pose_no'];
		// } elseif (!empty($inward_info)) {
		// 	$data['pose_no'] = $inward_info['pose_no'];
		// } else {
		// 	$data['pose_no'] = '';
		// }

		if (isset($this->request->get['order_id'])) {
			$data['order_id'] = $this->request->get['order_id'];
		} elseif (!empty($inward_info)) {
			$data['order_id'] = $inward_info['order_id'];
		} else {
			$data['order_id'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($inward_info)) {
			$data['status'] = $inward_info['status'];
		} else {
			$data['status'] = '';
		}


		$types = array(
			
		);

		$data['types'] = $types;

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} elseif (!empty($inward_info)) {
			$product_raw_datass = $this->db->query("SELECT * FROM `oc_inwarditem` WHERE `order_id` = '".$inward_info['order_id']."' ")->rows;
			//echo "<pre>";print_r($product_raw_datass);exit;
			$productraw_datas = array();

			$filter_month = date('m');
			$filter_year = date('Y');

			if($filter_month == 1){
				$prev_filter_month = 12;
				$prev_filter_year = $filter_year - 1;
			} else {
				$prev_filter_month = $filter_month - 1;
				$prev_filter_year = $filter_year;
			}

			foreach($product_raw_datass as $pkeys => $pvalues){
				$inward_po = $this->db->query("SELECT reduce_po_qty FROM `oc_inwarditem` WHERE `po_no` = '".$pvalues['po_no']."' ORDER BY `productraw_id` DESC LIMIT 1 ");
				if ($inward_po->num_rows > 0) {
					$reduce_po_qty = $inward_po->row['reduce_po_qty'];
				} else{
					$reduce_po_qty = $result['qty'];
				}
				$productraw_datas[] = array(
					'productraw_name' => $pvalues['productraw_name'],
					'med_id' => $pvalues['medicine_id'],
					'po_no' => $pvalues['po_no'],
					'po_qty' => $pvalues['po_qty'],
					'productraw_id' => $pvalues['productraw_id'],
					'product_id' => $pvalues['product_id'],
					'quantity' => $pvalues['quantity'],
					'purchase_price' => $pvalues['purchase_price'].' '.$pvalues['packing'],
					'value' => $pvalues['value'],
					'ex_date' => $pvalues['ex_date'],
					'batch_no' => $pvalues['batch_no'],
					'packing' => $pvalues['packing'].' '.$pvalues['volume'].' '.$pvalues['unit'],
					'ordered_quantity' => $pvalues['ordered_quantity'].' '.$pvalues['unit'],
					'gst_rate' => $pvalues['gst_rate'],
					'gst_value' => $pvalues['gst_value'],
					'total' => $pvalues['total'],
					'reduce_po_qty' => $reduce_po_qty,
				);	
			}
			$data['productraw_datas'] = $productraw_datas;
		} else {
			$data['productraw_datas'] = array();
		}

		//$data['user_type'] = $this->user->getUserType();	
		$data['user_group_id'] = $this->user->getGroupId();	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/inward_view', $data));
	}

	public function stock() {
		$this->load->language('catalog/inward');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/inward');


		$datas = $this->model_catalog_inward->daterawmaterial();
		foreach ($datas as $key => $value) {
			$data['datas'][] = array(
				'date' => $value['date']
			);

		$is_date = $this->db->query("SELECT * FROM `oc_inwarditem` WHERE `order_id` = '".$value['order_id']."' AND date = '".$value['date']."' ");
			if ($is_date->num_rows == 0){
					$this->db->query("INSERT INTO oc_inwarditem SET `order_id` = '".$value['order_id']."',date = '".$value['date']."' ");
				}
				
		}
		// echo '<pre>';print_r($is_date);exit;

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true));
		

		$this->getList();
	}

	public function delete() {
		$this->load->language('catalog/inward');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/inward');

		if (isset($this->request->post['selected']) /*&& $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $order_id) {
				$this->model_catalog_inward->deleteinward($order_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}
	public function approve() {
		$order_id1=$_GET['order_id'];
		$user_group_id=$this->db->query("UPDATE oc_inward SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$user_group=$this->db->query("UPDATE oc_inwarditem SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$this->response->redirect($this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function approve_onform() {
		$order_id1=$_GET['order_id'];
		$user_group_id=$this->db->query("UPDATE oc_inward SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$user_group=$this->db->query("UPDATE oc_inwarditem SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$this->response->redirect($this->url->link('catalog/inward/edit1', 'token=' . $this->session->data['token'] . '&order_id=' .$order_id1. $url, true));
	}

	protected function getList() {
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		if (isset($this->request->get['filter_inward'])) {
			$filter_inward = $this->request->get['filter_inward'];
		} else {
			$filter_inward = null;
		}

		if (isset($this->request->get['filter_order_no'])) {
			$filter_order_no = $this->request->get['filter_order_no'];
		} else {
			$filter_order_no = null;
		}

		if (isset($this->request->get['filter_medicine'])) {
			$filter_medicine = $this->request->get['filter_medicine'];
		} else {
			$filter_medicine = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['filter_dates'])) {
			$filter_dates = $this->request->get['filter_dates'];
		} else {
			$filter_dates = null;
		}

		if (isset($this->request->get['filter_productsort'])) {
			$filter_productsort = $this->request->get['filter_productsort'];
		} else {
			$filter_productsort = null;
		}

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$filter_inward_category = $this->request->get['filter_inward_category'];
		} else {
			$filter_inward_category = null;
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$filter_inward_category_id = $this->request->get['filter_inward_category_id'];
		} else {
			$filter_inward_category_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = 'Active';
			$data['filter_status'] = 'Active';
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}


		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = 0;
			$data['filter_status'] = 0;
		}

		$data['status'] =array(
				'0'  =>"Active",
				'1'  =>'In-Active'
		);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . $url, true);
		$data['add'] = $this->url->link('catalog/inward/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['stock'] = $this->url->link('catalog/inward/stock', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/inward/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['inwards'] = array();

		$filter_data = array(
			'filter_inward'	  => $filter_inward,
			'filter_order_no'	  => $filter_order_no,
			'filter_medicine'	  => $filter_medicine,
			'filter_date'	  => $filter_date,
			'filter_dates'	  => $filter_dates,
			'filter_status'	=> $filter_status,
			'filter_order_id'  => $filter_order_id,
			'filter_inward_category'	  => $filter_inward_category,
			'filter_inward_category_id'  => $filter_inward_category_id,
			'filter_productsort'	=> $filter_productsort,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
			//echo '<pre>';
			 //print_r($filter_data);
			//exit;

		$inward_total = $this->model_catalog_inward->getTotalinwards($filter_data);

		$results = $this->model_catalog_inward->getinwards($filter_data);
		$user_group_id = $this->user->getGroupId();

		foreach ($results as $result) {

			$logs_info = $this->db->query("SELECT * FROM `inward_logs` WHERE `inw_id` = '".$result['order_id']."' ORDER BY `id` DESC LIMIT 1 ");
			// echo'<pre>';
			// print_r("SELECT * FROM `supplier_logs` WHERE `supp_id` = '".$result['vendor_id']."' ");
			if ($logs_info->num_rows > 0) {
				$fname = $logs_info->row['fname'];
				$lname = $logs_info->row['lname'];
				$log_date = date('d-m-Y', strtotime($logs_info->row['log_date']));
				$log_time = $logs_info->row['log_time'];
				$log_datas = '<span>'.'Name: '.$fname.' '.$lname.'</span>'.'<br>'.'<span>'.'Date: '.$log_date.'</span>'.'<br>'.'<span>'.'Time: '.$log_time.'</span>';
			} else {
				$log_datas = '';
			}
			
			$data['inwards'][] = array(
				'order_id' => $result['order_id'],
				'order_no' => $result['order_no'],
				'supplier' => $result['supplier'],
				'date' => $result['date'],
				'log_datas'     => $log_datas,
				'status' => $result['status'],
				'view_status' => $result['view_status'],
				'approval_status' => $result['approval_status'],
				'approve'     => $this->url->link('catalog/inward/approve', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'edit'     => $this->url->link('catalog/inward/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'edit1'     => $this->url->link('catalog/inward/edit1', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'cancel'     => $this->url->link('catalog/inward/cancel', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'print'     => $this->url->link('catalog/inward/prints', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'email'     => $this->url->link('catalog/inward/email', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'approval'     => $this->url->link('catalog/inward/indent_approval', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'calculate'     => $this->url->link('catalog/inward/calculate', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true)
			);
		}

		$data['productsorts'] = array(
			'a'=>'A','b'=>'B','c'=>'C','d'=>'D','e'=>'E','f'=>'F','g'=>'G','h'=>'H',
			'i'=>'I','j'=>'J','k'=>'K','l'=>'L','m'=>'M','n'=>'N','o'=>'O','p'=>'P',
			'q'=>'Q','r'=>'R','s'=>'S','t'=>'T','u'=>'U','v'=>'V','w'=>'W','x'=>'X',
			'y'=>'Y','z'=>'Z','Other'
		);
			// echo '<pre>';
			// print_r($data['productsorts']);
			// exit;
		$data['user_group_id'] = $this->user->getGroupId();
		//$data['user_type'] = $this->user->getUserType();	
		$data['base_link_1'] = $this->url->link('catalog/inward/edit', 'token=' . $this->session->data['token']);
		$data['base_link'] = $this->url->link('catalog/inward/edit', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . '&sort=inward_name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $inward_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($inward_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($inward_total - $this->config->get('config_limit_admin'))) ? $inward_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $inward_total, ceil($inward_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_inward_category'] = $filter_inward_category;
		$data['filter_inward_category_id'] = $filter_inward_category_id;
		$data['filter_inward'] = $filter_inward;
		$data['filter_order_no'] = $filter_order_no;
		$data['filter_medicine'] = $filter_medicine;
		$data['filter_date'] = $filter_date;
		$data['filter_dates'] = $filter_dates;
		$data['filter_productsort'] = $filter_productsort;
		$data['filter_order_id'] = $filter_order_id;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];
		
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		$this->response->setOutput($this->load->view('catalog/inward_list', $data));
	}

	protected function getForm() {
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['req'])) {
			$data['error_req'] = $this->error['req'];
		} else {
			$data['error_req'] = '';
		}

		if (isset($this->error['reason'])) {
			$data['error_reason'] = $this->error['reason'];
		} else {
			$data['error_reason'] = array();
		}


		if (isset($this->error['date'])) {
			$data['error_date'] = $this->error['date'];
		} else {
			$data['error_date'] = array();
		}

		// echo '<pre>';
		// print_r($data['error_reason']);
		// exit;

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		if (isset($this->error['error_valierr_supplier_name'])) {
			$data['error_valierr_supplier_name'] = $this->error['error_valierr_supplier_name'];
		}  else {
			$data['error_valierr_supplier_name'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_medicine'])) {
			$url .= '&filter_medicine=' . urlencode(html_entity_decode($this->request->get['filter_medicine'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category'])) {
			$url .= '&filter_inward_category=' . urlencode(html_entity_decode($this->request->get['filter_inward_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_quantity'])) {
			$url .= '&filter_inward_quantity=' . urlencode(html_entity_decode($this->request->get['filter_inward_quantity'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_inward_category_id'])) {
			$url .= '&filter_inward_category_id=' . urlencode(html_entity_decode($this->request->get['filter_inward_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['previous'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'], true);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/inward/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/inward/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['order_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$inward_info = $this->model_catalog_inward->getinward($this->request->get['order_id']);
		}

		//echo "<pre>";print_r($inward_info);exit;

		if (!empty($inward_info)) {
			$data['value_total'] = $inward_info['value_total'];
		} else {
			$data['value_total'] = 0;
		}

		if (!empty($inward_info)) {
			$data['gst_value_total'] = $inward_info['gst_value_total'];
		} else {
			$data['gst_value_total'] = 0;
		}

		if (!empty($inward_info)) {
			$data['all_total'] = $inward_info['all_total'];
		} else {
			$data['all_total'] = 0;
		}

		$data['token'] = $this->session->data['token'];

		$order_nos = $this->db->query("SELECT order_no FROM oc_inward WHERE 1=1 ORDER BY order_no DESC LIMIT 1 ");
		if ($order_nos->num_rows > 0) {
			$order_no = $order_nos->row['order_no'];
		} else {
			$order_no = 0;
		}

		if (isset($this->request->post['order_no'])) {
			$data['order_no'] = $this->request->post['order_no'];
		} elseif (!empty($inward_info)) {
			$data['order_no'] = $inward_info['order_no'];
		} else {
			$data['order_no'] = $order_no + 1;
		}
		// if (isset($this->request->post['indent_no'])) {
		// 	$data['indent_no'] = $this->request->post['indent_no'];
		// } elseif (!empty($inward_info)) {
		// 	$data['indent_no'] = $inward_info['indent_no'];
		// } else {
		// 	$data['indent_no'] = '';
		// }

		// if (isset($this->request->post['pose_no'])) {
		// 	$data['pose_no'] = $this->request->post['pose_no'];
		// } elseif (!empty($inward_info)) {
		// 	$data['pose_no'] = $inward_info['pose_no'];
		// } else {
		// 	$data['pose_no'] = '';
		// }

		if (isset($this->request->post['order_id'])) {
			$data['order_id'] = $this->request->post['order_id'];
		} elseif (!empty($inward_info)) {
			$data['order_id'] = $inward_info['order_id'];
		} else {
			$data['order_id'] = '';
		}
		if (isset($this->request->post['supplier'])) {
			$data['supplier'] = $this->request->post['supplier'];
		} elseif (!empty($inward_info)) {
			$data['supplier'] = $inward_info['supplier'];
		} else {
			$data['supplier'] = '';
		}

		if (!empty($inward_info)) {
			$data['approval_status'] = $inward_info['approval_status'];
		} else {
			$data['approval_status'] = '';
		}

		if (!empty($inward_info)) {
			$data['approval'] = $this->url->link('catalog/inward/indent_approval', 'token=' . $this->session->data['token'] . '&order_id=' . $inward_info['order_id'] . $url, true);
		} else {
			$data['approval'] = '';
		}
		if (isset($this->request->post['date'])) {
			$data['date'] = $this->request->post['date'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['date'] = $rawmaterialreq_info['date'];
		} else {
			$data['date'] = date('d-m-Y');
		}

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} else {
			$data['productraw_datas'] = array();
		}

		$data['user_log_grp_id'] = $this->user->getGroupId();

		$data['user_log_id'] = $this->user->getId();

		// if (isset($this->request->get['order_id'])) {
		// 	$inward_total1 = $this->db->query("SELECT * FROM `oc_inward` WHERE `order_id` = '".$this->request->get['order_id']."' ");

		// 	// if ($inward_total1->num_rows == 0) {
		// 	// 	$data['value_total'] = $inward_total1['productraw_datas'];
		// 	// }

		// 	// echo "<pre>";print_r($inward_total1);exit;

		// }
		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} elseif (!empty($inward_info)) {
			$product_raw_datass = $this->db->query("SELECT * FROM `oc_inwarditem` WHERE `order_id` = '".$inward_info['order_id']."' ")->rows;
			//echo "<pre>";print_r($product_raw_datass);exit;
			$productraw_datas = array();

			$filter_month = date('m');
			$filter_year = date('Y');

			if($filter_month == 1){
				$prev_filter_month = 12;
				$prev_filter_year = $filter_year - 1;
			} else {
				$prev_filter_month = $filter_month - 1;
				$prev_filter_year = $filter_year;
			}

			foreach($product_raw_datass as $pkeys => $pvalues){
				$inward_po = $this->db->query("SELECT reduce_po_qty FROM `oc_inwarditem` WHERE `po_no` = '".$pvalues['po_no']."' ORDER BY `productraw_id` DESC LIMIT 1 ");
				if ($inward_po->num_rows > 0) {
					$reduce_po_qty = $inward_po->row['reduce_po_qty'];
				} else{
					$reduce_po_qty = $result['qty'];
				}
				$productraw_datas[] = array(
					'productraw_name' => $pvalues['productraw_name'],
					'med_id' => $pvalues['medicine_id'],
					'po_data' => $pvalues['po_no'],
					'po_qty' => $pvalues['po_qty'],
					'productraw_id' => $pvalues['productraw_id'],
					'productraw_code' => $pvalues['product_id'],
					'quantity' => $pvalues['quantity'],
					'purchase_price' => $pvalues['purchase_price'].' '.$pvalues['packing'],
					'value' => $pvalues['value'],
					'expiry_date' => $pvalues['ex_date'],
					'batch_no' => $pvalues['batch_no'],
					'packing' => $pvalues['packing'].' '.$pvalues['volume'].' '.$pvalues['unit'],
					'packings' => $pvalues['packing'],
					'ordered_quantity' => $pvalues['ordered_quantity'],
					'unit' => $pvalues['unit'],
					'gst_rate' => $pvalues['gst_rate'],
					'gst_value' => $pvalues['gst_value'],
					'total' => $pvalues['total'],
					'reduce_po_qty' => $reduce_po_qty,
					'volume' => $pvalues['volume'],
					
				);	
			}
			$data['productraw_datas'] = $productraw_datas;
		} else {
			$data['productraw_datas'] = array();
		}
		$data['user_group_id'] = $this->user->getGroupId();	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/inward_form', $data));
	}


	public function prints() {

		$inward = $this->db->query("SELECT * FROM oc_inward WHERE order_id = '".$this->request->get['order_id']."' ")->rows;

		foreach ($inward as $pvalue) {
			$data['inwards'][] = array(
				'inward_code'=> $pvalue['order_no'],
				'supplier'=> $pvalue['supplier'],
				'date'=> date('d-m-Y', strtotime($pvalue['date'])),
				'value_total'=> $pvalue['value_total'],
				'gst_value_total'=> $pvalue['gst_value_total'],
				'all_total'=> $pvalue['all_total'],
			);
		}


		$inward_item = $this->db->query("SELECT * FROM oc_inwarditem WHERE order_id = '".$this->request->get['order_id']."' ")->rows;

		//echo "<pre>";print_r($inward_item);exit;
		
		foreach ($inward_item as $value) {
			$inward_po = $this->db->query("SELECT reduce_po_qty FROM `oc_inwarditem` WHERE `po_no` = '".$value['po_no']."' ORDER BY `productraw_id` DESC LIMIT 1 ");
				if ($inward_po->num_rows > 0) {
					$reduce_po_qty = $inward_po->row['reduce_po_qty'];
				} else{
					$reduce_po_qty = $result['qty'];
				}
			$data['inwards_item'][] = array(
				'med_code'=> $value['product_id'],
				'med_name'=> $value['productraw_name'],
				'po_no'=> $value['po_no'],
				'po_qty'=> $value['po_qty'],
				'quantity'=> $value['quantity'],
				'value'=> $value['value'],
				'ex_date'=> $value['ex_date'],
				'batch_no'=> $value['batch_no'],
				'purchase_price' => $value['purchase_price'].' '.$value['packing'],
				'packing' => $value['volume'].' '.$value['unit'].' '.$value['packing'],
				'ordered_quantity' => $value['ordered_quantity'].' '.$value['unit'],
				'gst_rate' => $value['gst_rate'],
				'gst_value' => $value['gst_value'],
				'total' => $value['total'],
				'reduce_po_qty' => $value['reduce_po_qty'],
			);
		}

		//echo "<pre>";print_r($data);
		
		$html = $this->load->view('report/inward_report', $data);
		//echo $html;exit;
		$filename = 'Inward.html';
		//file_put_contents(DIR_DOWNLOAD.Indent, $html);
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;
	}

	public function email() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$inward = $this->db->query("SELECT * FROM oc_inward WHERE order_id = '".$this->request->get['order_id']."' ")->rows;

		foreach ($inward as $pvalue) {
			$data['inwards'][] = array(
				'inward_code'=> $pvalue['order_no'],
				'supplier'=> $pvalue['supplier'],
				'date'=> date('d-m-Y', strtotime($pvalue['date'])),
				'value_total'=> $pvalue['value_total'],
				'gst_value_total'=> $pvalue['gst_value_total'],
				'all_total'=> $pvalue['all_total'],
			);
		}


		$inward_item = $this->db->query("SELECT * FROM oc_inwarditem WHERE order_id = '".$this->request->get['order_id']."' ")->rows;

		//echo "<pre>";print_r($inward_item);exit;
		
		foreach ($inward_item as $value) {
			$inward_po = $this->db->query("SELECT reduce_po_qty FROM `oc_inwarditem` WHERE `po_no` = '".$value['po_no']."' ORDER BY `productraw_id` DESC LIMIT 1 ");
				if ($inward_po->num_rows > 0) {
					$reduce_po_qty = $inward_po->row['reduce_po_qty'];
				} else{
					$reduce_po_qty = $result['qty'];
				}
			$data['inwards_item'][] = array(
				'med_code'=> $value['product_id'],
				'med_name'=> $value['productraw_name'],
				'po_no'=> $value['po_no'],
				'po_qty'=> $value['po_qty'],
				'quantity'=> $value['quantity'],
				'value'=> $value['value'],
				'ex_date'=> $value['ex_date'],
				'batch_no'=> $value['batch_no'],
				'purchase_price' => $value['purchase_price'].' '.$value['packing'],
				'packing' => $value['packing'].' '.$value['volume'].' '.$value['unit'],
				'ordered_quantity' => $value['ordered_quantity'].' '.$value['unit'],
				'gst_rate' => $value['gst_rate'],
				'gst_value' => $value['gst_value'],
				'total' => $value['total'],
				'reduce_po_qty' => $value['reduce_po_qty'],
			);
		}

		//echo "<pre>";print_r($data);
		
		$html = $this->load->view('report/inward_report', $data);
		//echo $html;exit;
		$filename = 'Inward_'.$pvalue['order_no'].'.html';
		//file_put_contents(DIR_DOWNLOAD.Indent, $html);
		//header('Content-disposition: attachment; filename=' . $filename);
		//header('Content-type: text/html');
		//echo $html;exit;

		$inward = DIR_DOWNLOAD.'inward/inward_'.$pvalue['order_no'].'.html';
	    file_put_contents($inward, $html);
	    $filename = 'inward_'.$pvalue['order_no'].'.html';
	    $file_path = DIR_DOWNLOAD.'inward/'.$filename;
	    //echo $file_path;exit;

		require_once(DIR_SYSTEM . 'library/PHPMailer/PHPMailerAutoload.php');
		require_once(DIR_SYSTEM . 'library/PHPMailer/class.phpmailer.php');
		require_once(DIR_SYSTEM . 'library/PHPMailer/class.smtp.php');

		$mail = new PHPMailer();

		$mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		    )
		);
		//$mail->SMTPDebug = 3;
		//$mail->isMail(); // Set mailer to use SMTP
		//$mail->Host = 'localhost'; 
		$message = "Dear Sir,\nHere Attached Is The inward,\nThank You,";

		$subject = 'inward Report';
		//$from_email = $this->request->post['from_branch_email'];
		$from_email = 'eric.fargose@gmail.com';
		//$to_emails = $this->request->post['to_requester_email'];
		$to_emails = 'eric.fargose@gmail.com';
		if($to_emails != ''){
			$to_emails_array = explode(',', $to_emails);
		}

		$filename = 'inward_'.$pvalue['order_no'].'.html';
		$file_path =DIR_DOWNLOAD.'inward/'.$filename;

		//$mail->SMTPDebug = 3;
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Helo = "HELO";
		$mail->Host = 'smtp.gmail.com';//$this->config->get('config_smtp_host');
		$mail->Port = '587';//$this->config->get('config_smtp_port');
		$mail->Username = 'stores.rwitc';//$this->config->get('config_smtp_username');
		$mail->Password = 'Store#rwitc90';//$this->config->get('config_smtp_password');
		$mail->SMTPSecure = 'tls';
		$mail->SetFrom($from_email, 'RWITC');
		$mail->Subject = $subject;
		$mail->Body = html_entity_decode($message);
		$mail->addAttachment($file_path, $filename);
		foreach($to_emails_array as $ekey => $evalue){
			$mail->AddAddress($evalue);
		}

		if($mail->Send()) {
			$this->session->data['success'] = 'Mail Sent';
		  	// echo 'Mail Sent';
		  	// echo '<br />';
		  	// echo '<pre>';
		  	// print_r($mail->ErrorInfo);
		  	$this->db->query("UPDATE oc_inward SET email_send = 1 WHERE order_id = '".$this->request->get['order_id']."' ");
		} else {
			$this->session->data['success'] = 'Mail Not Sent';
		  	//echo "Mailer Error: " . $mail->ErrorInfo;
			// echo '<pre>';
			// print_r($mail->ErrorInfo);
			// exit;
		}
		$this->response->redirect($this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function calculate() {
		$this->load->language('catalog/inward');

		$this->document->setTitle('Product Costing');

		$this->load->model('catalog/inward');
		$this->load->model('catalog/product');
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Product Costing',
			'href' => $this->url->link('catalog/inward/calculate', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['cancel'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true);
		$data['action'] = $this->url->link('catalog/inward/make', 'token=' . $this->session->data['token'] . $url.'&finished_product_id='.$this->request->get['order_id'], true);

		if (isset($this->request->get['order_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$inward_info = $this->model_catalog_inward->getinward($this->request->get['order_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['inward_name'])) {
			$data['inward_name'] = $this->request->post['inward_name'];
		} elseif (!empty($inward_info)) {
			$data['inward_name'] = $inward_info['inward_name'];
		} else {
			$data['inward_name'] = '';
		}

		if (isset($this->request->post['cost_price'])) {
			$data['cost_price'] = $this->request->post['cost_price'];
		} elseif (!empty($inward_info)) {
			$data['cost_price'] = $inward_info['cost_price'];
		} else {
			$data['cost_price'] = '';
		}

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} elseif (!empty($inward_info)) {
			$product_raw_datass = $this->db->query("SELECT * FROM `oc_inwarditem` WHERE `order_id` = '".$inward_info['order_id']."' ")->rows;
			$productraw_datas = array();
			foreach($product_raw_datass as $pkeys => $pvalues){
				$product_datas = $this->db->query("SELECT `quantity` FROM `is_productnew` WHERE `productnew_id` = '".$pvalues['product_id']."' ")->row;
				if(isset($product_datas['quantity'])){
					$quantity_in_stock = $product_datas['quantity'];
				} else {
					$quantity_in_stock = 0;
				}
				$productraw_datas[] = array(
					'productraw_name' => $pvalues['productraw_name'],
					'productraw_id' => $pvalues['productraw_id'],
					'product_id' => $pvalues['product_id'],
					'order_no' => $pvalues['order_no'],
					'price' => $pvalues['price'],
					'quantity' => $pvalues['quantity'],
					'gst_type' => $pvalues['gst_type'],
					/*'outside_color' => $pvalues['outside_color'],
					'inside_color' => $pvalues['inside_color'],
					'raw_material' => $pvalues['raw_material'],
					'productnew_id' => $pvalues['productnew_id'],*/
					'gst_type_id' => $pvalues['gst_type_id'],
					/*'quantity_in_stock' => $quantity_in_stock,*/
					'total' => $pvalues['total'],
				); 			
			}
			$data['productraw_datas'] = $productraw_datas;
		} else {
			$data['productraw_datas'] = array();
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/inward_calculate', $data));
	}

	public function make() {
		$this->load->model('catalog/inward');
		$this->model_catalog_inward->makeProduct($this->request->get['finished_product_id'], $this->request->post);
		$this->response->redirect($this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true));
	}

	protected function validateForm() {
		if($this->request->post['supplier'] == ''){ 
			$this->error['error_valierr_supplier_name'] = 'Please Enter Supplier Name';
		}
		/*if (!$this->user->hasPermission('modify', 'catalog/inward')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}*/
		/*echo '<pre>';
		print_r($this->request->post);
		exit;*/
		// if ((utf8_strlen($this->request->post['order_no']) < 2) || (utf8_strlen($this->request->post['order_no']) > 64)) {
		// 	$this->error['name'] = $this->language->get('error_name');
		// }
		// if ((utf8_strlen($this->request->post['order_no_hidden']) =='')) {
		// 	$this->error['name'] = $this->language->get('error_name');
		// }
		$datas = $this->request->post;
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/inward')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
	public function autocomplete() {
		$json = array();
		 /*echo '<pre>';
		 print_r($this->request->get);
		 exit;*/
		if (isset($this->request->get['filter_order_id'])) {
			$this->load->model('catalog/inward');

			$filter_data = array(
				'filter_order_id' => $this->request->get['filter_order_id'],
				//'start'       => 0,
				//'limit'       => 5
			);
			$results = $this->model_catalog_inward->getinwards($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'order_id' => $result['order_id'],
					'order_no'     => strip_tags(html_entity_decode($result['order_no'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['order_no'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletesupplier() {
		$json = array();
		 // echo '<pre>';
		 // print_r($this->request->get);
		 // exit;
		if (isset($this->request->get['supplier'])) {
			$this->load->model('catalog/inward');

			$filter_data = array(
				'filter_supplier' => $this->request->get['supplier'],
				//'start'       => 0,
				//'limit'       => 5
			);
			$results = $this->model_catalog_inward->getsupplier($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'vendor_id' => $result['vendor_id'],
					'vendor_name'     => strip_tags(html_entity_decode($result['vendor_name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['vendor_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete_raw() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/inward');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_inward->getProductnews($filter_data);

			foreach ($results as $result) {
				$med_datas = $this->db->query("SELECT * FROM oc_inward i LEFT JOIN oc_inwarditem ii ON(i.order_no = ii.order_no) WHERE product_id = '".$result['med_code']."' ORDER BY productraw_id DESC LIMIT 1 ");
				if ($med_datas->num_rows > 0) {
					$prev_rate = $med_datas->row['purchase_price'];
				} else {
					$prev_rate = 0;
				}
				$quantity = 1;
				$json[] = array(
					'id' => $result['id'],
					'med_code' => strip_tags(html_entity_decode($result['med_code'], ENT_QUOTES, 'UTF-8')),
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8')),
					'price'     => '',
					'prev_rate' => strip_tags(html_entity_decode($prev_rate, ENT_QUOTES, 'UTF-8')),
					'quantity' => '',
					'store_unit' => strip_tags(html_entity_decode($result['store_unit'], ENT_QUOTES, 'UTF-8')),
					'unit'     => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8')),
					'packing'     => strip_tags(html_entity_decode($result['pack_type'], ENT_QUOTES, 'UTF-8')),
					'volume'     => strip_tags(html_entity_decode($result['volume'], ENT_QUOTES, 'UTF-8')),
					'gst_rate'     => strip_tags(html_entity_decode($result['gst_rate'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['med_name'];
			
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		// echo'<pre>';
		// print_r($json);
		// exit;
	}

	public function autocomplete_raw_code() {
		$json = array();

		if (isset($this->request->get['med_code'])) {
			$this->load->model('catalog/inward');

			$filter_data = array(
				'med_code' => $this->request->get['med_code'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_inward->getProductcode($filter_data);

			foreach ($results as $result) {
				$med_datas = $this->db->query("SELECT * FROM oc_inward i LEFT JOIN oc_inwarditem ii ON(i.order_no = ii.order_no) WHERE product_id = '".$result['med_code']."' ORDER BY productraw_id DESC LIMIT 1 ");
				if ($med_datas->num_rows > 0) {
					$prev_rate = $med_datas->row['purchase_price'];
				} else {
					$prev_rate = 0;
				}
				$quantity = 1;
				$json = array(
					'id' => $result['id'],
					'med_code' => strip_tags(html_entity_decode($result['med_code'], ENT_QUOTES, 'UTF-8')),
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8')),
					'quantity' => '',
					'price'     => '',
					'prev_rate' => strip_tags(html_entity_decode($prev_rate, ENT_QUOTES, 'UTF-8')),
					'store_unit' => strip_tags(html_entity_decode($result['store_unit'], ENT_QUOTES, 'UTF-8')),
					'unit'     => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8')),
					'packing'     => strip_tags(html_entity_decode($result['pack_type'], ENT_QUOTES, 'UTF-8')),
					'volume'     => strip_tags(html_entity_decode($result['volume'], ENT_QUOTES, 'UTF-8')),
					'gst_rate'     => strip_tags(html_entity_decode($result['gst_rate'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['med_name'];
			
		// }

		// array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getdata() {
		$json = array();
		$html = '';
		if (isset($this->request->get['filter_name_id'])) {
			$this->load->model('catalog/inward');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name_id'],
			);
			$filter_name_id = $this->request->get['filter_name_id'];

			$product_finished = $this->db->query("SELECT * FROM `oc_inward` WHERE `order_id` = '".$filter_name_id."'")->row;
			
			$product_finished_raw = $this->db->query("SELECT * FROM `oc_inwarditem` WHERE `order_id` = '".$filter_name_id."'")->rows;

			$html = '<table style="margin-top: 15px;" class="table table-hover">';
				$html .= '<tbody style="border-top:0px !important;">';
					$html .= '<tr>';	
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Name</b> : ';
							$html .= $product_finished['inward_name'];
						$html .= '</td>';
						
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Description</b> : ';
							$html .= $product_finished['inward_description'];
						$html .= '</td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Cost Price</b> : ';
							$html .= $product_finished['cost_price'];
						$html .= '</td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Selling Price</b> : ';
							$html .= $product_finished['selling_price'];
						$html .= '</td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>GST</b> : ';
							$html .= $product_finished['gst_type'];
						$html .= '</td>';
					$html .= '</tr>';

					/*$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Outside_color</b> : ';
							$html .= $product_finished['outside_color'];
						$html .= '</td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Inside_color</b> : ';
							$html .= $product_finished['inside_color'];
						$html .= '</td>';
					$html .= '</tr>';

					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Raw_Material</b> : ';
							$html .= $product_finished['raw_material'];
						$html .= '</td>';
					$html .= '</tr>';*/
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Assigned Category</b> : ';
							$html .= $product_finished['inward_category'];
						$html .= '</td>';
					$html .= '</tr>';
				$html .= '</tbody>';
			$html .= '</table>';
			$html .= '<table style="margin-top: 15px;" class="table table-hover">';
				$html .= '<tbody style="border-top:0px !important;">';
					$html .= '<tr style="border-top: 1px solid black;">';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Raw Materials</b>';
						$html .= '</td>';	
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Materials</b>';
						$html .= '</td>';	
						$html .= '<td style="border-top: 0px !important;">';
							$html .= '<b>Quantity</b>';
						$html .= '</td>';	
					$html .= '</tr>';
					if($product_finished_raw) {
						foreach ($product_finished_raw as $result) {
							$html .= '<tr>';	
								$html .= '<td style="border-top: 0px !important;">';
									$html .= $result['productraw_name'];
								$html .= '</td>';

								/*$html .= '<tr>';	
								$html .= '<td style="border-top: 0px !important;">';
									$html .= $result['order_no'];
								$html .= '</td>';*/
								$html .= '<td style="border-top: 0px !important;">';
									$html .= $result['quantity'];
								$html .= '</td>';
							$html .= '</tr>';
						}
					}
				$html .= '</tbody>';
			$html .= '</table>';
		}
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function PoDatas() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$json = array();

		// $quantity = $this->db->query("SELECT SUM(quantity) AS quantitys FROM `oc_inwarditem` WHERE `productraw_name` = '".$this->request->get['med_name']."' ")->row;


		// $inword_qty = $quantity['quantitys'];
		//echo "<pre>";print_r($inword_qty);exit;

		$results = $this->db->query("SELECT * FROM `oc_tally_po` WHERE `name_of_the_items` = '".$this->request->get['med_name']."' AND (`status` = 0 OR `status` = 1) ")->rows;
		
		foreach($results as $result){
			$json[] = array(
				'po_no' => $result['po_no'],
				'qty' => $result['qty']
			);//echo "<pre>";print_r($json);	
		}//exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function PoQty() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$json = array();
		$results = $this->db->query("SELECT * FROM `oc_tally_po` WHERE `po_no` = '".$this->request->get['po']."' ")->rows;
		// echo'<pre>';
		// print_r("SELECT * FROM `oc_tally_po` WHERE `po_no` = '".$this->request->get['po']."' ");
		// exit;
		foreach($results as $result){
			$inward_po = $this->db->query("SELECT reduce_po_qty FROM `oc_inwarditem` WHERE `po_no` = '".$result['po_no']."' ORDER BY `productraw_id` DESC LIMIT 1 ");
			if ($inward_po->num_rows > 0) {
				$reduce_po_qty = $inward_po->row['reduce_po_qty'];
			} else{
				$reduce_po_qty = $result['qty'];
			}

			$json = array(
				'reduce_po_qty' => $reduce_po_qty,
				'qty' => $result['qty']
			);//echo "<pre>";print_r($json);	
		}//exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	function indent_approval() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$user_id = $this->session->data['user_id'];
		// echo'<pre>';
		// print_r($user_id);
		// exit;
		$names = $this->db->query("SELECT * FROM oc_user WHERE user_id = '".$user_id."' ");
		if ($names->num_rows > 0) {
			$name = $names->row['firstname'];
		} else {
			$name = 'Admin';
		}

		$this->db->query("UPDATE oc_inward SET `approval_status` = 1, approved_by = '".$name."'  WHERE order_id = '".$this->request->get['order_id']."' ");
		
		$this->response->redirect($this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function cancel() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$this->db->query("UPDATE oc_inward SET status = 1 WHERE order_id = '".$this->request->get['order_id']."' ");
		//header("Location: http://localhost/rwitc_erp/admin/index.php?route=catalog/rawmaterialreq&token=".$this->request->get['token']."");
		$this->response->redirect($this->url->link('catalog/inward', 'token=' . $this->session->data['token'] ));
	}

	function inward_dis_approval() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$user_id = $this->session->data['user_id'];
		// echo'<pre>';
		// print_r($user_id);
		// exit;
		$names = $this->db->query("SELECT * FROM oc_user WHERE user_id = '".$user_id."' ");
		if ($names->num_rows > 0) {
			$name = $names->row['firstname'];
		} else {
			$name = 'Admin';
		}

		$this->db->query("UPDATE oc_inward SET `approval_status` = 2, approved_by = '".$name."', reason = '".$this->request->get['reason']."'  WHERE order_id = '".$this->request->get['order_id']."' ");
		
		$this->response->redirect($this->url->link('catalog/inward', 'token=' . $this->session->data['token'] . $url, true));
	}
}
