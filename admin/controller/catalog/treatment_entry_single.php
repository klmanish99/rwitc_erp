<?php
/*require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;*/
date_default_timezone_set("Asia/Kolkata");
class Controllercatalogtreatmententrysingle extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/treatment_entry_single');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/treatment_entry_single');
		$this->load->model('catalog/product');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/treatment_entry_single');
		/*$this->load->model('catalog/product');*/

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/treatment_entry_single');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
			$this->model_catalog_treatment_entry_single->addTreatmentEntry($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/treatment_entry_single');
		$this->load->model('catalog/product');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/treatment_entry_single');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_treatment_entry_single->editinward($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

		public function edit1() {
		$this->load->language('catalog/treatment_entry_single');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/treatment_entry_single');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') /*&& $this->validateForm()*/) {
			$this->model_catalog_treatment_entry_single->editinward1($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/treatment_entry_single/getForm', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getFormView();
	}

	public function getFormView() {
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		$url = '';

		if (isset($this->request->get['id'])) {
			$url .= '&id=' . urlencode(html_entity_decode($this->request->get['id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/treatment_entry_single/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/treatment_entry_single/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['id'] . $url, true);
		}

		$data['approve'] = $this->url->link('catalog/treatment_entry_single/approve_onform', 'token=' . $this->session->data['token'] . '&order_id=' .  $this->request->get['id'] . $url, true);
		$data['text_no_results'] = $this->language->get('text_no_results');


		$data['cancel'] = $this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true);
		$this->load->model('catalog/treatment_entry_single');
		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$transection_info_parent = $this->model_catalog_treatment_entry_single->getTransectionEntryParent($this->request->get['id']);
		}

		$data['token'] = $this->session->data['token'];

		$issue_nos = $this->db->query("SELECT issue_no FROM oc_treatment_entry WHERE 1=1 ORDER BY issue_no DESC LIMIT 1 ");

		if ($issue_nos->num_rows > 0) {
			$data['input_isse_number'] = $issue_nos->row['issue_no'] + 1;
		} else {
			$data['input_isse_number'] = 1;
		}

		if (isset($this->request->post['filter_parent_doctor'])) {
			$data['filter_parent_doctor'] = $this->request->post['filter_parent_doctor'];
		} elseif (!empty($transection_info_parent)) {
			$data['filter_parent_doctor'] = $transection_info_parent['clinic_name'];
		} else {
			$data['filter_parent_doctor'] = '';
		}

		if (isset($this->request->post['filterParentId'])) {
			$data['filter_parent_doctor_id'] = $this->request->post['filterParentId'];
		} elseif (!empty($transection_info_parent)) {
			$data['filter_parent_doctor_id'] = $transection_info_parent['clinic_id'];
		} else {
			$data['filter_parent_doctor_id'] = '';
		}

		if (isset($this->request->post['horse_name'])) {
			$data['horse_name'] = $this->request->post['horse_name'];
		} elseif (!empty($transection_info_parent)) {
			$data['horse_name'] = $transection_info_parent['horse_name'];
		} else {
			$data['horse_name'] = '';
		}

		if (isset($this->request->post['hidden_horse_name_id'])) {
			$data['hidden_horse_name_id'] = $this->request->post['hidden_horse_name_id'];
		} elseif (!empty($transection_info_parent)) {
			$data['hidden_horse_name_id'] = $transection_info_parent['horse_name_id'];
		} else {
			$data['hidden_horse_name_id'] = '';
		}

		if (isset($this->request->post['trainer_name'])) {
			$data['trainer_name'] = $this->request->post['trainer_name'];
		} elseif (!empty($transection_info_parent)) {
			$data['trainer_name'] = $transection_info_parent['trainer_name'];
		} else {
			$data['trainer_name'] = '';
		}

		if (isset($this->request->post['hidden_trainer_name_id'])) {
			$data['trainer_name_id'] = $this->request->post['hidden_trainer_name_id'];
		} elseif (!empty($transection_info_parent)) {
			$data['trainer_name_id'] = $transection_info_parent['trainer_name_id'];
		} else {
			$data['trainer_name_id'] = '';
		}

		if (isset($this->request->post['date'])) {
			$data['date'] = $this->request->post['date'];
		} elseif (!empty($transection_info_parent)) {
			$data['date'] = $transection_info_parent['entry_date'];
		} else {
			$data['date'] = date('d-m-Y');
		}
		
		if (isset($this->request->post['total_item'])) {
			$data['total_item'] = $this->request->post['total_item'];
		} elseif (!empty($transection_info_parent)) {
			$data['total_item'] = $transection_info_parent['total_item'];
		} else {
			$data['total_item'] =0;
		}

		if (isset($this->request->post['total_qty'])) {
			$data['total_qty'] = $this->request->post['total_qty'];
		} elseif (!empty($transection_info_parent)) {
			$data['total_qty'] = $transection_info_parent['total_qty'];
		} else {
			$data['total_qty'] =0;
		}

		if (isset($this->request->post['total_amt'])) {
			$data['total_amt'] = $this->request->post['total_amt'];
		} elseif (!empty($transection_info_parent)) {
			$data['total_amt'] = $transection_info_parent['total_amt'];
		} else {
			$data['total_amt'] =0;
		}

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} elseif (!empty($transection_info_parent)) {
			$product_raw_datass = $this->db->query("SELECT * FROM `oc_treatment_entry_trans` WHERE `parent_id` = '".$transection_info_parent['id']."' ")->rows;
			$productraw_datas = array();
			foreach($product_raw_datass as $pkeys => $pvalues){
				$syrin_info1 = ($pvalues['syringe_one'] != '') ? $pvalues['syringe_one'].' Syringe with 2 Needle' : '';
				$syrin_info2 = ($pvalues['syringe_two'] != '') ? $pvalues['syringe_two'].' Syringe with 2 Needle' : '';
				$syrin_info3 = ($pvalues['syringe_three'] != '') ? $pvalues['syringe_three'].' Syringe with 2 Needle' : '';
				$syrin_info4 = ($pvalues['syringe_four'] != '') ? $pvalues['syringe_four'].' Syringe with 2 Needle' : '';
				$syrin_info5 = ($pvalues['syringe_five'] != '') ? $pvalues['syringe_five'].' Syringe with 2 Needle' : '';
				$syrin_info6 = ($pvalues['syringe_six'] != '') ? $pvalues['syringe_six'].' Syringe with 2 Needle' : '';
				$syrin_info7 = ($pvalues['syringe_seven'] != '') ? $pvalues['syringe_seven'].' Syringe with 2 Needle' : '';
				
				$syrin_data = '';
				if($syrin_info1 != ''){
					$syrin_data .= $syrin_info1.'<br />';
				}
				if($syrin_info2 != ''){
					$syrin_data .= $syrin_info2.'<br />';
				}
				if($syrin_info3 != ''){
					$syrin_data .= $syrin_info3.'<br />';
				}
				if($syrin_info4 != ''){
					$syrin_data .= $syrin_info4.'<br />';
				}
				if($syrin_info5 != ''){
					$syrin_data .= $syrin_info5.'<br />';
				}
				if($syrin_info6 != ''){
					$syrin_data .= $syrin_info6.'<br />';
				}
				if($syrin_info7 != ''){
					$syrin_data .= $syrin_info7.'<br />';
				}



				$productraw_datas[] = array(
					'medicine_code' => $pvalues['medicine_code'],
					'medicine_name' => $pvalues['medicine_name'],
					'unit' => $pvalues['unit'],
					'medicine_qty' => $pvalues['medicine_qty'],
					'rate' => $pvalues['rate'],
					'value' => $pvalues['value'],
					'syringe_info' => $syrin_data,
				); 			
			}
			$data['productraw_datas'] = $productraw_datas;
		} else {
			$data['productraw_datas'] = array();
		}

		// echo'<pre>';
		// print_r($data['productraw_datas']);
		// exit;
		if (isset($this->request->post['to_doc']) && $this->request->post['to_doc'] != '') {
			$data['to_doc'] = $this->request->post['to_doc'];
			$data['child_doctor_name'] = $this->db->query("SELECT * FROM doctor WHERE id = '".$this->request->post['to_doc']."'	")->row['doctor_name'];
			$results =$this->db->query("SELECT * FROM  doctor WHERE isActive = 'Active' AND is_parent = 0 AND `parent_id` = '" . ($this->request->post['filterParentId']) . "' ")->rows;
			
			$json = array();
			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}

			$data['all_docs'] = $json;
		} elseif (!empty($transection_info_parent)) {
			$data['to_doc'] = $transection_info_parent['doctor_id'];
			$data['child_doctor_name'] = $this->db->query("SELECT * FROM doctor WHERE id = '".$transection_info_parent['doctor_id']."'	")->row['doctor_name'];
			$results =$this->db->query("SELECT * FROM  doctor WHERE isActive = 'Active' AND is_parent = 0 AND `parent_id` = '" . ($transection_info_parent['clinic_id']) . "' ")->rows;
			
			$json = array();
			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}

			$data['all_docs'] = $json;

		} else {
			$data['to_doc'] = '';
			$data['all_docs'] = array();
		}

		//$data['user_type'] = $this->user->getUserType();	
		$data['user_group_id'] = $this->user->getGroupId();	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/treatment_entry_single_view', $data));
	}

	public function stock() {
		$this->load->language('catalog/treatment_entry_single');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/treatment_entry_single');


		$datas = $this->model_catalog_treatment_entry_single->daterawmaterial();
		foreach ($datas as $key => $value) {
			$data['datas'][] = array(
				'date' => $value['date']
			);

		$is_date = $this->db->query("SELECT * FROM `oc_inwarditem` WHERE `order_id` = '".$value['order_id']."' AND date = '".$value['date']."' ");
			if ($is_date->num_rows == 0){
					$this->db->query("INSERT INTO oc_inwarditem SET `order_id` = '".$value['order_id']."',date = '".$value['date']."' ");
				}
				
		}
		// echo '<pre>';print_r($is_date);exit;

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true));
		

		$this->getList();
	}

	public function delete() {
		$this->load->language('catalog/treatment_entry_single');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/treatment_entry_single');

		if (isset($this->request->post['selected']) /*&& $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $order_id) {
				$this->model_catalog_treatment_entry_single->deleteinward($order_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}
	public function approve() {
		$order_id1=$_GET['order_id'];
		$user_group_id=$this->db->query("UPDATE oc_inward SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$user_group=$this->db->query("UPDATE oc_inwarditem SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$this->response->redirect($this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function approve_onform() {
		$order_id1=$_GET['order_id'];
		$user_group_id=$this->db->query("UPDATE oc_inward SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$user_group=$this->db->query("UPDATE oc_inwarditem SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$this->response->redirect($this->url->link('catalog/treatment_entry_single/edit1', 'token=' . $this->session->data['token'] . '&order_id=' .$order_id1. $url, true));
	}

	public function getList() {
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		//echo "<pre>";print_r($this->request->get);exit;

		if (isset($this->request->get['filter_clinic_id'])) {
			$filter_clinic_id = $this->request->get['filter_clinic_id'];
		} else {
			$filter_clinic_id = null;
		}

		if (isset($this->request->get['filter_clinic_name'])) {
			$filter_clinic_name = $this->request->get['filter_clinic_name'];
		} else {
			$filter_clinic_name = null;
		}

		if (isset($this->request->get['filter_doctor'])) {
			$filter_doctor = $this->request->get['filter_doctor'];
		} else {
			$filter_doctor = null;
		}

		if (isset($this->request->get['filter_doctor_id'])) {
			$filter_doctor_id = $this->request->get['filter_doctor_id'];
		} else {
			$filter_doctor_id = null;
		}

		if (isset($this->request->get['filter_horse_id'])) {
			$filter_horse_id = $this->request->get['filter_horse_id'];
		} else {
			$filter_horse_id = null;
		}

		if (isset($this->request->get['filterHorseName'])) {
			$filterHorseName = $this->request->get['filterHorseName'];
		} else {
			$filterHorseName = null;
		}

		if (isset($this->request->get['filterTrainerName'])) {
			$filterTrainerName = $this->request->get['filterTrainerName'];
		} else {
			$filterTrainerName = null;
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$filter_trainer_id = $this->request->get['filter_trainer_id'];
		} else {
			$filter_trainer_id = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['filter_dates'])) {
			$filter_dates = $this->request->get['filter_dates'];
		} else {
			$filter_dates = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		
		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}
		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . $this->request->get['filter_dates'];
		}

		if (isset($this->request->get['filter_clinic_id'])) {
			$url .= '&filter_clinic_id=' . urlencode(html_entity_decode($this->request->get['filter_clinic_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_clinic_name'])) {
			$url .= '&filter_clinic_name=' . urlencode(html_entity_decode($this->request->get['filter_clinic_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_doctor_id'])) {
			$url .= '&filter_doctor_id=' . urlencode(html_entity_decode($this->request->get['filter_doctor_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_doctor'])) {
			$url .= '&filter_doctor=' . urlencode(html_entity_decode($this->request->get['filter_doctor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filterHorseName'])) {
			$url .= '&filterHorseName=' . urlencode(html_entity_decode($this->request->get['filterHorseName'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filterTrainerName'])) {
			$url .= '&filterTrainerName=' . urlencode(html_entity_decode($this->request->get['filterTrainerName'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_horse_id'])) {
			$url .= '&filter_horse_id=' . urlencode(html_entity_decode($this->request->get['filter_horse_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$url .= '&filter_trainer_id=' . urlencode(html_entity_decode($this->request->get['filter_trainer_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . $url, true);
		$data['add'] = $this->url->link('catalog/treatment_entry_single/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['stock'] = $this->url->link('catalog/treatment_entry_single/stock', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/treatment_entry_single/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['inwards'] = array();

		$filter_data = array(
			'filter_clinic_id'	  => $filter_clinic_id,
			'filter_clinic_name'	  => $filter_clinic_name,
			'filter_doctor'	  => $filter_doctor,
			'filter_doctor_id'	  => $filter_doctor_id,
			'filter_horse_id'	  => $filter_horse_id,
			'filterHorseName'  => $filterHorseName,
			'filterTrainerName'	  => $filterTrainerName,
			'filter_trainer_id'  => $filter_trainer_id,
			'filter_date'	=> $filter_date,
			'filter_dates'	=> $filter_dates,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
			/*echo '<pre>';
			 print_r($filter_data);*/
			//exit;
		$results = array();
		//$inward_total = $this->model_catalog_treatment_entry_single->getTotalinwards($filter_data);
		
		//$results = $this->model_catalog_treatment_entry_single->getinwards($filter_data);
		$user_group_id = $this->user->getGroupId();
		$inward_total = 0;
		$user_group_id = $this->user->getGroupId();
		$medi_datas = array();
		$sql = "SELECT * FROM oc_treatment_entry WHERE 1=1";

		if (isset($filter_data['filter_date']) && !is_null($filter_data['filter_date']) && isset($filter_data['filter_dates']) && !is_null($filter_data['filter_dates'])) {
			$sql .= " AND entry_date >= '".date('Y-m-d', strtotime($filter_data['filter_date']))."' ";
			$sql .= " AND entry_date <= '".date('Y-m-d', strtotime($filter_data['filter_dates']))."' ";
		}

		if (isset($filter_data['filter_clinic_id']) && !is_null($filter_data['filter_clinic_id'])) {
			$sql .= " AND clinic_id = '".$filter_data['filter_clinic_id']."' ";
		}

		if (isset($filter_data['filter_doctor_id']) && !is_null($filter_data['filter_doctor_id'])) {
			$sql .= " AND doctor_id = '".$filter_data['filter_doctor_id']."' ";
		}

		if (isset($filter_data['filter_horse_id']) && !is_null($filter_data['filter_horse_id'])) {
			$sql .= " AND horse_name_id = '".$filter_data['filter_horse_id']."' ";
		}

		if (isset($filter_data['filter_trainer_id']) && !is_null($filter_data['filter_trainer_id'])) {
			$sql .= " AND trainer_name_id = '".$filter_data['filter_trainer_id']."' ";
		}
		$sql .= " ORDER BY horse_name ASC";

		// echo'<pre>';
		// print_r( $filter_data);exit;

		if (isset($filter_data['start']) || isset($filter_data['limit'])) {
			if ($filter_data['start'] < 0) {
				$filter_data['start'] = 0;
			}

			if ($filter_data['limit'] < 1) {
				$filter_data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
		}
		/*echo $sql ;
		exit;*/
		$medicine_trans_datas = $this->db->query($sql)->rows;
		foreach ($medicine_trans_datas as $key => $value) {

			$logs_info = $this->db->query("SELECT * FROM `treate_logs` WHERE `treate_id` = '".$value['id']."' ORDER BY `id` DESC LIMIT 1 ");
			// echo'<pre>';
			// print_r("SELECT * FROM `supplier_logs` WHERE `supp_id` = '".$result['vendor_id']."' ");
			if ($logs_info->num_rows > 0) {
				$fname = $logs_info->row['fname'];
				$lname = $logs_info->row['lname'];
				$log_date = date('d-m-Y', strtotime($logs_info->row['log_date']));
				$log_time = $logs_info->row['log_time'];
				$log_datas = '<span>'.'Name: '.$fname.' '.$lname.'</span>'.'<br>'.'<span>'.'Date: '.$log_date.'</span>'.'<br>'.'<span>'.'Time: '.$log_time.'</span>';
			} else {
				$log_datas = '';
			}

			$doc_datas = $this->db->query("SELECT * FROM doctor WHERE id = '".$value['doctor_id']."'");
			if ($doc_datas->num_rows > 0) {
				$doc_data = $doc_datas->row['doctor_name'];
			} else {
				$doc_data = '';
			}

			$medi_datas[] = array(
				'id' => $value['id'],
				'issue_no' => $value['issue_no'],
				'entry_date' => $value['entry_date'],
				'clinic_name' => $value['clinic_name'],
				'horse_name' => $value['horse_name'],
				'trainer_name' => $value['trainer_name'],
				'doctor_name' => $doc_data,
				'total_item' => $value['total_item'],
				'total_qty' => $value['total_qty'],
				'total_amt' => $value['total_amt'],
				'log_datas'     => $log_datas,
				'edit'     => $this->url->link('catalog/treatment_entry_single/getFormView', 'token=' . $this->session->data['token'] . '&id=' . $value['id'], true),
			);
		}
		$data['medicine_trans_datas'] = $medi_datas;

		$inward_total = count($medi_datas);

		$data['user_group_id'] = $this->user->getGroupId();
		//$data['user_type'] = $this->user->getUserType();	
		$data['base_link_1'] = $this->url->link('catalog/treatment_entry_single/edit', 'token=' . $this->session->data['token']);
		$data['base_link'] = $this->url->link('catalog/treatment_entry_single/edit', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_dates'])) {
			$url .= '&filter_dates=' . urlencode(html_entity_decode($this->request->get['filter_dates'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_clinic_id'])) {
			$url .= '&filter_clinic_id=' . urlencode(html_entity_decode($this->request->get['filter_clinic_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_clinic_name'])) {
			$url .= '&filter_clinic_name=' . urlencode(html_entity_decode($this->request->get['filter_clinic_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_doctor_id'])) {
			$url .= '&filter_doctor_id=' . urlencode(html_entity_decode($this->request->get['filter_doctor_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_doctor'])) {
			$url .= '&filter_doctor=' . urlencode(html_entity_decode($this->request->get['filter_doctor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filterHorseName'])) {
			$url .= '&filterHorseName=' . urlencode(html_entity_decode($this->request->get['filterHorseName'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filterTrainerName'])) {
			$url .= '&filterTrainerName=' . urlencode(html_entity_decode($this->request->get['filterTrainerName'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_horse_id'])) {
			$url .= '&filter_horse_id=' . urlencode(html_entity_decode($this->request->get['filter_horse_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$url .= '&filter_trainer_id=' . urlencode(html_entity_decode($this->request->get['filter_trainer_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		

		$data['sort_name'] = $this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . '&sort=inward_name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);


		$pagination = new Pagination();
		$pagination->total = $inward_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($inward_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($inward_total - $this->config->get('config_limit_admin'))) ? $inward_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $inward_total, ceil($inward_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_date'] = $filter_date;
		$data['filter_dates'] = $filter_dates;
		$data['filter_clinic_id'] = $filter_clinic_id;
		$data['filter_clinic_name'] = $filter_clinic_name;
		$data['filter_doctor_id'] = $filter_doctor_id;
		$data['filter_doctor'] = $filter_doctor;
		$data['filterHorseName'] = $filterHorseName;
		$data['filterTrainerName'] = $filterTrainerName;
		$data['filter_horse_id'] = $filter_horse_id;
		$data['filter_trainer_id'] = $filter_trainer_id;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];
		
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		$this->response->setOutput($this->load->view('catalog/treatment_entry_single_list', $data));
	}

	protected function getForm() {
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['m_treatment_entry'] = $this->url->link('catalog/treatment_entry/add', 'token=' . $this->session->data['token'] , true);

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['medicine_error'])) {
			$data['error_medicine_error'] = $this->error['medicine_error'];
		} else {
			$data['error_medicine_error'] = '';
		}

		if (isset($this->error['day_close_error'])) {
			$data['day_close_error'] = $this->error['day_close_error'];
		} else {
			$data['day_close_error'] = '';
		}


		if (isset($this->error['valierr_trainer'])) {
			$data['valierr_trainer'] = $this->error['valierr_trainer'];
		} else {
			$data['valierr_trainer'] = '';
		}

		if (isset($this->error['valierr_horse'])) {
			$data['valierr_horse'] = $this->error['valierr_horse'];
		} else {
			$data['valierr_horse'] = '';
		}

		if (isset($this->error['valierr_doctor'])) {
			$data['valierr_doctor'] = $this->error['valierr_doctor'];
		} else {
			$data['valierr_doctor'] = '';
		}

		if (isset($this->error['valierr_clinic'])) {
			$data['valierr_clinic'] = $this->error['valierr_clinic'];
		} else {
			$data['valierr_clinic'] = '';
		}

		if (isset($this->error['req'])) {
			$data['error_req'] = $this->error['req'];
		} else {
			$data['error_req'] = '';
		}

		$url = '';

		if (isset($this->request->get['id'])) {
			$url .= '&id=' . urlencode(html_entity_decode($this->request->get['id'], ENT_QUOTES, 'UTF-8'));
		}


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['previous'] = $this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'], true);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/treatment_entry_single/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/treatment_entry_single/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$transection_info_parent = $this->model_catalog_treatment_entry_single->getTransectionEntryParent($this->request->get['id']);
		}

		$data['token'] = $this->session->data['token'];

		$issue_nos = $this->db->query("SELECT issue_no FROM oc_treatment_entry WHERE 1=1 ORDER BY issue_no DESC LIMIT 1 ");
		/*echo '<pre>';
		print_r($issue_nos->row);
		exit;*/
		if ($issue_nos->num_rows > 0) {
			$data['input_isse_number'] = $issue_nos->row['issue_no'] + 1;
		} else {
			$data['input_isse_number'] = 1;
		}

		if (isset($this->request->post['filter_parent_doctor'])) {
			$data['filter_parent_doctor'] = $this->request->post['filter_parent_doctor'];
		} elseif (!empty($transection_info_parent)) {
			$data['filter_parent_doctor'] = $transection_info_parent['clinic_id'];
		} else {
			$data['filter_parent_doctor'] = '';
		}

		if (isset($this->request->post['filterParentId'])) {
			$data['filter_parent_doctor_id'] = $this->request->post['filterParentId'];
		} elseif (!empty($transection_info_parent)) {
			$data['filter_parent_doctor_id'] = $transection_info_parent['clinic_name'];
		} else {
			$data['filter_parent_doctor_id'] = '';
		}

		if (isset($this->request->post['horse_name'])) {
			$data['horse_name'] = $this->request->post['horse_name'];
		} elseif (!empty($transection_info_parent)) {
			$data['horse_name'] = $transection_info_parent['horse_name'];
		} else {
			$data['horse_name'] = '';
		}

		if (isset($this->request->post['hidden_horse_name_id'])) {
			$data['hidden_horse_name_id'] = $this->request->post['hidden_horse_name_id'];
		} elseif (!empty($transection_info_parent)) {
			$data['hidden_horse_name_id'] = $transection_info_parent['horse_name_id'];
		} else {
			$data['hidden_horse_name_id'] = '';
		}

		if (isset($this->request->post['trainer_name'])) {
			$data['trainer_name'] = $this->request->post['trainer_name'];
		} elseif (!empty($transection_info_parent)) {
			$data['trainer_name'] = $transection_info_parent['trainer_name'];
		} else {
			$data['trainer_name'] = '';
		}

		if (isset($this->request->post['hidden_trainer_name_id'])) {
			$data['trainer_name_id'] = $this->request->post['hidden_trainer_name_id'];
		} elseif (!empty($transection_info_parent)) {
			$data['trainer_name_id'] = $transection_info_parent['trainer_name_id'];
		} else {
			$data['trainer_name_id'] = '';
		}

		if (isset($this->request->post['date'])) {
			$data['date'] = $this->request->post['date'];
		} elseif (!empty($transection_info_parent)) {
			$data['date'] = $transection_info_parent['entry_date'];
		} else {
			if(isset($this->request->get['date_reconsilation'])){ 
				$data['date'] = $this->request->get['date_reconsilation'];
			} else {
				$data['date'] = date('d-m-Y');
			}
		}
		
		if (isset($this->request->post['total_item'])) {
			$data['total_item'] = $this->request->post['total_item'];
		} elseif (!empty($transection_info_parent)) {
			$data['total_item'] = $transection_info_parent['total_item'];
		} else {
			$data['total_item'] =0;
		}

		if (isset($this->request->post['total_qty'])) {
			$data['total_qty'] = $this->request->post['total_qty'];
		} elseif (!empty($transection_info_parent)) {
			$data['total_qty'] = $transection_info_parent['total_qty'];
		} else {
			$data['total_qty'] =0;
		}

		// echo '<pre>';
		// print_r($this->request->post);
		// exit;

		if (isset($this->request->post['total'])) {
			$data['total_amt'] = $this->request->post['total'];
		} elseif (!empty($transection_info_parent)) {
			$data['total_amt'] = $transection_info_parent['total_amt'];
		} else {
			$data['total_amt'] =0;
		}


		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} else {
			$data['productraw_datas'] = array();
		}



		if (isset($this->request->post['to_doc']) && $this->request->post['to_doc'] != '') {
			$data['to_doc'] = $this->request->post['to_doc'];
			$data['child_doctor_name'] = $this->db->query("SELECT * FROM doctor WHERE id = '".$this->request->post['to_doc']."'	")->row['doctor_name'];
			$results =$this->db->query("SELECT * FROM  doctor WHERE isActive = 'Active' AND is_parent = 0 AND `parent_id` = '" . ($this->request->post['filterParentId']) . "' ")->rows;
			
			$json = array();
			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}

			$data['all_docs'] = $json;
		} elseif (!empty($transection_info_parent)) {
			$data['to_doc'] = $transection_info_parent['doctor_id'];
			$data['child_doctor_name'] = $this->db->query("SELECT * FROM doctor WHERE id = '".$transection_info_parent['doctor_id']."'	")->row['doctor_name'];
			$results =$this->db->query("SELECT * FROM  doctor WHERE isActive = 'Active' AND is_parent = 0 AND `parent_id` = '" . ($transection_info_parent['clinic_id']) . "' ")->rows;
			
			$json = array();
			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}

			$data['all_docs'] = $json;

		} else {
			$data['to_doc'] = '';
			$data['all_docs'] = array();
		}

		$data['user_log_grp_id'] = $this->user->getGroupId();

		$data['user_log_id'] = $this->user->getId();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/treatment_entry_single_form', $data));
	}


	public function prints() {

		$inward = $this->db->query("SELECT * FROM oc_inward WHERE order_id = '".$this->request->get['id']."' ")->rows;

		foreach ($inward as $pvalue) {
			$data['inwards'][] = array(
				'inward_code'=> $pvalue['order_no'],
				'supplier'=> $pvalue['supplier'],
				'date'=> date('d-m-Y', strtotime($pvalue['date'])),
				'value_total'=> $pvalue['value_total'],
				'gst_value_total'=> $pvalue['gst_value_total'],
				'all_total'=> $pvalue['all_total'],
			);
		}


		$inward_item = $this->db->query("SELECT * FROM oc_inwarditem WHERE order_no = '".$this->request->get['id']."' ")->rows;
		
		foreach ($inward_item as $value) {
			$inward_po = $this->db->query("SELECT reduce_po_qty FROM `oc_inwarditem` WHERE `po_no` = '".$value['po_no']."' ORDER BY `productraw_id` DESC LIMIT 1 ");
				if ($inward_po->num_rows > 0) {
					$reduce_po_qty = $inward_po->row['reduce_po_qty'];
				} else{
					$reduce_po_qty = $result['qty'];
				}
			$data['inwards_item'][] = array(
				'med_code'=> $value['product_id'],
				'med_name'=> $value['productraw_name'],
				'po_no'=> $value['po_no'],
				'po_qty'=> $value['po_qty'],
				'quantity'=> $value['quantity'],
				'value'=> $value['value'],
				'ex_date'=> $value['ex_date'],
				'batch_no'=> $value['batch_no'],
				'purchase_price' => 'Rs'.' '.$value['purchase_price'].' / '.$value['packing'],
				'packing' => $value['packing'].' '.$value['volume'].' '.$value['unit'],
				'ordered_quantity' => $value['ordered_quantity'].' '.$value['unit'],
				'gst_rate' => $value['gst_rate'].'%',
				'gst_value' => $value['gst_value'],
				'total' => $value['total'],
				'reduce_po_qty' => $value['reduce_po_qty'],
			);
		}

		
		
		$html = $this->load->view('report/inward_report', $data);
		//echo $html;exit;
		$filename = 'Inward.html';
		//file_put_contents(DIR_DOWNLOAD.Indent, $html);
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;
	}

	public function calculate() {
		$this->load->language('catalog/treatment_entry_single');

		$this->document->setTitle('Product Costing');

		$this->load->model('catalog/treatment_entry_single');
		$this->load->model('catalog/product');
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_inward'])) {
			$url .= '&filter_inward=' . urlencode(html_entity_decode($this->request->get['filter_inward'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Product Costing',
			'href' => $this->url->link('catalog/treatment_entry_single/calculate', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['cancel'] = $this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true);
		$data['action'] = $this->url->link('catalog/treatment_entry_single/make', 'token=' . $this->session->data['token'] . $url.'&finished_product_id='.$this->request->get['id'], true);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$inward_info = $this->model_catalog_treatment_entry_single->getinward($this->request->get['id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['inward_name'])) {
			$data['inward_name'] = $this->request->post['inward_name'];
		} elseif (!empty($inward_info)) {
			$data['inward_name'] = $inward_info['inward_name'];
		} else {
			$data['inward_name'] = '';
		}

		if (isset($this->request->post['cost_price'])) {
			$data['cost_price'] = $this->request->post['cost_price'];
		} elseif (!empty($inward_info)) {
			$data['cost_price'] = $inward_info['cost_price'];
		} else {
			$data['cost_price'] = '';
		}

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} elseif (!empty($inward_info)) {
			$product_raw_datass = $this->db->query("SELECT * FROM `oc_inwarditem` WHERE `order_id` = '".$inward_info['order_id']."' ")->rows;
			$productraw_datas = array();
			foreach($product_raw_datass as $pkeys => $pvalues){
				$product_datas = $this->db->query("SELECT `quantity` FROM `is_productnew` WHERE `productnew_id` = '".$pvalues['product_id']."' ")->row;
				if(isset($product_datas['quantity'])){
					$quantity_in_stock = $product_datas['quantity'];
				} else {
					$quantity_in_stock = 0;
				}
				$productraw_datas[] = array(
					'productraw_name' => $pvalues['productraw_name'],
					'productraw_id' => $pvalues['productraw_id'],
					'product_id' => $pvalues['product_id'],
					'order_no' => $pvalues['order_no'],
					'price' => $pvalues['price'],
					'quantity' => $pvalues['quantity'],
					'gst_type' => $pvalues['gst_type'],
					/*'outside_color' => $pvalues['outside_color'],
					'inside_color' => $pvalues['inside_color'],
					'raw_material' => $pvalues['raw_material'],
					'productnew_id' => $pvalues['productnew_id'],*/
					'gst_type_id' => $pvalues['gst_type_id'],
					/*'quantity_in_stock' => $quantity_in_stock,*/
					'total' => $pvalues['total'],
				); 			
			}
			$data['productraw_datas'] = $productraw_datas;
		} else {
			$data['productraw_datas'] = array();
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/treatment_entry_single_calculate', $data));
	}

	public function make() {
		$this->load->model('catalog/treatment_entry_single');
		$this->model_catalog_treatment_entry_single->makeProduct($this->request->get['finished_product_id'], $this->request->post);
		$this->response->redirect($this->url->link('catalog/treatment_entry_single', 'token=' . $this->session->data['token'] . $url, true));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/treatment_entry_single')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		/*echo '<pre>';
		print_r($this->request->post);
		exit;*/
		if ((utf8_strlen($this->request->post['filter_parent_doctor']) == '')) {
			$this->error['valierr_clinic'] = "Please Enter Clinic Name";
		}

		if ((utf8_strlen($this->request->post['horse_name']) == '')) {
			$this->error['valierr_horse'] = "Please Select Horse";
		}

		$chceck_day_close_product = $this->db->query("SELECT * FROM daily_reconsilation WHERE entry_date = '".$this->db->escape(date('Y-m-d', strtotime($this->request->post['date'])))."'  ");
		
		if($chceck_day_close_product->num_rows > 0){
			$this->error['day_close_error'] = "Medicine Not Transfer Because Day Is Not Closed!";
		}

		// if ((utf8_strlen($this->request->post['to_doc']) == '')) {
		// 	$this->error['valierr_doctor'] = "Please Select Doctor";
		// }

		if ((utf8_strlen($this->request->post['trainer_name']) == '')) {
			$this->error['valierr_trainer'] = "Please Select Trainer";
		}

		if(!isset($this->request->post['productraw_datas'])){
			$this->error['medicine_error'] = ''." Please Enter Atleast Transfer Entry";
		} 
		$datas = $this->request->post;
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/treatment_entry_single')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}

	public function autocompleteParentDoc() {
		//echo 'in';
		$json = array();
		//echo '<pre>';print_r($this->request->get);exit;

		if (isset($this->request->get['filter_doctor_code'])) {
			$this->load->model('catalog/treatment_entry_single');

			$results = $this->model_catalog_treatment_entry_single->getDoctorAutos($this->request->get['filter_doctor_code']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'is_parent' => $result['is_parent'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_code'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteChildDoc() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['parent_doc_id'])) {
			$this->load->model('catalog/treatment_entry_single');

			$results = $this->model_catalog_treatment_entry_single->getChildDoctorAutos($this->request->get['parent_doc_id']);
			/*echo'<pre>';
			print_r($this->request->get['parent_doc_id']);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name' => $result['doctor_name'],
					'parent_id' => $result['parent_id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_code'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletehorse() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['trainer_id'])) {
			$this->load->model('catalog/treatment_entry_single');

			$results = $this->model_catalog_treatment_entry_single->getHorseAutos($this->request->get['trainer_id']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {
				if($result['horse_id']){
					$horse_sql =  $this->db->query("SELECT official_name FROM `horse1` WHERE horseseq = '".$result['horse_id']."'");
					if ($horse_sql->num_rows > 0) {
						$horse_name = $horse_sql->row['official_name'];
						$json[] = array(
							'horse_id' => $result['horse_id'],
							'horse_name' => $horse_name,
						);
					}
				}
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['horse_id'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autoHorseToTrainer() {
		//echo 'in';
		$json = array();

		if (isset($this->request->get['horse_id'])) {
			
			$this->load->model('catalog/horse');
			$results = $this->model_catalog_horse->getTrainerCurrent($this->request->get['horse_id']);

			if($results){
				$json['trainer_id'] = $results['trainer_id'];
				$json['trainer_name'] = $results['trainer_name'];
			}
			
		}
		//echo '<pre>';print_r($json);exit;
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function addMedicineTrans() {
		echo "<pre>"; print_r($this->request->post);exit;
		
	}

	public function clinic_search() {
		$json = array();
		$check_valid = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' ");
		if ($check_valid->num_rows > 0) {
			$json['success'] = 0;
			$code_datas = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' ");
			if ($code_datas->num_rows > 0) {
				$doctor_id = $code_datas->row['id'];
			} else {
				$doctor_id = '';
			}
			if ($doctor_id != '') {
				$doctor_datas = $this->db->query("SELECT * FROM `doctor` WHERE `parent_id` = '".$doctor_id."' AND `is_parent` = 0 ");

				if ($doctor_datas->num_rows > 0) {
					foreach ($doctor_datas->rows as $value) {
						$json['docs'][] = array(
							'doctor_id' => $value['id'],
							'doctor_name' => $value['doctor_name'],
						);
					}
					$json['parent'] = $value['parent'];
					$json['parent_id'] = $value['parent_id'];
					$json['success'] = 1;
				}
			}
			if ($json['success'] == 0) {
				$final_data = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' AND `is_parent` = 0 ");
				if ($final_data->num_rows > 0) {
					$parent_id = $final_data->row['parent_id'];
					if ($parent_id == 0) {
						$json = array(
							'parent_id' => $final_data->row['id'],
							'parent' => $final_data->row['doctor_name'],
							'doctor_id' => '',
							'doctor_name' => '',
						);
						$json['success'] = 3;
					} else {
						$json = array(
							'parent_id' => $final_data->row['parent_id'],
							'parent' => $final_data->row['parent'],
							'doctor_id' => $final_data->row['id'],
							'doctor_name' => $final_data->row['doctor_name'],
						);
					}
				} else {
					$final_datas = $this->db->query("SELECT * FROM `doctor` WHERE `doctor_code` = '".$this->request->get['code']."' ");
					if ($final_datas->num_rows > 0) {
						$json = array(
							'parent_id' => $final_datas->row['parent_id'],
							'parent' => $final_datas->row['parent'],
							'doctor_id' => '',
							'doctor_name' => '',
						);
						$json['success'] = 4;
					}
				}
			}
			$json['alert'] = '';
		} else {
			$json['alert'] = 'Please Enter Valid Code!';
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
