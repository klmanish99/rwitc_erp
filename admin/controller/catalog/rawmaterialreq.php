<?php
/*require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;*/
date_default_timezone_set("Asia/Kolkata");
class ControllerCatalogRawmaterialreq extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/rawmaterialreq');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/rawmaterialreq');
		$this->load->model('catalog/product');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/rawmaterialreq');
		/*$this->load->model('catalog/product');*/

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/rawmaterialreq');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') /*&& $this->validateForm()*/) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_rawmaterialreq->addrawmaterialreq($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/rawmaterialreq/add', 'token=' . $this->session->data['token'] . $url, true));
			//$this->print_indent($this->request->post);

		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/rawmaterialreq');
		$this->load->model('catalog/product');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/rawmaterialreq');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') /*&& $this->validateForm()*/) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_rawmaterialreq->editRawmaterialreq($this->request->get['order_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit1() {
		$this->load->language('catalog/rawmaterialreq');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/rawmaterialreq');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') /*&& $this->validateForm()*/) {
			$this->model_catalog_rawmaterialreq->editRawmaterialreq1($this->request->get['order_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			$this->response->redirect($this->url->link('catalog/rawmaterialreq/getForm', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getFormView();
	}

	public function getFormView() {
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['req'])) {
			$data['error_req'] = $this->error['req'];
		} else {
			$data['error_req'] = '';
		}

		if (isset($this->error['reason'])) {
			$data['error_reason'] = $this->error['reason'];
		} else {
			$data['error_reason'] = array();
		}


		if (isset($this->error['date'])) {
			$data['error_date'] = $this->error['date'];
		} else {
			$data['error_date'] = array();
		}

		// echo '<pre>';
		// print_r($data['error_reason']);
		// exit;

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_rawmaterialreq'])) {
			$url .= '&filter_rawmaterialreq=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq_category'])) {
			$url .= '&filter_rawmaterialreq_category=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq_quantity'])) {
			$url .= '&filter_rawmaterialreq_quantity=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_quantity'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq_category_id'])) {
			$url .= '&filter_rawmaterialreq_category_id=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/rawmaterialreq/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/rawmaterialreq/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . $url, true);
		}

		$data['approve'] = $this->url->link('catalog/rawmaterialreq/approve_onform', 'token=' . $this->session->data['token'] . '&order_id=' .  $this->request->get['order_id'] . $url, true);


		$data['cancel'] = $this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true);
		

		if (isset($this->request->get['order_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$rawmaterialreq_info = $this->model_catalog_rawmaterialreq->getRawmaterialreq($this->request->get['order_id']);
		}

		$data['token'] = $this->session->data['token'];

				if (isset($this->request->post['order_no'])) {
			$data['order_no'] = $this->request->post['order_no'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['order_no'] = $rawmaterialreq_info['order_no'];
		} else {
			$data['order_no'] = '';
		}
				if (isset($this->request->post['narration'])) {
			$data['narration'] = $this->request->post['narration'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['narration'] = $rawmaterialreq_info['narration'];
		} else {
			$data['narration'] = '';
		}
		if (isset($this->request->post['date'])) {
			$data['date'] = $this->request->post['date'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['date'] = $rawmaterialreq_info['date'];
		} else {
			$data['date'] = '';
		}

		if (isset($this->request->post['requested_by'])) {
			$data['requested_by'] = $this->request->post['requested_by'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['requested_by'] = $rawmaterialreq_info['requested_by'];
		} else {
			$data['requested_by'] = '';
		}

			if (isset($this->request->get['order_id'])) {
			$data['order_id'] = $this->request->get['order_id'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['order_id'] = $rawmaterialreq_info['order_id'];
		} else {
			$data['order_id'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['status'] = $rawmaterialreq_info['status'];
		} else {
			$data['status'] = '';
		}

		if (!empty($rawmaterialreq_info)) {
			$data['approval_status'] = $rawmaterialreq_info['approval'];
		} else {
			$data['approval_status'] = '';
		}

		if (!empty($rawmaterialreq_info)) {
			$data['approval'] = $this->url->link('catalog/rawmaterialreq/indent_approval', 'token=' . $this->session->data['token'] . '&order_id=' . $rawmaterialreq_info['order_id'] . $url, true);
		} else {
			$data['approval'] = '';
		}

		$types = array(
			
		);

		$data['types'] = $types;

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} elseif (!empty($rawmaterialreq_info)) {
			$product_raw_datass = $this->db->query("SELECT * FROM `oc_rawmaterialreqitem` WHERE `order_id` = '".$rawmaterialreq_info['order_id']."' ")->rows;
			$productraw_datas = array();

			$filter_month = date('m');
			$filter_year = date('Y');

			if($filter_month == 1){
				$prev_filter_month = 12;
				$prev_filter_year = $filter_year - 1;
			} else {
				$prev_filter_month = $filter_month - 1;
				$prev_filter_year = $filter_year;
			}

			$total_value = 0;
			$total_gst_val = 0;
			$total_total = 0;
			foreach($product_raw_datass as $pkeys => $pvalues){
				
				$productraw_datas[] = array(
					'productraw_name' => $pvalues['productraw_name'],
					'supplier_name' => $pvalues['supplier_name'],
					'productraw_id' => $pvalues['productraw_id'],
					'product_id' => $pvalues['product_id'],
					'request' => $pvalues['request'],
					'unit' => $pvalues['unit'],
					'quantity' => $pvalues['quantity'],
					'packing' => $pvalues['packing'],
					'volume' => $pvalues['volume'].' '.$pvalues['unit'],
					'ordered_qty' => $pvalues['ordered_qty'].' '.$pvalues['auto_unit'],
					'rate' => $pvalues['purchase_price'].' / '.$pvalues['packing'],
					'purchase_value' => $pvalues['purchase_value'],
					'gst_rate' => $pvalues['gst_rate'].'%',
					'gst_value' => $pvalues['gst_value'],
					'total' => $pvalues['total'],
					'current_qty' => $pvalues['inward_med_treat_quantity'],
				);
				$total_value = $pvalues['purchase_value'] + $total_value;
				$total_gst_val = $pvalues['gst_value'] + $total_gst_val;
				$total_total = $pvalues['total'] + $total_total;
			}
			$data['final_value'] = $total_value;
			$data['final_gst_val'] = $total_gst_val;
			$data['final_total'] = $total_total;
			$data['productraw_datas'] = $productraw_datas;
			$data['reason'] = $rawmaterialreq_info['reason'];
		} else {
			$data['productraw_datas'] = array();
		}
		//echo "<pre>";print_r($rawmaterialreq_info);exit;

		//$data['user_type'] = $this->user->getUserType();	
		$data['user_group_id'] = $this->user->getGroupId();
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/rawmaterialreq_form_view', $data));
	}

	public function stock() {
		$this->load->language('catalog/rawmaterialreq');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/rawmaterialreq');


		$datas = $this->model_catalog_rawmaterialreq->daterawmaterial();
		foreach ($datas as $key => $value) {
			$data['datas'][] = array(
				'date' => $value['date']
			);

		$is_date = $this->db->query("SELECT * FROM `oc_rawmaterialreqitem` WHERE `order_id` = '".$value['order_id']."' AND date = '".$value['date']."' ");
			if ($is_date->num_rows == 0){
					$this->db->query("INSERT INTO oc_rawmaterialreqitem SET `order_id` = '".$value['order_id']."',date = '".$value['date']."' ");
				}
				
		}
		// echo '<pre>';print_r($is_date);exit;

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true));
		

		$this->getList();
	}

	public function delete() {
		$this->load->language('catalog/rawmaterialreq');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/rawmaterialreq');

		if (isset($this->request->post['selected']) /*&& $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $order_id) {
				$this->model_catalog_rawmaterialreq->deleteRawmaterialreq($order_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}
	public function approve() {
		$order_id1=$_GET['order_id'];
		$user_group_id=$this->db->query("UPDATE oc_rawmaterialreq SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$user_group=$this->db->query("UPDATE oc_rawmaterialreqitem SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$this->response->redirect($this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function approve_onform() {
		$order_id1=$_GET['order_id'];
		$user_group_id=$this->db->query("UPDATE oc_rawmaterialreq SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$user_group=$this->db->query("UPDATE oc_rawmaterialreqitem SET 
							status=1
							WHERE order_id = '" . $order_id1 . "'");
		$this->response->redirect($this->url->link('catalog/rawmaterialreq/edit1', 'token=' . $this->session->data['token'] . '&order_id=' .$order_id1. $url, true));
	}

	protected function getList() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		if (isset($this->request->get['filter_rawmaterialreq'])) {
			$filter_rawmaterialreq = $this->request->get['filter_rawmaterialreq'];
		} else {
			$filter_rawmaterialreq = null;
		}

		if (isset($this->request->get['filter_order_no'])) {
			$filter_order_no = $this->request->get['filter_order_no'];
		} else {
			$filter_order_no = null;
		}

		if (isset($this->request->get['filter_productsort'])) {
			$filter_productsort = $this->request->get['filter_productsort'];
		} else {
			$filter_productsort = null;
		}

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_rawmaterialreq_category'])) {
			$filter_rawmaterialreq_category = $this->request->get['filter_rawmaterialreq_category'];
		} else {
			$filter_rawmaterialreq_category = null;
		}

		if (isset($this->request->get['filter_rawmaterialreq_category_id'])) {
			$filter_rawmaterialreq_category_id = $this->request->get['filter_rawmaterialreq_category_id'];
		} else {
			$filter_rawmaterialreq_category_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = 'Active';
			$data['filter_status'] = 'Active';
		}

		$url = '';

		if (isset($this->request->get['filter_rawmaterialreq_category'])) {
			$url .= '&filter_rawmaterialreq_category=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq_category_id'])) {
			$url .= '&filter_rawmaterialreq_category_id=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq'])) {
			$url .= '&filter_rawmaterialreq=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}


		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = 0;
			$data['filter_status'] = 0;
		}

		$data['status'] =array(
				'0'  =>"Active",
				'1'  =>'In-Active'
		);
		$data['user_group_id'] = $this->user->getGroupId();
		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . $url, true);
		$data['add'] = $this->url->link('catalog/rawmaterialreq/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['stock'] = $this->url->link('catalog/rawmaterialreq/stock', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/rawmaterialreq/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['rawmaterialreqs'] = array();

		$filter_data = array(
			'filter_rawmaterialreq'	  => $filter_rawmaterialreq,
			'filter_order_no'	  => $filter_order_no,
			'filter_order_id'  => $filter_order_id,
			'filter_rawmaterialreq_category'	  => $filter_rawmaterialreq_category,
			'filter_rawmaterialreq_category_id'  => $filter_rawmaterialreq_category_id,
			'filter_productsort'	=> $filter_productsort,
			'filter_status'	=> $filter_status,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
			//echo '<pre>';
			//print_r($filter_data);
			//exit;

		$rawmaterialreq_total = $this->model_catalog_rawmaterialreq->getTotalRawmaterialreqs($filter_data);

		$results = $this->model_catalog_rawmaterialreq->getRawmaterialreqs($filter_data);
		$user_group_id = $this->user->getGroupId();

			 //echo '<pre>';
			 //print_r($results);
			 //echo $user_group_id;
			 //exit;
			
		foreach ($results as $result) {

			$logs_info = $this->db->query("SELECT * FROM `indent_logs` WHERE `ind_id` = '".$result['order_id']."' ORDER BY `id` DESC LIMIT 1 ");
			// echo'<pre>';
			// print_r("SELECT * FROM `supplier_logs` WHERE `supp_id` = '".$result['vendor_id']."' ");
			if ($logs_info->num_rows > 0) {
				$fname = $logs_info->row['fname'];
				$lname = $logs_info->row['lname'];
				$log_date = date('d-m-Y', strtotime($logs_info->row['log_date']));
				$log_time = $logs_info->row['log_time'];
				$log_datas = '<span>'.'Name: '.$fname.' '.$lname.'</span>'.'<br>'.'<span>'.'Date: '.$log_date.'</span>'.'<br>'.'<span>'.'Time: '.$log_time.'</span>';
			} else {
				$log_datas = '';
			}
			
			$data['rawmaterialreqs'][] = array(
				'order_id' => $result['order_id'],
				'order_no' => $result['order_no'],
				'date' => $result['date'],
				'approval_status' => $result['approval'],
				'narration' => $result['narration'],
				'status' => $result['status'],
				'requested_by' => $result['requested_by'],
				'log_datas'     => $log_datas,
				'view_status' => $result['view_status'],
				'approve'     => $this->url->link('catalog/rawmaterialreq/approve', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'edit'     => $this->url->link('catalog/rawmaterialreq/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'edit1'     => $this->url->link('catalog/rawmaterialreq/edit1', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'cancel'     => $this->url->link('catalog/rawmaterialreq/cancel', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'print'     => $this->url->link('catalog/rawmaterialreq/prints', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'email'     => $this->url->link('catalog/rawmaterialreq/email', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'approval'     => $this->url->link('catalog/rawmaterialreq/indent_approval', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'dis_approval'     => $this->url->link('catalog/rawmaterialreq/indent_dis_approval', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'calculate'     => $this->url->link('catalog/rawmaterialreq/calculate', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true)
			);
		}

		$data['productsorts'] = array(
			'a'=>'A','b'=>'B','c'=>'C','d'=>'D','e'=>'E','f'=>'F','g'=>'G','h'=>'H',
			'i'=>'I','j'=>'J','k'=>'K','l'=>'L','m'=>'M','n'=>'N','o'=>'O','p'=>'P',
			'q'=>'Q','r'=>'R','s'=>'S','t'=>'T','u'=>'U','v'=>'V','w'=>'W','x'=>'X',
			'y'=>'Y','z'=>'Z','Other'
		);
			// echo '<pre>';
			// print_r($data['productsorts']);
			// exit;
		$data['user_group_id'] = $this->user->getGroupId();
		//$data['user_type'] = $this->user->getUserType();	
		$data['base_link_1'] = $this->url->link('catalog/rawmaterialreq/edit', 'token=' . $this->session->data['token']);
		$data['base_link'] = $this->url->link('catalog/rawmaterialreq/edit', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_rawmaterialreq_category'])) {
			$url .= '&filter_rawmaterialreq_category=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq_category_id'])) {
			$url .= '&filter_rawmaterialreq_category_id=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq'])) {
			$url .= '&filter_rawmaterialreq=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . '&sort=rawmaterialreq_name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_rawmaterialreq_category'])) {
			$url .= '&filter_rawmaterialreq_category=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq_category_id'])) {
			$url .= '&filter_rawmaterialreq_category_id=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq'])) {
			$url .= '&filter_rawmaterialreq=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_productsort'])) {
			$url .= '&filter_productsort=' . urlencode(html_entity_decode($this->request->get['filter_productsort'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $rawmaterialreq_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($rawmaterialreq_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($rawmaterialreq_total - $this->config->get('config_limit_admin'))) ? $rawmaterialreq_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $rawmaterialreq_total, ceil($rawmaterialreq_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_rawmaterialreq_category'] = $filter_rawmaterialreq_category;
		$data['filter_rawmaterialreq_category_id'] = $filter_rawmaterialreq_category_id;
		$data['filter_rawmaterialreq'] = $filter_rawmaterialreq;
		$data['filter_order_no'] = $filter_order_no;
		$data['filter_productsort'] = $filter_productsort;
		$data['filter_order_id'] = $filter_order_id;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];
		
		if(isset($this->session->data['is_user'])){
			if($this->user->getId() == '13'){
				$data['is_user'] = '0';
			} else {
				$data['is_user'] = '1';
			}
		} else {
			$data['is_user'] = '0';
		}

		
		

		$this->response->setOutput($this->load->view('catalog/rawmaterialreq_list', $data));
	}

	protected function getForm() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['req'])) {
			$data['error_req'] = $this->error['req'];
		} else {
			$data['error_req'] = '';
		}

		if (isset($this->error['reason'])) {
			$data['error_reason'] = $this->error['reason'];
		} else {
			$data['error_reason'] = array();
		}


		if (isset($this->error['date'])) {
			$data['error_date'] = $this->error['date'];
		} else {
			$data['error_date'] = array();
		}

		// echo '<pre>';
		// print_r($data['error_reason']);
		// exit;

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_rawmaterialreq'])) {
			$url .= '&filter_rawmaterialreq=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . urlencode(html_entity_decode($this->request->get['filter_order_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . urlencode(html_entity_decode($this->request->get['filter_order_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq_category'])) {
			$url .= '&filter_rawmaterialreq_category=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq_quantity'])) {
			$url .= '&filter_rawmaterialreq_quantity=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_quantity'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rawmaterialreq_category_id'])) {
			$url .= '&filter_rawmaterialreq_category_id=' . urlencode(html_entity_decode($this->request->get['filter_rawmaterialreq_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['previous'] = $this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'], true);
		$data['add_med'] = $this->url->link('catalog/medicine/add', 'token=' . $this->session->data['token']. '&indent='.'in', true);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/rawmaterialreq/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/rawmaterialreq/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['order_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$rawmaterialreq_info = $this->model_catalog_rawmaterialreq->getRawmaterialreq($this->request->get['order_id']);
		}

		$data['token'] = $this->session->data['token'];

		$order_nos = $this->db->query("SELECT order_no FROM oc_rawmaterialreq WHERE 1=1 ORDER BY order_id DESC LIMIT 1 ");
		if ($order_nos->num_rows > 0) {
			$order_no = $order_nos->row['order_no'];
		} else {
			$order_no = 0;
		}

		if (isset($this->request->post['order_no'])) {
			$data['order_no'] = $this->request->post['order_no'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['order_no'] = $rawmaterialreq_info['order_no'];
		} else {
			$data['order_no'] = $order_no + 1;
		}
		if (isset($this->request->post['narration'])) {
			$data['narration'] = $this->request->post['narration'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['narration'] = $rawmaterialreq_info['narration'];
		} else {
			$data['narration'] = '';
		}

		if (isset($this->request->post['requested_by'])) {
			$data['requested_by'] = $this->request->post['requested_by'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['requested_by'] = $rawmaterialreq_info['requested_by'];
		} else {
			$data['requested_by'] = '';
		}

		if (!empty($rawmaterialreq_info)) {
			$data['order_id'] = $rawmaterialreq_info['order_id'];
		} else {
			$data['order_id'] = '';
		}
		if (isset($this->request->post['date'])) {
			$data['date'] = $this->request->post['date'];
		} elseif (!empty($rawmaterialreq_info)) {
			$data['date'] = (date('d-m-Y', strtotime($rawmaterialreq_info['date'])));
		} else {
			$data['date'] = date('d-m-Y');
		}

		if (!empty($rawmaterialreq_info)) {
			$data['approval_status'] = $rawmaterialreq_info['approval'];
		} else {
			$data['approval_status'] = '';
		}

		if (!empty($rawmaterialreq_info)) {
			$data['approval'] = $this->url->link('catalog/rawmaterialreq/indent_approval', 'token=' . $this->session->data['token'] . '&order_id=' . $rawmaterialreq_info['order_id'] . $url, true);
		} else {
			$data['approval'] = '';
		}

		$data['user_log_grp_id'] = $this->user->getGroupId();

		$data['user_log_id'] = $this->user->getId();

		if (isset($this->request->post['productraw_datas'])) {
			$data['productraw_datas'] = $this->request->post['productraw_datas'];
		} elseif (!empty($rawmaterialreq_info)) {
			if ($this->request->get['order_id']) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = '';
			}
			$indent_datass = $this->db->query("SELECT * FROM `oc_rawmaterialreq` WHERE `order_id` = '".$rawmaterialreq_info['order_id']."' ");
			if ($indent_datass->num_rows > 0) {
				$data['purchase_value'] = $indent_datass->row['final_value'];
				$data['gst_value'] = $indent_datass->row['final_gst_value'];
				$data['total'] = $indent_datass->row['final_total'];
			} else {
				$data['purchase_value'] = 0;
				$data['gst_value'] = 0;
				$data['total'] = 0;
			}
			$product_raw_datass = $this->db->query("SELECT * FROM `oc_rawmaterialreqitem` WHERE `order_id` = '".$rawmaterialreq_info['order_id']."' ")->rows;
			$productraw_datas = array();

			foreach($product_raw_datass as $pkeys => $pvalues){
				
				$productraw_datas[] = array(
					'order_id' => $order_id,
					'productraw_id' => $pvalues['productraw_id'],
					'med_id' => $pvalues['med_id'],
					'product_id' => $pvalues['product_id'],
					'request' => $pvalues['request'],
					'productraw_code' => $pvalues['order_no'],
					'productraw_name' => $pvalues['productraw_name'],
					'supplier_name' => $pvalues['supplier_name'],
					'volume' => $pvalues['volume'],
					'packing' => $pvalues['packing'],
					'quantity' => $pvalues['quantity'],
					'ordered_qty' => $pvalues['ordered_qty'].' '.$pvalues['auto_unit'],
					'rate' => $pvalues['purchase_price'],
					'purchase_value' => $pvalues['purchase_value'],
					'gst_rate' => $pvalues['gst_rate'],
					'gst_value' => $pvalues['gst_value'],
					'total' => $pvalues['total'],
					'unit' => $pvalues['unit'],
					'auto_unit' => $pvalues['auto_unit'],
					'current_qty' => $pvalues['inward_med_treat_quantity'],
				);
			}
			$data['productraw_datas'] = $productraw_datas;
		} else {
			$data['productraw_datas'] = array();
			$data['purchase_value'] = 0;
			$data['gst_value'] = 0;
			$data['total'] = 0;
		}
		//$data['user_type'] = $this->user->getUserType();
		$data['user_group_id'] = $this->user->getGroupId();	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/rawmaterialreq_form', $data));
	}


	public function prints() {

		$indent = $this->db->query("SELECT * FROM oc_rawmaterialreq WHERE order_id = '".$this->request->get['order_id']."' ")->rows;

		foreach ($indent as $pvalue) {
			$data['indents'][] = array(
				'indent_code'=> $pvalue['order_no'],
				'date'=> date('d-m-Y', strtotime($pvalue['date'])),
				'narration'=> $pvalue['narration'],
				'requested_by'=> $pvalue['requested_by'],
				'approved_by'=> $pvalue['approved_by'],
			);
		}
		$data['final_value'] = $pvalue['final_value'];
		$data['final_gst_val'] = $pvalue['final_gst_value'];
		$data['final_total'] = $pvalue['final_total'];


		$indent_item = $this->db->query("SELECT * FROM oc_rawmaterialreqitem WHERE order_no = '".$this->request->get['order_id']."' ")->rows;
		
		$total_value = 0;
		$total_gst_val = 0;
		$total_total = 0;
		foreach ($indent_item as $value) {

			$data['indents_item'][] = array(
				'request'=> $value['request'],
				'med_code'=> $value['product_id'],
				'med_name'=> $value['productraw_name'],
				'supplier_name'=> $value['supplier_name'],
				'volume'=> $value['volume'].' '.$value['unit'],
				'packing'=> $value['packing'],
				'quantity'=> $value['quantity'],
				'ordered_qty'=> $value['ordered_qty'].' '.$value['auto_unit'],
				'purchase_price'=> $value['purchase_price'].' / '.$value['packing'],
				'purchase_value'=> $value['purchase_value'],
				'gst_rate'=> $value['gst_rate'].'%',
				'gst_value'=> $value['gst_value'],
				'total'=> $value['total'],
			);
			//$total_value = $value['purchase_value'] + $total_value;
			//$total_gst_val = $value['gst_value'] + $total_gst_val;
			//$total_total = $value['total'] + $total_total;
		}
		
		$html = $this->load->view('report/order_stock_html', $data);
		//echo $html;exit;
		$filename = 'Indent.html';
		//file_put_contents(DIR_DOWNLOAD.Indent, $html);
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;
	}

	public function autocomplete() {
		$json = array();
		 /*echo '<pre>';
		 print_r($this->request->get);
		 exit;*/
		if (isset($this->request->get['filter_order_id'])) {
			$this->load->model('catalog/rawmaterialreq');

			$filter_data = array(
				'filter_order_id' => $this->request->get['filter_order_id'],
				//'start'       => 0,
				//'limit'       => 5
			);
			$results = $this->model_catalog_rawmaterialreq->getRawmaterialreqs($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'order_id' => $result['order_id'],
					'order_no'     => strip_tags(html_entity_decode($result['order_no'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['order_no'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete_raw() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/rawmaterialreq');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_rawmaterialreq->getProductnews($filter_data);

			foreach ($results as $result) {
				$suppliers = $this->db->query("SELECT * FROM oc_inward i LEFT JOIN oc_inwarditem ii ON(i.order_no = ii.order_no) WHERE product_id = '".$result['med_code']."' ORDER BY productraw_id DESC LIMIT 1 ");
				if ($suppliers->num_rows > 0) {
					$supplier = $suppliers->row['supplier'];
					$purchase_price = $suppliers->row['purchase_price'];
				} else {
					$prates = $this->db->query("SELECT * FROM medicine WHERE med_code = '".$result['med_code']."' ");
					if ($prates->num_rows > 0) {
						$prate = $prates->row['purchase_price'];
					} else {
						$prate = '';
					}
					$supplier = '';
					$purchase_price = $prate;
				}
				$quantity = 1;
				$json[] = array(
					'med_id' => $result['id'],
					'med_code' => strip_tags(html_entity_decode($result['med_code'], ENT_QUOTES, 'UTF-8')),
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8')),
					'supplier'     => strip_tags(html_entity_decode($supplier, ENT_QUOTES, 'UTF-8')),
					'unit'     => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8')),
					'packing'     => strip_tags(html_entity_decode($result['pack_type'], ENT_QUOTES, 'UTF-8')),
					'volume'     => strip_tags(html_entity_decode($result['volume'], ENT_QUOTES, 'UTF-8')),
					'gst_rate'     => strip_tags(html_entity_decode($result['gst_rate'], ENT_QUOTES, 'UTF-8')),
					'purchase_price'     => strip_tags(html_entity_decode($purchase_price, ENT_QUOTES, 'UTF-8')),
					'quantity' => '',
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['med_name'];
			
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}

	public function autocomplete_raw_code() {
		$json = array();

		if (isset($this->request->get['med_code'])) {
			$this->load->model('catalog/rawmaterialreq');

			$filter_data = array(
				'med_code' => $this->request->get['med_code'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_rawmaterialreq->getProductcode($filter_data);

			foreach ($results as $result) {
				$suppliers = $this->db->query("SELECT * FROM oc_inward i LEFT JOIN oc_inwarditem ii ON(i.order_no = ii.order_no) WHERE product_id = '".$result['med_code']."' ORDER BY productraw_id DESC LIMIT 1 ");
				if ($suppliers->num_rows > 0) {
					$supplier = $suppliers->row['supplier'];
					$purchase_price = $suppliers->row['purchase_price'];
				} else {
					$prates = $this->db->query("SELECT * FROM medicine WHERE med_code = '".$result['med_code']."' ");
					if ($prates->num_rows > 0) {
						$prate = $prates->row['purchase_price'];
					} else {
						$prate = '';
					}
					$supplier = '';
					$purchase_price = $prate;
				}

				$quantity = 1;
				$json = array(
					'med_id' => $result['id'],
					'med_code' => strip_tags(html_entity_decode($result['med_code'], ENT_QUOTES, 'UTF-8')),
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8')),
					'supplier'     => strip_tags(html_entity_decode($supplier, ENT_QUOTES, 'UTF-8')),
					'unit'     => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8')),
					'packing'     => strip_tags(html_entity_decode($result['pack_type'], ENT_QUOTES, 'UTF-8')),
					'volume'     => strip_tags(html_entity_decode($result['volume'], ENT_QUOTES, 'UTF-8')),
					'gst_rate'     => strip_tags(html_entity_decode($result['gst_rate'], ENT_QUOTES, 'UTF-8')),
					'purchase_price'     => strip_tags(html_entity_decode($purchase_price, ENT_QUOTES, 'UTF-8')),
					'quantity' => '',
				);
			}//exit;
		}

		$sort_order = array();

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}

	public function cancel() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$this->db->query("UPDATE oc_rawmaterialreq SET status = 1 WHERE order_id = '".$this->request->get['order_id']."' ");
		$this->response->redirect($this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] ));

	}

	public function email() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$indent = $this->db->query("SELECT * FROM oc_rawmaterialreq WHERE order_id = '".$this->request->get['order_id']."' ")->rows;

		foreach ($indent as $pvalue) {
			$data['indents'][] = array(
				'indent_code'=> $pvalue['order_no'],
				'date'=> date('d-m-Y', strtotime($pvalue['date'])),
				'narration'=> $pvalue['narration'],
				'requested_by'=> $pvalue['requested_by'],
				'approved_by'=> $pvalue['approved_by'],
			);
		}


		$indent_item = $this->db->query("SELECT * FROM oc_rawmaterialreqitem WHERE order_no = '".$this->request->get['order_id']."' ")->rows;
		
		$total_value = 0;
		$total_gst_val = 0;
		$total_total = 0;
		foreach ($indent_item as $value) {

			$data['indents_item'][] = array(
				'request'=> $value['request'],
				'med_code'=> $value['product_id'],
				'med_name'=> $value['productraw_name'],
				'volume'=> $value['volume'].' '.$value['unit'],
				'packing'=> $value['packing'],
				'supplier_name'=> $value['supplier_name'],
				'quantity'=> $value['quantity'].' '.$value['auto_unit'],
				'ordered_qty'=> $value['ordered_qty'],
				'purchase_price'=> $value['purchase_price'].' / '.$value['packing'],
				'purchase_value'=> $value['purchase_value'],
				'gst_rate'=> $value['gst_rate'].'%',
				'gst_value'=> $value['gst_value'],
				'total'=> $value['total'],
			);
			$total_value = $value['purchase_value'] + $total_value;
			$total_gst_val = $value['gst_value'] + $total_gst_val;
			$total_total = $value['total'] + $total_total;
		}
		$data['final_value'] = $total_value;
		$data['final_gst_val'] = $total_gst_val;
		$data['final_total'] = $total_total;

		
		
		$html = $this->load->view('report/order_stock_html', $data);
		//echo $html;exit;
		$filename = 'Indent_'.$pvalue['order_no'].'.html';
		//file_put_contents(DIR_DOWNLOAD.Indent, $html);
		//header('Content-disposition: attachment; filename=' . $filename);
		//header('Content-type: text/html');
		//echo $html;exit;

		$indent = DIR_DOWNLOAD.'indent/Indent_'.$pvalue['order_no'].'.html';
	    file_put_contents($indent, $html);
	    $filename = 'Indent_'.$pvalue['order_no'].'.html';
	    $file_path = DIR_DOWNLOAD.'indent/'.$filename;
	    //echo $file_path;exit;

		require_once(DIR_SYSTEM . 'library/PHPMailer/PHPMailerAutoload.php');
		require_once(DIR_SYSTEM . 'library/PHPMailer/class.phpmailer.php');
		require_once(DIR_SYSTEM . 'library/PHPMailer/class.smtp.php');

		$mail = new PHPMailer();

		$mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		    )
		);
		//$mail->SMTPDebug = 3;
		//$mail->isMail(); // Set mailer to use SMTP
		//$mail->Host = 'localhost'; 
		$message = "Dear Sir,\nHere Attached Is The Indent,\nThank You,";

		$subject = 'Indent Report';
		//$from_email = $this->request->post['from_branch_email'];
		$from_email = 'eric.fargose@gmail.com';
		//$to_emails = $this->request->post['to_requester_email'];
		$to_emails = 'eric.fargose@gmail.com';
		if($to_emails != ''){
			$to_emails_array = explode(',', $to_emails);
		}

		$filename = 'Indent_'.$pvalue['order_no'].'.html';
		$file_path =DIR_DOWNLOAD.'indent/'.$filename;

		//$mail->SMTPDebug = 3;
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Helo = "HELO";
		$mail->Host = 'smtp.gmail.com';//$this->config->get('config_smtp_host');
		$mail->Port = '587';//$this->config->get('config_smtp_port');
		$mail->Username = 'stores.rwitc';//$this->config->get('config_smtp_username');
		$mail->Password = 'Store#rwitc90';//$this->config->get('config_smtp_password');
		$mail->SMTPSecure = 'tls';
		$mail->SetFrom($from_email, 'RWITC');
		$mail->Subject = $subject;
		$mail->Body = html_entity_decode($message);
		$mail->addAttachment($file_path, $filename);
		foreach($to_emails_array as $ekey => $evalue){
			$mail->AddAddress($evalue);
		}

		if($mail->Send()) {
			$this->session->data['success'] = 'Mail Sent';
		  	// echo 'Mail Sent';
		  	// echo '<br />';
		  	// echo '<pre>';
		  	// print_r($mail->ErrorInfo);
		  	$this->db->query("UPDATE oc_rawmaterialreq SET email_send = 1 WHERE order_id = '".$this->request->get['order_id']."' ");
		} else {
			$this->session->data['success'] = 'Mail Not Sent';
		  	//echo "Mailer Error: " . $mail->ErrorInfo;
			// echo '<pre>';
			// print_r($mail->ErrorInfo);
			// exit;
		}
		$this->response->redirect($this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function autocompletereq() {
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		$json = array();
		
		if (isset($this->request->get['requested_by'])) {
			$this->load->model('catalog/rawmaterialreq');

			$filter_data = array(
				'requested_by' => $this->request->get['requested_by'],
				//'start'       => 0,
				//'limit'       => 5
			);
			$results = $this->model_catalog_rawmaterialreq->getrequest($filter_data);

			foreach ($results as $result) {
				
				$json[] = array(
					'request' => $result['name'],
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['request'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	function indent_approval() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$user_id = $this->session->data['user_id'];
		// echo'<pre>';
		// print_r($user_id);
		// exit;
		$names = $this->db->query("SELECT * FROM oc_user WHERE user_id = '".$user_id."' ");
		if ($names->num_rows > 0) {
			$name = $names->row['firstname'];
		} else {
			$name = 'Admin';
		}

		$this->db->query("UPDATE oc_rawmaterialreq SET `approval` = 1, approved_by = '".$name."'  WHERE order_id = '".$this->request->get['order_id']."' ");
		
		$this->response->redirect($this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true));
	}

	function indent_dis_approval() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$user_id = $this->session->data['user_id'];
		// echo'<pre>';
		// print_r($user_id);
		// exit;
		$names = $this->db->query("SELECT * FROM oc_user WHERE user_id = '".$user_id."' ");
		if ($names->num_rows > 0) {
			$name = $names->row['firstname'];
		} else {
			$name = 'Admin';
		}

		$this->db->query("UPDATE oc_rawmaterialreq SET `approval` = 2, approved_by = '".$name."', reason = '".$this->request->get['reason']."'  WHERE order_id = '".$this->request->get['order_id']."' ");
		
		$this->response->redirect($this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'] . $url, true));
	}

	function valid_supplier() {
		$checks = $this->db->query("SELECT name FROM trainers WHERE name LIKE '%" . $this->db->escape($this->request->get['requested_by']) . "%' UNION ALL SELECT doctor_name FROM doctor WHERE doctor_name = '" . $this->db->escape($this->request->get['requested_by']) . "' ");
		if ($checks->num_rows > 0) {
			$json['valid'] = 1;
		} else {
			$json['valid'] = 0;
		}
		// echo'<pre>';
		// print_r($json);
		// exit;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
	public function CurrentDayQty() {
	
		$json = array();
		$inward_qty = 0;
		$inward_qty_sql = $this->db->query("SELECT SUM(quantity) AS inward_qty FROM `oc_inwarditem` WHERE `medicine_id` = '".$this->db->escape($this->request->get['medicine_id'])."'  AND date_added = '".date('Y-m-d')."' GROUP BY medicine_id");
		if ($inward_qty_sql->num_rows > 0) {
			$inward_qty = $inward_qty_sql->row['inward_qty'];
		} 

		$treatment_entry_qty = $this->db->query("SELECT SUM(medicine_qty) AS treament_entry_qtys FROM `oc_treatment_entry` LEFT JOIN `oc_treatment_entry_trans` ON oc_treatment_entry.id =oc_treatment_entry_trans.parent_id WHERE oc_treatment_entry_trans.medicine_code = '".$this->db->escape($this->request->get['medicine_code'])."' AND oc_treatment_entry.entry_date = '".date('Y-m-d')."' GROUP BY  oc_treatment_entry_trans.medicine_code");
		$json['treatment_entry_qty'] = 0;
		if ($treatment_entry_qty->num_rows > 0) {
			$json['treatment_entry_qty'] = $inward_qty - $treatment_entry_qty->row['treament_entry_qtys'];
		} else {
			$json['treatment_entry_qty'] = $inward_qty;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function CheckIndentProduct() {
		$checkindent_array = array();
		$check_product_in_table = $this->db->query("SELECT * FROM `oc_rawmaterialreq` LEFT JOIN `oc_rawmaterialreqitem` ON oc_rawmaterialreq.order_id =oc_rawmaterialreqitem.order_id  LEFT JOIN indent_logs AS il ON oc_rawmaterialreq.order_id = il.ind_id WHERE oc_rawmaterialreqitem.med_id = '".$this->db->escape($this->request->get['medicine_id'])."' AND oc_rawmaterialreq.approval = 0 ");
		
		$checkindent_array['already_indent_product'] = 0;
		if ($check_product_in_table->num_rows > 0) {
			$checkindent_array['user_name'] = $check_product_in_table->row['fname'];
			$checkindent_array['already_indent_product'] = 1;
		} 

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($checkindent_array));
	}


	/*protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/treatment_entry')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		echo '<pre>';
		print_r($this->request->post);
		exit;
		$datas = $this->request->post;

		
		return !$this->error;
	}*/
}