<?php
date_default_timezone_set("Asia/Kolkata");
class ControllerCatalogDoctor extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/doctor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/doctor');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/doctor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/doctor');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			$this->model_catalog_doctor->addDoctors($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/doctor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/doctor');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			$this->model_catalog_doctor->editDoctors($this->request->get['id'], $this->request->post);
			// echo '<pre>';
			// print_r($this->request->post);
			
			// exit;
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/doctor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/doctor');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_doctor->deleteDoctors($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function repair() {
		$this->load->language('catalog/doctor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/doctor');

		if ($this->validateRepair()) {
			$this->model_catalog_doctor->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('catalog/doctor', 'token=' . $this->session->data['token'], true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/doctor/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/doctor/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/doctor/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['filter_doctor_name'])) {
			$filter_doctor_name = $this->request->get['filter_doctor_name'];
			$data['filter_doctor_name'] = $this->request->get['filter_doctor_name'];
		}
		else{
			$filter_doctor_name = '';
			$data['filter_doctor_name'] = '';
		}

		if (isset($this->request->get['filter_doctor_code'])) {
			$filter_doctor_code = $this->request->get['filter_doctor_code'];
			$data['filter_doctor_code'] = $this->request->get['filter_doctor_code'];
		}
		else{
			$filter_doctor_code = '';
			$data['filter_doctor_code'] = '';
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = 'Active';
			$data['filter_status'] = 'Active';
		}

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_doctor_name'	=>	$filter_doctor_name,
			'filter_doctor_code'	=>	$filter_doctor_code,
			'filter_status'	=>	$filter_status
		);

		$data['status'] =array(
				'Active'  =>"Active",
				'In-Active'  =>'In-Active'
		);

		$category_total = $this->model_catalog_doctor->getTotalDoctors();

		$results = $this->model_catalog_doctor->getDoctor($filter_data);
		//echo "<pre>";print_r($results);exit;

		foreach ($results as $result) {
			$logs_info = $this->db->query("SELECT * FROM `doctor_logs` WHERE `doc_id` = '".$result['id']."' ORDER BY `id` DESC LIMIT 1 ");
			// echo'<pre>';
			// print_r("SELECT * FROM `supplier_logs` WHERE `supp_id` = '".$result['vendor_id']."' ");
			if ($logs_info->num_rows > 0) {
				$fname = $logs_info->row['fname'];
				$lname = $logs_info->row['lname'];
				$log_date = date('d-m-Y', strtotime($logs_info->row['log_date']));
				$log_time = $logs_info->row['log_time'];
				$log_datas = '<span>'.'Name: '.$fname.' '.$lname.'</span>'.'<br>'.'<span>'.'Date: '.$log_date.'</span>'.'<br>'.'<span>'.'Time: '.$log_time.'</span>';
			} else {
				$log_datas = '';
			}
			
			if ($result['doctor_name'] == $result['parent']) {
				$parent = '';
			} else {
				$parent = $result['parent'];
			}
			$data['categories'][] = array(
				'id' => $result['id'],
				'doctor_name'     => $result['doctor_name'],
				'doctor_code'        => $result['doctor_code'],
				'parent'        => $parent,
				'status'        => $result['isActive'],
				'log_datas'     => $log_datas,
				'edit'        => $this->url->link('catalog/doctor/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true),
				'delete'      => $this->url->link('catalog/doctor/delete', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		//$data['parent'] = $this->language->get('parent');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/doctor_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_doctor_code'] = $this->language->get('entry_doctor_code');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['valierr_doctor_name'])) {
			$data['valierr_doctor_name'] = $this->error['valierr_doctor_name'];
		} else {
			$data['valierr_doctor_name'] = '';
		}

		if (isset($this->error['valierr_doctor_code'])) {
			$data['valierr_doctor_code'] = $this->error['valierr_doctor_code'];
		} else {
			$data['valierr_doctor_code'] = '';
		}

		if (isset($this->request->post['alpha'])) {
			$data['alpha'] = $this->request->post['alpha'];
		} elseif (!empty($category_info)) {
			$data['alpha'] = $category_info['alpha'];
		} else {
			$data['alpha'] = '';
		}

		if (isset($this->request->post['alpha_no'])) {
			$data['alpha_no'] = $this->request->post['alpha_no'];
		} elseif (!empty($category_info)) {
			$data['alpha_no'] = $category_info['alpha_no'];
		} else {
			$data['alpha_no'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/doctor/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/doctor/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_doctor->getDoctors($this->request->get['id']);
		}

		if (isset($this->request->post['doctor_name'])) {
			$data['doctor_name'] = $this->request->post['doctor_name'];
		} elseif (!empty($category_info)) {
			$data['doctor_name'] = $category_info['doctor_name'];
		} else {
			$data['doctor_name'] = '';
		}

		if (isset($this->request->post['doctor_code'])) {
			$data['doctor_code'] = $this->request->post['doctor_code'];
		} elseif (!empty($category_info)) {
			$data['doctor_code'] = $category_info['doctor_code'];
		} else {
			$data['doctor_code'] = '';
		}

		if (isset($this->request->post['ambulance_no'])) {
			$data['ambulance_no'] = $this->request->post['ambulance_no'];
		} elseif (!empty($category_info)) {
			$data['ambulance_no'] = $category_info['ambulance_no'];
		} else {
			$data['ambulance_no'] = '';
		}

		if (isset($this->request->post['parent'])) {
			$data['parent'] = $this->request->post['parent'];
		} elseif (!empty($category_info)) {
			$data['parent'] = $category_info['parent'];
		} else {
			$data['parent'] = '';
		}

		if (isset($this->request->post['hidden_parent'])) {
			$data['hidden_parent'] = $this->request->post['hidden_parent'];
		} elseif (!empty($category_info)) {
			$data['hidden_parent'] = $category_info['parent_id'];
		} else {
			$data['hidden_parent'] = '';
		}

		$data['user_log_grp_id'] = $this->user->getGroupId();

		$data['user_log_id'] = $this->user->getId();

		$data['Active'] = array('In-Active'	=> 'In-Active',
							  'Active'	=> 'Active');

		if (isset($this->request->post['isActive'])) {
			$data['isActive'] = $this->request->post['isActive'];
		} elseif (!empty($category_info)) {
			$data['isActive'] = $category_info['isActive'];
		} else {
			$data['isActive'] = 'Active';
		}
		$data['token'] = $this->session->data['token'];

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/doctor', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/doctor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->request->post['doctor_name'] == ''){
			$this->error['valierr_doctor_name'] = 'Please Select Doctor Name!';
		}

		// if(($this->request->post['doctor_name'] ) == ($this->request->post['parent'] )) {
		// 	$this->request->post['parent'] = '';
		// }

		// else
		// {
		// 	$this->request->post['valierr_parent'] = 'Please Enter Assistant Too!';	
		// }

		if (($this->request->post['doctor_code'] != '')  && (!isset($this->request->get['id'])) ) {
			$avail_code = $this->db->query("SELECT * FROM doctor WHERE `doctor_code` = '".$this->request->post['doctor_code']."' ");
			if ($avail_code->num_rows > 0) {
				$this->error['valierr_doctor_code'] = 'This Code Is Already In Used! Please Select Another!';
			}
		}
		
		if($this->request->post['doctor_code'] == ''){
			$this->error['valierr_doctor_code'] = 'Please Select Doctor Code!';
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/doctor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/doctor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function DoctorIdGenerate(){
		$json = array();
		if (isset($this->request->get['filter_doctor_name'])) {
			$this->load->model('catalog/doctor');
			$last = $this->db->query("SELECT * FROM `doctor` WHERE alpha ='".$this->request->get['filter_doctor_name']."' ORDER BY id DESC LIMIT 1");
		
		if($last->num_rows > 0){
			$doc_code_no = $last->row['alpha_no'] + 1;
			$json['doc_code'] = $last->row['alpha'].''.$doc_code_no;
			$json['alpha'] = $last->row['alpha'];
			$json['alpha_no'] = $doc_code_no;
	} else {
		$num = 1;
		$json['doc_code'] = strtoupper($this->request->get['filter_doctor_name']).''.$num;
		$json['alpha']    = strtoupper($this->request->get['filter_doctor_name']);
		$json['alpha_no'] = $num;

	}

		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_doctor_name'])) {
			$this->load->model('catalog/doctor');

			$results = $this->model_catalog_doctor->getDoctorAuto($this->request->get['filter_doctor_name']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_name'     => strip_tags(html_entity_decode($result['doctor_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_name'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}

	public function autocompletes() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_doctor_code'])) {
			$this->load->model('catalog/doctor');

			$results = $this->model_catalog_doctor->getDoctorAutos($this->request->get['filter_doctor_code']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'doctor_code'     => strip_tags(html_entity_decode($result['doctor_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_code'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}

	public function autocompleteparent() {
		$json = array();
		 // echo '<pre>';
		 // print_r($this->request->get);
		 // exit;
		if (isset($this->request->get['parent'])) {
			$this->load->model('catalog/doctor');

			$filter_data = array(
				'filter_parent' => $this->request->get['parent'],
				//'start'       => 0,
				//'limit'       => 5
			);
			$results = $this->model_catalog_doctor->getsupplier($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'doctor_name'     => strip_tags(html_entity_decode($result['doctor_name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['doctor_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
