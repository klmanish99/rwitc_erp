<?php
//require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
//use Dompdf\Dompdf;
class Controllercataloghorseatglance extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('transaction/ownership_shift_module');
		$this->document->setTitle('Horse At Glance');
		$this->load->model('transaction/ownership_shift_module');
		$this->getList();
		//$this->rating();
	}

	public function add() {
		$this->load->language('transaction/ownership_shift_module');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/ownership_shift_module');

		// echo '<pre>';
		// print_r($this->request->post);
		// exit;
		if (($this->request->server['REQUEST_METHOD'] == 'POST')/* && $this->validateForm()*/) {
			$this->model_transaction_ownership_shift_module->addProspectus($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if($this->request->post['check_provi_owner'] == 'on'){
				$this->response->redirect($this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'], true));
			} else {				$this->response->redirect($this->url->link('transaction/horse_datas_link', 'token=' . $this->session->data['token'] .'&horse_id=' . $this->request->post['filterHorseId'].'&color=1&order=1', true));
			}
		}

		$this->getForm();
	}

	public function rating() { 
		$sql = "SELECT * FROM entry_horse WHERE pros_id = 2 ORDER BY `horse_rating`  DESC ";
		$horse_data = $this->db->query($sql)->rows;
		
		$max ="SELECT MAX(horse_rating) AS max FROM entry_horse WHERE pros_id = 2 ";
		$max1 = $this->db->query($max)->row;
		$min ="SELECT horse_rating  FROM entry_horse WHERE pros_id = 2 ORDER BY horse_rating ASc";
		$min1 = $this->db->query($min)->rows;
		foreach ($min1 as $key => $value) {
			$sql =("SELECT Rating From horse_weight WHERE Class ='Class II' AND Rating ='".$value['horse_rating']."' ");
			$query = $this->db->query($sql);
			if($query->num_rows > 0){
				$min_value= $value['horse_rating'];
				break;
			} 
		}
		$class1 ='Class II';
		$top_rating=$max1['max'];
		$bot_rating=$min_value;
		// $class1 ='Class V';
		// $top_rating=8;
		// $bot_rating=12;
		$this->load->model('transaction/ownership_shift_module');
		$result =$this->model_transaction_ownership_shift_module->rating($class1,$top_rating,$bot_rating);
		echo '<pre>';
		print_r($result);
		exit;
	}

	public function edit() {
		$this->load->language('transaction/ownership_shift_module');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/ownership_shift_module');
		if (($this->request->server['REQUEST_METHOD'] == 'POST')/* && $this->validateForm()*/) {
			$this->model_transaction_ownership_shift_module->editProspectus($this->request->get['pros_id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit1() { 
		$json = array();
		if (isset($this->request->get['pros_id'])) {
			$this->load->model('transaction/ownership_shift_module');
			$json = $this->model_transaction_ownership_shift_module->getpopupdata($this->request->get['pros_id']);
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function voidbutton() {
		if (isset($this->request->post)) {
			$void=$this->request->post;
			if(isset($void['shift_void_data'])){
				foreach ($void['shift_void_data'] as $key => $value) {
					$this->db->query("UPDATE prospectus SET race_status = 0 WHERE pros_id='".$value['pros_id']."'");
				}
			}
		}
		$json='';
		// $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function shiftbutton() {
		$date = date('Y-m-d', strtotime($this->request->post['date_changed']));
		// 	//$date=$this->request->post['date_changed'];
		// 	$date = date('Y-m-d', strtotime($this->request->post['date_changed']));
		if (isset($this->request->post)) {
			$date_shift=$this->request->post;
			if(isset($date_shift['shift_date'])){
				foreach ($date_shift['shift_date'] as $key => $value) {
					$this->db->query("UPDATE prospectus SET race_date = '".$date."' WHERE pros_id='".$value['pros_id']."'");
				}
			}
		}
		$json='';
		// $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function shiftracebutton() {
		if (isset($this->request->post)) {
			foreach ($this->request->post['shift_void_data'] as $skey => $svalue) {
				$data1  = ("SELECT * FROM `prospectus` WHERE pros_id='".$svalue['pros_id']."'");
				$final_data = $this->db->query($data1)->row;
				$data2[] = array(
					'pros_id' => $final_data['pros_id'],
					'race_name'        => $final_data['race_name'],
					'race_type'        => $final_data['race_type'],
					'race_date'        => date('d-m-Y', strtotime($final_data['race_date'])),
				);
			}
			$json = $data2;
			$this->response->setOutput(json_encode($json));
		}
	}
		
	public function selectedfunction11() {
		$string_from_array =$this->request->get['pros_id'];
		$data= preg_split("/[,]/",$string_from_array);
		if (isset($this->request->get['pros_id'])) {
			foreach ($data as $key => $value) {
				$data1  = ("SELECT * FROM `prospectus` WHERE pros_id='".$value."'");
				$final_data = $this->db->query($data1)->row;
				$data2[] = array(
					'pros_id' => $final_data['pros_id'],
					'race_name'        => $final_data['race_name'],
					'race_type'        => $final_data['race_type']
				);
			}
			$json = $data2;
			$this->response->setOutput(json_encode($json));
		}
	}

	public function delete() {
		$this->load->language('transaction/ownership_shift_module');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/ownership_shift_module');
		
		if (isset($this->request->post['selected'])/* && $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $pros_id) {
				$this->model_transaction_ownership_shift_module->deleteprospectus($pros_id);
			}
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getList();
	}

	public function repair() {
		$this->load->language('transaction/ownership_shift_module');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/ownership_shift_module');
		if ($this->validateRepair()) {
			$this->model_transaction_ownership_shift_module->repairCategories();
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'], true));
		}
		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['filter_race_name'])) {
			$data['filter_race_name'] =$this->request->get['filter_race_name'];
		} else{
			$data['filter_race_name']  ="";
		}

		if(isset($this->request->get['filter_acceptance_date'])){
			$data['filter_acceptance_date'] = $this ->request->get['filter_acceptance_date'];
		}
		 else{
			$data['filter_acceptance_date'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('transaction/ownership_shift_module/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('transaction/ownership_shift_module/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('transaction/ownership_shift_module/repair', 'token=' . $this->session->data['token'] . $url, true);


		if (!isset($this->request->get['pros_id'])) {
			$data['action'] = $this->url->link('transaction/ownership_shift_module/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('transaction/ownership_shift_module/edit', 'token=' . $this->session->data['token'] . '&pros_id=' . $this->request->get['pros_id'] . $url, true);
		}

		$data['categories'] = array();

		$data['ownerships_types'] = array(
			'Sale'  => 'Sale',
			'Lease'  => 'Lease',
			'Sub Lease'  => 'Sub Lease',
		);

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_race_name' => $data['filter_race_name'],
			'filter_acceptance_date' => $data['filter_acceptance_date']
		);

		if (isset($this->request->get['horse_id'])) {
			$horse_datass = $this->db->query("SELECT `official_name`, `horseseq` FROM `horse1` WHERE horseseq = '".$this->request->get['horse_id']."'")->row;
			$traniner_datass = $this->db->query("SELECT `trainer_name`, `trainer_id` FROM `horse_to_trainer` WHERE horse_id = '".$this->request->get['horse_id']."'")->row;

			$data['horse_name'] = $horse_datass['official_name'];
			$data['horse_id'] = $horse_datass['horseseq'];
			$data['trainer_name'] = $traniner_datass['trainer_name'];
			$data['trainer_id'] = $traniner_datass['trainer_id'];
			$data['gt_id'] = 1;
		} else {
			$data['horse_name'] = '';
			$data['horse_id'] = '';
			$data['trainer_name'] = '';
			$data['trainer_id'] = '';
			$data['gt_id'] = 0;
		}

		if(isset($this->request->post['fromdate'])) {
			$data['fromdate'] = $this->request->post['fromdate'];
		} else {
			$data['fromdate'] = date('d-m-Y');
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		if (isset($this->request->post['selected1'])) {
			$data['selected1'] = (array)$this->request->post['selected1'];
		} else {
			$data['selected1'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/horse_at_glance', $data));
	}
	

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'transaction/ownership_shift_module')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['linkedOwnerName'] as $key => $value) {
			if ($value['linked_owner_id'] == '') {
				$this->error['linkedOwner'] = 'Please select valid linked owner.';
			}
		}

		if ($this->request->post['gstType'] == 'Registered' && $this->request->post['gstNo'] == '') {
			$this->error['gst_number'] = 'GST No can not be blank.';
		}

		foreach ($this->request->post['owner_shared_color'] as $key => $value) {
			if ($value['shared_owner_id'] == '') {
				$this->error['sharedColor'] = 'Please select valid owner to share color.';
			}
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'transaction/ownership_shift_module')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'transaction/ownership_shift_module')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}



	public function autocompleteHorse() {
		//echo 'in';
		$json = array();

		$this->load->model('catalog/horse');
		if (isset($this->request->get['horse_name'])) {

			$results = $this->model_catalog_horse->getHorsesAuto($this->request->get['horse_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'horse_id' => $result['horseseq'],
						'horse_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		} elseif(isset($this->request->get['old_horse_name'])) {

			

			$sql =("SELECT changehorse_name,horseseq FROM horse1 WHERE 1 = 1");
			if($this->request->get['old_horse_name'] != ''){
				$sql .= " AND `changehorse_name` LIKE '%" . $this->db->escape($this->request->get['old_horse_name']) . "%'";
			}
			$sql .= " ORDER BY `changehorse_name` ASC LIMIT 0, 100";

			$query = $this->db->query($sql)->rows;
				

			if($query){
				foreach ($query as $result) {
					$json[] = array(
						'horse_id' => $result['horseseq'],
						'horse_name'        => strip_tags(html_entity_decode($result['changehorse_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}

		} elseif(isset($this->request->get['mare_name'])) {

			

			$sql =("SELECT dam_name FROM horse1 WHERE 1 = 1");
			if($this->request->get['mare_name'] != ''){
				$sql .= " AND `dam_name` LIKE '%" . $this->db->escape($this->request->get['mare_name']) . "%'";
			}
			$sql .= " ORDER BY `dam_name` ASC LIMIT 0, 100";

			$query = $this->db->query($sql)->rows;
				

			if($query){
				foreach ($query as $result) {
					$json[] = array(
						'dam_name'        => strip_tags(html_entity_decode($result['dam_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}

		}

		/*echo '<pre>';print_r($json);
			exit;*/
		// $sort_order = array();

		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['horse_name'];
		// }

		// array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}


	public function getAllHorseOfMare(){
		$json = array();

		$this->load->model('catalog/horse');
		if(isset($this->request->get['mare_name'])) {

			$sql =("SELECT official_name,horseseq, sire_name, dam_name, color, sex, age, foal_date  FROM horse1 WHERE 1 = 1");
			if($this->request->get['mare_name'] != ''){
				$sql .= " AND `dam_name` = '" . $this->db->escape($this->request->get['mare_name']) . "'";
			}
			$sql .= " ORDER BY `dam_name` ASC";

			$query = $this->db->query($sql)->rows;
				
			$html = '';

			if($query){
				$html .= '<table class="table table-bordered ">';
				$html .= '<thead>';
				$html .= '<tr>';
					$html .= '<td style="text-align: left;vertical-align: middle;">Horse Name</td>';
					$html .= '<td style="text-align: left;vertical-align: middle;">Color</td>';
					$html .= '<td style="text-align: left;vertical-align: middle;">Sex</td>';
					$html .= '<td style="text-align: left;vertical-align: middle;">Age</td>';
					$html .= '<td style="text-align: left;vertical-align: middle;">Foal Date</td>';
					$html .= '<td style="text-align: left;vertical-align: middle;">Sire Name</td>';
					$html .= '<td style=text-align: left;vertical-align: middle;">Dame Name</td>';
					
				$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';
				$i = 0;
				foreach ($query as $result) {
					// echo'<pre>';
					// print_r($result);
					// exit;
					$hrf = "index.php?route=catalog/horse/view&token=".$this->session->data['token']."&horse_id=".$result['horseseq'];

					$html .= '<tr id="tr_'.$i.'">';
						$html .= '<td><a href="'.$hrf.'">'.$result['official_name'].'</a> </td>';
						$html .= '<td> '.$result['color'].'</td>';
						$html .= '<td>'.$result['sex'].' </td>';
						$html .= '<td>'.$result['age'].' </td>';
						$html .= '<td>'.$result['foal_date'].' </td>';
						$html .= '<td> '.$result['sire_name'].'</td>';
						$html .= '<td>'.$result['dam_name'].' </td>';
					$html .= '</tr>';

					$i++;

					// $json[] = array(
					// 	'horse_id' => $result['horseseq'],
					// 	'official_name'        => strip_tags(html_entity_decode($result['official_name'], ENT_QUOTES, 'UTF-8')),
					// 	'color' => $result['color'],
					// 	'sex' => $result['sex'],
					// 	'age' => $result['age'],
					// 	'foal_date' => $result['foal_date'],
					// 	'dam_name'        => strip_tags(html_entity_decode($result['dam_name'], ENT_QUOTES, 'UTF-8')),
					// 	'sire_name'        => strip_tags(html_entity_decode($result['sire_name'], ENT_QUOTES, 'UTF-8')),
					// );
				}
				$html .= '</tbody>';
				$html .= '</table>';
			}
		}

		$json['html'] = $html;

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));

	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('transaction/ownership_shift_module');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'owner_Id'	  => $this->request->get['owner_Id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_ownership_shift_module->getOwners($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'owner_id' => $result['id'],
					'owner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['owner_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autoHorseToTrainer() {
		$json = array();
		if (isset($this->request->get['horse_id'])) {
			$this->load->model('transaction/ownership_shift_module');
			$filter_data = array(
				'horse_id' => $this->request->get['horse_id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_transaction_ownership_shift_module->gethorsetoTrainer($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'trainer_id' => $result['trainer_id'],
					'trainer_code' => $result['trainer_code'],
					'trainer_name'        => strip_tags(html_entity_decode($result['trainer_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getOwnerData() {
		$json = '';

		//echo "<pre>";print_r($this->request->get);
		if (isset($this->request->get['filter_hourse_id']) && isset($this->request->get['ownership_type_id'])) {
			$this->load->model('transaction/ownership_shift_module');
			$filter_data = array(
				'horse_id' => $this->request->get['filter_hourse_id'],
				'ownership_type_id' => $this->request->get['ownership_type_id'],
			);
			$results = $this->model_transaction_ownership_shift_module->gethorsetoOwner($filter_data);

			//echo "<pre>";print_r($results);exit;
			$html = '';
			
			if($results){
				$html .= '<table class="table table-bordered ownertable">';
				$html .= '<thead>';
				$html .= '<tr>';
					$html .= '<td style="width: 40px;text-align: center;"></td>';
					$html .= '<td style="text-align: center;vertical-align: middle;">Name</td>';
					$html .= '<td style="text-align: center;vertical-align: middle;">Type <br> (O / L / SL)</td>';
					$html .= '<td style="text-align: center;vertical-align: middle;">Period</td>';

					$html .= '<td style="width: 160px;text-align: center;vertical-align: middle;">Share</td>';
					
					$html .= '<td style="width: 100px;text-align: center;vertical-align: middle;">Color</td>';
					$html .= '<td style="width: 200px;text-align: center;vertical-align: middle;">Contingency</td>';
					$html .= '<td style="width: 300px;text-align: center;vertical-align: middle;">Ownership</td>';
					
				$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';

				$i=1;
				$parent_data =array();
				foreach ($results as $result) {
					$lease_table_data = $this->db->query("SELECT * FROM `horse_to_owner_lease` WHERE child_trans_id ='".$result['horse_to_owner_id']."'");
					if($lease_table_data->num_rows > 0){
						$parent_data = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$lease_table_data->row['parent_trans_id']."'")->row;
					}

					$owners_partner = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$result['to_owner_id']."'");
					$partner_status = ($owners_partner->num_rows > 0) ? "1" : "0";

					$checked_color = '';
					$html .= '<tr>';
					$html .= '<td class="r_'.$i.'" style="text-align: center;">';
						if($result['ownership_type'] == 'Sale' && ($this->request->get['ownership_type_id'] == 'Lease' || $this->request->get['ownership_type_id'] == 'Sale')){
							$html .= '<input type="checkbox" name="owner_datas['.$i.'][share_saler]"  class="checkbox_value ent-evnt" id="fro_ow_ckper_'.$i.'" />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
						} else if($result['ownership_type'] == 'Lease' && $this->request->get['ownership_type_id'] == 'Sub Lease'){
							$html .= '<input type="checkbox" name="owner_datas['.$i.'][share_saler]"  class="checkbox_value ent-evnt" id="fro_ow_ckper_'.$i.'" />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
						} else {
							$html .= '<input type="checkbox" name="owner_datas['.$i.'][share_saler]"  class="checkbox_value ent-evnt" id="fro_ow_ckper_'.$i.'" onclick="return false;" aria-disabled/>';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][horse_to_owner_id]" value="'.$result['horse_to_owner_id'].'"  id="fro_ow_ck_'.$i.'"  />';
							$html .= '<input type="hidden" name="owner_datas['.$i.'][to_owner_id]" value="'.$result['to_owner_id'].'"  id="to_owner_id_'.$i.'"  />';
						}
					$html .= '</td>';


					$from_date = date('d-m-Y', strtotime($result['date_of_ownership']));
					$to_date = date('d-m-Y', strtotime($result['end_date_of_ownership']));

					$html .= '<td class="r_'.$i.'" style="text-align: left;">'; 
						if($partner_status == 1) {
							$html .= '<span style="cursor:pointer"><a id="partners_'.$result['to_owner_id'].'"  class="partners" >'.$result['to_owner'].'</a></span><br>';
						}else {
							$html .= '<span >'.$result['to_owner'].'</span><br>';
						}
							$html .= '<input type="hidden" name="owner_datas['.$i.'][from_owner_hidden]" value="'.$result['to_owner_id'].'" class="ent-evnt" id="owner_id_'.$i.'"  />';
					$html .= '</td>';

					$html .= '<td class="r_'.$i.'" style="text-align: center;">';  
						$provisional_ownership  = ($result['provisional_ownership'] == 'Yes') ? 'Provisional' : "";
							
						if($result['ownership_type'] == 'Lease'){
							$html .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> L  </span>';
						} elseif($result['ownership_type'] == 'Sub Lease'){
								
							$html .= '<span style="font-size:12px;">  <b>'.$provisional_ownership.'</b> SL  </span>';
						} else {
							$html .= '<span style="font-size:12px;"> <b>'.$provisional_ownership.'</b> O </span>';
						}
					$html .= '</td>';

					$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
							
						if($result['ownership_type'] == 'Lease'){
							$html .= '<span style="font-size:12px;"> '.$from_date. ' - ' .$to_date.'</span>';
						} elseif($result['ownership_type'] == 'Sub Lease'){
								
							$html .= '<span style="font-size:12px;">'.$from_date. ' - ' .$to_date.'</span>';
						} else {
							$html .= '<span style="font-size:12px;"> '.$from_date.'</span>';
						}
					$html .= '</td>';
					


					$html .= '<td class="r_'.$i.'" style="text-align: right;">';  
						$html .= '<span>'.$result['owner_percentage'].'%'.'</span>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_percentage]" value="'.$result['owner_percentage'].'" id="owner_percentage_'.$i.'"  />';
					$html .= '</td>';

					
					$html .= '<td class="r_'.$i.'" style="text-align: center;">';
					if($result['owner_color'] == 'Yes'){
						$html .= '<i class="fa fa-check" aria-hidden="true" style="color: green;font-size:1.4em;"></i>';
						//$html .= '<input  type="checkbox" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'" checked="checked"/>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'" checked="checked"  />';

					} else {
						//$html .= '<input  type="checkbox" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'"/>';
						$html .= '<input type="hidden" name="owner_datas['.$i.'][owner_color]" value="'.$result['owner_color'].'" class="checkbox_color ent-evnt" id="owner_color_'.$i.'"  />';
					} 
					$html .= '</td>';
					$contengency = ($result['contengency'] == 1) ? "Yes" : "N/A";
					$cont_percentage = ($result['cont_percentage'] != 0) ? $result['cont_percentage'] : "";
					$cont_amount = ($result['cont_amount'] != 0) ? $result['cont_amount'] : "0";
					$win_gross_stake = ($result['win_gross_stake'] == 1) ? "Yes" : "N/A";
					$win_net_stake = ($result['win_net_stake'] == 1) ? "Yes" : "N/A";
					$cont_place = ($result['cont_place'] == 1) ? "Yes" : "N/A";
					$millionover = ($result['millionover'] == 1) ? "Yes" : "N/A";
					$millionover_amt = ($result['millionover_amt'] != 0) ? $result['millionover_amt'] : "0";
					$grade1 = ($result['grade1'] == 1) ? "Yes" : "N/A";
					$grade2 = ($result['grade2'] == 1) ? "Yes" : "N/A";
					$grade3 = ($result['grade3'] == 1) ? "Yes" : "N/A";


					$html .= '<td class="r_'.$i.'" style="text-align: center;">';
						if($contengency == 'Yes'){
							$html .= '<span style="cursor:pointer"><a class="modalopen"  id="details_'.$result['horse_to_owner_id'].'" > '.$cont_percentage.'% / Rs.'.$cont_amount.' </a></span><br>';
						}
					$html .= '</td>';
						
					if($parent_data){
						if($parent_data['to_owner'] != '' && $result['ownership_type'] != 'Sale'){
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								if($result['ownership_type'] == 'Lease'){
									$html .= '<span>'.$parent_data['to_owner'].' ( In the case of Lease )</span>';
								} else {
									$html .= '<span>'.$parent_data['to_owner'].' ( In the case of Sub-Lease )</span>';

								}
								$html .= '<input type="hidden" name="owner_datas['.$i.'][parent_to_owner]" value="'.$parent_data['to_owner_id'].'" id="parent_to_owner_'.$i.'"  />';
							$html .= '</td>';
						}else {
							$html .= '<td class="r_'.$i.'" style="text-align: left;">';  
								$html .= '<span> </span>';
								$html .= '<input type="hidden" name="owner_datas['.$i.'][parent_to_owner]" value="0" id="parent_to_owner_'.$i.'"  />';
							$html .= '</td>';
						}
					} else {
						$html .= '<td class="r_'.$i.'" style="text-align: left;">';
						$html .= '</td>';
					}
					$i++;
				}
				$html .= '</tbody>';
				$html .= '</table>';
				$json['new_user'] = 0;
			} else {

				$json['new_user'] = 1;
			}
		}
		// echo'<pre>';
		// print_r($json);
		// exit;
		$json['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getContaingencyData(){
		
		$html = '';
		$json = array();
		$containgency_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_to_owner_id ='".$this->request->get['horse_owner_id']."'");
		// echo'<pre>';
		// print_r($containgency_datas);
		// exit;
		if($containgency_datas->num_rows > 0){
			$containgency_data = $containgency_datas->row;
			$contengency = ($containgency_data['contengency'] == 1) ? "Yes" : "N/A";
			$cont_percentage = ($containgency_data['cont_percentage'] != 0) ? $containgency_data['cont_percentage'] : "";
			$cont_amount = ($containgency_data['cont_amount'] != 0) ? $containgency_data['cont_amount'] : "0";
			$win_gross_stake = ($containgency_data['win_gross_stake'] == 1) ? "Yes" : "N/A";
			$win_net_stake = ($containgency_data['win_net_stake'] == 1) ? "Yes" : "N/A";
			$cont_place = ($containgency_data['cont_place'] == 1) ? "Yes" : "N/A";
			$millionover = ($containgency_data['millionover'] == 1) ? "Yes" : "N/A";
			$millionover_amt = ($containgency_data['millionover_amt'] != 0) ? $containgency_data['millionover_amt'] : "0";
			$grade1 = ($containgency_data['grade1'] == 1) ? "Yes" : "N/A";
			$grade2 = ($containgency_data['grade2'] == 1) ? "Yes" : "N/A";
			$grade3 = ($containgency_data['grade3'] == 1) ? "Yes" : "N/A";

			$html .= '<div class="col-sm-10>';
				$html .= '<div class="row">';
				$html .= '<span style="font-size:16px;"><b>Name : </b> '.$containgency_data['to_owner'].'</span><br><br>';

					$html .= '<div class="col-sm-6">';
						$html .= '<span style="font-size: 14px;"> <b> Contingency :</b> '.$contengency .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Amount :</b> Rs.'.$cont_amount .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Win Net Stake :</b> '.$win_net_stake .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Millionover :</b> '.$millionover .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Grade 1 :</b> '.$grade1 .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Grade 3 :</b> '.$grade3 .'</span> <br>';
					$html .= '</div>';

					$html .= '<div class="col-sm-6">';
						$html .= '<span style="font-size: 14px;"> <b> Percentage :</b> '.$cont_percentage .'% </span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Win Gross Stake :</b> '.$win_gross_stake .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Place :</b> '.$cont_place .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Millionover Amt :</b> Rs.'.$millionover_amt .'</span> <br>';
						$html .= '<span style="font-size: 14px;"> <b> Grade 2 :</b> '.$grade2 .'</span> <br>';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
		}
		$json = $html;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getPartnersData(){

		$html = '';
		$json = array();
		$partners_datasss = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id ='".$this->request->get['parent_owner_id']."'");
		$owner_datasss = $this->db->query("SELECT `id`,`owner_name` FROM `owners` WHERE id ='".$this->request->get['parent_owner_id']."'")->row;

		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		if($partners_datasss->num_rows > 0){

			$html .= '<div class="modal fade" id="partners_data_'.$this->request->get['parent_owner_id'].'" role="dialog">';
                $html .= '<div class="modal-dialog">';
                     $html .= '<div class="modal-content">';
                        $html .= '<div class="modal-header">';
                            $html .= '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                            $html .= '<h4 class="modal-title">Partners Details</h4>';
                        $html .= '</div>';
                        $html .= '<div class="modal-body partners_div" style="height: 440px;">';
							$html .= '<table class="table table-bordered partner_tbl" id="tableid_'.$this->request->get['parent_owner_id'].'">';
								$html .= '<thead>';
								$html .= '<tr>';
									$html .= '<td style="width: 40px;text-align: center;"></td>';
									$html .= '<td style="text-align: center;vertical-align: middle;">Partners Name</td>';
								$html .= '</tr>';
								$html .= '</thead>';
								$html .= '<tbody>';
									$html .= '<span style="font-size:16px;"><b>Name : </b> '.$owner_datasss['owner_name'].'</span><br><br>';
									foreach ($partners_datasss->rows as $key => $value) {
										$represntative_datasss = $this->db->query("SELECT `represntative_id` FROM `horse_to_owner` WHERE `to_owner_id` ='".$this->request->get['parent_owner_id']."' AND `horse_id` ='".$this->request->get['horse_id']."' AND `owner_share_status` = '1' AND `represntative_id` = '".$value['partner_id']."' ");

										$repres_status = ($represntative_datasss->num_rows > 0) ? "1" : "0";

										$html .= '<tr>';
											$html .= '<td class="p_'.$key.'" style="text-align: center;">';
												if($repres_status == 1){
													$html .= '<input type="checkbox" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][is_reprentative]"  class="parent_value" id="owner_partner_id_'.$key.'" checked="checked />';

												} else {

													$html .= '<input type="checkbox" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][is_reprentative]"  class="parent_value" id="owner_partner_id_'.$key.'" />';
												}
												$html .= '<input type="hidden" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][partner_id]" value="'.$value['partner_id'].'"  id="partner_id_'.$key.'"  />';
												$html .= '<input type="hidden" name="partners_datas['.$this->request->get['parent_owner_id'].']['.$key.'][owner_id]" value="'.$owner_datasss['id'].'"  id="partner_id_'.$key.'"  />';

											$html .= '</td>';
											$html .= '<td class="p_'.$key.'" style="text-align: left;">';
												$html .= '<span style="font-size: 14px;"> '.$value['partner_name'] .'</span>';
											$html .= '</td>';
										$html .= '</tr>';
									}
								$html .= '</tbody>';
							$html .= '</table>';
						$html .= '</div>';
                        $html .= '<div class="modal-footer">';
                            $html .= '<button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>';
                        $html .= '</div>';
                    $html .= '</div>';
                $html .= '</div>';
           $html .=  '</div>';
		}
		$json = $html;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));


	}

	public function autocompleteOwners(){
		$json = array();
		if (isset($this->request->get['owner_name'])) {
			$this->load->model('transaction/ownership_shift_module');
			$filter_data = array(
				'owner_name' => $this->request->get['owner_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);


			$results = $this->model_transaction_ownership_shift_module->getAllOwners($filter_data);

			$strings= '';
			$alluser_ids = explode(',',$this->request->get['pre_owners']);
			$added_ids = explode(',',$this->request->get['added_ids']);
			$updated_ids = '';
			foreach ($alluser_ids as $akey => $avalue) {
				if(!in_array($avalue, $added_ids)){
					if($avalue != ''){
						$updated_ids .="'" . $avalue. "'".','; 
					}
				}
			}

			$strings = rtrim($updated_ids, ',');
			if($strings != ''){
				$all_allowners = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_ids']."' AND owner_share_status = '1' AND to_owner_id NOT IN ($strings) ")->rows;
			} else {
				$all_allowners = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_id = '".$this->request->get['horse_ids']."' AND owner_share_status = '1' ")->rows;
			}
			
			$final_allowners = array();
			foreach ($all_allowners as $key => $value) {
				$final_allowners[] = $value['to_owner_id']; 
			}

			
			foreach ($results as $result) {
				if(!in_array($result['id'], $final_allowners) && !in_array($result['id'], $added_ids)){

					$partner_datasz = $this->db->query("SELECT * FROM `owners_partner` WHERE owner_id = '".$result['id']."'");
					$status = ($partner_datasz->num_rows > 0) ? "1" : "0";
					$json[] = array(
						'owner_id' => $result['id'],
						'owner_code' => $result['owner_code'],
						'status' => $status,
						'owner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}




	public function report() {
		if (isset($this->request->get['filter_race_date'])) {
			$race_date = date('Y-m-d', strtotime($this->request->get['filter_race_date']));
		}

		if (isset($this->request->get['filter_race_name'])) {
			$race_name = $this->request->get['filter_race_name'];
		}

		$data['report_data'] = array();

		$data['horse_data'] = array();

		$sql = "SELECT * FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id) WHERE entry.entry_id ";

		if (!empty($race_name)) {
			$sql .= " AND prospectus.race_name LIKE '%" . $race_name . "%'";
		}

		if (!empty($race_date)) {
			$sql .= " AND prospectus.race_date LIKE '%" . $race_date . "%'";
		}

		$report_data = $this->db->query($sql)->rows;

		if (!empty($report_data)) {

			foreach ($report_data as $key => $value) {
				$this->db->query("UPDATE `prospectus` SET `handicap_status` = '1' WHERE `pros_id` = '".$value['pros_id']."' ");

				$race_date = $value['race_date'];
				$race_dates = date('d-F-Y', strtotime($race_date));
				$final_race_date = explode('-', $race_dates);
				$race_date_day  = $final_race_date[0];
				$race_date_month = $final_race_date[1];
				$race_date_year  = $final_race_date[2];
				$race_day = date('l-F-Y', strtotime($race_date));
				$final_race_day = explode('-', $race_day);
				$race_date_days  = $final_race_day[0];
				$sql = "SELECT * FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ORDER BY `horse_rating`  DESC ";
				$horse_data = $this->db->query($sql)->rows;
				
				$max ="SELECT MAX(horse_rating) AS max FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ";
				$max1 = $this->db->query($max)->row;
				$min ="SELECT horse_rating  FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ORDER BY horse_rating ASC";
				$min1 = $this->db->query($min)->rows;
				foreach ($min1 as $mkey => $mvalue) {
					$sql =("SELECT Rating From horse_weight WHERE Class ='".$value['class']."' AND Rating ='".$mvalue['horse_rating']."' ");
					$query = $this->db->query($sql);
					if($query->num_rows > 0){
						$min_value= $mvalue['horse_rating'];
						break;
					} 
				}
				$class1 = $value['class'];
				$top_rating=$max1['max'];
				$bot_rating=$min_value;
				
				$this->load->model('transaction/ownership_shift_module');
				$result =$this->model_transaction_ownership_shift_module->rating($class1,$top_rating,$bot_rating);

				if($result['res_n'] !=''){
					$final_min_value=$result['res_n'];
				}else if($result['res_p'] !=''){
					$final_min_value=$result['res_p'];
				}
				else {
					$final_min_value='';
				}
				
				$acceptance_date = $value['acceptance_date'];
				$acceptance_dates = date('d-F-Y', strtotime($acceptance_date));
				$final_acceptance_date = explode('-', $acceptance_dates);
				$acceptance_date_day  = $final_acceptance_date[0];
				$acceptance_date_month = $final_acceptance_date[1];
				$acceptance_date_year  = $final_acceptance_date[2];
				$acceptance_day= date('l-F-Y', strtotime($acceptance_date));
				$final_acceptance_day = explode('-', $acceptance_day);
				$acceptance_date_days  = $final_acceptance_day[0];
				$acceptance_date_times = $value['acceptance_date_time'];
				$final_acceptance_date_times = str_replace(".", ":", $acceptance_date_times);

				$declaration_date = $value['declaration_date'];
				$declaration_dates = date('d-F-Y', strtotime($declaration_date));
				$final_declaration_date = explode('-', $declaration_dates);
				$declaration_date_day  = $final_declaration_date[0];
				$declaration_date_month = $final_declaration_date[1];
				$declaration_date_year  = $final_declaration_date[2];
				$declaration_day= date('l-F-Y', strtotime($declaration_date));
				$final_declaration_day = explode('-', $declaration_day);
				$declaration_date_days  = $final_declaration_day[0];
				$declaration_date_times = $value['declaration_date_time'];
				$final_declaration_date_times = str_replace(".", ":", $declaration_date_times);
				
				$data['report_data'][] = array(
					'race_name' => $value['race_name'],
					'race_description' => $value['race_description'],
					'distance' => $value['distance'],
					'season' => $value['season'],
					'year' => $value['year'],
					'race_day' => $value['race_day'],
					'race_date_days' => $race_date_days,
					'race_date_day' => $race_date_day,
					'race_date_month' => $race_date_month,
					'race_date_year' => $race_date_year,
					'race_name' => $value['race_name'],
					'race_description' => $value['race_description'],
					'final_acceptance_date_times' => $final_acceptance_date_times,
					'acceptance_date_days' => $acceptance_date_days,
					'acceptance_date_day' => $acceptance_date_day,
					'acceptance_date_month' => $acceptance_date_month,
					'acceptance_date_year' => $acceptance_date_year,
					'final_declaration_date_times' => $final_declaration_date_times,
					'declaration_date_days' => $declaration_date_days,
					'declaration_date_day' => $declaration_date_day,
					'declaration_date_month' => $declaration_date_month,
					'declaration_date_year' => $declaration_date_year,
				);
				foreach ($horse_data as $hkey => $hvalue) {
					if($hvalue['weight'] !=''){
						$weight = $hvalue['weight'] + $final_min_value;
					}else{
						$weight = '';
					}
					$data['horse_data'][] = array(
						'horse_rating' => $hvalue['horse_rating'],
						'horse_name' => $hvalue['horse_name'],
						'weight' => $weight,
					);
				}
			}
		}

		$data['base'] = HTTP_SERVER;
 		$html = $this->load->view('catalog/all_report_html', $data);
		$filename = 'Handicap Report.html';
		header('Content-type: text/html');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $html;exit;
	}


	public function reports() {

		$sql = "SELECT * FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id) WHERE entry.entry_id = '".$this->request->get['entry_id']."' ";

		$report_data = $this->db->query($sql)->rows;

		if (!empty($report_data)) {

			foreach ($report_data as $key => $value) {
				$this->db->query("UPDATE `prospectus` SET `handicap_status` = '1' WHERE `pros_id` = '".$value['pros_id']."' ");
				$race_date = $value['race_date'];
				$race_dates = date('d-F-Y', strtotime($race_date));
				$final_race_date = explode('-', $race_dates);
				$race_date_day  = $final_race_date[0];
				$race_date_month = $final_race_date[1];
				$race_date_year  = $final_race_date[2];
				$race_day = date('l-F-Y', strtotime($race_date));
				$final_race_day = explode('-', $race_day);
				$race_date_days  = $final_race_day[0];
				$sql = "SELECT * FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ORDER BY `horse_rating`  DESC ";
				$horse_data = $this->db->query($sql)->rows;

				$max ="SELECT MAX(horse_rating) AS max FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ";
				$max1 = $this->db->query($max)->row;
				$min ="SELECT horse_rating  FROM entry_horse WHERE pros_id = '".$value['pros_id']."' ORDER BY horse_rating ASC";
				$min1 = $this->db->query($min)->rows;
				foreach ($min1 as $mkey => $mvalue) {
					$sql =("SELECT Rating From horse_weight WHERE Class ='".$value['class']."' AND Rating ='".$mvalue['horse_rating']."' ");
					$query = $this->db->query($sql);
					if($query->num_rows > 0){
						$min_value= $mvalue['horse_rating'];
						break;
					} 
				}
				$class1 = $value['class'];
				$top_rating=$max1['max'];
				$bot_rating=$min_value;
				
				$this->load->model('transaction/ownership_shift_module');
				$result =$this->model_transaction_ownership_shift_module->rating($class1,$top_rating,$bot_rating);

				if($result['res_n'] !=''){
					$final_min_value=$result['res_n'];
				}else if($result['res_p'] !=''){
					$final_min_value=$result['res_p'];
				}
				else {
					$final_min_value='';
				}

				$acceptance_date = $value['acceptance_date'];
				$acceptance_dates = date('d-F-Y', strtotime($acceptance_date));
				$final_acceptance_date = explode('-', $acceptance_dates);
				$acceptance_date_day  = $final_acceptance_date[0];
				$acceptance_date_month = $final_acceptance_date[1];
				$acceptance_date_year  = $final_acceptance_date[2];
				$acceptance_day= date('l-F-Y', strtotime($acceptance_date));
				$final_acceptance_day = explode('-', $acceptance_day);
				$acceptance_date_days  = $final_acceptance_day[0];
				$acceptance_date_times = $value['acceptance_date_time'];
				$final_acceptance_date_times = str_replace(".", ":", $acceptance_date_times);

				$declaration_date = $value['declaration_date'];
				$declaration_dates = date('d-F-Y', strtotime($declaration_date));
				$final_declaration_date = explode('-', $declaration_dates);
				$declaration_date_day  = $final_declaration_date[0];
				$declaration_date_month = $final_declaration_date[1];
				$declaration_date_year  = $final_declaration_date[2];
				$declaration_day= date('l-F-Y', strtotime($declaration_date));
				$final_declaration_day = explode('-', $declaration_day);
				$declaration_date_days  = $final_declaration_day[0];
				$declaration_date_times = $value['declaration_date_time'];
				$final_declaration_date_times = str_replace(".", ":", $declaration_date_times);
				
				$data['report_data'][] = array(
					'race_name' => $value['race_name'],
					'race_description' => $value['race_description'],
					'distance' => $value['distance'],
					'season' => $value['season'],
					'year' => $value['year'],
					'race_day' => $value['race_day'],
					'race_date_days' => $race_date_days,
					'race_date_day' => $race_date_day,
					'race_date_month' => $race_date_month,
					'race_date_year' => $race_date_year,
					'race_name' => $value['race_name'],
					'race_description' => $value['race_description'],
					'final_acceptance_date_times' => $final_acceptance_date_times,
					'acceptance_date_days' => $acceptance_date_days,
					'acceptance_date_day' => $acceptance_date_day,
					'acceptance_date_month' => $acceptance_date_month,
					'acceptance_date_year' => $acceptance_date_year,
					'final_declaration_date_times' => $final_declaration_date_times,
					'declaration_date_days' => $declaration_date_days,
					'declaration_date_day' => $declaration_date_day,
					'declaration_date_month' => $declaration_date_month,
					'declaration_date_year' => $declaration_date_year,
				);

				foreach ($horse_data as $hkey => $hvalue) {
					if($hvalue['weight'] !=''){
						$weight = $hvalue['weight'] + $final_min_value;
					}else{
						$weight = '';
					}
					$data['horse_data'][] = array(
						'horse_rating' => $hvalue['horse_rating'],
						'horse_name' => $hvalue['horse_name'],
						'weight' => $weight,
					);
				}
			}
		}
			
 		$data['base'] = HTTP_SERVER;
 		$html = $this->load->view('catalog/all_report_html', $data);
		$filename = 'Handicap Report.html';
		header('Content-type: text/html');	
		header('Content-Disposition: attachment; filename='.$filename);
		echo $html;exit;
	}

	public function redirect() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		
		$url = '';

		if (isset($this->request->get['horse_id'])) {
			$horse_id .= '&horse_id=' . $this->request->get['horse_id'];
		}
		$this->response->redirect($this->url->link('catalog/horse/view', 'token=' . $this->session->data['token'] . $url, true));
	}
}
