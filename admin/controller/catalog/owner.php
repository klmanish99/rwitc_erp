<?php
class Controllercatalogowner extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/owner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/owner');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/owner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/owner');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo "<pre>"; print_r($this->request->post);exit;
			$this->model_catalog_owner->addOwner($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		//echo "<pre>";print_r($this->request->post);exit;

		$this->load->language('catalog/owner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/owner');

		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				//echo "<pre>"; print_r($this->request->post);exit;
				$this->model_catalog_owner->editOwner($this->request->get['ownerId'], $this->request->post);

				$this->session->data['success'] = $this->language->get('text_success');

				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				$this->response->redirect($this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function view() {
		//echo "<pre>";print_r($this->request->post);exit;

		$this->load->language('catalog/owner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/owner');

		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				//echo "<pre>"; print_r($this->request->post);exit;
				$this->model_catalog_owner->editOwner($this->request->get['ownerId'], $this->request->post);

				$this->session->data['success'] = $this->language->get('text_success');

				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				$this->response->redirect($this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getFormview();
	}

	public function delete() {
		//echo "dle";exit;
		$this->load->language('catalog/owner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/owner');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $owner_id) {
				$this->model_catalog_owner->deleteOwner($owner_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function repair() {
		$this->load->language('catalog/owner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/owner');

		if ($this->validateRepair()) {
			$this->model_catalog_owner->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('catalog/owner', 'token=' . $this->session->data['token'], true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['token'] = $this->session->data['token'];

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/owner/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/owner/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/owner/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		//echo "<pre>"; print_r($this->request->get);exit;

		if (isset($this->request->get['filter_owner_name'])) {
			$filter_owner_name = $this->request->get['filter_owner_name'];
			$data['filter_owner_name'] = $this->request->get['filter_owner_name'];
		}
		else{
			$filter_owner_name = '';
			$data['filter_owner_name'] = '';
		}

		if (isset($this->request->get['filter_owner_id'])) {
			$filter_owner_ids = $this->request->get['filter_owner_id'];
			$data['filter_owner_id'] = $this->request->get['filter_owner_id'];
			$filter_owner_id =substr($filter_owner_ids,1);
		}
		else{
			$filter_owner_ids = '';
			$filter_owner_id = '';
			$data['filter_owner_id'] = '';
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = '1';
			$data['filter_status'] = '1';
		}

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_owner_name'	=>	$filter_owner_name,
			'filter_owner_id'	=>	$filter_owner_id,
			'filter_status'	=>	$filter_status
		);

		$category_total = $this->model_catalog_owner->getTotalCategories($filter_data);

		$results = $this->model_catalog_owner->getOwners($filter_data);
		$today = date("Y-m-d");

		$data['status'] =array();
		foreach ($results as $result) {
			$no_of_horse = $this->db->query("SELECT COUNT(from_owner_id) AS own_id FROM horse_to_owner WHERE from_owner_id = '".$result['id']."' AND end_date_of_ownership > '".$today."' AND `owner_share_status` ='1' ");
			if ($no_of_horse->num_rows >0) {
				$fcount = $no_of_horse->row['own_id'];
			} else {
				$fcount = 0;
			}
			$no_of_horses = $this->db->query("SELECT COUNT(to_owner_id) AS own_ids FROM horse_to_owner WHERE to_owner_id = '".$result['id']."' AND end_date_of_ownership > '".$today."' AND `owner_share_status` ='1' ");
			if ($no_of_horses->num_rows >0) {
				$tcount = $no_of_horses->row['own_ids'];
			} else {
				$tcount = 0;
			}


			$ban_amount = $this->db->query("SELECT amount,start_date,end_date FROM `owners_ban` WHERE `owner_id` = '".$result['id']."' AND `end_date` >= '". $today ."' ORDER BY end_date DESC ");
			$ban_amounts = '';
			if ($ban_amount->num_rows >0) {
				foreach ($ban_amount->rows as $akey => $avalue) {
					$ban_amounts .= '<b>'.'DATE:'.'</b>'.date('d-m-Y', strtotime($avalue['start_date'])).' '.'TO'.' '.date('d-m-Y', strtotime($avalue['end_date'])).' '.'<b>'.'Amt:'.'</b>'.$avalue['amount'].'<br/>';
				}
				
			}




			$total_horse = $fcount+$tcount;
			//echo "<pre>"; print_r($result);
			$data['ownersDatas'][] = array(
				'ownerId' 	  	=> $result['owner_code'],
				'ownerName'   	=> $result['owner_name'],
				'panNumber'	  	=> $result['pan_number'],
				'mobileNo'	  	=> $result['mobile_no_1'],
				'gst_type'	  	=> $result['gst_type'],
				'emailId'	  	=> $result['email_id'],
				'ownershipType'	=> $result['type_of_ownership'],
				'approvalDate'	=> $result['approval_date'],
				'count' 		=> $total_horse,
				'ownerStatus' 	=> $result['active'],
				'ban_amount' 	=> $ban_amounts,
				'edit'        	=> $this->url->link('catalog/owner/edit', 'token=' . $this->session->data['token'] . '&ownerId=' . $result['id'] . $url, true),
				'view'        	=> $this->url->link('catalog/owner/view', 'token=' . $this->session->data['token'] . '&ownerId=' . $result['id'] . $url, true)
			);
		} //exit; 

		$data['status'] =array(
				'1'  =>"In",
				'0'  =>'Exit'
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_view'] = $this->language->get('button_view');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['linkedOwner'])) {
			$data['error_linkedOwner'] = $this->error['linkedOwner'];
		} else {
			$data['error_linkedOwner'] = '';
		}

		if (isset($this->error['sharedColor'])) {
			$data['error_sharedColor'] = $this->error['sharedColor'];
		} else {
			$data['error_sharedColor'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sr_no'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=sr_no' . $url, true);
		$data['owner_name'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=owner_name' . $url, true);
		$data['type_of_ownership'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=type_of_ownership' . $url, true);
		$data['mobile_no'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=mobile_no_1' . $url, true);
		$data['email_id'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=email_id' . $url, true);
		$data['pan_no'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=pan_number' . $url, true);

		$data['gst_no'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=gst_no' . $url, true);
		
		$data['no_of_horse'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=t.no_of_horse' . $url, true);
		$data['statuses'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=t.status' . $url, true);
		$data['ban'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=ban' . $url, true);

		$data['sort_name'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/owner_list', $data));
	}

	public function getForm() {  //echo "<pre>";print_r($this->request->get);exit;

		

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['ownerId']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		$users_id = $this->session->data['user_id'];
		$user_group_ids = $this->db->query("SELECT * FROM oc_user WHERE user_group_id = '".$users_id."' ");
		if ($user_group_ids->num_rows > 0) {
			$data['group_id'] = $user_group_ids->row['user_group_id'];
		} else {
			$data['group_id'] = '';
		}

		if (isset($this->request->get['ownerId'])) {
			$data['ownerId'] = $this->request->get['ownerId'];
		}
		
		$data['ownerNamePrefixes'] = array(	'Mr'			=> 'Mr',
										    'Mrs'			=> 'Mrs',
										    'MS'			=> 'MS',
										    'Miss'			=> 'Miss',
										    'Master'		=> 'Master',
										    'Father'		=> 'Father',
										    'Reverend'		=> 'Reverend',
										    'Doctor'		=> 'Doctor',
										    'Honorable'		=> 'Honorable',
										    'Professor'		=> 'Professor',
										    'Officer'		=> 'Officer',
										    'Estate of late'=> 'Estate of late'
											);
									  

		$data['isTrainer'] = array('No'	=> 'No',
							  'Yes'	=> 'Yes');

		$data['Active'] = array('0'	=> 'Exit',
							  '1'	=> 'In');

		$data['ownershipType'] = array( 'Individual'	=> 'Individual',
								  		'Partnership Firm'	=> 'Partnership Firm',
								  		'Director'			=> 'Director',
								  		'Corporate'		=> 'Corporate',
								  		'LLP'			=> 'LLP',
								  		'Trust'			=> 'Trust',
								  		'Estate of late'=> 'Estate of late'
								  	);
		$data['approvedStatus'] = array( 'No'	=> 'No',
									'Yes'	=> 'Yes');

		$data['committeMember'] = array( 'No'	=> 'No',
									'Yes'	=> 'Yes');

		$data['isMember'] = array( 'No'	=> 'No',
							'Yes'	=> 'Yes');
		
		$data['isApproved'] = array( 'No'	=> 'No',
							  'Yes'	=> 'Yes');

		$data['isLinkedOwner'] = array( 'No'		=> 'No',
							       'Yes'	=> 'Yes');
		$relationshipz = $this->db->query("SELECT * FROM relationship WHERE 1=1 ")->rows;
		/*echo'<pre>';
		print_r($relationshipz);
		exit;*/
		$this->db->query("DELETE FROM relationship WHERE relationship = '' OR relationship = 'Other' ");
		$this->db->query("INSERT INTO relationship SET relationship = 'Other' ");

		foreach ($relationshipz as $value) {
			if ($value != '') {
				$data['relationshipArray'][] = $value['relationship'];
			}
			/*echo'<pre>';
			print_r($data['relationshipArray']);
			exit;*/
		}

		$data['memArray'] = array( 'No'	=> 'No',
							  'Yes'	=> 'Yes');

		$data['state_1'] = array('Andaman and Nicobar Islands'=> 'Andaman and Nicobar Islands',
							'Andhra Pradesh'	=>	'Andhra Pradesh',
							'Arunachal Pradesh'	=>	'Arunachal Pradesh',
							'Assam'				=>	'Assam',
							'Bihar'				=>	'Bihar',
							'Chandigarh'		=>	'Chandigarh',
							'Chhattisgarh'		=>	'Chhattisgarh',
							'Dadra and Nagar Haveli and Daman and Diu'=>'Dadra and Nagar Haveli and Daman and Diu',
							'Delhi'				=>	'Delhi',
							'Goa'				=>	'Goa',
							'Gujarat'			=>	'Gujarat',
							'Haryana'			=>	'Haryana',
							'Himachal Pradesh'	=>	'Himachal Pradesh',
							'Jammu and Kashmir'	=>	'Jammu and Kashmir',
							'Jharkhand'			=>	'Jharkhand',
							'Karnataka'			=>	'Karnataka',
							'Kerala'			=>	'Kerala',
							'Ladakh'			=>	'Ladakh',
							'Lakshadweep'		=>	'Lakshadweep',
							'Madhya Pradesh'	=>	'Madhya Pradesh',
							'Maharashtra'		=>	'Maharashtra',	
							'Manipur'			=>	'Manipur',		
							'Meghalaya'			=>	'Meghalaya',
							'Mizoram'			=>	'Mizoram',
							'Nagaland'			=>	'Nagaland',
							'Odisha'			=>	'Odisha',
							'Punjab'			=>	'Punjab',
							'Puducherry'		=>	'Puducherry',
							'Rajasthan'			=>	'Rajasthan',
							'Sikkim'			=>	'Sikkim',
							'Tamil Nadu'		=>	'Tamil Nadu',
							'Telangana'			=>	'Telangana',
							'Tripura'			=>	'Tripura',
							'Uttar Pradesh'		=>	'Uttar Pradesh',
							'Uttarakhand'		=>	'Uttarakhand',
							'West Bengal'		=>	'West Bengal'
							);
		$data['state_2'] = array('Andaman and Nicobar Islands'=> 'Andaman and Nicobar Islands',
							'Andhra Pradesh'	=>	'Andhra Pradesh',
							'Arunachal Pradesh'	=>	'Arunachal Pradesh',
							'Assam'				=>	'Assam',
							'Bihar'				=>	'Bihar',
							'Chandigarh'		=>	'Chandigarh',
							'Chhattisgarh'		=>	'Chhattisgarh',
							'Dadra and Nagar Haveli and Daman and Diu'=>'Dadra and Nagar Haveli and Daman and Diu',
							'Delhi'				=>	'Delhi',
							'Goa'				=>	'Goa',
							'Gujarat'			=>	'Gujarat',
							'Haryana'			=>	'Haryana',
							'Himachal Pradesh'	=>	'Himachal Pradesh',
							'Jammu and Kashmir'	=>	'Jammu and Kashmir',
							'Jharkhand'			=>	'Jharkhand',
							'Karnataka'			=>	'Karnataka',
							'Kerala'			=>	'Kerala',
							'Ladakh'			=>	'Ladakh',
							'Lakshadweep'		=>	'Lakshadweep',
							'Madhya Pradesh'	=>	'Madhya Pradesh',
							'Maharashtra'		=>	'Maharashtra',	
							'Manipur'			=>	'Manipur',		
							'Meghalaya'			=>	'Meghalaya',
							'Mizoram'			=>	'Mizoram',
							'Nagaland'			=>	'Nagaland',
							'Odisha'			=>	'Odisha',
							'Punjab'			=>	'Punjab',
							'Puducherry'		=>	'Puducherry',
							'Rajasthan'			=>	'Rajasthan',
							'Sikkim'			=>	'Sikkim',
							'Tamil Nadu'		=>	'Tamil Nadu',
							'Telangana'			=>	'Telangana',
							'Tripura'			=>	'Tripura',
							'Uttar Pradesh'		=>	'Uttar Pradesh',
							'Uttarakhand'		=>	'Uttarakhand',
							'West Bengal'		=>	'West Bengal'
							);

		$data['gst'] = array('Registered'=>'Registered',
						'Unregistered'=>'Unregistered'
						);

		$data['allowShareColorArray'] = array( 'No'	=> 'No',
										  'Yes'	=> 'Yes');

		$data['season_array'] = array( 'Mumbai' => 'Mumbai',
								 'Pune'	  => 'Pune');

		$data['clubs'] = array( 
			'RWITC'  => 'RWITC',
			'RCTC'  => 'RCTC',
			'DRC'  => 'DRC',
			'BTC'  => 'BTC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'MYRC'  => 'MYRC',
						);
		
		$data['banType'] = array('BTF'	=> 'BTF',
								 'UFL'	=> 'UFL'
							);

		$data['product_recurrings'] = array();
		$data['itrUploadDatas'] = array();
		$data['shareColorToOwnerDatas'] = array();
		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();
		$data['countries2'] = $this->model_localisation_country->getCountries();
		// echo'<pre>';
		// print_r($data['countries']);
		// exit;
		$data['zones'] = $this->model_localisation_country->getZone();
		$data['zones2'] = $this->model_localisation_country->getZone();
		/*echo'<pre>';
		print_r($data['zone']);
		exit;*/

		if (isset($this->request->post['address'])) {
			$data['addresses'] = $this->request->post['address'];
		} elseif (isset($this->request->get['customer_id'])) {
			$data['addresses'] = $this->model_customer_customer->getAddresses($this->request->get['customer_id']);
		} else {
			$data['addresses'] = array();
		}

		if (isset($this->error['valierr_start_date'])) {
			$data['valierr_start_date'] = $this->error['valierr_start_date'];
		} 

		if (isset($this->error['valierr_code'])) {
			$data['valierr_code'] = $this->error['valierr_code'];
		}


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['valierr_dateApproval'])) {
			$data['valierr_dateApproval'] = $this->error['valierr_dateApproval'];
		}

		if (isset($this->error['pan_errors'])) {
			$data['pan_errors'] = $this->error['pan_errors'];
		} else {
			$data['pan_errors'] = '';
		}

		if (isset($this->error['valierr_owner_name'])) {
			$data['valierr_owner_name'] = $this->error['valierr_owner_name'];
		}

		if (isset($this->error['valierr_ownerNamePrefix'])) {
			$data['valierr_ownerNamePrefix'] = $this->error['valierr_ownerNamePrefix'];
		}

		if (isset($this->error['color_date'])) {
			$data['color_date'] = $this->error['color_date'];
		}

		if (isset($this->error['linkedOwner'])) {
			$data['error_linkedOwner'] = $this->error['linkedOwner'];
		} else {
			$data['error_linkedOwner'] = '';
		}

		if (isset($this->error['partner_owner'])) {
			$data['error_partner_owner'] = $this->error['partner_owner'];
		} else {
			$data['error_partner_owner'] = '';
		}

		if (isset($this->error['gst_number'])) {
			$data['error_gst_number'] = $this->error['gst_number'];
		} else {
			$data['error_gst_number'] = '';
		}
		
		if (isset($this->error['sharedColor'])) {
			$data['error_sharedColor'] = $this->error['sharedColor'];
		} else {
			$data['error_sharedColor'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['error_status'])) {
			$data['error_status'] = $this->error['error_status'];
		} else {
			$data['error_status'] = '';
		}

		if (isset($this->error['remarks_errors'])) {
			$data['remarks_errors'] = $this->error['remarks_errors'];
		} else {
			$data['remarks_errors'] = '';
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['ownerId'])) {
			$data['action'] = $this->url->link('catalog/owner/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/owner/edit', 'token=' . $this->session->data['token'] . '&ownerId=' . $this->request->get['ownerId'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['date_filer'])) {
			$date_filer = $this->request->get['date_filer'];
			$data['active_style']= "active";
			$data['defalult_style']= "";
		} else {
			$data['defalult_style']= "active";
			$data['active_style']= "";
			$date_filer = '';
		}

		if (isset($this->request->get['ownerId'])) {
			$ownerId = $this->request->get['ownerId'];
		} else {
			$ownerId = '';
		}

		if (isset($this->request->get['date_filer']) ) {
			$url .= '&date_filer=' . $this->request->get['date_filer'];
		}

		if (isset($this->request->get['ownerId']) ) {
			$url .= '&ownerId=' . $this->request->get['ownerId'];
		}
		

		if (isset($this->request->get['ownerId']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$owner_info = $this->model_catalog_owner->getOwnersById($this->request->get['ownerId']);
			$data['linked_owner_query'] =	$owner_info['linkedOwnerQuery'];
			$data['itr_owner_query'] =	$owner_info['itrOwnerQuery'];
			$data['add_color'] =	$owner_info['colorQuery'];
			$data['colorHistoryQuery'] = $owner_info['colorHistoryQuery'];
			$data['owner_shared_color'] = $owner_info['owner_shared_color'];
			
			
			$data['owner_ban'] = $owner_info['owner_ban'];
			$data['owner_ban_history'] = $owner_info['owner_ban_history'];
			$data['owner_ids'] = $this->request->get['ownerId'];
			$horsewithowner_info = $this->model_catalog_owner->gethorseowner($ownerId,$date_filer);
			//echo "<pre>"; print_r($horsewithowner_info);exit;
		}

		if (isset($this->request->post['horsewithowner_info'])) {
			$data['horsewithowner_info'] = $this->request->post['horsewithowner_info'];
			$data['totalNoOfHorses'] = $this->request->post['totalNoOfHorses'];
		} elseif (!empty($owner_info)) {
			$data['horsewithowner_info'] = $horsewithowner_info;
			$data['totalNoOfHorses'] = count($horsewithowner_info);;
		} else {
			$data['totalNoOfHorses'] = 0;
			$data['horsewithowner_info'] = array();
		}

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['linkedOwnerName'])) {
			$data['linked_owner_query'] = $this->request->post['linkedOwnerName'];
		} elseif (!empty($owner_info)) {
			$data['linked_owner_query'] = $owner_info['linkedOwnerQuery'];
		} else {
			$data['linked_owner_query'] = array();
		}

		if (isset($this->request->post['partnersData'])) {
			$data['partnersData_query'] = $this->request->post['partnersData'];
		} elseif (!empty($owner_info)) {
			$data['partnersData_query'] = $owner_info['partnerOwnerQuery'];
		} else {
			$data['partnersData_query'] = array();
		}

		if (isset($this->request->post['itrDatas'])) {
			$data['itr_owner_query'] = $this->request->post['itrDatas'];
		} elseif (!empty($owner_info)) {
			$data['itr_owner_query'] = $owner_info['itrOwnerQuery'];
		} else {
			$data['itr_owner_query'] = array();
		}

		if (isset($this->request->post['add_color'])) {
			$data['colorQuery'] = $this->request->post['add_color'];
		} elseif (!empty($owner_info)) {
			$data['colorQuery'] = $owner_info['colorQuery'];
		} else {
			$data['colorQuery'] = array();
		}

		if (isset($this->request->post['owner_shared_color'])) {
			$data['owner_shared_color'] = $this->request->post['owner_shared_color'];
		} elseif (!empty($owner_info)) {
			$data['owner_shared_color'] = $owner_info['owner_shared_color'];
		} else {
			$data['owner_shared_color'] = array();
		}

		if (isset($this->request->post['file_number_uploads'])) {
			$data['file_number_uploads'] = $this->request->post['file_number_uploads'];
		} elseif (!empty($owner_info)) {
			$data['file_number_uploads'] = $owner_info['file_number_uploads'];
		} else {
			$data['file_number_uploads'] = '';
		}

		if (!empty($owner_info)) {
    		// $img = ltrim($owner_info['image_source'],"/");
    		if($owner_info['uploaded_file_sources'] != ''){
    			$data['img_path'] = $owner_info['uploaded_file_sources'];
    		} else {
    			$data['img_path'] = '1';
    		}
		} else {
      		$data['img_path'] = '#';
    	}
		
		if (isset($this->request->post['uploaded_file_sources'])) {
			$data['uploaded_file_sources'] = $this->request->post['uploaded_file_sources'];
		} elseif (!empty($owner_info)) {
			$data['uploaded_file_sources'] = $owner_info['uploaded_file_sources'];
		} else {
			$data['uploaded_file_sources'] = '';
		}


		// echo'<pre>';
		// print_r($data['owner_shared_color']);
		// exit;

		if (isset($this->request->post['ownerBan'])) {
			$data['owner_ban'] = $this->request->post['ownerBan'];
		} elseif (!empty($owner_info)) {
			$data['owner_ban'] = $owner_info['owner_ban'];
		} else {
			$data['owner_ban'] = array();
		}

		if (isset($this->request->post['ownerNamePrefix'])) {
			$data['owner_name_prefix'] = $this->request->post['ownerNamePrefix'];
		} elseif (!empty($owner_info)) {
			$data['owner_name_prefix'] = $owner_info['owner_name_prefix'];
		} else {
			$data['owner_name_prefix'] = '';
		}

		if (isset($this->request->post['owner_name'])) {
			$data['owner_name'] = $this->request->post['owner_name'];
		} elseif (!empty($owner_info)) {
			$data['owner_name'] = $owner_info['owner_name'];
		} else {
			$data['owner_name'] = '';
		}

		$owner_codes = $this->db->query("SELECT owner_code FROM owners WHERE 1=1 ORDER BY owner_code DESC LIMIT 1 ");
		/*echo'<pre>';
		print_r("SELECT med_code FROM medicine WHERE 1=1 ORDER BY id DESC LIMIT 1 ");
		exit;*/
		if ($owner_codes->num_rows > 0) {
			$owner_code = $owner_codes->row['owner_code'];
		} else {
			$owner_code = '';
		}

		if (isset($this->request->post['owner_code'])) {
			$data['owner_code'] = $this->request->post['owner_code'];
		} elseif (!empty($owner_info)) {
			$data['owner_code'] = $owner_info['owner_code'];
		} elseif (isset($this->request->post['hidden_owner_code'])) {
			$data['owner_code'] = $this->request->post['hidden_owner_code'];
		} else {
			$addOcode = $owner_code + 1;
			$data['owner_code'] = $addOcode;
		}

		if (isset($this->request->post['isTrainer'])) {
			$data['is_trainer'] = $this->request->post['isTrainer'];
		} elseif (!empty($owner_info)) {
			$data['is_trainer'] = $owner_info['is_trainer'];
		} else {
			$data['is_trainer'] = '';
		}

		if (isset($this->request->post['isActive'])) {
			$data['isActive'] = $this->request->post['isActive'];
		} elseif (!empty($owner_info)) {
			$data['isActive'] = $owner_info['active'];
		} else {
			$data['isActive'] = '1';
		}

		if (isset($this->request->post['hidden_active'])) {
			$data['isActive1'] = $this->request->post['hidden_active'];
		} elseif (!empty($owner_info)) {
			$data['isActive1'] = $owner_info['active'];
		} else {
			$data['isActive1'] = '';
		}



		// if (isset($this->request->post['isActive'])) {
		// 	$data['is_active'] = $this->request->post['isActive'];
		// } elseif (!empty($owner_info)) {
		// 	if ($owner_info['active'] == '1') {
		// 		$data['is_active'] = 'Active';
		// 	} else {
		// 		$data['is_active'] = 'In-Active';
		// 	}
		// } else {
		// 	$data['is_active'] = '';
		// }

		if (isset($this->request->post['dateApproval'])) {
			$data['approval_date'] = date('d-m-Y', strtotime($this->request->post['dateApproval']));
		} elseif (!empty($owner_info)) {
			if ($owner_info['approval_date'] == '1970-01-01') {
				$data['approval_date'] = '';
			} else {
				$data['approval_date'] = date('d-m-Y', strtotime($owner_info['approval_date']));
			}
		} else {
			$data['approval_date'] = '';
		}

		if (isset($this->request->post['raceCardName'])) {
			$data['race_card_name'] = $this->request->post['raceCardName'];
		} elseif (!empty($owner_info)) {
			$data['race_card_name'] = $owner_info['race_card_name'];
		} else {
			$data['race_card_name'] = '';
		}

		if (isset($this->request->post['WIRHOA'])) {
			$data['wirhoa'] = $this->request->post['WIRHOA'];
		} elseif (!empty($owner_info)) {
			$data['wirhoa'] = $owner_info['wirhoa'];
		} else {
			$data['wirhoa'] = 0;
		}
		
		if (isset($this->request->post['ownershipType'])) {
			$data['type_of_ownership'] = $this->request->post['ownershipType'];
		} elseif (!empty($owner_info)) {
			$data['type_of_ownership'] = $owner_info['type_of_ownership'];
		} else {
			$data['type_of_ownership'] = '';
		}

		if (isset($this->request->post['approved_status'])) {
			$data['approved_status'] = $this->request->post['approved_status'];
		} elseif (!empty($owner_info)) {
			$data['approved_status'] = $owner_info['approved_status'];
		} else {
			$data['approved_status'] = '';
		}
		
		if (isset($this->request->post['committeMember'])) {
			$data['cummitte_member'] = $this->request->post['committeMember'];
		} elseif (!empty($owner_info)) {
			$data['cummitte_member'] = $owner_info['cummitte_member'];
		} else {
			$data['cummitte_member'] = '';
		}

		if (isset($this->request->post['member'])) {
			$data['member'] = $this->request->post['member'];
		} elseif (!empty($owner_info)) {
			$data['member'] = $owner_info['member'];
		} else {
			$data['member'] = '';
		}

		if (isset($this->request->post['isLinkedOwner'])) {
			$data['linked_owners'] = $this->request->post['isLinkedOwner'];
		} elseif (!empty($owner_info)) {
			$data['linked_owners'] = $owner_info['linked_owners'];
		} else {
			$data['linked_owners'] = '';
		}

		if (isset($this->request->post['fileNumber'])) {
			$data['file_number'] = $this->request->post['fileNumber'];
		} elseif (!empty($owner_info)) {
			$data['file_number'] = $owner_info['file_number'];
		} else {
			$data['file_number'] = '';
		}

		if (isset($this->request->post['file_number_upload'])) {
			$data['file_number_upload'] = $this->request->post['file_number_upload'];
		} elseif (!empty($owner_info)) {
			$data['file_number_upload'] = $owner_info['file_number_upload'];
		} else {
			$data['file_number_upload'] = '';
		}
		
		if (isset($this->request->post['uploaded_file_source'])) {
			$data['uploaded_file_source'] = $this->request->post['uploaded_file_source'];
		} elseif (!empty($owner_info)) {
			$data['uploaded_file_source'] = $owner_info['uploaded_file_source'];
		} else {
			$data['uploaded_file_source'] = '';
		}

		if (isset($this->request->post['remark'])) {
			$data['remark'] = $this->request->post['remark'];
		} elseif (!empty($owner_info)) {
			$data['remark'] = $owner_info['REMARK'];
		} else {
			$data['remark'] = '';
		}

		if (isset($this->request->post['phoneNo'])) {
			$data['phone_no'] = $this->request->post['phoneNo'];
		} elseif (!empty($owner_info)) {
			$data['phone_no'] = $owner_info['phone_no'];
		} else {
			$data['phone_no'] = '';
		}

		if (isset($this->request->post['mobileNo1'])) {
			$data['mobile_no_1'] = $this->request->post['mobileNo1'];
		} elseif (!empty($owner_info)) {
			$data['mobile_no_1'] = $owner_info['mobile_no_1'];
		} else {
			$data['mobile_no_1'] = '';
		}
		
		if (isset($this->request->post['altMobileNo'])) {
			$data['alt_mobile_no'] = $this->request->post['altMobileNo'];
		} elseif (!empty($owner_info)) {
			$data['alt_mobile_no'] = $owner_info['alt_mobile_no'];
		} else {
			$data['alt_mobile_no'] = '';
		}

		if (isset($this->request->post['emailId'])) {
			$data['email_id'] = $this->request->post['emailId'];
		} elseif (!empty($owner_info)) {
			$data['email_id'] = $owner_info['email_id'];
		} else {
			$data['email_id'] = '';
		}

		if (isset($this->request->post['altEmailId'])) {
			$data['alt_email_id'] = $this->request->post['altEmailId'];
		} elseif (!empty($owner_info)) {
			$data['alt_email_id'] = $owner_info['alt_email_id'];
		} else {
			$data['alt_email_id'] = '';
		}

		if (isset($this->request->post['address1'])) {
			$data['address_1'] = $this->request->post['address1'];
		} elseif (!empty($owner_info)) {
			$data['address_1'] = $owner_info['address_1'];
		} else {
			$data['address_1'] = '';
		}

		if (isset($this->request->post['address2'])) {
			$data['address_2'] = $this->request->post['address2'];
		} elseif (!empty($owner_info)) {
			$data['address_2'] = $owner_info['address_2'];
		} else {
			$data['address_2'] = '';
		}

		if (isset($this->request->post['address_3'])) {
			$data['address_3'] = $this->request->post['address_3'];
		} elseif (!empty($owner_info)) {
			$data['address_3'] = $owner_info['address_3'];
		} else {
			$data['address_3'] = '';
		}

		if (isset($this->request->post['address_4'])) {
			$data['address_4'] = $this->request->post['address_4'];
		} elseif (!empty($owner_info)) {
			$data['address_4'] = $owner_info['address_4'];
		} else {
			$data['address_4'] = '';
		}

		if (isset($this->request->post['localArea1'])) {
			$data['local_area_1'] = $this->request->post['localArea1'];
		} elseif (!empty($owner_info)) {
			$data['local_area_1'] = $owner_info['local_area_1'];
		} else {
			$data['local_area_1'] = '';
		}

		if (isset($this->request->post['localArea2'])) {
			$data['local_area_2'] = $this->request->post['localArea2'];
		} elseif (!empty($owner_info)) {
			$data['local_area_2'] = $owner_info['local_area_2'];
		} else {
			$data['local_area_2'] = '';
		}

		if (isset($this->request->post['state1'])) {
			$data['state1'] = $this->request->post['state1'];
		} elseif (!empty($owner_info)) {
			$data['state1'] = $owner_info['state_1'];
		} else {
			$data['state1'] = '';
		}

		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = $this->request->post['zone_id'];
		} elseif (!empty($owner_info)) {
			$data['zone_id'] = $owner_info['state_1'];
		} else {
			$data['zone_id'] = 1493;
		}


		if (isset($this->request->post['zone_id2'])) {
			$data['zone_id2'] = $this->request->post['zone_id2'];
		} elseif (!empty($owner_info)) {
			$data['zone_id2'] = $owner_info['state_2'];
		} else {
			$data['zone_id2'] = 1493;
		}

		if (isset($this->request->post['city1'])) {
			$data['city_1'] = $this->request->post['city1'];
		} elseif (!empty($owner_info)) {
			$data['city_1'] = $owner_info['city_1'];
		} else {
			$data['city_1'] = '';
		}

		if (isset($this->request->post['state2'])) {
			$data['state2'] = $this->request->post['state2'];
		} elseif (!empty($owner_info)) {
			$data['state2'] = $owner_info['state_2'];
		} else {
			$data['state2'] = '';
		}

		if (isset($this->request->post['city2'])) {
			$data['city_2'] = $this->request->post['city2'];
		} elseif (!empty($owner_info)) {
			$data['city_2'] = $owner_info['city_2'];
		} else {
			$data['city_2'] = '';
		}

		if (isset($this->request->post['pincode1'])) {
			$data['pincode_1'] = $this->request->post['pincode1'];
		} elseif (!empty($owner_info)) {
			$data['pincode_1'] = $owner_info['pincode_1'];
		} else {
			$data['pincode_1'] = '';
		}

		if (isset($this->request->post['country1'])) {
			$data['country_1'] = $this->request->post['country1'];
		} elseif (!empty($owner_info)) {
			$data['country_1'] = $owner_info['country_1'];
		} else {
			$data['country_1'] = 99;
		}

		if (isset($this->request->post['pincode2'])) {
			$data['pincode_2'] = $this->request->post['pincode2'];
		} elseif (!empty($owner_info)) {
			$data['pincode_2'] = $owner_info['pincode_2'];
		} else {
			$data['pincode_2'] = '';
		}

		if (isset($this->request->post['country2'])) {
			$data['country_2'] = $this->request->post['country2'];
		} elseif (!empty($owner_info)) {
			$data['country_2'] = $owner_info['country_2'];
		} else {
			$data['country_2'] = 99;
		}
		
		if (isset($this->request->post['gstType'])) {
			$data['gst_type'] = $this->request->post['gstType'];
		} elseif (!empty($owner_info)) {
			$data['gst_type'] = $owner_info['gst_type'];
		} else {
			$data['gst_type'] = '';
		}

		if (isset($this->request->post['gstNo'])) {
			$data['gst_no'] = $this->request->post['gstNo'];
		} elseif (!empty($owner_info)) {
			$data['gst_no'] = $owner_info['gst_no'];
		} else {
			$data['gst_no'] = '';
		}

		if (isset($this->request->post['panNumber'])) {
			$data['pan_number'] = $this->request->post['panNumber'];
		} elseif (!empty($owner_info)) {
			$data['pan_number'] = $owner_info['pan_number'];
		} else {
			$data['pan_number'] = '';
		}

		if (isset($this->request->post['profTaxNoDetails'])) {
			//echo "<pre>"; print_r($this->request->post);exit;
			$data['prof_tax_no_details'] = $this->request->post['profTaxNoDetails'];
		} elseif (!empty($owner_info)) {
			//echo "<pre>"; print_r($owner_info);exit;
			$data['prof_tax_no_details'] = $owner_info['prof_tax_no_details'];
		} else {
			$data['prof_tax_no_details'] = '';
		}

		if (isset($this->request->post['start_date'])) {
			$data['start_date'] = date('d-m-Y', strtotime($this->request->post['start_date']));
		} elseif (isset($owner_info['colorQuery'][0]['start_date'])) {
			$data['start_date'] = date('d-m-Y', strtotime($owner_info['colorQuery'][0]['start_date']));
		} else {
			$data['start_date'] = '';
		}

		if (isset($this->request->post['end_date'])) {
			$data['end_date'] = date('d-m-Y', strtotime($this->request->post['end_date']));
		} elseif (isset($owner_info['colorQuery'][0]['end_date'])) {
			$data['end_date'] = date('d-m-Y', strtotime($owner_info['colorQuery'][0]['end_date']));
		} else {
			$data['end_date'] = '';
		}

		if (isset($this->request->post['season'])) {
			$data['season'] = $this->request->post['season'];
		} elseif (isset($owner_info['colorQuery'][0]['season'])) {
			$data['season'] = $owner_info['colorQuery'][0]['season'];
		} else {
			$data['season'] = '';
		}

		if (isset($this->request->post['allow_Share_Color'])) {
			$data['allow_Share_Color'] = $this->request->post['allow_Share_Color'];
		} elseif (isset($owner_info['colorQuery'][0]['allow_Share_Color'])) {
			$data['allow_Share_Color'] = $owner_info['colorQuery'][0]['allow_Share_Color'];
		} else {
			$data['allow_Share_Color'] = '';
		}



		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['date_filer'] = $date_filer;

		$this->response->setOutput($this->load->view('catalog/owner_form', $data));
	}

	public function getFormview() {  //echo "<pre>";print_r($this->request->get);exit;

		

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['ownerId']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		$users_id = $this->session->data['user_id'];
		$user_group_ids = $this->db->query("SELECT * FROM oc_user WHERE user_group_id = '".$users_id."' ");
		if ($user_group_ids->num_rows > 0) {
			$data['group_id'] = $user_group_ids->row['user_group_id'];
		} else {
			$data['group_id'] = '';
		}

		if (isset($this->request->get['ownerId'])) {
			$data['ownerId'] = $this->request->get['ownerId'];
		}
		
		$data['ownerNamePrefixes'] = array(	'Mr'			=> 'Mr',
										    'Mrs'			=> 'Mrs',
										    'MS'			=> 'MS',
										    'Miss'			=> 'Miss',
										    'Master'		=> 'Master',
										    'Father'		=> 'Father',
										    'Reverend'		=> 'Reverend',
										    'Doctor'		=> 'Doctor',
										    'Honorable'		=> 'Honorable',
										    'Professor'		=> 'Professor',
										    'Officer'		=> 'Officer',
										    'Estate of late'=> 'Estate of late'
											);
									  

		$data['isTrainer'] = array('No'	=> 'No',
							  'Yes'	=> 'Yes');

		$data['Active'] = array('0'	=> 'Exit',
							  '1'	=> 'In');

		$data['ownershipType'] = array( 'Individual'	=> 'Individual',
								  		'Partnership Firm'	=> 'Partnership Firm',
								  		'Director'			=> 'Director',
								  		'Corporate'		=> 'Corporate',
								  		'LLP'			=> 'LLP',
								  		'Trust'			=> 'Trust',
								  		'Estate of late'=> 'Estate of late'
								  	);
		$data['approvedStatus'] = array( 'No'	=> 'No',
									'Yes'	=> 'Yes');

		$data['committeMember'] = array( 'No'	=> 'No',
									'Yes'	=> 'Yes');

		$data['isMember'] = array( 'No'	=> 'No',
							'Yes'	=> 'Yes');
		
		$data['isApproved'] = array( 'No'	=> 'No',
							  'Yes'	=> 'Yes');

		$data['isLinkedOwner'] = array( 'No'		=> 'No',
							       'Yes'	=> 'Yes');
		$relationshipz = $this->db->query("SELECT * FROM relationship WHERE 1=1 ")->rows;
		/*echo'<pre>';
		print_r($relationshipz);
		exit;*/
		$this->db->query("DELETE FROM relationship WHERE relationship = '' OR relationship = 'Other' ");
		$this->db->query("INSERT INTO relationship SET relationship = 'Other' ");

		foreach ($relationshipz as $value) {
			if ($value != '') {
				$data['relationshipArray'][] = $value['relationship'];
			}
			/*echo'<pre>';
			print_r($data['relationshipArray']);
			exit;*/
		}

		$data['memArray'] = array( 'No'	=> 'No',
							  'Yes'	=> 'Yes');

		$data['state_1'] = array('Andaman and Nicobar Islands'=> 'Andaman and Nicobar Islands',
							'Andhra Pradesh'	=>	'Andhra Pradesh',
							'Arunachal Pradesh'	=>	'Arunachal Pradesh',
							'Assam'				=>	'Assam',
							'Bihar'				=>	'Bihar',
							'Chandigarh'		=>	'Chandigarh',
							'Chhattisgarh'		=>	'Chhattisgarh',
							'Dadra and Nagar Haveli and Daman and Diu'=>'Dadra and Nagar Haveli and Daman and Diu',
							'Delhi'				=>	'Delhi',
							'Goa'				=>	'Goa',
							'Gujarat'			=>	'Gujarat',
							'Haryana'			=>	'Haryana',
							'Himachal Pradesh'	=>	'Himachal Pradesh',
							'Jammu and Kashmir'	=>	'Jammu and Kashmir',
							'Jharkhand'			=>	'Jharkhand',
							'Karnataka'			=>	'Karnataka',
							'Kerala'			=>	'Kerala',
							'Ladakh'			=>	'Ladakh',
							'Lakshadweep'		=>	'Lakshadweep',
							'Madhya Pradesh'	=>	'Madhya Pradesh',
							'Maharashtra'		=>	'Maharashtra',	
							'Manipur'			=>	'Manipur',		
							'Meghalaya'			=>	'Meghalaya',
							'Mizoram'			=>	'Mizoram',
							'Nagaland'			=>	'Nagaland',
							'Odisha'			=>	'Odisha',
							'Punjab'			=>	'Punjab',
							'Puducherry'		=>	'Puducherry',
							'Rajasthan'			=>	'Rajasthan',
							'Sikkim'			=>	'Sikkim',
							'Tamil Nadu'		=>	'Tamil Nadu',
							'Telangana'			=>	'Telangana',
							'Tripura'			=>	'Tripura',
							'Uttar Pradesh'		=>	'Uttar Pradesh',
							'Uttarakhand'		=>	'Uttarakhand',
							'West Bengal'		=>	'West Bengal'
							);
		$data['state_2'] = array('Andaman and Nicobar Islands'=> 'Andaman and Nicobar Islands',
							'Andhra Pradesh'	=>	'Andhra Pradesh',
							'Arunachal Pradesh'	=>	'Arunachal Pradesh',
							'Assam'				=>	'Assam',
							'Bihar'				=>	'Bihar',
							'Chandigarh'		=>	'Chandigarh',
							'Chhattisgarh'		=>	'Chhattisgarh',
							'Dadra and Nagar Haveli and Daman and Diu'=>'Dadra and Nagar Haveli and Daman and Diu',
							'Delhi'				=>	'Delhi',
							'Goa'				=>	'Goa',
							'Gujarat'			=>	'Gujarat',
							'Haryana'			=>	'Haryana',
							'Himachal Pradesh'	=>	'Himachal Pradesh',
							'Jammu and Kashmir'	=>	'Jammu and Kashmir',
							'Jharkhand'			=>	'Jharkhand',
							'Karnataka'			=>	'Karnataka',
							'Kerala'			=>	'Kerala',
							'Ladakh'			=>	'Ladakh',
							'Lakshadweep'		=>	'Lakshadweep',
							'Madhya Pradesh'	=>	'Madhya Pradesh',
							'Maharashtra'		=>	'Maharashtra',	
							'Manipur'			=>	'Manipur',		
							'Meghalaya'			=>	'Meghalaya',
							'Mizoram'			=>	'Mizoram',
							'Nagaland'			=>	'Nagaland',
							'Odisha'			=>	'Odisha',
							'Punjab'			=>	'Punjab',
							'Puducherry'		=>	'Puducherry',
							'Rajasthan'			=>	'Rajasthan',
							'Sikkim'			=>	'Sikkim',
							'Tamil Nadu'		=>	'Tamil Nadu',
							'Telangana'			=>	'Telangana',
							'Tripura'			=>	'Tripura',
							'Uttar Pradesh'		=>	'Uttar Pradesh',
							'Uttarakhand'		=>	'Uttarakhand',
							'West Bengal'		=>	'West Bengal'
							);

		$data['gst'] = array('Registered'=>'Registered',
						'Unregistered'=>'Unregistered'
						);

		$data['allowShareColorArray'] = array( 'No'	=> 'No',
										  'Yes'	=> 'Yes');

		$data['season_array'] = array( 'Mumbai' => 'Mumbai',
								 'Pune'	  => 'Pune');

		$data['clubs'] = array( 
			'RWITC'  => 'RWITC',
			'RCTC'  => 'RCTC',
			'DRC'  => 'DRC',
			'BTC'  => 'BTC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'MYRC'  => 'MYRC',
						);
		
		$data['banType'] = array('BTF'	=> 'BTF',
								 'UFL'	=> 'UFL'
							);

		$data['product_recurrings'] = array();
		$data['itrUploadDatas'] = array();
		$data['shareColorToOwnerDatas'] = array();
		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();
		$data['countries2'] = $this->model_localisation_country->getCountries();
		// echo'<pre>';
		// print_r($data['countries']);
		// exit;
		$data['zones'] = $this->model_localisation_country->getZone();
		$data['zones2'] = $this->model_localisation_country->getZone();
		/*echo'<pre>';
		print_r($data['zone']);
		exit;*/

		if (isset($this->request->post['address'])) {
			$data['addresses'] = $this->request->post['address'];
		} elseif (isset($this->request->get['customer_id'])) {
			$data['addresses'] = $this->model_customer_customer->getAddresses($this->request->get['customer_id']);
		} else {
			$data['addresses'] = array();
		}

		if (isset($this->error['valierr_start_date'])) {
			$data['valierr_start_date'] = $this->error['valierr_start_date'];
		} 

		if (isset($this->error['valierr_code'])) {
			$data['valierr_code'] = $this->error['valierr_code'];
		}


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['valierr_dateApproval'])) {
			$data['valierr_dateApproval'] = $this->error['valierr_dateApproval'];
		}

		if (isset($this->error['pan_errors'])) {
			$data['pan_errors'] = $this->error['pan_errors'];
		} else {
			$data['pan_errors'] = '';
		}

		if (isset($this->error['valierr_owner_name'])) {
			$data['valierr_owner_name'] = $this->error['valierr_owner_name'];
		}

		if (isset($this->error['valierr_ownerNamePrefix'])) {
			$data['valierr_ownerNamePrefix'] = $this->error['valierr_ownerNamePrefix'];
		}

		if (isset($this->error['color_date'])) {
			$data['color_date'] = $this->error['color_date'];
		}

		if (isset($this->error['linkedOwner'])) {
			$data['error_linkedOwner'] = $this->error['linkedOwner'];
		} else {
			$data['error_linkedOwner'] = '';
		}

		if (isset($this->error['partner_owner'])) {
			$data['error_partner_owner'] = $this->error['partner_owner'];
		} else {
			$data['error_partner_owner'] = '';
		}

		if (isset($this->error['gst_number'])) {
			$data['error_gst_number'] = $this->error['gst_number'];
		} else {
			$data['error_gst_number'] = '';
		}
		
		if (isset($this->error['sharedColor'])) {
			$data['error_sharedColor'] = $this->error['sharedColor'];
		} else {
			$data['error_sharedColor'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['error_status'])) {
			$data['error_status'] = $this->error['error_status'];
		} else {
			$data['error_status'] = '';
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['ownerId'])) {
			$data['action'] = $this->url->link('catalog/owner/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/owner/edit', 'token=' . $this->session->data['token'] . '&ownerId=' . $this->request->get['ownerId'] . $url, true);
		}

		if (isset($this->request->get['back'])) {
			$data['cancel'] = $this->url->link('catalog/owner_at_glance', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['cancel'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'] . $url, true);
		}

		if (isset($this->request->get['date_filer'])) {
			$date_filer = $this->request->get['date_filer'];
			$data['active_style']= "active";
			$data['defalult_style']= "";
		} else {
			$data['defalult_style']= "active";
			$data['active_style']= "";
			$date_filer = '';
		}

		if (isset($this->request->get['ownerId'])) {
			$ownerId = $this->request->get['ownerId'];
		} else {
			$ownerId = '';
		}

		if (isset($this->request->get['date_filer']) ) {
			$url .= '&date_filer=' . $this->request->get['date_filer'];
		}

		if (isset($this->request->get['ownerId']) ) {
			$url .= '&ownerId=' . $this->request->get['ownerId'];
		}
		

		if (isset($this->request->get['ownerId']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$owner_info = $this->model_catalog_owner->getOwnersById($this->request->get['ownerId']);
			$data['linked_owner_query'] =	$owner_info['linkedOwnerQuery'];
			$data['itr_owner_query'] =	$owner_info['itrOwnerQuery'];
			$data['add_color'] =	$owner_info['colorQuery'];
			$data['colorHistoryQuery'] = $owner_info['colorHistoryQuery'];
			$data['owner_shared_color'] = $owner_info['owner_shared_color'];
			
			
			$data['owner_ban'] = $owner_info['owner_ban'];
			$data['owner_ban_history'] = $owner_info['owner_ban_history'];
			$data['owner_ids'] = $this->request->get['ownerId'];
			$horsewithowner_info = $this->model_catalog_owner->gethorseowner($ownerId,$date_filer);
			//echo "<pre>"; print_r($horsewithowner_info);exit;
		}

		if (isset($this->request->post['horsewithowner_info'])) {
			$data['horsewithowner_info'] = $this->request->post['horsewithowner_info'];
			$data['totalNoOfHorses'] = $this->request->post['totalNoOfHorses'];
		} elseif (!empty($owner_info)) {
			$data['horsewithowner_info'] = $horsewithowner_info;
			$data['totalNoOfHorses'] = count($horsewithowner_info);;
		} else {
			$data['totalNoOfHorses'] = 0;
			$data['horsewithowner_info'] = array();
		}

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['linkedOwnerName'])) {
			$data['linked_owner_query'] = $this->request->post['linkedOwnerName'];
		} elseif (!empty($owner_info)) {
			$data['linked_owner_query'] = $owner_info['linkedOwnerQuery'];
		} else {
			$data['linked_owner_query'] = array();
		}

		if (isset($this->request->post['partnersData'])) {
			$data['partnersData_query'] = $this->request->post['partnersData'];
		} elseif (!empty($owner_info)) {
			$data['partnersData_query'] = $owner_info['partnerOwnerQuery'];
		} else {
			$data['partnersData_query'] = array();
		}

		if (isset($this->request->post['itrDatas'])) {
			$data['itr_owner_query'] = $this->request->post['itrDatas'];
		} elseif (!empty($owner_info)) {
			$data['itr_owner_query'] = $owner_info['itrOwnerQuery'];
		} else {
			$data['itr_owner_query'] = array();
		}

		if (isset($this->request->post['add_color'])) {
			$data['colorQuery'] = $this->request->post['add_color'];
		} elseif (!empty($owner_info)) {
			$data['colorQuery'] = $owner_info['colorQuery'];
		} else {
			$data['colorQuery'] = array();
		}

		if (isset($this->request->post['owner_shared_color'])) {
			$data['owner_shared_color'] = $this->request->post['owner_shared_color'];
		} elseif (!empty($owner_info)) {
			$data['owner_shared_color'] = $owner_info['owner_shared_color'];
		} else {
			$data['owner_shared_color'] = array();
		}

		if (isset($this->request->post['file_number_uploads'])) {
			$data['file_number_uploads'] = $this->request->post['file_number_uploads'];
		} elseif (!empty($owner_info)) {
			$data['file_number_uploads'] = $owner_info['file_number_uploads'];
		} else {
			$data['file_number_uploads'] = '';
		}

		if (!empty($owner_info)) {
    		// $img = ltrim($owner_info['image_source'],"/");
    		if($owner_info['uploaded_file_sources'] != ''){
    			$data['img_path'] = $owner_info['uploaded_file_sources'];
    		} else {
    			$data['img_path'] = '1';
    		}
		} else {
      		$data['img_path'] = '#';
    	}
		
		if (isset($this->request->post['uploaded_file_sources'])) {
			$data['uploaded_file_sources'] = $this->request->post['uploaded_file_sources'];
		} elseif (!empty($owner_info)) {
			$data['uploaded_file_sources'] = $owner_info['uploaded_file_sources'];
		} else {
			$data['uploaded_file_sources'] = '';
		}


		// echo'<pre>';
		// print_r($data['owner_shared_color']);
		// exit;

		if (isset($this->request->post['ownerBan'])) {
			$data['owner_ban'] = $this->request->post['ownerBan'];
		} elseif (!empty($owner_info)) {
			$data['owner_ban'] = $owner_info['owner_ban'];
		} else {
			$data['owner_ban'] = array();
		}

		if (isset($this->request->post['ownerNamePrefix'])) {
			$data['owner_name_prefix'] = $this->request->post['ownerNamePrefix'];
		} elseif (!empty($owner_info)) {
			$data['owner_name_prefix'] = $owner_info['owner_name_prefix'];
		} else {
			$data['owner_name_prefix'] = '';
		}

		if (isset($this->request->post['owner_name'])) {
			$data['owner_name'] = $this->request->post['owner_name'];
		} elseif (!empty($owner_info)) {
			$data['owner_name'] = $owner_info['owner_name'];
		} else {
			$data['owner_name'] = '';
		}

		$owner_codes = $this->db->query("SELECT owner_code FROM owners WHERE 1=1 ORDER BY owner_code DESC LIMIT 1 ");
		/*echo'<pre>';
		print_r("SELECT med_code FROM medicine WHERE 1=1 ORDER BY id DESC LIMIT 1 ");
		exit;*/
		if ($owner_codes->num_rows > 0) {
			$owner_code = $owner_codes->row['owner_code'];
		} else {
			$owner_code = '';
		}

		if (isset($this->request->post['owner_code'])) {
			$data['owner_code'] = $this->request->post['owner_code'];
		} elseif (!empty($owner_info)) {
			$data['owner_code'] = $owner_info['owner_code'];
		} elseif (isset($this->request->post['hidden_owner_code'])) {
			$data['owner_code'] = $this->request->post['hidden_owner_code'];
		} else {
			$addOcode = $owner_code + 1;
			$data['owner_code'] = $addOcode;
		}

		if (isset($this->request->post['isTrainer'])) {
			$data['is_trainer'] = $this->request->post['isTrainer'];
		} elseif (!empty($owner_info)) {
			$data['is_trainer'] = $owner_info['is_trainer'];
		} else {
			$data['is_trainer'] = '';
		}

		if (isset($this->request->post['isActive'])) {
			$data['isActive'] = $this->request->post['isActive'];
		} elseif (!empty($owner_info)) {
			$data['isActive'] = $owner_info['active'];
		} else {
			$data['isActive'] = '1';
		}

		if (!empty($owner_info)) {
			$data['last_upadate'] = date("d-m-Y h:i:s", strtotime($owner_info['last_update_date']));
		} else {
			$data['last_upadate'] = '';
		}

		if (!empty($owner_info) && $owner_info['user_id'] != 0) {
			$user_name = $this->db->query("SELECT username FROM oc_user WHERE user_id = '".$owner_info['user_id']."' ")->row;
			$data['user'] = $user_name['username'] ;
		} else {
			$data['user'] = '';
		}

		if (isset($this->request->post['hidden_active'])) {
			$data['isActive1'] = $this->request->post['hidden_active'];
		} elseif (!empty($owner_info)) {
			$data['isActive1'] = $owner_info['active'];
		} else {
			$data['isActive1'] = '';
		}

		// if (isset($this->request->post['isActive'])) {
		// 	$data['is_active'] = $this->request->post['isActive'];
		// } elseif (!empty($owner_info)) {
		// 	if ($owner_info['active'] == '1') {
		// 		$data['is_active'] = 'Active';
		// 	} else {
		// 		$data['is_active'] = 'In-Active';
		// 	}
		// } else {
		// 	$data['is_active'] = '';
		// }

		if (isset($this->request->post['dateApproval'])) {
			$data['approval_date'] = date('d-m-Y', strtotime($this->request->post['dateApproval']));
		} elseif (!empty($owner_info)) {
			if ($owner_info['approval_date'] == '1970-01-01') {
				$data['approval_date'] = '';
			} else {
				$data['approval_date'] = date('d-m-Y', strtotime($owner_info['approval_date']));
			}
		} else {
			$data['approval_date'] = '';
		}

		if (isset($this->request->post['raceCardName'])) {
			$data['race_card_name'] = $this->request->post['raceCardName'];
		} elseif (!empty($owner_info)) {
			$data['race_card_name'] = $owner_info['race_card_name'];
		} else {
			$data['race_card_name'] = '';
		}

		if (isset($this->request->post['WIRHOA'])) {
			$data['wirhoa'] = $this->request->post['WIRHOA'];
		} elseif (!empty($owner_info)) {
			$data['wirhoa'] = $owner_info['wirhoa'];
		} else {
			$data['wirhoa'] = 0;
		}
		
		if (isset($this->request->post['ownershipType'])) {
			$data['type_of_ownership'] = $this->request->post['ownershipType'];
		} elseif (!empty($owner_info)) {
			$data['type_of_ownership'] = $owner_info['type_of_ownership'];
		} else {
			$data['type_of_ownership'] = '';
		}

		if (isset($this->request->post['approved_status'])) {
			$data['approved_status'] = $this->request->post['approved_status'];
		} elseif (!empty($owner_info)) {
			$data['approved_status'] = $owner_info['approved_status'];
		} else {
			$data['approved_status'] = '';
		}
		
		if (isset($this->request->post['committeMember'])) {
			$data['cummitte_member'] = $this->request->post['committeMember'];
		} elseif (!empty($owner_info)) {
			$data['cummitte_member'] = $owner_info['cummitte_member'];
		} else {
			$data['cummitte_member'] = '';
		}

		if (isset($this->request->post['member'])) {
			$data['member'] = $this->request->post['member'];
		} elseif (!empty($owner_info)) {
			$data['member'] = $owner_info['member'];
		} else {
			$data['member'] = '';
		}

		if (isset($this->request->post['isLinkedOwner'])) {
			$data['linked_owners'] = $this->request->post['isLinkedOwner'];
		} elseif (!empty($owner_info)) {
			$data['linked_owners'] = $owner_info['linked_owners'];
		} else {
			$data['linked_owners'] = '';
		}

		if (isset($this->request->post['fileNumber'])) {
			$data['file_number'] = $this->request->post['fileNumber'];
		} elseif (!empty($owner_info)) {
			$data['file_number'] = $owner_info['file_number'];
		} else {
			$data['file_number'] = '';
		}

		if (isset($this->request->post['file_number_upload'])) {
			$data['file_number_upload'] = $this->request->post['file_number_upload'];
		} elseif (!empty($owner_info)) {
			$data['file_number_upload'] = $owner_info['file_number_upload'];
		} else {
			$data['file_number_upload'] = '';
		}
		
		if (isset($this->request->post['uploaded_file_source'])) {
			$data['uploaded_file_source'] = $this->request->post['uploaded_file_source'];
		} elseif (!empty($owner_info)) {
			$data['uploaded_file_source'] = $owner_info['uploaded_file_source'];
		} else {
			$data['uploaded_file_source'] = '';
		}

		if (isset($this->request->post['remark'])) {
			$data['remark'] = $this->request->post['remark'];
		} elseif (!empty($owner_info)) {
			$data['remark'] = $owner_info['REMARK'];
		} else {
			$data['remark'] = '';
		}

		if (isset($this->request->post['phoneNo'])) {
			$data['phone_no'] = $this->request->post['phoneNo'];
		} elseif (!empty($owner_info)) {
			$data['phone_no'] = $owner_info['phone_no'];
		} else {
			$data['phone_no'] = '';
		}

		if (isset($this->request->post['mobileNo1'])) {
			$data['mobile_no_1'] = $this->request->post['mobileNo1'];
		} elseif (!empty($owner_info)) {
			$data['mobile_no_1'] = $owner_info['mobile_no_1'];
		} else {
			$data['mobile_no_1'] = '';
		}
		
		if (isset($this->request->post['altMobileNo'])) {
			$data['alt_mobile_no'] = $this->request->post['altMobileNo'];
		} elseif (!empty($owner_info)) {
			$data['alt_mobile_no'] = $owner_info['alt_mobile_no'];
		} else {
			$data['alt_mobile_no'] = '';
		}

		if (isset($this->request->post['emailId'])) {
			$data['email_id'] = $this->request->post['emailId'];
		} elseif (!empty($owner_info)) {
			$data['email_id'] = $owner_info['email_id'];
		} else {
			$data['email_id'] = '';
		}

		if (isset($this->request->post['altEmailId'])) {
			$data['alt_email_id'] = $this->request->post['altEmailId'];
		} elseif (!empty($owner_info)) {
			$data['alt_email_id'] = $owner_info['alt_email_id'];
		} else {
			$data['alt_email_id'] = '';
		}

		if (isset($this->request->post['address1'])) {
			$data['address_1'] = $this->request->post['address1'];
		} elseif (!empty($owner_info)) {
			$data['address_1'] = $owner_info['address_1'];
		} else {
			$data['address_1'] = '';
		}

		if (isset($this->request->post['address2'])) {
			$data['address_2'] = $this->request->post['address2'];
		} elseif (!empty($owner_info)) {
			$data['address_2'] = $owner_info['address_2'];
		} else {
			$data['address_2'] = '';
		}

		if (isset($this->request->post['address_3'])) {
			$data['address_3'] = $this->request->post['address_3'];
		} elseif (!empty($owner_info)) {
			$data['address_3'] = $owner_info['address_3'];
		} else {
			$data['address_3'] = '';
		}

		if (isset($this->request->post['address_4'])) {
			$data['address_4'] = $this->request->post['address_4'];
		} elseif (!empty($owner_info)) {
			$data['address_4'] = $owner_info['address_4'];
		} else {
			$data['address_4'] = '';
		}

		if (isset($this->request->post['localArea1'])) {
			$data['local_area_1'] = $this->request->post['localArea1'];
		} elseif (!empty($owner_info)) {
			$data['local_area_1'] = $owner_info['local_area_1'];
		} else {
			$data['local_area_1'] = '';
		}

		if (isset($this->request->post['localArea2'])) {
			$data['local_area_2'] = $this->request->post['localArea2'];
		} elseif (!empty($owner_info)) {
			$data['local_area_2'] = $owner_info['local_area_2'];
		} else {
			$data['local_area_2'] = '';
		}

		if (isset($this->request->post['state1'])) {
			$data['state1'] = $this->request->post['state1'];
		} elseif (!empty($owner_info)) {
			$data['state1'] = $owner_info['state_1'];
		} else {
			$data['state1'] = '';
		}

		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = $this->request->post['zone_id'];
		} elseif (!empty($owner_info)) {
			$data['zone_id'] = $owner_info['state_1'];
		} else {
			$data['zone_id'] = '';
		}


		if (isset($this->request->post['zone_id2'])) {
			$data['zone_id2'] = $this->request->post['zone_id2'];
		} elseif (!empty($owner_info)) {
			$data['zone_id2'] = $owner_info['state_2'];
		} else {
			$data['zone_id2'] = '';
		}

		if (isset($this->request->post['city1'])) {
			$data['city_1'] = $this->request->post['city1'];
		} elseif (!empty($owner_info)) {
			$data['city_1'] = $owner_info['city_1'];
		} else {
			$data['city_1'] = '';
		}

		if (isset($this->request->post['state2'])) {
			$data['state2'] = $this->request->post['state2'];
		} elseif (!empty($owner_info)) {
			$data['state2'] = $owner_info['state_2'];
		} else {
			$data['state2'] = '';
		}

		if (isset($this->request->post['city2'])) {
			$data['city_2'] = $this->request->post['city2'];
		} elseif (!empty($owner_info)) {
			$data['city_2'] = $owner_info['city_2'];
		} else {
			$data['city_2'] = '';
		}

		if (isset($this->request->post['pincode1'])) {
			$data['pincode_1'] = $this->request->post['pincode1'];
		} elseif (!empty($owner_info)) {
			$data['pincode_1'] = $owner_info['pincode_1'];
		} else {
			$data['pincode_1'] = '';
		}

		if (isset($this->request->post['country1'])) {
			$data['country_1'] = $this->request->post['country1'];
		} elseif (!empty($owner_info)) {
			$data['country_1'] = $owner_info['country_1'];
		} else {
			$data['country_1'] = '';
		}

		if (isset($this->request->post['pincode2'])) {
			$data['pincode_2'] = $this->request->post['pincode2'];
		} elseif (!empty($owner_info)) {
			$data['pincode_2'] = $owner_info['pincode_2'];
		} else {
			$data['pincode_2'] = '';
		}

		if (isset($this->request->post['country2'])) {
			$data['country_2'] = $this->request->post['country2'];
		} elseif (!empty($owner_info)) {
			$data['country_2'] = $owner_info['country_2'];
		} else {
			$data['country_2'] = '';
		}
		
		if (isset($this->request->post['gstType'])) {
			$data['gst_type'] = $this->request->post['gstType'];
		} elseif (!empty($owner_info)) {
			$data['gst_type'] = $owner_info['gst_type'];
		} else {
			$data['gst_type'] = '';
		}

		if (isset($this->request->post['gstNo'])) {
			$data['gst_no'] = $this->request->post['gstNo'];
		} elseif (!empty($owner_info)) {
			$data['gst_no'] = $owner_info['gst_no'];
		} else {
			$data['gst_no'] = '';
		}

		if (isset($this->request->post['panNumber'])) {
			$data['pan_number'] = $this->request->post['panNumber'];
		} elseif (!empty($owner_info)) {
			$data['pan_number'] = $owner_info['pan_number'];
		} else {
			$data['pan_number'] = '';
		}

		if (isset($this->request->post['profTaxNoDetails'])) {
			//echo "<pre>"; print_r($this->request->post);exit;
			$data['prof_tax_no_details'] = $this->request->post['profTaxNoDetails'];
		} elseif (!empty($owner_info)) {
			//echo "<pre>"; print_r($owner_info);exit;
			$data['prof_tax_no_details'] = $owner_info['prof_tax_no_details'];
		} else {
			$data['prof_tax_no_details'] = '';
		}

		if (isset($this->request->post['start_date'])) {
			$data['start_date'] = date('d-m-Y', strtotime($this->request->post['start_date']));
		} elseif (isset($owner_info['colorQuery'][0]['start_date'])) {
			$data['start_date'] = date('d-m-Y', strtotime($owner_info['colorQuery'][0]['start_date']));
		} else {
			$data['start_date'] = '';
		}

		if (isset($this->request->post['end_date'])) {
			$data['end_date'] = date('d-m-Y', strtotime($this->request->post['end_date']));
		} elseif (isset($owner_info['colorQuery'][0]['end_date'])) {
			$data['end_date'] = date('d-m-Y', strtotime($owner_info['colorQuery'][0]['end_date']));
		} else {
			$data['end_date'] = '';
		}

		if (isset($this->request->post['season'])) {
			$data['season'] = $this->request->post['season'];
		} elseif (isset($owner_info['colorQuery'][0]['season'])) {
			$data['season'] = $owner_info['colorQuery'][0]['season'];
		} else {
			$data['season'] = '';
		}

		if (isset($this->request->post['allow_Share_Color'])) {
			$data['allow_Share_Color'] = $this->request->post['allow_Share_Color'];
		} elseif (isset($owner_info['colorQuery'][0]['allow_Share_Color'])) {
			$data['allow_Share_Color'] = $owner_info['colorQuery'][0]['allow_Share_Color'];
		} else {
			$data['allow_Share_Color'] = '';
		}



		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['date_filer'] = $date_filer;

		$this->response->setOutput($this->load->view('catalog/owner_form_view', $data));
	}

	public function upload() {
		//$this->load->language('catalog/employee');
		$json = array();
		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/owner/file_number')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$file_show_path = HTTP_CATALOG.'system/storage/download/owner/file_number/';
		$file_upload_path = DIR_DOWNLOAD.'owner/file_number/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				$this->log->write(print_r($this->request->files, true));
				$this->log->write($image_name);
				$this->log->write($img_extension);
				$this->log->write($filename);

				
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';
				$this->log->write(print_r($allowed, true));
				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] ='Please upload valid file!'. $this->request->files['file']['error'];
				}
			} else {
				$json['error'] ='Please upload valid file!';
			}
		}

		if (!$json) {
			$file = $filename;
			if(move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file)){
				$destFile = $file_upload_path . $file;
				//chmod($destFile, 0777);
				$json['filename'] = $file;
				$json['link_href'] = $file_show_path . $file;

				$json['success'] = 'Your file was successfully uploaded!';
			} else {
				$json['error'] = 'Please upload valid file!';
			}
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function upload_partner() {
		$this->load->language('catalog/owner');
		$json = array();
		if (!$this->user->hasPermission('modify', 'catalog/owner')) {
			$json['error'] = $this->language->get('error_permission');
		}
		$id = $this->request->get['id'];
		$base = HTTP_CATALOG.'system/storage/download/owner/owner_partner';
		$file_upload_path = DIR_DOWNLOAD.'owner/owner_partner/';
		$file_show_path = $base.'/';
		$image_name = 'partner_image_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}
				$allowed = array();
				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));
				$filetypes = explode("\n", $extension_allowed);
				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('Please upload valid file!');
				}
				$allowed = array();
				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));
				$filetypes = explode("\n", $mime_allowed);
				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('Please upload valid file!');
				}
				$content = file_get_contents($this->request->files['file']['tmp_name']);
				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('Please upload valid file!');
				}
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}
		if (!$json) {
			$file = $filename;
			if(move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file)){
				$destFile = $file_upload_path . $file;
				chmod($destFile, 0777);
				date_default_timezone_set("Asia/Kolkata");
				$json['filename'] = $file;
				$json['filepath'] = $file_show_path . $file;
				$json['id'] = $id;
				$json['success'] = 'Your file was successfully uploaded!';
			} else {
				$json['error'] = 'Please upload valid file!';
			}
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	

	public function upload_itr() {
		$this->load->language('catalog/owner');
		$json = array();
		if (!$this->user->hasPermission('modify', 'catalog/owner/owner_itr')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$id = $this->request->get['id'];
		$base = HTTP_CATALOG.'system/storage/download/owner/owner_itr';
		$file_upload_path = DIR_DOWNLOAD.'owner/owner_itr/';
		$file_show_path = $base.'/';
		$image_name = 'itr_image_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('Please upload valid file!');
				}
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename;
			move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file);
			$destFile = $file_upload_path . $file;
			chmod($destFile, 0777);
			date_default_timezone_set("Asia/Kolkata");
			$json['filename'] = $file;
			$json['filepath'] = $file_show_path . $file;
			$json['id'] = $id;
			$json['success'] = $this->language->get('Your file was successfully uploaded!');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function upload_patner() {
		$this->load->language('catalog/owner');
		$json = array();
		if (!$this->user->hasPermission('modify', 'catalog/owner/owner_itr')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$id = $this->request->get['id'];
		$base = HTTP_CATALOG.'/system/storage/download/owner/owner_itr';
		$file_upload_path = DIR_DOWNLOAD.'owner/owner_itr/';
		$file_show_path = $base.'/';
		$image_name = 'itr_image_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('Please upload valid file!');
				}
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename;
			move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file);
			$destFile = $file_upload_path . $file;
			chmod($destFile, 0777);
			date_default_timezone_set("Asia/Kolkata");
			$json['filename'] = $file;
			$json['filepath'] = $file_show_path . $file;
			$json['id'] = $id;
			$json['success'] = $this->language->get('Your file was successfully uploaded!');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/owner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// echo '<pre>';
		// print_r($this->request->post);
		// exit;
		if (($this->request->post['start_date'] != '') && ($this->request->post['end_date'] != '')) {
			if($this->request->post['start_date'] > $this->request->post['end_date']){
				$this->error['color_date'] = 'Start date must be less then end date ';
			}
		}

		if($this->request->post['panNumber'] != ''){ 
			if(strlen($this->request->post['panNumber']) > 10 || strlen($this->request->post['panNumber']) < 10) { 
				$this->error['pan_errors'] = 'Please Enter Valid PAN NO';
			}
		}

		if($this->request->post['dateApproval']!= '') {
			if (!$this->ischecktDate($this->request->post['dateApproval'])) {
				$this->error['valierr_dateApproval'] = 'Please Enter Valid Date';
				$this->request->post['dateApproval'] = '';
			}
		}
		
		if($this->request->post['owner_name'] == ''){
			$this->error['valierr_owner_name'] = 'Please Select Ownership Name';
		}

		if($this->request->post['isActive'] == 0 && $this->request->post['remark'] == ''){ 
			$this->error['remarks_errors'] = 'Please Enter Remark';
		}

		/*if($this->request->post['ownerNamePrefix'] == '0'){
			$this->error['valierr_ownerNamePrefix'] = 'Please Select Owner Name Prefix';
		}*/

		if ($this->request->post['isActive'] == 0 && $this->request->post['hidden_active'] == 1) {
			//echo "innnnn";
			$sql =("SELECT * FROM `horse_to_owner` WHERE `to_owner_id` ='".$this->request->get['ownerId']."' AND `owner_share_status` ='1' ");
			$query = $this->db->query($sql);

			$link =("SELECT * FROM `linked_owners` WHERE `owner_id` ='".$this->request->get['ownerId']."' ");
			$query1 = $this->db->query($link);

			$color =("SELECT * FROM `owners_shared_color` WHERE `owner_id` ='".$this->request->get['ownerId']."' ");
			$query2 = $this->db->query($color);

			$Corporate =("SELECT * FROM `owners` WHERE `id` ='".$this->request->get['ownerId']."' ");
			$query3 = $this->db->query($Corporate)->row;
			//echo "<pre>";print_r($query3);exit;
			if($query->num_rows > 0){
				//echo "innn";exit;
				$this->error['error_status'] = "You can not Exit status because it is assinged ".$query->num_rows." Horses!";
				//$this->session->data['status_error'] = $this->language->get('error_status');
			}

			if($query1->num_rows > 0){
				//echo "innn";exit;
				$this->error['error_status'] = "You can not Exit status because it is linked!";
				//$this->session->data['status_error'] = $this->language->get('error_status');
			}

			if($query2->num_rows > 0){
				//echo "innn";exit;
				$this->error['error_status'] = "You can not change status because it is assigned to shared color!";
				//$this->session->data['status_error'] = $this->language->get('error_status');
			}

			if ($query3['type_of_ownership'] == 'Corporate') {
				$this->error['error_status'] = "You can not Exit status because it is Corporate ownership!";
			}

		
		}


		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
			//echo "<pre>"; print_r($this->error);exit;
		}
		
		return !$this->error;
		// echo '<pre>';
		// print_r($this->error);
		// exit;
	}


	function ischecktDate($string) { 
		$matches = array();
	    $pattern = '/^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/';
	    if (!preg_match($pattern, $string, $matches)) return false;
	    if (!checkdate($matches[2], $matches[1], $matches[3])) return false;
	    return true;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/owner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/owner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		//echo "<pre>"; print_r($this->request->get);exit;
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/owner');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'owner_Id'	  => $this->request->get['owner_Id'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_owner->getOwnerss($filter_data);
			//echo "<pre>"; print_r($results); exit;
			foreach ($results as $result) {
				$json[] = array(
					'owner_id' => $result['id'],
					'owner_name'        => strip_tags(html_entity_decode($result['owner_name_prefix'].' '.$result['owner_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['owner_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteOwner() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['trainer_name'])) {
			$this->load->model('catalog/owner');

			$results = $this->model_catalog_owner->getOwnersAuto($this->request->get['trainer_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'trainer_id' => $result['id'],
						'trainer_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function getMemberdata() {
		$pan_no = $this->request->get['pan'];

		$url = 'http://113.193.26.178/club1/get_member_data_api.php?pan=' .$pan_no;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$data = json_decode(curl_exec($ch), true);
		$this->response->setOutput(json_encode($data));
	}

	public function colorauto() {
		$json = array();

		//echo "<pre>"; print_r($this->request->get);exit;
		if (isset($this->request->get['filter_color'])) {
			$this->load->model('catalog/owner');

			$filter_data = array(
				'filter_color' => $this->request->get['filter_color'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_owner->getColorsAuto($filter_data);
			//echo "<pre>"; print_r($results); exit;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'color'        => strip_tags(html_entity_decode($result['color'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['color'];
		}

		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function upload_profile() {
		//$this->load->language('catalog/employee');
		$json = array();
		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/owner/profile_pic')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$file_show_path = HTTP_CATALOG.'system/storage/download/owner/profile_pic/';
		$file_upload_path = DIR_DOWNLOAD.'owner/profile_pic/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				$this->log->write(print_r($this->request->files, true));
				$this->log->write($image_name);
				$this->log->write($img_extension);
				$this->log->write($filename);

				
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';
				$this->log->write(print_r($allowed, true));
				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] ='Please upload valid file!'. $this->request->files['file']['error'];
				}
			} else {
				$json['error'] ='Please upload valid file!';
			}
		}

		if (!$json) {
			$file = $filename;
			if(move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file)){
				$destFile = $file_upload_path . $file;
				//chmod($destFile, 0777);
				$json['filename'] = $file;
				$json['link_href'] = $file_show_path . $file;

				$json['success'] = 'Your file was successfully uploaded!';
			} else {
				$json['error'] = 'Please upload valid file!';
			}
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
