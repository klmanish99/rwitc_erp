<?php
date_default_timezone_set("Asia/Kolkata");
class ControllerCatalogMedicine extends Controller {
	private $error = array();

	public function index() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$this->load->language('catalog/medicine');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/medicine');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/medicine');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/medicine');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_medicine->addMedicines($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if (($this->request->get['indent']) && ($this->request->get['indent'] == 'in')) {
				echo "<script>window.close();</script>";
				//$this->response->redirect($this->url->link('catalog/rawmaterialreq/add', 'token=' . $this->session->data['token']. '&close=1' . $url, true));
			} else {
				$this->response->redirect($this->url->link('catalog/medicine', 'token=' . $this->session->data['token'] . $url, true));
			}

		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/medicine');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/medicine');

		if (($this->request->server['REQUEST_METHOD'] == 'POST'&& $this->validateForm())) {
			$this->model_catalog_medicine->editMedicines($this->request->get['id'], $this->request->post);
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/medicine', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/medicine');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/medicine');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_medicine->deleteMedicines($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/medicine', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function repair() {
		$this->load->language('catalog/medicine');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/medicine');

		if ($this->validateRepair()) {
			$this->model_catalog_medicine->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('catalog/medicine', 'token=' . $this->session->data['token'], true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/medicine', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/medicine/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/medicine/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/medicine/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['filter_medicine_name'])) {
			$filter_medicine_name = $this->request->get['filter_medicine_name'];
			$data['filter_medicine_name'] = $this->request->get['filter_medicine_name'];
		}
		else{
			$filter_medicine_name = '';
			$data['filter_medicine_name'] = '';
		}

		if (isset($this->request->get['filter_medicine_id'])) {
			$filter_medicine_id = $this->request->get['filter_medicine_id'];
			$data['filter_medicine_id'] = $this->request->get['filter_medicine_id'];
		}
		else{
			$filter_medicine_id = '';
			$data['filter_medicine_id'] = '';
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = 'Active';
			$data['filter_status'] = 'Active';
		}

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_medicine_name'	=>	$filter_medicine_name,
			'filter_medicine_id'	=>	$filter_medicine_id,
			'filter_status'	=>	$filter_status
		);

		$data['status'] =array(
				'Active'  =>"Active",
				'In-Active'  =>'In-Active'
		);

		$category_total = $this->model_catalog_medicine->getTotalMedicines();

		$results = $this->model_catalog_medicine->getMedicine($filter_data);

		foreach ($results as $result) {
			$logs_info = $this->db->query("SELECT * FROM `medicine_logs` WHERE `med_id` = '".$result['id']."' ORDER BY `id` DESC LIMIT 1 ");
			// echo'<pre>';
			// print_r("SELECT * FROM `supplier_logs` WHERE `supp_id` = '".$result['vendor_id']."' ");
			if ($logs_info->num_rows > 0) {
				$fname = $logs_info->row['fname'];
				$lname = $logs_info->row['lname'];
				$log_date = date('d-m-Y', strtotime($logs_info->row['log_date']));
				$log_time = $logs_info->row['log_time'];
				$log_datas = '<span>'.'Name: '.$fname.' '.$lname.'</span>'.'<br>'.'<span>'.'Date: '.$log_date.'</span>'.'<br>'.'<span>'.'Time: '.$log_time.'</span>';
			} else {
				$log_datas = '';
			}

			$data['categories'][] = array(
				'id' => $result['id'],
				'med_code'        => $result['med_code'],
				'med_name'        => $result['med_name'],
				'med_type'		  => $result['med_type'],
				'log_datas'     => $log_datas,
				'isActive'		  => $result['isActive'],
				'edit'        => $this->url->link('catalog/medicine/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true),
				'delete'      => $this->url->link('catalog/medicine/delete', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/medicine', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/medicine', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/medicine', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/medicine_list', $data));
	}

	protected function getForm() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_descp'] = $this->language->get('entry_descp');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_pack'] = $this->language->get('entry_pack');
		$data['entry_volume'] = $this->language->get('entry_volume');
		$data['entry_unit'] = $this->language->get('entry_unit');
		$data['entry_kg'] = $this->language->get('entry_kg');
		$data['entry_gm'] = $this->language->get('entry_gm');
		$data['entry_l'] = $this->language->get('entry_l');
		$data['entry_ml'] = $this->language->get('entry_ml');
		$data['entry_mg'] = $this->language->get('entry_mg');
		$data['entry_gst'] = $this->language->get('entry_gst');
		$data['entry_pur'] = $this->language->get('entry_purchase');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		if (isset($this->error['valierr_med_name'])) {
			$data['valierr_med_name'] = $this->error['valierr_med_name'];
		}

		if (isset($this->error['valierr_med_code'])) {
			$data['valierr_med_code'] = $this->error['valierr_med_code'];
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/medicine', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			if ((isset($this->request->get['indent'])) && ($this->request->get['indent'] == 'in')) {
				$data['action'] = $this->url->link('catalog/medicine/add', 'token=' . $this->session->data['token']. '&indent='.$this->request->get['indent'] . $url, true);
			} else {
				$data['action'] = $this->url->link('catalog/medicine/add', 'token=' . $this->session->data['token'] . $url, true);
			}
		} else {
			$data['action'] = $this->url->link('catalog/medicine/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		if ((isset($this->request->get['indent'])) && ($this->request->get['indent'] == 'in')) {
			$data['cancel'] = $this->url->link('catalog/rawmaterialreq/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['cancel'] = $this->url->link('catalog/medicine', 'token=' . $this->session->data['token'] . $url, true);
		}

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_medicine->getMedicines($this->request->get['id']);
		}


		// echo '<pre>';
		// print_r($category_info);
		// exit;
		$data['doctors'] = array();

		$results = $this->db->query("SELECT * FROM doctor WHERE isActive = 'Active' ")->rows;

		foreach ($results as $result) {
			$data['doctors'][] = array(
				'id'			=> $result['id'],
				'cost'			=> '',
				'doctor_code'	=> $result['doctor_code'],
				'doctor_name'	=> $result['doctor_name']
			);
		}

		if (isset($this->request->post['med_code'])) {
			$data['med_code'] = $this->request->post['med_code'];
		} elseif (!empty($category_info)) {
			$data['med_code'] = $category_info['med_code'];
		} else {
			$data['med_code'] = '';
		}

		if (isset($this->request->post['alpha'])) {
			$data['alpha'] = $this->request->post['alpha'];
		} elseif (!empty($category_info)) {
			$data['alpha'] = $category_info['alpha'];
		} else {
			$data['alpha'] = '';
		}

		if (isset($this->request->post['alpha_no'])) {
			$data['alpha_no'] = $this->request->post['alpha_no'];
		} elseif (!empty($category_info)) {
			$data['alpha_no'] = $category_info['alpha_no'];
		} else {
			$data['alpha_no'] = '';
		}

		// echo'<pre>';
		// print_r($data['alpha_no']);
		// exit;

		if (isset($this->request->post['med_name'])) {
			$data['med_name'] = $this->request->post['med_name'];
		} elseif (!empty($category_info)) {
			$data['med_name'] = $category_info['med_name'];
		} else {
			$data['med_name'] = '';
		}

		$services = $this->db->query("SELECT * FROM service WHERE isActive = 'Active';")->rows;
		if ($services) {
			foreach ($services as $service) {
				$data['medType'][] = $service['med_type'];
			}
		} else {
			$data['medType'][] = '';
		}

		if (isset($this->request->post['med_type'])) {
			$data['med_type'] = $this->request->post['med_type'];
		} elseif (!empty($category_info)) {
			$data['med_type'] = $category_info['med_type'];
		} else {
			$data['med_type'] = '';
		}

		if (isset($this->request->post['unit_cost'])) {
			$data['unit_cost'] = $this->request->post['unit_cost'];
		} elseif (!empty($category_info)) {
			$data['unit_cost'] = $category_info['unit_cost'];
		} else {
			$data['unit_cost'] = '';
		}

		if (isset($this->request->post['pack'])) {
			$data['pack'] = $this->request->post['pack'];
		} elseif (!empty($category_info)) {
			$data['pack'] = $category_info['pack_type'];
		} else {
			$data['pack'] = '';
		}

		$data['pack_types'] = array(
			'bottle'  => 'Bottles',
			'vials'  => 'Vials',
			'bpack'  => 'Blister packs',
			'sachet'  => 'Sachets',
			'inj'  => 'Syringes',
			'amp'  => 'Ampoules',
		);

		if (isset($this->request->post['gst'])) {
			$data['gst'] = $this->request->post['gst'];
		} elseif (!empty($category_info)) {
			$data['gst'] = $category_info['gst_rate'];
		} else {
			$data['gst'] = '';
		}
		
		if (isset($this->request->post['pur_price'])) {
			$data['p_price'] = $this->request->post['pur_price'];
		} elseif (!empty($category_info)) {
			$data['p_price'] = $category_info['purchase_price'];
		} else {
			$data['p_price'] = '';
		}
		//echo "<PRE>";print_r($data['pack']);echo "</PRE>";die('end line');
 
		if (isset($this->request->post['volume'])) {
			$data['volume'] = $this->request->post['volume'];
		} elseif (!empty($category_info)) {
			$data['volume'] = $category_info['volume'];
		} else {
			$data['volume'] = '';
		}

		if (isset($this->request->post['unit'])) {
			$data['unit'] = $this->request->post['unit'];
		} elseif (!empty($category_info)) {
			$data['unit'] = $category_info['unit'];
		} else {
			$data['unit'] = '';
		}

		$data['unit_list'] = array(
			'pcs'  => 'pcs',
			'kilogram'  => 'kg',
			'grams'  => 'gm',
			'litre'  => 'litre',
			'mililitre'  => 'ml',
			'miligram'  => 'mg',
			
		);



		if (isset($this->request->post['final'])) {
			$data['final'] = $this->request->post['final'];
		} elseif (!empty($category_info)) {
			$finals = $this->db->query("SELECT * FROM `doctor_cost` WHERE `medicine_id` = '".$category_info['id']."' ")->rows;
			$doctors = array();

			foreach($finals as $fkeys => $fvalues){
				$doctor_names = $this->db->query("SELECT doctor_name FROM doctor WHERE id = '".$fvalues['doctor_id']."' ");
				if ($doctor_names->num_rows > 0) {
					$name = $doctor_names->row['doctor_name'];
				} else {
					$name = '';
				}
				
				$doctors[] = array(
					'doctor_name' => $name,
					'id' => $fvalues['doctor_id'],
					'cost' => $fvalues['cost'],
				);	
			}
			$data['doctors'] = $doctors;

		} else {
			$data['final'] = '';
		}
		/*echo'<pre>';
		print_r($data['final']);
		exit;*/

		if (isset($this->request->post['cost_a2'])) {
			$data['cost_a2'] = $this->request->post['cost_a2'];
		} elseif (!empty($category_info)) {
			$data['cost_a2'] = $category_info['cost_a2'];
		} else {
			$data['cost_a2'] = '';
		}

		if (isset($this->request->post['cost_a3'])) {
			$data['cost_a3'] = $this->request->post['cost_a3'];
		} elseif (!empty($category_info)) {
			$data['cost_a3'] = $category_info['cost_a3'];
		} else {
			$data['cost_a3'] = '';
		}

		if (isset($this->request->post['cost_a4'])) {
			$data['cost_a4'] = $this->request->post['cost_a4'];
		} elseif (!empty($category_info)) {
			$data['cost_a4'] = $category_info['cost_a4'];
		} else {
			$data['cost_a4'] = '';
		}

		if (isset($this->request->post['cost_a5'])) {
			$data['cost_a5'] = $this->request->post['cost_a5'];
		} elseif (!empty($category_info)) {
			$data['cost_a5'] = $category_info['cost_a5'];
		} else {
			$data['cost_a5'] = '';
		}

		if (isset($this->request->post['cost_a6'])) {
			$data['cost_a6'] = $this->request->post['cost_a6'];
		} elseif (!empty($category_info)) {
			$data['cost_a6'] = $category_info['cost_a6'];
		} else {
			$data['cost_a6'] = '';
		}

		if (isset($this->request->post['cost_a7'])) {
			$data['cost_a7'] = $this->request->post['cost_a7'];
		} elseif (!empty($category_info)) {
			$data['cost_a7'] = $category_info['cost_a7'];
		} else {
			$data['cost_a7'] = '';
		}

		if (isset($this->request->post['cost_a8'])) {
			$data['cost_a8'] = $this->request->post['cost_a8'];
		} elseif (!empty($category_info)) {
			$data['cost_a8'] = $category_info['cost_a8'];
		} else {
			$data['cost_a8'] = '';
		}

		if (isset($this->request->post['cost_a9'])) {
			$data['cost_a9'] = $this->request->post['cost_a9'];
		} elseif (!empty($category_info)) {
			$data['cost_a9'] = $category_info['cost_a9'];
		} else {
			$data['cost_a9'] = '';
		}

		if (isset($this->request->post['cost_a10'])) {
			$data['cost_a10'] = $this->request->post['cost_a10'];
		} elseif (!empty($category_info)) {
			$data['cost_a10'] = $category_info['cost_a10'];
		} else {
			$data['cost_a10'] = '';
		}

		if (isset($this->request->post['normal'])) {
			$data['normal'] = $this->request->post['normal'];
		} elseif (!empty($category_info)) {
			$data['normal'] = $category_info['normal'];
		} else {
			$data['normal'] = '';
		}

		if (isset($this->request->post['store_unit'])) {
			$data['store_unit'] = $this->request->post['store_unit'];
		} elseif (!empty($category_info)) {
			$data['store_unit'] = $category_info['store_unit'];
		} else {
			$data['store_unit'] = '';
		}
		
		$data['quan_unit'] = array(
			'pcs'  => 'pcs',
			'kilogram'  => 'kg',
			'grams'  => 'gm',
			'litre'  => 'litre',
			'mililitre'  => 'ml',
			'miligram'  => 'mg',
		);

		if (isset($this->request->post['brand'])) {
			$data['brand'] = $this->request->post['brand'];
		} elseif (!empty($category_info)) {
			$data['brand'] = $category_info['brand'];
		} else {
			$data['brand'] = '';
		}

		$data['Active'] = array('In-Active'	=> 'In-Active',
							  'Active'	=> 'Active');

		if (isset($this->request->post['isActive'])) {
			$data['isActive'] = $this->request->post['isActive'];
		} elseif (!empty($category_info)) {
			$data['isActive'] = $category_info['isActive'];
		} else {
			$data['isActive'] = 'Active';
		}

		if (isset($this->request->post['reorder_lvl'])) {
			$data['reorder_lvl'] = $this->request->post['reorder_lvl'];
		} elseif (!empty($category_info)) {
			$data['reorder_lvl'] = $category_info['reorder_lvl'];
		} else {
			$data['reorder_lvl'] = '';
		}

		if (isset($this->request->post['reorder_qty'])) {
			$data['reorder_qty'] = $this->request->post['reorder_qty'];
		} elseif (!empty($category_info)) {
			$data['reorder_qty'] = $category_info['reorder_qty'];
		} else {
			$data['reorder_qty'] = '';
		}

		$data['user_log_grp_id'] = $this->user->getGroupId();

		$data['user_log_id'] = $this->user->getId();

		$data['token'] = $this->session->data['token'];

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/medicine', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/medicine')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if($this->request->post['med_name'] == ''){
			$this->error['valierr_med_name'] = 'Please Select Medicine Name!';
		}

		// if (($this->request->post['med_code'] != '') && (!isset($this->request->get['id'])) ) {
		// 	$avail_code = $this->db->query("SELECT * FROM medicine WHERE `med_code` = '".$this->request->post['med_code']."' ");
		// 	if ($avail_code->num_rows > 0) {
		// 		$this->error['valierr_med_code'] = 'This Code Is Already In Used! Please Select Another!';
		// 	}
		// }
		
		if($this->request->post['med_code'] == ''){
			$this->error['valierr_med_code'] = 'Please Select Medicine Code!';
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/medicine')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/medicine')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_medicine_name']) || isset($this->request->get['filter_item_code'])) {
			$this->load->model('catalog/medicine');

			if (isset($this->request->get['filter_medicine_name'])) {
				$filter_medicine_name = $this->request->get['filter_medicine_name'];
			} else {
				$filter_medicine_name = '';
			}

			if (isset($this->request->get['filter_item_code'])) {
				$filter_item_code = $this->request->get['filter_item_code'];
			} else {
				$filter_item_code = '';
			}

			$filter_data = array(
				'filter_medicine_name'  => $filter_medicine_name,
				'filter_item_code' => $filter_item_code,
			);

			$results = $this->model_catalog_medicine->getMedicineAuto($filter_data);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {
				$inward_exp_date = $this->db->query("SELECT quantity, ex_date FROM `oc_inwarditem` WHERE product_id = '".$result['med_code']."' AND cons_status = 0 ORDER BY ex_date ASC");
				if($inward_exp_date->num_rows > 0){
					$ex_date = date('d-m-Y',strtotime($inward_exp_date->row['ex_date']));
				} else {
					$ex_date = '';
				}
				$final_price = 0;
				$final_price = $result['unit_cost'] * (int)$result['normal'];
				$json[] = array(
					'id' => $result['id'],
					'med_code' => $result['med_code'],
					'purchase_price' => $result['unit_cost'],
					'store_unit' => $result['store_unit'],
					'final_price' =>$final_price,
					'normal' => $result['normal'],
					'expiry_date' => $ex_date,
					'med_type' => $result['med_type'],
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['med_name'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}

	public function MedicineIdGenerate() {
		
		
		$json = array();
		if (isset($this->request->get['filter_medicine_name'])) {
			$this->load->model('catalog/medicine');

			$laste_alph_no = $this->db->query("SELECT * FROM `medicine` WHERE alpha = '".$this->request->get['filter_medicine_name']."' ORDER BY id DESC LIMIT 1 ");

			if($laste_alph_no->num_rows > 0){
				$med_code_no = $laste_alph_no->row['alpha_no'] + 1;
				$json['med_code'] = $laste_alph_no->row['alpha'].''.$med_code_no;
				$json['alpha'] = $laste_alph_no->row['alpha'];
				$json['alpha_no'] = $med_code_no; 
			} else {
				$num = 1;
				$json['med_code'] = strtoupper($this->request->get['filter_medicine_name']).''.$num;
				$json['alpha'] = strtoupper($this->request->get['filter_medicine_name']);
				$json['alpha_no'] = $num;
			}

		// 	echo'<pre>';
		// print_r($med_code);
		// exit;
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}

	public function autocompletes() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_medicine_code'])) {
			$this->load->model('catalog/medicine');

			$results = $this->model_catalog_medicine->getMedicineAutos($this->request->get['filter_medicine_code']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'med_code'     => strip_tags(html_entity_decode($result['med_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['med_code'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}
}
