<?php
class ControllerCatalogRaceType extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/racetype');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/racetype');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/racetype');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/racetype');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			
			$this->model_catalog_racetype->addRaceType($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/racetype', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/racetype');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/racetype');

		if (($this->request->server['REQUEST_METHOD'] == 'POST'&& $this->validateForm())) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_racetype->editRaceType($this->request->get['id'], $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/racetype', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/racetype');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/racetype');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_racetype->deleteRaceType($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/racetype', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function repair() {
		$this->load->language('catalog/racetype');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/racetype');

		if ($this->validateRepair()) {
			$this->model_catalog_racetype->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('catalog/racetype', 'token=' . $this->session->data['token'], true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/racetype', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/racetype/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/racetype/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/racetype/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['token'] = $this->session->data['token'];

		$data['status'] =array(
				'Active'  =>"Active",
				'In-Active'  =>'In-Active'
		);

		$category_total = $this->model_catalog_racetype->getTotalRaceType();

		$results = $this->model_catalog_racetype->getRaceTypes();

		foreach ($results as $result) {
			$data['categories'][] = array(
				'id' => $result['id'],
				'race_type'        => $result['race_type'],
				'race_class'        => $result['race_class'],
				'lower' => $result['lower_class_eligible'],
				'edit'        => $this->url->link('catalog/racetype/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true),
				'delete'      => $this->url->link('catalog/racetype/delete', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');
		$data['column_racetype'] = $this->language->get('column_racetype');
		$data['column_class'] = $this->language->get('column_class');
		$data['column_lower'] = $this->language->get('column_lower');
		

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/racetype', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/racetype', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/racetype', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/racetype_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_racetype'] = $this->language->get('entry_racetype');
		$data['entry_class'] = $this->language->get('entry_class');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_class_eligible'] = $this->language->get('entry_class_eligible');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_pack'] = $this->language->get('entry_pack');
		$data['entry_volume'] = $this->language->get('entry_volume');
		$data['entry_unit'] = $this->language->get('entry_unit');
		$data['entry_kg'] = $this->language->get('entry_kg');
		$data['entry_gm'] = $this->language->get('entry_gm');
		$data['entry_l'] = $this->language->get('entry_l');
		$data['entry_ml'] = $this->language->get('entry_ml');
		$data['entry_mg'] = $this->language->get('entry_mg');
		
		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['error_racetype'])) {
			$data['error_racetype'] = $this->error['error_racetype'];
		} else {
			$data['error_racetype'] = '';
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		if (isset($this->error['valierr_med_name'])) {
			$data['valierr_med_name'] = $this->error['valierr_med_name'];
		}

		if (isset($this->error['valierr_med_code'])) {
			$data['valierr_med_code'] = $this->error['valierr_med_code'];
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/racetype', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/racetype/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/racetype/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/racetype', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_racetype->getRaceType($this->request->get['id']);
			//echo "<PRE>";print_r($category_info);echo "</PRE>";die('end line');
		}

		$race_type = $this->db->query("SELECT race_type FROM racetypes WHERE 1=1 ORDER BY race_type DESC LIMIT 1 ");
		// echo "<PRE>";print_r($race_type);echo "</PRE>";die('end line');
		if ($race_type) {
			$race_type = $race_type->row['race_type'];
		} else {
			$race_type = '';
		}

		$data['race_types'] = array(
			'Handicap Race'  =>'Handicap Race',
			'Term Race'  => 'Term Race',

		);


		$data['classs']= array(
			'Class I'  =>'Class I',
			'Class II'  =>'Class II',
			'Class III'  =>'Class III',
			'Class IV'  =>'Class IV',
			'Class V'  =>'Class V',
			'Mix Class'  =>'Mix Class',
		);

		$data['terms']= array(
			'Grade I'  =>'Grade I',
			'Grade II'  =>'Grade II',
			'Grade III'  =>'Grade III',
			'Mix Grade'  =>'Mix Grade',
		);


		if (isset($this->request->post['race_class'])) {
			$data['race_class'] = $this->request->post['race_class'];
		} elseif (!empty($category_info)) {
			$data['race_class'] = $category_info['race_class'];
		} else {
			$data['race_class'] = '';

		}

		if (isset($this->request->post['race_grade'])) {
			$data['race_grade'] = $this->request->post['race_grade'];
		} elseif (!empty($category_info)) {
			$data['race_grade'] = $category_info['race_class'];
		} else {
			$data['race_grade'] = '';

		}


		if (isset($this->request->post['race_type'])) {
			$data['race_type'] = $this->request->post['race_type'];
		} elseif (!empty($category_info)) {
			$data['race_type'] = $category_info['race_type'];
		} else {
			$data['race_type'] = '';
		}

		// if (isset($this->request->post['lower'])) {
		// 	$data['lower'] = $this->request->post['lower'];
		// } elseif (!empty($category_info)) {
		// 	$data['lower'] = $category_info['lower_class_eligible'];
		// } else {
		// 	$data['lower'] = '';
		// }
		//echo "<PRE>";print_r($data['lower']);echo "</PRE>";die('end line');
 
		if (isset($this->request->post['desc'])) {
			$data['desc'] = $this->request->post['desc'];
		} elseif (!empty($category_info)) {
			$data['desc'] = $category_info['description'];
		} else {
			$data['desc'] = '';
		}

		if (isset($this->request->post['cat'])) {
			$data['cat'] = $this->request->post['cat'];
		} elseif (!empty($category_info)) {
			$data['cat'] = $category_info['category'];
		} else {
			$data['cat'] = '';
		}

		if (isset($this->request->post['l_desc'])) {
			$data['l_desc'] = $this->request->post['l_desc'];
		} elseif (!empty($category_info)) {
			$data['l_desc'] = $category_info['l_description'];
		} else {
			$data['l_desc'] = '';
		}

		if (isset($this->request->post['l_cat'])) {
			$data['l_cat'] = $this->request->post['l_cat'];
		} elseif (!empty($category_info)) {
			$data['l_cat'] = $category_info['l_category'];
		} else {
			$data['l_cat'] = '';
		}

		if (isset($this->request->post['rating_from'])) {
			$data['rating_from'] = $this->request->post['rating_from'];
		} elseif (!empty($category_info)) {
			$data['rating_from'] = $category_info['rating_from'];
		} else {
			$data['rating_from'] = '';
		}

		if (isset($this->request->post['rating_to'])) {
			$data['rating_to'] = $this->request->post['rating_to'];
		} elseif (!empty($category_info)) {
			$data['rating_to'] = $category_info['rating_to'];
		} else {
			$data['rating_to'] = '';
		}

		if (isset($this->request->post['l_rating_from'])) {
			$data['l_rating_from'] = $this->request->post['l_rating_from'];
		} elseif (!empty($category_info)) {
			$data['l_rating_from'] = $category_info['l_rating_from'];
		} else {
			$data['l_rating_from'] = '';
		}

		if (isset($this->request->post['l_rating_to'])) {
			$data['l_rating_to'] = $this->request->post['l_rating_to'];
		} elseif (!empty($category_info)) {
			$data['l_rating_to'] = $category_info['l_rating_to'];
		} else {
			$data['l_rating_to'] = '';
		}





		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/racetype_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/racetype')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		
		
		if($this->request->post['race_type'] == '0'){
			$this->error['error_racetype'] = 'Please Select Race Type';
		}

		
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	 }

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/racetype')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/racetype')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_medicine_name'])) {
			$this->load->model('catalog/racetype');

			$results = $this->model_catalog_racetype->getMedicineAuto($this->request->get['filter_medicine_name']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'med_name'     => strip_tags(html_entity_decode($result['med_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['med_name'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}

	public function autocompletes() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_medicine_code'])) {
			$this->load->model('catalog/racetype');

			$results = $this->model_catalog_racetype->getRaceTypeAutos($this->request->get['filter_medicine_code']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'med_code'     => strip_tags(html_entity_decode($result['med_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['med_code'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}
}
