<?php
date_default_timezone_set("Asia/Kolkata");
class ControllerCatalogService extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/service');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/service');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/service');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/service');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			$this->model_catalog_service->addServices($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/service', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/service');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/service');

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm())) {
			$this->model_catalog_service->editServices($this->request->get['id'], $this->request->post);
			// echo '<pre>';
			// print_r($this->request->post);
			
			// exit;
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/service', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/service');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/service');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_service->deleteServices($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/service', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function repair() {
		$this->load->language('catalog/service');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/service');

		if ($this->validateRepair()) {
			$this->model_catalog_service->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('catalog/service', 'token=' . $this->session->data['token'], true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_medicine_type'])) {
			$filter_medicine_type = $this->request->get['filter_medicine_type'];
			$data['filter_medicine_type'] = $this->request->get['filter_medicine_type'];
		}
		else{
			$filter_medicine_type = '';
			$data['filter_medicine_type'] = '';
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = 'Active';
			$data['filter_status'] = 'Active';
		}

		$data['token'] = $this->session->data['token'];

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/service', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/service/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/service/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/service/repair', 'token=' . $this->session->data['token'] . $url, true);

		$data['categories'] = array();

		$data['status'] =array(
				'Active'  =>"Active",
				'In-Active'  =>'In-Active'
		);

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_medicine_type'	=>	$filter_medicine_type,
			//'filter_medicine_id'	=>	$filter_medicine_id,
			'filter_status'	=>	$filter_status
		);

		$category_total = $this->model_catalog_service->getTotalCategories();

		$results = $this->model_catalog_service->getService($filter_data);

		foreach ($results as $result) {

			$logs_info = $this->db->query("SELECT * FROM `service_logs` WHERE `serv_id` = '".$result['id']."' ORDER BY `id` DESC LIMIT 1 ");
			// echo'<pre>';
			// print_r("SELECT * FROM `supplier_logs` WHERE `supp_id` = '".$result['vendor_id']."' ");
			if ($logs_info->num_rows > 0) {
				$fname = $logs_info->row['fname'];
				$lname = $logs_info->row['lname'];
				$log_date = date('d-m-Y', strtotime($logs_info->row['log_date']));
				$log_time = $logs_info->row['log_time'];
				$log_datas = '<span>'.'Name: '.$fname.' '.$lname.'</span>'.'<br>'.'<span>'.'Date: '.$log_date.'</span>'.'<br>'.'<span>'.'Time: '.$log_time.'</span>';
			} else {
				$log_datas = '';
			}
			$data['categories'][] = array(
				'id' => $result['id'],
				'med_type'     => $result['med_type'],
				'descp'        => $result['descp'],
				'status'        => $result['isActive'],
				'log_datas'     => $log_datas,
				'edit'        => $this->url->link('catalog/service/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true),
				'delete'      => $this->url->link('catalog/service/delete', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/service', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/service', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/service', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/service_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_descp'] = $this->language->get('entry_descp');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['valierr_med_type'])) {
			$data['valierr_med_type'] = $this->error['valierr_med_type'];
		} else {
			$data['valierr_med_type'] = '';
		}

		if (isset($this->error['valierr_med_descp'])) {
			$data['valierr_med_descp'] = $this->error['valierr_med_descp'];
		} else {
			$data['valierr_med_descp'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/service', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/service/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/service/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/service', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_service->getServices($this->request->get['id']);
		}

		if (isset($this->request->post['med_type'])) {
			$data['med_type'] = $this->request->post['med_type'];
		} elseif (!empty($category_info)) {
			$data['med_type'] = $category_info['med_type'];
		} else {
			$data['med_type'] = '';
		}

		if (isset($this->request->post['descp'])) {
			$data['descp'] = $this->request->post['descp'];
		} elseif (!empty($category_info)) {
			$data['descp'] = $category_info['descp'];
		} else {
			$data['descp'] = '';
		}

		if (isset($this->request->post['limit1'])) {
			$data['limit1'] = $this->request->post['limit1'];
		} elseif (!empty($category_info)) {
			$data['limit1'] = $category_info['limit1'];
		} else {
			$data['limit1'] = '';
		}

		if (isset($this->request->post['limit2'])) {
			$data['limit2'] = $this->request->post['limit2'];
		} elseif (!empty($category_info)) {
			$data['limit2'] = $category_info['limit2'];
		} else {
			$data['limit2'] = '';
		}

		if (isset($this->request->post['sc'])) {
			$data['sc'] = $this->request->post['sc'];
		} elseif (!empty($category_info)) {
			$data['sc'] = $category_info['sc'];
		} else {
			$data['sc'] = '';
		}

		$data['Active'] = array('In-Active'	=> 'In-Active',
							  'Active'	=> 'Active');

		if (isset($this->request->post['isActive'])) {
			$data['isActive'] = $this->request->post['isActive'];
		} elseif (!empty($category_info)) {
			$data['isActive'] = $category_info['isActive'];
		} else {
			$data['isActive'] = 'Active';
		}

		$data['user_log_grp_id'] = $this->user->getGroupId();

		$data['user_log_id'] = $this->user->getId();

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/service', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/service')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->request->post['med_type'] == ''){
			$this->error['valierr_med_type'] = 'Please Select Medicine Type!';
		}
		
		if($this->request->post['descp'] == ''){
			$this->error['valierr_med_descp'] = 'Please Select Description!';
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/service')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/service')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		/*echo'<pre>';
		print_r($this->request->get);
		exit;*/
		$json = array();

		if (isset($this->request->get['filter_medicine_type'])) {
			$this->load->model('catalog/service');

			$results = $this->model_catalog_service->getServiceAuto($this->request->get['filter_medicine_type']);
			/*echo'<pre>';
			print_r($results);
			exit;*/

			foreach ($results as $result) {

				$json[] = array(
					'id' => $result['id'],
					'med_type'     => strip_tags(html_entity_decode($result['med_type'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['med_type'];
			//$sort_order[$key] = $value['place'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		/*echo'<pre>';
		print_r($json);
		exit;*/
	}
}
