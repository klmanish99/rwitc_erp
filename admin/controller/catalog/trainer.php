<?php
class Controllercatalogtrainer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/trainer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/trainer');
		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/trainer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/trainer');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_trainer->addCategory($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->response->redirect($this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getForm();
	}

	public function deletebandeatils() {
		$json = array();
		if (isset($this->request->get['trainer_id']) && isset($this->request->get['ban_id'])) {
			$this->db->query("DELETE FROM `trainer_ban` WHERE trainer_id = '" .(int)$this->request->get['trainer_id'] . "' AND ban_id = '".$this->request->get['ban_id']."'");
			$this->log->write("DELETE FROM `trainer_ban` WHERE trainer_id = '" .(int)$this->request->get['trainer_id'] . "' AND ban_id = '".$this->request->get['ban_id']."'");

			$json['success'] = 'Delete Record Sucessfully';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function tab_owner_authority_to_trainer_save() {
		//echo "<pre>";print_r($this->request->post);exit;
		$json = array();
		$datas = $this->request->post;
 	    $data = json_decode(json_encode($datas),true);
		$exist_owner = 0;
		$new_id= '';
 	    $exist_ownerss = $this->db->query("SELECT owner_code FROM owners WHERE owner_code = '".$data['hidden_owner_id']."'");
		if($exist_ownerss->num_rows == 0){
			$exist_owner = 1;
		}

		if (isset($data) && $exist_owner == 0) {
			$this->db->query("DELETE FROM owner_to_trainer_authority WHERE autho_id='".$data['auth_id']."'");
			$start_date = $this->db->escape($data['start_date']);
			$end_date= $this->db->escape($data['end_date']);

			$start_dates = date('Y-m-d', strtotime($start_date));
			if ($start_dates == '1970-01-01') {
				$date1 = '';
			} else {
				$date1 = $start_dates;
			}
			if ($end_date == '31-12-2050') {
				$end_dates = '2050-12-31';
			} else {
				$end_dates = date('Y-m-d', strtotime($end_date));
			}

			if ($end_dates == '1970-01-01') {
				$date2 = '';
			} else {
				$date2 = $end_dates;
			}

			// if (isset($this->request->post['monthly_maint_cost'])) {
			// $data['monthly_maint_cost'] = $this->request->post['monthly_maint_cost'];
			// } elseif (!empty($category_info)) {
			// 	$data['monthly_maint_cost'] = $category_info['monthly_maint_cost'];
			// } else {
			// 	$data['monthly_maint_cost'] = '';
			// }

			$insertQuery = $this->db->query("INSERT INTO owner_to_trainer_authority SET
				check_name = '".$this->db->escape(htmlspecialchars_decode($data['owner_name']))."',
				owner_id = '".$this->db->escape($data['hidden_owner_id'])."',
				trainer_id = '".$this->db->escape($data['hidden_trainer_id'])."',
				start_date ='".$date1."',
				end_date ='".$date2."',
				is_monthly_maint_cost  = '" .$this->db->escape($data['monthly_maint_cost']). "',
				is_commission_to_trn_jock = '".$this->db->escape($data['Commission_to_tm_and_jockey'])."',
				is_synces_bonus = '".$this->db->escape($data['syces'])."',
				is_pay_othr_club = '".$this->db->escape($data['Payment_other_clubs'])."',
				is_waiter_charge = '".$this->db->escape($data['water_charges'])."',
				is_stable_rent = '".$this->db->escape($data['stable_rent'])."',
				is_extra_oats = '".$this->db->escape($data['extra_oats'])."',
				is_priv_doc_vet_bills = '".$this->db->escape($data['private_doctors_bills'])."',
				is_eia_test_charge = '".$this->db->escape($data['eia_test_charges'])."',
				is_extras = '".$this->db->escape($data['extras'])."',
				is_electricity_charge = '".$this->db->escape($data['electricitty_charges'])."',
				is_buy = '".$this->db->escape($data['buy'])."',
				is_sell = '".$this->db->escape($data['sell'])."',
				is_any_othr_payment = '".$this->db->escape($data['any_other_payment'])."',
				is_sub_authority = '".$this->db->escape($data['appoint_sub_agent'])."',
				is_indefinite = '".$this->db->escape($data['is_indefinite'])."',
				remarks = '".$this->db->escape($data['remarks'])."'

			");
			$new_id=$this->db->getLastId();
			$json = $data;
		}
			$json1=array('json' =>$json,'new_id' =>$new_id , 'exist_owner' => $exist_owner);
			$this->response->setOutput(json_encode($json1));
	}

	public function SubAuthority(){
		$datas = $this->request->post;
 	    $data = json_decode(json_encode($datas),true);
		//echo "<pre>";print_r($data);exit;
		$exist_trainer = 0;
		$exist_owner = 0;
		$new_id = '';
		$exist_ownerss = $this->db->query("SELECT owner_code FROM owners WHERE owner_code = '".$data['sub_hidden_owner_id']."'");
		if($exist_ownerss->num_rows == 0){
			$exist_owner = 1;
		}

		$exist_trainerss = $this->db->query("SELECT id FROM trainers WHERE id = '".$data['sub_hidden_trainer_id']."'");
		if($exist_trainerss->num_rows == 0){
			$exist_trainer = 1;
		}

 	    if (isset($data) &&  $exist_owner == 0 && $exist_trainer == 0) {

 	    	$this->db->query("DELETE FROM sub_owner_to_trainer_authority WHERE sub_autho_id='".$data['sub_autho_id']."'");
 	    	$start_date = $this->db->escape($data['sub_start_date']);
			$end_date= $this->db->escape($data['sub_end_date']);

			$start_dates = date('Y-m-d', strtotime($start_date));
			if ($start_dates == '1970-01-01') {
				$date1 = '';
			} else {
				$date1 = $start_dates;
			}
			if ($end_date == '31-12-2050') {
				$end_dates = '2050-12-31';
			} else {
				$end_dates = date('Y-m-d', strtotime($end_date));
			}

			if ($end_dates == '1970-01-01') {
				$date2 = '';
			} else {
				$date2 = $end_dates;
			}

			$insertQuery = $this->db->query("INSERT INTO sub_owner_to_trainer_authority SET
				owner_name = '".$this->db->escape(htmlspecialchars_decode($data['sub_owner_name']))."',
				owner_id = '".$this->db->escape($data['sub_hidden_owner_id'])."',
				trainer_name = '".$this->db->escape(htmlspecialchars_decode($data['sub_trainer_name']))."',
				trainer_id = '".$this->db->escape($data['hidden_trainer_id'])."',
				start_date ='".$date1."',
				end_date ='".$date2."',
				enter  = '" .$this->db->escape($data['enter']). "',
				declares = '".$this->db->escape($data['declare'])."',
				scratch = '".$this->db->escape($data['scratch'])."',
				pay_money = '".$this->db->escape($data['pay_money'])."',
				receive_money = '".$this->db->escape($data['receive_money'])."',
				sub_buy = '".$this->db->escape($data['sub_buy'])."',
				sub_sell = '".$this->db->escape($data['sub_sell'])."',
				sub_remarks = '".$this->db->escape($data['sub_remarks'])."'
			");
			$new_id=$this->db->getLastId();
			
		} 
		$json = $data;
		$json1=array('json' =>$json,'new_id' =>$new_id, 'exist_trainer' => $exist_trainer, 'exist_owner' => $exist_owner);
		$this->response->setOutput(json_encode($json1));
		//echo '<pre>';print_r($this->request->post);exit;
	}

	public function edit() {  
		$this->load->language('catalog/trainer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/trainer');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo '<pre>';print_r($this->request->post);exit;
			$this->model_catalog_trainer->editCategory($this->request->get['id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->response->redirect($this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getForm();
	}

	public function view() {  
		$this->load->language('catalog/trainer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/trainer');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo '<pre>';print_r($this->request->post);exit;
			$this->model_catalog_trainer->editCategory($this->request->get['id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->response->redirect($this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getFormview();
	}
	
	public function edit1() { 
		//echo '<pre>';print_r($this->request->get);exit;
		$json = array();
		if (isset($this->request->get['auth_id_name'])) {
			$this->load->model('catalog/trainer');
			$results = $this->model_catalog_trainer->editCategory1($this->request->get['auth_id_name']);
			
			$start_date = $this->db->escape($results['start_date']);
			$end_date= $this->db->escape($results['end_date']);

			$start_dates = date('Y-m-d', strtotime($start_date));
			if ($start_dates == '1970-01-01') {
				$date1 = '';
			} else {
				$date1 = date('d-m-Y', strtotime($start_dates));
			}

			if ($end_date == '2050-12-31') {
				$end_dates = '31-12-2050';
			} else {
				$end_dates = date('Y-m-d', strtotime($end_date));
			}

			if ($end_dates == '31-12-2050') {
				$date2 = '31-12-2050';
			} elseif ($end_dates == '1970-01-01') {
				$date2 = '';
			} else {
				$date2 = date('d-m-Y', strtotime($end_dates));
			}
			$json = array(
				'autho_id'  => $results['autho_id'],
				'check_name' => $results['check_name'],
				'sub_auth_id' => $results['sub_auth_id'],
				'owner_id' => $results['owner_id'],
				'trainer_id' => $results['trainer_id'],
				'start_date' => $date1,
				'end_date' => $date2,
				'is_monthly_maint_cost' => $results['is_monthly_maint_cost'],
				'is_commission_to_trn_jock' => $results['is_commission_to_trn_jock'],
				'is_synces_bonus' => $results['is_synces_bonus'],
				'is_pay_othr_club' => $results['is_pay_othr_club'],
				'is_waiter_charge' => $results['is_waiter_charge'],
				'is_stable_rent' => $results['is_stable_rent'],
				'is_extra_oats' => $results['is_extra_oats'],
				'is_priv_doc_vet_bills' => $results['is_priv_doc_vet_bills'],
				'is_eia_test_charge' => $results['is_eia_test_charge'],
				'is_extras' => $results['is_extras'],
				'is_electricity_charge' => $results['is_electricity_charge'],
				'is_buy' => $results['is_buy'],
				'is_sell' => $results['is_sell'],
				'is_any_othr_payment' => $results['is_any_othr_payment'],
				'is_sub_authority' => $results['is_sub_authority'],
				'is_basic_maintance_tax' => $results['is_basic_maintance_tax'],
				'is_allow_ent_and_scratch' => $results['is_allow_ent_and_scratch'],
				'is_pay_money' => $results['is_pay_money'],
				'is_receive_money' => $results['is_receive_money'],
				'is_sub_pay_to_othr_club' => $results['is_sub_pay_to_othr_club'],
				'sub_auth_start_date' => $results['sub_auth_start_date'],
				'sub_trainer_id' => $results['sub_trainer_id'],
				'is_indefinite' => $results['is_indefinite'],
				'remarks' => $results['remarks']
			);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function edit2() { 
		//echo '<pre>';print_r($this->request->post);exit;
		$json = array();
		if (isset($this->request->get['auth_id_name'])) {
			$this->load->model('catalog/trainer');
			$results = $this->model_catalog_trainer->editCategory2($this->request->get['auth_id_name']);
			//echo '<pre>';print_r($results);exit;
			$start_date = $this->db->escape($results['start_date']);
			$end_date= $this->db->escape($results['end_date']);

			$start_dates = date('Y-m-d', strtotime($start_date));
			if ($start_dates == '1970-01-01') {
				$date1 = '';
			} else {
				$date1 = date('d-m-Y', strtotime($start_dates));
			}

			if ($end_date == '2050-12-31') {
				$end_dates = '31-12-2050';
			} else {
				$end_dates = date('Y-m-d', strtotime($end_date));
			}

			if ($end_dates == '31-12-2050') {
				$date2 = '31-12-2050';
			} elseif ($end_dates == '1970-01-01') {
				$date2 = '';
			} else {
				$date2 = date('d-m-Y', strtotime($end_dates));
			}
			$json = array(
				'sub_autho_id'  => $results['sub_autho_id'],
				'owner_name' => $results['owner_name'],
				'trainer_name' => $results['trainer_name'],
				'owner_id' => $results['owner_id'],
				'trainer_id' => $results['trainer_id'],
				'start_date' => $date1,
				'end_date' => $date2,
				'enter' => $results['enter'],
				'declares' => $results['declares'],
				'scratch' => $results['scratch'],
				'pay_money' => $results['pay_money'],
				'receive_money' => $results['receive_money'],
				'sub_buy' => $results['sub_buy'],
				'sub_sell' => $results['sub_sell'],
				'sub_remarks' => $results['sub_remarks']
			);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function delete() {
		$this->load->language('catalog/trainer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/trainer');
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_trainer->deleteTrainers($id);
			}
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->response->redirect($this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getList();
	}


	public function upload() {
		$this->load->language('catalog/trainer');
		$json = array();
		if (!$this->user->hasPermission('modify', 'catalog/trainer')) {
			$json['error'] = $this->language->get('error_permission');
		}
		$id = $this->request->get['id'];
		$base = HTTP_CATALOG.'/system/storage/download/trainer_itr';
		$file_upload_path = DIR_DOWNLOAD.'trainer_itr/';
		$file_show_path = $base.'/';
		$image_name = 'itr_image_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}
				$allowed = array();
				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));
				$filetypes = explode("\n", $extension_allowed);
				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';
				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}
				$allowed = array();
				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));
				$filetypes = explode("\n", $mime_allowed);
				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}
				$content = file_get_contents($this->request->files['file']['tmp_name']);
				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}
		if (!$json) {
			$file = $filename;
			move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file);
			$destFile = $file_upload_path . $file;
			chmod($destFile, 0777);
			date_default_timezone_set("Asia/Kolkata");
			$json['filename'] = $file;
			$json['filepath'] = $file_show_path . $file;
			$json['id'] = $id;
			$json['success'] = $this->language->get('Your file was successfully uploaded!');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function repair() {
		$this->load->language('catalog/trainer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/trainer');
		if ($this->validateRepair()) {
			$this->model_catalog_trainer->repairCategories();
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('catalog/trainer', 'token=' . $this->session->data['token'], true));
		}
		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['filter_trainer_name'])) {
			$filter_trainer_name =$this->request->get['filter_trainer_name'];
		} else{
			$filter_trainer_name ="";
		}
		if (isset($this->request->get['filter_trainer_code'])) {
			$filter_trainer_codes =$this->request->get['filter_trainer_code'];
			$filter_trainer_code =substr($filter_trainer_codes,1);
		} else{
			$filter_trainer_codes ="";
			$filter_trainer_code ='';
		}

		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_trainer_name'])) {
			$url .= '&filter_trainer_name=' . $this->request->get['filter_trainer_name'];
		}
		if (isset($this->request->get['filter_trainer_code'])) {
			$url .= '&filter_trainer_code=' . $this->request->get['filter_trainer_code'];
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = '1';
			$data['filter_status'] = '1';
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . $url, true)
		);
		$Trainer=array();
		$data['add'] = $this->url->link('catalog/trainer/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/trainer/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/trainer/repair', 'token=' . $this->session->data['token'] . $url, true);
		$data['categories'] = array();
		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
			'filter_trainer_name' => $filter_trainer_name,
			'filter_trainer_code' => $filter_trainer_code,
			'filter_status'	=>	$filter_status
		);
		$data['Trainers'] =array();
		$category_total = $this->model_catalog_trainer->getTotalTrainers($filter_data);
		$results = $this->model_catalog_trainer->getTrainerFilter($filter_data);
		
		foreach ($results as $result) {
			// $ban = $this->db->query("SELECT * FROM trainer_ban WHERE trainer_id = '".$result['id']."' ");
			
			// $ban_date = '';
			// if ($ban->num_rows >0) {
			// 	foreach ($ban->rows as $akey => $avalue) {
			// 		$ban_date .= '<b>'.'DATE:'.'</b>'.date('d-m-Y', strtotime($avalue['start_date1'])).' '.'TO'.' '.date('d-m-Y', strtotime($avalue['end_date1'])).' '.'<b>';
			// 	}
				
			// }

			$current_date = date("Y-m-d");

			$ban_sql = "SELECT * FROM `trainer_ban` WHERE trainer_id = '".$result['id']."' ";
			$ban = $this->db->query($ban_sql);
			
			$final_ban = '';
			$start='';
			$end='';
			if ($ban->num_rows >0) {
				foreach ($ban->rows as $akey => $avalue) {
					// echo "<pre>";print_r($current_date);
					$start= date('d-m-Y', strtotime($avalue['start_date1']));
					$end =  date('d-m-Y', strtotime($avalue['end_date1']));
					// echo "<pre>";print_r($avalue['enddate_ban']);exit;
					if ($avalue['end_date1'] >= $current_date) {
						$final_ban .= '</b>'.$avalue['club'].'-'.$avalue['authority'].'<br>'.'<b>';
					}else{
						$final_ban = '';
					}
				}
				
			}

			$trainer_sql = "SELECT * FROM `horse_to_trainer` WHERE trainer_id = '".$result['id']."' ";
			$trainer = $this->db->query($trainer_sql);
			//echo "<pre>";print_r($trainer);exit;
			$horse_name = '';
			if ($trainer->num_rows > 0) {
				// foreach ($trainer->rows as $tkey => $tvalue) {
				// 	$horse_sql = "SELECT * FROM `horse1` WHERE horseseq = '".$tvalue['horse_id']."' ";
				// 	$horse = $this->db->query($horse_sql)->row;

				// 	if (isset($horse['official_name'])) {
				// 		$horse_name = $horse['official_name'];
				// 	} else{
				// 		$horse_name = '';
				// 	}
				// 	//echo "<pre>";print_r($horse);
				// }//exit;
					$horse_sql ="SELECT COUNT(horse_id) AS Horse_count FROM horse_to_trainer WHERE trainer_id = '".$result['id']."'";
					$horse = $this->db->query($horse_sql)->row['Horse_count'];
			} else{
				$horse =0;
			}

			// if ($start == '01-01-1970' || $end == '01-01-1970') {
			// 	$date_start = '';
			// 	$date_end = '';
			// } else{
			// 	$date_start = $start;
			// 	$date_end = $end;
			// }

			$data['Trainers'][] = array(
				'id'=> $result['id'],
				'trainer_code' => $result['trainer_code_new'],
				'name'        => $result['name'],
				'pan_no'	=>$result['pan_no'],
				'mobile_no'	=>$result['mobile_no'],
				'alternate_mob_no'	=>$result['alternate_mob_no'],
				'license_type' =>$result['license_type'],
				'email_id'	=>$result['email_id'],
				'horses_in_charge'	=>$horse,
				'ban'	=>$final_ban,
				'start' => $start,
				'end' => $end,
				'trainer_Status' 	=> $result['active'],
				'edit'        => $this->url->link('catalog/trainer/edit', 'token=' . $this->session->data['token'] .'&id=' .$result['id']. $url, true),
				'view'        => $this->url->link('catalog/trainer/view', 'token=' . $this->session->data['token'] .'&id=' .$result['id']. $url, true)
			);
		}
		$data['status'] =array();

		$data['status'] =array(
				'0'  =>'Inactive',
				'1'  =>"Active",
				'2'	=> 'Left',

		);

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_view'] = $this->language->get('button_view');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}



		$data['sr_no'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=trainers.id' . $url, true);
		$data['trainer_code'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=trainer_code' . $url, true);
		$data['trainer_racing_name'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['mobile_number'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=tcd.mobile_no' . $url, true);
		$data['email_id'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=tcd.email_id' . $url, true);
		$data['pan_no'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=pan_no' . $url, true);

		$data['license_type'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=license_type' . $url, true);
		$data['horses_in_charge'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=t.horses_in_charge' . $url, true);
		$data['ban'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=t.ban' . $url, true);
		$data['statuses'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=t.status' . $url, true);
		$data['sort_name'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));
		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['filter_trainer_name'] = $filter_trainer_name;
		$data['filter_trainer_code'] = $filter_trainer_code;
		$this->response->setOutput($this->load->view('catalog/trainer_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		$users_id = $this->session->data['user_id'];
		$user_group_ids = $this->db->query("SELECT * FROM oc_user WHERE user_id = '".$users_id."' ");
		if ($user_group_ids->num_rows > 0) {
			$data['group_id'] = $user_group_ids->row['user_group_id'];
		} else {
			$data['group_id'] = '';
		}
		// echo '<pre>';
		// print_r($data['group_id']);
		// 	exit;
		$data['itrUploadDatas'] = array();

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}
		if (isset($this->error['error_status'])) {
			$data['error_status'] = $this->error['error_status'];
		} else {
			$data['error_status'] = '';
		}

		if (isset($this->error['remarks_errors'])) {
			$data['remarks_errors'] = $this->error['remarks_errors'];
		} else {
			$data['remarks_errors'] = '';
		}

		if (isset($this->error['gst_errors'])) {
			$data['gst_errors'] = $this->error['gst_errors'];
		} else {
			$data['gst_errors'] = '';
		}

		if (isset($this->error['pan_errors'])) {
			$data['pan_errors'] = $this->error['pan_errors'];
		} else {
			$data['pan_errors'] = '';
		}

		if (isset($this->error['valierr_trainer_name'])) {
			$data['valierr_trainer_name'] = $this->error['valierr_trainer_name'];
		}
		if (isset($this->error['valierr_trainer_code'])) {
			$data['valierr_trainer_code'] = $this->error['valierr_trainer_code'];
		}
		if (isset($this->error['valierr_date_of_birth'])) {
			$data['valierr_date_of_birth'] = $this->error['valierr_date_of_birth'];
		}
		if (isset($this->error['valierr_license_issue_date'])) {
			$data['valierr_license_issue_date'] = $this->error['valierr_license_issue_date'];
		}
		if (isset($this->error['valierr_license_renewal_date'])) {
			$data['valierr_license_renewal_date'] = $this->error['valierr_license_renewal_date'];
		}
		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}
		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . $url, true)
		);
		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/trainer/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/trainer/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}
		$data['cancel'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . $url, true);
		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_trainer->getTrainer($this->request->get['id']);
		}
		
		if(isset($this->request->get['autho_id']) && isset($this->request->get['owner_id'])){
			$data['owner_info'] = $this->model_catalog_trainer->editCategory1($this->request->get['autho_id']);
			$this->response->setOutput($this->load->view('catalog/trainer_form', $data));
		}
		if (!empty($owner_info)) {
				$data['check_name'] = $owner_info['check_name'];
		} else {
			$data['check_name'] = '';
		}
		$data['token'] = $this->session->data['token'];
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		//echo "<pre>";print_r($category_info);exit;

		if (isset($this->request->post['isActive'])) {
			$data['isActive'] = $this->request->post['isActive'];
		} elseif (!empty($category_info)) {
			$activeStatus = ($category_info['license_type'] == '') ? 0 : $category_info['active'];
			$data['isActive'] = $activeStatus;
		} else {
			$data['isActive'] = '1';
		}

		if(isset($this->request->get['id'])){
			$data['getTrainerId'] = $this->request->get['id'];
		} else {
			$data['getTrainerId'] = 0;
		}


		if (isset($this->request->post['hidden_active'])) {
			$data['isActive1'] = $this->request->post['hidden_active'];
		} elseif (!empty($category_info)) {
			$data['isActive1'] = $category_info['active'];
		} else {
			$data['isActive1'] = '';
		}

		$data['assistant_trainers'] = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5'
		);

		$data['bloodgroups'] = array(
			'AB+' =>'AB+',
			'AB-'  =>'AB-',
			'A+' =>'A+',
			'A-'  =>'A-',
			'B+' =>'B+',
			'B-'  =>'B-',
			'O+' =>'O+',
			'O-'  =>'O-',
		);

		$data['Active'] = array('0'	=> 'Inactive',
							  '1'	=> 'Active',
							  '2'	=> 'Left',
							);

		//$results = $this->model_catalog_trainer->getTrainerFilter($this->request->get['id']);
		$data['Trainers'] = array();
		if (isset($this->request->get['id'])) {
		$data['trainer_idss'] = $this->request->get['id'];
			$results = $this->db->query("SELECT * FROM trainer_renewal_history WHERE `trainer_id` = '".$this->request->get['id']."' ")->rows;
			foreach ($results as $result) {
				$data['Trainers'][] = array(
					'id' => $result['id'],
					'renewal_date' => date('d-m-Y', strtotime($result['renewal_date'])),
					'amount' => $result['amount'],
					'fees' => $result['fees'],
					'license_start_date' => date('d-m-Y', strtotime($result['license_start_date'])),
					'license_end_date' => date('d-m-Y', strtotime($result['license_end_date'])),
					'license_type' => $result['license_type'],
					'apprenties' => $result['apprenties'],
					'delete' => $this->url->link('catalog/trainer/delete_transaction')
				);
				
			}
		}

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();
		$data['countries2'] = $this->model_localisation_country->getCountries();
		// echo'<pre>';
		// print_r($data['countries']);
		// exit;
		$data['zones'] = $this->model_localisation_country->getZone();
		$data['zones2'] = $this->model_localisation_country->getZone();


		if (isset($this->request->post['assistant_trainer'])) {
			$data['assistant_trainer'] = $this->request->post['assistant_trainer'];
		} elseif (!empty($category_info)) {
			if($category_info['assistant_trainer_count'] != 'NA'){
				$data['assistant_trainer'] = $category_info['assistant_trainer_count'];
			} else {
				$data['assistant_trainer'] = '';
			}
		} else {
			$data['assistant_trainer'] = '';
		}


		if (isset($this->request->post['file_number_uploads'])) {
			$data['file_number_uploads'] = $this->request->post['file_number_uploads'];
		} elseif (!empty($category_info)) {
			$data['file_number_uploads'] = $category_info['file_number_uploads'];
		} else {
			$data['file_number_uploads'] = '';
		}

		if (!empty($category_info)) {
    		// $img = ltrim($category_info['image_source'],"/");
    		if($category_info['uploaded_file_sources'] != ''){
    			$data['img_path'] = $category_info['uploaded_file_sources'];
    		} else {
    			$data['img_path'] = '1';
    		}
		} else {
      		$data['img_path'] = '#';
    	}
		
		if (isset($this->request->post['uploaded_file_sources'])) {
			$data['uploaded_file_sources'] = $this->request->post['uploaded_file_sources'];
		} elseif (!empty($category_info)) {
			$data['uploaded_file_sources'] = $category_info['uploaded_file_sources'];
		} else {
			$data['uploaded_file_sources'] = '';
		}

		if (isset($this->request->post['assistant_trainer_1'])) {
			$data['assistant_trainer_1'] = $this->request->post['assistant_trainer_1'];
		} elseif (!empty($category_info)) {
			if($category_info['assistant_trainer_1'] != 'NA'){
				$data['assistant_trainer_1'] = $category_info['assistant_trainer_1'];
			} else {
				$data['assistant_trainer_1'] = '';
			}
		} else {
			$data['assistant_trainer_1'] = '';
		}

		if (isset($this->request->post['assistant_trainer_2'])) {
			$data['assistant_trainer_2'] = $this->request->post['assistant_trainer_2'];
		} elseif (!empty($category_info)) {
			if($category_info['assistant_trainer_2'] != 'NA'){
				$data['assistant_trainer_2'] = $category_info['assistant_trainer_2'];
			} else {
				$data['assistant_trainer_2'] = '';
			}
		} else {
			$data['assistant_trainer_2'] = '';
		}

		if (isset($this->request->post['assistant_trainer_3'])) {
			$data['assistant_trainer_3'] = $this->request->post['assistant_trainer_3'];
		} elseif (!empty($category_info)) {
			if($category_info['assistant_trainer_3'] != 'NA'){
				$data['assistant_trainer_3'] = $category_info['assistant_trainer_3'];
			} else {
				$data['assistant_trainer_3'] = '';
			}
		} else {
			$data['assistant_trainer_3'] = '';
		}
		
		if (isset($this->request->post['assistant_trainer_4'])) {
			$data['assistant_trainer_4'] = $this->request->post['assistant_trainer_4'];
		} elseif (!empty($category_info)) {
			if($category_info['assistant_trainer_4'] != 'NA'){
				$data['assistant_trainer_4'] = $category_info['assistant_trainer_4'];
			} else {
				$data['assistant_trainer_4'] = '';
			}
		} else {
			$data['assistant_trainer_4'] = '';
		}

		if (isset($this->request->post['assistant_trainer_5'])) {
			$data['assistant_trainer_5'] = $this->request->post['assistant_trainer_5'];
		} elseif (!empty($category_info)) {
			$data['assistant_trainer_5'] = $category_info['assistant_trainer_5'];
		} else {
			$data['assistant_trainer_5'] = '';
		}

		if (isset($this->request->post['is_wita'])) {
			$data['is_wita'] = $this->request->post['is_wita'];
		} elseif (!empty($category_info)) {
			$data['is_wita'] = $category_info['is_wita'];
		} else {
			$data['is_wita'] = '';
		}

		if (isset($this->request->post['is_tfi'])) {
			$data['is_tfi'] = $this->request->post['is_tfi'];
		} elseif (!empty($category_info)) {
			$data['is_tfi'] = $category_info['is_tfi'];
		} else {
			$data['is_tfi'] = '';
		}


		if (isset($this->request->post['is_kta'])) {
			$data['is_kta'] = $this->request->post['is_kta'];
		} elseif (!empty($category_info)) {
			$data['is_kta'] = $category_info['is_kta'];
		} else {
			$data['is_kta'] = '';
		}


		if (isset($this->request->post['id'])) {
			$data['id'] = $this->request->post['id'];
		} elseif (!empty($category_info)) {
			$data['id'] = $category_info['id'];
		} else {
			$data['id'] = '';
		}

		//echo "<pre>";print_r($data['id']);exit;

		if (isset($this->request->post['trainer_name'])) {
			$data['trainer_name'] = $this->request->post['trainer_name'];
		} elseif (!empty($category_info)) {
			$data['trainer_name'] = $category_info['name'];
		} else {
			$data['trainer_name'] = '';
		}
		// if (isset($this->request->post['license_amt'])) {
		// 	$data['license_amt'] = $this->request->post['license_amt'];
		// } elseif (!empty($category_info)) {
		// 	$data['license_amt'] = $category_info['license_amt'];
		// } else {
		// 	$data['license_amt'] = '';
		// }

		$trainer_codes = $this->db->query("SELECT trainer_code FROM trainers WHERE 1=1 ORDER BY trainer_code DESC LIMIT 1 ");
		/*echo'<pre>';
		print_r("SELECT med_code FROM medicine WHERE 1=1 ORDER BY id DESC LIMIT 1 ");
		exit;*/
		if ($trainer_codes->num_rows > 0) {
			//echo "<pre>";print_r($trainer_codes->row);exit;
			$trainer_code = $trainer_codes->row['trainer_code'];
		} else {
			$trainer_code = '';
		}

		if (isset($this->request->post['trainer_code'])) {
			$data['trainer_code'] = $this->request->post['trainer_code'];
		} elseif (!empty($category_info)) {
			$data['trainer_code'] = 'T'.$category_info['trainer_code'];
		} elseif (isset($this->request->post['hidden_trainer_code'])) {
			$data['trainer_code'] = $this->request->post['hidden_trainer_code'];
		} else {
			$addTcode = $trainer_code + 1;
			$data['trainer_code'] = 'T'.$addTcode;
		}
		if (isset($this->request->post['arrival_charges_to_be_paid'])) {
			$data['arrival_charges_to_be_paid'] = $this->request->post['arrival_charges_to_be_paid'];
		} elseif (!empty($category_info)) {
			$data['arrival_charges_to_be_paid'] = $category_info['arrival_charges_to_be_paid'];
		} else {
			$data['arrival_charges_to_be_paid'] = '';
		}
		if (isset($this->request->post['racing_name'])) {
			$data['racing_name'] = $this->request->post['racing_name'];
		} elseif (!empty($category_info)) {
			$data['racing_name'] = $category_info['racing_name'];
		} else {
			$data['racing_name'] = '';
		}

		if (isset($this->request->post['trainer_code_new'])) {
			$data['trainer_code_new'] = $this->request->post['trainer_code_new'];
		} elseif (!empty($category_info)) {
			$data['trainer_code_new'] = $category_info['trainer_code_new'];
		} else {
			$data['trainer_code_new'] = '';
		}

		if (isset($this->request->post['date_of_birth'])) {
				$data['date_of_birth'] = $this->request->post['date_of_birth'];
		} elseif (!empty($category_info)) {
			if($category_info['date_of_birth'] == '1970-01-01' || $category_info['date_of_birth'] == '0000-00-00'){
				$data['date_of_birth'] = '';
			} else {
				$data['date_of_birth'] = date('d-m-Y', strtotime($category_info['date_of_birth']));
			}
		} else {
			$data['date_of_birth'] = '';
		}

		if (isset($this->request->post['date_of_license_issue'])) {
			$data['date_of_license_issue'] = $this->request->post['date_of_license_issue'];
		} elseif (!empty($category_info)) {
			if($category_info['date_of_license_issue'] == '1970-01-01' || $category_info['date_of_license_issue'] == '0000-00-00'){
				$data['date_of_license_issue'] = '';
			} else {
				$data['date_of_license_issue'] = date('d-m-Y', strtotime($category_info['date_of_license_issue']));
			}
		} else {
			$data['date_of_license_issue'] = '';
		}

		if (isset($this->request->post['date_of_license_issue2'])) {
			$data['date_of_license_issue2'] = $this->request->post['date_of_license_issue2'];
		} elseif (!empty($category_info)) {
			if($category_info['date_of_license_issue2'] == '1970-01-01' || $category_info['date_of_license_issue2'] == '0000-00-00'){
				$data['date_of_license_issue2'] = '';
			} else {
				$data['date_of_license_issue2'] = date('d-m-Y', strtotime($category_info['date_of_license_issue2']));
			}
		} else {
			$data['date_of_license_issue2'] = '';
		}

		if (isset($this->request->post['date_of_license_renewal'])) {
			$data['date_of_license_renewal'] = $this->request->post['date_of_license_renewal'];
		} elseif (!empty($category_info)) {
			if($category_info['date_of_license_renewal'] == '1970-01-01' || $category_info['date_of_license_renewal'] == '0000-00-00'){
				$data['date_of_license_renewal'] = '';
			} else {
				$data['date_of_license_renewal'] = date('d-m-Y', strtotime($category_info['date_of_license_renewal']));
			}
		} else {
			$data['date_of_license_renewal'] = '';
		}

		if (isset($this->request->post['hidden_date_of_license_renewal'])) {
			$data['hidden_date_of_license_renewal'] = $this->request->post['hidden_date_of_license_renewal'];
		} else {
			$data['hidden_date_of_license_renewal'] = '';
		}

		// if (isset($this->request->post['license_type'])) {
		// 	$data['previous_license_type'] = $this->request->post['license_type'];
		// } elseif (!empty($category_info)) {
		// 	$data['previous_license_type'] = $category_info['previous_license_type'];
		// } else {
		// 	$data['previous_license_type'] = '';
		// }

		// if (isset($this->request->post['hidden_previous_license_type'])) {
		// 	$data['hidden_previous_license_type'] = $this->request->post['hidden_previous_license_type'];
		// } elseif (!empty($category_info)) {
		// 	$data['hidden_previous_license_type'] = $category_info['license_type'];
		// } else {
		// 	$data['hidden_previous_license_type'] = '';
		// }

		if (isset($this->request->post['license_type'])) {
			$data['license_type'] = $this->request->post['license_type'];
		} elseif (!empty($category_info)) {
			$data['license_type'] = $category_info['license_type'];
		} else {
			$data['license_type'] = '';
		}
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = '';
		}
		if (isset($this->request->post['private_trainer'])) {
			$data['private_trainer'] = $this->request->post['private_trainer'];
		} elseif (!empty($category_info)) {
			$data['private_trainer'] = $category_info['private_trainer'];
		} else {
			$data['private_trainer'] = '';
		}
		if (isset($this->request->post['file_number'])) {
			$data['file_number'] = $this->request->post['file_number'];
		} elseif (!empty($category_info)) {
			$data['file_number'] = $category_info['file_number'];
		} else {
			$data['file_number'] = '';
		}
		if (isset($this->request->post['remarks'])) {
			$data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($category_info)) {
			$data['remarks'] = $category_info['trainer_remarks'];
		} else {
			$data['remarks'] = '';
		}
		if (isset($this->request->post['mobile_no'])) {
			$data['mobile_no'] = $this->request->post['mobile_no'];
		} elseif (!empty($category_info)) {
			$data['mobile_no'] = $category_info['mobile_no'];
		} else {
			$data['mobile_no'] = '';
		}
		if (isset($this->request->post['email_id'])) {
			$data['email_id'] = $this->request->post['email_id'];
		} elseif (!empty($category_info)) {
			$data['email_id'] = $category_info['email_id'];
		} else {
			$data['email_id'] = '';
		}
		if (isset($this->request->post['alternate_mob_no'])) {
			$data['alternate_mob_no'] = $this->request->post['alternate_mob_no'];
		} elseif (!empty($category_info)) {
			$data['alternate_mob_no'] = $category_info['alternate_mob_no'];
		} else {
			$data['alternate_mob_no'] = '';
		}
		if (isset($this->request->post['alternate_email_id'])) {
			$data['alternate_email_id'] = $this->request->post['alternate_email_id'];
		} elseif (!empty($category_info)) {
			$data['alternate_email_id'] = $category_info['alternate_email_id'];
		} else {
			$data['alternate_email_id'] = '';
		}
		// if (isset($this->request->post['address_line_1'])) {
		// 	$data['address_line_1'] = $this->request->post['address_line_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['address_line_1'] = $category_info['address_line_1'];
		// } else {
		// 	$data['address_line_1'] = '';
		// }
		// if (isset($this->request->post['address_line_2'])) {
		// 	$data['address_line_2'] = $this->request->post['address_line_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['address_line_2'] = $category_info['address_line_2'];
		// } else {
		// 	$data['address_line_2'] = '';
		// }
		// if (isset($this->request->post['locality_1'])) {
		// 	$data['locality_1'] = $this->request->post['locality_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['locality_1'] = $category_info['locality_1'];
		// } else {
		// 	$data['locality_1'] = '';
		// }
		// if (isset($this->request->post['locality_2'])) {
		// 	$data['locality_2'] = $this->request->post['locality_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['locality_2'] = $category_info['locality_2'];
		// } else {
		// 	$data['locality_2'] = '';
		// }
		// if (isset($this->request->post['pin_code_1'])) {
		// 	$data['pin_code_1'] = $this->request->post['pin_code_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['pin_code_1'] = $category_info['pin_code_1'];
		// } else {
		// 	$data['pin_code_1'] = '';
		// }
		// if (isset($this->request->post['pin_code_2'])) {
		// 	$data['pin_code_2'] = $this->request->post['pin_code_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['pin_code_2'] = $category_info['pin_code_2'];
		// } else {
		// 	$data['pin_code_2'] = '';
		// }
		// if (isset($this->request->post['country_1'])) {
		// 	$data['country_1'] = $this->request->post['country_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['country_1'] = $category_info['country_1'];
		// } else {
		// 	$data['country_1'] = '';
		// }
		// if (isset($this->request->post['country_2'])) {
		// 	$data['country_2'] = $this->request->post['country_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['country_2'] = $category_info['country_2'];
		// } else {
		// 	$data['country_2'] = '';
		// }
		if (isset($this->request->post['phone_no'])) {
			$data['phone_no'] = $this->request->post['phone_no'];
		} elseif (!empty($category_info)) {
			$data['phone_no'] = $category_info['phone_no'];
		} else {
			$data['phone_no'] = '';
		}
		// if (isset($this->request->post['city_1'])) {
		// 	$data['city_1'] = $this->request->post['city_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['city_1'] = $category_info['city_1'];
		// } else {
		// 	$data['city_1'] = '';
		// }
		// if (isset($this->request->post['city_2'])) {
		// 	$data['city_2'] = $this->request->post['city_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['city_2'] = $category_info['city_2'];
		// } else {
		// 	$data['city_2'] = '';
		// }
		// if (isset($this->request->post['state_1'])) {
		// 	$data['state_1'] = $this->request->post['state_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['state_1'] = $category_info['state_1'];
		// } else {
		// 	$data['state_1'] = '';
		// }
		// if (isset($this->request->post['state_2'])) {
		// 	$data['state_2'] = $this->request->post['state_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['state_2'] = $category_info['state_2'];
		// } else {
		// 	$data['state_2'] = '';
		// }

		if (isset($this->request->post['address1'])) {
			$data['address_1'] = $this->request->post['address1'];
		} elseif (!empty($category_info)) {
			$data['address_1'] = $category_info['address_line_1'];
		} else {
			$data['address_1'] = '';
		}

		if (isset($this->request->post['address2'])) {
			$data['address_2'] = $this->request->post['address2'];
		} elseif (!empty($category_info)) {
			$data['address_2'] = $category_info['address_line_2'];
		} else {
			$data['address_2'] = '';
		}

		if (isset($this->request->post['address_3'])) {
			$data['address_3'] = $this->request->post['address_3'];
		} elseif (!empty($category_info)) {
			$data['address_3'] = $category_info['address_3'];
		} else {
			$data['address_3'] = '';
		}

		if (isset($this->request->post['address_4'])) {
			$data['address_4'] = $this->request->post['address_4'];
		} elseif (!empty($category_info)) {
			$data['address_4'] = $category_info['address_4'];
		} else {
			$data['address_4'] = '';
		}

		if (isset($this->request->post['localArea1'])) {
			$data['local_area_1'] = $this->request->post['localArea1'];
		} elseif (!empty($category_info)) {
			$data['local_area_1'] = $category_info['locality_1'];
		} else {
			$data['local_area_1'] = '';
		}

		if (isset($this->request->post['localArea2'])) {
			$data['local_area_2'] = $this->request->post['localArea2'];
		} elseif (!empty($category_info)) {
			$data['local_area_2'] = $category_info['locality_2'];
		} else {
			$data['local_area_2'] = '';
		}

		if (isset($this->request->post['state1'])) {
			$data['state1'] = $this->request->post['state1'];
		} elseif (!empty($category_info)) {
			$data['state1'] = $category_info['state_1'];
		} else {
			$data['state1'] = '';
		}

		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = $this->request->post['zone_id'];
		} elseif (!empty($category_info)) {
			$data['zone_id'] = $category_info['state_1'];
		} else {
			$data['zone_id'] = 1493;
		}

		if (isset($this->request->post['blood_group'])) {
			$data['blood_group'] = $this->request->post['blood_group'];
		} elseif (!empty($category_info)) {
			$data['blood_group'] = $category_info['blood_group'];
		} else {
			$data['blood_group'] = 1493;
		}


		if (isset($this->request->post['zone_id2'])) {
			$data['zone_id2'] = $this->request->post['zone_id2'];
		} elseif (!empty($category_info)) {
			$data['zone_id2'] = $category_info['state_2'];
		} else {
			$data['zone_id2'] = 1493;
		}

		if (isset($this->request->post['city1'])) {
			$data['city_1'] = $this->request->post['city1'];
		} elseif (!empty($category_info)) {
			$data['city_1'] = $category_info['city_1'];
		} else {
			$data['city_1'] = '';
		} 

		if (isset($this->request->post['state2'])) {
			$data['state2'] = $this->request->post['state2'];
		} elseif (!empty($category_info)) {
			$data['state2'] = $category_info['state_2'];
		} else {
			$data['state2'] = '';
		}

		if (isset($this->request->post['city2'])) {
			$data['city_2'] = $this->request->post['city2'];
		} elseif (!empty($category_info)) {
			$data['city_2'] = $category_info['city_2'];
		} else {
			$data['city_2'] = '';
		}

		if (isset($this->request->post['pincode1'])) {
			$data['pincode_1'] = $this->request->post['pincode1'];
		} elseif (!empty($category_info)) {
			$data['pincode_1'] = $category_info['pin_code_1'];
		} else {
			$data['pincode_1'] = '';
		}

		if (isset($this->request->post['country1'])) {
			$data['country_1'] = $this->request->post['country1'];
		} elseif (!empty($category_info)) {
			$data['country_1'] = $category_info['country_1'];
		} else {
			$data['country_1'] = 99;
		}

		if (isset($this->request->post['pincode2'])) {
			$data['pincode_2'] = $this->request->post['pincode2'];
		} elseif (!empty($category_info)) {
			$data['pincode_2'] = $category_info['pin_code_2'];
		} else {
			$data['pincode_2'] = '';
		}

		if (isset($this->request->post['country2'])) {
			$data['country_2'] = $this->request->post['country2'];
		} elseif (!empty($category_info)) {
			$data['country_2'] = $category_info['country_2'];
		} else {
			$data['country_2'] = 99;
		}

		if (isset($this->request->post['gst_type'])) {
			$data['gst_type'] = $this->request->post['gst_type'];
		} elseif (!empty($category_info)) {
			$data['gst_type'] = $category_info['gst_type'];
		} else {
			$data['gst_type'] = '';
		}
		if (isset($this->request->post['gst_no'])) {
			$data['gst_no'] = $this->request->post['gst_no'];
		} elseif (!empty($category_info)) {
			$data['gst_no'] = $category_info['gst_no'];
		} else {
			$data['gst_no'] = '';
		}
		if (isset($this->request->post['pan_no'])) {
			$data['pan_no'] = $this->request->post['pan_no'];
		} elseif (!empty($category_info)) {
			$data['pan_no'] = $category_info['pan_no'];
		} else {
			$data['pan_no'] = '';
		}
		if (isset($this->request->post['prof_tax_no'])) {
			$data['prof_tax_no'] = $this->request->post['prof_tax_no'];
		} elseif (!empty($category_info)) {
			$data['prof_tax_no'] = $category_info['prof_tax_no'];
		} else {
			$data['prof_tax_no'] = '';
		}
		if (isset($this->request->post['epf_no'])) {
			$data['epf_no'] = $this->request->post['epf_no'];
		} elseif (!empty($category_info)) {
			$data['epf_no'] = $category_info['epf_no'];
		} else {
			$data['epf_no'] = '';
		}

		$this->load->model('catalog/filter');
		if (isset($this->request->post['category_filter'])) {
			$filters = $this->request->post['category_filter'];
		} elseif (isset($this->request->get['category_id'])) {
			$filters = $this->model_catalog_trainer->getCategoryFilters($this->request->get['category_id']);
		} else {
			$filters = array();
		}

		// echo "<pre>";
		// print_r($category_info);
		// exit;

		if( isset($this->request->post['private_trainers_owner_name'])){
			$data['private_trainers_owner_data'] = $this->request->post['private_trainers_owner_name'];
		} elseif (!empty($category_info) ){
			if($category_info['private_trainer'] == 'Y'){
				$data['private_trainers_owner_data'] = $this->model_catalog_trainer->getPrivateTrainerOwnersDatas($category_info['id']);

			} else {
				$data['private_trainers_owner_data'] = array();
			}
		} else {
			$data['private_trainers_owner_data'] = array();
		}

		$data['category_filters'] = array();
		foreach ($filters as $filter_id) {
			$filter_info = $this->model_catalog_filter->getFilter($filter_id);

			if ($filter_info) {
				$data['category_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}
		$this->load->model('setting/store');
		$data['stores'] = $this->model_setting_store->getStores();
		if (isset($this->request->post['category_store'])) {
			$data['category_store'] = $this->request->post['category_store'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_store'] = $this->model_catalog_trainer->getCategoryStores($this->request->get['category_id']);
		} else {
			$data['category_store'] = array(0);
		}
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = true;
		}
		if (isset($this->request->post['category_layout'])) {
			$data['category_layout'] = $this->request->post['category_layout'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_layout'] = $this->model_catalog_trainer->getCategoryLayouts($this->request->get['category_id']);
		} else {
			$data['category_layout'] = array();
		}
		$data['state'] =  array(
			 'Andhra Pradesh' => 'Andhra Pradesh',
			 'Arunachal Pradesh' => 'Arunachal Pradesh',
			 'Assam' => 'Assam',
			 'Bihar' => 'Bihar',
			 'Chhattisgarh' => 'Chhattisgarh',
			 'Goa' => 'Goa',
			 'Gujarat' => 'Gujarat',
			 'Haryana' => 'Haryana',
			 'Himachal Pradesh' => 'Himachal Pradesh',
			 'Jammu & Kashmir' => 'Jammu & Kashmir',
			 'JH' => 'Jharkhand',
			 'Karnataka' => 'Karnataka',
			 'Kerala' => 'Kerala',
			 'Madhya Pradesh' => 'Madhya Pradesh',
			 'Maharashtra' => 'Maharashtra',
			 'Manipur' => 'Manipur',
			 'Meghalaya' => 'Meghalaya',
			 'Mizoram' => 'Mizoram',
			 'Nagaland' => 'Nagaland',
			 'Odisha' => 'Odisha',
			 'Punjab' => 'Punjab',
			 'RJ' => 'Rajasthan',
			 'Rajasthan' => 'Sikkim',
			 'Tamil Nadu' => 'Tamil Nadu',
			 'Tripura' => 'Tripura',
			 'Uttarakhand' => 'Uttarakhand',
			 'Uttar Pradesh' => 'Uttar Pradesh',
			 'West Bengal' => 'West Bengal',
		);
		$data['fees'] =  array(
			'Cash' => 'Cash',
			'Debit' => 'Debit',
			'Wita' => 'Wita',
			'Internet' => 'Internet',
		);
		$data['trainer_option'] =  array(
			'N' => 'No',
			'Y' => 'Yes',
			
		);
		$data['license_type1'] =  array(
			'A' => 'A',
			'B'   => 'B',
			'' =>'None',
		);
		$data['owner_autority'] =  array(
			'Y' => 'Yes',
			'N'   => 'No',
			
		);
		// $data['status1']=array(
		// 	'A' =>'Enable',
		// 	'I' =>'Disable',
		// );


		$data['status1'] = array(
			'RWITC'  => 'RWITC',
			'RCTC'  => 'RCTC',
			'DRC'  => 'DRC',
			'BTC'  => 'BTC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'MYRC'  => 'MYRC',
		);

		$data['gst_type1']=array(
			'R' =>'Registered',
			'N' =>'Non-Registered',
		);
		$data['trainer_clubs'] = array(
			'RWITC'  => 'RWITC',
			'RCTC'  => 'RCTC',
			'DRC'  => 'DRC',
			'BTC'  => 'BTC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'MYRC'  => 'MYRC',
		);
		$data['authorty_bans'] = array(
			'SS'  => 'SS',
			'SM'  => 'SM',
			'SC'  => 'SC',
			'BA'  => 'BA',
			'MS'  => 'MS',
			'MC'  => 'MC',
			'MB'  => 'MB',
		);
		$this->load->model('design/layout');
		$data['layouts'] = $this->model_design_layout->getLayouts();
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		if(isset($authoritys)){
			$data['authorityarray'] = array();
		}else{
			$category_total1 = $this->model_catalog_trainer->getTotalAuthority();
			if(isset($this->request->get['id'])){
				$authoritys = $this->model_catalog_trainer->getAuthority($this->request->get['id']);
			}
			if(isset($authoritys)){
				foreach ($authoritys as $authority) {
					if($authority['start_date'] == '1970-01-01' || $authority['start_date'] == '0000-00-00'){
						$startdate = ''; 
					} else {
						$startdate = date('d-m-Y', strtotime($authority['start_date']));
					}

					if ($authority['end_date'] == '2050-12-31') {
						$enddate = '31-12-2050';
					} else {
						if($authority['end_date'] == '1970-01-01' || $authority['end_date'] == '0000-00-00'){
							$enddate = '';
						} else {
							$enddate = date('d-m-Y', strtotime($authority['end_date']));
						}
					}
					$data['authorityarray'][] = array(
						'autho_id'=> $authority['autho_id'],
						'check_name' => $authority['check_name'],
						'start_date' => $startdate,
						'end_date'	=> $enddate,
						'sub_auth_id'	=>$authority['sub_auth_id'],
						'owner_id'  => $authority['owner_id'],
						'edit'        => $this->url->link('catalog/trainer/edit1', 'token=' . $this->session->data['token'] .'&autho_id=' .$authority['autho_id'].'&owner_id='.$authority['owner_id']. $url, true)
					);
				}
			}
		}

		if(isset($Subauthoritys)){
			$data['Subauthorityarray'] = array();
		}else{
			$category_total1 = $this->model_catalog_trainer->getTotalSubAuthority();
			if(isset($this->request->get['id'])){
				$Subauthoritys = $this->model_catalog_trainer->getSubAuthority($this->request->get['id']);
			}
			if(isset($Subauthoritys)){
				foreach ($Subauthoritys as $Subauthority) {
					if($Subauthority['start_date'] == '1970-01-01' || $Subauthority['start_date'] == '0000-00-00'){
						$startdate = ''; 
					} else {
						$startdate = date('d-m-Y', strtotime($Subauthority['start_date']));
					}

					if ($Subauthority['end_date'] == '2050-12-31') {
						$enddate = '31-12-2050';
					} else {
						if($Subauthority['end_date'] == '1970-01-01' || $Subauthority['end_date'] == '0000-00-00'){
							$enddate = '';
						} else {
							$enddate = date('d-m-Y', strtotime($Subauthority['end_date']));
						}
					}
					$data['Subauthorityarray'][] = array(
						'sub_autho_id'=> $Subauthority['sub_autho_id'],
						'owner_name' => $Subauthority['owner_name'],
						'start_date' => $startdate,
						'end_date'	=> $enddate,
						'owner_id'  => $Subauthority['owner_id'],
						'edit'        => $this->url->link('catalog/trainer/edit2', 'token=' . $this->session->data['token'] .'&owner_id='.$Subauthority['owner_id']. $url, true)
					);
				}
			}
		}
		$data['horseDatas'] = array();
		if(isset($this->request->get['id'])){
			$traineritrs = $this->model_catalog_trainer->getTraineritr($this->request->get['id']);
			$baninfo_with_date_end =  $this->model_catalog_trainer->getBanhistory($this->request->get['id']);//FOR BAN HISTORY
			$staff_details = $this->model_catalog_trainer->getStaffs($this->request->get['id']);
			$baninfo_witout_date_end =  $this->model_catalog_trainer->getBandetails($this->request->get['id']);//FOR BAN DETAILS
    		$horses = $this->db->query("SELECT * FROM `horse1` h LEFT JOIN horse_to_trainer ht ON h.horseseq =ht.horse_id WHERE ht.trainer_id = '".$this->request->get['id']."' ");


    		foreach ($horses->rows as $horse) {
    			//echo "<pre>";print_r($horse);exit;

    			$current_owners = $this->db->query("SELECT to_owner FROM horse_to_owner WHERE horse_id = '".$horse['horseseq']."' AND owner_share_status = 1 ");
    			$owners = "";
    			
    			if($current_owners->num_rows > 0){
    				foreach ($current_owners->rows as $okey => $ovalue) {
    					$owners .= $ovalue['to_owner'].',<br/>';
    				}
    			}


    			//echo "<pre>";print_r($final_owners);exit;


    			$equipments = $this->db->query("SELECT * FROM `horse_equipments` WHERE horse_id = '".$horse['horseseq']."' ");
    			$final_equipment_name = '';
				if ($equipments->num_rows >0) {
					foreach ($equipments->rows as $akey => $avalue) {
						$short_name = $this->db->query("SELECT short_name FROM equipment WHERE equipment_name = '".$avalue['equipment_name']."' ");
					
						if ($short_name->num_rows > 0) {
							$final_equipment_name .= '</b>'.$short_name->row['short_name'].', '.'<b>';
						} else {
							$final_equipment_name .= '</b>'.$avalue['equipment_name'].', '.'<b>';
						}


						//$final_equipment_name .= '</b>'.$avalue['equipment_name'].'<br>'.'<b>';
					}
					
				}

				$current_date = date("Y-m-d");
				$ban_sql = "SELECT * FROM `horse_ban` WHERE horse_id = '".$horse['horseseq']."' ORDER BY `horse_ban_id` DESC LIMIT 1 ";
				$ban = $this->db->query($ban_sql);
				//echo "<pre>";print_r($ban);
				$final_ban = '';
				if ($ban->num_rows >0) {
						if ($ban->row['enddate_ban'] >= $current_date) {
							$final_ban .= '</b>'.$ban->row['club_ban'].'-'.$ban->row['authority'].'<br>'.'<b>';
						}else{
							$final_ban = '';
						}
					
				}

				$shoe_name_sql = "SELECT * FROM `horse_shoeing` WHERE horse_id = '".$horse['horseseq']."' ORDER BY `horse_shoeing_id` DESC LIMIT 1 ";
				$shoe_name = $this->db->query($shoe_name_sql);
				//echo "<pre>";print_r($shoe_name);
				$bit_name_sql = "SELECT * FROM `horse_bits` WHERE horse_id = '".$horse['horseseq']."' ORDER BY `id` DESC LIMIT 1 ";

				$bit_name = $this->db->query($bit_name_sql);
				//echo "<pre>";print_r($bit_name);

				$final_shoe_name = '';
				if ($shoe_name->num_rows >0 && $bit_name->num_rows >0) {
					//foreach ($shoe_name->rows as $akey => $avalue) {
						if ($shoe_name->row['type'] == 'Aluminium') {
							$type = 'A';
						} elseif ($shoe_name->row['type'] == 'Steel') {
							$type = 'S';
						} else{
							$type = '';
						}
						$final_shoe_name .= '</b>'.$type.' '.$shoe_name->row['shoe_description'].' '.$bit_name->row['short_name'].'<br>'.'<b>';
					//}
					
				}
				$data['horseDatas'][] = array(
					'official_name'=> $horse['official_name'],
					'age' => $horse['age'],
					'date_of_charge' => date('d-m-Y', strtotime($horse['date_of_charge'])),
					'extra_narration'	=> $horse['extra_narration'],
					'sire_name'  => $horse['sire_name'],
					'dam_name'  => $horse['dam_name'],
					'horse_equipment_name' => $final_equipment_name,
					'owners' => $owners,
					'ban' => $final_ban,
					'shoe_name' => $final_shoe_name
					
				);
    		}//exit;
		}
		/*echo '<pre>';
		print_r($traineritrs);
		exit;*/
		if (!empty($horses)) {
			$data['horses'] = $horses;
		} else {
      		$data['horses'] = array();
    	}
		if (isset($this->request->post['banhistorydatas'])) {
      		$data['bandatas'] = $baninfo_with_date_end;
    	} elseif (!empty($baninfo_with_date_end)) {
			$data['bandatas'] = $baninfo_with_date_end;
		} else {
      		$data['bandatas'] = array();
    	}
    	if (!empty($staff_details)) {
			$data['staffs'] = $staff_details;
		} else {
      		$data['staffs'] = array();
    	}
    	if (isset($this->request->post['bandats'])) {
      		$data['bandeatils'] = $this->request->post['bandats'];
    	} elseif (!empty($baninfo_witout_date_end)) {
			$data['bandeatils'] = $baninfo_witout_date_end;
		} else {
      		$data['bandeatils'] = array();
    	}
    	if (isset($this->request->post['id_hidden_band'])) {
      		$data['id_hidden_band'] = $this->request->post['id_hidden_band'];
    	} elseif (!empty($baninfo_witout_date_end)) {
			$ban_counts = count($baninfo_witout_date_end);
			$data['id_hidden_band'] = $ban_counts;
		} else {
      		$data['id_hidden_band'] = 1;
    	}
		if (!empty($traineritrs)) {
			$data['uploaditr'] = $traineritrs;
		} else {
      		$data['uploaditr'] = array();
    	}
    	if (isset($this->request->post['upload_hidden_id'])) {
      		$data['upload_hidden_id'] = $this->request->post['upload_hidden_id'];
    	} elseif (!empty($traineritrs)) {
			$traineritrs = count($traineritrs);
			$data['upload_hidden_id'] = $traineritrs;
		} else {
      		$data['upload_hidden_id'] = 1;
    	}

    	$data['trainer_license'] = $this->url->link('transaction/trainer_license_trans', 'token=' . $this->session->data['token'] . '&filter_trainer='.$data['trainer_name'].'', true);
    	$data['staffEntry'] = $this->url->link('catalog/trainer_staff', 'token=' . $this->session->data['token'], true);
    	
		$pagination = new Pagination();
		$pagination->total = $category_total1;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/trainer/add', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination1'] = $pagination->render();

		$data['authoritys'] = sprintf($this->language->get('text_pagination'), ($category_total1) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total1 - $this->config->get('config_limit_admin'))) ? $category_total1 : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total1, ceil($category_total1 / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;
		$this->response->setOutput($this->load->view('catalog/trainer_form', $data));
	}

	protected function getFormview() {
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		$users_id = $this->session->data['user_id'];
		$user_group_ids = $this->db->query("SELECT * FROM oc_user WHERE user_id = '".$users_id."' ");
		if ($user_group_ids->num_rows > 0) {
			$data['group_id'] = $user_group_ids->row['user_group_id'];
		} else {
			$data['group_id'] = '';
		}
		// echo '<pre>';
		// print_r($data['group_id']);
		// 	exit;
		$data['itrUploadDatas'] = array();

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}
		if (isset($this->error['error_status'])) {
			$data['error_status'] = $this->error['error_status'];
		} else {
			$data['error_status'] = '';
		}

		if (isset($this->error['gst_errors'])) {
			$data['gst_errors'] = $this->error['gst_errors'];
		} else {
			$data['gst_errors'] = '';
		}

		if (isset($this->error['pan_errors'])) {
			$data['pan_errors'] = $this->error['pan_errors'];
		} else {
			$data['pan_errors'] = '';
		}

		if (isset($this->error['valierr_trainer_name'])) {
			$data['valierr_trainer_name'] = $this->error['valierr_trainer_name'];
		}
		// if (isset($this->error['valierr_trainer_code'])) {
		// 	$data['valierr_trainer_code'] = $this->error['valierr_trainer_code'];
		// }
		if (isset($this->error['valierr_date_of_birth'])) {
			$data['valierr_date_of_birth'] = $this->error['valierr_date_of_birth'];
		}
		if (isset($this->error['valierr_license_issue_date'])) {
			$data['valierr_license_issue_date'] = $this->error['valierr_license_issue_date'];
		}
		if (isset($this->error['valierr_license_renewal_date'])) {
			$data['valierr_license_renewal_date'] = $this->error['valierr_license_renewal_date'];
		}
		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}
		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}
		if (isset($this->error['valierr_trainer_code'])) {
			$data['valierr_trainer_code'] = $this->error['valierr_trainer_code'];
		} else {
			$data['valierr_trainer_code'] = '';
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . $url, true)
		);
		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/trainer/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/trainer/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}
		if (isset($this->request->get['back'])) {
			$data['cancel'] = $this->url->link('catalog/trainer_at_glance', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['cancel'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'] . $url, true);
		}

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_trainer->getTrainer($this->request->get['id']);
		}
		
		if(isset($this->request->get['autho_id']) && isset($this->request->get['owner_id'])){
			$data['owner_info'] = $this->model_catalog_trainer->editCategory1($this->request->get['autho_id']);
			$this->response->setOutput($this->load->view('catalog/trainer_form', $data));
		}
		if (!empty($owner_info)) {
				$data['check_name'] = $owner_info['check_name'];
		} else {
			$data['check_name'] = '';
		}
		$data['token'] = $this->session->data['token'];
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		//echo "<pre>";print_r($category_info);exit;

		if (isset($this->request->post['isActive'])) {
			$data['isActive'] = $this->request->post['isActive'];
		} elseif (!empty($category_info)) {
			$data['isActive'] = $category_info['active'];
		} else {
			$data['isActive'] = '1';
		}

		if (!empty($category_info)) {
			$data['last_upadate'] = date("d-m-Y h:i:s", strtotime($category_info['last_update_date']));
		} else {
			$data['last_upadate'] = '';
		}

		if (!empty($category_info) && $category_info['user_id'] != 0) {
			$user_name = $this->db->query("SELECT username FROM oc_user WHERE user_id = '".$category_info['user_id']."' ")->row;
			$data['user'] = $user_name['username'] ;
		} else {
			$data['user'] = '';
		}


		if (isset($this->request->post['hidden_active'])) {
			$data['isActive1'] = $this->request->post['hidden_active'];
		} elseif (!empty($category_info)) {
			$data['isActive1'] = $category_info['active'];
		} else {
			$data['isActive1'] = '';
		}

		$data['assistant_trainers'] = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5'
		);

		$data['Active'] = array('0'	=> 'Exit',
							  '1'	=> 'In');

		//$results = $this->model_catalog_trainer->getTrainerFilter($this->request->get['id']);
		$data['Trainers'] = array();
		if (isset($this->request->get['id'])) {
		$data['trainer_idss'] = $this->request->get['id'];
			$results = $this->db->query("SELECT * FROM trainer_renewal_history WHERE `trainer_id` = '".$this->request->get['id']."' ")->rows;
			foreach ($results as $result) {
				$data['Trainers'][] = array(
					'id' => $result['id'],
					'renewal_date' => date('d-m-Y', strtotime($result['renewal_date'])),
					'amount' => $result['amount'],
					'fees' => $result['fees'],
					'license_start_date' => date('d-m-Y', strtotime($result['license_start_date'])),
					'license_end_date' => date('d-m-Y', strtotime($result['license_end_date'])),
					'license_type' => $result['license_type'],
					'apprenties' => $result['apprenties'],
					'delete' => $this->url->link('catalog/trainer/delete_transaction')
				);
				
			}
		}

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();
		$data['countries2'] = $this->model_localisation_country->getCountries();
		// echo'<pre>';
		// print_r($data['countries']);
		// exit;
		$data['zones'] = $this->model_localisation_country->getZone();
		$data['zones2'] = $this->model_localisation_country->getZone();


		if (isset($this->request->post['assistant_trainer'])) {
			$data['assistant_trainer'] = $this->request->post['assistant_trainer'];
		} elseif (!empty($category_info)) {
			if($category_info['assistant_trainer_count'] != 'NA'){
				$data['assistant_trainer'] = $category_info['assistant_trainer_count'];
			} else {
				$data['assistant_trainer'] = '';
			}
		} else {
			$data['assistant_trainer'] = '';
		}


		if (isset($this->request->post['file_number_uploads'])) {
			$data['file_number_uploads'] = $this->request->post['file_number_uploads'];
		} elseif (!empty($category_info)) {
			$data['file_number_uploads'] = $category_info['file_number_uploads'];
		} else {
			$data['file_number_uploads'] = '';
		}

		if (!empty($category_info)) {
    		// $img = ltrim($category_info['image_source'],"/");
    		if($category_info['uploaded_file_sources'] != ''){
    			$data['img_path'] = $category_info['uploaded_file_sources'];
    		} else {
    			$data['img_path'] = '1';
    		}
		} else {
      		$data['img_path'] = '#';
    	}
		
		if (isset($this->request->post['uploaded_file_sources'])) {
			$data['uploaded_file_sources'] = $this->request->post['uploaded_file_sources'];
		} elseif (!empty($category_info)) {
			$data['uploaded_file_sources'] = $category_info['uploaded_file_sources'];
		} else {
			$data['uploaded_file_sources'] = '';
		}

		if (isset($this->request->post['assistant_trainer_1'])) {
			$data['assistant_trainer_1'] = $this->request->post['assistant_trainer_1'];
		} elseif (!empty($category_info)) {
			if($category_info['assistant_trainer_1'] != 'NA'){
				$data['assistant_trainer_1'] = $category_info['assistant_trainer_1'];
			} else {
				$data['assistant_trainer_1'] = '';
			}
		} else {
			$data['assistant_trainer_1'] = '';
		}

		if (isset($this->request->post['assistant_trainer_2'])) {
			$data['assistant_trainer_2'] = $this->request->post['assistant_trainer_2'];
		} elseif (!empty($category_info)) {
			if($category_info['assistant_trainer_2'] != 'NA'){
				$data['assistant_trainer_2'] = $category_info['assistant_trainer_2'];
			} else {
				$data['assistant_trainer_2'] = '';
			}
		} else {
			$data['assistant_trainer_2'] = '';
		}

		if (isset($this->request->post['assistant_trainer_3'])) {
			$data['assistant_trainer_3'] = $this->request->post['assistant_trainer_3'];
		} elseif (!empty($category_info)) {
			if($category_info['assistant_trainer_3'] != 'NA'){
				$data['assistant_trainer_3'] = $category_info['assistant_trainer_3'];
			} else {
				$data['assistant_trainer_3'] = '';
			}
		} else {
			$data['assistant_trainer_3'] = '';
		}
		
		if (isset($this->request->post['assistant_trainer_4'])) {
			$data['assistant_trainer_4'] = $this->request->post['assistant_trainer_4'];
		} elseif (!empty($category_info)) {
			if($category_info['assistant_trainer_4'] != 'NA'){
				$data['assistant_trainer_4'] = $category_info['assistant_trainer_4'];
			} else {
				$data['assistant_trainer_4'] = '';
			}
		} else {
			$data['assistant_trainer_4'] = '';
		}

		if (isset($this->request->post['assistant_trainer_5'])) {
			$data['assistant_trainer_5'] = $this->request->post['assistant_trainer_5'];
		} elseif (!empty($category_info)) {
			$data['assistant_trainer_5'] = $category_info['assistant_trainer_5'];
		} else {
			$data['assistant_trainer_5'] = '';
		}

		if (isset($this->request->post['is_wita'])) {
			$data['is_wita'] = $this->request->post['is_wita'];
		} elseif (!empty($category_info)) {
			$data['is_wita'] = $category_info['is_wita'];
		} else {
			$data['is_wita'] = '';
		}

		if (isset($this->request->post['id'])) {
			$data['id'] = $this->request->post['id'];
		} elseif (!empty($category_info)) {
			$data['id'] = $category_info['id'];
		} else {
			$data['id'] = '';
		}
		if (isset($this->request->post['trainer_name'])) {
			$data['trainer_name'] = $this->request->post['trainer_name'];
		} elseif (!empty($category_info)) {
			$data['trainer_name'] = $category_info['name'];
		} else {
			$data['trainer_name'] = '';
		}
		// if (isset($this->request->post['license_amt'])) {
		// 	$data['license_amt'] = $this->request->post['license_amt'];
		// } elseif (!empty($category_info)) {
		// 	$data['license_amt'] = $category_info['license_amt'];
		// } else {
		// 	$data['license_amt'] = '';
		// }

		$trainer_codes = $this->db->query("SELECT trainer_code FROM trainers WHERE 1=1 ORDER BY trainer_code DESC LIMIT 1 ");
		/*echo'<pre>';
		print_r("SELECT med_code FROM medicine WHERE 1=1 ORDER BY id DESC LIMIT 1 ");
		exit;*/
		if ($trainer_codes->num_rows > 0) {
			//echo "<pre>";print_r($trainer_codes->row);exit;
			$trainer_code = $trainer_codes->row['trainer_code'];
		} else {
			$trainer_code = '';
		}

		if (isset($this->request->post['trainer_code'])) {
			$data['trainer_code'] = $this->request->post['trainer_code'];
		} elseif (!empty($category_info)) {
			$data['trainer_code'] = 'T'.$category_info['trainer_code'];
		} elseif (isset($this->request->post['hidden_trainer_code'])) {
			$data['trainer_code'] = $this->request->post['hidden_trainer_code'];
		} else {
			$addTcode = $trainer_code + 1;
			$data['trainer_code'] = 'T'.$addTcode;
		}
		if (isset($this->request->post['arrival_charges_to_be_paid'])) {
			$data['arrival_charges_to_be_paid'] = $this->request->post['arrival_charges_to_be_paid'];
		} elseif (!empty($category_info)) {
			$data['arrival_charges_to_be_paid'] = $category_info['arrival_charges_to_be_paid'];
		} else {
			$data['arrival_charges_to_be_paid'] = '';
		}
		if (isset($this->request->post['racing_name'])) {
			$data['racing_name'] = $this->request->post['racing_name'];
		} elseif (!empty($category_info)) {
			$data['racing_name'] = $category_info['racing_name'];
		} else {
			$data['racing_name'] = '';
		}

		if (isset($this->request->post['trainer_code_new'])) {
			$data['trainer_code_new'] = $this->request->post['trainer_code_new'];
		} elseif (!empty($category_info)) {
			$data['trainer_code_new'] = $category_info['trainer_code_new'];
		} else {
			$data['trainer_code_new'] = '';
		}

		if (isset($this->request->post['date_of_birth'])) {
				$data['date_of_birth'] = $this->request->post['date_of_birth'];
		} elseif (!empty($category_info)) {
			if($category_info['date_of_birth'] == '1970-01-01' || $category_info['date_of_birth'] == '0000-00-00'){
				$data['date_of_birth'] = '';
			} else {
				$data['date_of_birth'] = date('d-m-Y', strtotime($category_info['date_of_birth']));
			}
		} else {
			$data['date_of_birth'] = '';
		}

		if (isset($this->request->post['date_of_license_issue'])) {
			$data['date_of_license_issue'] = $this->request->post['date_of_license_issue'];
		} elseif (!empty($category_info)) {
			if($category_info['date_of_license_issue'] == '1970-01-01' || $category_info['date_of_license_issue'] == '0000-00-00'){
				$data['date_of_license_issue'] = '';
			} else {
				$data['date_of_license_issue'] = date('d-m-Y', strtotime($category_info['date_of_license_issue']));
			}
		} else {
			$data['date_of_license_issue'] = '';
		}

		if (isset($this->request->post['date_of_license_issue2'])) {
			$data['date_of_license_issue2'] = $this->request->post['date_of_license_issue2'];
		} elseif (!empty($category_info)) {
			if($category_info['date_of_license_issue2'] == '1970-01-01' || $category_info['date_of_license_issue2'] == '0000-00-00'){
				$data['date_of_license_issue2'] = '';
			} else {
				$data['date_of_license_issue2'] = date('d-m-Y', strtotime($category_info['date_of_license_issue2']));
			}
		} else {
			$data['date_of_license_issue2'] = '';
		}

		if (isset($this->request->post['date_of_license_renewal'])) {
			$data['date_of_license_renewal'] = $this->request->post['date_of_license_renewal'];
		} elseif (!empty($category_info)) {
			if($category_info['date_of_license_renewal'] == '1970-01-01' || $category_info['date_of_license_renewal'] == '0000-00-00'){
				$data['date_of_license_renewal'] = '';
			} else {
				$data['date_of_license_renewal'] = date('d-m-Y', strtotime($category_info['date_of_license_renewal']));
			}
		} else {
			$data['date_of_license_renewal'] = '';
		}

		if (isset($this->request->post['hidden_date_of_license_renewal'])) {
			$data['hidden_date_of_license_renewal'] = $this->request->post['hidden_date_of_license_renewal'];
		} else {
			$data['hidden_date_of_license_renewal'] = '';
		}

		if (isset($this->request->post['license_type'])) {
			$data['license_type'] = $this->request->post['license_type'];
		} elseif (!empty($category_info)) {
			$data['license_type'] = $category_info['license_type'];
		} else {
			$data['license_type'] = '';
		}
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = '';
		}
		if (isset($this->request->post['private_trainer'])) {
			$data['private_trainer'] = $this->request->post['private_trainer'];
		} elseif (!empty($category_info)) {
			$data['private_trainer'] = $category_info['private_trainer'];
		} else {
			$data['private_trainer'] = '';
		}
		if (isset($this->request->post['file_number'])) {
			$data['file_number'] = $this->request->post['file_number'];
		} elseif (!empty($category_info)) {
			$data['file_number'] = $category_info['file_number'];
		} else {
			$data['file_number'] = '';
		}
		if (isset($this->request->post['remarks'])) {
			$data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($category_info)) {
			$data['remarks'] = $category_info['trainer_remarks'];
		} else {
			$data['remarks'] = '';
		}
		if (isset($this->request->post['mobile_no'])) {
			$data['mobile_no'] = $this->request->post['mobile_no'];
		} elseif (!empty($category_info)) {
			$data['mobile_no'] = $category_info['mobile_no'];
		} else {
			$data['mobile_no'] = '';
		}
		if (isset($this->request->post['email_id'])) {
			$data['email_id'] = $this->request->post['email_id'];
		} elseif (!empty($category_info)) {
			$data['email_id'] = $category_info['email_id'];
		} else {
			$data['email_id'] = '';
		}
		if (isset($this->request->post['alternate_mob_no'])) {
			$data['alternate_mob_no'] = $this->request->post['alternate_mob_no'];
		} elseif (!empty($category_info)) {
			$data['alternate_mob_no'] = $category_info['alternate_mob_no'];
		} else {
			$data['alternate_mob_no'] = '';
		}
		if (isset($this->request->post['alternate_email_id'])) {
			$data['alternate_email_id'] = $this->request->post['alternate_email_id'];
		} elseif (!empty($category_info)) {
			$data['alternate_email_id'] = $category_info['alternate_email_id'];
		} else {
			$data['alternate_email_id'] = '';
		}
		// if (isset($this->request->post['address_line_1'])) {
		// 	$data['address_line_1'] = $this->request->post['address_line_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['address_line_1'] = $category_info['address_line_1'];
		// } else {
		// 	$data['address_line_1'] = '';
		// }
		// if (isset($this->request->post['address_line_2'])) {
		// 	$data['address_line_2'] = $this->request->post['address_line_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['address_line_2'] = $category_info['address_line_2'];
		// } else {
		// 	$data['address_line_2'] = '';
		// }
		// if (isset($this->request->post['locality_1'])) {
		// 	$data['locality_1'] = $this->request->post['locality_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['locality_1'] = $category_info['locality_1'];
		// } else {
		// 	$data['locality_1'] = '';
		// }
		// if (isset($this->request->post['locality_2'])) {
		// 	$data['locality_2'] = $this->request->post['locality_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['locality_2'] = $category_info['locality_2'];
		// } else {
		// 	$data['locality_2'] = '';
		// }
		// if (isset($this->request->post['pin_code_1'])) {
		// 	$data['pin_code_1'] = $this->request->post['pin_code_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['pin_code_1'] = $category_info['pin_code_1'];
		// } else {
		// 	$data['pin_code_1'] = '';
		// }
		// if (isset($this->request->post['pin_code_2'])) {
		// 	$data['pin_code_2'] = $this->request->post['pin_code_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['pin_code_2'] = $category_info['pin_code_2'];
		// } else {
		// 	$data['pin_code_2'] = '';
		// }
		// if (isset($this->request->post['country_1'])) {
		// 	$data['country_1'] = $this->request->post['country_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['country_1'] = $category_info['country_1'];
		// } else {
		// 	$data['country_1'] = '';
		// }
		// if (isset($this->request->post['country_2'])) {
		// 	$data['country_2'] = $this->request->post['country_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['country_2'] = $category_info['country_2'];
		// } else {
		// 	$data['country_2'] = '';
		// }
		if (isset($this->request->post['phone_no'])) {
			$data['phone_no'] = $this->request->post['phone_no'];
		} elseif (!empty($category_info)) {
			$data['phone_no'] = $category_info['phone_no'];
		} else {
			$data['phone_no'] = '';
		}
		// if (isset($this->request->post['city_1'])) {
		// 	$data['city_1'] = $this->request->post['city_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['city_1'] = $category_info['city_1'];
		// } else {
		// 	$data['city_1'] = '';
		// }
		// if (isset($this->request->post['city_2'])) {
		// 	$data['city_2'] = $this->request->post['city_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['city_2'] = $category_info['city_2'];
		// } else {
		// 	$data['city_2'] = '';
		// }
		// if (isset($this->request->post['state_1'])) {
		// 	$data['state_1'] = $this->request->post['state_1'];
		// } elseif (!empty($category_info)) {
		// 	$data['state_1'] = $category_info['state_1'];
		// } else {
		// 	$data['state_1'] = '';
		// }
		// if (isset($this->request->post['state_2'])) {
		// 	$data['state_2'] = $this->request->post['state_2'];
		// } elseif (!empty($category_info)) {
		// 	$data['state_2'] = $category_info['state_2'];
		// } else {
		// 	$data['state_2'] = '';
		// }

		if (isset($this->request->post['address1'])) {
			$data['address_1'] = $this->request->post['address1'];
		} elseif (!empty($category_info)) {
			$data['address_1'] = $category_info['address_line_1'];
		} else {
			$data['address_1'] = '';
		}

		if (isset($this->request->post['address2'])) {
			$data['address_2'] = $this->request->post['address2'];
		} elseif (!empty($category_info)) {
			$data['address_2'] = $category_info['address_line_2'];
		} else {
			$data['address_2'] = '';
		}

		if (isset($this->request->post['address_3'])) {
			$data['address_3'] = $this->request->post['address_3'];
		} elseif (!empty($category_info)) {
			$data['address_3'] = $category_info['address_3'];
		} else {
			$data['address_3'] = '';
		}

		if (isset($this->request->post['address_4'])) {
			$data['address_4'] = $this->request->post['address_4'];
		} elseif (!empty($category_info)) {
			$data['address_4'] = $category_info['address_4'];
		} else {
			$data['address_4'] = '';
		}

		if (isset($this->request->post['localArea1'])) {
			$data['local_area_1'] = $this->request->post['localArea1'];
		} elseif (!empty($category_info)) {
			$data['local_area_1'] = $category_info['locality_1'];
		} else {
			$data['local_area_1'] = '';
		}

		if (isset($this->request->post['localArea2'])) {
			$data['local_area_2'] = $this->request->post['localArea2'];
		} elseif (!empty($category_info)) {
			$data['local_area_2'] = $category_info['locality_2'];
		} else {
			$data['local_area_2'] = '';
		}

		if (isset($this->request->post['state1'])) {
			$data['state1'] = $this->request->post['state1'];
		} elseif (!empty($category_info)) {
			$data['state1'] = $category_info['state_1'];
		} else {
			$data['state1'] = '';
		}

		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = $this->request->post['zone_id'];
		} elseif (!empty($category_info)) {
			$data['zone_id'] = $category_info['state_1'];
		} else {
			$data['zone_id'] = '';
		}


		if (isset($this->request->post['zone_id2'])) {
			$data['zone_id2'] = $this->request->post['zone_id2'];
		} elseif (!empty($category_info)) {
			$data['zone_id2'] = $category_info['state_2'];
		} else {
			$data['zone_id2'] = '';
		}

		if (isset($this->request->post['city1'])) {
			$data['city_1'] = $this->request->post['city1'];
		} elseif (!empty($category_info)) {
			$data['city_1'] = $category_info['city_1'];
		} else {
			$data['city_1'] = '';
		} 

		if (isset($this->request->post['state2'])) {
			$data['state2'] = $this->request->post['state2'];
		} elseif (!empty($category_info)) {
			$data['state2'] = $category_info['state_2'];
		} else {
			$data['state2'] = '';
		}

		if (isset($this->request->post['city2'])) {
			$data['city_2'] = $this->request->post['city2'];
		} elseif (!empty($category_info)) {
			$data['city_2'] = $category_info['city_2'];
		} else {
			$data['city_2'] = '';
		}

		if (isset($this->request->post['pincode1'])) {
			$data['pincode_1'] = $this->request->post['pincode1'];
		} elseif (!empty($category_info)) {
			$data['pincode_1'] = $category_info['pin_code_1'];
		} else {
			$data['pincode_1'] = '';
		}

		if (isset($this->request->post['country1'])) {
			$data['country_1'] = $this->request->post['country1'];
		} elseif (!empty($category_info)) {
			$data['country_1'] = $category_info['country_1'];
		} else {
			$data['country_1'] = '';
		}

		if (isset($this->request->post['pincode2'])) {
			$data['pincode_2'] = $this->request->post['pincode2'];
		} elseif (!empty($category_info)) {
			$data['pincode_2'] = $category_info['pin_code_2'];
		} else {
			$data['pincode_2'] = '';
		}

		if (isset($this->request->post['country2'])) {
			$data['country_2'] = $this->request->post['country2'];
		} elseif (!empty($category_info)) {
			$data['country_2'] = $category_info['country_2'];
		} else {
			$data['country_2'] = '';
		}

		if (isset($this->request->post['gst_type'])) {
			$data['gst_type'] = $this->request->post['gst_type'];
		} elseif (!empty($category_info)) {
			$data['gst_type'] = $category_info['gst_type'];
		} else {
			$data['gst_type'] = '';
		}
		if (isset($this->request->post['gst_no'])) {
			$data['gst_no'] = $this->request->post['gst_no'];
		} elseif (!empty($category_info)) {
			$data['gst_no'] = $category_info['gst_no'];
		} else {
			$data['gst_no'] = '';
		}
		if (isset($this->request->post['pan_no'])) {
			$data['pan_no'] = $this->request->post['pan_no'];
		} elseif (!empty($category_info)) {
			$data['pan_no'] = $category_info['pan_no'];
		} else {
			$data['pan_no'] = '';
		}
		if (isset($this->request->post['prof_tax_no'])) {
			$data['prof_tax_no'] = $this->request->post['prof_tax_no'];
		} elseif (!empty($category_info)) {
			$data['prof_tax_no'] = $category_info['prof_tax_no'];
		} else {
			$data['prof_tax_no'] = '';
		}
		if (isset($this->request->post['epf_no'])) {
			$data['epf_no'] = $this->request->post['epf_no'];
		} elseif (!empty($category_info)) {
			$data['epf_no'] = $category_info['epf_no'];
		} else {
			$data['epf_no'] = '';
		}

		$this->load->model('catalog/filter');
		if (isset($this->request->post['category_filter'])) {
			$filters = $this->request->post['category_filter'];
		} elseif (isset($this->request->get['category_id'])) {
			$filters = $this->model_catalog_trainer->getCategoryFilters($this->request->get['category_id']);
		} else {
			$filters = array();
		}

		// echo "<pre>";
		// print_r($category_info);
		// exit;

		if( isset($this->request->post['private_trainers_owner_name'])){
			$data['private_trainers_owner_data'] = $this->request->post['private_trainers_owner_name'];
		} elseif (!empty($category_info) ){
			if($category_info['private_trainer'] == 'Y'){
				$data['private_trainers_owner_data'] = $this->model_catalog_trainer->getPrivateTrainerOwnersDatas($category_info['id']);

			} else {
				$data['private_trainers_owner_data'] = array();
			}
		} else {
			$data['private_trainers_owner_data'] = array();
		}

		$data['category_filters'] = array();
		foreach ($filters as $filter_id) {
			$filter_info = $this->model_catalog_filter->getFilter($filter_id);

			if ($filter_info) {
				$data['category_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}
		$this->load->model('setting/store');
		$data['stores'] = $this->model_setting_store->getStores();
		if (isset($this->request->post['category_store'])) {
			$data['category_store'] = $this->request->post['category_store'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_store'] = $this->model_catalog_trainer->getCategoryStores($this->request->get['category_id']);
		} else {
			$data['category_store'] = array(0);
		}
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = true;
		}
		if (isset($this->request->post['category_layout'])) {
			$data['category_layout'] = $this->request->post['category_layout'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_layout'] = $this->model_catalog_trainer->getCategoryLayouts($this->request->get['category_id']);
		} else {
			$data['category_layout'] = array();
		}
		$data['state'] =  array(
			 'Andhra Pradesh' => 'Andhra Pradesh',
			 'Arunachal Pradesh' => 'Arunachal Pradesh',
			 'Assam' => 'Assam',
			 'Bihar' => 'Bihar',
			 'Chhattisgarh' => 'Chhattisgarh',
			 'Goa' => 'Goa',
			 'Gujarat' => 'Gujarat',
			 'Haryana' => 'Haryana',
			 'Himachal Pradesh' => 'Himachal Pradesh',
			 'Jammu & Kashmir' => 'Jammu & Kashmir',
			 'JH' => 'Jharkhand',
			 'Karnataka' => 'Karnataka',
			 'Kerala' => 'Kerala',
			 'Madhya Pradesh' => 'Madhya Pradesh',
			 'Maharashtra' => 'Maharashtra',
			 'Manipur' => 'Manipur',
			 'Meghalaya' => 'Meghalaya',
			 'Mizoram' => 'Mizoram',
			 'Nagaland' => 'Nagaland',
			 'Odisha' => 'Odisha',
			 'Punjab' => 'Punjab',
			 'RJ' => 'Rajasthan',
			 'Rajasthan' => 'Sikkim',
			 'Tamil Nadu' => 'Tamil Nadu',
			 'Tripura' => 'Tripura',
			 'Uttarakhand' => 'Uttarakhand',
			 'Uttar Pradesh' => 'Uttar Pradesh',
			 'West Bengal' => 'West Bengal',
		);
		$data['fees'] =  array(
			'Cash' => 'Cash',
			'Debit' => 'Debit',
			'Wita' => 'Wita',
			'Internet' => 'Internet',
		);
		$data['trainer_option'] =  array(
			'N' => 'No',
			'Y' => 'Yes',
			
		);
		$data['license_type1'] =  array(
			'A' => 'A',
			'B'   => 'B',
			'' =>'None',
		);
		$data['owner_autority'] =  array(
			'Y' => 'Yes',
			'N'   => 'No',
			
		);
		// $data['status1']=array(
		// 	'A' =>'Enable',
		// 	'I' =>'Disable',
		// );


		$data['status1'] = array(
			'CAL'  => 'CAL',
			'DEL'  => 'DEL',
			'BNG'  => 'BNG',
			'MYS'  => 'MYS',
			'HYD'  => 'HYD',
			'MAD'  => 'MAD',
		);

		$data['gst_type1']=array(
			'R' =>'Registered',
			'N' =>'Non-Registered',
		);
		$data['trainer_clubs'] = array(
			'RWITC'  => 'RWITC',
			'CAL'  => 'CAL',
			'DRC'  => 'DRC',
			'BTC'  => 'BTC',
			'MRC'  => 'MRC',
			'HRC'  => 'HRC',
			'MYRC'  => 'MYRC',
		);
		$data['authorty_bans'] = array(
			'SS'  => 'SS',
			'SM'  => 'SM',
			'SC'  => 'SC',
			'BA'  => 'BA',
			'MS'  => 'MS',
			'MC'  => 'MC',
			'MB'  => 'MB',
		);
		$this->load->model('design/layout');
		$data['layouts'] = $this->model_design_layout->getLayouts();
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		if(isset($authoritys)){
			$data['authorityarray'] = array();
		}else{
			$category_total1 = $this->model_catalog_trainer->getTotalAuthority();
			if(isset($this->request->get['id'])){
				$authoritys = $this->model_catalog_trainer->getAuthority($this->request->get['id']);
			}
			if(isset($authoritys)){
				foreach ($authoritys as $authority) {
					if($authority['start_date'] == '1970-01-01' || $authority['start_date'] == '0000-00-00'){
						$startdate = ''; 
					} else {
						$startdate = date('d-m-Y', strtotime($authority['start_date']));
					}

					if ($authority['end_date'] == '2050-12-31') {
						$enddate = '31-12-2050';
					} else {
						if($authority['end_date'] == '1970-01-01' || $authority['end_date'] == '0000-00-00'){
							$enddate = '';
						} else {
							$enddate = date('d-m-Y', strtotime($authority['end_date']));
						}
					}
					$data['authorityarray'][] = array(
						'autho_id'=> $authority['autho_id'],
						'check_name' => $authority['check_name'],
						'start_date' => $startdate,
						'end_date'	=> $enddate,
						'sub_auth_id'	=>$authority['sub_auth_id'],
						'owner_id'  => $authority['owner_id'],
						'edit'        => $this->url->link('catalog/trainer/edit1', 'token=' . $this->session->data['token'] .'&autho_id=' .$authority['autho_id'].'&owner_id='.$authority['owner_id']. $url, true)
					);
				}
			}
		}

		if(isset($Subauthoritys)){
			$data['Subauthorityarray'] = array();
		}else{
			$category_total1 = $this->model_catalog_trainer->getTotalSubAuthority();
			if(isset($this->request->get['id'])){
				$Subauthoritys = $this->model_catalog_trainer->getSubAuthority($this->request->get['id']);
			}
			if(isset($Subauthoritys)){
				foreach ($Subauthoritys as $Subauthority) {
					if($Subauthority['start_date'] == '1970-01-01' || $Subauthority['start_date'] == '0000-00-00'){
						$startdate = ''; 
					} else {
						$startdate = date('d-m-Y', strtotime($Subauthority['start_date']));
					}

					if ($Subauthority['end_date'] == '2050-12-31') {
						$enddate = '31-12-2050';
					} else {
						if($Subauthority['end_date'] == '1970-01-01' || $Subauthority['end_date'] == '0000-00-00'){
							$enddate = '';
						} else {
							$enddate = date('d-m-Y', strtotime($Subauthority['end_date']));
						}
					}
					$data['Subauthorityarray'][] = array(
						'sub_autho_id'=> $Subauthority['sub_autho_id'],
						'owner_name' => $Subauthority['owner_name'],
						'start_date' => $startdate,
						'end_date'	=> $enddate,
						'owner_id'  => $Subauthority['owner_id'],
						'edit'        => $this->url->link('catalog/trainer/edit2', 'token=' . $this->session->data['token'] .'&owner_id='.$Subauthority['owner_id']. $url, true)
					);
				}
			}
		}
		$data['horseDatas'] = array();
		if(isset($this->request->get['id'])){
			$traineritrs = $this->model_catalog_trainer->getTraineritr($this->request->get['id']);
			$baninfo_with_date_end =  $this->model_catalog_trainer->getBanhistory($this->request->get['id']);//FOR BAN HISTORY
			$staff_details = $this->model_catalog_trainer->getStaffs($this->request->get['id']);
			$baninfo_witout_date_end =  $this->model_catalog_trainer->getBandetails($this->request->get['id']);//FOR BAN DETAILS
    		$horses = $this->db->query("SELECT * FROM `horse1` h LEFT JOIN horse_to_trainer ht ON h.horseseq =ht.horse_id WHERE ht.trainer_id = '".$this->request->get['id']."' ");
    		foreach ($horses->rows as $horse) {
    			//echo "<pre>";print_r($horse);
    			$equipments = $this->db->query("SELECT * FROM `horse_equipments` WHERE horse_id = '".$horse['horseseq']."' ");
    			$final_equipment_name = '';
				if ($equipments->num_rows >0) {
					foreach ($equipments->rows as $akey => $avalue) {
						$final_equipment_name .= '</b>'.$avalue['equipment_name'].'<br>'.'<b>';
					}
					
				}

				$current_date = date("Y-m-d");
				$ban_sql = "SELECT * FROM `horse_ban` WHERE horse_id = '".$horse['horseseq']."' ORDER BY `horse_ban_id` DESC LIMIT 1 ";
				$ban = $this->db->query($ban_sql);
				//echo "<pre>";print_r($ban);
				$final_ban = '';
				if ($ban->num_rows >0) {
						if ($ban->row['enddate_ban'] >= $current_date) {
							$final_ban .= '</b>'.$ban->row['club_ban'].'-'.$ban->row['authority'].'<br>'.'<b>';
						}else{
							$final_ban = '';
						}
					
				}

				$shoe_name_sql = "SELECT * FROM `horse_shoeing` WHERE horse_id = '".$horse['horseseq']."' ORDER BY `horse_shoeing_id` DESC LIMIT 1 ";
				$shoe_name = $this->db->query($shoe_name_sql);
				//echo "<pre>";print_r($shoe_name);
				$bit_name_sql = "SELECT * FROM `horse_bits` WHERE horse_id = '".$horse['horseseq']."' ORDER BY `id` DESC LIMIT 1 ";

				$bit_name = $this->db->query($bit_name_sql);
				//echo "<pre>";print_r($bit_name);

				$final_shoe_name = '';
				if ($shoe_name->num_rows >0) {
					//foreach ($shoe_name->rows as $akey => $avalue) {
						if ($shoe_name->row['type'] == 'Aluminium') {
							$type = 'A';
						} elseif ($shoe_name->row['type'] == 'Steel') {
							$type = 'S';
						} else{
							$type = '';
						}
						$final_shoe_name .= '</b>'.$type.' '.$shoe_name->row['shoe_description'].' '.$bit_name->row['short_name'].'<br>'.'<b>';
					//}
					
				}
				$data['horseDatas'][] = array(
					'official_name'=> $horse['official_name'],
					'age' => $horse['age'],
					'date_of_charge' => date('d-m-Y', strtotime($horse['date_of_charge'])),
					'extra_narration'	=> $horse['extra_narration'],
					'sire_name'  => $horse['sire_name'],
					'dam_name'  => $horse['dam_name'],
					'horse_equipment_name' => $final_equipment_name,
					'ban' => $final_ban,
					'shoe_name' => $final_shoe_name
					
				);
    		}//exit;
		}
		/*echo '<pre>';
		print_r($traineritrs);
		exit;*/
		if (!empty($horses)) {
			$data['horses'] = $horses;
		} else {
      		$data['horses'] = array();
    	}
		if (isset($this->request->post['banhistorydatas'])) {
      		$data['bandatas'] = $this->request->post['banhistorydatas'];
    	} elseif (!empty($baninfo_with_date_end)) {
			$data['bandatas'] = $baninfo_with_date_end;
		} else {
      		$data['bandatas'] = array();
    	}
    	if (!empty($staff_details)) {
			$data['staffs'] = $staff_details;
		} else {
      		$data['staffs'] = array();
    	}
    	if (isset($this->request->post['bandats'])) {
      		$data['bandeatils'] = $this->request->post['bandats'];
    	} elseif (!empty($baninfo_witout_date_end)) {
			$data['bandeatils'] = $baninfo_witout_date_end;
		} else {
      		$data['bandeatils'] = array();
    	}
    	if (isset($this->request->post['id_hidden_band'])) {
      		$data['id_hidden_band'] = $this->request->post['id_hidden_band'];
    	} elseif (!empty($baninfo_witout_date_end)) {
			$ban_counts = count($baninfo_witout_date_end);
			$data['id_hidden_band'] = $ban_counts;
		} else {
      		$data['id_hidden_band'] = 1;
    	}
		if (!empty($traineritrs)) {
			$data['uploaditr'] = $traineritrs;
		} else {
      		$data['uploaditr'] = array();
    	}
    	if (isset($this->request->post['upload_hidden_id'])) {
      		$data['upload_hidden_id'] = $this->request->post['upload_hidden_id'];
    	} elseif (!empty($traineritrs)) {
			$traineritrs = count($traineritrs);
			$data['upload_hidden_id'] = $traineritrs;
		} else {
      		$data['upload_hidden_id'] = 1;
    	}
    	
		$pagination = new Pagination();
		$pagination->total = $category_total1;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/trainer/add', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination1'] = $pagination->render();

		$data['authoritys'] = sprintf($this->language->get('text_pagination'), ($category_total1) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total1 - $this->config->get('config_limit_admin'))) ? $category_total1 : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total1, ceil($category_total1 / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;
		$this->response->setOutput($this->load->view('catalog/trainer_form_view', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/trainer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// echo '<pre>';
		// print_r($this->request->post);
		// exit();
		if($this->request->post['trainer_name'] == ''){
			$this->error['valierr_trainer_name'] = 'Please Enter Trainer Name';
		}

		if($this->request->post['isActive'] == 0 && $this->request->post['remarks'] == ''){ 
			$this->error['remarks_errors'] = 'Please Enter Remark';
		}

		if(isset($this->request->post['gst_no']) != '' && $this->request->post['gst_type'] != 'N'){ 
			if(strlen($this->request->post['gst_no']) > 15 || strlen($this->request->post['gst_no']) < 15) { 
				$this->error['gst_errors'] = 'Please Enter Valid GST NO';
			}
		}

		if(isset($this->request->post['pan_no']) != '' && $this->request->post['gst_type'] != 'N'){ 
			if(strlen($this->request->post['pan_no']) > 10 || strlen($this->request->post['pan_no']) < 10) { 
				$this->error['pan_errors'] = 'Please Enter Valid PAN NO';
			}
		}
		if ($this->request->post['isActive'] == 0 && $this->request->post['hidden_active'] == 1) {
			//echo "innnnn";
			$sql =("SELECT * FROM `horse_to_trainer` WHERE `trainer_id` ='".$this->request->get['id']."' ");
			$query = $this->db->query($sql);
			//echo "<pre>";print_r($query);exit;
			if($query->num_rows > 0){
				//echo $query->num_rows;exit;
				$this->error['error_status'] = "You can not Exit status because it is assinged ".$query->num_rows." Horses!";
				//$this->session->data['status_error'] = $this->language->get('error_status');
			}
		}


		if (($this->request->post['trainer_code_new'] != '')  && (!isset($this->request->get['id'])) ) {
			$avail_code = $this->db->query("SELECT * FROM trainers WHERE `trainer_code_new` = '".$this->request->post['trainer_code_new']."' ");
			if ($avail_code->num_rows > 0) {
				$this->error['valierr_trainer_code'] = 'This Code Is Already In Used! Please Select Another!';
			}
		}
		
		if($this->request->post['trainer_code_new'] == ''){//echo'inn';exit;
			$this->error['valierr_trainer_code'] = 'Please Select Trainer Code!';
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/trainer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/trainer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
	public function autocompleteOwnername() {
		$json = array();
		if (isset($this->request->get['owner_name'])) {
			$this->load->model('catalog/trainer');
			$results = $this->model_catalog_trainer->getownername($this->request->get['owner_name']);
			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'owner_name'        => strip_tags(html_entity_decode($result['owner_name'], ENT_QUOTES, 'UTF-8')),
						'owner_id'  =>$result['id']
					);
				}
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['owner_name'];
			$sort_order[$key] = $value['owner_id'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteTrainername() {
		$json = array();
		if (isset($this->request->get['trainer_name'])) {
			$this->load->model('catalog/trainer');
			$results = $this->model_catalog_trainer->gettrainername($this->request->get['trainer_name']);
			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'trainer_name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
						'trainer_id'  =>$result['id']
					);
				}
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
			$sort_order[$key] = $value['trainer_id'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/trainer');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_trainer->getCategories($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function checkTranCode() {
		$json = array();
		$json['status'] = 0;
		if (isset($this->request->get['trainer_id']) && $this->request->get['trainer_id'] != '') {
			$old_trainers = $this->db->query("SELECT trainer_code_new FROM trainers WHERE trainer_code_new = '". $this->request->get['trainer_id'] ."'");
			$json['status'] = ($old_trainers->num_rows > 0) ? 1 : 0;

		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function autocompleteTrainer() {
		/*echo 'in';exit;*/
		$json = array();

		if (isset($this->request->get['trainer_name'])) {
			$this->load->model('catalog/trainer');

			$results = $this->model_catalog_trainer->getTrainersAuto($this->request->get['trainer_name']);


			if($results){
				foreach ($results as $result) {
					$json[] = array(
						'trainer_code' => $result['trainer_code'],
						'trainer_id' => $result['id'],
						'trainer_name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
		}

		// echo '<pre>';print_r($json);
		// 	exit;
		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['trainer_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		//echo '<pre>';print_r($json);
		$this->response->setOutput(json_encode($json));
	}

	public function transaction() {
		/*echo '<pre>';
		print_r($this->request->get);
		exit;*/
		$id = $this->request->get['trainer_idss'];
		$date = $this->request->get['dates'];
		$amount = $this->request->get['amts'];
		$fees = $this->request->get['feess'];

		$this->db->query("INSERT INTO `trainer_renewal_history` SET trainer_id = '".$id."', renewal_date = '".date('Y-m-d', strtotime($date))."', fees = '".$fees."', amount = '".$amount."' ");
		$json['success'] = '1';
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		//echo '<pre>';print_r($json);exit;
	}

	public function delete_transaction() {
		//echo '<pre>';print_r($this->request->get);exit;
		$id = $this->request->get['id'];
		$this->db->query("DELETE FROM `trainer_renewal_history` WHERE id = '".$id."' ");
		$json['success'] = '1';
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		//echo '<pre>';print_r($json);exit;
	}

	public function upload_profile() {
		//$this->load->language('catalog/employee');
		$json = array();
		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/trainer/profile_pic')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$file_show_path = HTTP_CATALOG.'system/storage/download/trainer/profile_pic/';
		$file_upload_path = DIR_DOWNLOAD.'trainer/profile_pic/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				$this->log->write(print_r($this->request->files, true));
				$this->log->write($image_name);
				$this->log->write($img_extension);
				$this->log->write($filename);

				
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';
				$this->log->write(print_r($allowed, true));
				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = 'Please upload valid file!';
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] ='Please upload valid file!'. $this->request->files['file']['error'];
				}
			} else {
				$json['error'] ='Please upload valid file!';
			}
		}

		if (!$json) {
			$file = $filename;
			if(move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file)){
				$destFile = $file_upload_path . $file;
				//chmod($destFile, 0777);
				$json['filename'] = $file;
				$json['link_href'] = $file_show_path . $file;

				$json['success'] = 'Your file was successfully uploaded!';
			} else {
				$json['error'] = 'Please upload valid file!';
			}
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}
