<?php
class ControllerCommonDashboard extends Controller {
	public function index() {
		$this->load->language('common/dashboard');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_map'] = $this->language->get('text_map');
		$data['text_activity'] = $this->language->get('text_activity');
		$data['text_recent'] = $this->language->get('text_recent');
		$data['user_group_id'] = $this->user->getGroupId();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		// Check install directory exists
		if (is_dir(dirname(DIR_APPLICATION) . '/install')) {
			$data['error_install'] = $this->language->get('error_install');
		} else {
			$data['error_install'] = '';
		}

		$data['eventDatas'] = array();

		$sql = "SELECT * FROM `events` WHERE 1=1 ";

		$current_date = Date('Y-m-d');

		if (!empty($current_date)) {
			$sql .= " AND date_of_event LIKE '%" . $current_date . "%'";
		}

		$results = $this->db->query($sql)->rows;
		
		foreach ($results as $result) {
			$data['eventDatas'][]= array(
				'eventID' 	  			=> $result['event_id'],
				'eventName'   			=> $result['event_name'],
				'eventDescription'   			=> $result['description'],
				'eventDate'   			=> $result['date_of_event'],
			);
		}

		$data['token'] = $this->session->data['token'];
		$this->load->model('catalog/horse');
		$this->load->model('catalog/owner');
		$this->load->model('catalog/trainer');
		$this->load->model('catalog/jockey');

		$this->load->model('catalog/vendor');
		$this->load->model('catalog/medicine');
		$this->load->model('catalog/doctor');
		$this->load->model('catalog/service');

		$order_total = $this->model_catalog_horse->getTotalCategories();
		$order_total_owner = $this->model_catalog_owner->getTotalCategories();
		$order_total_trainer = $this->model_catalog_trainer->getTotalCategories();
		$order_total_jockey = $this->model_catalog_jockey->getTotalCategoriess();
		 // echo "<PRE>";print_r($order_total_jockey);echo "</PRE>";die('end line');
		$total_supplier = $this->model_catalog_vendor->getTotalVendors();
		$total_medicine = $this->model_catalog_medicine->getTotalMedicines();
		$total_doctor = $this->model_catalog_doctor->getTotalDoctors();
		$total_service_charge = $this->model_catalog_service->getTotalCategories();

		$data['total'] = $order_total;
		$data['total_owner'] = $order_total_owner;
		$data['total_trainer'] = $order_total_trainer;
		$data['total_jockey'] = $order_total_jockey;

		$data['total_supplier'] = $total_supplier;
		$data['total_medicine'] = $total_medicine;
		$data['total_doctor'] = $total_doctor;
		$data['total_service_charge'] = $total_service_charge;


		$data['horse_at_glance'] = $this->url->link('catalog/horse_at_glance', 'token=' . $this->session->data['token'], true);
		$data['horse_master'] = $this->url->link('catalog/horse', 'token=' . $this->session->data['token'], true);
		$data['supplier_master'] = $this->url->link('catalog/vendor', 'token=' . $this->session->data['token'], true);
		$data['medicine_master'] = $this->url->link('catalog/medicine', 'token=' . $this->session->data['token'], true);
		$data['doctor_master'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'], true);
		$data['service_master'] = $this->url->link('catalog/service', 'token=' . $this->session->data['token'], true);

		$data['owner_at_glance'] = $this->url->link('catalog/owner_at_glance', 'token=' . $this->session->data['token'], true);
		$data['owner_master'] = $this->url->link('catalog/owner', 'token=' . $this->session->data['token'], true);
		$data['trainer_master'] = $this->url->link('catalog/trainer', 'token=' . $this->session->data['token'], true);
		$data['jockey_master'] = $this->url->link('catalog/jockey', 'token=' . $this->session->data['token'], true);
		$data['trainer_at_glance'] = $this->url->link('catalog/trainer_at_glance', 'token=' . $this->session->data['token'], true);
		$data['jockey_at_glance'] = $this->url->link('catalog/jockey_at_glance', 'token=' . $this->session->data['token'], true);
		$data['prospectus_master'] = $this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'], true);
		$data['entries_master'] = $this->url->link('transaction/entries', 'token=' . $this->session->data['token'], true);
		$data['prospectus_master'] = $this->url->link('transaction/prospectus', 'token=' . $this->session->data['token'], true);
		$data['handicapping_master'] = $this->url->link('transaction/handicapping', 'token=' . $this->session->data['token'], true);
		$data['reg_master'] = $this->url->link('transaction/registeration_module', 'token=' . $this->session->data['token'], true);
		$data['indent_master'] = $this->url->link('catalog/rawmaterialreq', 'token=' . $this->session->data['token'], true);
		$data['inward_master'] = $this->url->link('catalog/inward', 'token=' . $this->session->data['token'], true);
		$data['horse_treatment_report'] = $this->url->link('report/horse_treatment_report', 'token=' . $this->session->data['token'], true);
		$data['horsewise_treat_entry'] = $this->url->link('report/horsewise_treat_entry', 'token=' . $this->session->data['token'], true);
		
		$data['importtally_master'] = $this->url->link('catalog/import_list', 'token=' . $this->session->data['token'], true);
		$data['medicinetransfer_master'] = $this->url->link('catalog/medicine_transfer', 'token=' . $this->session->data['token'], true);
		$data['medicine_return'] = $this->url->link('catalog/medicine_return', 'token=' . $this->session->data['token'], true);
		$data['medicine_transaction'] = $this->url->link('report/medicine_transaction', 'token=' . $this->session->data['token'], true);
		$data['store_reconsilation'] = $this->url->link('report/store_reconsilation', 'token=' . $this->session->data['token'], true);
		$data['treatment_entry'] = $this->url->link('catalog/treatment_entry_single/add', 'token=' . $this->session->data['token'], true);
		
		$data['arrival_charges_master'] = $this->url->link('transaction/arrival_charges', 'token=' . $this->session->data['token'], true);
		$data['jockey_lisence_trans'] = $this->url->link('transaction/jockey_lisence_trans', 'token=' . $this->session->data['token'], true);
		$data['trainer_license_trans'] = $this->url->link('transaction/trainer_license_trans', 'token=' . $this->session->data['token'], true);
		$data['assign_trainer'] = $this->url->link('catalog/assign_trainer', 'token=' . $this->session->data['token'], true);
		$data['acceptance_master'] = $this->url->link('transaction/acceptance', 'token=' . $this->session->data['token'], true);
		$data['cancel_ownership_master'] = $this->url->link('transaction/cancel_ownership', 'token=' . $this->session->data['token'], true);
		$data['ownership_master'] = $this->url->link('transaction/ownership_shift_module', 'token=' . $this->session->data['token'], true);
		$data['undertaking_charge_master'] = $this->url->link('report/report_horse_undertaking_charge', 'token=' . $this->session->data['token'], true);
		$data['medicine_report'] = $this->url->link('report/medicine_report', 'token=' . $this->session->data['token'], true);
		
		$data['inward_report'] = $this->url->link('report/inward_po_report', 'token=' . $this->session->data['token'], true);
		$data['indent_report'] = $this->url->link('report/indent_po_report', 'token=' . $this->session->data['token'], true);
		$data['med_transfer_report'] = $this->url->link('report/med_transfer_report', 'token=' . $this->session->data['token'], true);
		$data['stock_report'] = $this->url->link('report/stock_report', 'token=' . $this->session->data['token'], true);
		$data['arrival_report'] = $this->url->link('report/arrival_charge_report', 'token=' . $this->session->data['token'], true);
		$data['ownership_report'] = $this->url->link('report/ownership_registration_report', 'token=' . $this->session->data['token'], true);
		$data['jockey_license_transaction'] = $this->url->link('report/jockey_license_report', 'token=' . $this->session->data['token'], true);
		$data['trainer_license_transaction'] = $this->url->link('report/trainer_license_report', 'token=' . $this->session->data['token'], true);
		$data['report_reconsilation'] = $this->url->link('report/reconsilation_report', 'token=' . $this->session->data['token'], true);



		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['order'] = $this->load->controller('dashboard/order');
		$data['sale'] = $this->load->controller('dashboard/sale');
		$data['customer'] = $this->load->controller('dashboard/customer');
		$data['online'] = $this->load->controller('dashboard/online');
		$data['map'] = $this->load->controller('dashboard/map');
		$data['chart'] = $this->load->controller('dashboard/chart');
		$data['horse_glance'] = $this->load->controller('dashboard/horse_glance');
		$data['owner_glance'] = $this->load->controller('dashboard/owner_glance');
		$data['trainer_glance'] = $this->load->controller('dashboard/trainer_glance');
		$data['jockey_glance'] = $this->load->controller('dashboard/jockey_glance');
		$data['activity'] = $this->load->controller('dashboard/activity');
		$data['recent'] = $this->load->controller('dashboard/recent');
		$data['footer'] = $this->load->controller('common/footer');
		$data['report_horse_undertaking_charge'] = $this->load->controller('dashboard/report_horse_undertaking_charge');

		// Run currency update
		if ($this->config->get('config_currency_auto')) {
			$this->load->model('localisation/currency');

			$this->model_localisation_currency->refresh();
		}

		$this->response->setOutput($this->load->view('common/dashboard', $data));
	}
}