<?php
class ControllerUserUserPermission extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		$this->getList();
	}

	public function add() {
		$this->load->language('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_user_user_group->addUserGroup($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_user_user_group->editUserGroup($this->request->get['user_group_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $user_group_id) {
				$this->model_user_user_group->deleteUserGroup($user_group_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('user/user_permission/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('user/user_permission/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['user_groups'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$user_group_total = $this->model_user_user_group->getTotalUserGroups();

		$results = $this->model_user_user_group->getUserGroups($filter_data);

		foreach ($results as $result) {
			$data['user_groups'][] = array(
				'user_group_id' => $result['user_group_id'],
				'name'          => $result['name'],
				'edit'          => $this->url->link('user/user_permission/edit', 'token=' . $this->session->data['token'] . '&user_group_id=' . $result['user_group_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $user_group_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($user_group_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($user_group_total - $this->config->get('config_limit_admin'))) ? $user_group_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $user_group_total, ceil($user_group_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('user/user_group_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_form'] = !isset($this->request->get['user_group_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_access'] = $this->language->get('entry_access');
		$data['entry_modify'] = $this->language->get('entry_modify');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['user_group_id'])) {
			$data['action'] = $this->url->link('user/user_permission/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('user/user_permission/edit', 'token=' . $this->session->data['token'] . '&user_group_id=' . $this->request->get['user_group_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['user_group_id']) && $this->request->server['REQUEST_METHOD'] != 'POST') {
			$user_group_info = $this->model_user_user_group->getUserGroup($this->request->get['user_group_id']);
		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($user_group_info)) {
			$data['name'] = $user_group_info['name'];
		} else {
			$data['name'] = '';
		}

		/*$ignore = array(
			'common/dashboard',
			'common/startup',
			'common/login',
			'common/logout',
			'common/forgotten',
			'common/reset',			
			'common/footer',
			'common/header',
			'error/not_found',
			'error/permission',
			'dashboard/order',
			'dashboard/sale',
			'dashboard/customer',
			'dashboard/online',
			'dashboard/map',
			'dashboard/activity',
			'dashboard/chart',
			'dashboard/recent'
		);*/

		$data['permissions'] = array();
		/*$files = array();

		// Make path into an array
		$path = array(DIR_APPLICATION . 'controller/*');

		// While the path array is still populated keep looping through
		while (count($path) != 0) {
			$next = array_shift($path);

			foreach (glob($next) as $file) {
				// If directory add to path array
				if (is_dir($file)) {
					$path[] = $file . '/*';
				}

				// Add the file to the files to be deleted array
				if (is_file($file)) {
					$files[] = $file;
				}
			}
		}

		// Sort the file array
		sort($files);
					
		foreach ($files as $file) {
			$controller = substr($file, strlen(DIR_APPLICATION . 'controller/'));

			$permission = substr($controller, 0, strrpos($controller, '.'));

			if (!in_array($permission, $ignore)) {
				$data['permissions'][] = $permission;
			}
				$raw_name = strtok($permission, '/');
				$names = str_replace($raw_name,"",$permission);
				$name = str_replace("/","",$names);
				$final_name = str_replace("_"," ",$name);
				$data['finalname'] = ucfirst($final_name);
		}*/

		// THIS IS CATALOG PERMISSIONS
		$data['permissions']['catalog/racetype'] = 'Race Type';
		$data['permissions']['catalog/owner'] = 'Owner';
		$data['permissions']['catalog/trainer'] = 'Trainer';
		$data['permissions']['catalog/horse'] = 'Horse';
		$data['permissions']['catalog/jockey'] = 'Jockey';
		$data['permissions']['catalog/equipments'] = 'Equipments';
		$data['permissions']['catalog/color'] = 'Color';
		$data['permissions']['catalog/vendor'] = 'Supplier';
		$data['permissions']['catalog/medicine'] = 'Medicine';
		$data['permissions']['catalog/doctor'] = 'Doctor';
		$data['permissions']['catalog/trainer_staff'] = 'Trainer Staff';
		$data['permissions']['catalog/assign_trainer'] = 'Assign Trainer';
		$data['permissions']['catalog/service'] = 'Service Charge';
		$data['permissions']['catalog/rawmaterialreq'] = 'Indent';
		$data['permissions']['catalog/inward'] = 'Inward';
		$data['permissions']['catalog/shoe'] = 'Shoe';
		$data['permissions']['catalog/bit'] = 'Bit';
		$data['permissions']['catalog/isb_horse'] = 'ISB Horse';
		$data['permissions']['catalog/horse_at_glance'] = 'Horse At Glance';
		$data['permissions']['catalog/owner_at_glance'] = 'Owner At Glance';
		$data['permissions']['catalog/trainer_at_glance'] = 'Trainer At Glance';
		$data['permissions']['catalog/jockey_at_glance'] = 'Jockey At Glance';
		$data['permissions']['catalog/import_list'] = 'PO List';
		$data['permissions']['catalog/medicine_transfer'] = 'Medicine Transfer';
		$data['permissions']['catalog/medicine_return'] = 'Medicine Return';
		$data['permissions']['catalog/treatment_entry'] = 'Multiple Horse Treatment Entry';
		$data['permissions']['catalog/treatment_entry_single'] = 'Treatment Entry';

		// THIS IS TRANSACTION PERMISSIONS
		$data['permissions']['transaction/stacks'] = 'Stacks';
		$data['permissions']['transaction/class_entry'] = 'Class Entry';
		$data['permissions']['transaction/prospectus'] = 'Prospectus';
		$data['permissions']['transaction/entries'] = 'Entries';
		$data['permissions']['transaction/handicapping'] = 'Handicapping';
		$data['permissions']['transaction/acceptance'] = 'Acceptance';
		$data['permissions']['transaction/amount_tran'] = 'Transaction Amount';
		$data['permissions']['transaction/horse_datas_link'] = 'Owner Order And Colour Change';
		$data['permissions']['transaction/ownership_shift_module'] = 'Ownership Shift Module';
		$data['permissions']['transaction/cancel_ownership'] = 'Cancel Lease Owner';
		$data['permissions']['transaction/registeration_module'] = 'Registration Module';
		$data['permissions']['transaction/arrival_charges'] = 'Arrival Charges';
		$data['permissions']['transaction/multi_arrival_charges'] = 'Multi Arrival Charges';
		$data['permissions']['transaction/send_mail_charges'] = 'Send Mail Charges';

		$data['permissions']['transaction/multi_horse_transfer'] = 'Multi Horse Transfer';
		$data['permissions']['transaction/jockey_ban'] = 'Jockey Ban';
		$data['permissions']['transaction/trainer_ban'] = 'Trainer Ban';


		// THIS IS REPORT PERMISSIONS
		$data['permissions']['report/report_horse_undertaking_charge'] = 'Report Horse Undertaking Charge';
		$data['permissions']['report/inward_po_report'] = 'Inward Report';
		$data['permissions']['report/cancel_own'] = 'Cancel Ownership Report';
		$data['permissions']['report/arrival_charge_report'] = 'Arrival Charge Report';
		$data['permissions']['report/ownership_registration_report'] = 'Ownership Registartion Report';
		$data['permissions']['report/indent_po_report'] = 'Indent Report';
		$data['permissions']['report/name_change_report'] = 'Name Change Report';
		$data['permissions']['report/stock_report'] = 'Stock Report';
		$data['permissions']['report/med_transfer_report'] = 'Medicine Transfer Report';
		$data['permissions']['report/medicine_report'] = 'Medicine Report';
		$data['permissions']['report/medicine_transaction'] = 'Medicine Transaction';
		$data['permissions']['report/store_reconsilation'] = 'Store Reconsilation';
		$data['permissions']['report/horse_treatment_report'] = 'Datewise Treatment Entry';
		$data['permissions']['report/horsewise_treat_entry'] = 'Horsewise Treatment Entry';
		$data['permissions']['report/monthly_medicine_transaction'] = 'Monthly Medicine Transaction';
		$data['permissions']['report/trainer_license_report'] = 'Trainer License Report';
		$data['permissions']['report/jockey_license_report'] = 'Jockey License Report';
		$data['permissions']['transaction/jockey_lisence_trans'] = 'Jockey License Transaction';
		$data['permissions']['transaction/trainer_license_trans'] = 'Trainer License Transaction';
		$data['permissions']['report/reconsilation_report'] = 'Reconsilation Report';
		
		//This IS Utility permission
		$data['permissions']['utility/import_horse'] = 'Import Horse';
		$data['permissions']['utility/import_tally_po'] = 'Import Tally Po';
		$data['permissions']['utility/is_end'] = 'Is End';
		$data['permissions']['utility/is_end_j'] = 'Jockey Is End';
		$data['permissions']['localisation/country'] = 'State/Country';
		$data['permissions']['snippet/import_indent'] = 'Import Indent';

		if (isset($this->request->post['permission']['access'])) {
			$data['access'] = $this->request->post['permission']['access'];
		} elseif (isset($user_group_info['permission']['access'])) {
			$data['access'] = $user_group_info['permission']['access'];
		} else {
			$data['access'] = array();
		}

		if (isset($this->request->post['permission']['modify'])) {
			$data['modify'] = $this->request->post['permission']['modify'];
		} elseif (isset($user_group_info['permission']['modify'])) {
			$data['modify'] = $user_group_info['permission']['modify'];
		} else {
			$data['modify'] = array();
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('user/user_group_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'user/user_permission')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'user/user_permission')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('user/user');

		foreach ($this->request->post['selected'] as $user_group_id) {
			$user_total = $this->model_user_user->getTotalUsersByGroupId($user_group_id);

			if ($user_total) {
				$this->error['warning'] = sprintf($this->language->get('error_user'), $user_total);
			}
		}

		return !$this->error;
	}
}