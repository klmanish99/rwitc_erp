
UPDATE `horse1` SET sex = 'f' where sex = 'g'
UPDATE `horse1` SET sex = 'm' where sex = 'h'
ALTER TABLE `horse1` ADD `changehorse_name` VARCHAR(255) NOT NULL DEFAULT '' AFTER `official_name`;

ALTER TABLE `horse1` ADD `official_name_change_status` INT(11) NOT NULL DEFAULT '0' AFTER `changehorse_name`;

ALTER TABLE `owners` ADD `uploaded_file_source` VARCHAR(255) NOT NULL AFTER `file_number_upload`;
ALTER TABLE `owners` CHANGE `uploaded_file_source` `uploaded_file_source` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '';

'Date 5-7-2020'

CREATE TABLE `colors` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `color` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
)

CREATE TABLE `relationship` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `relationship` varchar(255) NOT NULL
)

CREATE TABLE `trainer_renewal_history` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `trainer_id` int(11) NOT NULL,
  `renewal_date` date NOT NULL DEFAULT '0000-00-00'
)

ALTER TABLE `owners` ADD `approved_status` VARCHAR(255) NOT NULL AFTER `is_trainer`;

ALTER TABLE `owners_partner` ADD `member` VARCHAR(255) NOT NULL AFTER `partner_name`;

ALTER TABLE `owner_to_trainer_authority` ADD `remarks` VARCHAR(255) NOT NULL AFTER `sub_trainer_id`;

ALTER TABLE `trainers` ADD `license_amt` VARCHAR(255) NOT NULL AFTER `date_of_license_renewal`;

ALTER TABLE `trainer_renewal_history` ADD `fees` VARCHAR(255) NOT NULL AFTER `renewal_date`, ADD `amount` VARCHAR(255) NOT NULL AFTER `fees`;

ALTER TABLE `horse_to_owner` ADD `owner_share_status` INT(11) NOT NULL DEFAULT '0' AFTER `to_owner_id`, ADD `parent_owner_id` INT(11) NOT NULL AFTER `owner_share_status`;

ALTER TABLE `trainers` ADD `active` INT(11) NOT NULL AFTER `is_wita`;

CREATE TABLE `service` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `med_type` varchar(255) NOT NULL,
  `descp` varchar(255) NOT NULL,
  `limit1` int(11) NOT NULL,
  `limit2` int(11) NOT NULL,
  `sc` int(11) NOT NULL
)

ALTER TABLE `is_vendor` ADD `gst_no` INT(11) NOT NULL AFTER `mask5`;

CREATE TABLE `doctor` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `doctor_name` varchar(255) NOT NULL,
  `doctor_code` varchar(255) NOT NULL,
  `ambulance_no` varchar(255) NOT NULL
)

ALTER TABLE `is_vendor` ADD `isActive` VARCHAR(20) NOT NULL AFTER `gst_no`;

CREATE TABLE `medicine` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `med_code` varchar(255) NOT NULL,
  `med_name` varchar(255) NOT NULL,
  `med_type` varchar(255) NOT NULL,
  `unit_cost` decimal(10,4) NOT NULL,
  `cost_a1` decimal(10,4) NOT NULL,
  `cost_a2` decimal(10,4) NOT NULL,
  `cost_a3` decimal(10,4) NOT NULL,
  `cost_a4` decimal(10,4) NOT NULL,
  `cost_a5` decimal(10,4) NOT NULL,
  `cost_a6` decimal(10,4) NOT NULL,
  `cost_a7` decimal(10,4) NOT NULL,
  `cost_a8` decimal(10,4) NOT NULL,
  `cost_a9` decimal(10,4) NOT NULL,
  `cost_a10` decimal(10,4) NOT NULL,
  `normal` decimal(10,4) NOT NULL,
  `store_unit` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `reorder_lvl` int(11) NOT NULL,
  `reorder_qty` int(11) NOT NULL
)

ALTER TABLE `medicine` ADD `isActive` VARCHAR(11) NOT NULL AFTER `brand`;

ALTER TABLE `service` ADD `isActive` VARCHAR(11) NOT NULL AFTER `sc`;

ALTER TABLE `doctor` ADD `isActive` VARCHAR(11) NOT NULL AFTER `ambulance_no`;

ALTER TABLE `jockey` ADD `jocky_status` INT(11) NOT NULL AFTER `jai_member`;

ALTER TABLE `horse1` ADD `horse_status` INT(11) NOT NULL AFTER `horse_remarks`;

ALTER TABLE `horse1` ADD `horse_code` VARCHAR(255) NOT NULL AFTER `changehorse_name`;

ALTER TABLE `owners` ADD `owner_code` VARCHAR(255) NOT NULL AFTER `owner_name`;

ALTER TABLE `horse_to_trainer` ADD `undertaking_charge` INT(11) NOT NULL AFTER `left_date_of_charge`;

CREATE TABLE `oc_rawmaterialreq` (
  `order_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `order_no` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `narration` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `view_status` int(11) NOT NULL,
  `requested_by` varchar(255) NOT NULL
)

CREATE TABLE `oc_rawmaterialreqitem` (
  `productraw_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_no` varchar(255) NOT NULL,
  `productraw_name` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL
)

ALTER TABLE `horse_to_owner` ADD `horse_to_owner_order` INT(11) NOT NULL DEFAULT '0' AFTER `owner_color`;


ALTER TABLE `oc_inwarditem` ADD `reduce_po_qty` INT(11) NOT NULL AFTER `po_qty`;

ALTER TABLE `oc_tally_po` ADD `status` INT(11) NOT NULL DEFAULT '0' AFTER `user_id`;



ALTER TABLE `horse_to_owner` ADD `trainer_id` INT(11) NOT NULL AFTER `horse_id`;
ALTER TABLE `horse_to_owner` ADD `contengency` INT(11) NOT NULL AFTER `horse_to_owner_order`, ADD `cont_amount` INT(11) NOT NULL AFTER `contengency`, ADD `win_gross_stake` INT(11) NOT NULL AFTER `cont_amount`, ADD `win_net_stake` INT(11) NOT NULL AFTER `win_gross_stake`, ADD `millionover` INT(11) NOT NULL AFTER `win_net_stake`, ADD `millionover_amt` INT(11) NOT NULL AFTER `millionover`, ADD `grade1` INT(11) NOT NULL AFTER `millionover_amt`;
ALTER TABLE `horse_to_owner` ADD `grade2` INT(11) NOT NULL AFTER `grade1`, ADD `grade3` INT(11) NOT NULL AFTER `grade2`;
ALTER TABLE `horse_to_owner` ADD `cont_percentage` INT(11) NOT NULL AFTER `contengency`;
ALTER TABLE `horse_to_owner` ADD `cont_place` INT(11) NOT NULL AFTER `win_net_stake`;

ALTER TABLE `horse_to_owner` ADD `horse_group_id` INT(11) NOT NULL AFTER `horse_id`;




CREATE TABLE `doctor_cost` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `medicine_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `cost` int(11) NOT NULL
)



ALTER TABLE `horse_to_owner` ADD `parent_group_id` INT(11) NOT NULL AFTER `horse_group_id`;




ALTER TABLE `foal_isb` ADD `status` INT(11) NOT NULL DEFAULT '0' AFTER `id`;



ALTER TABLE `owners_shared_color` ADD `color_trans_id` INT NOT NULL AFTER `owner_id`;


ALTER TABLE `owners_shared_color` ADD `color_trans_id` INT NOT NULL AFTER `owner_id`;

ALTER TABLE `equipment` CHANGE `hourse_id` `short_name` VARCHAR(255) NOT NULL;



CREATE TABLE `trainer_private_owner_names` 
  ( `id` INT NOT NULL AUTO_INCREMENT , `trainer_id` INT NOT NULL , `owner_id` INT NOT NULL , `owner_name` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

  ALTER TABLE `horse1` ADD `horse_isb_id` INT(11) NOT NULL DEFAULT '0' AFTER `horse_status`;

  CREATE TABLE `horse_to_owner_lease` ( `parent_trans_id` INT NOT NULL , `child_trans_id` INT NOT NULL ) ENGINE = InnoDB;
ALTER TABLE `horse_to_owner` ADD `provisional_ownership` VARCHAR(10) NOT NULL AFTER `grade3`;

ALTER TABLE `oc_registration_module` ADD `print_letters` VARCHAR(20) NOT NULL AFTER `remark`, ADD `registration_date` DATE NOT NULL AFTER `print_letters`, ADD `letter_dated` DATE NOT NULL AFTER `registration_date`, ADD `letter_received` DATE NOT NULL AFTER `letter_dated`;


ALTER TABLE `owners` ADD `wirhoa` INT(11) NOT NULL DEFAULT '0' AFTER `race_card_name`;

ALTER TABLE `owners` ADD `file_number_uploads` VARCHAR(255) NOT NULL AFTER `active`, ADD `uploaded_file_sources` VARCHAR(255) NOT NULL AFTER `file_number_uploads`;

ALTER TABLE `oc_registration_module` ADD `modification` VARCHAR(11) NOT NULL AFTER `letter_received`;

CREATE TABLE `jockey_returned_by` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `jockey_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `owner_name` varchar(255) NOT NULL
)

ALTER TABLE `jockey` ADD `jockey_returned_by` VARCHAR(255) NOT NULL AFTER `jai_member`;
ALTER TABLE `jockey` ADD `out_station_wins` INT(11) NOT NULL AFTER `allowance_claiming`, ADD `station_wins` INT(11) NOT NULL AFTER `out_station_wins`;
ALTER TABLE `owners` ADD `wirhoa` INT(11) NOT NULL DEFAULT '0' AFTER `race_card_name`;


-- mukesh start

ALTER TABLE `owners_partner` CHANGE `mem` `member` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `horse_to_owner` ADD `represntative_id` INT NOT NULL AFTER `horse_group_id`;
ALTER TABLE `horse1` ADD `passp_no` TEXT NOT NULL AFTER `horse_isb_id`;
ALTER TABLE `horse_equipments` ADD `equipment_end_date` DATE NOT NULL DEFAULT '0000-00-00' AFTER `equipment_date`;
ALTER TABLE `horse_to_trainer` ADD `trainer_status` INT NOT NULL AFTER `left_date_of_charge`;


-- mukesh end

-- mukesh start date - 01-08-2020 time - 10:10
ALTER TABLE `horse_shoeing` CHANGE `shoe_name` `full_form` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '';
ALTER TABLE `horse_shoeing` ADD `shoe_start_date` DATE NOT NULL AFTER `shoe_description`, ADD `shoe_end_date` DATE NOT NULL AFTER `shoe_start_date`;

CREATE TABLE `bit` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `short_name` varchar(255) NOT NULL,
  `long_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `horse_shoeing` ADD `shoe_description_id` INT NOT NULL AFTER `shoe_description`;
ALTER TABLE `horse_bits` ADD `end_date` DATE NOT NULL AFTER `date`;
ALTER TABLE `horse_bits` CHANGE `date` `start_date` DATE NOT NULL;
ALTER TABLE `horse_bits` ADD `bit_id` INT NOT NULL AFTER `horse_id`;

-- mukesh start date - 01-08-2020 time - 12:49

ALTER TABLE `trainers` ADD `file_number_uploads` VARCHAR(255) NOT NULL AFTER `active`, ADD `uploaded_file_sources` VARCHAR(255) NOT NULL AFTER `file_number_uploads`;


ALTER TABLE `jockey` ADD `file_number_upload` VARCHAR(255) NOT NULL AFTER `jocky_status`, ADD `uploaded_file_source` VARCHAR(255) NOT NULL AFTER `file_number_upload`;


ALTER TABLE `trainer_contact_details` ADD `address_3` VARCHAR(255) NOT NULL AFTER `country_2`, ADD `address_4` VARCHAR(255) NOT NULL AFTER `address_3`;

ALTER TABLE `jockey_contact_details` ADD `address_3` VARCHAR(255) NOT NULL AFTER `country_2`, ADD `address_4` VARCHAR(255) NOT NULL AFTER `address_3`;

ALTER TABLE `owners` ADD `address_3` VARCHAR(255) NOT NULL AFTER `country_2`, ADD `address_4` VARCHAR(255) NOT NULL AFTER `address_3`;

ALTER TABLE `trainers` ADD `epf_no` VARCHAR(255) NOT NULL AFTER `uploaded_file_sources`;

ALTER TABLE `jockey_contact_details` ADD `epf_no` VARCHAR(255) NOT NULL AFTER `prof_tax_no`;

--gaurav start---
ALTER TABLE `horse1` ADD `one_time_lavy` INT(11) NOT NULL AFTER `arrival_charges_to_be_paid`;
---gaurav end----


--mukesh 

CREATE TABLE `db_rwitc_erp2020`.`arrival_charges` ( `id` INT NOT NULL AUTO_INCREMENT , `horse_name` VARCHAR(255) NOT NULL , `horse_id` INT NOT NULL , `trainer_name` VARCHAR(255) NOT NULL , `trainer_id` INT NOT NULL , `license_type` VARCHAR(255) NOT NULL , `date` DATE NOT NULL , `arrival_charge` INT NOT NULL , `levy_charge` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

---gaurav start----
ALTER TABLE `trainers` ADD `date_of_license_issue2` DATE NOT NULL DEFAULT '0000-00-00' AFTER `date_of_license_issue`;
---end--


--mukesh 06-08-2020
ALTER TABLE `horse_to_owner` ADD `charge_type` VARCHAR(255) NOT NULL AFTER `provisional_ownership`;
CREATE TABLE `arrival_charge_owners` ( `id` INT NOT NULL AUTO_INCREMENT , `arrival_charge_id` INT NOT NULL , `horse_id` INT NOT NULL , `owner_id` INT NOT NULL , `entry_date` DATE NOT NULL , `charge_amt` FLOAT(10,4) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `arrival_charge_owners` ADD `owner_name` VARCHAR(255) NOT NULL AFTER `owner_id`;


---gaurav start---
CREATE TABLE `oc_tally_po` (
  `tally_po_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `voucher_no` varchar(255) NOT NULL,
  `voucher_type` varchar(255) NOT NULL,
  `date_of_posting` date NOT NULL,
  `po_no` varchar(255) NOT NULL,
  `pi_no` varchar(255) NOT NULL,
  `pi_date` date NOT NULL,
  `supplier_po` varchar(255) NOT NULL,
  `indent_no` varchar(255) NOT NULL,
  `cln_supplier_name` varchar(255) NOT NULL,
  `clc_supplier_ledger_code` varchar(255) NOT NULL,
  `debit_ledger_name` varchar(255) NOT NULL,
  `debit_ledger_code` varchar(255) NOT NULL,
  `name_of_the_items` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `uom` varchar(255) NOT NULL,
  `rate` decimal(10,1) NOT NULL,
  `amount` int(11) NOT NULL,
  `godown_name` varchar(255) NOT NULL,
  `narration` varchar(255) NOT NULL,
  `locations` varchar(255) NOT NULL,
  `divisions` varchar(255) NOT NULL,
  `input_sgst_not_eligible_for_itc` decimal(10,1) NOT NULL,
  `input_cgst_not_eligible_for_itc` decimal(10,1) NOT NULL,
  `input_igst_not_eligible_for_itc` decimal(10,1) NOT NULL,
  `total` int(11) NOT NULL
) 


ALTER TABLE `oc_tally_po` ADD `date_added` DATE NOT NULL DEFAULT '0000-00-00' AFTER `total`, ADD `user_id` INT(11) NOT NULL AFTER `date_added`;
--end----

ALTER TABLE `oc_rawmaterialreqitem` ADD `request` VARCHAR(255) NOT NULL AFTER `productraw_name`;

ALTER TABLE `oc_rawmaterialreqitem`  ADD `unit` VARCHAR(25) NOT NULL  AFTER `quantity`;

ALTER TABLE `oc_rawmaterialreqitem` CHANGE `product_id` `product_id` VARCHAR(25) NOT NULL;

ALTER TABLE `medicine` ADD `pack_type` VARCHAR(255) NOT NULL AFTER `reorder_qty`, ADD `volume` VARCHAR(255) NOT NULL AFTER `pack_type`, ADD `unit` VARCHAR(255) NOT NULL AFTER `volume`;

------gaurav-------
ALTER TABLE `oc_inwarditem` ADD `unit` VARCHAR(100) NOT NULL AFTER `quantity`;
----end------

ALTER TABLE `oc_rawmaterialreqitem` ADD `packing` VARCHAR(255) NOT NULL AFTER `status`, ADD `volume` VARCHAR(255) NOT NULL AFTER `packing`;

ALTER TABLE `oc_inwarditem` CHANGE `product_id` `product_id` VARCHAR(255) NOT NULL;


-----------gaurav-------

CREATE TABLE `oc_inward` (
  `order_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `order_no` int(11) NOT NULL,
  `supplier` varchar(255) NOT NULL,
  `batch_no` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  `view_status` int(11) NOT NULL
)

CREATE TABLE `oc_inwarditem` (
  `productraw_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL,
  `productraw_name` varchar(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `po_no` varchar(255) NOT NULL,
  `po_qty` int(11) NOT NULL,
  `reduce_po_qty` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `purchase_price` decimal(10,2) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `ex_date` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
)

----------gaurav end-----------

ALTER TABLE `medicine` ADD `purchase_price` INT(11) NOT NULL AFTER `unit`, ADD `gst_rate` INT(11) NOT NULL AFTER `purchase_price`;

ALTER TABLE `oc_rawmaterialreqitem` ADD `gst_rate` INT(11) NOT NULL AFTER `ordered_qty`

--mukesh 10-08-2020

CREATE TABLE `db_rwitc_erp2020`.`oc_registration_module_owners` ( `id` INT NOT NULL AUTO_INCREMENT , `parent_id` INT(11) NOT NULL , `horse_id` INT(11) NOT NULL , `owner_id` INT(11) NOT NULL , `owner_name` VARCHAR(255) NOT NULL , `owner_share` INT NOT NULL , `charge_amount` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `oc_rawmaterialreqitem` ADD `gst_value` INT(11) NOT NULL AFTER `gst_rate`;

ALTER TABLE `oc_rawmaterialreqitem` ADD `purchase_price` INT(100) NOT NULL AFTER `ordered_qty`, ADD `purchase_value` INT(100) NOT NULL AFTER `purchase_price`;


--mukesh 11-08-2020

CREATE TABLE `horse_to_owner_charges` ( `id` INT NOT NULL AUTO_INCREMENT , `horse_to_owner_id` INT NOT NULL , `horse_id` INT NOT NULL , `owner_id` INT NOT NULL , `owner_name` VARCHAR(255) NOT NULL , `share` INT NOT NULL , `amount` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `oc_rawmaterialreqitem` ADD `total` VARCHAR(255) NOT NULL AFTER `gst_value`;

ALTER TABLE `oc_rawmaterialreqitem` CHANGE `volume` `volume` DECIMAL(10,2) NOT NULL, CHANGE `ordered_qty` `ordered_qty` DECIMAL(10,2) NOT NULL, CHANGE `purchase_price` `purchase_price` DECIMAL(10,2) NOT NULL, CHANGE `purchase_value` `purchase_value` DECIMAL(10,2) NOT NULL, CHANGE `gst_rate` `gst_rate` DECIMAL(10,2) NOT NULL, CHANGE `gst_value` `gst_value` DECIMAL(10,2) NOT NULL, CHANGE `total` `total` DECIMAL(10,2) NOT NULL;

ALTER TABLE `oc_rawmaterialreq` ADD `final_total` DECIMAL(10,2) NOT NULL AFTER `requested_by`;


------gaurav 12-08-2020---
ALTER TABLE `oc_inwarditem` ADD `packing` VARCHAR(255) NOT NULL AFTER `quantity`, ADD `volume` INT(11) NOT NULL AFTER `packing`, ADD `ordered_quantity` INT(11) NOT NULL AFTER `volume`;

ALTER TABLE `oc_inwarditem` ADD `gst_rate` INT(11) NOT NULL AFTER `value`, ADD `gst_value` DECIMAL(10,2) NOT NULL AFTER `gst_rate`, ADD `total` DECIMAL(10,2) NOT NULL AFTER `gst_value`;

ALTER TABLE `oc_inwarditem` CHANGE `total` `total` DECIMAL(15,2) NOT NULL;

ALTER TABLE `oc_inwarditem` CHANGE `gst_value` `gst_value` DECIMAL(15,2) NOT NULL;

ALTER TABLE `oc_inward` ADD `value_total` DECIMAL(15,2) NOT NULL AFTER `date`, ADD `gst_value_total` DECIMAL(15,2) NOT NULL AFTER `value_total`, ADD `all_total` DECIMAL(15,2) NOT NULL AFTER `gst_value_total`;

ALTER TABLE `oc_inwarditem` ADD `date_added` DATE NOT NULL DEFAULT '0000-00-00' AFTER `total`;

$permission_datas['modify'][] = 'report/stock_report';

$data['permissions']['report/stock_report'] = 'Stock Report';

ALTER TABLE `is_vendor` ADD `uploaded_file_source` VARCHAR(255) NOT NULL AFTER `mask`;

ALTER TABLE `is_vendor` ADD `uploaded_file_source2` VARCHAR(255) NOT NULL AFTER `mask2`;

ALTER TABLE `is_vendor` ADD `uploaded_file_source3` VARCHAR(255) NOT NULL AFTER `mask3`;

ALTER TABLE `is_vendor` ADD `uploaded_file_source4` VARCHAR(255) NOT NULL AFTER `mask4`;

ALTER TABLE `medicine` CHANGE `unit_cost` `unit_cost` DECIMAL(10,2) NOT NULL;

CREATE TABLE `db_rwitc_erp`.`stacks` ( `stacks_id` INT(11) NULL AUTO_INCREMENT ,  `race_type` VARCHAR(255) NOT NULL ,  `class` VARCHAR(255) NOT NULL ,  `race_status` INT(11) NOT NULL ,    PRIMARY KEY  (`stacks_id`)) ENGINE = InnoDB;

CREATE TABLE `db_rwitc_erp`.`stacks_value` ( `stacks_value_id` INT(11) NULL AUTO_INCREMENT ,  `stacks_id` INT(11) NOT NULL ,  `stacks_type` VARCHAR(255) NOT NULL ,  `owner` DECIMAL(10,2) NOT NULL ,  `trainer` DECIMAL(10,2) NOT NULL ,  `jockey` DECIMAL(10,2) NOT NULL ,  `total` DECIMAL(10,2) NOT NULL ,  `incentive` DECIMAL(10,2) NOT NULL ,    PRIMARY KEY  (`stacks_value_id`)) ENGINE = InnoDB;

ALTER TABLE `stacks` ADD `grade` VARCHAR(255) NOT NULL AFTER `class`;
-------end-------

ALTER TABLE `oc_rawmaterialreqitem` ADD `auto_unit` VARCHAR(25) NOT NULL AFTER `unit`;

ALTER TABLE `oc_rawmaterialreqitem` CHANGE `auto_unit` `auto_unit` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `oc_rawmaterialreqitem` ADD `med_id` INT(255) NOT NULL AFTER `order_no`;

ALTER TABLE `horse_to_owner_charges` ADD `trans_id` INT NOT NULL AFTER `horse_to_owner_id`;

ALTER TABLE `prospectus` ADD `stacks_per` INT(11) NOT NULL AFTER `race_description`;

ALTER TABLE `race_stakes_details` ADD `forefit_date` DATE NOT NULL AFTER `entry_fees`, ADD `forefit_time` VARCHAR(255) NOT NULL AFTER `forefit_date`, ADD `forefit_amt` INT NOT NULL AFTER `forefit_time`;

ALTER TABLE `oc_rawmaterialreq` ADD `final_value` DECIMAL(10,5) NOT NULL AFTER `requested_by`, ADD `final_gst_value` DECIMAL(10,5) NOT NULL AFTER `final_value`;

ALTER TABLE `oc_rawmaterialreq` ADD `approval` INT(11) NOT NULL AFTER `requested_by`;
------gaurav-------
CREATE TABLE `class_entry` ( `class_entry_id` INT(11) NULL AUTO_INCREMENT ,  `distance` VARCHAR(255) NOT NULL ,  `class` VARCHAR(255) NOT NULL ,  `entry` VARCHAR(255) NOT NULL ,  `acceptance` VARCHAR(255) NOT NULL ,  `division` VARCHAR(255) NOT NULL ,  `nov_dec_entry` VARCHAR(255) NOT NULL ,  `nov_dec_acceptance` VARCHAR(255) NOT NULL ,  `sweepstake` VARCHAR(255) NOT NULL ,    PRIMARY KEY  (`class_entry_id`)) ENGINE = InnoDB;

ALTER TABLE `class_entry` CHANGE `distance` `distance` INT(11) NOT NULL;


ALTER TABLE `class_entry` CHANGE `nov_dec_entry` `two_nov_dec_entry` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `class_entry` CHANGE `nov_dec_acceptance` `two_nov_dec_acceptance` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

------end--------

------end--------

ALTER TABLE `oc_rawmaterialreq` ADD `approved_by` VARCHAR(255) NOT NULL AFTER `approval`;

ALTER TABLE `oc_inward` DROP `batch_no`;

ALTER TABLE `oc_inwarditem` ADD `batch_no` INT(11) NOT NULL AFTER `ex_date`;

ALTER TABLE `horse1` ADD `license_type` VARCHAR(11) NOT NULL AFTER `chip_no2`;

ALTER TABLE `oc_rawmaterialreqitem` ADD `supplier_name` VARCHAR(255) NOT NULL AFTER `productraw_name`;

ALTER TABLE `doctor` ADD `parent` VARCHAR(255) NOT NULL AFTER `ambulance_no`, ADD `hidden_parent` INT(11) NOT NULL AFTER `parent`;

ALTER TABLE `doctor` ADD `is_parent` INT(11) NOT NULL DEFAULT '0' AFTER `hidden_parent`;

ALTER TABLE `doctor` CHANGE `hidden_parent` `parent_id` INT(11) NOT NULL;

ALTER TABLE `horse1` ADD `max_win` INT NOT NULL AFTER `passp_no`, ADD `maidens` INT NOT NULL AFTER `max_win`, ADD `unraced` INT NOT NULL AFTER `maidens`, ADD `unplaced` INT NOT NULL AFTER `unraced`;
ALTER TABLE `entry` ADD `bonafide_count` INT NOT NULL AFTER `race_description`;
ALTER TABLE `horse1` ADD `not1_2_3` INT NOT NULL AFTER `unplaced`;


ALTER TABLE `oc_inwarditem` ADD `cons_status` INT NOT NULL AFTER `status`;

CREATE TABLE `medicine_trans_items` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `expire_date` date NOT NULL,
  `product_qty` int(11) NOT NULL
) ;
ALTER TABLE `medicine_trans_items`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `medicine_trans_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


CREATE TABLE `medicine_transfer` (
  `id` int(11) NOT NULL,
  `issue_no` int(11) NOT NULL,
  `parent_doctor_id` int(11) NOT NULL,
  `parent_doctor_name` text NOT NULL,
  `child_doctor_id` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `total_item` int(11) NOT NULL,
  `total_qty` int(11) NOT NULL,
  `total_amt` int(11) NOT NULL
) ;
ALTER TABLE `medicine_transfer`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `medicine_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


CREATE TABLE `inward_medicine_transfer` (
  `id` int(11) NOT NULL,
  `inward_id` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `transfer_date` date NOT NULL
) ;

ALTER TABLE `inward_medicine_transfer`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `inward_medicine_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

ALTER TABLE `horse_to_trainer` ADD `provisional` VARCHAR(11) NOT NULL AFTER `extra_narration_two`;

ALTER TABLE `medicine` CHANGE `normal` `normal` DECIMAL(10,2) NOT NULL;

ALTER TABLE `medicine` CHANGE `cost_a1` `cost_a1` DECIMAL(10,2) NOT NULL, CHANGE `cost_a2` `cost_a2` DECIMAL(10,2) NOT NULL, CHANGE `cost_a3` `cost_a3` DECIMAL(10,2) NOT NULL, CHANGE `cost_a4` `cost_a4` DECIMAL(10,2) NOT NULL, CHANGE `cost_a5` `cost_a5` DECIMAL(10,2) NOT NULL, CHANGE `cost_a6` `cost_a6` DECIMAL(10,2) NOT NULL, CHANGE `cost_a7` `cost_a7` DECIMAL(10,2) NOT NULL, CHANGE `cost_a8` `cost_a8` DECIMAL(10,2) NOT NULL, CHANGE `cost_a9` `cost_a9` DECIMAL(10,2) NOT NULL, CHANGE `cost_a10` `cost_a10` DECIMAL(10,2) NOT NULL;

ALTER TABLE `horse1` DROP `license_type`;

ALTER TABLE `trainers` ADD `trainer_code_new` INT(11) NOT NULL AFTER `trainer_code`;

ALTER TABLE `jockey` ADD `jockey_code_new` INT(11) NOT NULL AFTER `jockey_code`;

ALTER TABLE `oc_inward` ADD `email_send` INT(11) NOT NULL DEFAULT '0' AFTER `view_status`;

ALTER TABLE `oc_inward` ADD `approval_status` INT(11) NOT NULL DEFAULT '0' AFTER `email_send`;

ALTER TABLE `oc_inward` ADD `approved_by` INT(11) NOT NULL DEFAULT '0' AFTER `approval_status`;



CREATE TABLE `oc_treatment_entry` (
  `id` int(11) NOT NULL,
  `issue_no` int(11) NOT NULL,
  `clinic_id` int(11) NOT NULL,
  `clinic_name` varchar(255) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `horse_name` varchar(255) NOT NULL,
  `horse_name_id` int(11) NOT NULL,
  `trainer_name` varchar(255) NOT NULL,
  `trainer_name_id` int(11) NOT NULL,
  `total_item` int(11) NOT NULL,
  `total_qty` int(11) NOT NULL,
  `total_amt` decimal(10,2) NOT NULL,
  `entry_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `oc_treatment_entry`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_treatment_entry`
--
ALTER TABLE `oc_treatment_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

CREATE TABLE `oc_treatment_entry_trans` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `medicine_code` varchar(255) NOT NULL,
  `medicine_name` varchar(255) NOT NULL,
  `medicine_qty` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `value` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `oc_treatment_entry_trans`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `oc_treatment_entry_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

ALTER TABLE `oc_rawmaterialreq` ADD `reason` VARCHAR(255) NOT NULL AFTER `approval`;

ALTER TABLE `arrival_charges` ADD `entry_status` INT NOT NULL AFTER `levy_charge`;

CREATE TABLE `db_rwitc_2020`.`schedule_mail` ( `id` INT NOT NULL AUTO_INCREMENT , `description_id` INT NOT NULL , `status` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `db_rwitc_2020`.`schedule_mail_trans` ( `id` INT NOT NULL AUTO_INCREMENT , `parent_id` INT NOT NULL , `description_id` INT NOT NULL , `status` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `schedule_mail` CHANGE `description_id` `description` TEXT NOT NULL;
ALTER TABLE `schedule_mail_trans` CHANGE `parent_id` `schedule_mail_id` INT(11) NOT NULL;

ALTER TABLE `oc_treatment_entry_trans` ADD `syringe_one` VARCHAR(255) NOT NULL AFTER `value`, ADD `syringe_two` VARCHAR(255) NOT NULL AFTER `syringe_one`, ADD `needle_one` VARCHAR(255) NOT NULL AFTER `syringe_two`, ADD `needle_two` VARCHAR(255) NOT NULL AFTER `needle_one`;

ALTER TABLE `trainers` CHANGE `trainer_code_new` `trainer_code_new` VARCHAR(50) NULL DEFAULT NULL;


------------27/08/2020--------------
ALTER TABLE `jockey` ADD `age` INT(11) NOT NULL AFTER `birth_date`;

ALTER TABLE `jockey` ADD `Fathers_name` VARCHAR(255) NOT NULL AFTER `jockey_name`, ADD `educational_qualification` VARCHAR(255) NOT NULL AFTER `Fathers_name`, ADD `emergency_contact_person` VARCHAR(255) NOT NULL AFTER `educational_qualification`, ADD `contact_no` INT(11) NOT NULL AFTER `emergency_contact_person`;


ALTER TABLE `jockey_renewal_history` ADD `jockey_name` VARCHAR(255) NOT NULL AFTER `trainer_id`;
ALTER TABLE `jockey_renewal_history` ADD `licence_start_date` DATE NOT NULL AFTER `amount`, ADD `license_end_date` DATE NOT NULL AFTER `licence_start_date`, ADD `license_type` VARCHAR(255) NOT NULL AFTER `license_end_date`, ADD `apprenties` VARCHAR(255) NOT NULL AFTER `license_type`;
ALTER TABLE `jockey_renewal_history` CHANGE `licence_start_date` `license_start_date` DATE NOT NULL;
ALTER TABLE `jockey_renewal_history` ADD `season_start` DATE NOT NULL AFTER `apprenties`, ADD `season_end` DATE NOT NULL AFTER `season_start`;
ALTER TABLE `jockey_renewal_history` ADD `entry_status` INT NOT NULL AFTER `season_end`;

CREATE TABLE `trainer_staff_entry` (
  `id` int(11) NOT NULL,
  `staff` varchar(255) NOT NULL,
  `file_number_upload` varchar(255) NOT NULL,
  `uploaded_file_source` varchar(255) NOT NULL,
  `adhar_no` varchar(12) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `blood_group` varchar(11) NOT NULL,
  `trainer_name` varchar(255) NOT NULL,
  `trainer_id` int(11) NOT NULL
)

ALTER TABLE `trainer_staff_entry`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `trainer_staff_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


ALTER TABLE `trainer_renewal_history` ADD `trainer_name` VARCHAR(255) NOT NULL AFTER `amount`, ADD `license_start_date` DATE NOT NULL AFTER `trainer_name`, ADD `license_end_date` DATE NOT NULL AFTER `license_start_date`, ADD `license_type` VARCHAR(255) NOT NULL AFTER `license_end_date`, ADD `apprenties` VARCHAR(255) NOT NULL AFTER `license_type`, ADD `season_start` DATE NOT NULL AFTER `apprenties`, ADD `season_end` DATE NOT NULL AFTER `season_start`, ADD `entry_status` INT(11) NOT NULL AFTER `season_end`;



ALTER TABLE `medicine_trans_items` ADD `entry_date` DATE NOT NULL DEFAULT '0000-00-00' AFTER `product_qty`;

ALTER TABLE `oc_rawmaterialreq` CHANGE `final_value` `final_value` DECIMAL(10,2) NOT NULL, CHANGE `final_gst_value` `final_gst_value` DECIMAL(10,2) NOT NULL, CHANGE `final_total` `final_total` DECIMAL(10,2) NOT NULL;

CREATE TABLE `daily_reconsilation` (
  `id` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `medicine_name` varchar(255) NOT NULL,
  `medicine_code` varchar(255) NOT NULL,
  `store_unit` varchar(255) NOT NULL DEFAULT '',
  `actual_qty` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `daily_reconsilation`
  ADD PRIMARY KEY (`id`);

  ALTER TABLE `daily_reconsilation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

ALTER TABLE `oc_rawmaterialreq` CHANGE `order_no` `order_no` INT(255) NOT NULL;

ALTER TABLE `oc_inward` ADD `reason` VARCHAR(255) NOT NULL AFTER `approval_status`;

ALTER TABLE `horse_to_owner` CHANGE `owner_percentage` `owner_percentage` FLOAT(10,2) NOT NULL;

ALTER TABLE `oc_treatment_entry_trans` ADD `syringe_three` VARCHAR(255) NOT NULL AFTER `syringe_two`, ADD `syringe_four` VARCHAR(255) NOT NULL AFTER `syringe_three`, ADD `syringe_five` VARCHAR(255) NOT NULL AFTER `syringe_four`, ADD `syringe_six` VARCHAR(255) NOT NULL AFTER `syringe_five`, ADD `syringe_seven` VARCHAR(255) NOT NULL AFTER `syringe_six`;
ALTER TABLE `oc_treatment_entry_trans` ADD `needle_three` VARCHAR(255) NOT NULL AFTER `needle_two`, ADD `needle_four` VARCHAR(255) NOT NULL AFTER `needle_three`, ADD `needle_five` VARCHAR(255) NOT NULL AFTER `needle_four`, ADD `needle_six` VARCHAR(255) NOT NULL AFTER `needle_five`, ADD `needle_seven` VARCHAR(255) NOT NULL AFTER `needle_six`;

CREATE TABLE `db_rwitc_erp`.`supplier_logs` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `group_id` INT(11) NOT NULL , `log_id` INT(11) NOT NULL , `fname` VARCHAR(255) NOT NULL , `lname` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `supplier_logs` ADD `log_date` DATE NOT NULL AFTER `lname`, ADD `log_time` VARCHAR(255) NOT NULL AFTER `log_date`;

ALTER TABLE `supplier_logs` ADD `supp_id` INT(11) NOT NULL AFTER `log_time`;

CREATE TABLE `db_rwitc_erp`.`doctor_logs` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `group_id` INT(11) NOT NULL , `log_id` INT(11) NOT NULL , `fname` VARCHAR(255) NOT NULL , `lname` VARCHAR(255) NOT NULL , `log_date` DATE NOT NULL , `log_time` VARCHAR(255) NOT NULL , `doc_id` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `db_rwitc_erp`.`service_logs` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `group_id` INT(11) NOT NULL , `log_id` INT(11) NOT NULL , `fname` VARCHAR(255) NOT NULL , `lname` VARCHAR(255) NOT NULL , `log_date` DATE NOT NULL , `log_time` VARCHAR(255) NOT NULL , `serv_id` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `db_rwitc_erp`.`medicine_logs` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `group_id` INT(11) NOT NULL , `log_id` INT(11) NOT NULL , `fname` VARCHAR(255) NOT NULL , `lname` VARCHAR(255) NOT NULL , `log_date` DATE NOT NULL , `log_time` INT(255) NOT NULL , `med_id` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `trainer_renewal_history` ADD `no_horse` INT(11) NOT NULL AFTER `fees`;

ALTER TABLE `jockey_renewal_history` ADD `age` INT(11) NOT NULL AFTER `jockey_name`;

ALTER TABLE `jockey` ADD `user_id` INT(11) NOT NULL AFTER `uploaded_file_source`, ADD `last_update_date` DATE NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `user_id`;

ALTER TABLE `trainers` ADD `user_id` INT(11) NOT NULL AFTER `epf_no`, ADD `last_update_date` DATE NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `user_id`;

ALTER TABLE `horse1` ADD `user_id` INT(11) NOT NULL AFTER `passp_no`, ADD `last_update_date` DATE NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `user_id`;

ALTER TABLE `owners` ADD `user_id` INT(11) NOT NULL AFTER `uploaded_file_sources`, ADD `last_update_date` DATE NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `user_id`;

CREATE TABLE `db_rwitc_erp`.`indent_logs` ( `id` INT NOT NULL AUTO_INCREMENT , `group_id` INT NOT NULL , `log_id` INT NOT NULL , `fname` INT NOT NULL , `lname` INT NOT NULL , `log_date` INT NOT NULL , `log_time` INT NOT NULL , `ind_id` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `indent_logs` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `fname` `fname` VARCHAR(255) NOT NULL, CHANGE `lname` `lname` VARCHAR(255) NOT NULL, CHANGE `log_date` `log_date` DATE NOT NULL, CHANGE `log_time` `log_time` VARCHAR(255) NOT NULL;

ALTER TABLE `indent_logs` CHANGE `log_date` `log_date` DATE NOT NULL;

CREATE TABLE `db_rwitc_erp`.`inward_logs` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `group_id` INT(11) NOT NULL , `log_id` INT(11) NOT NULL , `fname` VARCHAR(255) NOT NULL , `lname` VARCHAR(255) NOT NULL , `log_date` DATE NOT NULL , `log_time` VARCHAR(255) NOT NULL , `inw_id` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `db_rwitc_erp`.`med_trans_logs` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `group_id` INT(11) NOT NULL , `log_id` INT(11) NOT NULL , `fname` VARCHAR(255) NOT NULL , `lname` VARCHAR(255) NOT NULL , `log_date` DATE NOT NULL , `log_time` VARCHAR(255) NOT NULL , `medt_id` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `db_rwitc_erp`.`treate_logs` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `group_id` INT(11) NOT NULL , `log_id` INT(11) NOT NULL , `fname` VARCHAR(255) NOT NULL , `lname` VARCHAR(255) NOT NULL , `log_date` DATE NOT NULL , `log_time` VARCHAR(255) NOT NULL , `treate_id` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `db_rwitc_erp`.`treatem_logs` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `group_id` INT(11) NOT NULL , `log_id` INT(11) NOT NULL , `fname` VARCHAR(255) NOT NULL , `lname` VARCHAR(255) NOT NULL , `log_date` DATE NOT NULL , `log_time` VARCHAR(255) NOT NULL , `treatem_id` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `medicine` ADD `alpha` TEXT NOT NULL AFTER `med_code`, ADD `alpha_no` INT NOT NULL AFTER `alpha`;

ALTER TABLE `doctor` ADD `alpha` TEXT NOT NULL AFTER `isActive`, ADD `alpha_no` INT(255) NOT NULL AFTER `alpha`;

ALTER TABLE `jockey_renewal_history` ADD `user_id` INT(11) NOT NULL AFTER `entry_status`, ADD `last_update_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `user_id`;

ALTER TABLE `trainer_renewal_history` ADD `user_id` INT(11) NOT NULL AFTER `entry_status`, ADD `last_update_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `user_id`;

ALTER TABLE `oc_registration_module` ADD `user_id` INT(11) NOT NULL AFTER `modification`, ADD `last_update_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `user_id`;

ALTER TABLE `arrival_charges` ADD `user_id` INT(11) NOT NULL AFTER `entry_status`, ADD `last_update_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `user_id`;


ALTER TABLE `horse_to_owner` ADD `user_id` INT(11) NOT NULL AFTER `charge_type`, ADD `date_added` DATE NOT NULL DEFAULT '0000-00-00' AFTER `user_id`;

ALTER TABLE `horse_to_owner` ADD `cancel_status` VARCHAR(255) NOT NULL DEFAULT '' AFTER `date_added`;

ALTER TABLE `horse_to_owner` ADD `cancel_lease_status` VARCHAR(255) NOT NULL DEFAULT '' AFTER `date_added`;

ALTER TABLE `horse_to_owner` ADD `cancel_contigencey` VARCHAR(255) NOT NULL DEFAULT '' AFTER `cancel_lease_status`;
ALTER TABLE `horse_to_owner` ADD `entry_count` INT NOT NULL AFTER `cancel_status`;

ALTER TABLE `horse_to_owner` ADD `join_color` VARCHAR(100) NOT NULL AFTER `owner_color`;

ALTER TABLE `oc_rawmaterialreqitem` ADD `inward_med_treat_quantity` INT(11) NOT NULL DEFAULT '0' AFTER `total`;
ALTER TABLE `horse_to_owner` ADD `grandpa_id` INT NOT NULL AFTER `entry_count`;

CREATE TABLE IF NOT EXISTS `horse_to_owner_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) NOT NULL,
  `horse_id` int(11) NOT NULL,
  `horse_to_owner_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `racetypes` ADD `l_description` VARCHAR(255) NOT NULL AFTER `category`, ADD `l_category` VARCHAR(255) NOT NULL AFTER `l_description`;

ALTER TABLE `racetypes` ADD `rating_from` DATE NOT NULL DEFAULT '0000-00-00' AFTER `category`, ADD `rating_to` DATE NOT NULL DEFAULT '0000-00-00' AFTER `rating_from`;

ALTER TABLE `racetypes` ADD `l_rating_from` DATE NOT NULL DEFAULT '0000-00-00' AFTER `l_category`, ADD `l_rating_to` DATE NOT NULL DEFAULT '0000-00-00' AFTER `l_rating_from`;

ALTER TABLE `racetypes` CHANGE `rating_from` `rating_from` INT(11) NOT NULL, CHANGE `rating_to` `rating_to` INT(11) NOT NULL, CHANGE `l_rating_from` `l_rating_from` INT(11) NOT NULL, CHANGE `l_rating_to` `l_rating_to` INT(11) NOT NULL;

ALTER TABLE `prospectus` ADD `serial_no` INT(11) NOT NULL AFTER `race_date`;

ALTER TABLE `daily_reconsilation` ADD `doctor_id` INT(11) NOT NULL AFTER `entry_date`, ADD `doctor_name` VARCHAR(255) NOT NULL DEFAULT '' AFTER `doctor_id`;

ALTER TABLE `prospectus` ADD `rating_from` INT(11) NOT NULL DEFAULT '0' AFTER `race_description`, ADD `rating_to` INT(11) NOT NULL DEFAULT '0' AFTER `rating_from`;

ALTER TABLE `daily_reconsilation` ADD `transfer_qty` INT(11) NOT NULL AFTER `actual_qty`, ADD `opening` INT(11) NOT NULL AFTER `transfer_qty`, ADD `treatment_medicine_qty` INT(11) NOT NULL AFTER `opening`, ADD `closing` INT(11) NOT NULL AFTER `treatment_medicine_qty`, ADD `day_close_status` INT(11) NOT NULL AFTER `closing`;


ALTER TABLE `trainers` ADD `blood_group` VARCHAR(255) NOT NULL AFTER `date_of_birth`;
ALTER TABLE `trainers` ADD `is_tfi` INT NOT NULL AFTER `is_wita`, ADD `is_kta` INT NOT NULL AFTER `is_tfi`;
ALTER TABLE `trainers` CHANGE `is_tfi` `is_tfi` VARCHAR(11) NOT NULL, CHANGE `is_kta` `is_kta` VARCHAR(11) NOT NULL;
ALTER TABLE `horse_to_trainer` ADD `bulk_transfer` INT NOT NULL AFTER `provisional`;

ALTER TABLE `horse_equipments` ADD `reason` VARCHAR( 255 ) NOT NULL AFTER `equipment_end_date` ;

CREATE TABLE `jockey_ban_offences` ( `id` INT NOT NULL AUTO_INCREMENT , `offences` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `jockey_ban` ADD `offences` TEXT NOT NULL AFTER `reason`, ADD `horse_id` VARCHAR(255) NOT NULL AFTER `offences`, ADD `horse_name` VARCHAR(255) NOT NULL AFTER `horse_id`, ADD `race_no` VARCHAR(255) NOT NULL AFTER `horse_name`, ADD `race_date` DATE NOT NULL AFTER `race_no`, ADD `wdr_licence` INT NOT NULL AFTER `race_date`, ADD `ride_work` INT NOT NULL AFTER `wdr_licence`;

ALTER TABLE `jockey_ban` ADD `mock_race` INT NOT NULL AFTER `ride_work`;

ALTER TABLE `jockey_ban` ADD `jockey_code` VARCHAR(255) NOT NULL AFTER `jockey_id`;
ALTER TABLE `jockey` CHANGE `jockey_code_new` `jockey_code_new` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `jockey_ban` ADD `jockey_name` VARCHAR(255) NOT NULL AFTER `jockey_code`;
ALTER TABLE `jockey_ban` ADD `reason_stewards` TEXT NOT NULL AFTER `mock_race`, ADD `reason_board_of_authority` TEXT NOT NULL AFTER `reason_stewards`;
ALTER TABLE `trainers` CHANGE `trainer_code_new` `trainer_code_new` VARCHAR(255) NOT NULL;


ALTER TABLE `jockey_ban` ADD `stipes_entry_date` DATE NOT NULL AFTER `reason_board_of_authority`, ADD `stewards_offences` VARCHAR(255) NOT NULL AFTER `stipes_entry_date`, ADD `stewards_fine` INT(11) NOT NULL AFTER `stewards_offences`, ADD `stewards_entry_date` DATE NOT NULL AFTER `stewards_fine`, ADD `board_of_authority_offences` VARCHAR(255) NOT NULL AFTER `stewards_entry_date`, ADD `board_of_authority_fine` INT(11) NOT NULL AFTER `board_of_authority_offences`, ADD `board_of_authority_entry_date` DATE NOT NULL AFTER `board_of_authority_fine`;

ALTER TABLE `trainer_ban` ADD `trainer_code` VARCHAR(255) NOT NULL AFTER `trainer_id`, ADD `trainer_name` VARCHAR(255) NOT NULL AFTER `trainer_code`;
ALTER TABLE `trainer_ban`  ADD `offences` TEXT NOT NULL  AFTER `reason`,  ADD `horse_id` VARCHAR(255) NOT NULL  AFTER `offences`,  ADD `horse_name` VARCHAR(255) NOT NULL  AFTER `horse_id`,  ADD `race_no` VARCHAR(255) NOT NULL  AFTER `horse_name`,  ADD `race_date` DATE NOT NULL  AFTER `race_no`,  ADD `wdr_licence` INT(11) NOT NULL  AFTER `race_date`,  ADD `ride_work` INT(11) NOT NULL  AFTER `wdr_licence`,  ADD `mock_race` INT(11) NOT NULL  AFTER `ride_work`,  ADD `reason_stewards` TEXT NOT NULL  AFTER `mock_race`,  ADD `reason_board_of_authority` TEXT NOT NULL  AFTER `reason_stewards`,  ADD `stipes_entry_date` DATE NOT NULL  AFTER `reason_board_of_authority`,  ADD `stewards_offences` TEXT NOT NULL  AFTER `stipes_entry_date`,  ADD `stewards_fine` INT(11) NOT NULL  AFTER `stewards_offences`,  ADD `stewards_entry_date` DATE NOT NULL  AFTER `stewards_fine`,  ADD `board_of_authority_offences` TEXT NOT NULL  AFTER `stewards_entry_date`,  ADD `board_of_authority_fine` INT(11) NOT NULL  AFTER `board_of_authority_offences`,  ADD `board_of_authority_entry_date` DATE NOT NULL  AFTER `board_of_authority_fine`;


CREATE TABLE `store_reconsilation` (
  `id` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `doctor_name` varchar(255) NOT NULL,
  `medicine_name` varchar(255) NOT NULL,
  `medicine_code` varchar(255) NOT NULL,
  `opening` int(11) NOT NULL,
  `closing` int(11) NOT NULL,
  `day_close_status` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `store_reconsilation`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `store_reconsilation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

CREATE TABLE `medicine_return` (
  `id` int(11) NOT NULL,
  `issue_no` int(11) NOT NULL,
  `parent_doctor_id` int(11) NOT NULL,
  `parent_doctor_name` varchar(255) NOT NULL,
  `child_doctor_id` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `total_item` int(11) NOT NULL,
  `total_qty` int(11) NOT NULL,CREATE TABLE `medicine_return_items` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `expire_date` date NOT NULL,
  `product_qty` int(11) NOT NULL,
  `entry_date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  `total_amt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;ALTER TABLE `medicine_return`
  ADD PRIMARY KEY (`id`);ALTER TABLE `medicine_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;CREATE TABLE `medicine_return_items` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `expire_date` date NOT NULL,
  `product_qty` int(11) NOT NULL,
  `entry_date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `medicine_return_items`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `medicine_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
