<?php
class ModelReportHorseTreatmentReport extends Model {

	public function getHorses($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_treatment_entry_trans tt LEFT JOIN oc_treatment_entry t ON (tt.parent_id = t.id)";
			
		$sql .= " WHERE 1=1";

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sql .= " ORDER BY entry_date DESC ";

		$sort_data = array(
			'order_no',
		);

		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}
}
