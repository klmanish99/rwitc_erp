<?php 
class ModelReportnamechangereport extends Model {
	public function getUndertakingChareges($data = array()) {//echo "<pre>";print_r($data);
		$horse_sql = "SELECT * FROM `oc_registration_module` WHERE 1 = 1";

		if (!empty($data['filter_hourse_id'])) {
			$horse_sql .= " AND horse_id = '" . (int)$data['filter_hourse_id'] . "'";
		}

		if (!empty($data['filter_date'])) {
			$horse_sql .= " AND DATE(registration_date) >= '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date']))). "'";
		}

		if (!empty($data['filter_date_end'])) {
			$horse_sql .= " AND DATE(registration_date) <= '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date_end']))). "'";
		}
		$horse_sql .= " AND registeration_type != 'ISB'";
		
		$horse_sql .=  " ORDER BY horse_id";

		$final_array = array();
		$owner_datas = array();
		$horse_query1 = $this->db->query($horse_sql);//echo "<pre>";print_r($horse_query1->rows);exit;
		if($horse_query1->num_rows > 0){
			foreach ($horse_query1->rows as $key => $value) {
					//$result[$value['horse_id']]['horse_data'][] = $hqvalue;
				$registered_horseowner_query = "SELECT * FROM oc_registration_module_owners WHERE horse_id = '".$value['horse_id']."' AND parent_id = '".$value['id']."' ";
				if (!empty($data['filter_owner_id'])) {
					$registered_horseowner_query .= " AND owner_id = '" . $this->db->escape($data['filter_owner_id']) . "'";
				}
				$registered_horseowner_query_datas = $this->db->query($registered_horseowner_query);
				$owner_datasss = array();
				if($registered_horseowner_query_datas->num_rows > 0){
					foreach ($registered_horseowner_query_datas->rows as $okey => $ovalue) {
						$owner_query = $this->db->query("SELECT ownership_type, provisional_ownership,date_of_ownership,end_date_of_ownership FROM `horse_to_owner` WHERE `horse_id` = '".$ovalue['horse_id']."' AND to_owner_id = '".$ovalue['owner_id']."'");

						if($owner_query->num_rows > 0){
							$owner_datas['ownership_type'] = $owner_query->row['ownership_type'];
							$owner_datas['provisional_ownership'] = $owner_query->row['provisional_ownership'];
							$owner_datas['date_of_ownership'] = $owner_query->row['date_of_ownership'];
							$owner_datas['end_date_of_ownership'] = $owner_query->row['end_date_of_ownership'];

						} else {
							//$owner_datas = array();
							$owner_datas['ownership_type'] = "";
							$owner_datas['provisional_ownership'] = "";
							$owner_datas['date_of_ownership'] = "";
							$owner_datas['end_date_of_ownership'] = "";
							
						}
						$owner_datasss[] = array(
							'owner_name' => $ovalue['owner_name'],
							'owner_sharss' => $ovalue['owner_share'],
							'charge_amount' => $ovalue['charge_amount'],
							'ownership_type' => $owner_datas['ownership_type'],
							'provisional_ownership' => $owner_datas['provisional_ownership'],
							'date_of_ownership' => $owner_datas['date_of_ownership'],
							'end_date_of_ownership' => $owner_datas['end_date_of_ownership'],
							
						);
					}
				}

				$final_array[$key] = array(
					'horse_name'=> $value['horse_name'],
					'registeration_type'=> $value['registeration_type'],
					'registeration_fee'=> $value['registeration_fee'],
					'remark'=> $value['remark'],
					'modification'=> $value['modification'],
					'registration_date'=> $value['registration_date'],
					'owners_datas' => $owner_datasss	
				);
			}
		}

		//echo "<pre>";print_r($final_array);exit;
		return $final_array;
	}

	public function getOwnerAuto($owner) {

		$sql =("SELECT DISTINCT(`owner_id`),owner_name  FROM oc_registration_module_owners WHERE 1 = 1");
		if($owner != ''){
			$sql .= " AND `owner_name` LIKE '%" . $this->db->escape($owner) . "%'";
		}
		$sql .= " ORDER BY `owner_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	
}