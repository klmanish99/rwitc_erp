<?php 
class ModelReportjockeylicensereport extends Model {
	public function getJockeyLicense($data = array()) {
		$sql = "SELECT * FROM  jockey_renewal_history WHERE 1 = 1";


		if (!empty($data['filter_date'])) {
			$sql .= " AND license_start_date >= '" . date('Y-m-d', strtotime($data['filter_date'])). "'";
		}

		if (!empty($data['filter_end_dates'])) {
			$sql .= " AND license_end_date <= '" . date('Y-m-d', strtotime($data['filter_end_dates'])). "'";
		}

		$sql .= " ORDER BY id ASC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			/*echo $sql;exit;*/
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalJockeyLicense($data = array()) { 
		$sql = ("SELECT COUNT(*) AS total FROM jockey_renewal_history WHERE 1 = 1");

		if (!empty($data['filter_date'])) {
			$sql .= " AND license_start_date >= '" . date('Y-m-d', strtotime($data['filter_date'])). "'";
		}

		if (!empty($data['filter_end_dates'])) {
			$sql .= " AND license_end_date <= '" . date('Y-m-d', strtotime($data['filter_end_dates'])). "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	
}