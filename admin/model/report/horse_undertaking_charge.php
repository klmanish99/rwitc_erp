<?php 
class ModelReporthorseundertakingcharge extends Model {
	public function getUndertakingChareges($data = array()) {
		$sql = "SELECT * FROM  horse_to_trainer WHERE undertaking_charge = 1";

		/*if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_group'])) {
			$group = $data['filter_group'];
		} else {
			$group = 'week';
		}*/



		if (!empty($data['filter_trainer_name'])) {
			$sql .= " AND LOWER(trainer_name) LIKE '%" . $this->db->escape(strtolower($data['filter_trainer_name'])) . "%'";
		}

		if (!empty($data['filter_hourse_id'])) {
			$sql .= " AND `horse_id` = '" . ($data['filter_hourse_id']) . "' ";
		}


		$sql .= " ORDER BY left_date_of_charge ASC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			/*echo $sql;exit;*/
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUndertakingChareges($data = array()) { 
		$sql = ("SELECT COUNT(*) AS total FROM horse_to_trainer WHERE undertaking_charge = 1");

		if (!empty($data['filter_trainer_name'])) {
			$sql .= " AND LOWER(trainer_name) LIKE '%" . $this->db->escape(strtolower($data['filter_trainer_name'])) . "%'";
		}

		if (!empty($data['filter_hourse_id'])) {
			$sql .= " AND `horse_id` = '" . ($data['filter_hourse_id']) . "' ";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	
}