<?php 
class ModelReportMedicineTransaction extends Model {
	public function getMedicineTranscation($data = array()) {
		$sql = "SELECT DISTINCT  * FROM `medicine_transfer`m LEFT JOIN medicine_trans_items mi ON(m.id = mi.parent_id) WHERE 1 = 1";

		if (!empty($data['filter_doctor_id'])) {
			$sql .= " AND (case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end)  = '" .$this->db->escape($data['filter_doctor_id']). "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND m.entry_date <= '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date']))). "'";
		}
		$sql .= " GROUP BY mi.product_id ORDER BY m.entry_date ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
		//echo $sql;exit;
/*
		$sql = "SELECT * FROM `medicine_transfer` WHERE 1 = 1";

		if (!empty($data['filter_date'])) {
			$sql .= " AND entry_date = '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date']))). "'";
		}

		if (!empty($data['filter_doctor_id'])) {
			$sql .= " AND parent_doctor_id = '" .$this->db->escape($data['filter_doctor_id']). "'";
		}
		$sql .= " GROUP BY entry_date ";

		//echo $sql;exit;

		$query = $this->db->query($sql);

		$final_array = array();
		if($query->num_rows > 0){
			foreach ($query->rows as $key => $value) {
				$medicine_product = $this->db->query("SELECT *, SUM(product_qty) AS total_trans FROM `medicine_trans_items` WHERE `entry_date` = '".$value['entry_date']."' GROUP BY product_id, entry_date");

				if($medicine_product->num_rows > 0){

						
					foreach ($medicine_product->rows as $pkey => $palue) {
						$medicine_product = $this->db->query("SELECT store_unit FROM `medicine` WHERE `med_code` = '".$palue['product_id']."' ");
						if($medicine_product->num_rows > 0){
							$medicine_product_code = $medicine_product->row['store_unit'] ;
						} else {
							$medicine_product_code = '' ;
						}
						$final_array[] = array(
							'entry_date'=> $palue['entry_date'],
							'total_trans'=> $palue['total_trans'],
							'product_name'=> $palue['product_name'],
							'product_id'=> $palue['product_id'],
							'store_unit'=> $medicine_product_code,
						);
					}
				}

			}
		}*/
		//echo "<pre>";print_r($final_array);exit;
		
		//return $final_array;
	}

	public function addMedData($data) {//echo "<pre>";print_r($data);exit;
		
		

		foreach ($data['productraw_datas'] as $key => $value) {
			$this->db->query("INSERT INTO `daily_reconsilation` SET 
			`entry_date` = '".date('Y-m-d', strtotime($data['filter_date']))."',
			`medicine_name` = '".$this->db->escape($value['product_name'])."',
			`medicine_code` = '".$this->db->escape($value['product_id'])."',
			`store_unit` = '".$this->db->escape($value['store_unit'])."',
			`doctor_name` = '".$this->db->escape($data['filter_doctor_name'])."',
			`doctor_id` = '".$data['filter_doctor_id']."',
			`transfer_qty` = '".$value['transfer_qty']."' ,
			`treatment_medicine_qty` = '".$value['treatment_medicine_qty']."' ,
			`opening` = '".$value['opening']."' , 
			`closing` = '".$value['closing']."' ,
			`day_close_status` = '1' 
			");
		}
		
	}

	public function CheckDayClose($data = array()) {
		//---------------------------------status----------------------------------------------------------------
		$check_transfer = $this->db->query("SELECT * FROM medicine_transfer WHERE entry_date = '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date'] . ' -1 day'))). "' LIMIT 1");
		if($check_transfer->num_rows > 0){
			$check_day_close_table = $this->db->query("SELECT * FROM daily_reconsilation WHERE entry_date = '" .date('Y-m-d', strtotime($check_transfer->row['entry_date'])). "' LIMIT 1");
			if($check_day_close_table->num_rows > 0){
				return 1;
			} else {
				return 'day_close';
			}
		} else {
			$check_treatment = $this->db->query("SELECT * FROM oc_treatment_entry WHERE entry_date = '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date'] . ' -1 day'))). "' LIMIT 1");
			if($check_treatment->num_rows > 0){
				$in = 0;
				$check_day_close_table = $this->db->query("SELECT * FROM daily_reconsilation WHERE entry_date = '" .date('Y-m-d', strtotime($check_treatment->row['entry_date'])). "' LIMIT 1");
				if($check_day_close_table->num_rows > 0){
					return 1;
				} else {
					return 'day_close';
				}
			} else {
				$check_day_close_table = $this->db->query("SELECT * FROM daily_reconsilation WHERE entry_date = '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date'] . ' -1 day'))). "' LIMIT 1");
			
				if($check_day_close_table->num_rows > 0 || date('Y-m-d', strtotime($data['filter_date'])) == date('Y-m-d')){
					return 1;//both not have means safe
				} else {
					$is_first_time = $this->db->query("SELECT * FROM daily_reconsilation WHERE doctor_id = '" .$this->db->escape($data['filter_doctor_id']). "' AND entry_date = '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date'] . ' -1 day'))). "' ");
					if($is_first_time->num_rows == 0){
						return 1;//  first time	
					}
					return 'no_results';
				}

			}
		}
	}

	public function getDoctorAuto($doctor_name) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT doctor_name,doctor_code,id FROM  doctor WHERE 1 = 1");
		if($doctor_name != ''){
			$sql .= " AND `doctor_name` LIKE '%" . $this->db->escape($doctor_name) . "%'";
		}
		
		$sql .= " ORDER BY `doctor_code` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getMedicineTreatmentTranscation($data = array()) {
		$sql = "SELECT *, SUM(medicine_qty) AS medicine_qtys FROM `oc_treatment_entry`m LEFT JOIN oc_treatment_entry_trans mi ON (m.id = mi.parent_id) WHERE 1 = 1";

		if (!empty($data['filter_doctor_id'])) {
			$sql .= " AND (case when `doctor_id` = 0 then clinic_id else doctor_id end)  = '" .$this->db->escape($data['filter_doctor_id']). "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND m.entry_date <= '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date']))). "'";
		}
		$sql .= " GROUP BY mi.medicine_code ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}
}