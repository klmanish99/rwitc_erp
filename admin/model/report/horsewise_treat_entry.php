<?php
class ModelReportHorseWiseTreatEntry extends Model {

	public function getHorses($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_treatment_entry_trans tt LEFT JOIN oc_treatment_entry t ON (tt.parent_id = t.id)";
			
		$sql .= " WHERE 1=1";

		if (!empty($data['filter_horse'])) {
			$sql .= " AND horse_name LIKE '%" . $this->db->escape($data['filter_horse']) . "%'";
		}

		if (!empty($data['filter_trainer'])) {
			$sql .= " AND trainer_name LIKE '%" . $this->db->escape($data['filter_trainer']) . "%'";
		}

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND medicine_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sql .= " ORDER BY entry_date DESC ";

		$sort_data = array(
			'order_no',
		);

		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTrainers($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_treatment_entry_trans tt LEFT JOIN oc_treatment_entry t ON (tt.parent_id = t.id)";
			
		$sql .= " WHERE 1=1";

		if (!empty($data['filter_trainer'])) {
			$sql .= " AND trainer_name LIKE '%" . $this->db->escape($data['filter_trainer']) . "%'";
		}

		$sql .= " ORDER BY entry_date DESC ";

		$sort_data = array(
			'order_no',
		);

		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getHorsesAuto($to_owner) {

		$sql =("SELECT official_name,horseseq FROM horse1 WHERE 1 = 1");
		if($to_owner != ''){
			$sql .= " AND `official_name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		$sql .= " ORDER BY `official_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getTrainersAuto($to_owner) {

		$sql =("SELECT name,trainer_code,id FROM trainers WHERE 1 = 1");
		if($to_owner != ''){
			$sql .= " AND `name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		$sql .= " ORDER BY `name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}
}
