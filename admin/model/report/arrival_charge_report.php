<?php 
class ModelReportarrivalchargereport extends Model {
	public function getUndertakingChareges($data = array()) {
		$horse_sql = "SELECT * FROM `arrival_charges` WHERE 1 = 1";

		if (!empty($data['filter_trainer_id'])) {
			$horse_sql .= " AND trainer_id = '" . (int)$data['filter_trainer_id'] . "'";
		}

		if (!empty($data['filter_hourse_id'])) {
			$horse_sql .= " AND horse_id = '" . (int)$data['filter_hourse_id'] . "'";
		}

		if (!empty($data['filter_date'])) {
			$horse_sql .= " AND DATE(date) >= '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date']))). "'";
		}

		if (!empty($data['filter_date_end'])) {
			$horse_sql .= " AND DATE(date) <= '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date_end']))). "'";
		}
		$result = array();
		$owner_datas = array();
		$horse_query1 = $this->db->query($horse_sql);
		if($horse_query1->num_rows > 0){
			foreach($horse_query1->rows as $mkey => $mvalue){
				$result[$mvalue['id']] =$mvalue;
				$arrival_charge_owners_query = "SELECT * FROM arrival_charge_owners WHERE arrival_charge_id = '".$mvalue['id']."' ";
				if (!empty($data['filter_owner_id'])) {
					$arrival_charge_owners_query .= " AND owner_id = '" . $this->db->escape($data['filter_owner_id']) . "'";
				}
				$arrival_charge_owners_query_datas = $this->db->query($arrival_charge_owners_query);

				
				foreach ($arrival_charge_owners_query_datas->rows as $key => $value) {

				$owner_query = $this->db->query("SELECT * FROM `horse_to_owner` WHERE `owner_share_status` = 1 AND `horse_id` = '".$mvalue['horse_id']."' AND to_owner_id = '".$value['owner_id']."'");
				if($owner_query->num_rows > 0){
					$owner_datas = $owner_query->row;
				} else {
					$owner_datas = array();
				}

					$result[$mvalue['id']]['owner_name'][] = array(
						'arrival_charge_id' => $value['arrival_charge_id'],
						'horse_id' => $value['horse_id'],
						'owner_id' => $value['owner_id'],
						'owner_name' => $value['owner_name'],
						'entry_date' => $value['entry_date'],
						'charge_amt' => $value['charge_amt'],
						'owners_datas' => $owner_datas
					);
				}
				

			}
		}

		//echo "<pre>";print_r($result);exit;
		return $result;
	}

	public function getOwnerAuto($owner) {

		$sql =("SELECT DISTINCT(`owner_id`),owner_name  FROM arrival_charge_owners WHERE 1 = 1");
		if($owner != ''){
			$sql .= " AND `owner_name` LIKE '%" . $this->db->escape($owner) . "%'";
		}
		$sql .= " ORDER BY `owner_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	
}