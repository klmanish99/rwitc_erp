<?php
class ModelReportstockreport extends Model {

	public function getinward($order_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM oc_inward WHERE order_id = '" . (int)$order_id . "'");
		return $query->row;
	}

	public function getinwards($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM medicine ";
			
		$sql .= " WHERE `isActive` = 'Active'";

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND med_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalinwards($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM medicine";

		$sql .= " WHERE `isActive` = 'Active'";

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND med_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}
