<?php 
class ModelReportOwnershipRegistrationReport extends Model {
	public function getUndertakingChareges($data = array()) {
		$final_array = array();
		$result = array();
		$owner_datas = array();

		$trans_datas = "SELECT id, amount, horse_id, date_added FROM `account_ownership_transfer` WHERE 1=1";
		if (!empty($data['filter_hourse_id'])) {
			$trans_datas .= " AND horse_id = '" . (int)$data['filter_hourse_id'] . "'";
		}

		if (!empty($data['filter_date'])) {
			$trans_datas .= " AND DATE(date_added) >= '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date']))). "'";
		}

		if (!empty($data['filter_date_end'])) {
			$trans_datas .= " AND DATE(date_added) <= '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date_end']))). "'";
		}


		$trans_data = $this->db->query($trans_datas)->rows;
		foreach ($trans_data as $tkey => $tvalue) {
			$owners_array = array();
			$onwers_datas = "SELECT * FROM `horse_to_owner_charges` WHERE trans_id = '".$tvalue['id']."'";
			$owners_data = $this->db->query($onwers_datas)->rows;
			foreach ($owners_data as $okey => $ovalue) {
				$horse_to_ownersdatas = $this->db->query("SELECT ownership_type, provisional_ownership FROM `horse_to_owner` WHERE horse_id ='".$tvalue['horse_id']."'  ")->row;

				$owners_array[] = array(
					'owner_name' => $ovalue['owner_name'],
					'ownership_type' => $horse_to_ownersdatas['ownership_type'],
					'provisional_ownership' => $horse_to_ownersdatas['provisional_ownership'],
					'share' => $ovalue['share'],
					'amount' => $ovalue['amount'],

				);
				
			//$final_array[$tkey]['trainer_name']  = '1111';
			}
			$horse_names = $this->db->query("SELECT official_name FROM `horse1` WHERE horseseq = '".$tvalue['horse_id']."' ");
			if($horse_names->num_rows > 0){
				$horse_nam = $horse_names->row['official_name'];
			} else {
				$horse_nam = '';
			}
			$horse_to_ownersdata = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_id ='".$tvalue['horse_id']."'  ")->row;
			// echo'<pre>';
			// print_r($horse_to_ownersdata);
			// exit;

			$tra_id = isset($horse_to_ownersdata['trainer_id']) ? $horse_to_ownersdata['trainer_id'] : '';
			$trainerss_name = $this->db->query("SELECT name FROM `trainers` WHERE trainer_code = '".$tra_id."' ");
			if($trainerss_name->num_rows > 0){
				$tnames = $trainerss_name->row['name'];
			} else {
				$tnames = '';
			}

			
			$final_array[$tkey] = array(
				'trainer_name' => $tnames,
				'horse_name' => $horse_nam,
				'date_of_entry' => date('d-m-Y', strtotime($tvalue['date_added'])),
				'total_amts' => $tvalue['amount'],
				'owners_data' => $owners_array,
			);
			
		}

		// echo'<pre>';
		// print_r($final_array);
		// exit;

		return $final_array;
	}

	public function getOwnerAuto($owner) {

		$sql =("SELECT DISTINCT(`owner_id`),owner_name  FROM arrival_charge_owners WHERE 1 = 1");
		if($owner != ''){
			$sql .= " AND `owner_name` LIKE '%" . $this->db->escape($owner) . "%'";
		}
		$sql .= " ORDER BY `owner_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	
}