<?php
class ModelReportInwardPoReport extends Model {

	public function getinward($order_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM oc_inward WHERE order_id = '" . (int)$order_id . "'");
		return $query->row;
	}

	public function getinwards($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_inward i LEFT JOIN oc_inwarditem ii ON (i.order_no = ii.order_no)";
			
		$sql .= " WHERE 1=1";

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND productraw_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		if (!empty($data['filter_po_no'])) {
			$sql .= " AND po_no LIKE '%" . $this->db->escape($data['filter_po_no']) . "%'";
		}

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sort_data = array(
			'order_no',
		);

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY order_id";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " ASC";
		// } else {
		// 	$sql .= " DESC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getsupplier($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM is_vendor";
			
		$sql .= " WHERE 1=1";
		
		if (!empty($data['filter_supplier'])) {
			$sql .= " AND vendor_name LIKE '%" . $this->db->escape($data['filter_supplier']) . "%'";
		}

		$sort_data = array(
			'order_no',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY vendor_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalinwards($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_inward";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND order_id = '" . $this->db->escape($data['filter_order_id']) . "'";
		}

		/*if (!empty($this->user->getUserType() == '4')) {
			$sql .= " AND 	status = '1' " ;
		}*/

		if (!empty($data['filter_order_no'])) {
			$sql .= " AND order_no LIKE '%" . $this->db->escape($data['filter_order_no']) . "%'";
		}

		if (!empty($data['filter_productfinished_category'])) {
			$sql .= " AND productfinished_category LIKE '%" . $this->db->escape($data['filter_productfinished_category']) . "%'";
		}

		if (!empty($data['filter_productfinished_category_id'])) {
			$sql .= " AND productfinished_category_id = '" . $this->db->escape($data['filter_productfinished_category_id']) . "'";
		}

		if (!empty($data['filter_productsort'])) {
			$sql .= " AND UCASE(productfinished_name) LIKE '" . $this->db->escape($data['filter_productsort']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}


	public function daterawmaterial() {
		$query = $this->db->query("SELECT * FROM oc_inward WHERE 1=1")->rows;
		return $query;	
	}

	public function getcatname($category_id) {
		$query = $this->db->query("SELECT `name` FROM `" . DB_PREFIX . "category` WHERE `category_id` = '".$category_id."' ");
		if(isset($query->row['name'])){
			return $query->row['name'];
		} else {
			return false;
		}
	}

	public function getproducthistory_data($order_id) {
		$query = $this->db->query("SELECT * FROM `is_product_history` WHERE `order_id` = '".$order_id."' ORDER BY `id` DESC");
		return $query->rows;
	}

	public function getproductvendor_datas($order_id) {
		$query = $this->db->query("SELECT * FROM `is_vendor_materials` WHERE `order_id` = '".$order_id."' ");
		return $query->rows;
	}

	public function makeProduct($finished_product_id, $data) {
		
		$sql_finished = "INSERT INTO `is_manufacturing_register_finished` SET `finished_product_id` = '".$finished_product_id."', `finished_product_name` = '".$data['productraw_name']."', `quantity` = '".$data['quantity_all']."', `user_id` = '".$this->user->getId()."', `date` = now()";
		$query_finished = $this->db->query($sql_finished);
		$order_id = $this->db->getLastId();

		$add_to_finished = "UPDATE `oc_inward` SET `quantity` = (`quantity` + " . (int)$data['quantity_all'] . ") WHERE `order_id` = '".$finished_product_id."'";
		$this->db->query($add_to_finished);

		foreach($data['productraw_datas'] as $result) {
			$sql_raw = "INSERT INTO `is_manufacturing_register_raw` SET `id` = '".$order_id."', `product_id` = '".$result['product_id']."', `product_name` = '".$result['productraw_name']."', `quantity` = '".$result['quantity']."'";
			$query_raw = $this->db->query($sql_raw);


/*			$remove_from_raw = "UPDATE `is_productnew` SET `quantity` = (`quantity` - " . (int)$result['quantity'] . ") WHERE `productnew_id` = '".$result['product_id']."'";
			$this->db->query($remove_from_raw);
*/
			$remove_from_raw = "UPDATE `is_raw_material` SET `quantity` = (`quantity` - " . (int)$result['quantity'] . ") WHERE `raw_material_id` = '".$result['product_id']."'";
			$this->db->query($remove_from_raw);
		}
	}

	public function getProductnews($data = array()) {
		/*echo'<pre>';
		print_r($data);
		exit;*/
		$sql = "SELECT * FROM medicine";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_name'])) {
			$sql .= " AND med_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['med_code'])) {
			$sql .= " AND med_code LIKE '%" . $this->db->escape($data['med_code']) . "%'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo'<pre>';print_r($sql);exit;

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getProductcode($data = array()) {
		/*echo'<pre>';
		print_r($data);
		exit;*/
		$sql = "SELECT * FROM medicine";

		$sql .= " WHERE 1=1";

		if (!empty($data['med_code'])) {
			$sql .= " AND med_code = '" . $this->db->escape($data['med_code']) . "'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo'<pre>';print_r($sql);exit;

		$query = $this->db->query($sql);
		return $query->rows;
	}
}
