<?php
class Modelreportmedicinereport extends Model {

	// public function getinward($order_id) {
	// 	$query = $this->db->query("SELECT DISTINCT * FROM oc_inward WHERE order_id = '" . (int)$order_id . "'");
	// 	return $query->row;
	// }

	public function getindents($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_inward i LEFT JOIN oc_inwarditem ii ON (i.order_no = ii.order_no)";
			
		$sql .= " WHERE 1=1";

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND productraw_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		if (!empty($data['filter_po_no'])) {
			$sql .= " AND po_no LIKE '%" . $this->db->escape($data['filter_po_no']) . "%'";
		}

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND i.date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND i.date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sort_data = array(
			'order_no',
		);

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY order_id";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " ASC";
		// } else {
		// 	$sql .= " DESC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function gettransfer($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM medicine_transfer m LEFT JOIN medicine_trans_items mi ON (m.id = mi.parent_id)";
			
		$sql .= " WHERE 1=1";

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND mi.product_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		// if (!empty($data['filter_doctor'])) {
		// 	$sql .= " AND child_doctor_id LIKE '%" . $this->db->escape($data['filter_doctor']) . "%'";
		// }

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND m.entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND m.entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sort_data = array(
			'order_no',
		);

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY order_id";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " ASC";
		// } else {
		// 	$sql .= " DESC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotaltransfer($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM medicine_transfer m LEFT JOIN medicine_trans_items mi ON (m.id = mi.parent_id)";

		$sql .= " WHERE 1=1";

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sort_data = array(
			'order_no',
		);

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function gettreatment($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_treatment_entry t LEFT JOIN oc_treatment_entry_trans tt ON (t.id = tt.parent_id)";
		
		$sql .= " WHERE 1=1";

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND medicine_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		// if (!empty($data['filter_doctor'])) {
		// 	$sql .= " AND child_doctor_id LIKE '%" . $this->db->escape($data['filter_doctor']) . "%'";
		// }

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		// $sort_data = array(
		// 	'order_no',
		// );

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY order_id";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " ASC";
		// } else {
		// 	$sql .= " DESC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotaltreatment($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_treatment_entry";

		$sql .= " WHERE 1=1";

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sort_data = array(
			'order_no',
		);

		$query = $this->db->query($sql);

		return $query->row['total'];
	}



	public function getTotalindents($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_inward i LEFT JOIN oc_inwarditem ii ON (i.order_no = ii.order_no)";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND productraw_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		if (!empty($data['filter_po_no'])) {
			$sql .= " AND po_no LIKE '%" . $this->db->escape($data['filter_po_no']) . "%'";
		}

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND i.date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND i.date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sort_data = array(
			'order_no',
		);

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
}
