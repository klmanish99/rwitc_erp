<?php 
class ModelReportmonthlymedicinetransaction extends Model {
	public function getMedicineTranscation($data = array()) {
		//$sql = "SELECT *, SUM(product_qty) AS total_trans FROM `medicine_transfer`m LEFT JOIN medicine_trans_items mi ON(m.id = mi.parent_id) WHERE 1 = 1";

		/*if (!empty($data['filter_date'])) {
			$sql .= " AND entry_date = '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date']))). "'";
		}*/

		$sql = "SELECT * FROM `medicine_transfer` WHERE 1 = 1";

		if (!empty($data['filter_months'])) {
			$sql .= " AND MONTH(`entry_date`) = '" .$data['filter_months']. "'";
		}
		if (!empty($data['filter_year'])) {
			$sql .= " AND YEAR(`entry_date`) = '" .$data['filter_year']. "'";
		}

		$sql .= " GROUP BY entry_date ";

		$query = $this->db->query($sql);

		$final_array = array();
		if($query->num_rows > 0){
			foreach ($query->rows as $key => $value) {
				$medicine_product = $this->db->query("SELECT *, SUM(product_qty) AS total_trans FROM `medicine_trans_items` WHERE `entry_date` = '".$value['entry_date']."' GROUP BY product_id, entry_date");

				if($medicine_product->num_rows > 0){

						
					foreach ($medicine_product->rows as $pkey => $palue) {
						$medicine_product = $this->db->query("SELECT store_unit FROM `medicine` WHERE `med_code` = '".$palue['product_id']."' ");
						if($medicine_product->num_rows > 0){
							$medicine_product_code = $medicine_product->row['store_unit'] ;
						} else {
							$medicine_product_code = '' ;
						}
						$final_array[] = array(
							'entry_date'=> $palue['entry_date'],
							'total_trans'=> $palue['total_trans'],
							'product_name'=> $palue['product_name'],
							'product_id'=> $palue['product_id'],
							'store_unit'=> $medicine_product_code,
						);
					}
				}

			}
		}
		//echo "<pre>";print_r($final_array);exit;
		
		return $final_array;
	}

	public function addMedData($data) {
		
		
		$entry_date = $data['filter_year'].'-'.$data['filter_months'].'-'.date('d');
		

		foreach ($data['productraw_datas'] as $key => $value) {
			if($value['actual_qty']){
				$this->db->query("INSERT INTO `daily_reconsilation` SET 
				`entry_date` = '".$entry_date."',
				`medicine_name` = '".$this->db->escape($value['product_name'])."',
				`medicine_code` = '".$this->db->escape($value['product_id'])."',
				`store_unit` = '".$this->db->escape($value['store_unit'])."',
				`actual_qty` = '".$value['actual_qty']."' ");
			}
			
		}
		
	}
}