<?php
class ModelReportIndentPoReport extends Model {

	// public function getinward($order_id) {
	// 	$query = $this->db->query("SELECT DISTINCT * FROM oc_inward WHERE order_id = '" . (int)$order_id . "'");
	// 	return $query->row;
	// }

	public function getindents($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_rawmaterialreq i LEFT JOIN oc_rawmaterialreqitem ii ON (i.order_no = ii.order_no)";
			
		$sql .= " WHERE 1=1";

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND productraw_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		if (!empty($data['filter_po_no'])) {
			$sql .= " AND po_no LIKE '%" . $this->db->escape($data['filter_po_no']) . "%'";
		}

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND i.date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND i.date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sort_data = array(
			'order_no',
		);

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY order_id";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " ASC";
		// } else {
		// 	$sql .= " DESC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalindents($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_rawmaterialreq i LEFT JOIN oc_rawmaterialreqitem ii ON (i.order_no = ii.order_no)";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND productraw_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		if (!empty($data['filter_po_no'])) {
			$sql .= " AND po_no LIKE '%" . $this->db->escape($data['filter_po_no']) . "%'";
		}

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND i.date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND i.date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sort_data = array(
			'order_no',
		);

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
}
