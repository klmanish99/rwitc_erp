<?php
class ModelReportMedTransferReport extends Model {

	public function getmedtransfer($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM medicine_transfer m LEFT JOIN medicine_trans_items mi ON (m.id = mi.parent_id)";
			
		$sql .= " WHERE 1=1";

		if (!empty($data['filter_medicine'])) {
			$sql .= " AND mi.product_name LIKE '%" . $this->db->escape($data['filter_medicine']) . "%'";
		}

		if (!empty($data['filter_doctor'])) {
			$sql .= " AND m.child_doctor_id = '" . $this->db->escape($data['filter_doctor']) . "'";
		}

		if (!empty($data['filter_hospital'])) {
			$sql .= " AND m.parent_doctor_name LIKE '%" . $this->db->escape($data['filter_hospital']) . "%'";
		}

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND m.entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND m.entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$sort_data = array(
			'order_no',
		);

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY order_id";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " ASC";
		// } else {
		// 	$sql .= " DESC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalmedtransfer($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM medicine_transfer";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_doctor'])) {
			$sql .= " AND parent_doctor_name LIKE '%" . $this->db->escape($data['filter_doctor']) . "%'";
		}

		if (!empty($data['filter_hospital'])) {
			$sql .= " AND parent_doctor_name LIKE '%" . $this->db->escape($data['filter_hospital']) . "%'";
		}

		if ($data['filter_date'] != '' && $data['filter_dates'] != '') {
			$sql .= " AND entry_date >= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "'";
			$sql .= " AND entry_date <= '" . $this->db->escape(date('Y-m-d', strtotime($data['filter_dates']))) . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}
