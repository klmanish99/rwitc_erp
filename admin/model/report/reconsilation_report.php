<?php 
class ModelReportReconsilationReport extends Model {
	public function getReconcilation($data = array()) {
		$sql = "SELECT *, SUM(actual_qty) AS total_trans FROM `daily_reconsilation` WHERE 1 = 1";

		/*if (!empty($data['filter_doctor_id'])) {
			$sql .= " AND (case when m.child_doctor_id = 0 then m.parent_doctor_id else m.child_doctor_id end)  = '" .$this->db->escape($data['filter_doctor_id']). "'";
		}*/

		if (!empty($data['filter_date'])) {
			$sql .= " AND entry_date = '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date']))). "'";
		}
		$sql .= " GROUP BY medicine_code ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
		//echo $sql;exit;
/*
		$sql = "SELECT * FROM `medicine_transfer` WHERE 1 = 1";

		if (!empty($data['filter_date'])) {
			$sql .= " AND entry_date = '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date']))). "'";
		}

		if (!empty($data['filter_doctor_id'])) {
			$sql .= " AND parent_doctor_id = '" .$this->db->escape($data['filter_doctor_id']). "'";
		}
		$sql .= " GROUP BY entry_date ";

		//echo $sql;exit;

		$query = $this->db->query($sql);

		$final_array = array();
		if($query->num_rows > 0){
			foreach ($query->rows as $key => $value) {
				$medicine_product = $this->db->query("SELECT *, SUM(product_qty) AS total_trans FROM `medicine_trans_items` WHERE `entry_date` = '".$value['entry_date']."' GROUP BY product_id, entry_date");

				if($medicine_product->num_rows > 0){

						
					foreach ($medicine_product->rows as $pkey => $palue) {
						$medicine_product = $this->db->query("SELECT store_unit FROM `medicine` WHERE `med_code` = '".$palue['product_id']."' ");
						if($medicine_product->num_rows > 0){
							$medicine_product_code = $medicine_product->row['store_unit'] ;
						} else {
							$medicine_product_code = '' ;
						}
						$final_array[] = array(
							'entry_date'=> $palue['entry_date'],
							'total_trans'=> $palue['total_trans'],
							'product_name'=> $palue['product_name'],
							'product_id'=> $palue['product_id'],
							'store_unit'=> $medicine_product_code,
						);
					}
				}

			}
		}*/
		//echo "<pre>";print_r($final_array);exit;
		
		//return $final_array;
	}

	public function addMedData($data) {//echo "<pre>";print_r($data);exit;
		
		

		foreach ($data['productraw_datas'] as $key => $value) {
			if($value['actual_qty']){
				$this->db->query("INSERT INTO `daily_reconsilation` SET 
				`entry_date` = '".date('Y-m-d', strtotime($data['filter_date']))."',
				`medicine_name` = '".$this->db->escape($value['product_name'])."',
				`medicine_code` = '".$this->db->escape($value['product_id'])."',
				`store_unit` = '".$this->db->escape($value['store_unit'])."',
				`actual_qty` = '".$value['actual_qty']."' ,
				`doctor_name` = '".$this->db->escape($data['filter_doctor_name'])."',
				`doctor_id` = '".$data['filter_doctor_id']."' 
				");

			}
			
		}
		
	}

	public function getDoctorAuto($doctor_name) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT doctor_name,doctor_code,id FROM  doctor WHERE 1 = 1");
		if($doctor_name != ''){
			$sql .= " AND `doctor_name` LIKE '%" . $this->db->escape($doctor_name) . "%'";
		}
		
		$sql .= " ORDER BY `doctor_code` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getTotalReconcilation($data = array()) { 
		$sql = ("SELECT COUNT(DISTINCT medicine_code) AS total FROM daily_reconsilation WHERE 1 = 1");

		if (!empty($data['filter_date'])) {
			$sql .= " AND entry_date = '" .$this->db->escape(date('Y-m-d', strtotime($data['filter_date']))). "'";
		}
		//$sql .= " GROUP BY medicine_code ";
		//echo $sql;

		$query = $this->db->query($sql);

		if($query->num_rows > 0){
			return $query->row['total'];
		} else {
			return 0;
		}

		
	}
}