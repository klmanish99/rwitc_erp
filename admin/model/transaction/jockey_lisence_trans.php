<?php
class ModelTransactionJockeyLisenceTrans extends Model {
	public function addJockeyTrans($data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$last_update_date = $date->format('Y-m-d h:i:s');
		$season_start = date('Y-m-d', strtotime($data['season_start']));
		$season_end = date('Y-m-d', strtotime($data['season_end']));
		foreach ($data['jockey_datas'] as $jkey => $jvalue) {
			if(isset($jvalue['selected_jockey'])){
				$apprenties = isset($jvalue['apprenties']) ? "Yes" : "No";
				$this->db->query("INSERT INTO `jockey_renewal_history` SET 
					trainer_id = '".$jvalue['jockey_id']."',
					jockey_name = '".$jvalue['jockey_name']."',
					amount = '".$jvalue['amt']."',
					age = '".$jvalue['age']."',
					renewal_date = '". date('Y-m-d', strtotime($jvalue['lisence_end_date'])) ."',
					license_start_date = '". date('Y-m-d', strtotime($jvalue['lisence_start_date'])) ."',
					license_end_date = '". date('Y-m-d', strtotime($jvalue['lisence_end_date'])) ."',
					license_type ='". $jvalue['lisence_type'] ."',
					apprenties = '". $apprenties ."',
					fees = '". $jvalue['fees_type'] ."',
					season_start = '".$season_start."',
					season_end = '".$season_end."',
					entry_status = '1',
					`user_id` = '".$this->session->data['user_id']."',
					`last_update_date` = '".$last_update_date."'
				");
				$this->db->query("UPDATE jockey SET
				`license_type` = '". $jvalue['lisence_type'] ."' WHERE jockey_id = '" . $jvalue['jockey_id'] . "'
		    	");
			}
		}
	}
	public function getjockeyDatas($data = array()) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$sql = "SELECT * FROM jockey";

		$sql .= " WHERE `jocky_status` = 1";

		if (!empty($data['filter_jockey'])) {
			$sql .= " AND jockey_name LIKE '%" . $this->db->escape($data['filter_jockey']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
}
