<?php
class Modeltransactionclassentry extends Model {
	public function addDoctors($data) {
		$this->db->query("INSERT INTO class_entry SEt 
		distance = '" . $this->db->escape($data['distance']) . "',
		class ='".$this->db->escape($data['class'])."',
		entry ='".$this->db->escape($data['entry'])."',
		acceptance ='".$this->db->escape($data['acceptance'])."',
		division ='".$this->db->escape($data['division'])."',
		two_nov_dec_entry ='".$this->db->escape($data['two_nov_dec_entry'])."',
		two_nov_dec_acceptance ='".$this->db->escape($data['two_nov_dec_acceptance'])."',
		sweepstake ='".$this->db->escape($data['sweepstake'])."'
		");
		$class_entry_id = $this->db->getLastId();
		return $class_entry_id;
	}


	public function editDoctors($class_entry_id,$data) {
		$this->db->query("UPDATE class_entry SEt 
		distance = '" . $this->db->escape($data['distance']) . "',
		class ='".$this->db->escape($data['class'])."',
		entry ='".$this->db->escape($data['entry'])."',
		acceptance ='".$this->db->escape($data['acceptance'])."',
		division ='".$this->db->escape($data['division'])."',
		two_nov_dec_entry ='".$this->db->escape($data['two_nov_dec_entry'])."',
		two_nov_dec_acceptance ='".$this->db->escape($data['two_nov_dec_acceptance'])."',
		sweepstake ='".$this->db->escape($data['sweepstake'])."'
		WHERE class_entry_id ='".$class_entry_id."'
		");
	}

	

	public function deleteDoctors($class_entry_id) {
		$this->db->query("DELETE FROM class_entry WHERE class_entry_id = '" . (int)$class_entry_id . "'");
	}

	public function getDoctor($data = array()) {
		$sql = "SELECT *  FROM  class_entry  WHERE 1=1 ";

		// if (!empty($data['filter_doctor_name'])) {
		// 	$sql .= " AND doctor_name LIKE '" . $this->db->escape($data['filter_doctor_name']) . "%'";
		// }

		// if (!empty($data['filter_doctor_code'])) {
		// 	$sql .= " AND doctor_code = '" . $this->db->escape($data['filter_doctor_code']) . "' ";
		// }

		// if (isset($data['filter_status'])) {
		// 	$sql .= " AND isActive = '" . $this->db->escape($data['filter_status']) . "' ";
		// }

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		/*echo'<pre>';
		print_r($sql);
		exit;*/
		$query = $this->db->query($sql)->rows;
		return $query;
	
	}

	public function getDoctors($class_entry_id) {  
		$sql = "SELECT *  FROM  class_entry  WHERE class_entry_id='".$class_entry_id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}

	public function getTotalDoctors() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM class_entry");

		return $query->row['total'];
	}

	public function getDoctorAuto($doctor_name) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT doctor_name,doctor_code,id FROM  doctor WHERE 1 = 1");
		if($doctor_name != ''){
			$sql .= " AND `doctor_name` LIKE '%" . $this->db->escape($doctor_name) . "%'";
		}
		
		$sql .= " ORDER BY `doctor_code` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getDoctorAutos($doctor_code) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT doctor_name,doctor_code,id FROM  doctor WHERE 1 = 1");
		if($doctor_code != ''){
			$sql .= " AND `doctor_code` LIKE '%" . $this->db->escape($doctor_code) . "%'";
		}
		
		$sql .= " ORDER BY `doctor_code` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}
}
