<?php
class ModelCatalogamounttrans extends Model {
	public function addHorse($data) {
		/*echo'<pre>';
		print_r($data);
		exit;*/
		$change_horse_dates = date('Y-m-d', strtotime($data['change_horse_dates']));
		$registeration_dates = date('Y-m-d', strtotime($data['registeration_date']));
		$std_stall_certificate_date = date('Y-m-d', strtotime($data['date_std_stall_certificate']));
		$charge_trainer_date = date('Y-m-d', strtotime($data['date_charge_trainer']));
		//$left_trainer_date = date('Y-m-d', strtotime($data['date_left_trainer']));
		$foal_date = date('Y-m-d', strtotime($data['date_foal_date']));

		if(!isset($data['undertaking_charge'])){
			$undertaking_charge = 0;
		} else {
			$undertaking_charge = 1;
		}

		if($data['date_left_trainer'] != ''){
			$left_trainer_date = date('Y-m-d', strtotime($data['date_left_trainer']));
		} else {
			$left_trainer_date = '0000-00-00';
		}

		if($data['castration_date'] != '' && $data['sex'] == 'g'){
			$castration_date = date('Y-m-d', strtotime($data['castration_date']));
		} else {
			$castration_date = '0000-00-00';
		}

		$code = $this->db->escape($data['hidden_horse_code']);
		$Hcode =substr($code,1);

		$this->db->query("INSERT INTO `horse1` SET official_name = '".$data['horse_name']."',changehorse_name='".$data['changehorse_name']."' ,date_of_changehorse_name = '".$change_horse_dates."',official_name_change_status='".$data['official_name_change']."' , life_no = '".$data['passport_no']."',reg_date = '".$registeration_dates."',reg_auth_id = '".$data['registerarton_authentication']."',sire_name = '".$data['sire']."', dam_name = '".$data['dam']."',chip_no1 = '".$data['chip_no_one']."',chip_no2 = '".$data['chip_no_two']."',arrival_charges_to_be_paid = '".$data['arrival_charges_to_be_paid']."' ,foal_date = '".$foal_date ."',age = '".$data['age']."' , color  = '".$data['color']."'  , orgin  = '".$data['origin']."', sex = '".$data['sex']."' ,castration_date = '".$castration_date."' ,stud_form = '".$data['stud_form']."',breeder_name = '".$data['breeder']."',rating = '".$data['rating']."',saddle_no = '".$data['saddle_no']."' ,octroi_details = '".$data['octroi_details']."',awbi_registration_no = '".$data['awbi_registration_no']."',std_stall_certificate_date = '".$std_stall_certificate_date."',Id_by_brand = '".$data['Id_by_brand']."',std_stall_remarks = '".$data['std_stall_remarks']."',horse_remarks = '".$data['horse_remarks']."', awbi_registration_file = '".$data['awbi_registration_file']."' ,awbi_registration_file_source = '".$data['awbi_registration_file_source']."' ,horse_status='".$this->db->escape($data['isActive'])."' ,horse_code = '".$Hcode."'");
		$this->log->write("INSERT INTO `horse1` SET official_name = '".$data['horse_name']."',changehorse_name='".$data['changehorse_name']."' ,date_of_changehorse_name = '".$change_horse_dates."',official_name_change_status='".$data['official_name_change']."' , life_no = '".$data['passport_no']."',reg_date = '".$registeration_dates."',reg_auth_id = '".$data['registerarton_authentication']."',sire_name = '".$data['sire']."', dam_name = '".$data['dam']."',chip_no1 = '".$data['chip_no_one']."',chip_no2 = '".$data['chip_no_two']."',arrival_charges_to_be_paid = '".$data['arrival_charges_to_be_paid']."' ,foal_date = '".$foal_date ."',age = '".$data['age']."' , color  = '".$data['color']."'  , orgin  = '".$data['origin']."',  sex = '".$data['sex']."' ,castration_date = '".$castration_date."' ,stud_form = '".$data['stud_form']."',breeder_name = '".$data['breeder']."',saddle_no = '".$data['saddle_no']."' ,octroi_details = '".$data['octroi_details']."',awbi_registration_no = '".$data['awbi_registration_no']."',std_stall_certificate_date = '".$std_stall_certificate_date."',Id_by_brand = '".$data['Id_by_brand']."',std_stall_remarks = '".$data['std_stall_remarks']."',horse_remarks = '".$data['horse_remarks']."' , awbi_registration_file = '".$data['awbi_registration_file']."' ,awbi_registration_file_source = '".$data['awbi_registration_file_source']."' ,horse_status='".$this->db->escape($data['isActive'])."' ,horse_code = '".$Hcode."'");
		$horse_id = $this->db->getLastId();

		$this->db->query("INSERT INTO `horse_to_trainer` SET horse_id = '".$horse_id."', trainer_name = '".$this->db->escape($data['trainer_name'])."',trainer_id = '".$this->db->escape($data['trainer_id'])."',trainer_code = '".$this->db->escape($data['trainer_codes_id'])."',date_of_charge = '".$charge_trainer_date."', left_date_of_charge = '".$left_trainer_date."', arrival_time = '".$data['arrival_time_trainers']."',extra_narration = '".$this->db->escape($data['extra_narrration_trainer'] )."',extra_narration_two = '".$this->db->escape($data['extra_narrration_two_trainer'] )."',undertaking_charge = '".$undertaking_charge."'  ");
		$this->log->write("INSERT INTO `horse_to_trainer` SET horse_id = '".$horse_id."', trainer_name = '".$this->db->escape($data['trainer_name'])."',trainer_id = '".$this->db->escape($data['trainer_id'])."',trainer_code = '".$this->db->escape($data['trainer_codes_id'])."',date_of_charge = '".$charge_trainer_date."', left_date_of_charge = '".$left_trainer_date."', arrival_time = '".$data['arrival_time_trainers']."',extra_narration = '".$this->db->escape($data['extra_narrration_trainer'] )."',extra_narration_two = '".$this->db->escape($data['extra_narrration_two_trainer'] )."' ,undertaking_charge = '".$undertaking_charge."'");

		/*$owner_exist_sql = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_id = '" .(int)$horse_id . "'");
		if($owner_exist_sql->num_rows > 0){ 
			$this->db->query("DELETE FROM `horse_to_owner` WHERE horse_id = '" .(int)$horse_id . "'");
			$this->log->write("DELETE FROM `horse_to_owner` WHERE horse_id = '" .(int)$horse_id . "'");
		}

		if(isset($data['ownerdatas'])){
			foreach ($data['ownerdatas'] as $okey => $ovalue) {
				$ownership_from_date = date("Y-m-d", strtotime($ovalue['date_of_ownership']));
				if($ovalue['end_date_of_ownership'] != ''){
					$ownership_date_end = date("Y-m-d", strtotime($ovalue['end_date_of_ownership']));
				} else {
					$ownership_date_end = '0000-00-00';
				}
				$this->db->query("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$horse_id."', from_owner = '".$this->db->escape($ovalue['from_owner'])."',from_owner_id = '".$ovalue['from_owner_id']."', to_owner = '".$this->db->escape($ovalue['to_owner'])."',to_owner_id = '".$ovalue['to_owner_id']."',owner_percentage ='".$ovalue['owner_percentage']."',ownership_type ='".$ovalue['ownership_type']."' ,date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."',remark_horse_to_owner = '".$this->db->escape($ovalue['remark_horse_to_owner'])."' , owner_color = '".$this->db->escape($ovalue['owner_color'])."'  ");
				$this->log->write("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$horse_id."', from_owner = '".$this->db->escape($ovalue['from_owner'])."',from_owner_id = '".$ovalue['from_owner_id']."', to_owner = '".$this->db->escape($ovalue['to_owner'])."',to_owner_id = '".$ovalue['to_owner_id']."',owner_percentage ='".$ovalue['owner_percentage']."',ownership_type ='".$ovalue['ownership_type']."' ,date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."',remark_horse_to_owner = '".$this->db->escape($ovalue['remark_horse_to_owner'])."' , owner_color = '".$this->db->escape($ovalue['owner_color'])."'  ");
				
			}
		}*/
	}

	public function editCategory($horse_id, $data) {
		/*echo'<pre>';
		print_r($data);
		exit;*/
		if(!isset($data['undertaking_charge'])){
			$undertaking_charge = 0;
		} else {
			$undertaking_charge = 1;
		}
		$this->db->query("UPDATE `horse1` SET changehorse_name = official_name WHERE horseseq = '".$horse_id."' ");
		$change_horse_dates = date('Y-m-d', strtotime($data['change_horse_dates']));
		$registeration_dates = date('Y-m-d', strtotime($data['registeration_date']));
		$std_stall_certificate_date = date('Y-m-d', strtotime($data['date_std_stall_certificate']));
		$charge_trainer_date = date('Y-m-d', strtotime($data['date_charge_trainer']));
		//$left_trainer_date = date('Y-m-d', strtotime($data['date_left_trainer']));
		if($data['date_left_trainer'] != ''){
			$left_trainer_date = date('Y-m-d', strtotime($data['date_left_trainer']));
		} else {
			$left_trainer_date = '0000-00-00';
		}

		if($data['castration_date'] != '' && $data['sex'] == 'g'){
			$castration_date = date('Y-m-d', strtotime($data['castration_date']));
		} else {
			$castration_date = '0000-00-00';
		}

		$code = $this->db->escape($data['hidden_horse_code']);
		$Hcode =substr($code,1);

		$foal_date = date('Y-m-d', strtotime($data['date_foal_date']));
		$this->db->query("UPDATE horse1 SET official_name = '".$data['horse_name']."',date_of_changehorse_name = '".$change_horse_dates."',official_name_change_status='".$data['official_name_change']."' , life_no = '".$data['passport_no']."',reg_date = '".$registeration_dates."',reg_auth_id = '".$data['registerarton_authentication']."',sire_name = '".$data['sire']."', dam_name = '".$data['dam']."',chip_no1 = '".$data['chip_no_one']."',chip_no2 = '".$data['chip_no_two']."',arrival_charges_to_be_paid = '".$data['arrival_charges_to_be_paid']."' ,foal_date = '".$foal_date ."',age = '".$data['age']."' , color  = '".$data['color']."'  , orgin  = '".$data['origin']."', sex = '".$data['sex']."' ,castration_date = '".$castration_date."' ,stud_form = '".$data['stud_form']."',breeder_name = '".$data['breeder']."',rating = '".$data['rating']."',saddle_no = '".$data['saddle_no']."' ,octroi_details = '".$data['octroi_details']."',awbi_registration_no = '".$data['awbi_registration_no']."',std_stall_certificate_date = '".$std_stall_certificate_date."',Id_by_brand = '".$data['Id_by_brand']."',std_stall_remarks = '".$data['std_stall_remarks']."',horse_remarks = '".$data['horse_remarks']."' , awbi_registration_file = '".$data['awbi_registration_file']."' ,awbi_registration_file_source = '".$data['awbi_registration_file_source']."' ,horse_status='".$this->db->escape($data['isActive'])."',horse_code = '".$Hcode."'WHERE horseseq = '" . (int)$horse_id . "'");
		$this->log->write("UPDATE horse1 SET official_name = '".$data['horse_name']."',changehorse_name='".$data['changehorse_name']."' ,date_of_changehorse_name = '".$change_horse_dates."',official_name_change_status='".$data['official_name_change']."' , life_no = '".$data['passport_no']."',reg_date = '".$registeration_dates."',reg_auth_id = '".$data['registerarton_authentication']."',sire_name = '".$data['sire']."', dam_name = '".$data['dam']."',chip_no1 = '".$data['chip_no_one']."',chip_no2 = '".$data['chip_no_two']."',arrival_charges_to_be_paid = '".$data['arrival_charges_to_be_paid']."' ,foal_date = '".$foal_date ."',age = '".$data['age']."' , color  = '".$data['color']."'  , orgin  = '".$data['origin']."', sex = '".$data['sex']."' ,castration_date = '".$castration_date."' ,stud_form = '".$data['stud_form']."',breeder_name = '".$data['breeder']."',saddle_no = '".$data['saddle_no']."' ,octroi_details = '".$data['octroi_details']."',awbi_registration_no = '".$data['awbi_registration_no']."',std_stall_certificate_date = '".$std_stall_certificate_date."',Id_by_brand = '".$data['Id_by_brand']."',std_stall_remarks = '".$data['std_stall_remarks']."',horse_remarks = '".$data['horse_remarks']."' , awbi_registration_file = '".$data['awbi_registration_file']."' ,awbi_registration_file_source = '".$data['awbi_registration_file_source']."' ,horse_status='".$this->db->escape($data['isActive'])."',horse_code = '".$Hcode."' WHERE horseseq = '" . (int)$horse_id . "'");

		
		$trainer_sql = $this->db->query("SELECT * FROM `horse_to_trainer` WHERE horse_id = '" .(int)$horse_id . "' AND date_of_charge = '".$charge_trainer_date."' ");
		if($trainer_sql->num_rows == 0){
		$this->db->query("INSERT INTO `horse_to_trainer` SET horse_id = '".$horse_id."', trainer_name = '".$this->db->escape($data['trainer_name'])."',trainer_id = '".$this->db->escape($data['trainer_id'])."',trainer_code = '".$this->db->escape($data['trainer_codes_id'])."',date_of_charge = '".$charge_trainer_date."', left_date_of_charge = '".$left_trainer_date."', arrival_time = '".$data['arrival_time_trainers']."',extra_narration = '".$this->db->escape($data['extra_narrration_trainer'] )."',extra_narration_two = '".$this->db->escape($data['extra_narrration_two_trainer'] )."' ,undertaking_charge = '".$undertaking_charge."' "); 
		$this->log->write("INSERT INTO `horse_to_trainer` SET horse_id = '".$horse_id."', trainer_name = '".$this->db->escape($data['trainer_name'])."',trainer_id = '".$this->db->escape($data['trainer_id'])."',trainer_code = '".$this->db->escape($data['trainer_codes_id'])."',date_of_charge = '".$charge_trainer_date."', left_date_of_charge = '".$left_trainer_date."', arrival_time = '".$data['arrival_time_trainers']."',extra_narration = '".$this->db->escape($data['extra_narrration_trainer'] )."',extra_narration_two = '".$this->db->escape($data['extra_narrration_two_trainer'] )."',undertaking_charge = '".$undertaking_charge."' ");

		}else{
		$this->db->query("UPDATE `horse_to_trainer` SET  trainer_name = '".$this->db->escape($data['trainer_name'])."',trainer_code = '".$this->db->escape($data['trainer_codes_id'])."',date_of_charge = '".$charge_trainer_date."', left_date_of_charge = '".$left_trainer_date."', arrival_time = '".$data['arrival_time_trainers']."',extra_narration = '".$this->db->escape($data['extra_narrration_trainer'] )."',extra_narration_two = '".$this->db->escape($data['extra_narrration_two_trainer'] )."',undertaking_charge = '".$undertaking_charge."' WHERE horse_id = '" . (int)$horse_id . "'  AND horse_to_trainer_id = '".$this->db->escape($data['horse_to_trainer_id'])."' "); 
		$this->log->write("UPDATE `horse_to_trainer` SET trainer_name = '".$this->db->escape($data['trainer_name'])."',trainer_id = '".$this->db->escape($data['trainer_id'])."',trainer_code = '".$this->db->escape($data['trainer_codes_id'])."',date_of_charge = '".$charge_trainer_date."', left_date_of_charge = '".$left_trainer_date."', arrival_time = '".$data['arrival_time_trainers']."',extra_narration = '".$this->db->escape($data['extra_narrration_trainer'] )."',extra_narration_two = '".$this->db->escape($data['extra_narrration_two_trainer'] )."',undertaking_charge = '".$undertaking_charge."' WHERE horse_id = '" . (int)$horse_id . "'  AND horse_to_trainer_id = '".$this->db->escape($data['horse_to_trainer_id'])."' ");

		}
		

		/*$owner_exist_sql = $this->db->query("SELECT * FROM `horse_to_owner` WHERE horse_id = '" .(int)$horse_id . "'");
		if($owner_exist_sql->num_rows > 0){ 
			$this->db->query("DELETE FROM `horse_to_owner` WHERE horse_id = '" .(int)$horse_id . "'");
			$this->log->write("DELETE FROM `horse_to_owner` WHERE horse_id = '" .(int)$horse_id . "'");
		}
		if(isset($data['ownerdatas'])){
			foreach ($data['ownerdatas'] as $okey => $ovalue) {
				$ownership_from_date = date("Y-m-d", strtotime($ovalue['date_of_ownership']));
				if($ovalue['end_date_of_ownership'] != ''){
					$ownership_date_end = date("Y-m-d", strtotime($ovalue['end_date_of_ownership']));
				} else {
					$ownership_date_end = '0000-00-00';
				}
				$this->db->query("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$horse_id."', from_owner = '".$this->db->escape($ovalue['from_owner'])."',from_owner_id = '".$ovalue['from_owner_id']."', to_owner = '".$this->db->escape($ovalue['to_owner'])."',to_owner_id = '".$ovalue['to_owner_id']."',owner_percentage ='".$ovalue['owner_percentage']."',ownership_type ='".$ovalue['ownership_type']."' ,date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."',remark_horse_to_owner = '".$this->db->escape($ovalue['remark_horse_to_owner'])."' , owner_color = '".$this->db->escape($ovalue['owner_color'])."'  ");
				$this->log->write("INSERT INTO `horse_to_owner` SET horse_id = '".(int)$horse_id."', from_owner = '".$this->db->escape($ovalue['from_owner'])."',from_owner_id = '".$ovalue['from_owner_id']."', to_owner = '".$this->db->escape($ovalue['to_owner'])."',to_owner_id = '".$ovalue['to_owner_id']."',owner_percentage ='".$ovalue['owner_percentage']."',ownership_type ='".$ovalue['ownership_type']."' ,date_of_ownership ='".$ownership_from_date."', end_date_of_ownership ='".$ownership_date_end."',remark_horse_to_owner = '".$this->db->escape($ovalue['remark_horse_to_owner'])."' , owner_color = '".$this->db->escape($ovalue['owner_color'])."'  ");
				
			}
		}*/

		if(isset($data['bandats'])){
			foreach ($data['bandats'] as $bkey => $bvalue) { //echo "<pre>";print_r($data['ownerdatas']);exit;
				$start_date_bans = date("Y-m-d", strtotime($bvalue['startdate_ban']));
				if($bvalue['enddate_ban'] != ''){
					$end_date_ban = date("Y-m-d", strtotime($bvalue['enddate_ban']));
				} else {
					$end_date_ban = '0000-00-00';
				}
				if(isset($bvalue['horse_ban_id'])){
					if($bvalue['horse_ban_id'] != '0'){
						$ban_exist_sql = $this->db->query("SELECT * FROM `horse_ban` WHERE horse_id = '" .(int)$horse_id . "' AND horse_ban_id = '".$bvalue['horse_ban_id']."' ");
						if($ban_exist_sql->num_rows > 0){ 
							$this->db->query("DELETE FROM `horse_ban` WHERE horse_id = '" .(int)$horse_id . "' AND horse_ban_id = '".$bvalue['horse_ban_id']."'");
							$this->log->write("DELETE FROM `horse_ban` WHERE horse_id = '" .(int)$horse_id . "' AND horse_ban_id = '".$bvalue['horse_ban_id']."'");
						}
					}
				}
				$this->db->query("INSERT INTO `horse_ban` SET horse_id = '".(int)$horse_id."', club_ban = '".$this->db->escape($bvalue['club_ban'])."',startdate_ban = '".$start_date_bans."',enddate_ban = '".$end_date_ban."', authority = '".$this->db->escape($bvalue['authority'])."', reason_ban = '".$this->db->escape($bvalue['reason_ban'])."'");
				$this->log->write("INSERT INTO `horse_ban` SET horse_id = '".(int)$horse_id."', club_ban = '".$this->db->escape($bvalue['club_ban'])."',startdate_ban = '".$start_date_bans."',enddate_ban = '".$end_date_ban."', authority = '".$this->db->escape($bvalue['authority'])."', reason_ban = '".$this->db->escape($bvalue['reason_ban'])."'");
			}
		}

		if(isset($data['equipment_dtas'])){
			foreach ($data['equipment_dtas'] as $equipmentkey => $equipmentvalue) {
				if($equipmentvalue['equipment_date'] != ''){
					$choose_dates = date("Y-m-d", strtotime($equipmentvalue['equipment_date']));
				} else {
					$choose_dates = '0000-00-00';
				}
				$this->db->query("INSERT INTO `horse_equipments` SET horse_id = '".(int)$horse_id."', equipment_name = '".$this->db->escape($equipmentvalue['equipment_name'])."',equipment_date = '".$choose_dates."',equipment_status = '".$equipmentvalue['equipment_status']."' ");
				$this->log->write("INSERT INTO `horse_equipments` SET horse_id = '".(int)$horse_id."', equipment_name = '".$this->db->escape($equipmentvalue['equipment_name'])."',equipment_date = '".$choose_dates."',equipment_status = '".$equipmentvalue['equipment_status']."' ");
			}
		}

		if(isset($data['shoe_datas'])){
			foreach ($data['shoe_datas'] as $shoekey => $shoevalue) {
				
				if($shoevalue['shoe_and_bit_date'] != ''){
					$shoe_and_bit_date = date("Y-m-d", strtotime($shoevalue['shoe_and_bit_date']));
				} else {
					$shoe_and_bit_date = '0000-00-00';
				}
				$this->log->write("INSERT INTO `horse_shoeing` SET horse_id = '".(int)$horse_id."', type = '".$this->db->escape($shoevalue['type'])."', shoe_name = '".$this->db->escape($shoevalue['shoe_name'])."', shoe_description = '".$this->db->escape($shoevalue['shoe_description'])."', bit_name = '".$this->db->escape($shoevalue['bit_name'])."', bit_description = '".$this->db->escape($shoevalue['bit_description'])."', shoe_and_bit_date = '".$shoe_and_bit_date."' , shoe_and_bit_status = '".$shoevalue['shoe_and_bit_status']."'");
				$this->db->query("INSERT INTO `horse_shoeing` SET horse_id = '".(int)$horse_id."', type = '".$this->db->escape($shoevalue['type'])."', shoe_name = '".$this->db->escape($shoevalue['shoe_name'])."', shoe_description = '".$this->db->escape($shoevalue['shoe_description'])."', bit_name = '".$this->db->escape($shoevalue['bit_name'])."', bit_description = '".$this->db->escape($shoevalue['bit_description'])."', shoe_and_bit_date = '".$shoe_and_bit_date."' , shoe_and_bit_status = '".$shoevalue['shoe_and_bit_status']."'");
			}
		}

		if(isset($data['stackesdatas'])){
			foreach ($data['stackesdatas'] as $stackkey => $stackvalue) {
				if($stackvalue['stack_race_date'] != ''){
					$stacke_date = date("Y-m-d", strtotime($stackvalue['stack_race_date']));
				} else {
					$stacke_date = '0000-00-00';
				}
				if($stackvalue['horse_stackoutstation_id'] == '0'){
					$this->log->write("INSERT INTO `horse_stackoutstation` SET horse_id = '".(int)$horse_id."', stack_venue = '".$this->db->escape($stackvalue['stack_venue'])."', season_name = '".$this->db->escape($stackvalue['season_name'])."', stack_race_no = '".(int)$stackvalue['stack_race_no']."',stack_placing_no = '".(int)$stackvalue['stack_placing_no']."',stack_id = '".(int)$stackvalue['stack_id']."',stack_race_date = '".$stacke_date."',grade_stack_id = '".$stackvalue['grade_stack_id']."'");
					$this->db->query("INSERT INTO `horse_stackoutstation` SET horse_id = '".(int)$horse_id."', stack_venue = '".$this->db->escape($stackvalue['stack_venue'])."', season_name = '".$this->db->escape($stackvalue['season_name'])."', stack_race_no = '".(int)$stackvalue['stack_race_no']."',stack_placing_no = '".(int)$stackvalue['stack_placing_no']."',stack_id = '".(int)$stackvalue['stack_id']."',stack_race_date = '".$stacke_date."',grade_stack_id = '".$stackvalue['grade_stack_id']."'");
				}
			}
		}
	}

	public function deleteCategory($category_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_path WHERE category_id = '" . (int)$category_id . "'");

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path WHERE path_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$this->deleteCategory($result['category_id']);
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_category WHERE category_id = '" . (int)$category_id . "'");

		$this->cache->delete('category');
	}

	public function repairCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$parent_id . "'");

		foreach ($query->rows as $category) {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category['category_id'] . "'");

			// Fix for records with no paths
			$level = 0;

			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$parent_id . "' ORDER BY level ASC");

			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

				$level++;
			}

			$this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$category['category_id'] . "', level = '" . (int)$level . "'");

			$this->repairCategories($category['category_id']);
		}
	}

	public function getHorses($data) {

		$sql =("SELECT * FROM `horse1` WHERE 1=1");

		if (!empty($data['filter_hourse_name'])) {
			$sql .= " AND LOWER(official_name) LIKE '%" . $this->db->escape(strtolower($data['filter_hourse_name'])) . "%'";
		}

		if (!empty($data['filter_hourse_id'])) {
			$sql .= " AND `horse_code` = '" . ($data['filter_hourse_id']) . "' ";
		}

		/*if (!empty($data['filter_trainer_name'])) {
			$sql .= " AND LOWER(trainer_name) LIKE '%" . $this->db->escape(strtolower($data['filter_trainer_name'])) . "%' ";
		}*/
		$sql .= " AND horse_status = '" . $this->db->escape($data['filter_status']) . "' ";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getHourseCount($data) {

		$sql = ("SELECT COUNT(*) AS total FROM `horse1` LEFT JOIN horse_to_trainer ON horse1.horseseq =horse_to_trainer.horse_id WHERE 1=1");

		if (!empty($data['filter_hourse_name'])) {
			$sql .= " AND LOWER(official_name) LIKE '%" . $this->db->escape(strtolower($data['filter_hourse_name'])) . "%'";
		}

		if (!empty($data['filter_hourse_id'])) {
			$sql .= " AND `horse_code` = '" . ($data['filter_hourse_id']) . "' ";
		}

		if (!empty($data['filter_trainer_name'])) {
			$sql .= " AND LOWER(trainer_name) LIKE '%" . $this->db->escape(strtolower($data['filter_trainer_name'])) . "%' ";
		}

		$sql .= " AND horse_status = '" . $this->db->escape($data['filter_status']) . "' ";

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getHorsesform($horse_id) {
		$sql =("SELECT * FROM horse1 WHERE `horseseq`= '".$horse_id."' ");
		$query = $this->db->query($sql)->row;
		return $query;
	}

	public function getTrainerAll($horse_id) {
		$sql =("SELECT * FROM horse_to_trainer WHERE date_of_charge != (SELECT MAX(date_of_charge) FROM horse_to_trainer) AND horse_id = '".$horse_id."' ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getTrainerCurrent($horse_id) {
		$sql =("SELECT * FROM `horse_to_trainer`WHERE horse_id = '".$horse_id."' ORDER BY `horse_to_trainer_id` DESC LIMIT 1 ");
		$query = $this->db->query($sql)->row;
		return $query;
	}

	public function getOwnerform($horse_id) {
		$sql =("SELECT * FROM horse_to_owner WHERE `horse_id`= '".$horse_id."' ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getBanWithdateEnd($horse_id) {
		$sql =("SELECT * FROM horse_ban WHERE `horse_id`= '".$horse_id."' AND (DATE(`enddate_ban`) <> '0000-00-00') ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getBanWithoutdateEnd($horse_id) {
		$sql =("SELECT * FROM horse_ban WHERE `horse_id`= '".$horse_id."' AND (enddate_ban = '' OR DATE(`enddate_ban`) = '0000-00-00') ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getEquipment($horse_id) {
		$sql =("SELECT * FROM horse_equipments WHERE `horse_id`= '".$horse_id."' ORDER BY equipment_date ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getShoie($horse_id) {
		$sql =("SELECT * FROM  horse_shoeing WHERE `horse_id`= '".$horse_id."' ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getStackOt($horse_id) {
		$sql =("SELECT * FROM  horse_stackoutstation WHERE `horse_id`= '".$horse_id."' ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getHorsesFromOwnerAuto($from_owner) {
		$sql =("SELECT  owner_name,id FROM owners WHERE 1 = 1");
		if($from_owner != ''){
			$sql .= " AND `owner_name` LIKE '%" . $this->db->escape($from_owner) . "%' ";
		}
		$sql .= " ORDER BY `owner_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";
		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getHorsesToOwnerAuto($to_owner) {

		$sql =("SELECT owner_name,id FROM owners WHERE 1 = 1");
		if($to_owner != ''){
			$sql .= " AND `owner_name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getHorsesToTrainerAuto($to_owner) {

		$sql =("SELECT name,trainer_code,id FROM trainers WHERE active = 1");
		if($to_owner != ''){
			$sql .= " AND `name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		$sql .= " ORDER BY `name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getHorsesequipments($equipment_name) {

		$sql =("SELECT equipment_name FROM equipment WHERE equipment_status = 1 ");
		if($equipment_name != ''){
			$sql .= " AND `equipment_name` LIKE '%" . $this->db->escape($equipment_name) . "%'";
		}
		$sql .= " ORDER BY `equipment_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getCategories($data = array()) {
		$sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.category_id";

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCategoryDescriptions($category_id) {
		$category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'description'      => $result['description']
			);
		}

		return $category_description_data;
	}

	public function getCategoryFilters($category_id) {
		$category_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_filter_data[] = $result['filter_id'];
		}

		return $category_filter_data;
	}

	public function getCategoryStores($category_id) {
		$category_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_store_data[] = $result['store_id'];
		}

		return $category_store_data;
	}

	public function getCategoryLayouts($category_id) {
		$category_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $category_layout_data;
	}

	public function getTotalCategories() {

		$query = $this->db->query("SELECT COUNT(*) AS total FROM horse1 where 1=1");

		return $query->row['total'];
	}
	
	public function getTotalCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}

	public function getHorsesAuto($to_owner) {

		$sql =("SELECT official_name,horseseq FROM horse1 WHERE 1 = 1");
		if($to_owner != ''){
			$sql .= " AND `official_name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		$sql .= " ORDER BY `official_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}	
}
