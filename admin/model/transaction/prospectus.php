<?php
class ModelTransactionProspectus extends Model {
	public function addProspectus($data) {
		// echo '<pre>';
		// print_r($data);
		// exit;

		if($this->db->escape($data['entries_close_date']) != ''){
			$entries_close_date = $this->db->escape(date('Y-m-d', strtotime($data['entries_close_date'])));
		}else{
			$entries_close_date = '0000-00-00';
		}

		if($this->db->escape($data['handicaps_date']) != ''){
			$handicaps_date = $this->db->escape(date('Y-m-d', strtotime($data['handicaps_date'])));
		}else{
			$handicaps_date = '0000-00-00';
		}
		if($this->db->escape($data['acceptance_date']) != ''){
			$acceptance_date = $this->db->escape(date('Y-m-d', strtotime($data['acceptance_date'])));
		}else{
			$acceptance_date = '0000-00-00';
		}
		if($this->db->escape($data['declaration_date']) != ''){
			$declaration_date = $this->db->escape(date('Y-m-d', strtotime($data['declaration_date'])));
		}else{
			$declaration_date = '0000-00-00';
		}
		if ($data['race_type'] == 'Handicap Race') {
			$data['race_status'] = '1';
		} 
		else if ($data['race_type'] == 'Term Race') {
			$data['race_status'] = '2';
		} else {
			$data['race_status'] = '';
		}

		$evening_race = isset($data['evening_race']) ? 'Yes' : 'No';
		$lower_class_aligible = isset($data['lower_class_aligible']) ? 'Yes' : 'No';


		$this->db->query("INSERT INTO prospectus SET  
			`race_date` ='" . $this->db->escape(date('Y-m-d', strtotime($data['race_date']))) . "',
			`serial_no` = '" . $this->db->escape($data['serial_no']) . "',
			`season` = '" . $this->db->escape($data['season']) . "',
			`year` = '" . $this->db->escape($data['year']) . "',
			`race_day` = '" . $this->db->escape($data['race_day']) . "',
			`evening_race` = '" . $this->db->escape($evening_race) . "',
			`race_name` = '" . $this->db->escape($data['race_name']) . "',
			`race_type` = '" . $this->db->escape($data['race_type']) . "',
			`race_status` = '" . $this->db->escape($data['race_status']) . "',
			`class` = '" . $this->db->escape($data['class']) . "',
			`grade` = '" . $this->db->escape($data['grade']) . "',
			`race_category` = '" . $this->db->escape($data['race_category']) . "',
			`race_description` = '" . $this->db->escape($data['race_description']) . "',
			`distance` = '" . $this->db->escape($data['distance']) . "',
			`lower_class_aligible` = '" . $this->db->escape($lower_class_aligible) . "',
			`entries_close_date` = '" . $entries_close_date. "',
			`entries_close_date_time` = '" . $this->db->escape($data['entries_close_date_time']) . "',
			`handicaps_date` ='" . $handicaps_date . "',
			`handicaps_date_time` = '" . $this->db->escape($data['handicaps_date_time']) . "',
			`acceptance_date` = '" . $acceptance_date . "',
			`acceptance_date_time` = '" . $this->db->escape($data['acceptance_date_time']) . "',
			`declaration_date` ='" . $declaration_date . "',
			`declaration_date_time` = '" . $this->db->escape($data['declaration_date_time']) . "',
			`rating_from` = '" . $this->db->escape($data['rating_from']) . "',
            `rating_to` = '" . $this->db->escape($data['rating_to']) . "',
			`stacks_per` = '".$data['stakes_per']."'
			
		");

		$pros_id = $this->db->getLastId();


		if($this->db->escape($data['run_thrice_date']) != ''){
			$run_thrice_date = $this->db->escape(date('Y-m-d', strtotime($data['run_thrice_date'])));
			$run_thrice = 'Yes';
		}else{
			$run_thrice_date = '0000-00-00';
			$run_thrice = 'No';

		}
		if($this->db->escape($data['run_not_placed_date']) != ''){
			$run_not_placed_date = $this->db->escape(date('Y-m-d', strtotime($data['run_not_placed_date'])));
			$run_not_placed = 'Yes';

		}else{
			$run_not_placed_date = '0000-00-00';
			$run_not_placed = 'No';

		}
		if($this->db->escape($data['run_and_not_won_during_mtg_date']) != ''){
			$run_and_not_won_during_mtg_date = $this->db->escape(date('Y-m-d', strtotime($data['run_and_not_won_during_mtg_date'])));
			$run_and_not_won_during_mtg = 'Yes';

		}else{
			$run_and_not_won_during_mtg_date = '0000-00-00';
			$run_and_not_won_during_mtg = 'No';

		}
		if($this->db->escape($data['run_not_won_date']) != ''){
			$run_not_won_date = $this->db->escape(date('Y-m-d', strtotime($data['run_not_won_date'])));
			$run_not_won = 'Yes';

		}else{
			$run_not_won_date = '0000-00-00';
			$run_not_won = 'No';

		}

		$maidens = isset($data['maidens']) ? 'Yes' : 'No';
		$unraced = isset($data['unraced']) ? 'Yes' : 'No';
		$unplaced = isset($data['unplaced']) ? 'Yes' : 'No';
		$not1_2_3 = isset($data['not1_2_3']) ? 'Yes' : 'No';
		$foreign_jockey_eligible = isset($data['foreign_jockey_eligible']) ? 'Yes' : 'No';
		$foreign_horse_eligible = isset($data['foreign_horse_eligible']) ? 'Yes' : 'No';
		$sire_dam = isset($data['sire_dam']) ? 'Yes' : 'No';
		$no_whip = isset($data['no_whip']) ? 'Yes' : 'No';


		$this->db->query("INSERT INTO prospectus_race_eligibility_criteria SET
			`pros_id` = '" . (int)$pros_id . "',
			`age_from` = '" . $this->db->escape($data['age_from']) . "',
			`age_to` = '" . $this->db->escape($data['age_to']) . "',
			`max_win` = '" . $this->db->escape($data['max_win']) . "',
			`sex` = '" . $this->db->escape($data['sex']) . "',
			`maidens` = '" . $this->db->escape($maidens) . "',
			`unraced` = '" . $this->db->escape($unraced) . "',
			`not1_2_3` = '" . $this->db->escape($not1_2_3) . "',
			`unplaced` = '" . $this->db->escape($unplaced) . "',
			
			`max_stakes_race` = '" . $this->db->escape($data['max_stakes_race']) . "',
			`run_thrice` = '" . $this->db->escape($run_thrice) . "',
			`run_thrice_date` = '" . $run_thrice_date . "',
			`run_not_placed` = '" . $this->db->escape($run_not_placed) . "',
			`run_not_placed_date` = '" . $run_not_placed_date . "',
			`run_not_won` = '" . $this->db->escape($run_not_won) . "',
			`run_not_won_date` ='" . $run_not_won_date . "',

			`run_and_not_won_during_mtg` = '" . $this->db->escape($run_and_not_won_during_mtg) . "',
			`run_and_not_won_during_mtg_date` ='" . $run_and_not_won_during_mtg_date . "',
			`no_whip` = '" . $this->db->escape($no_whip) . "',
			`foreign_jockey_eligible` = '" . $this->db->escape($foreign_jockey_eligible) . "',
			`foreign_horse_eligible` = '" . $this->db->escape($foreign_horse_eligible) . "',
			`sire_dam` = '" . $this->db->escape($sire_dam) . "'
			");

		if($this->db->escape($data['entry_date']) != ''){
			$entry_date = $this->db->escape(date('Y-m-d', strtotime($data['entry_date'])));
		}else{
			$entry_date = '0000-00-00';
		}

		if($this->db->escape($data['forefit_date']) != ''){
			$forefit_date = $this->db->escape(date('Y-m-d', strtotime($data['forefit_date'])));
		}else{
			$forefit_date = '0000-00-00';
		}

		$bonus_applicable = isset($data['bonus_applicable']) ? 'Yes' : 'No';
		$additional_entry = isset($data['additional_entry']) ? 'Yes' : 'No';

		$bonus_to_breeder = isset($data['bonus_to_breeder']) ? 'Yes' : 'No';

		
		$this->db->query("INSERT INTO race_stakes_details SET
			`pros_id` = '" . (int)$pros_id . "',
			`stakes_in` = '" . $this->db->escape($data['stakes_in']) . "',
			`trophy_value_amount` = '" . $this->db->escape($data['trophy_value_amount']) . "',
			`positions` = '" . $this->db->escape($data['positions']) . "',
			`stakes_money` = '" . $this->db->escape($data['stakes_money']) . "',
			`bonus_applicable` = '" . $this->db->escape($data['bonus_applicable']) . "',
			`sweepstake_race` = '" . $this->db->escape($data['sweepstake_race']) . "',
			`sweepstake_of` = '" . $this->db->escape($data['sweepstake_of']) . "',
			`no_of_forfiet` = '" . $this->db->escape($data['no_of_forfiet']) . "',
			`entry_date` ='" . $entry_date . "',
			`entry_time` = '" . $this->db->escape($data['entry_time']) . "',
			`entry_fees` = '" . $this->db->escape($data['entry_fees']) . "',

			`forefit_date` ='" . $forefit_date . "',
			`forefit_time` = '" . $this->db->escape($data['forefit_time']) . "',
			`forefit_amt` = '" . $this->db->escape($data['forefit_amt']) . "',

			`additional_entry` = '" . $this->db->escape($additional_entry) . "',
			`bonus_to_breeder` = '" . $this->db->escape($bonus_to_breeder) . "',
			`remarks` = '" . $this->db->escape($data['remarks']) . "'
			");


		if(isset($data['race'])){
			foreach ($data['race'] as $key => $value) {
				$this->db->query("INSERT INTO prospectus_prize_distribution SET
				pros_id = '".(int)$pros_id."',
				placing = '".$this->db->escape($value['placing'])."',
				owner_per = '".$this->db->escape($value['owner_per'])."',
				owner = '".$this->db->escape($value['owner'])."',
				trainer_perc = '".$this->db->escape($value['trainer_perc'])."' ,
				trainer = '".$this->db->escape($value['trainer'])."',
				jockey_perc = '".$this->db->escape($value['jockey_perc'])."',
				jockey = '".$this->db->escape($value['jockey'])."' ");
			}
		}


		if(isset($data['race_rs'])){
			foreach ($data['race_rs'] as $key => $value) {
				$this->db->query("INSERT INTO prospectus_prize_distribution SET
				pros_id = '".(int)$pros_id."',
				placing = '".$this->db->escape($value['placing'])."',
				owner = '".$this->db->escape($value['owner'])."',
				trainer = '".$this->db->escape($value['trainer'])."',
				jockey = '".$this->db->escape($value['jockey'])."' ");
			}
		}

		return $pros_id;
	}
	

	public function editProspectus($pros_id,$data) {
		if($this->db->escape($data['entries_close_date']) != ''){
			$entries_close_date = $this->db->escape(date('Y-m-d', strtotime($data['entries_close_date'])));
		}else{
			$entries_close_date = '0000-00-00';
		}

		if($this->db->escape($data['handicaps_date']) != ''){
			$handicaps_date = $this->db->escape(date('Y-m-d', strtotime($data['handicaps_date'])));
		}else{
			$handicaps_date = '0000-00-00';
		}
		if($this->db->escape($data['acceptance_date']) != ''){
			$acceptance_date = $this->db->escape(date('Y-m-d', strtotime($data['acceptance_date'])));
		}else{
			$acceptance_date = '0000-00-00';
		}
		if($this->db->escape($data['declaration_date']) != ''){
			$declaration_date = $this->db->escape(date('Y-m-d', strtotime($data['declaration_date'])));
		}else{
			$declaration_date = '0000-00-00';
		}
		if ($data['race_type'] == 'Handicap Race') {
			$data['race_status'] = '1';
		} 
		else if ($data['race_type'] == 'Term Race') {
			$data['race_status'] = '2';
		} else {
			$data['race_status'] = '';
		}

		$evening_race = isset($data['evening_race']) ? 'Yes' : 'No';
		$lower_class_aligible = isset($data['lower_class_aligible']) ? 'Yes' : 'No';

		
		$this->db->query("UPDATE prospectus SET
			`race_date` ='" . $this->db->escape(date('Y-m-d', strtotime($data['race_date']))) . "',
			`serial_no` = '" . $this->db->escape($data['serial_no']) . "',
			`season` = '" . $this->db->escape($data['season']) . "',
			`year` = '" . $this->db->escape($data['year']) . "',
			`race_day` = '" . $this->db->escape($data['race_day']) . "',
			`evening_race` = '" . $this->db->escape($evening_race) . "',
			`race_name` = '" . $this->db->escape($data['race_name']) . "',
			`race_type` = '" . $this->db->escape($data['race_type']) . "',
			`race_status` = '" . $this->db->escape($data['race_status']) . "',
			`class` = '" . $this->db->escape($data['class']) . "',
			`grade` = '" . $this->db->escape($data['grade']) . "',
			`race_category` = '" . $this->db->escape($data['race_category']) . "',
			`race_description` = '" . $this->db->escape($data['race_description']) . "',
			`distance` = '" . $this->db->escape($data['distance']) . "',
			`lower_class_aligible` = '" . $this->db->escape($lower_class_aligible) . "',
			`entries_close_date` = '" . $entries_close_date. "',
			`entries_close_date_time` = '" . $this->db->escape($data['entries_close_date_time']) . "',
			`handicaps_date` ='" . $handicaps_date . "',
			`handicaps_date_time` = '" . $this->db->escape($data['handicaps_date_time']) . "',
			`acceptance_date` = '" . $acceptance_date . "',
			`acceptance_date_time` = '" . $this->db->escape($data['acceptance_date_time']) . "',
			`declaration_date` ='" . $declaration_date . "',
			`declaration_date_time` = '" . $this->db->escape($data['declaration_date_time']) . "',
			`rating_from` = '" . $this->db->escape($data['rating_from']) . "',
            `rating_to` = '" . $this->db->escape($data['rating_to']) . "',
			`stacks_per` = '".$data['stakes_per']."'

			WHERE pros_id='".$pros_id."'
			");

		if($this->db->escape($data['run_thrice_date']) != ''){
			$run_thrice_date = $this->db->escape(date('Y-m-d', strtotime($data['run_thrice_date'])));
			$run_thrice = 'Yes';
		}else{
			$run_thrice_date = '0000-00-00';
			$run_thrice = 'No';

		}
		if($this->db->escape($data['run_not_placed_date']) != ''){
			$run_not_placed_date = $this->db->escape(date('Y-m-d', strtotime($data['run_not_placed_date'])));
			$run_not_placed = 'Yes';

		}else{
			$run_not_placed_date = '0000-00-00';
			$run_not_placed = 'No';

		}
		if($this->db->escape($data['run_and_not_won_during_mtg_date']) != ''){
			$run_and_not_won_during_mtg_date = $this->db->escape(date('Y-m-d', strtotime($data['run_and_not_won_during_mtg_date'])));
			$run_and_not_won_during_mtg = 'Yes';

		}else{
			$run_and_not_won_during_mtg_date = '0000-00-00';
			$run_and_not_won_during_mtg = 'No';

		}
		if($this->db->escape($data['run_not_won_date']) != ''){
			$run_not_won_date = $this->db->escape(date('Y-m-d', strtotime($data['run_not_won_date'])));
			$run_not_won = 'Yes';

		}else{
			$run_not_won_date = '0000-00-00';
			$run_not_won = 'No';

		}

		$maidens = isset($data['maidens']) ? 'Yes' : 'No';
		$unraced = isset($data['unraced']) ? 'Yes' : 'No';
		$unplaced = isset($data['unplaced']) ? 'Yes' : 'No';
		$not1_2_3 = isset($data['not1_2_3']) ? 'Yes' : 'No';
		$foreign_jockey_eligible = isset($data['foreign_jockey_eligible']) ? 'Yes' : 'No';
		$foreign_horse_eligible = isset($data['foreign_horse_eligible']) ? 'Yes' : 'No';
		$sire_dam = isset($data['sire_dam']) ? 'Yes' : 'No';
		$no_whip = isset($data['no_whip']) ? 'Yes' : 'No';


		$this->db->query("UPDATE prospectus_race_eligibility_criteria SET
			
			`age_from` = '" . $this->db->escape($data['age_from']) . "',
			`age_to` = '" . $this->db->escape($data['age_to']) . "',
			`max_win` = '" . $this->db->escape($data['max_win']) . "',
			`sex` = '" . $this->db->escape($data['sex']) . "',
			`maidens` = '" . $this->db->escape($maidens) . "',
			`unraced` = '" . $this->db->escape($unraced) . "',
			`not1_2_3` = '" . $this->db->escape($not1_2_3) . "',
			`unplaced` = '" . $this->db->escape($unplaced) . "',
			`max_stakes_race` = '" . $this->db->escape($data['max_stakes_race']) . "',
			`run_thrice` = '" . $this->db->escape($run_thrice) . "',
			`run_thrice_date` = '" . $run_thrice_date . "',
			`run_not_placed` = '" . $this->db->escape($run_not_placed) . "',
			`run_not_placed_date` = '" . $run_not_placed_date . "',
			`run_not_won` = '" . $this->db->escape($run_not_won) . "',
			`run_not_won_date` ='" . $run_not_won_date . "',

			`run_and_not_won_during_mtg` = '" . $this->db->escape($run_and_not_won_during_mtg) . "',
			`run_and_not_won_during_mtg_date` ='" . $run_and_not_won_during_mtg_date . "',
			`no_whip` = '" . $this->db->escape($no_whip) . "',
			`foreign_jockey_eligible` = '" . $this->db->escape($foreign_jockey_eligible) . "',
			`foreign_horse_eligible` = '" . $this->db->escape($foreign_horse_eligible) . "',
			`sire_dam` = '" . $this->db->escape($sire_dam) . "'
			WHERE pros_id='".$pros_id."'
			");

		if($this->db->escape($data['entry_date']) != ''){
			$entry_date = $this->db->escape(date('Y-m-d', strtotime($data['entry_date'])));
		}else{
			$entry_date = '0000-00-00';
		}

		if($this->db->escape($data['forefit_date']) != ''){
			$forefit_date = $this->db->escape(date('Y-m-d', strtotime($data['forefit_date'])));
		}else{
			$forefit_date = '0000-00-00';
		}

		$bonus_applicable = isset($data['bonus_applicable']) ? 'Yes' : 'No';

		$this->db->query("UPDATE race_stakes_details SET
			`stakes_in` = '" . $this->db->escape($data['stakes_in']) . "',
			`trophy_value_amount` = '" . $this->db->escape($data['trophy_value_amount']) . "',
			`positions` = '" . $this->db->escape($data['positions']) . "',
			`stakes_money` = '" . $this->db->escape($data['stakes_money']) . "',
			`bonus_applicable` = '" . $this->db->escape($bonus_applicable) . "',
			`sweepstake_race` = '" . $this->db->escape($data['sweepstake_race']) . "',
			`sweepstake_of` = '" . $this->db->escape($data['sweepstake_of']) . "',
			`no_of_forfiet` = '" . $this->db->escape($data['no_of_forfiet']) . "',
			`entry_date` ='" . $entry_date . "',
			`entry_time` = '" . $this->db->escape($data['entry_time']) . "',
			`entry_fees` = '" . $this->db->escape($data['entry_fees']) . "',
			`forefit_date` ='" . $forefit_date . "',
			`forefit_time` = '" . $this->db->escape($data['forefit_time']) . "',
			`forefit_amt` = '" . $this->db->escape($data['forefit_amt']) . "',
			`additional_entry` = '" . $this->db->escape($data['additional_entry']) . "',
			`bonus_to_breeder` = '" . $this->db->escape($data['bonus_to_breeder']) . "',
			`remarks` = '" . $this->db->escape($data['remarks']) . "'
			WHERE pros_id='".$pros_id."'
			");

		$this->db->query("DELETE FROM  prospectus_prize_distribution WHERE pros_id='".$pros_id."' ");
		if(isset($data['race'])){
			foreach ($data['race'] as $key => $value) {
				$this->db->query("INSERT INTO prospectus_prize_distribution SET
				pros_id = '".(int)$pros_id."',
				placing = '".$this->db->escape($value['placing'])."',
				owner = '".$this->db->escape($value['owner'])."',
				owner_per = '".$this->db->escape($value['owner_per'])."',
				trainer_perc = '".$this->db->escape($value['trainer_perc'])."' ,
				trainer = '".$this->db->escape($value['trainer'])."',
				jockey_perc = '".$this->db->escape($value['jockey_perc'])."',
				jockey = '".$this->db->escape($value['jockey'])."' ");
			}
		}
		

		if(isset($data['race_rs'])){
			foreach ($data['race_rs'] as $key => $value) {
				$this->db->query("INSERT INTO prospectus_prize_distribution SET
				pros_id = '".(int)$pros_id."',
				placing = '".$this->db->escape($value['placing'])."',
				owner = '".$this->db->escape($value['owner'])."',
				trainer = '".$this->db->escape($value['trainer'])."',
				jockey = '".$this->db->escape($value['jockey'])."' ");
			}
		}
	}

	public function deleteprospectus($pros_id) {
		$this->db->query("DELETE FROM `prospectus` WHERE pros_id = '" . (int)$pros_id . "'");
		$this->db->query("DELETE FROM `prospectus_race_eligibility_criteria` WHERE pros_id = '" . (int)$pros_id . "'");
		$this->db->query("DELETE FROM `race_stakes_details` WHERE pros_id = '" . (int)$pros_id . "'");
		$this->db->query("DELETE FROM  prospectus_prize_distribution WHERE pros_id='".$pros_id."' ");

	}


	public function getprospectus($data) {
		$date = date('Y-m-d');
		//echo "<pre>";print_r($date);exit;
		
		$sql =("SELECT * FROM `prospectus` LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id)  WHERE prospectus.race_date > '".$date."' ");

		if (!empty($data['filter_race_name'])) {
			$sql .= " AND LOWER(race_name) LIKE '%" . $this->db->escape(strtolower($data['filter_race_name'])) . "%' ";
		}
		if (!empty($data['filter_race_date'])) {
			$sql .= " AND LOWER(race_date) LIKE '%" . $this->db->escape(date('Y-m-d', strtotime($data['filter_race_date']))) . "%'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getCategory($pros_id) {  
		$query = $this->db->query("SELECT * FROM prospectus LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id) LEFT JOIN race_stakes_details ON(prospectus.pros_id =race_stakes_details.pros_id) WHERE prospectus.pros_id ='".$pros_id."'");

		// echo "SELECT * FROM prospectus LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id) LEFT JOIN race_stakes_details ON(prospectus.pros_id =race_stakes_details.pros_id) WHERE prospectus.pros_id ='".$pros_id."'";
		// exit;
		return $query->row;
	}


	public function getracename($pros_id) {  
		$query = $this->db->query("SELECT * FROM prospectus LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) WHERE pros_id='".$pros_id."'");
		return $query->rows;
	}


	public function getprizedistribution($pros_id) {  
		$query = $this->db->query("SELECT * FROM prospectus_prize_distribution WHERE pros_id='".$pros_id."' ORDER BY ppd_id ");
		return $query->rows;
	}


	public function getRacenameauto($race_name) {

        $sql =("SELECT race_name FROM prospectus WHERE 1 = 1");
        if($race_name != ''){
            $sql .= " AND `race_name` LIKE '%" . $this->db->escape($race_name) . "%'";
        }
        $sql .= " ORDER BY `race_name` ASC LIMIT 0, 100";
        
        $query = $this->db->query($sql)->rows;
        return $query;
    }


	public function getTotalCategories($data) {
		$date = date('Y-m-d');

		$query =("SELECT COUNT(*) AS total FROM prospectus LEFT JOIN prospectus_race_eligibility_criteria ON prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id WHERE prospectus.race_date > '".$date."' ");

		if (!empty($data['filter_race_name'])) {
			$query .= " AND LOWER(race_name) LIKE '%" . $this->db->escape(strtolower($data['filter_race_name'])) . "%' ";
		}

		if (!empty($data['filter_race_date'])) {
			$query .= " AND LOWER(race_date) LIKE '%" . $this->db->escape(date('Y-m-d', strtotime($data['filter_race_date']))) . "%'";
		}

		$sql = $this->db->query($query);
		return $sql->row['total'];
		
	}
	
	public function getTotalCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}	
}
