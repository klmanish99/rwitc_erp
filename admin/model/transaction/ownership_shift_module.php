<?php
class ModelTransactionOwnershipshiftmodule extends Model {
	public function addProspectus($data) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$entry_count = 1;
		$entry_counts = $this->db->query("SELECT entry_count FROM `horse_to_owner` ORDER BY horse_to_owner_id DESC LIMIT 1 ");
		if($entry_counts->num_rows > 0){
			$entry_count = $entry_counts->row['entry_count'] + 1;
		}
		$grandpa_id = 0;


		$date_added = date('Y-m-d');

		$data['total_share'] = (int)($data['total_share']);
		foreach($data['owner_datas'] as $okey => $ovalue){
			$share_saler_id = isset($ovalue['share_saler']) ? $ovalue['horse_to_owner_id'] : 0;
			if($share_saler_id != 0){
				$expire_date = date('Y-m-d');
				//$this->db->query("UPDATE `horse_to_owner` SET `owner_share_status` = '0', `end_date_of_ownership` = '".$expire_date."' WHERE `horse_to_owner_id` = '".$share_saler_id."'");
				$this->db->query("UPDATE `horse_to_owner` SET `owner_share_status` = '0' WHERE `horse_to_owner_id` = '".$share_saler_id."'");
				if( ($data['ownership_type_name'] == 'Lease' || $data['ownership_type_name'] == 'Sub Lease' ) && ($data['total_share'] > 0 )){
					$previous_owner_datas = $this->db->query("SELECT * FROM `horse_to_owner` WHERE `horse_to_owner_id` = '".$share_saler_id."'")->row;
					$fromdate = ($data['fromdate'] != '') ? date('Y-m-d',strtotime($data['fromdate'])) : "";
					$todate  = ($data['todate'] != '') ? date('Y-m-d',strtotime($data['todate'])) : "";

					//$type_of_os = ($data['ownership_type_name'] == 'Lease') ? "Lease" : "Lease"; //'Lease';//;

					if($data['ownership_type_name'] == 'Lease'){
						$type_of_os = "Lease";
					} elseif($data['ownership_type_name'] == 'Sub Lease'){
						$type_of_os = "Sub Lease";
					}

					$grandpa_id = isset($ovalue['share_saler']) ? $ovalue['grandpa_id'] : 0;

					$this->db->query("INSERT INTO `horse_to_owner` SET  
						`horse_id` = '" . $this->db->escape($previous_owner_datas['horse_id']) . "',
						`horse_group_id` = '" . $this->db->escape($previous_owner_datas['horse_group_id']) . "',
						`trainer_id` = '" . $this->db->escape($previous_owner_datas['trainer_id']) . "',
						`ownership_type` = '".$type_of_os."',
						`parent_group_id` = '".$previous_owner_datas['horse_group_id']."',
						`date_of_ownership` = '" . $this->db->escape($fromdate) . "',
						`end_date_of_ownership` = '" . $this->db->escape($todate) . "',
						`remark_horse_to_owner` = '" . $this->db->escape($previous_owner_datas['remark_horse_to_owner']) . "',
						`owner_percentage` = '" . $this->db->escape($data['total_share']) . "',
						`to_owner` = '" . $this->db->escape($previous_owner_datas['to_owner']) . "',
						`to_owner_id` = '" . $this->db->escape($previous_owner_datas['to_owner_id']) . "',
						`contengency` = '" . $this->db->escape($previous_owner_datas['contengency']) . "',
						`cont_percentage` = '" . $this->db->escape($previous_owner_datas['cont_percentage']) . "',
						`cont_amount` = '" . $this->db->escape($previous_owner_datas['cont_amount']) . "',
						`cont_place` = '" . $this->db->escape($previous_owner_datas['cont_place']) . "',
						`win_gross_stake` = '" . $this->db->escape($previous_owner_datas['win_gross_stake']) . "',
						`win_net_stake` = '" . $this->db->escape($previous_owner_datas['win_net_stake']) . "',
						`millionover` = '" . $this->db->escape($previous_owner_datas['millionover']) . "',
						`millionover_amt` = '" . $this->db->escape($previous_owner_datas['millionover_amt']) . "',
						`grade1` = '" . $this->db->escape($previous_owner_datas['grade1']) . "',
						`grade2` = '" . $this->db->escape($previous_owner_datas['grade2']) . "',
						`grade3` = '" . $this->db->escape($previous_owner_datas['grade3']) . "',
						`owner_share_status` = '1',
						`date_added` = '".$date_added."',
						`user_id` = '".$this->session->data['user_id']."',
						`entry_count` = '".$entry_count."',
						`grandpa_id` = '".$grandpa_id."',
						`provisional_ownership` = '".$previous_owner_datas['provisional_ownership']."'
					");

					$old_owner_trans_id = $this->db->getLastId();


					$this->db->query("INSERT INTO `horse_to_owner_charges` SET 
						`horse_to_owner_id` = '" . $old_owner_trans_id . "',
						 `horse_id` = '" . $this->db->escape($data['filterHorseId']) . "',
						 `owner_id` = '" . $this->db->escape($previous_owner_datas['to_owner_id']) . "', 
						 `owner_name` = '" . $this->db->escape($previous_owner_datas['to_owner']) . "', 
						 `share` = '" .$this->db->escape($data['total_share']). "' 
					");


					if(isset($ovalue['share_saler'])){
						// $previous_parent_datas = $this->db->query("SELECT parent_trans_id FROM `horse_to_owner_lease` WHERE `child_trans_id` = '".$ovalue['horse_to_owner_id']."'");
						// if($previous_parent_datas->num_rows > 0){
							$this->db->query("INSERT INTO `horse_to_owner_lease` SET `parent_trans_id` = '" . $ovalue['horse_to_owner_id'] . "', `child_trans_id` = '" . $old_owner_trans_id . "' ");
						
						//$type_of_ownership = 'Sale';
					}
				


				}
				
			}
		}

		$horse_group_idsss = $this->db->query("SELECT `horse_group_id` FROM horse_to_owner WHERE `horse_id` ='".$data['filterHorseId']."' ORDER BY horse_group_id DESC LIMIT 1")->row;
		if($horse_group_idsss['horse_group_id'] != 0){
			$horse_grp_id = $horse_group_idsss['horse_group_id'];
		} else {
			$last_horse_grpid = $this->db->query("SELECT `horse_group_id` FROM horse_to_owner  ORDER BY horse_group_id DESC LIMIT 1")->row;
			$horse_grp_id = $last_horse_grpid['horse_group_id'] + 1;
		}

		foreach($data['new_owners'] as $key => $value){
			$contengency = isset($value['contengency']) ? "1" : "0";
			$per = isset($value['per']) ? $value['per'] : "0";
			$amount = isset($value['amount']) ?  $value['amount'] : "0";
			$mo_rs = isset($value['mo_rs']) ?  $value['mo_rs'] : "0";
			$wgs = isset($value['wgs']) ? "1" : "0";
			$wns = isset($value['wns']) ? "1" : "0";
			$place = isset($value['place']) ? "1" : "0";
			$millionover = isset($value['millionover'] ) ? "1" : "0";
			$grade1 = isset($value['grade1']) ? "1" : "0";
			$grade2 = isset($value['grade2']) ? "1" : "0";
			$grade3 = isset($value['grade3']) ? "1" : "0";
			$check_provisional_owner = isset($data['check_provi_owner']) ? "Yes" : "No";
			$representative_owner_id = '';
			if(isset($data['partners_datas']) && isset($data['partners_datas'][$value['ownername_id']])){

				foreach ($data['partners_datas'][$value['ownername_id']] as $rkey => $rvalue) {
					if(isset($rvalue['is_reprentative']) &&  $value['ownername_id'] == $rvalue['owner_id']){
						$representative_owner_id = $rvalue['partner_id'];
					}
				}
			}
			$check_provisional_owner = isset($data['check_provi_owner']) ? "Yes" : "No";


			$fromdate = ($data['fromdate'] != '') ? date('Y-m-d',strtotime($data['fromdate'])) : "";
			$todate  = ($data['todate'] != '') ? date('Y-m-d',strtotime($data['todate'])) : "";
			$parent_group_id = ($data['ownership_type_name'] == 'Lease') ? $horse_grp_id : 0;
			if ($data['check_provi_owner'] == 1) {
				$type_of_ownership = 'Sale';
			} else{
				$type_of_ownership = $data['ownership_type_name'];
			}
			if($data['ownership_type_name'] == 'Lease' || $data['ownership_type_name'] == 'Sub Lease'){
				foreach($data['owner_datas'] as $wkey => $wvalue){
					if($wvalue['from_owner_hidden'] == $value['ownername_id']){
						//$type_of_ownership = 'Lease';//($data['ownership_type_name'] == 'Lease') ? "Sale" : "Lease";
						if($data['ownership_type_name'] == 'Lease'){
							$type_of_ownership = "Lease";
						} elseif($data['ownership_type_name'] == 'Sub Lease'){
							$type_of_ownership = "Sub Lease";
						}
					}
					if(isset($wvalue['share_saler'])){
						$grandpa_id = $wvalue['grandpa_id'];
					}
				}
			}
			
			
			

			if($value['ownername'] != ''){
				$this->db->query("INSERT INTO `horse_to_owner` SET  
					`horse_id` = '" . $this->db->escape($data['filterHorseId']) . "',
					`horse_group_id` = '" . $this->db->escape($horse_grp_id) . "',
					`parent_group_id` = '" . $this->db->escape($parent_group_id) . "',
					`trainer_id` = '" . $this->db->escape($data['trainer_id']) . "',
					`ownership_type` = '" . $type_of_ownership . "',
					`date_of_ownership` = '" . $this->db->escape($fromdate) . "',
					`end_date_of_ownership` = '" . $this->db->escape($todate) . "',
					`remark_horse_to_owner` = '" . $this->db->escape($data['remark']) . "',
					`owner_percentage` = '" . $this->db->escape($value['percentage']) . "',
					`to_owner` = '" . $this->db->escape($value['ownername']) . "',
					`to_owner_id` = '" . $this->db->escape($value['ownername_id']) . "',
					`contengency` = '" . $this->db->escape($contengency) . "',
					`cont_percentage` = '" . $this->db->escape($per) . "',
					`cont_amount` = '" . $this->db->escape($amount) . "',
					`cont_place` = '" . $this->db->escape($place) . "',
					`win_gross_stake` = '" . $this->db->escape($wgs) . "',
					`win_net_stake` = '" . $this->db->escape($wns) . "',
					`millionover` = '" . $this->db->escape($millionover) . "',
					`millionover_amt` = '" . $this->db->escape($mo_rs) . "',
					`grade1` = '" . $this->db->escape($grade1) . "',
					`grade2` = '" . $this->db->escape($grade2) . "',
					`grade3` = '" . $this->db->escape($grade3) . "',
					`owner_share_status` = '1',
					`provisional_ownership` = '".$check_provisional_owner."',
					`represntative_id` = '".$representative_owner_id."',
					`date_added` = '".$date_added."',
					`user_id` = '".$this->session->data['user_id']."',
					`entry_count` = '".$entry_count."',
					`grandpa_id` = '".$grandpa_id."',
					`charge_type` = '".$data['charge_type']."'

				");
			}

			$new_owner_trans_id = $this->db->getLastId();

			if($data['ownership_type_name'] == 'Sale'){
				// $grandpa_idss = $this->db->query("SELECT grandpa_id FROM horse_to_owner WHERE horse_id = '".$data['filterHorseId']."' AND  ownership_type = 'Sale'  ");
				// $grandpa_id =  ($grandpa_idss->num_rows > 0 ) ? $grandpa_idss->row['grandpa_id'] : $new_owner_trans_id;
				$this->db->query("UPDATE horse_to_owner SET grandpa_id = '".$new_owner_trans_id."' WHERE horse_to_owner_id
					 = '".$new_owner_trans_id."'");
 			}

			$this->db->query("INSERT INTO `horse_to_owner_charges` SET 
				`horse_to_owner_id` = '" . $new_owner_trans_id . "',
				 `horse_id` = '" . $this->db->escape($data['filterHorseId']) . "',
				 `owner_id` = '" . $this->db->escape($value['ownername_id']) . "', 
				 `owner_name` = '" . $this->db->escape($value['ownername']) . "', 
				 `share` = '" .$this->db->escape($value['percentage']). "' 
			");



			//if($data['ownership_type_name'] == 'Lease' || $data['ownership_type_name'] == 'Sub Lease'){
				foreach($data['owner_datas'] as $wkey => $wvalue){
					if(isset($wvalue['share_saler'])){
						$this->db->query("INSERT INTO `horse_to_owner_lease` SET `parent_trans_id` = '" . $wvalue['horse_to_owner_id'] . "', `child_trans_id` = '" . $new_owner_trans_id . "' ");
						//$type_of_ownership = 'Sale';
					}
				}
			//}
		}

		$history_owners = $this->db->query("SELECT `batch_id` FROM `horse_to_owner_history` WHERE horse_id = '".$data['filterHorseId']."' ORDER BY id DESC LIMIT 1 ");
		$batch_id = 1;
		if($history_owners->num_rows > 0){
			$batch_id = $history_owners->row['batch_id'] + 1;
			$this->db->query("UPDATE `horse_to_owner_history` SET status = 1 WHERE batch_id = '".$history_owners->row['batch_id']."' ");
		}

		$allCurrOwners = $this->db->query("SELECT horse_id, horse_to_owner_id FROM `horse_to_owner` WHERE horse_id = '".$data['filterHorseId']."' AND owner_share_status = 1 ");
		if($allCurrOwners->num_rows > 0){
			foreach ($allCurrOwners->rows as $ohkey => $ohvalue) {
				$this->db->query("INSERT INTO `horse_to_owner_history` SET 
					`horse_id` = '" . $ohvalue['horse_id'] . "',
					`batch_id` = '" . $batch_id . "',
					`horse_to_owner_id` = '" . $ohvalue['horse_to_owner_id'] . "',
					`status` = 0
					");
			}
		}

	}

	
	public function gethorsetoTrainer($data) {

		$sql =("SELECT trainer_id,trainer_code,trainer_name FROM `horse_to_trainer` WHERE 1=1");

		if (!empty($data['horse_id'])) {
			$sql .= " AND `horse_id` = '" . ($data['horse_id']) . "' ";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function gethorsetoOwner($data) {

		$sql =("SELECT * FROM `horse_to_owner` WHERE `owner_share_status` = 1 AND provisional_ownership = 'No' ");

		if (!empty($data['horse_id'])) {
			$sql .= " AND `horse_id` = '" . ($data['horse_id']) . "' ";
		}

		$sql .= " ORDER BY horse_to_owner_order ASC";


		$query = $this->db->query($sql)->rows;
		return $query;
	}


	public function getAllOwners($data){
		$sql =("SELECT * FROM `owners` WHERE `active` = 1");

		if (!empty($data['owner_name'])) {
			$sql .= " AND `owner_name` LIKE '%" . ($data['owner_name']) . "%' ";
		}

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getHorsesAuto($to_owner) {

		$sql =("SELECT official_name,horseseq FROM horse1 WHERE horse_status = 1");
		if($to_owner != ''){
			$sql .= " AND `official_name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		$sql .= " ORDER BY `official_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	
}
