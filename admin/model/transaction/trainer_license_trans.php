<?php
class ModelTransactionTrainerLicenseTrans extends Model {
	public function addTrainerTrans($data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$last_update_date = $date->format('Y-m-d h:i:s');
		$season_start = date('Y-m-d', strtotime($data['season_start']));
		$season_end = date('Y-m-d', strtotime($data['season_end']));
		foreach ($data['trainer_datas'] as $jkey => $jvalue) {
			if(isset($jvalue['selected_trainer'])){
				$apprenties = isset($jvalue['apprenties']) ? "Yes" : "No";
				$this->db->query("INSERT INTO `trainer_renewal_history` SET 
					trainer_id = '".$jvalue['trainer_id']."',
					trainer_name = '".$jvalue['trainer_name']."',
					no_horse = '".$jvalue['no_horse']."',
					amount = '".$jvalue['amt']."',
					renewal_date = '". date('Y-m-d', strtotime($jvalue['lisence_end_date'])) ."',
					license_start_date = '". date('Y-m-d', strtotime($jvalue['lisence_start_date'])) ."',
					license_end_date = '". date('Y-m-d', strtotime($jvalue['lisence_end_date'])) ."',
					license_type ='". $jvalue['lisence_type'] ."',
					apprenties = '". $apprenties ."',
					fees = '". $jvalue['fees_type'] ."',
					season_start = '".$season_start."',
					season_end = '".$season_end."',
					entry_status = '1',
					`user_id` = '".$this->session->data['user_id']."',
					`last_update_date` = '".$last_update_date."'
				");
				$this->db->query("UPDATE trainers SET
				`license_type` = '". $jvalue['lisence_type'] ."' WHERE id = '" . $jvalue['trainer_id'] . "'
		    	");
			}
		}
	}
	public function getTrainersDatas($data = array()) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$sql = "SELECT * FROM trainers";

		$sql .= " WHERE `active` = 1";

		if (!empty($data['filter_trainer'])) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_trainer']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
}
