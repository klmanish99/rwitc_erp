<?php
class ModelTransactionacceptance extends Model {
	public function addProspectus($data) {
		$this->db->query("INSERT INTO prospectus SET  
			`race_date` ='" . $this->db->escape(date('Y-m-d', strtotime($data['race_date']))) . "',
			`season` = '" . $this->db->escape($data['season']) . "',
			`year` = '" . $this->db->escape($data['year']) . "',
			`race_day` = '" . $this->db->escape($data['race_day']) . "',
			`evening_race` = '" . $this->db->escape($data['evening_race']) . "',
			`race_name` = '" . $this->db->escape($data['race_name']) . "',
			`race_type` = '" . $this->db->escape($data['race_type']) . "',
			`class` = '" . $this->db->escape($data['class']) . "',
			`grade` = '" . $this->db->escape($data['grade']) . "',
			`race_category` = '" . $this->db->escape($data['race_category']) . "',
			`distance` = '" . $this->db->escape($data['distance']) . "',
			`lower_class_aligible` = '" . $this->db->escape($data['lower_class_aligible']) . "',
			`entries_close_date` = '" . $this->db->escape(date('Y-m-d', strtotime($data['entries_close_date']))) . "',
			`entries_close_date_time` = '" . $this->db->escape($data['entries_close_date_time']) . "',
			`handicaps_date` ='" . $this->db->escape(date('Y-m-d', strtotime($data['handicaps_date']))) . "',
			`handicaps_date_time` = '" . $this->db->escape($data['handicaps_date_time']) . "',
			`acceptance_date` = '" . $this->db->escape(date('Y-m-d', strtotime($data['acceptance_date']))) . "',
			`acceptance_date_time` = '" . $this->db->escape($data['acceptance_date_time']) . "',
			`declaration_date` ='" . $this->db->escape(date('Y-m-d', strtotime($data['declaration_date']))) . "',
			`declaration_date_time` = '" . $this->db->escape($data['declaration_date_time']) . "'
			
		");

		$pros_id = $this->db->getLastId();


		$this->db->query("INSERT INTO prospectus_race_eligibility_criteria SET
			`pros_id` = '" . (int)$pros_id . "',
			`age_from` = '" . $this->db->escape($data['age_from']) . "',
			`age_to` = '" . $this->db->escape($data['age_to']) . "',
			`max_win` = '" . $this->db->escape($data['max_win']) . "',
			`sex` = '" . $this->db->escape($data['sex']) . "',
			`maidens` = '" . $this->db->escape($data['maidens']) . "',
			`unraced` = '" . $this->db->escape($data['unraced']) . "',
			`not1_2_3` = '" . $this->db->escape($data['not1_2_3']) . "',
			`unplaced` = '" . $this->db->escape($data['unplaced']) . "',
			`short_not` = '" . $this->db->escape($data['short_not']) . "',
			`max_stakes_race` = '" . $this->db->escape($data['max_stakes_race']) . "',
			`run_thrice` = '" . $this->db->escape($data['run_thrice']) . "',
			`run_thrice_date` = '" . $this->db->escape(date('Y-m-d', strtotime($data['run_thrice_date']))) . "',
			`run_not_placed` = '" . $this->db->escape($data['run_not_placed']) . "',
			`run_not_placed_date` = '" . $this->db->escape(date('Y-m-d', strtotime($data['run_not_placed_date']))) . "',
			`run_not_won` = '" . $this->db->escape($data['run_not_won']) . "',
			`run_not_won_date` ='" . $this->db->escape(date('Y-m-d', strtotime($data['run_not_won_date']))) . "',

			`run_and_not_won_during_mtg` = '" . $this->db->escape($data['run_and_not_won_during_mtg']) . "',
			`run_and_not_won_during_mtg_date` ='" . $this->db->escape(date('Y-m-d', strtotime($data['run_and_not_won_during_mtg_date']))) . "',
			`no_whip` = '" . $this->db->escape($data['no_whip']) . "',
			`foreign_jockey_eligible` = '" . $this->db->escape($data['foreign_jockey_eligible']) . "',
			`foreign_horse_eligible` = '" . $this->db->escape($data['foreign_horse_eligible']) . "',
			`sire_dam` = '" . $this->db->escape($data['sire_dam']) . "'
			");

		$this->db->query("INSERT INTO race_stakes_details SET
			`pros_id` = '" . (int)$pros_id . "',
			`stakes_in` = '" . $this->db->escape($data['stakes_in']) . "',
			`trophy_value_amount` = '" . $this->db->escape($data['trophy_value_amount']) . "',
			`positions` = '" . $this->db->escape($data['positions']) . "',
			`stakes_money` = '" . $this->db->escape($data['stakes_money']) . "',
			`bonus_applicable` = '" . $this->db->escape($data['bonus_applicable']) . "',
			`sweepstake_race` = '" . $this->db->escape($data['sweepstake_race']) . "',
			`sweepstake_of` = '" . $this->db->escape($data['sweepstake_of']) . "',
			`no_of_forfiet` = '" . $this->db->escape($data['no_of_forfiet']) . "',
			`entry_date` ='" . $this->db->escape($data['entry_date']) . "',
			`entry_time` = '" . $this->db->escape($data['entry_time']) . "',
			`entry_fees` = '" . $this->db->escape($data['entry_fees']) . "',
			`additional_entry` = '" . $this->db->escape($data['additional_entry']) . "',
			`bonus_to_breeder` = '" . $this->db->escape($data['bonus_to_breeder']) . "',
			`remarks` = '" . $this->db->escape($data['remarks']) . "'
			");


		if(isset($data['race'])){
			foreach ($data['race'] as $key => $value) {
				$this->db->query("INSERT INTO prospectus_prize_distribution SET
				pros_id = '".(int)$pros_id."',
				placing = '".$this->db->escape($value['placing'])."',
				owner_per = '".$this->db->escape($value['owner_per'])."',
				owner = '".$this->db->escape($value['owner'])."',
				trainer_perc = '".$this->db->escape($value['trainer_perc'])."' ,
				trainer = '".$this->db->escape($value['trainer'])."',
				jockey_perc = '".$this->db->escape($value['jockey_perc'])."',
				jockey = '".$this->db->escape($value['jockey'])."' ");
			}
		}

		return $pros_id;
	}
	

	public function editProspectus($pros_id,$data) {
		
		$this->db->query("DELETE FROM  entry WHERE pros_id='".$pros_id."' ");
		$this->db->query("INSERT INTO entry SET
			pros_id = '".(int)$pros_id."',
			count = '".$this->db->escape($data['count'])."',
			race_description = '".$this->db->escape($data['race_description'])."'
			");
		$entry_id = $this->db->getLastId();


		$this->db->query("DELETE FROM entry_horse WHERE pros_id='".$pros_id."' ");
		if(isset($data['selecthors'])){
			foreach ($data['selecthors'] as $key => $value) {
				$this->db->query("INSERT INTO entry_horse SET
				pros_id = '".(int)$pros_id."',
				trainer_id ='".$this->db->escape($value['trainer_id'])."',
				entry_id = '".$entry_id."',
				horse_name = '".$this->db->escape($value['horse_name'])."',
				horse_id = '".$this->db->escape($value['horse_id'])."',
				horse_rating = '".$this->db->escape($value['horse_rating'])."',
				horse_age = '".$this->db->escape($value['horse_age'])."' ,
				horse_sex = '".$this->db->escape($value['horse_sex'])."',
				trainer_name = '".$this->db->escape($value['trainer_name'])."'
				 ");
			}
		}
	}

	public function rating($class1,$top_rating,$bot_rating) {

		$result =array();
		$res ='';
		$res_p = '';
		$query = $this->db->query("SELECT Weight FROM horse_weight WHERE Class ='".$class1."' AND  	Rating ='".$top_rating."'");
		//$top_weight = $query->row['Weight'];
		if($query->num_rows > 0){
			$top_weight = $query->row['Weight'];
		} else {
			$top_weight = 0;
		}
		$query1 = $this->db->query("SELECT Weight FROM horse_weight WHERE Class ='".$class1."' AND  	Rating ='".$bot_rating."'");
		//$bot_weight = $query1->row['Weight'];
		if($query1->num_rows > 0){
			$bot_weight = $query1->row['Weight'];
		} else {
			$bot_weight= 0;
		}
		if(59 <= $top_weight &&  62 >= $top_weight){
			if(50 < $bot_weight ){
				$res = 50 - $bot_weight;
				$res1=$top_weight + $res;
				if($res1 < 59 ){
					$aa= 59 - $res1;
					$res = $res + $aa;
				}
			}
		}else if(59 >  $top_weight){
			$res_p = 59 - $top_weight;
			
		}
		else{
		}
		$result['res_n'] = $res;
		$result['res_p'] = $res_p;
		
		return $result;
	}

	public function deleteprospectus($pros_id) {
		/*$this->db->query("DELETE FROM `prospectus` WHERE pros_id = '" . (int)$pros_id . "'");
		$this->db->query("DELETE FROM `prospectus_race_eligibility_criteria` WHERE pros_id = '" . (int)$pros_id . "'");
		$this->db->query("DELETE FROM `race_stakes_details` WHERE pros_id = '" . (int)$pros_id . "'");
		$this->db->query("DELETE FROM  prospectus_prize_distribution WHERE pros_id='".$pros_id."' ");*/

	}
	public function getentrydatas($pros_id) {
		$query = $this->db->query("SELECT * FROM entry WHERE pros_id ='".$pros_id."'");
		return $query->row;
	}
	public function getentrydatas1($pros_id) {
		$query = $this->db->query("SELECT * FROM  entry_horse WHERE pros_id ='".$pros_id."'");
		return $query->rows;
	}

	public function getentrydata_id($pros_id) {
		$query = $this->db->query("SELECT horse_id FROM  entry_horse WHERE pros_id ='".$pros_id."'");
		return $query->rows;
	}


	public function getpopupdata($pros_id) {
		$sql="SELECT * FROM entry_horse WHERE pros_id='".$pros_id."'";
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getracenamedesc($pros_id) {
		$sql="SELECT * FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) WHERE pros_id='".$pros_id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}


	/*public function getpopupdata() {
		$query = $this->db->query("SELECT * FROM entry_horse WHERE 1=1");
		return $query->rows;
	}*/


	public function getHorses($age_from,$age_to,$sex) {

		$sql =("SELECT * FROM `horse1` LEFT JOIN horse_to_trainer ON horse1.horseseq =horse_to_trainer.horse_id WHERE 1=1");

		if ($age_from !='') {
			$sql .= " AND age  >=" . $age_from ;
		}
		if ($age_to  != '') {
			$sql .= " AND age  <=" .$age_to;
		}
		if ($sex != '') {
			$sql .= " AND sex  ='". $sex ."'";
		}
		

		/*$sql =("SELECT * FROM `horse1` LEFT JOIN horse_to_trainer ON horse1.horseseq =horse_to_trainer.horse_id WHERE horse1.age >='".$age_from."' AND age <='".$age_to."' AND sex = '".$sex."'");*/
		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getprospectus($data) {
		
		$sql =("SELECT * FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id)  WHERE entry.entry_id <> ''  AND prospectus.race_status !=0   ");
		//echo $sql;

		// if (!empty($data['filter_race_name'])) {
		// 	$sql .= " AND LOWER(race_name) LIKE '%" . $this->db->escape(strtolower($data['filter_race_name'])) . "%' ";
		// }
		if (!empty($data['filter_acceptance_date'])) {
			$sql .= " AND LOWER(acceptance_date) LIKE '%" . $this->db->escape(date('Y-m-d', strtotime($data['filter_acceptance_date']))) . "%'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getCategory($pros_id) {  
		$query = $this->db->query("SELECT * FROM prospectus LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id) LEFT JOIN race_stakes_details ON(prospectus.pros_id =race_stakes_details.pros_id) WHERE prospectus.pros_id ='".$pros_id."'");
		return $query->row;
	}


	public function getprizedistribution($pros_id) {  
		$query = $this->db->query("SELECT * FROM prospectus_prize_distribution WHERE pros_id='".$pros_id."' ORDER BY ppd_id ");
		return $query->rows;
	}

	public function getTotalCategories($data) {
		

		$query =("SELECT  COUNT(*) AS total FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id)  WHERE entry.entry_id <> ''  AND prospectus.race_status !=0   ");

		//$query =("SELECT COUNT(*) AS total FROM prospectus LEFT JOIN entry ON prospectus.pros_id =entry.pros_id WHERE entry.entry_id ");

		// if (!empty($data['filter_race_name'])) {
		// 	$query .= " AND LOWER(race_name) LIKE '%" . $this->db->escape(strtolower($data['filter_race_name'])) . "%' ";
		// }

		if (!empty($data['filter_acceptance_date'])) {
			$query .= " AND LOWER(race_date) LIKE '%" . $this->db->escape(date('Y-m-d', strtotime($data['filter_acceptance_date']))) . "%'";
		}

		$sql = $this->db->query($query);
		return $sql->row['total'];
		
	}
	
	public function getTotalCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}

	/*public function getallhandicapreport() {
		$sql =("SELECT * FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id)  WHERE entry.entry_id ");
		
		$query = $this->db->query($sql)->rows;
		return $query;
	}*/
}
