<?php
class ModelTransactionregisterationmodule extends Model {
	
	public function rating($class1,$top_rating,$bot_rating) {

		$result =array();
		$res ='';
		$res_p = '';
		$query = $this->db->query("SELECT Weight FROM horse_weight WHERE Class ='".$class1."' AND  	Rating ='".$top_rating."'");
		//$top_weight = $query->row['Weight'];
		if($query->num_rows > 0){
			$top_weight = $query->row['Weight'];
		} else {
			$top_weight = 0;
		}
		$query1 = $this->db->query("SELECT Weight FROM horse_weight WHERE Class ='".$class1."' AND  	Rating ='".$bot_rating."'");
		//$bot_weight = $query1->row['Weight'];
		if($query1->num_rows > 0){
			$bot_weight = $query1->row['Weight'];
		} else {
			$bot_weight= 0;
		}
		if(59 <= $top_weight &&  62 >= $top_weight){
			if(50 < $bot_weight ){
				$res = 50 - $bot_weight;
				$res1=$top_weight + $res;
				if($res1 < 59 ){
					$aa= 59 - $res1;
					$res = $res + $aa;
				}
			}
		}else if(59 >  $top_weight){
			$res_p = 59 - $top_weight;
			
		}
		else{
		}
		$result['res_n'] = $res;
		$result['res_p'] = $res_p;
		
		return $result;
	}

	public function insert_registration($data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if (isset($data['modification'])) {
			$modification = '1';
		} else {
			$modification = '2';
		}
		
		$regis_date = date('Y-m-d', strtotime($data['reg_date']));
		$letter_date = date('Y-m-d', strtotime($data['letter_date']));
		$letter_received = date('Y-m-d', strtotime($data['letter_received']));
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$last_update_date = $date->format('Y-m-d h:i:s');
		$print_latters = isset($data['print_latters']) ? 'Yes' : 'No';

		

		$exist_horse = $this->db->query("SELECT id FROM `oc_registration_module` WHERE horse_id = '" . (int)$data['filterHorseId'] . "'");
		if($exist_horse->num_rows > 0){
			$this->db->query("INSERT INTO `oc_registration_module` SET `horse_id` = '" . $data['filterHorseId'] . "', `modification` = '".$modification."', `horse_name` = '" . $data['horse_name'] . "' , `registeration_type` = '" . $data['registeration_type'] . "', `registeration_fee` = '" . (int)$data['registration_fee'] . "' , `remark` = '" . $data['remark_regi'] . "', `print_letters` = '".$print_latters."', `registration_date` = '".$regis_date."', `letter_dated` = '".$letter_date."', `letter_received` = '".$letter_received."',`user_id` = '".$this->session->data['user_id']."',`last_update_date` = '".$last_update_date."' ");
			$parent_id = $this->db->getLastId();
			
			$this->db->query("UPDATE horse1 SET official_name = '".$data['horse_name']."' WHERE horseseq = '" . (int)$data['filterHorseId'] . "' ");
		} else {
			$this->db->query("INSERT INTO `oc_registration_module` SET `horse_id` = '" . $data['filterHorseId'] . "', `modification` = '".$modification."', `horse_name` = '" . $data['filterHorseName'] . "' , `registeration_type` = 'ISB' , `registration_date` = '".$regis_date."'");

			$this->db->query("INSERT INTO `oc_registration_module` SET `horse_id` = '" . $data['filterHorseId'] . "', `modification` = '".$modification."', `horse_name` = '" . $data['horse_name'] . "' , `registeration_type` = '" . $data['registeration_type'] . "', `registeration_fee` = '" . (int)$data['registration_fee'] . "' , `remark` = '" . $data['remark_regi'] . "', `print_letters` = '".$print_latters."', `registration_date` = '".$regis_date."', `letter_dated` = '".$letter_date."', `letter_received` = '".$letter_received."',`user_id` = '".$this->session->data['user_id']."',`last_update_date` = '".$last_update_date."' ");

			$parent_id = $this->db->getLastId();

			
			$this->db->query("UPDATE horse1 SET official_name = '".$data['horse_name']."' WHERE horseseq = '" . (int)$data['filterHorseId'] . "' ");
			
		}

		if(isset($data['registeration_type']) && ($data['registeration_type'] == 'rename')){
			$this->db->query("UPDATE horse1 SET date_of_changehorse_name = '".date('Y-m-d')."' ,official_name_change_status='1' WHERE horseseq = '" . (int)$data['filterHorseId'] . "' ");
		} else {
			$this->db->query("UPDATE horse1 SET date_of_changehorse_name = '".date('Y-m-d')."',reg_date = '".$regis_date."',official_name_change_status='1' WHERE horseseq = '" . (int)$data['filterHorseId'] . "' ");
		}


		if(isset($data['owner_datas'])){
			foreach ($data['owner_datas'] as $okey => $ovalue) {
				$amt = ($data['registration_fee'] * $ovalue['owner_percentage'] / 100); 

				// $exist_owners = $this->db->query("SELECT id FROM `oc_registration_module_owners` WHERE horse_id = '" . (int)$data['filterHorseId'] . "'  AND owner_id ='".$ovalue['to_owner_hidden']."'  ");
				// if($exist_owners->num_rows > 0){
				// 	$this->db->query("UPDATE `oc_registration_module_owners` SET owner_share = '".$ovalue['owner_percentage']."' , charge_amount = '".$amt."' WHERE id = '" . (int)$exist_owners->row['id'] . "' ");

				// } else{
					$this->db->query("INSERT INTO `oc_registration_module_owners` SET
				 	`horse_id` = '" . $data['filterHorseId'] . "',
				 	`parent_id` = '".$parent_id."', 
				 	`owner_id` = '" . $ovalue['to_owner_hidden'] . "' ,
				 	`owner_name` = '" . $ovalue['to_owner_name_hidden'] . "',
				 	`owner_share` = '" . $ovalue['owner_percentage'] . "',
				 	`charge_amount` = '" . (int)$amt . "'  ");
				//}
				# code...
			}

		}





		
		return $data['filterHorseId'];

	}
	public function getdetails($last_id) {
		$query = $this->db->query("SELECT * FROM oc_registration_module WHERE horse_id ='".$last_id."'");
		return $query->row;
	}
	public function getRegiserationmodule($data) {
		$query = $this->db->query("SELECT * FROM oc_registration_module WHERE horse_id ='".$data['horse_id']."'");
		return $query->rows;
	}
	public function getentrydatas1($pros_id) {
		$query = $this->db->query("SELECT * FROM  entry_horse WHERE pros_id ='".$pros_id."'");
		return $query->rows;
	}

	public function getentrydata_id($pros_id) {
		$query = $this->db->query("SELECT horse_id FROM  entry_horse WHERE pros_id ='".$pros_id."'");
		return $query->rows;
	}


	public function getpopupdata($pros_id) {
		$sql="SELECT * FROM entry_horse WHERE pros_id='".$pros_id."'";
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getracenamedesc($pros_id) {
		$sql="SELECT * FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) WHERE pros_id='".$pros_id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}


	/*public function getpopupdata() {
		$query = $this->db->query("SELECT * FROM entry_horse WHERE 1=1");
		return $query->rows;
	}*/


	public function getHorses($age_from,$age_to,$sex) {

		$sql =("SELECT * FROM `horse1` LEFT JOIN horse_to_trainer ON horse1.horseseq =horse_to_trainer.horse_id WHERE 1=1");

		if ($age_from !='') {
			$sql .= " AND age  >=" . $age_from ;
		}
		if ($age_to  != '') {
			$sql .= " AND age  <=" .$age_to;
		}
		if ($sex != '') {
			$sql .= " AND sex  ='". $sex ."'";
		}
		

		/*$sql =("SELECT * FROM `horse1` LEFT JOIN horse_to_trainer ON horse1.horseseq =horse_to_trainer.horse_id WHERE horse1.age >='".$age_from."' AND age <='".$age_to."' AND sex = '".$sex."'");*/
		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getprospectus($data) {
		
		$sql =("SELECT * FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id)  WHERE entry.entry_id <> ''  AND prospectus.race_status !=0   ");
		//echo $sql;

		// if (!empty($data['filter_race_name'])) {
		// 	$sql .= " AND LOWER(race_name) LIKE '%" . $this->db->escape(strtolower($data['filter_race_name'])) . "%' ";
		// }
		if (!empty($data['filter_acceptance_date'])) {
			$sql .= " AND LOWER(acceptance_date) LIKE '%" . $this->db->escape(date('Y-m-d', strtotime($data['filter_acceptance_date']))) . "%'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getCategory($pros_id) {  
		$query = $this->db->query("SELECT * FROM prospectus LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id) LEFT JOIN race_stakes_details ON(prospectus.pros_id =race_stakes_details.pros_id) WHERE prospectus.pros_id ='".$pros_id."'");
		return $query->row;
	}


	public function getprizedistribution($pros_id) {  
		$query = $this->db->query("SELECT * FROM prospectus_prize_distribution WHERE pros_id='".$pros_id."' ORDER BY ppd_id ");
		return $query->rows;
	}

	public function getTotalCategories($data) {
		

		$query =("SELECT  COUNT(*) AS total FROM `prospectus` LEFT JOIN entry ON(prospectus.pros_id =entry.pros_id) LEFT JOIN prospectus_race_eligibility_criteria ON(prospectus.pros_id =prospectus_race_eligibility_criteria.pros_id)  WHERE entry.entry_id <> ''  AND prospectus.race_status !=0   ");

		//$query =("SELECT COUNT(*) AS total FROM prospectus LEFT JOIN entry ON prospectus.pros_id =entry.pros_id WHERE entry.entry_id ");

		// if (!empty($data['filter_race_name'])) {
		// 	$query .= " AND LOWER(race_name) LIKE '%" . $this->db->escape(strtolower($data['filter_race_name'])) . "%' ";
		// }

		if (!empty($data['filter_acceptance_date'])) {
			$query .= " AND LOWER(race_date) LIKE '%" . $this->db->escape(date('Y-m-d', strtotime($data['filter_acceptance_date']))) . "%'";
		}

		$sql = $this->db->query($query);
		return $sql->row['total'];
		
	}
	
	public function getTotalCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}

	public function gethorsetoTrainer($data) {

		$sql =("SELECT trainer_id,trainer_code,trainer_name FROM `horse_to_trainer` WHERE trainer_status = 1");

		if (!empty($data['horse_id'])) {
			$sql .= " AND `horse_id` = '" . ($data['horse_id']) . "' ";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function gethorsetoOwner($data) {

		$sql =("SELECT * FROM `horse_to_owner` WHERE `owner_share_status` = 1 AND provisional_ownership = 'No' ");

		if (!empty($data['horse_id'])) {
			$sql .= " AND `horse_id` = '" . ($data['horse_id']) . "' ";
		}

		// if (!empty($data['ownership_type_id']) && ($data['ownership_type_id'] == 'Lease' || $data['ownership_type_id'] == 'Sale')) {
		// 	$sql .= " AND `ownership_type` = 'Sale' ";
		/* if($data['ownership_type_id'] == 'Sub Lease'){
			$sql .= " AND `ownership_type` = 'Lease' ";
		}*/

		$sql .= " ORDER BY horse_to_owner_order ASC";


		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function gethorsetoOwnerprovi($data) {
		$date_added = date('Y-m-d');

		$sql =("SELECT * FROM `horse_to_owner` WHERE `owner_share_status` = 1 AND provisional_ownership = 'Yes' AND date_added = '".$date_added."' ");

		if (!empty($data['horse_id'])) {
			$sql .= " AND `horse_id` = '" . ($data['horse_id']) . "' ";
		}

		// if (!empty($data['ownership_type_id']) && ($data['ownership_type_id'] == 'Lease' || $data['ownership_type_id'] == 'Sale')) {
		// 	$sql .= " AND `ownership_type` = 'Sale' ";
		/* if($data['ownership_type_id'] == 'Sub Lease'){
			$sql .= " AND `ownership_type` = 'Lease' ";
		}*/

		$sql .= " ORDER BY horse_to_owner_order ASC";


		$query = $this->db->query($sql)->rows;
		return $query;
	}


	public function getAllOwners($data){
		$sql =("SELECT * FROM `owners` WHERE `active` = 1");

		if (!empty($data['owner_name'])) {
			$sql .= " AND `owner_name` LIKE '%" . ($data['owner_name']) . "%' ";
		}

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getHorsesAuto($to_owner) {

		$sql =("SELECT official_name,horseseq, color, sex,foal_date, sire_name, dam_name, sire_nationality, dam_nationality FROM horse1 WHERE horse_status = 1 ");
		if($to_owner != ''){
			$sql .= " AND `official_name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		$sql .= " ORDER BY `official_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getregdatas($data = array()) {
		//echo "<pre>"; print_r($data);exit;
		$sql = ("SELECT * FROM `oc_registration_module` WHERE horse_id = '".$this->db->escape($data['id'])."' AND registeration_type = '".$this->db->escape($data['type'])."' ");

		//echo "<pre>"; print_r($sql);exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}

}
