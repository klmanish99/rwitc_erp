<?php 
class ModelTransactionhorsedataslink extends Model {
	public function gettrainer($data = array()) {

		$sql = ("SELECT * FROM `horse1` h LEFT JOIN horse_to_trainer ht ON h.horseseq =ht.horse_id WHERE h.horseseq = '".$data['hourse_id']."' ");
		$query = $this->db->query($sql)->row;
		return $query;
		
	}

	public function getowner($data = array()) {
		//echo "<pre>";print_r($data);exit;
		if ($data['provi_owner'] == 1) {
			$sql = ("SELECT * FROM `horse_to_owner` WHERE `horse_id` = '".$data['hourse_id']."' AND `owner_share_status` = '1' AND provisional_ownership = 'Yes' ORDER BY owner_percentage DESC ");
		} else {
			$sql = ("SELECT * FROM `horse_to_owner` WHERE `horse_id` = '".$data['hourse_id']."' AND `owner_share_status` = '1' AND provisional_ownership = 'No' ORDER BY owner_percentage DESC ");
		}

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function updateOwnerDatas($data = array()) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		if ($data['owner_color'] == 2) {
			$this->db->query("UPDATE `horse_to_owner` SET  owner_color = 'join' WHERE horse_id = '" .$data['filterHorseId'] . "' AND owner_share_status = '1' ");
			foreach ($data['owner_datas'] as $okey => $ovalue) {
				$owner_color = 'join';

				// echo'<pre>';
				// print_r($owner_color);
				// exit;
				$this->db->query("UPDATE `horse_to_owner` SET  owner_color = '".$this->db->escape($owner_color)."' , horse_to_owner_order = '".$ovalue['owner_order']."' WHERE horse_to_owner_id = '" .(int)$ovalue['horse_to_owner_id'] . "'");
				$this->log->write("UPDATE `horse_to_owner` SET  owner_color = '".$this->db->escape($owner_color)."' , horse_to_owner_order = '".$ovalue['owner_order']."' WHERE horse_to_owner_id = '" .(int)$ovalue['horse_to_owner_id'] . "'");
			}
		} elseif(isset($data['owner_datas'])){
			$this->db->query("UPDATE `horse_to_owner` SET  owner_color = 'No' WHERE horse_id = '" .$data['filterHorseId'] . "' AND owner_share_status = '1' ");
			foreach ($data['owner_datas'] as $okey => $ovalue) {
				$owner_color = 'No';
				if($ovalue['horse_color_selected'] != 0 && $ovalue['horse_color_selected'] != ''){
					$owner_color = 'Yes';
				} 

				// echo'<pre>';
				// print_r($owner_color);
				// exit;
				$this->db->query("UPDATE `horse_to_owner` SET  owner_color = '".$this->db->escape($owner_color)."' , horse_to_owner_order = '".$ovalue['owner_order']."' WHERE horse_to_owner_id = '" .(int)$ovalue['horse_to_owner_id'] . "'");
				$this->log->write("UPDATE `horse_to_owner` SET  owner_color = '".$this->db->escape($owner_color)."' , horse_to_owner_order = '".$ovalue['owner_order']."' WHERE horse_to_owner_id = '" .(int)$ovalue['horse_to_owner_id'] . "'");
			}
			
		}

	}
	public function getsupplier($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM colors";
			
		$sql .= " WHERE 1=1";
		
		if (!empty($data['filter_join_color'])) {
			$sql .= " AND color LIKE '%" . $this->db->escape($data['filter_join_color']) . "%'";
		}

		$sort_data = array(
			'id',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	
}