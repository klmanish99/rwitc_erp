<?php 
class ModelTransactioncancelownership extends Model {
	public function gethorsetoTrainer($data) {

		$sql =("SELECT trainer_id,trainer_code,trainer_name FROM `horse_to_trainer` WHERE 1=1");

		if (!empty($data['horse_id'])) {
			$sql .= " AND `horse_id` = '" . ($data['horse_id']) . "' ";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function gethorsetoOwner($data) {

		$sql =("SELECT * FROM `horse_to_owner` WHERE `owner_share_status` = 1 AND provisional_ownership = 'No' ");

		if (!empty($data['horse_id'])) {
			$sql .= " AND `horse_id` = '" . ($data['horse_id']) . "' ";
		}

		$sql .= " ORDER BY horse_to_owner_order ASC";

		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}


	public function getAllOwners($data){
		$sql =("SELECT * FROM `owners` WHERE `active` = 1");

		if (!empty($data['owner_name'])) {
			$sql .= " AND `owner_name` LIKE '%" . ($data['owner_name']) . "%' ";
		}

		$query = $this->db->query($sql)->rows;
		return $query;
	}
}