<?php
class ModelTransactionstacks extends Model {
	public function addstacks($data) {

		// echo "<pre>";
		// print_r($data);
		// exit;

		if($this->db->escape($data['entries_close_date']) != ''){
			$entries_close_date = $this->db->escape(date('Y-m-d', strtotime($data['entries_close_date'])));
		}else{
			$entries_close_date = '0000-00-00';
		}

		if($this->db->escape($data['handicaps_date']) != ''){
			$handicaps_date = $this->db->escape(date('Y-m-d', strtotime($data['handicaps_date'])));
		}else{
			$handicaps_date = '0000-00-00';
		}
		if($this->db->escape($data['acceptance_date']) != ''){
			$acceptance_date = $this->db->escape(date('Y-m-d', strtotime($data['acceptance_date'])));
		}else{
			$acceptance_date = '0000-00-00';
		}
		if($this->db->escape($data['declaration_date']) != ''){
			$declaration_date = $this->db->escape(date('Y-m-d', strtotime($data['declaration_date'])));
		}else{
			$declaration_date = '0000-00-00';
		}
		if ($data['race_type'] == 'Handicap Race') {
			$data['race_status'] = '1';
		} 
		else if ($data['race_type'] == 'Term Race') {
			$data['race_status'] = '2';
		} else {
			$data['race_status'] = '';
		}

		$race_class_type = ($data['class'] != '' && $data['class'] != '0') ? $data['class'] : $data['grade'];

		$this->db->query("INSERT INTO stacks SET  
			`race_type` = '" . $this->db->escape($data['race_type']) . "',
			`class` = '" . $this->db->escape($race_class_type) . "',
			`race_status` = '" . $this->db->escape($data['race_status']) . "'
		");

		$stacks_id = $this->db->getLastId();

		$this->db->query("DELETE FROM `stacks_value` WHERE `stacks_id` = '".$stacks_id."' ");
		if(isset($data['contact_datas'])){
			foreach($data['contact_datas'] as $ckey => $cvalue){
				$this->db->query("INSERT INTO `stacks_value` SET
						stacks_type = '" . $this->db->escape($cvalue['name']) . "',
						owner = '" . $this->db->escape($cvalue['owner']) . "',
						trainer = '" . $this->db->escape($cvalue['trainer']) . "',
						jockey = '" . $this->db->escape($cvalue['jockey']) . "',
						total  = '" . $this->db->escape($cvalue['total']) . "',
						incentive = '" . $this->db->escape($cvalue['incentive']) . "',
						stacks_id = '" . $this->db->escape($stacks_id) . "'
						");
			}
		}

		return $stacks_id;
	}
	

	public function editstacks($stacks_id,$data) {
		if($this->db->escape($data['entries_close_date']) != ''){
			$entries_close_date = $this->db->escape(date('Y-m-d', strtotime($data['entries_close_date'])));
		}else{
			$entries_close_date = '0000-00-00';
		}

		if($this->db->escape($data['handicaps_date']) != ''){
			$handicaps_date = $this->db->escape(date('Y-m-d', strtotime($data['handicaps_date'])));
		}else{
			$handicaps_date = '0000-00-00';
		}
		if($this->db->escape($data['acceptance_date']) != ''){
			$acceptance_date = $this->db->escape(date('Y-m-d', strtotime($data['acceptance_date'])));
		}else{
			$acceptance_date = '0000-00-00';
		}
		if($this->db->escape($data['declaration_date']) != ''){
			$declaration_date = $this->db->escape(date('Y-m-d', strtotime($data['declaration_date'])));
		}else{
			$declaration_date = '0000-00-00';
		}
		if ($data['race_type'] == 'Handicap Race') {
			$data['race_status'] = '1';
		} 
		else if ($data['race_type'] == 'Term Race') {
			$data['race_status'] = '2';
		} else {
			$data['race_status'] = '';
		}
		$race_class_type = ($data['class'] != '' && $data['class'] != '0') ? $data['class'] : $data['grade'];
		
		
		$this->db->query("UPDATE stacks SET
			`race_type` = '" . $this->db->escape($data['race_type']) . "',
			`class` = '" . $this->db->escape($race_class_type) . "',
			`race_status` = '" . $this->db->escape($data['race_status']) . "'
			WHERE stacks_id='".$stacks_id."'
			");

		$this->db->query("DELETE FROM `stacks_value` WHERE `stacks_id` = '".$stacks_id."' ");
		if(isset($data['contact_datas'])){
			foreach($data['contact_datas'] as $ckey => $cvalue){
				$this->db->query("INSERT INTO `stacks_value` SET
						stacks_type = '" . $this->db->escape($cvalue['name']) . "',
						owner = '" . $this->db->escape($cvalue['owner']) . "',
						trainer = '" . $this->db->escape($cvalue['trainer']) . "',
						jockey = '" . $this->db->escape($cvalue['jockey']) . "',
						total = '" . $this->db->escape($cvalue['total']) . "',
						incentive = '" . $this->db->escape($cvalue['incentive']) . "',
						stacks_id = '" . $this->db->escape($stacks_id) . "'
						");
			}
		}

		
	}

	public function deletestacks($stacks_id) {
		$this->db->query("DELETE FROM `stacks` WHERE stacks_id = '" . (int)$stacks_id . "'");
	}


	public function getstacks($data) {
		
		$sql =("SELECT * FROM `stacks`  WHERE 1=1 ");

		// if (!empty($data['filter_race_name'])) {
		// 	$sql .= " AND LOWER(race_name) LIKE '%" . $this->db->escape(strtolower($data['filter_race_name'])) . "%' ";
		// }
		// if (!empty($data['filter_race_date'])) {
		// 	$sql .= " AND LOWER(race_date) LIKE '%" . $this->db->escape(date('Y-m-d', strtotime($data['filter_race_date']))) . "%'";
		// }

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getCategory($stacks_id) {  
		$query = $this->db->query("SELECT * FROM stacks WHERE stacks_id ='".$stacks_id."'");
		return $query->row;
	}


	public function getracename($stacks_id) {  
		$query = $this->db->query("SELECT * FROM stacks LEFT JOIN entry ON(stacks.stacks_id =entry.stacks_id) WHERE stacks_id='".$stacks_id."'");
		return $query->rows;
	}


	public function getprizedistribution($stacks_id) {  
		// $query = $this->db->query("SELECT * FROM stacks_prize_distribution WHERE stacks_id='".$stacks_id."' ORDER BY ppd_id ");
		// return $query->rows;
	}


	public function getRacenameauto($race_name) {

        $sql =("SELECT race_name FROM stacks WHERE 1 = 1");
        if($race_name != ''){
            $sql .= " AND `race_name` LIKE '%" . $this->db->escape($race_name) . "%'";
        }
        $sql .= " ORDER BY `race_name` ASC LIMIT 0, 100";
        
        $query = $this->db->query($sql)->rows;
        return $query;
    }


	public function getTotalCategories($data) {
		

		$query =("SELECT COUNT(*) AS total FROM stacks  WHERE 1=1 ");

		// if (!empty($data['filter_race_name'])) {
		// 	$query .= " AND LOWER(race_name) LIKE '%" . $this->db->escape(strtolower($data['filter_race_name'])) . "%' ";
		// }

		// if (!empty($data['filter_race_date'])) {
		// 	$query .= " AND LOWER(race_date) LIKE '%" . $this->db->escape(date('Y-m-d', strtotime($data['filter_race_date']))) . "%'";
		// }

		$sql = $this->db->query($query);
		return $sql->row['total'];
		
	}
	
	public function getTotalCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}	

	public function getcontact_data($stacks_id) {
		$query = $this->db->query("SELECT * FROM `stacks_value` WHERE `stacks_id` = '".$stacks_id."' ");
		return $query->rows;
	}
}
