<?php 
class ModelCatalogisbhorse extends Model {
	public function getIsbHorses($data = array()) {
		$sql = "SELECT * FROM foal_isb WHERE 1 = 1";

		/*if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_group'])) {
			$group = $data['filter_group'];
		} else {
			$group = 'week';
		}*/

  




		if (!empty($data['filtermare_name'])) {
			$sql .= " AND LOWER(MARENAME) LIKE '%" . $this->db->escape(strtolower($data['filtermare_name'])) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] != '-3') {
			$sql .= " AND `status` = '" . $data['filter_status'] . "' ";
		}

		if (isset($data['filtersire']) && $data['filtersire'] != '') {
			$sql .= " AND `SIRENAME` = '" . $data['filtersire'] . "' ";
		}

		if (isset($data['filterstudname']) && $data['filterstudname'] != '') {
			$sql .= " AND `STUDNAME` = '" . $data['filterstudname'] . "' ";
		}

		if (isset($data['year_of_foaling']) && $data['year_of_foaling'] != '0') {
			$sql .= " AND `YROFFLNG` = '" . $data['year_of_foaling'] . "' ";
		}


		$sql .= " ORDER BY MARENAME ASC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalIsbHorses($data = array()) { 
		$sql = ("SELECT COUNT(*) AS total FROM foal_isb WHERE 1 = 1");

		if (!empty($data['filtermare_name'])) {
			$sql .= " AND LOWER(MARENAME) LIKE '%" . $this->db->escape(strtolower($data['filtermare_name'])) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] != '-3') {
			$sql .= " AND `status` = '" . $data['filter_status'] . "' ";
		}

		if (isset($data['filtersire']) && $data['filtersire'] != '') {
			$sql .= " AND `SIRENAME` = '" . $data['filtersire'] . "' ";
		}

		if (isset($data['filterstudname']) && $data['filterstudname'] != '') {
			$sql .= " AND `STUDNAME` = '" . $data['filterstudname'] . "' ";
		}

		if (isset($data['year_of_foaling']) && $data['year_of_foaling'] != '0') {
			$sql .= " AND `YROFFLNG` = '" . $data['year_of_foaling'] . "' ";
		}



		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getHorsesIsbAuto($horse_name) {

		$sql =("SELECT * FROM foal_isb WHERE 1=1");
		if($horse_name != ''){
			$sql .= " AND `MARENAME` LIKE '%" . $this->db->escape($horse_name) . "%' OR `FOALNAME` LIKE '%" . $this->db->escape($horse_name) . "%'";
		}
		$sql .= " ORDER BY `MARENAME` ASC LIMIT 0, 15";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getstud_nameAuto($stud_name) {

		$sql =("SELECT DISTINCT STUDNAME FROM foal_isb WHERE 1=1");
		if($stud_name != ''){
			$sql .= " AND `STUDNAME` LIKE '%" . $this->db->escape($stud_name) . "%' ";
		}
		$sql .= " ORDER BY `STUDNAME` ASC LIMIT 0, 15";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getsire_namebAuto($sire_name) {

		$sql =("SELECT DISTINCT SIRENAME FROM foal_isb WHERE 1=1");
		if($sire_name != ''){
			$sql .= " AND `SIRENAME` LIKE '%" . $this->db->escape($sire_name) . "%' ";
		}
		$sql .= " ORDER BY `SIRENAME` ASC LIMIT 0, 15";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getyearfoaling($sire_name) {

		$sql =("SELECT DISTINCT YROFFLNG FROM foal_isb WHERE 1=1");

		$query = $this->db->query($sql)->rows;
		return $query;
	}



	
}