<?php
class ModelCatalogEquipments extends Model {
	public function addEquipments($data) {
		$this->db->query("INSERT INTO equipment SEt 
		equipment_name = '" . $this->db->escape($data['equipment_name']) . "',
		short_name ='".$this->db->escape($data['short_name'])."',
		equipment_status ='".$this->db->escape($data['equipment_status'])."'
		");

		$equipment_id = $this->db->getLastId();

		return $equipment_id;
	}


	public function editEquipments($equipment_id,$data) {
		$this->db->query("UPDATE equipment SEt 
		equipment_name = '" . $this->db->escape($data['equipment_name']) . "',
		short_name ='".$this->db->escape($data['short_name'])."',
		equipment_status ='".$this->db->escape($data['equipment_status'])."'
		WHERE equipment_id ='".$equipment_id."'
		");

		
	}

	

	public function deleteCategory($equipment_id) {
		$this->db->query("DELETE FROM equipment WHERE equipment_id = '" . (int)$equipment_id . "'");

		
	}

	public function repairCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$parent_id . "'");

		foreach ($query->rows as $category) {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category['category_id'] . "'");

			// Fix for records with no paths
			$level = 0;

			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$parent_id . "' ORDER BY level ASC");

			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

				$level++;
			}

			$this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$category['category_id'] . "', level = '" . (int)$level . "'");

			$this->repairCategories($category['category_id']);
		}
	}


	public function getEquipment($equipment_id) {  
		$sql = "SELECT *  FROM  equipment  WHERE equipment_id='".$equipment_id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}

	public function getCategory($category_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path, (SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "') AS keyword FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getCategories($data) {
		$sql = "SELECT *  FROM  equipment  WHERE 1=1 ";

		if (!empty($data['filter_equipment_name'])) {
			$sql .= " AND LOWER(equipment_name) LIKE '%" . $this->db->escape(strtolower($data['filter_equipment_name'])) . "%' ";
		}
		$sql .= " AND equipment_status = '" . $this->db->escape($data['filter_status']) . "' ";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	
	}

	public function getCategoryDescriptions($category_id) {
		$category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'description'      => $result['description']
			);
		}

		return $category_description_data;
	}

	public function getCategoryFilters($category_id) {
		$category_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_filter_data[] = $result['filter_id'];
		}

		return $category_filter_data;
	}

	public function getCategoryStores($category_id) {
		$category_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_store_data[] = $result['store_id'];
		}

		return $category_store_data;
	}

	public function getCategoryLayouts($category_id) {
		$category_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $category_layout_data;
	}

	public function getTotalCategories() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM equipment");

		return $query->row['total'];
	}
	
	public function getTotalCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}

	public function getEquipmentAuto($to_owner) {

		$sql =("SELECT equipment_name,equipment_id FROM  equipment WHERE 1 = 1");
		if($to_owner != ''){
			$sql .= " AND `equipment_name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		$sql .= " ORDER BY `equipment_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}	
}
