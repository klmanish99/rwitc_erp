<?php
class ModelCatalogOwner extends Model {
	public function addOwner($data) {
		//echo "<pre>"; print_r($data);exit;
		if (isset($data['gstType'])  && $data['gstType'] == 'Registered' ) {
			$gstNo = $data['gstNo'];
		}
		else{
			$gstNo = '';
		}
		$wirhoa = isset($data['WIRHOA']) ? "1" : "0";
		$gstType = isset($data['gstType']) ? $data['gstType'] : "";
		$code = $this->db->escape($data['hidden_owner_code']);
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$last_update_date = $date->format('Y-m-d h:i:s');
		//$Ocode =substr($code,1);

		// if ( $data['isActive'] == 'Active' ) {
		// 	$isActive = '1';
		// }
		// else{
		// 	$isActive = '0';
		// }
		$this->db->query("INSERT INTO owners SET 
			`owner_name_prefix` = '" . $this->db->escape($data['ownerNamePrefix']) . "',
			`owner_name` = '" . $this->db->escape($data['owner_name']) . "',
			`is_trainer` = '" . $this->db->escape($data['isTrainer']) . "',
			`active` = '" . $this->db->escape($data['isActive']) . "',
			`approval_date` = '" . $this->db->escape(date('Y-m-d', strtotime($data['dateApproval']))) . "',
			`race_card_name` = '" . $this->db->escape($data['raceCardName']) . "',
			`wirhoa` = '" . $this->db->escape($wirhoa) . "',
			`type_of_ownership` = '" . $this->db->escape($data['ownershipType']) . "',
			`cummitte_member` = '" . $this->db->escape($data['committeMember']) . "',
			`member` = '" . $this->db->escape($data['member']) . "',
			`approved_status` = '" . $this->db->escape($data['approved_status']) . "',
			`linked_owners` = '" . $this->db->escape($data['isLinkedOwner']) . "',
			`file_number` = '" . $this->db->escape($data['fileNumber']) . "',
			`file_number_upload` = '". $this->db->escape($data['file_number_upload']) ."',
			`uploaded_file_source` = '". $this->db->escape($data['uploaded_file_source']) ."',
			`remark` = '" . $this->db->escape($data['remark']) . "',
			`phone_no` = '".$this->db->escape($data['phoneNo'])."',
		    `mobile_no_1` = '".$this->db->escape($data['mobileNo1'])."',
		    `alt_mobile_no` = '".$this->db->escape($data['altMobileNo'])."',
		    `email_id` = '".$this->db->escape($data['emailId'])."',
		    `alt_email_id` = '".$this->db->escape($data['altEmailId'])."',
		    `address_1` = '".$this->db->escape($data['address1'])."',
		    `address_2` = '".$this->db->escape($data['address2'])."',
		    `local_area_1` = '".$this->db->escape($data['localArea1'])."',
		    `local_area_2` = '".$this->db->escape($data['localArea2'])."',
		    `state_1` = '".$this->db->escape($data['zone_id'])."',
		    `city_1` = '".$this->db->escape($data['city1'])."',
		    `state_2` = '".$this->db->escape($data['zone_id2'])."',
		    `city_2` = '".$this->db->escape($data['city2'])."',
		    `pincode_1` = '".$this->db->escape($data['pincode1'])."',
		    `country_1` = '".$this->db->escape($data['country1'])."',
		    `pincode_2` = '".$this->db->escape($data['pincode2'])."',
		    `country_2` = '".$this->db->escape($data['country2'])."',
		    address_3 = '" .$this->db->escape($data['address_3']). "',
		    address_4 = '" .$this->db->escape($data['address_4']). "',
		    `gst_type` = '".$this->db->escape($gstType)."',
		    `gst_no` = '".$this->db->escape($gstNo)."',
		    `pan_number` = '".$this->db->escape($data['panNumber'])."',
		    `prof_tax_no_details` = '".$this->db->escape($data['profTaxNoDetails'])."',
		     owner_code = '".$code."',
			`file_number_uploads` = '". $this->db->escape($data['file_number_uploads']) ."',
			`uploaded_file_sources` = '". $this->db->escape($data['uploaded_file_sources']) ."',
			`user_id` = '".$this->session->data['user_id']."',
			`last_update_date` = '".$last_update_date."'
		");

		$owner_id = $this->db->getLastId();

		if (isset($data['linkedOwnerName']) && $data['isLinkedOwner'] == 'Yes') {
			foreach ($data['linkedOwnerName'] as $value) {

				$relationz = $this->db->query("SELECT * FROM relationship WHERE `relationship` = '".$this->db->escape($value['relationships'])."' ");
				if ($relationz->num_rows > 0) {
					$relationship = $this->db->escape($value['relationship']);
				} elseif ($value['relationship'] == 'Other') {
					$relationship = $this->db->escape($value['relationships']);
					$this->db->query("INSERT INTO relationship SET relationship = '".$this->db->escape($relationship)."' ");
				} else {
					$relationship = $value['relationship'];
				}
				$this->db->query("INSERT INTO `linked_owners` SET 
					owner_id = '" . (int)$owner_id . "',
					owner_name = '" . $this->db->escape($value['owner_name']) . "',
					linked_owner_id = '" . (int)$value['linked_owner_id'] . "',
					relationship = '" . $this->db->escape($relationship) . "' 
				");
			}
		}

		//FOR OWNERS Partners
			if (isset($data['partnersData'])) {
				foreach ($data['partnersData'] as $pvalue) {
						$mem = isset($pvalue['mem']) ? $pvalue['mem'] : "";

					$this->db->query("INSERT INTO `owners_partner` SET 
						owner_id = '" . (int)$owner_id . "',
						partner_name = '" . $this->db->escape($pvalue['partner_name']) . "',
						member = '" . $mem . "',
						partner_id = '" . (int)$pvalue['partner_id'] . "',
						file = '" . $this->db->escape($pvalue['file']) . "' ,
						file_path = '" . $this->db->escape($pvalue['file_path']) . "' 
					");
				}
			}
			

		if (isset($data['itrDatas'])) {
			foreach ($data['itrDatas'] as $value) {
				$this->db->query("INSERT INTO `owners_itr` SET 
					assessment_year = '" . $this->db->escape($value['assessment_year']) . "',
					file_path = '" . $this->db->escape($value['file_path']) . "',
					owner_id = '" . (int)$owner_id . "',
					file = '" . $this->db->escape($value['file']) . "'
				");
			}
		}

		//FOR OWNER ADD COLOR
		if (isset($data['add_color'])) {
			foreach ($data['add_color'] as $value) {
				$this->db->query("INSERT INTO `owners_color` SET 
					owner_id = '" . (int)$owner_id . "',
					color = '" . $this->db->escape($value['color']) . "',
					start_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['start_date']))) . "',
					end_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['end_date']))) . "',
					season = '".$this->db->escape($data['season'])."',
		    		allow_Share_Color = '".$this->db->escape($data['allow_Share_Color'])."'
				");
		
				$color_auto_id = $this->db->getLastId();

				// echo'<pre';
				// print_r($color_auto_id);
				// exit;

				//FOR SHARED COLOR WITH OWNERS
				if (isset($data['owner_shared_color'])) {
					foreach ($data['owner_shared_color'] as $value) {
						$this->db->query("INSERT INTO `owners_shared_color` SET 
							owner_id = '" . (int)$owner_id . "',
							`color_trans_id` = '".$color_auto_id."',
							shared_owner_name = '" . $this->db->escape($value['shared_owner_name']) . "',
							shared_owner_id = '". (int)$value['shared_owner_id'] ."',
							start_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['start_date']))) . "',
							end_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['end_date']))) . "'
						");
					}
				}
			}
		}

		if (isset($data['ownerBan'])) {
			foreach ($data['ownerBan'] as $value) {
				$this->db->query("INSERT INTO `owners_ban` SET 
					owner_id = '" . (int)$owner_id . "',
					club = '" . $this->db->escape($value['club']) . "',
    				ban_type = '". $this->db->escape($value['ban_type']) ."',
    				start_date = '" . $this->db->escape(date('Y-m-d', strtotime($value['start_date']))) . "',
					end_date = '" . $this->db->escape(date('Y-m-d', strtotime($value['end_date']))) . "',
					amount = '" . $this->db->escape($value['amount']) . "'
				");
			}
		}
		
		return $owner_id;
	}

	public function editOwner($ownerId, $data) {
		//echo "<pre>"; print_r($data); exit;
		if (isset($data['gstType'])  && $data['gstType'] == 'Registered' ) {
			$gstNo = $data['gstNo'];
		}
		else{
			$gstNo = '';
		}

		$wirhoa = isset($data['WIRHOA']) ? "1" : "0";
		$gstType = isset($data['gstType']) ? $data['gstType'] : "";

		$code = $this->db->escape($data['hidden_owner_code']);
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$last_update_date = $date->format('Y-m-d h:i:s');
		//$Ocode =substr($code,1);

		// if ( $data['isActive'] == 'Active' ) {
		// 	$isActive = '1';
		// }
		// else{
		// 	$isActive = '0';
		// }
		$this->db->query("UPDATE `owners` SET 
			`owner_name_prefix` = '" . $this->db->escape($data['ownerNamePrefix']) . "',
			`owner_name` = '" . $this->db->escape($data['owner_name']) . "',
			`is_trainer` = '" . $this->db->escape($data['isTrainer']) . "',
			`active` = '" . $this->db->escape($data['isActive']) . "',
			`approval_date` = '" . $this->db->escape(date('Y-m-d', strtotime($data['dateApproval']))) . "',
			`race_card_name` = '" . $this->db->escape($data['raceCardName']) . "',
			`wirhoa` = '" . $this->db->escape($wirhoa) . "',
			`type_of_ownership` = '" . $this->db->escape($data['ownershipType']) . "',
			`cummitte_member` = '" . $this->db->escape($data['committeMember']) . "',
			`member` = '" . $this->db->escape($data['member']) . "',
			`approved_status` = '" . $this->db->escape($data['approved_status']) . "',
			`linked_owners` = '" . $this->db->escape($data['isLinkedOwner']) . "',
			`file_number` = '" . $this->db->escape($data['fileNumber']) . "',
			`file_number_upload` = '". $this->db->escape($data['file_number_upload']) ."',
			`uploaded_file_source` = '". $this->db->escape($data['uploaded_file_source']) ."',
			`remark` = '" . $this->db->escape($data['remark']) . "',
			`phone_no` = '".$this->db->escape($data['phoneNo'])."',
		    `mobile_no_1` = '".$this->db->escape($data['mobileNo1'])."',
		    `alt_mobile_no` = '".$this->db->escape($data['altMobileNo'])."',
		    `email_id` = '".$this->db->escape($data['emailId'])."',
		    `alt_email_id` = '".$this->db->escape($data['altEmailId'])."',
		    `address_1` = '".$this->db->escape($data['address1'])."',
		    `address_2` = '".$this->db->escape($data['address2'])."',
		    `local_area_1` = '".$this->db->escape($data['localArea1'])."',
		    `local_area_2` = '".$this->db->escape($data['localArea2'])."',
		    `state_1` = '".$this->db->escape($data['zone_id'])."',
		    `city_1` = '".$this->db->escape($data['city1'])."',
		    `state_2` = '".$this->db->escape($data['zone_id2'])."',
		    `city_2` = '".$this->db->escape($data['city2'])."',
		    `pincode_1` = '".$this->db->escape($data['pincode1'])."',
		    `country_1` = '".$this->db->escape($data['country1'])."',
		    `pincode_2` = '".$this->db->escape($data['pincode2'])."',
		    `country_2` = '".$this->db->escape($data['country2'])."',
		    address_3 = '" .$this->db->escape($data['address_3']). "',
		    address_4 = '" .$this->db->escape($data['address_4']). "',
		    `gst_type` = '".$this->db->escape($gstType)."',
		    `gst_no` = '".$this->db->escape($gstNo)."',
		    `pan_number` = '".$this->db->escape($data['panNumber'])."',
		    `prof_tax_no_details` = '".$this->db->escape($data['profTaxNoDetails'])."',
		     owner_code = '".$code."',
			`file_number_uploads` = '". $this->db->escape($data['file_number_uploads']) ."',
			`uploaded_file_sources` = '". $this->db->escape($data['uploaded_file_sources']) ."',
			`user_id` = '".$this->session->data['user_id']."',
			`last_update_date` = '".$last_update_date."'
			WHERE id = '" . (int)$ownerId . "'
			");
			
			//FOR LINKED OWNERS
			if (isset($data['linkedOwnerName']) && $data['isLinkedOwner'] == 'Yes') {
				$remove = $this->db->query("DELETE FROM `linked_owners` WHERE owner_id = '" . (int)$ownerId . "'");
				if ($remove) {
					foreach ($data['linkedOwnerName'] as $value) {

						$relationz = $this->db->query("SELECT * FROM relationship WHERE `relationship` = '".$this->db->escape($value['relationships'])."' ");
						if ($relationz->num_rows > 0) {
							$relationship = $this->db->escape($value['relationship']);
						} elseif ($value['relationship'] == 'Other') {
							$relationship = $this->db->escape($value['relationships']);
							$this->db->query("INSERT INTO relationship SET relationship = '".$relationship."' ");
						} else {
							$relationship = $value['relationship'];
						}
						$this->db->query("INSERT INTO `linked_owners` SET 
							owner_id = '" . (int)$ownerId . "',
							owner_name = '" . $this->db->escape($value['owner_name']) . "',
							linked_owner_id = '" . (int)$value['linked_owner_id'] . "',
							relationship = '" . $relationship . "' 
						");
					}
				}
			}
			else{
				$remove = $this->db->query("DELETE FROM `linked_owners` WHERE owner_id = '" . (int)$ownerId . "'");
			}

			//FOR OWNERS Partners
			if (isset($data['partnersData'])) {
				$remove = $this->db->query("DELETE FROM `owners_partner` WHERE owner_id = '" . (int)$ownerId . "'");
				if ($remove) {
					foreach ($data['partnersData'] as $pvalue) {
						$mem = isset($pvalue['mem']) ? $pvalue['mem'] : "";

						$this->db->query("INSERT INTO `owners_partner` SET 
							owner_id = '" . (int)$ownerId . "',
							partner_name = '" . $this->db->escape($pvalue['partner_name']) . "',
							member = '" . $this->db->escape($mem) . "',
							partner_id = '" . (int)$pvalue['partner_id'] . "',
							file = '" . $this->db->escape($pvalue['file']) . "' ,
							file_path = '" . $this->db->escape($pvalue['file_path']) . "' 
						");
					}
				}
			}
			else{
				$remove = $this->db->query("DELETE FROM `owners_partner` WHERE owner_id = '" . (int)$ownerId . "'");
			}

			//FOR OWNERS ITR DATA
			if (isset($data['itrDatas'])) {
				$remove = $this->db->query("DELETE FROM `owners_itr` WHERE owner_id = '" . (int)$ownerId . "'");
				if ($remove) {	
					foreach ($data['itrDatas'] as $value) {
						$this->db->query("INSERT INTO `owners_itr` SET 
							assessment_year = '" . $this->db->escape($value['assessment_year']) . "',
							file_path = '" . $this->db->escape($value['file_path']) . "',
							owner_id = '" . (int)$ownerId . "',
							file = '" . $this->db->escape($value['file']) . "'
						");
					}
				}
			}
			else{
				$remove = $this->db->query("DELETE FROM `owners_itr` WHERE owner_id = '" . (int)$ownerId . "'");
			}

			//FOR OWNER ADD COLOR
			if (isset($data['add_color'])) {
				$remove = $this->db->query("DELETE FROM `owners_color` WHERE owner_id = '" . (int)$ownerId . "' AND end_date >= '".date('Y-m-d')."' ");
				if ($remove) {
					foreach ($data['add_color'] as $value) {
						$this->db->query("INSERT INTO `owners_color` SET 
							owner_id = '" . (int)$ownerId . "',
							color = '" . $this->db->escape($value['color']) . "',
							start_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['start_date']))) . "',
							end_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['end_date']))) . "',
							season = '".$this->db->escape($data['season'])."',
		    				allow_Share_Color = '".$this->db->escape($data['allow_Share_Color'])."'
						");
						$color_auto_id = $this->db->getLastId();

						//FOR SHARED COLOR WITH OWNERS
						if (isset($data['owner_shared_color'])) {
							$remove = $this->db->query("DELETE FROM `owners_shared_color` WHERE owner_id = '" . (int)$ownerId . "' AND end_date >= '".date('Y-m-d')."' ");
								if ($remove) {
								foreach ($data['owner_shared_color'] as $value) {
									$this->db->query("INSERT INTO `owners_shared_color` SET 
										owner_id = '" . (int)$ownerId . "',
										`color_trans_id` = '".$color_auto_id."',
										shared_owner_name = '" . $this->db->escape($value['shared_owner_name']) . "',
										shared_owner_id = '". (int)$value['shared_owner_id'] ."',
										start_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['start_date']))) . "',
										end_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['end_date']))) . "'
									");
								}
							}
						}
						else{
							$remove = $this->db->query("DELETE FROM `owners_shared_color` WHERE owner_id = '" . (int)$ownerId . "' AND end_date >= '".date('Y-m-d')."' ");
						}
					}
				}
			}
			else{
				$remove = $this->db->query("DELETE FROM `owners_color` WHERE owner_id = '" . (int)$ownerId . "' AND end_date >= '".date('Y-m-d')."' ");
			}

			

			if (isset($data['ownerBan'])) {
				$remove = $this->db->query("DELETE FROM `owners_ban` WHERE owner_id = '" . (int)$ownerId . "' AND end_date >= '".date('Y-m-d')."' ");
				if ($remove) {
					foreach ($data['ownerBan'] as $value) {
						$this->db->query("INSERT INTO `owners_ban` SET 
							owner_id = '" . (int)$ownerId . "',
							club = '" . $this->db->escape($value['club']) . "',
		    				ban_type = '". $this->db->escape($value['ban_type']) ."',
		    				start_date = '" . $this->db->escape(date('Y-m-d', strtotime($value['start_date']))) . "',
							end_date = '" . $this->db->escape(date('Y-m-d', strtotime($value['end_date']))) . "',
							amount = '" . $this->db->escape($value['amount']) . "'
						");
					}
				}
			}
			else{
				$remove = $this->db->query("DELETE FROM `owners_ban` WHERE owner_id = '" . (int)$ownerId . "' AND end_date >= '".date('Y-m-d')."' ");
			}
	}

	public function deleteOwner($owner_id) {
		$this->db->query("DELETE FROM `owners` WHERE id = '" . (int)$owner_id . "'");
		$this->db->query("DELETE FROM `linked_owners` WHERE owner_id = '" . (int)$owner_id . "'");
	}

	public function getOwners($data = array()) {
		//echo "<pre>"; print_r($data);exit;
		$sql =("SELECT * FROM `owners` WHERE 1=1 ");

		if (!empty($data['filter_owner_name'])) {
			$sql .= " AND owner_name LIKE '" . $this->db->escape($data['filter_owner_name']) . "%'";
		}

		if (!empty($data['filter_owner_id'])) {
			$sql .= " AND owner_code = '" . $this->db->escape($data['filter_owner_id']) . "' ";
		}

		if (isset($data['filter_status'])) {
			$sql .= " AND active = '" . $this->db->escape($data['filter_status']) . "' ";
		}

		if (!empty($data['filter_name']) || isset($data['owner_Id'])) {
			$sql .= " AND id != '".$data['owner_Id']."' AND owner_name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		// $sql .= " ORDER BY id";

		$sort_data = array(
			'owner_name',
			'type_of_ownership',
			'mobile_no_1',
			'email_id',
			'pan_number',
			'gst_no'
			
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY owner_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getOwnersById($ownerId) {
		$data = array();
		$date = date('Y-m-d');
		$sql =("SELECT * FROM `owners` WHERE `id` = '".$ownerId."' ");
		$linkedOwnerQuery = $this->db->query("SELECT * FROM `linked_owners` WHERE `owner_id` = '".$ownerId."' ")->rows;
		$partnerOwnerQuery = $this->db->query("SELECT * FROM `owners_partner` WHERE `owner_id` = '".$ownerId."' ")->rows;
		$itrOwnerQuery = $this->db->query("SELECT * FROM `owners_itr` WHERE `owner_id` = '".$ownerId."' ")->rows;
		$colorQuery = $this->db->query("SELECT * FROM `owners_color` WHERE `owner_id` = '".$ownerId."' AND `end_date` >= '". $date ."' ")->rows;
		//$colorHistoryQuery = $this->db->query("SELECT * FROM `owners_color` WHERE `owner_id` = '".$ownerId."' AND `end_date` < '". $date ."' ")->rows;
		$tmp = $this->db->query("SELECT end_date,start_date FROM `owners_color` WHERE `owner_id` = '".$ownerId."' AND `end_date` < '". $date ."' GROUP BY end_date,start_date ORDER BY end_date DESC ")->rows;
		if ($tmp != array()) {
			foreach ($tmp as $key => $value) {
				$colorHistoryQuery[$value['start_date'].'_'.$value['end_date']] = $this->db->query("SELECT id, color, owner_id FROM `owners_color` WHERE `owner_id` = '".$ownerId."' AND start_date = '".$value['start_date']."' AND end_date = '".$value['end_date']."' ")->rows;
				
			}
		}
		else{
			$colorHistoryQuery = array();
		}

		foreach($colorHistoryQuery as $kkey => $kvalue){
			$myid = '';
			foreach($kvalue as $fkey => $fvalue){
				if($fvalue['id'] != $myid){
					$myid = $fvalue['id'];
					$history_shared_color_datas = $this->db->query("SELECT shared_owner_name From `owners_shared_color` WHERE `color_trans_id` = '".$fvalue['id']."' GROUP BY shared_owner_id ")->rows;
					$own_name_color_shared = '';
					foreach($history_shared_color_datas as $hckey => $hcvalue){
						$own_name_color_shared .= $hcvalue['shared_owner_name'].',';
					}

					$colorHistoryQuery[$kkey][$fkey]['own_name'] =  rtrim($own_name_color_shared, ',');
				}
			}
		}
		
		
		// echo'<pre>';
		// 			print_r($colorHistoryQuery);
		// exit;

			

		$owner_shared_color = $this->db->query("SELECT * FROM `owners_shared_color` WHERE `owner_id` = '".$ownerId."' AND `end_date` >= '". $date ."'  GROUP BY shared_owner_id  ")->rows;
		$owner_ban = $this->db->query("SELECT * FROM `owners_ban` WHERE `owner_id` = '".$ownerId."' AND `end_date` >= '". $date ."' ORDER BY end_date DESC ")->rows;
		$owner_ban_history = $this->db->query("SELECT * FROM `owners_ban` WHERE `owner_id` = '".$ownerId."' AND `end_date` < '". $date ."' ORDER BY end_date DESC ")->rows;

		$data = $this->db->query($sql)->row;
		$data = array_merge($data, array('linkedOwnerQuery'=>$linkedOwnerQuery));
		$data = array_merge($data, array('itrOwnerQuery'=>$itrOwnerQuery));
		$data = array_merge($data, array('colorQuery'=>$colorQuery));
		$data = array_merge($data, array('colorHistoryQuery'=>$colorHistoryQuery));
		$data = array_merge($data, array('owner_shared_color'=>$owner_shared_color));
		$data = array_merge($data, array('owner_ban'=>$owner_ban));
		$data = array_merge($data, array('owner_ban_history'=>$owner_ban_history));
		$data = array_merge($data, array('partnerOwnerQuery'=>$partnerOwnerQuery));
		//echo "<pre>"; print_r($data);exit;
		return $data;
	}

	public function gethorseowner($owner_id,$date) {

		$sql = ("SELECT * FROM `horse1` h LEFT JOIN horse_to_trainer ht ON h.horseseq =ht.horse_id LEFT JOIN horse_to_owner ho ON h.horseseq =ho.horse_id WHERE ho.to_owner_id = '".$owner_id."' AND ho.owner_share_status = 1 ");
		
		if($date!= ''){
			$sql .= " AND ho.date_of_ownership <= '".$date."' AND ho.end_date_of_ownership >= '".$date."'";
		}
		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	/*public function getCategories($data = array()) {
		$sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.category_id";

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}*/

	public function getCategoryDescriptions($category_id) {
		$category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'description'      => $result['description']
			);
		}

		return $category_description_data;
	}

	public function getCategoryFilters($category_id) {
		$category_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_filter_data[] = $result['filter_id'];
		}

		return $category_filter_data;
	}

	public function getCategoryStores($category_id) {
		$category_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_store_data[] = $result['store_id'];
		}

		return $category_store_data;
	}

	public function getCategoryLayouts($category_id) {
		$category_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $category_layout_data;
	}

	public function getTotalCategories($data = array()) {


		$sql = "SELECT COUNT(*) AS total FROM `owners`WHERE 1=1 ";

		if (!empty($data['filter_owner_name'])) {
			$sql .= " AND owner_name LIKE '" . $this->db->escape($data['filter_owner_name']) . "%'";
		}

		if (!empty($data['filter_owner_id'])) {
			$sql .= " AND owner_code = '" . $this->db->escape($data['filter_owner_id']) . "' ";
		}


		if (isset($data['filter_status'])) {
			$sql .= " AND active = '" . $this->db->escape($data['filter_status']) . "' ";
		}

		if (!empty($data['filter_name']) || isset($data['owner_Id'])) {
			$sql .= " AND id != '".$data['owner_Id']."' AND owner_name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}

	public function getTotalCategoriess($data = array()) {


		$sql = "SELECT COUNT(*) AS total FROM `owners`WHERE active=1 ";

		if (!empty($data['filter_owner_name'])) {
			$sql .= " AND owner_name LIKE '" . $this->db->escape($data['filter_owner_name']) . "%'";
		}

		if (!empty($data['filter_owner_id'])) {
			$sql .= " AND owner_code = '" . $this->db->escape($data['filter_owner_id']) . "' ";
		}


		if (isset($data['filter_status'])) {
			$sql .= " AND active = '" . $this->db->escape($data['filter_status']) . "' ";
		}

		if (!empty($data['filter_name']) || isset($data['owner_Id'])) {
			$sql .= " AND id != '".$data['owner_Id']."' AND owner_name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getTotalCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}

	public function getOwnerss($data = array()) {
		//echo "<pre>"; print_r($data);exit;
		$sql =("SELECT * FROM `owners` WHERE 1=1 AND (type_of_ownership = 'Individual' OR type_of_ownership = 'Directory') ");

		if (!empty($data['filter_owner_name'])) {
			$sql .= " AND owner_name LIKE '" . $this->db->escape($data['filter_owner_name']) . "%'";
		}

		if (!empty($data['filter_owner_id'])) {
			$sql .= " AND owner_code = '" . $this->db->escape($data['filter_owner_id']) . "' ";
		}


		if (isset($data['filter_status'])) {
			$sql .= " AND active = '" . $this->db->escape($data['filter_status']) . "' ";
		}

		if (!empty($data['filter_name']) || isset($data['owner_Id'])) {
			$sql .= " AND id != '".$data['owner_Id']."' AND owner_name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getOwnersAuto($to_owner) {

		$sql =("SELECT owner_name,id FROM owners WHERE 1 = 1");
		if($to_owner != ''){
			$sql .= " AND `owner_name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		$sql .= " ORDER BY `owner_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getColorsAuto($data = array()) {
		//echo "<pre>"; print_r($data);exit;
		$sql =("SELECT * FROM `colors` WHERE 1=1 ");

		if (!empty($data['filter_name']) ) {
			$sql .= " AND color LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	}	
}
