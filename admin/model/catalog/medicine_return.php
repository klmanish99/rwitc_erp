<?php
class ModelCatalogmedicinereturn extends Model {
	public function addMedicineTrans($data) {
		/*echo'<pre>';
		print_r($data);
		exit;*/

		$this->db->query("INSERT INTO `medicine_return` SET 
			`issue_no` = '".$data['issue_number']."',
			`parent_doctor_id` = '".$data['filterParentId']."',
			`parent_doctor_name` = '".$data['filter_parent_doctor']."',
			`child_doctor_id` = '".$data['to_doc']."',
			`entry_date` = '".date('Y-m-d', strtotime($data['date']))."',
			`total_item` = '".$data['total_item']."',
			`total_qty` = '".$data['total_qty']."',
			`total_amt` = '".$data['total']."'
		");
		$trans_id = $this->db->getLastId();
		foreach ($data['productraw_datas'] as $key => $value) {
			$this->db->query("INSERT INTO `medicine_return_items` SET 
				`parent_id` = '".$trans_id."',
				`product_id` = '".$value['input_item_code']."',
				`product_name` = '".$value['item_name']."',
				`entry_date` = '".date('Y-m-d', strtotime($data['date']))."',
				`expire_date` = '".date('Y-m-d', strtotime($value['expiry_date']))."',
				`product_qty` = '".$value['input_qty']."'
			");
		}
/*
		foreach ($data['productraw_datas'] as $key => $value) {
			$this->db->query("INSERT INTO `medicine_return_items` SET 
				`parent_id` = '".$trans_id."',
				`product_id` = '".$value['input_item_code']."',
				`product_name` = '".$value['item_name']."',
				`entry_date` = '".date('Y-m-d', strtotime($data['date']))."',
				`expire_date` = '".date('Y-m-d', strtotime($value['expiry_date']))."',
				`product_qty` = '".$value['input_qty']."'
			");

			$total_inward_qty = $this->db->query("SELECT  productraw_id ,quantity FROM `oc_inwarditem` WHERE product_id = '".$value['input_item_code']."' AND cons_status = 0 order by ex_date ASC ");
			if ($total_inward_qty->num_rows > 0) {
				$quantity = $total_inward_qty->row['quantity'];
				$productraw_id = $total_inward_qty->row['productraw_id'];
			} else {
				$quantity = '';
				$productraw_id = '';
			}

			$pre_transfer_qty = $this->db->query("SELECT sum(qty) as qty FROM `inward_medicine_transfer` WHERE product_id = '".$value['input_item_code']."' AND inward_id ='".$productraw_id."' ");
			
			if($pre_transfer_qty->num_rows > 0){
				// echo '<pre>';
				// print_r($pre_transfer_qty->row['qty']);
				// echo '<br />';
				// echo '<pre>';
				// print_r($total_inward_qty['quantity']);
				// echo '<br />';
				// exit;
				$bal_qty = $quantity - $pre_transfer_qty->row['qty'];
				if($value['input_qty'] > $bal_qty){
					$this->db->query("INSERT INTO `inward_medicine_transfer` SET inward_id = '".$productraw_id."', transfer_id = '".$trans_id."', product_id = '".$value['input_item_code']."', qty = '". $bal_qty."', transfer_date = '".date('Y-m-d', strtotime($data['date']))."' ");
					$this->db->query("UPDATE oc_inwarditem SET cons_status = 1 WHERE productraw_id = '".$productraw_id."' ");


					$total_new_inward_qty = $this->db->query("SELECT  productraw_id ,quantity FROM `oc_inwarditem` WHERE product_id = '".$value['input_item_code']."' AND cons_status = 0 order by ex_date ASC ");
					if ($total_new_inward_qty->num_rows > 0){
						$qty = $value['input_qty'] - $bal_qty;
						$this->db->query("INSERT INTO `inward_medicine_transfer` SET inward_id = '".$total_new_inward_qty->row['productraw_id']."', transfer_id = '".$trans_id."', product_id = '".$value['input_item_code']."', qty = '".$qty."', transfer_date = '".date('Y-m-d', strtotime($data['date']))."' ");
					}
				} else {
					if($bal_qty == 0){
						$this->db->query("UPDATE oc_inwarditem SET cons_status = 1 WHERE productraw_id = '".$productraw_id."' ");
					}
					$this->db->query("INSERT INTO `inward_medicine_transfer` SET inward_id = '".$productraw_id."', transfer_id = '".$trans_id."', product_id = '".$value['input_item_code']."', qty = '".$value['input_qty']."', transfer_date = '".date('Y-m-d', strtotime($data['date']))."' ");
				}
			} else {
				$this->db->query("INSERT INTO `inward_medicine_transfer` SET inward_id = '".$productraw_id."', transfer_id = '".$trans_id."', product_id = '".$value['input_item_code']."', qty = '".$value['input_qty']."', transfer_date = '".date('Y-m-d', strtotime($data['date']))."' ");
			}
		}*/

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');


		$this->db->query("INSERT INTO `med_return_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `medt_id` = '".$trans_id."' ");
		
	}
	

	public function editinward($order_id, $data) {
		/*echo '<pre>';
		print_r($data);
		exit;*/
		$this->db->query("UPDATE oc_inward SET 
							
							order_no = '" . $this->db->escape($data['order_no']) . "',
							date = '" . $this->db->escape(date('Y-m-d', strtotime($data['date']))) . "',
							supplier = '" . $this->db->escape($data['supplier']) . "'
							WHERE order_id = '" . (int)$order_id . "'");				
		$this->db->query("DELETE FROM `oc_inwarditem` WHERE `order_id` = '".$order_id."'");
		if(isset($data['productraw_datas'])){
			foreach($data['productraw_datas'] as $vkeys => $pvalues){
				$total = $pvalues['purchase_price'] * $pvalues['quantity'];
				$this->db->query("INSERT INTO `oc_inwarditem` SET
								order_id = '".$order_id."',
								order_no = '" . $this->db->escape($data['order_no']) . "',
								productraw_name = '".$pvalues['productraw_name']."',
								product_id = '".$pvalues['product_id']."',
								quantity = '".$pvalues['quantity']."',
								purchase_price = '".$pvalues['purchase_price']."',
								value = '".$total."',
								ex_date = '".$pvalues['expiry_date']."',
								batch_no = '".$pvalues['batch_no']."',
								po_no = '".$pvalues['po_data']."',
								po_qty = '".$pvalues['po_qty']."',
								reduce_po_qty = '".$pvalues['reduce_po_qty']."'	
							");
				if ($pvalues['reduce_po_qty'] == 0) {
					$this->db->query("UPDATE oc_tally_po SET 
									status = 2
									WHERE po_no = '" .$pvalues['po_data']. "'
									");
				} elseif ($pvalues['reduce_po_qty'] != 0) {
					$this->db->query("UPDATE oc_tally_po SET 
									status = 1
									WHERE po_no = '" .$pvalues['po_data']. "'
									");
				}
			}
		}

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');


		$this->db->query("INSERT INTO `med_return_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `medt_id` = '".$trans_id."' ");
		
	}

	public function deleteinward($order_id) {
		$this->db->query("DELETE FROM oc_inward WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM oc_inwarditem WHERE order_id = '" . (int)$order_id . "'");
	}

	public function getinward($order_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM medicine_return WHERE id = '" . (int)$order_id . "'");
		return $query->row;
	}

	public function getinwards($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_inward";
			
		$sql .= " WHERE 1=1";
		
		if (!empty($data['filter_inward'])) {
			$sql .= " AND order_id LIKE '%" . $this->db->escape($data['filter_inward']) . "%'";
		}

		if (!empty($data['filter_order_no'])) {
			$sql .= " AND order_no LIKE '%" . $this->db->escape($data['filter_order_no']) . "%'";
		}

		$sort_data = array(
			'order_no',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getsupplier($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM is_vendor";
			
		$sql .= " WHERE 1=1";
		
		if (!empty($data['filter_supplier'])) {
			$sql .= " AND vendor_name LIKE '%" . $this->db->escape($data['filter_supplier']) . "%'";
		}

		$sort_data = array(
			'order_no',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY vendor_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}	


	public function getTotalinwards($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_inward";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND order_id = '" . $this->db->escape($data['filter_order_id']) . "'";
		}

		/*if (!empty($this->user->getUserType() == '4')) {
			$sql .= " AND 	status = '1' " ;
		}*/

		if (!empty($data['filter_order_no'])) {
			$sql .= " AND order_no LIKE '%" . $this->db->escape($data['filter_order_no']) . "%'";
		}

		if (!empty($data['filter_productfinished_category'])) {
			$sql .= " AND productfinished_category LIKE '%" . $this->db->escape($data['filter_productfinished_category']) . "%'";
		}

		if (!empty($data['filter_productfinished_category_id'])) {
			$sql .= " AND productfinished_category_id = '" . $this->db->escape($data['filter_productfinished_category_id']) . "'";
		}

		if (!empty($data['filter_productsort'])) {
			$sql .= " AND UCASE(productfinished_name) LIKE '" . $this->db->escape($data['filter_productsort']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}


	public function daterawmaterial() {
		$query = $this->db->query("SELECT * FROM oc_inward WHERE 1=1")->rows;
		return $query;	
	}

	public function getcatname($category_id) {
		$query = $this->db->query("SELECT `name` FROM `" . DB_PREFIX . "category` WHERE `category_id` = '".$category_id."' ");
		if(isset($query->row['name'])){
			return $query->row['name'];
		} else {
			return false;
		}
	}

	public function getproducthistory_data($order_id) {
		$query = $this->db->query("SELECT * FROM `is_product_history` WHERE `order_id` = '".$order_id."' ORDER BY `id` DESC");
		return $query->rows;
	}

	public function getproductvendor_datas($order_id) {
		$query = $this->db->query("SELECT * FROM `is_vendor_materials` WHERE `order_id` = '".$order_id."' ");
		return $query->rows;
	}

	public function makeProduct($finished_product_id, $data) {
		
		$sql_finished = "INSERT INTO `is_manufacturing_register_finished` SET `finished_product_id` = '".$finished_product_id."', `finished_product_name` = '".$data['productraw_name']."', `quantity` = '".$data['quantity_all']."', `user_id` = '".$this->user->getId()."', `date` = now()";
		$query_finished = $this->db->query($sql_finished);
		$order_id = $this->db->getLastId();

		$add_to_finished = "UPDATE `oc_inward` SET `quantity` = (`quantity` + " . (int)$data['quantity_all'] . ") WHERE `order_id` = '".$finished_product_id."'";
		$this->db->query($add_to_finished);

		foreach($data['productraw_datas'] as $result) {
			$sql_raw = "INSERT INTO `is_manufacturing_register_raw` SET `id` = '".$order_id."', `product_id` = '".$result['product_id']."', `product_name` = '".$result['productraw_name']."', `quantity` = '".$result['quantity']."'";
			$query_raw = $this->db->query($sql_raw);


/*			$remove_from_raw = "UPDATE `is_productnew` SET `quantity` = (`quantity` - " . (int)$result['quantity'] . ") WHERE `productnew_id` = '".$result['product_id']."'";
			$this->db->query($remove_from_raw);
*/
			$remove_from_raw = "UPDATE `is_raw_material` SET `quantity` = (`quantity` - " . (int)$result['quantity'] . ") WHERE `raw_material_id` = '".$result['product_id']."'";
			$this->db->query($remove_from_raw);
		}
	}

	public function getProductnews($data = array()) {
		/*echo'<pre>';
		print_r($data);
		exit;*/
		$sql = "SELECT * FROM medicine";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_name'])) {
			$sql .= " AND med_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['med_code'])) {
			$sql .= " AND med_code LIKE '%" . $this->db->escape($data['med_code']) . "%'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo'<pre>';print_r($sql);exit;

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getProductcode($data = array()) {
		/*echo'<pre>';
		print_r($data);
		exit;*/
		$sql = "SELECT * FROM medicine";

		$sql .= " WHERE 1=1";

		if (!empty($data['med_code'])) {
			$sql .= " AND med_code = '" . $this->db->escape($data['med_code']) . "'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo'<pre>';print_r($sql);exit;

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getDoctorAutos($doctor_name) {
		// echo'<pre>';
		// print_r($doctor_name);
		// exit;

		$sql =("SELECT * FROM  doctor WHERE 1=1 AND isActive = 'Active' AND (parent_id = 0 || is_parent) ");
		if($doctor_name != ''){
			$sql .= " AND `doctor_name` LIKE '%" . $this->db->escape($doctor_name) . "%'";
		}
		
		$sql .= " ORDER BY `doctor_name` ";

		//echo $sql;exit;
		

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getChildDoctorAutos($child_doctor_code) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT * FROM  doctor WHERE isActive = 'Active' AND is_parent = 0 ");
		if($child_doctor_code != ''){
			$sql .= " AND `parent_id` = '" . ($child_doctor_code) . "' ";
		}
		
		$sql .= " ORDER BY `doctor_name` ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}
}
