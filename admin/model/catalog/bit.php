<?php
class ModelCatalogBit extends Model {
	public function addBits($data) {
	/*echo'<pre>';
	print_r($data);
	exit;*/
		$this->db->query("INSERT INTO bit SEt 
		short_name = '" . $this->db->escape($data['short_name']) . "',
		long_name ='".$this->db->escape($data['long_name'])."'
		");
		$id = $this->db->getLastId();
		return $id;
	}


	public function editBits($id,$data) {
		/*echo'<pre>';
	print_r($data);
	exit;*/
		$this->db->query("UPDATE bit SEt 
		short_name = '" . $this->db->escape($data['short_name']) . "',
		long_name ='".$this->db->escape($data['long_name'])."'
		WHERE id ='".$id."'
		");
	}

	

	public function deleteBits($id) {
		$this->db->query("DELETE FROM bit WHERE id = '" . (int)$id . "'");
	}

	public function getBit($data = array()) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$sql = "SELECT *  FROM  bit WHERE 1=1 ";

		if (!empty($data['filter_short_name'])) {
			$sql .= " AND short_name LIKE '" . $this->db->escape($data['filter_short_name']) . "%'";
		}

		// if (!empty($data['filter_status'])) {
		// 	$sql .= " AND type LIKE '" . $this->db->escape($data['filter_status']) . "%'";
		// }

		/*echo'<pre>';
		print_r($sql);
		exit;*/

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	
	}

	public function getBits($id) {  
		$sql = "SELECT *  FROM  bit  WHERE id='".$id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}

	public function getTotalBits($data = array()) {
		$sql = ("SELECT COUNT(*) AS total FROM bit WHERE 1=1 ");

		if (!empty($data['filter_short_name'])) {
			$sql .= " AND short_name LIKE '" . $this->db->escape($data['filter_short_name']) . "%'";
		}

		// if (!empty($data['filter_status'])) {
		// 	$sql .= " AND type LIKE '" . $this->db->escape($data['filter_status']) . "%'";
		// }

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getBitAuto($short_name) {
		// echo'<pre>';
		// print_r($short_name);
		// exit;

		$sql =("SELECT id, short_name FROM  bit");
		if($short_name != ''){
			$sql .= " WHERE `short_name` LIKE '%" . $this->db->escape($short_name) . "%'";
		}
		
		$sql .= " ORDER BY `id` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}

}
