<?php
class ModelCatalogImportList extends Model {

	public function getTally($data = array()) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$sql = "SELECT * FROM oc_tally_po";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_po_no'])) {
			$sql .= " AND po_no LIKE '%" . $this->db->escape($data['filter_po_no']) . "%'";
		}

		if (!empty($data['filter_item'])) {
			$sql .= " AND name_of_the_items LIKE '%" . $this->db->escape($data['filter_item']) . "%'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND date_added LIKE '%" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "%'";
		}

		if(isset($data['filter_status'])){
			if($data['filter_status'] == '0'){
				$sql .= " AND status = 0 ";
			}elseif ($data['filter_status'] == '1') {
				$sql .= " AND status = 1 ";
			}
			elseif ($data['filter_status'] == '2') {
				$sql .= " AND status = 2 ";
			}
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalTally($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_tally_po";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_po_no'])) {
			$sql .= " AND po_no LIKE '%" . $this->db->escape($data['filter_po_no']) . "%'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND date_added LIKE '%" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date']))) . "%'";
		}

		if(isset($data['filter_status'])){
			if($data['filter_status'] == '0'){
				$sql .= " AND status = 0 ";
			}elseif ($data['filter_status'] == '1') {
				$sql .= " AND status = 1 ";
			}
			elseif ($data['filter_status'] == '2') {
				$sql .= " AND status = 2 ";
			}
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTallys($data = array()) {
		$sql = "SELECT * FROM oc_tally_po";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_po_no'])) {
			$sql .= " AND po_no LIKE '%" . $this->db->escape($data['filter_po_no']) . "%'";
		}

		// echo'<pre>';
		// print_r($sql);
		// exit;

		$query = $this->db->query($sql);

		return $query->rows;
	}

}
