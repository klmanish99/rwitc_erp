<?php
class ModelCatalogtrainer extends Model {

	public function addCategory($data) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$code = $this->db->escape($data['hidden_trainer_code']);
		$Tcode =substr($code,1);
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$last_update_date = $date->format('Y-m-d h:i:s');
		$this->db->query("INSERT INTO trainers SET 
			name = '" .$this->db->escape($data['trainer_name']). "',
			trainer_code = '" .$Tcode. "',
			trainer_code_new = '" .$this->db->escape($data['trainer_code_new']). "',
			`active` = '" . $this->db->escape($data['isActive']) . "',
		    arrival_charges_to_be_paid = '" .$this->db->escape($data['arrival_charges_to_be_paid']). "',
		    racing_name = '" .$this->db->escape($data['racing_name']). "',
		    date_of_birth = '" .date('Y-m-d', strtotime($data['date_of_birth'])). "',
		    license_type = '" .$this->db->escape($data['license_type']). "',
		    status='".$this->db->escape($data['status'])."',
		 
		    date_of_license_issue = '" .date('Y-m-d', strtotime($data['date_of_license_issue'])). "',
		    date_of_license_issue2 = '" .date('Y-m-d', strtotime($data['date_of_license_issue2'])). "',
		    private_trainer = '" .$this->db->escape($data['private_trainer']). "',
		    file_number = '" .$this->db->escape($data['file_number']). "',
		    remarks = '" .$this->db->escape($data['remarks']). "',
		    epf_no = '" .$this->db->escape($data['epf_no']). "',
		    is_wita  = '" .$this->db->escape($data['is_wita']). "',
		    is_tfi  = '" .$this->db->escape($data['is_tfi']). "',
		    is_kta  = '" .$this->db->escape($data['is_kta']). "',
		    assistant_trainer_count = '".(int)$data['assistant_trainer']."',
		    assistant_trainer_1  = '" .$this->db->escape($data['assistant_trainer_1']). "',
		    assistant_trainer_2  = '" .$this->db->escape($data['assistant_trainer_2']). "',
		    assistant_trainer_3  = '" .$this->db->escape($data['assistant_trainer_3']). "',
		    assistant_trainer_4  = '" .$this->db->escape($data['assistant_trainer_4']). "',
		    assistant_trainer_5  = '" .$this->db->escape($data['assistant_trainer_5']). "',
			`file_number_uploads` = '". $this->db->escape($data['file_number_uploads']) ."',
			`uploaded_file_sources` = '". $this->db->escape($data['uploaded_file_sources']) ."',
			`user_id` = '".$this->session->data['user_id']."',
			`blood_group` = '". $this->db->escape($data['blood_group']) ."',
			`last_update_date` = '".$last_update_date."'
		    ");
		$trainer_get_last_td=$this->db->getLastId();
		
		$this->db->query("INSERT INTO trainer_contact_details SET
			trainer_id = '".(int)$trainer_get_last_td."',
			phone_no = '" .$this->db->escape($data['phone_no']). "',
		    mobile_no = '" .$this->db->escape($data['mobile_no']). "',
		    alternate_mob_no = '" .$this->db->escape($data['alternate_mob_no']). "',
		    email_id = '" .$this->db->escape($data['email_id']). "',
		    alternate_email_id = '" .$this->db->escape($data['alternate_email_id']). "',
		    address_line_1 = '" .$this->db->escape($data['address1']). "',
		    locality_1 = '" .$this->db->escape($data['localArea1']). "',
		    city_1 = '" .$this->db->escape($data['city1']). "',
		    pin_code_1 = '" .$this->db->escape($data['pincode1']). "',
		    state_1 = '" .$this->db->escape($data['zone_id']). "',
		    country_1 = '" .$this->db->escape($data['country1']). "',
		    address_line_2 = '" .$this->db->escape($data['address2']). "',
		    locality_2 = '" .$this->db->escape($data['localArea2']). "',
		    city_2 = '" .$this->db->escape($data['city2']). "',
		    pin_code_2 = '" .$this->db->escape($data['pincode2']). "',
		    state_2 = '" .$this->db->escape($data['zone_id2']). "',
		    country_2 = '" .$this->db->escape($data['country2']). "',
		    address_3 = '" .$this->db->escape($data['address_3']). "',
		    address_4 = '" .$this->db->escape($data['address_4']). "'
		    ");
		if(isset($data['itrdatas'])){
			foreach ($data['itrdatas'] as $key => $value) {
				$this->db->query("INSERT INTO trainer_itr SET
				trainer_id = '".(int)$trainer_get_last_td."',
				assesment_year = '".$this->db->escape($value['year'])."',
				file = '".$this->db->escape($value['image'])."',
				file_path = '".$this->db->escape($value['imagepath'])."' ");

			}
		}

		if(isset($data['private_trainers_owner_name'])){
			foreach ($data['private_trainers_owner_name'] as $key => $value) {
				$this->db->query("INSERT INTO `trainer_private_owner_names` SET
				trainer_id = '".(int)$trainer_get_last_td."',
				owner_id = '".$this->db->escape($value['owner_id'])."',
				owner_name = '".$this->db->escape($value['owner_name'])."'
				");

			}
		}


		return $id;
	}

	public function editCategory($id,$data) {
		//echo '<pre>';print_r($data);exit;
		$code = $this->db->escape($data['hidden_trainer_code']);
		$Tcode =substr($code,1);
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$last_update_date = $date->format('Y-m-d h:i:s');
		$this->db->query("UPDATE trainers SET
			name = '" .$this->db->escape($data['trainer_name']). "',
			trainer_code = '" .$Tcode. "',
			trainer_code_new = '" .$this->db->escape($data['trainer_code_new']). "',
			`active` = '" . $this->db->escape($data['isActive']) . "',
		    arrival_charges_to_be_paid = '" .$this->db->escape($data['arrival_charges_to_be_paid']). "',
		    racing_name = '" .$this->db->escape($data['racing_name']). "',
		    date_of_birth = '" .date('Y-m-d', strtotime($data['date_of_birth'])). "',
		    license_type = '" .$this->db->escape($data['license_type']). "',
		    status='".$this->db->escape($data['status'])."',
		    date_of_license_issue = '" .date('Y-m-d', strtotime($data['date_of_license_issue'])). "',
		    date_of_license_issue2 = '" .date('Y-m-d', strtotime($data['date_of_license_issue2'])). "',
		    private_trainer = '" .$this->db->escape($data['private_trainer']). "',
		    file_number = '" .$this->db->escape($data['file_number']). "',
		    remarks = '" .$this->db->escape($data['remarks']). "',
		    epf_no = '" .$this->db->escape($data['epf_no']). "',
		    gst_type = '".$this->db->escape($data['gst_type'])."',
			gst_no = '".$this->db->escape($data['gst_no'])."',
			pan_no = '".$this->db->escape($data['pan_no'])."',
			prof_tax_no = '".$this->db->escape($data['prof_tax_no'])."',
			is_wita  = '" .$this->db->escape($data['is_wita']). "',
			is_tfi  = '" .$this->db->escape($data['is_tfi']). "',
		    is_kta  = '" .$this->db->escape($data['is_kta']). "',
			assistant_trainer_count = '".(int)$data['assistant_trainer']."',
		    assistant_trainer_1  = '" .$this->db->escape($data['assistant_trainer_1']). "',
		    assistant_trainer_2  = '" .$this->db->escape($data['assistant_trainer_2']). "',
		    assistant_trainer_3  = '" .$this->db->escape($data['assistant_trainer_3']). "',
		    assistant_trainer_4  = '" .$this->db->escape($data['assistant_trainer_4']). "',
		    assistant_trainer_5  = '" .$this->db->escape($data['assistant_trainer_5']). "',
			`file_number_uploads` = '". $this->db->escape($data['file_number_uploads']) ."',
			`uploaded_file_sources` = '". $this->db->escape($data['uploaded_file_sources']) ."',
			`user_id` = '".$this->session->data['user_id']."',
			`blood_group` = '". $this->db->escape($data['blood_group']) ."',
			
			`last_update_date` = '".$last_update_date."'
		    WHERE id = '" . $id . "'
		    ");

		/*if ($data['date_of_license_renewal'] != $data['hidden_date_of_license_renewal']) {
			$this->db->query("INSERT INTO `trainer_renewal_history` SET trainer_id = '".$id."', renewal_date = '".date('Y-m-d', strtotime($data['date_of_license_renewal']))."' ");
		}*/

		$this->db->query("UPDATE trainer_contact_details SET 
			phone_no = '" .$this->db->escape($data['phone_no']). "',
		    mobile_no = '" .$this->db->escape($data['mobile_no1']). "',
		    alternate_mob_no = '" .$this->db->escape($data['alternate_mob_no']). "',
		    email_id = '" .$this->db->escape($data['email_id']). "',
		    alternate_email_id = '" .$this->db->escape($data['alternate_email_id']). "',
		    address_line_1 = '" .$this->db->escape($data['address1']). "',
		    locality_1 = '" .$this->db->escape($data['localArea1']). "',
		    city_1 = '" .$this->db->escape($data['city1']). "',
		    pin_code_1 = '" .$this->db->escape($data['pincode1']). "',
		    state_1 = '" .$this->db->escape($data['zone_id']). "',
		    country_1 = '" .$this->db->escape($data['country1']). "',
		    address_line_2 = '" .$this->db->escape($data['address2']). "',
		    locality_2 = '" .$this->db->escape($data['localArea2']). "',
		    city_2 = '" .$this->db->escape($data['city2']). "',
		    pin_code_2 = '" .$this->db->escape($data['pincode2']). "',
		    state_2 = '" .$this->db->escape($data['zone_id2']). "',
		    country_2 = '" .$this->db->escape($data['country2']). "',
		    address_3 = '" .$this->db->escape($data['address_3']). "',
		    address_4 = '" .$this->db->escape($data['address_4']). "'
		    WHERE trainer_id = '" . $id . "'
		 	");

		$this->db->query("DELETE FROM  trainer_itr WHERE trainer_id='".$id."' ");
		if(isset($data['itrdatas'])){
			foreach ($data['itrdatas'] as $key => $value) {
				$this->db->query("INSERT INTO trainer_itr SET
				trainer_id = '".(int)$id."',
				assesment_year = '".$this->db->escape($value['year'])."',
				file = '".$this->db->escape($value['image'])."',
				file_path = '".$this->db->escape($value['imagepath'])."' ");
			}
		}

		$this->db->query("DELETE FROM  trainer_private_owner_names WHERE trainer_id='".$id."' ");
		if(isset($data['private_trainers_owner_name'])){
			foreach ($data['private_trainers_owner_name'] as $key => $value) {
				$this->db->query("INSERT INTO `trainer_private_owner_names` SET
				trainer_id = '".(int)$id."',
				owner_id = '".$this->db->escape($value['owner_id'])."',
				owner_name = '".$this->db->escape($value['owner_name'])."'
				");

			}
		}

		$this->db->query("DELETE FROM `trainer_ban` WHERE trainer_id = '" .(int)$id . "'");
		if(isset($data['bandats'])){
			foreach ($data['bandats'] as $bkey => $bvalue) {
				$start_date_bans_remove_slash = str_replace("/", "-",$bvalue['start_date1']);
				$start_date11 = date("Y-m-d", strtotime($start_date_bans_remove_slash));
				
				if($bvalue['end_date1'] != ''){
					$end_date_bans_remove_slash = str_replace("/", "-",$bvalue['end_date1']);
					$end_date11 = date("Y-m-d", strtotime($end_date_bans_remove_slash));
				}else{
					$end_date11 = '0000-00-00';
				}


				if ($bvalue['start_date2'] != '') {
					$start_date_bans_remove_slash2 = str_replace("/", "-",$bvalue['start_date2']);
					$start_date22 = date("Y-m-d", strtotime($start_date_bans_remove_slash2));
				} else {
					$start_date22 = '0000-00-00';
				}
				
				if($bvalue['end_date2'] != ''){
					$end_date_bans_remove_slash2 = str_replace("/", "-",$bvalue['end_date2']);
					$end_date22 = date("Y-m-d", strtotime($end_date_bans_remove_slash2));
				}else{
					$end_date22 = '0000-00-00';
				}
				
				$this->db->query("INSERT INTO `trainer_ban` SET 
					trainer_id = '".(int)$id."',
					club = '".$this->db->escape($bvalue['club'])."',
					start_date1 = '".date('Y-m-d', strtotime($start_date11))."',
					end_date1 = '".$end_date11."',
					start_date2 = '".$start_date22."',
					end_date2 = '".$end_date22."',
					authority = '".$this->db->escape($bvalue['authority'])."', 
					reason = '".$this->db->escape($bvalue['reason'])."',
					amount = '".$this->db->escape($bvalue['amount'])."'

					");
			}
			
		}
		if(isset($data['banhistorydatas'])){
			foreach ($data['banhistorydatas'] as $hkey => $hvalue) {
				$start_date_bans_remove_slash = str_replace("/", "-",$hvalue['history_startdate_ban']);
				$start_date11 = date("Y-m-d", strtotime($start_date_bans_remove_slash));
				
				if($hvalue['history_enddate_ban'] != ''){
					$end_date_bans_remove_slash = str_replace("/", "-",$hvalue['history_enddate_ban']);
					$end_date11 = date("Y-m-d", strtotime($end_date_bans_remove_slash));
				}else{
					$end_date11 = '0000-00-00';
				}
				$start_date_bans_remove_slash2 = str_replace("/", "-",$hvalue['history_startdate_ban2']);
				$start_date22 = date("Y-m-d", strtotime($start_date_bans_remove_slash2));
				
				if($hvalue['history_enddate_ban2'] != ''){
					$end_date_bans_remove_slash2 = str_replace("/", "-",$hvalue['history_enddate_ban2']);
					$end_date22 = date("Y-m-d", strtotime($end_date_bans_remove_slash2));
				}else{
					$end_date22 = '0000-00-00';
				}
				$this->db->query("INSERT INTO `trainer_ban` SET 
					trainer_id = '".(int)$id."',
					club = '".$this->db->escape($hvalue['history_club'])."',
					start_date1 = '".date('Y-m-d', strtotime($start_date11))."',
					end_date1 = '".$end_date11."',
					start_date2 = '".$start_date22."',
					end_date2 = '".$end_date22."',
					authority = '".$this->db->escape($hvalue['history_authority'])."', 
					reason = '".$this->db->escape($hvalue['history_reason'])."',
					amount = '".$this->db->escape($hvalue['history_amount'])."'

					");
			}
			
		}
	}


	public function getTrainerFilter($data) {
		$sql = "SELECT * FROM trainers LEFT JOIN trainer_contact_details tcd ON (trainers.id = tcd.trainer_id)  WHERE 1=1";

		if (!empty($data['filter_trainer_name'])) {
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_trainer_name'])) . "%' ";
		}
		if (!empty($data['filter_trainer_code'])) {
			$sql .= " AND LOWER(trainer_code_new) LIKE '%" . $this->db->escape(strtolower($data['filter_trainer_code'])) . "%'";
		}

			$sql .= " AND active = '" . $this->db->escape($data['filter_status']) . "' ";


		$sort_data = array(
			'name',
			'trainer_code',
			'tcd.mobile_no',
			'tcd.email_id',
			'pan_no',
			'license_type'
			
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}


		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	}


	public function getTotalTrainers($data) {
		$sql = ("SELECT COUNT(*) AS total FROM `trainers` LEFT JOIN trainer_contact_details ON trainers.id =trainer_contact_details.trainer_id WHERE 1=1");
		if (!empty($data['filter_trainer_name'])) {
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_trainer_name'])) . "%' ";
		}
		if (!empty($data['filter_trainer_code'])) {
			$sql .= " AND LOWER(trainer_code_new) LIKE '%" . $this->db->escape(strtolower($data['filter_trainer_code'])) . "%'";
		}

			$sql .= " AND active = '" . $this->db->escape($data['filter_status']) . "' ";

		$query = $this->db->query($sql);
		return $query->row['total'];
	}


	public function editCategory1($id) {
		$sql="SELECT * FROM owner_to_trainer_authority WHERE autho_id='".$id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}

	public function editCategory2($id) {
		$sql="SELECT * FROM  sub_owner_to_trainer_authority WHERE sub_autho_id='".$id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}
	
	public function getTrainer($id) {
		$query = $this->db->query("SELECT * ,trainers.remarks AS trainer_remarks FROM trainers LEFT JOIN trainer_contact_details ON (trainers.id = trainer_contact_details.trainer_id) LEFT JOIN owner_to_trainer_authority ON (owner_to_trainer_authority.trainer_id =trainers.id ) WHERE trainers.id ='".$id."'");
		return $query->row;
	}

	public function getAuthorityss($trainer_id) {
		$query = $this->db->query("SELECT *  FROM  owner_to_trainer_authority  WHERE trainer_id='".$trainer_id."'");
		return $query->row;
	}

	public function getownername($owner_name) {

		$sql =("SELECT owner_name , id FROM owners WHERE 1 = 1 AND active = 1");
		if($owner_name != ''){
			$sql .= " AND `owner_name` LIKE '%" . $this->db->escape($owner_name) . "%'";
		}
		$sql .= " ORDER BY `owner_name` ASC LIMIT 0, 100";
		
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function gettrainername($trainer_name) {

		$sql =("SELECT name , id FROM trainers WHERE 1 = 1 AND active = 1");
		if($trainer_name != ''){
			$sql .= " AND `name` LIKE '%" . $this->db->escape($trainer_name) . "%'";
		}
		$sql .= " ORDER BY `name` ASC LIMIT 0, 100";
		
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getBanhistory($id) {
		$current_date = date("Y-m-d");
		$sql =("SELECT * FROM trainer_ban WHERE `trainer_id`= '".$id."' AND end_date1 <'".$current_date."' AND end_date1 <>'0000-00-00'  ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getStaffs($id) {
		$sql =("SELECT * FROM trainer_staff_entry WHERE `trainer_id` = '".$id."' ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getBandetails($id) {
		$current_date = date("Y-m-d");
		$sql =("SELECT * FROM trainer_ban WHERE `trainer_id`= '".$id."' AND end_date1 >='".$current_date."' ||   end_date1='0000-00-00'   ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getTrainerss($data) {  
		$sql = "SELECT * FROM trainers LEFT JOIN trainer_contact_details ON (trainers.id = trainer_contact_details.trainer_id)  WHERE 1=1";
			
			$sql .= " AND active = '" . $this->db->escape($data['filter_status']) . "' ";
			
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getTraineritr($trainer_id) {  
		$sql = "SELECT *  FROM  trainer_itr  WHERE trainer_id='".$trainer_id."' ORDER BY id";
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getAuthority($trainer_id) {
		$sql = "SELECT *  FROM  owner_to_trainer_authority  WHERE trainer_id='".$trainer_id."'";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getSubAuthority($trainer_id) {
		$sql = "SELECT *  FROM  sub_owner_to_trainer_authority  WHERE trainer_id='".$trainer_id."'";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	

	public function getTotalItrupload() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM trainer_itr");
		return $query->row['total'];
	}

	public function getTotalCategories() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM trainers WHERE active=1");
		return $query->row['total'];
	}

	public function getTotalAuthority() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM owner_to_trainer_authority
			 WHERE 1=1");
		return $query->row['total'];
	}

	public function getTotalSubAuthority() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM sub_owner_to_trainer_authority
			 WHERE 1=1");
		return $query->row['total'];
	}

	public function deleteTrainers($id) {
		$this->db->query("DELETE FROM trainers WHERE id = '" . (int)$id . "'");

		$this->db->query("DELETE FROM trainer_contact_details WHERE trainer_id = '" . (int)$id . "'");
		$this->db->query("DELETE FROM trainer_ban WHERE trainer_id = '" . (int)$id . "'");


		$this->db->query("DELETE FROM owner_to_trainer_authority WHERE trainer_id = '" . (int)$id . "'");
		$this->db->query("DELETE FROM trainer_itr WHERE trainer_id = '" . (int)$id . "'");
	}

	public function getTrainersAuto($to_owner) {

		$sql =("SELECT name,trainer_code,id FROM trainers WHERE 1 = 1");
		if($to_owner != ''){
			$sql .= " AND `name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		$sql .= " ORDER BY `name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getPrivateTrainerOwnersDatas($id){
		$private_trainer_owners_datas =  $this->db->query("SELECT * FROM `trainer_private_owner_names` WHERE trainer_id ='".$id."' ")->rows;

		return $private_trainer_owners_datas;
	}
	
}
