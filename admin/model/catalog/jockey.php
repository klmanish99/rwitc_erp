<?php
class Modelcatalogjockey extends Model {
	public function addCategory($data) {
		//echo "<pre>";print_r($data);exit;

		if ($data['age'] >= 25) {
			$allowance_claiming = '';
		} elseif ($data['apprentice'] == 'Yes') {
			if ($data['total_wins'] <=9) {
				$allowance_claiming = '-5kg';
			}
			if ($data['total_wins'] >=10 && $data['total_wins'] <=19) {
				$allowance_claiming = '-3.5kg';
			}
			if ($data['total_wins'] >=20 && $data['total_wins'] <=29) {
				$allowance_claiming = '-2.5kg';
			}
			if ($data['total_wins'] >=30 && $data['total_wins'] <=39) {
				$allowance_claiming = '-1.5kg';
			}
		} else{
			$allowance_claiming = $this->db->escape($data['allowance_claiming']);
		}

		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));

		$last_update_date = $date->format('Y-m-d h:i:s');

		// if ($data['date_of_birth'] != '') {
		// 	$dob = $data['date_of_birth'];
		//     $birthdate = new DateTime($dob);
	 //        $today   = new DateTime('today');
	 //        $age = $birthdate->diff($today)->y;
		// }

		$code = $this->db->escape($data['hidden_jockey_code']);
		$jcode =substr($code,1);
		$this->db->query("INSERT INTO jockey SET
			jockey_code ='".$jcode."',
			racing_name ='".$this->db->escape($data['racing_name'])."',
			jockey_name ='".$this->db->escape($data['jockey_name'])."',

			Fathers_name ='".$this->db->escape($data['Fathers_name'])."',
			educational_qualification ='".$this->db->escape($data['educational_qualification'])."',
			emergency_contact_person ='".$this->db->escape($data['emergency_contact_person'])."',
			contact_no ='".$this->db->escape($data['contact_no'])."',

			jockey_code_new ='".$this->db->escape($data['jockey_code_new'])."',
			birth_date ='" . $this->db->escape(date('Y-m-d', strtotime($data['date_of_birth']))) . "',
			age ='".$this->db->escape($data['age'])."',
			origin ='".$this->db->escape($data['origin'])."',
			first_time_license_issued ='" . $this->db->escape(date('Y-m-d', strtotime($data['license_issued_date']))) . "',
			license_type ='".$this->db->escape($data['license_type'])."',
			center_holding_license_type_a ='".$this->db->escape($data['center_holding_license_type_a'])."',
			license_renewal_date ='" . $this->db->escape(date('Y-m-d', strtotime($data['license_renewal']))) . "',
			license_end_date ='" . $this->db->escape(date('Y-m-d', strtotime($data['license_end_date']))) . "',
			fees_paid ='".$this->db->escape($data['fees_paid'])."',
			ride_weight ='".$this->db->escape($data['ride_weight'])."',
			file_number ='".$this->db->escape($data['file_number'])."',
			file_upload ='".$this->db->escape($data['file'])."',
			blood_group ='".$this->db->escape($data['blood_group'])."',
			no_whip ='".$this->db->escape($data['no_whip'])."',
			apprentice ='".$this->db->escape($data['apprentice'])."',
			allowance_claiming ='".$allowance_claiming."',
			out_station_wins ='".$this->db->escape($data['out_station_wins'])."',
			station_wins ='".$this->db->escape($data['station_wins'])."',
			total_wins ='".$this->db->escape($data['total_wins'])."',
			master_trainer ='".$this->db->escape($data['master_trainer'])."',
			master_trainer_s_date ='".$this->db->escape(date('Y-m-d', strtotime($data['master_trainer_s_date']))) . "',
			master_trainer_e_date ='".$this->db->escape(date('Y-m-d', strtotime($data['master_trainer_e_date']))) . "',
			remarks ='".$this->db->escape($data['remarks'])."',
			jai_member ='".$this->db->escape($data['jaimember'])."',
			jockey_returned_by = '" .$this->db->escape($data['jockey_returned_by']). "',
			`file_number_upload` = '". $this->db->escape($data['file_number_upload']) ."',
			`uploaded_file_source` = '". $this->db->escape($data['uploaded_file_source']) ."',
			jocky_status='".$this->db->escape($data['isActive'])."',
			`user_id` = '".$this->session->data['user_id']."',
			`last_update_date` = '".$last_update_date."'
			");

		$jockey_get_last_td=$this->db->getLastId();
		
		$this->db->query("INSERT INTO jockey_contact_details SET
			jockey_id = '".(int)$jockey_get_last_td."',
			phone_no = '" .$this->db->escape($data['phone_no']). "',
		    mobile_no = '" .$this->db->escape($data['mobile_no1']). "',
		    alternate_mob_no = '" .$this->db->escape($data['alternate_mob_no']). "',
		    email_id = '" .$this->db->escape($data['email_id']). "',
		    alternate_email_id = '" .$this->db->escape($data['alternate_email_id']). "',
		    address_line_1 = '" .$this->db->escape($data['address1']). "',
		    locality_1 = '" .$this->db->escape($data['localArea1']). "',
		    city_1 = '" .$this->db->escape($data['city1']). "',
		    pin_code_1 = '" .$this->db->escape($data['pincode1']). "',
		    state_1 = '" .$this->db->escape($data['zone_id']). "',
		    country_1 = '" .$this->db->escape($data['country1']). "',
		    address_line_2 = '" .$this->db->escape($data['address2']). "',
		    locality_2 = '" .$this->db->escape($data['localArea2']). "',
		    city_2 = '" .$this->db->escape($data['city2']). "',
		    pin_code_2 = '" .$this->db->escape($data['pincode2']). "',
		    state_2 = '" .$this->db->escape($data['zone_id2']). "',
		    country_2 = '" .$this->db->escape($data['country2']). "',
		    address_3 = '" .$this->db->escape($data['address_3']). "',
		    address_4 = '" .$this->db->escape($data['address_4']). "',
		    epf_no = '" .$this->db->escape($data['epf_no']). "',
		    gst_type = '".$this->db->escape($data['gst_type'])."',
			gst_no = '".$this->db->escape($data['gst_no'])."',
			pan_no = '".$this->db->escape($data['pan_no'])."',
			prof_tax_no = '".$this->db->escape($data['prof_tax_no'])."'
		    ");

		if(isset($data['itrdatas'])){
			//echo "<pre>";print_r($data['itrdatas']);
			foreach ($data['itrdatas'] as $key => $value) {
				$this->db->query("INSERT INTO jockey_itr SET
				jockey_id = '".(int)$jockey_get_last_td."',
				assesment_year = '".$this->db->escape($value['year'])."',
				file = '".$this->db->escape($value['image'])."',
				file_path = '".$this->db->escape($value['imagepath'])."' ");

			}
		}
		if(isset($data['out_station_name'])){
			//echo "innnnn";
			foreach ($data['out_station_name'] as $key => $value) {
				$this->db->query("INSERT INTO oc_out_station SET jockey_id = '".(int)$jockey_get_last_td."', out_station_name = '".$this->db->escape($value['name'])."' ");
			}
		}
		if(isset($data['master_trainer'])){
			$this->db->query("INSERT INTO jockey_trainer_history SET
				jockey_id = '".(int)$jockey_get_last_td."',
				trainer_history = '".$this->db->escape($data['master_trainer'])."'
		    ");
		}

		if(isset($data['private_trainers_owner_name'])){
			foreach ($data['private_trainers_owner_name'] as $key => $value) {
				$this->db->query("INSERT INTO `jockey_returned_by` SET
				jockey_id = '".(int)$jockey_get_last_td."',
				owner_id = '".$this->db->escape($value['owner_id'])."',
				owner_name = '".$this->db->escape($value['owner_name'])."'
				");

			}
		}
		return $jockey_id;
	}

	public function editCategory($jockey_id, $data) {
		//echo "<pre>";print_r($data);exit;
		if ($data['age'] >= 25) {
			$allowance_claiming = '';
		} elseif ($data['apprentice'] == 'Yes') {
			if ($data['total_wins'] <=9) {
				$allowance_claiming = '-5kg';
			}
			if ($data['total_wins'] >=10 && $data['total_wins'] <=19) {
				$allowance_claiming = '-3.5kg';
			}
			if ($data['total_wins'] >=20 && $data['total_wins'] <=29) {
				$allowance_claiming = '-2.5kg';
			}
			if ($data['total_wins'] >=30 && $data['total_wins'] <=39) {
				$allowance_claiming = '-1.5kg';
			}
		} else{
			$allowance_claiming = $this->db->escape($data['allowance_claiming']);
		}
		
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));

		$last_update_date = $date->format('Y-m-d h:i:s');
		
		//echo "<pre>";print_r($last_update_date);exit;

		// if ($data['date_of_birth'] != '') {
		// 	$dob = $data['date_of_birth'];
		//     $birthdate = new DateTime($dob);
	 //        $today   = new DateTime('today');
	 //        $age = $birthdate->diff($today)->y;
		//     //echo $age;exit;
		// }

		$code = $this->db->escape($data['hidden_jockey_code']);
		$jcode =substr($code,1);
		$this->db->query("UPDATE jockey SET
			jockey_code ='".$jcode."',
			racing_name ='".$this->db->escape($data['racing_name'])."',
			jockey_name ='".$this->db->escape($data['jockey_name'])."',

			Fathers_name ='".$this->db->escape($data['Fathers_name'])."',
			educational_qualification ='".$this->db->escape($data['educational_qualification'])."',
			emergency_contact_person ='".$this->db->escape($data['emergency_contact_person'])."',
			contact_no ='".$this->db->escape($data['contact_no'])."',

			jockey_code_new ='".$this->db->escape($data['jockey_code_new'])."',
			birth_date ='" . $this->db->escape(date('Y-m-d', strtotime($data['date_of_birth']))) . "',
			age ='".$this->db->escape($data['age'])."',
			origin ='".$this->db->escape($data['origin'])."',
			first_time_license_issued ='" . $this->db->escape(date('Y-m-d', strtotime($data['license_issued_date']))) . "',
			license_type ='".$this->db->escape($data['license_type'])."',
			center_holding_license_type_a ='".$this->db->escape($data['center_holding_license_type_a'])."',
			license_renewal_date ='" . $this->db->escape(date('Y-m-d', strtotime($data['license_renewal']))) . "',
			license_end_date ='" . $this->db->escape(date('Y-m-d', strtotime($data['license_end_date']))) . "',
			ride_weight ='".$this->db->escape($data['ride_weight'])."',
			file_number ='".$this->db->escape($data['file_number'])."',
			file_upload ='".$this->db->escape($data['file'])."',
			blood_group ='".$this->db->escape($data['blood_group'])."',
			no_whip ='".$this->db->escape($data['no_whip'])."',
			apprentice ='".$this->db->escape($data['apprentice'])."',
			allowance_claiming ='".$allowance_claiming."',
			out_station_wins ='".$this->db->escape($data['out_station_wins'])."',
			station_wins ='".$this->db->escape($data['station_wins'])."',
			total_wins ='".$this->db->escape($data['total_wins'])."',
			master_trainer ='".$this->db->escape($data['master_trainer'])."',
			master_trainer_s_date ='".$this->db->escape(date('Y-m-d', strtotime($data['master_trainer_s_date']))) . "',
			master_trainer_e_date ='".$this->db->escape(date('Y-m-d', strtotime($data['master_trainer_e_date']))) . "',
			remarks ='".$this->db->escape($data['remarks'])."',
			jai_member ='".$this->db->escape($data['jaimember'])."',
			jockey_returned_by = '" .$this->db->escape($data['jockey_returned_by']). "',
			`file_number_upload` = '". $this->db->escape($data['file_number_upload']) ."',
			`uploaded_file_source` = '". $this->db->escape($data['uploaded_file_source']) ."',
			jocky_status='".$this->db->escape($data['isActive'])."',
			`user_id` = '".$this->session->data['user_id']."',
			`last_update_date` = '".$last_update_date."'
			WHERE jockey_id = '" . $jockey_id . "'
		    ");

		$this->db->query("UPDATE jockey_contact_details SET 
			phone_no = '" .$this->db->escape($data['phone_no']). "',
		    mobile_no = '" .$this->db->escape($data['mobile_no1']). "',
		    alternate_mob_no = '" .$this->db->escape($data['alternate_mob_no']). "',
		    email_id = '" .$this->db->escape($data['email_id']). "',
		    alternate_email_id = '" .$this->db->escape($data['alternate_email_id']). "',
		    address_line_1 = '" .$this->db->escape($data['address1']). "',
		    locality_1 = '" .$this->db->escape($data['localArea1']). "',
		    city_1 = '" .$this->db->escape($data['city1']). "',
		    pin_code_1 = '" .$this->db->escape($data['pincode1']). "',
		    state_1 = '" .$this->db->escape($data['zone_id']). "',
		    country_1 = '" .$this->db->escape($data['country1']). "',
		    address_line_2 = '" .$this->db->escape($data['address2']). "',
		    locality_2 = '" .$this->db->escape($data['localArea2']). "',
		    city_2 = '" .$this->db->escape($data['city2']). "',
		    pin_code_2 = '" .$this->db->escape($data['pincode2']). "',
		    epf_no = '" .$this->db->escape($data['epf_no']). "',
		    state_2 = '" .$this->db->escape($data['zone_id2']). "',
		    country_2 = '" .$this->db->escape($data['country2']). "',
		    address_3 = '" .$this->db->escape($data['address_3']). "',
		    address_4 = '" .$this->db->escape($data['address_4']). "',
		    gst_type = '".$this->db->escape($data['gst_type'])."',
			gst_no = '".$this->db->escape($data['gst_no'])."',
			pan_no = '".$this->db->escape($data['pan_no'])."',
			prof_tax_no = '".$this->db->escape($data['prof_tax_no'])."'
		    WHERE jockey_id = '" . $jockey_id . "'
		 	");

		$this->db->query("DELETE FROM  jockey_itr WHERE jockey_id='".$jockey_id."' ");
		if(isset($data['itrdatas'])){
			foreach ($data['itrdatas'] as $key => $value) {
				$this->db->query("INSERT INTO jockey_itr SET
				jockey_id = '".(int)$jockey_id."',
				assesment_year = '".$this->db->escape($value['year'])."',
				file = '".$this->db->escape($value['image'])."',
				file_path = '".$this->db->escape($value['imagepath'])."' ");
			}
		}
		$this->db->query("DELETE FROM  oc_out_station WHERE jockey_id='".$jockey_id."' ");
		if(isset($data['out_station_name'])){
			//echo "innnnn";
			foreach ($data['out_station_name'] as $key => $value) {
				$this->db->query("INSERT INTO oc_out_station SET 
					jockey_id = '".(int)$jockey_id."', 
					out_station_name = '".$this->db->escape($value['name'])."' ");
			}
		}

		if(isset($data['master_trainer']) != $data['hidden_master_trainer']){
			$this->db->query("INSERT INTO jockey_trainer_history SET
				jockey_id = '".(int)$jockey_id."',
				trainer_history = '".$this->db->escape($data['master_trainer'])."'
		    ");
		}

		$this->db->query("DELETE FROM  jockey_returned_by WHERE jockey_id='".$jockey_id."' ");
		if(isset($data['private_trainers_owner_name'])){
			foreach ($data['private_trainers_owner_name'] as $key => $value) {
				$this->db->query("INSERT INTO `jockey_returned_by` SET
				jockey_id = '".(int)$jockey_id."',
				owner_id = '".$this->db->escape($value['owner_id'])."',
				owner_name = '".$this->db->escape($value['owner_name'])."'
				");

			}
		}

		$this->db->query("DELETE FROM `jockey_ban` WHERE jockey_id = '" .(int)$jockey_id . "'");
		if(isset($data['bandats'])){
			foreach ($data['bandats'] as $bkey => $bvalue) {
				$start_date_bans_remove_slash = str_replace("/", "-",$bvalue['start_date1']);
				$start_date11 = date("Y-m-d", strtotime($start_date_bans_remove_slash));
				
				if($bvalue['end_date1'] != ''){
					$end_date_bans_remove_slash = str_replace("/", "-",$bvalue['end_date1']);
					$end_date11 = date("Y-m-d", strtotime($end_date_bans_remove_slash));
				}else{
					$end_date11 = '0000-00-00';
				}


				if($bvalue['start_date2'] != ''){
					$start_date_bans_remove_slash2 = str_replace("/", "-",$bvalue['start_date2']);
					$start_date22 = date("Y-m-d", strtotime($start_date_bans_remove_slash2));
				}else{
					$start_date22 = '0000-00-00';
				}
				
				if($bvalue['end_date2'] != ''){
					$end_date_bans_remove_slash2 = str_replace("/", "-",$bvalue['end_date2']);
					$end_date22 = date("Y-m-d", strtotime($end_date_bans_remove_slash2));
				}else{
					$end_date22 = '0000-00-00';
				}
				
				$this->db->query("INSERT INTO `jockey_ban` SET 
					jockey_id = '".(int)$jockey_id."',
					club = '".$this->db->escape($bvalue['club'])."',
					start_date1 = '".$start_date11."',
					end_date1 = '".$end_date11."',
					start_date2 = '".$start_date22."',
					end_date2 = '".$end_date22."',
					authority = '".$this->db->escape($bvalue['authority'])."', 
					reason = '".$this->db->escape($bvalue['reason'])."',
					amount = '".$this->db->escape($bvalue['amount'])."'

					");
				
			}
			
		}
		if(isset($data['banhistorydatas'])){
			foreach ($data['banhistorydatas'] as $hkey => $hvalue) {
				$start_date_bans_remove_slash = str_replace("/", "-",$hvalue['history_startdate_ban']);
				$start_date11 = date("Y-m-d", strtotime($start_date_bans_remove_slash));
				
				if($hvalue['history_enddate_ban'] != ''){
					$end_date_bans_remove_slash = str_replace("/", "-",$hvalue['history_enddate_ban']);
					$end_date11 = date("Y-m-d", strtotime($end_date_bans_remove_slash));
				}else{
					$end_date11 = '0000-00-00';
				}
				$start_date_bans_remove_slash2 = str_replace("/", "-",$hvalue['history_startdate_ban2']);
				$start_date22 = date("Y-m-d", strtotime($start_date_bans_remove_slash2));
				
				if($hvalue['history_enddate_ban2'] != ''){
					$end_date_bans_remove_slash2 = str_replace("/", "-",$hvalue['history_enddate_ban2']);
					$end_date22 = date("Y-m-d", strtotime($end_date_bans_remove_slash2));
				}else{
					$end_date22 = '0000-00-00';
				}
				$this->db->query("INSERT INTO `jockey_ban` SET 
					jockey_id = '".(int)$jockey_id."',
					club = '".$this->db->escape($hvalue['history_club'])."',
					start_date1 = '".$start_date11."',
					end_date1 = '".$end_date11."',
					start_date2 = '".$start_date22."',
					end_date2 = '".$end_date22."',
					authority = '".$this->db->escape($hvalue['history_authority'])."', 
					reason = '".$this->db->escape($hvalue['history_reason'])."',
					amount = '".$this->db->escape($hvalue['history_amount'])."'

					");
			}
			
		}//exit;
		
	}

	public function deleteCategory($jockey_id) {
		$this->db->query("DELETE FROM jockey WHERE jockey_id = '" . (int)$jockey_id . "'");
		
		$this->db->query("DELETE FROM jockey_ban WHERE jockey_id = '" . (int)$jockey_id . "'");

		$this->db->query("DELETE FROM jockey_contact_details WHERE jockey_id = '" . (int)$jockey_id . "'");

		$this->db->query("DELETE FROM jockey_itr WHERE jockey_id = '" . (int)$jockey_id . "'");

	}

	public function getJockeyitr($jockey_id) {  
		$sql = "SELECT *  FROM  jockey_itr  WHERE jockey_id='".$jockey_id."'";
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getCategory($jockey_id) {  
		$query = $this->db->query("SELECT * FROM jockey LEFT JOIN jockey_contact_details ON (jockey.jockey_id = jockey_contact_details.jockey_id)  WHERE jockey.jockey_id ='".$jockey_id."'");
		return $query->row;
	}
	
	public function getCategories($data) {
		$sql = "SELECT * FROM jockey LEFT JOIN jockey_contact_details jcd ON (jockey.jockey_id = jcd.jockey_id)   WHERE 1=1";

		if (!empty($data['filter_jockey_name'])) {
			$sql .= " AND LOWER(jockey_name) LIKE '%" . $this->db->escape(strtolower($data['filter_jockey_name'])) . "%' ";
		}
		if (!empty($data['filter_jockey_code'])) {
			$sql .= " AND LOWER(jockey_code_new) LIKE '%" . $this->db->escape(strtolower($data['filter_jockey_code'])) . "%'";
		}
		$sql .= " AND jocky_status = '" . $this->db->escape($data['filter_status']) . "' ";

		$sort_data = array(
			'jockey_code',
			'jockey_name',
			'jcd.mobile_no',
			'jcd.email_id',
			'jcd.pan_no',
			'license_type',
			'origin',
			'allowance_claiming',
			'jocky_status',
			'apprentice'
			
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY jockey_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;

	}

	public function getTotalCategories($data) {
		$sql = ("SELECT COUNT(*) AS total FROM `jockey` LEFT JOIN jockey_contact_details ON jockey.jockey_id =jockey_contact_details.jockey_id WHERE 1=1");
		if (!empty($data['filter_jockey_name'])) {
			$sql .= " AND LOWER(jockey_name) LIKE '%" . $this->db->escape(strtolower($data['filter_jockey_name'])) . "%' ";
		}
		if (!empty($data['filter_jockey_code'])) {
			$sql .= " AND LOWER(jockey_code) LIKE '%" . $this->db->escape(strtolower($data['filter_jockey_code'])) . "%'";
		}
		$sql .= " AND jocky_status = '" . $this->db->escape($data['filter_status']) . "' ";
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getTotalCategoriess($data = array()) {


		$sql = "SELECT COUNT(*) AS total FROM `jockey`WHERE jocky_status=1 ";

		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}


	public function getBanhistory($jockey_id) {
		$current_date = date("Y-m-d");
		$sql =("SELECT * FROM jockey_ban WHERE `jockey_id`= '".$jockey_id."' AND end_date1 <'".$current_date."'  AND end_date1 <>'0000-00-00' ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getBandetails($jockey_id) {
		$current_date = date("Y-m-d");
		$sql =("SELECT * FROM jockey_ban WHERE `jockey_id`= '".$jockey_id."' AND end_date1 >='".$current_date."' ||   end_date1='0000-00-00' ");
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getJockeysAuto($to_owner) {

		$sql =("SELECT jockey_name,jockey_code,jockey_id FROM  jockey WHERE 1 = 1");
		if($to_owner != ''){
			$sql .= " AND `jockey_name` LIKE '%" . $this->db->escape($to_owner) . "%'";
		}
		$sql .= " ORDER BY `jockey_name` ASC LIMIT 0, 100";
		//$sql .= "GROUP BY OWNCODE ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getsupplier($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM trainers";
			
		$sql .= " WHERE 1=1";
		
		if (!empty($data['filter_trainer'])) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_trainer']) . "%'";
		}

		$sort_data = array(
			'order_no',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getPrivateTrainerOwnersDatas($jockey_id){
		$private_jockey_owners_datas =  $this->db->query("SELECT * FROM `jockey_returned_by` WHERE jockey_id ='".$jockey_id."' ")->rows;

		return $private_jockey_owners_datas;
	}
	
}
