<?php
class ModelCatalogRaceType extends Model {
	public function addRaceType($data) {
	// echo'<pre>';
	// print_r($data);
	// exit;

	if(isset($data['race_grade']) && ($data['race_grade'] != '')){
		$data['race_class'] = $data['race_grade'];
	} 

	$type_of_option = ($data['race_class'] != '' && $data['race_class'] != '0') ? $data['race_class'] : $data['race_grade'];

	$this->db->query("INSERT INTO racetypes SET 
	race_type = '" . $this->db->escape($data['race_type']) . "',
	race_class ='".$this->db->escape($type_of_option)."',
	description ='".$this->db->escape($data['desc'])."',
	category ='".$this->db->escape($data['cat'])."',
	rating_from = '" . $this->db->escape($data['rating_from']) . "',
	rating_to = '" . $this->db->escape($data['rating_to']) . "',
	l_description ='".$this->db->escape($data['l_desc'])."',
	l_category ='".$this->db->escape($data['l_cat'])."',
	l_rating_from = '" . $this->db->escape($data['l_rating_from']) . "',
	l_rating_to = '" . $this->db->escape($data['l_rating_to']) . "'
	");
	$id = $this->db->getLastId();

		return $id;
	}


	public function editRaceType($id,$data) {
		// echo'<pre>';
		// print_r($data);
		// exit;

		if ($data['race_type'] == 'Handicap Race') {
			$type_of_option = $data['race_class'];
		} elseif ($data['race_type'] == 'Term Race') {
			$type_of_option = $data['race_grade'];
		}

		//$type_of_option = ($data['race_class'] != '' && $data['race_class'] != '0') ? $data['race_class'] : $data['race_grade'];
		
		// echo'<pre>';
		// print_r($type_of_option);
		// exit;
		$this->db->query("UPDATE racetypes SET 
		race_type = '" . $this->db->escape($data['race_type']) . "',
		race_class ='".$this->db->escape($type_of_option)."',
		description ='".$this->db->escape($data['desc'])."',
		category ='".$this->db->escape($data['cat'])."',
		rating_from = '" . $this->db->escape($data['rating_from']) . "',
		rating_to = '" . $this->db->escape($data['rating_to']) . "',
		l_description ='".$this->db->escape($data['l_desc'])."',
		l_category ='".$this->db->escape($data['l_cat'])."',
		l_rating_from = '" . $this->db->escape($data['l_rating_from']) . "',
		l_rating_to = '" . $this->db->escape($data['l_rating_to']) . "'
		WHERE id ='".$id."'
		");

		
	}

	

	public function deleteRaceType($id) {
		$this->db->query("DELETE FROM racetypes WHERE id = '" . (int)$id . "'");
	}

	public function getRaceTypes($data = array()) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$sql = "SELECT *  FROM  racetypes  WHERE 1=1 ";

		if (!empty($data['filter_racetypes_name'])) {
			$sql .= " AND med_name LIKE '" . $this->db->escape($data['filter_racetypes_name']) . "%'";
		}

		if (!empty($data['filter_racetypes_id'])) {
			$sql .= " AND med_code = '" . $this->db->escape($data['filter_racetypes_id']) . "' ";
		}

		if (isset($data['filter_status'])) {
			$sql .= " AND isActive = '" . $this->db->escape($data['filter_status']) . "' ";
		}

		/*echo'<pre>';
		print_r($sql);
		exit;*/

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	
	}

	public function getRaceType($id) {  
		$sql = "SELECT *  FROM  racetypes  WHERE id='".$id."'";
		$query = $this->db->query($sql)->row;
		return $query;
		//echo "<PRE>";print_r($query);echo "</PRE>";die('end line');
	}

	public function getTotalRaceType() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM racetypes");

		return $query->row['total'];
	}

	public function getRaceTypeAuto($med_name) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT med_name,med_code,id FROM  racetypes WHERE 1 = 1");
		if($med_name != ''){
			$sql .= " AND `med_name` LIKE '%" . $this->db->escape($med_name) . "%'";
		}
		
		$sql .= " ORDER BY `med_code` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getRaceTypeAutos($med_code) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT med_name,med_code,id FROM  racetypes WHERE 1 = 1");
		if($med_code != ''){
			$sql .= " AND `med_code` LIKE '%" . $this->db->escape($med_code) . "%'";
		}

		$sql .= " ORDER BY `med_code` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}
}
