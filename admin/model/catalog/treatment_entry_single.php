<?php
class ModelCatalogtreatmententrysingle extends Model {
	public function addTreatmentEntry($data) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$this->db->query("INSERT INTO `oc_treatment_entry` SET 
			`issue_no` = '".$data['treatment_number']."',
			`clinic_id` = '".$data['filterParentId']."',
			`clinic_name` = '".$data['filter_parent_doctor']."',
			`doctor_id` = '".$data['to_doc']."',
			`entry_date` = '".date('Y-m-d', strtotime($data['date']))."',
			`total_item` = '".$data['total_item']."',
			`total_qty` = '".$data['total_qty']."',
			`total_amt` = '".$data['total']."',
			`horse_name` = '".$this->db->escape($data['horse_name'])."',
			`horse_name_id` = '".$data['hidden_horse_name_id']."',
			`trainer_name` = '".$this->db->escape($data['trainer_name'])."',
			`trainer_name_id` = '".$data['hidden_trainer_name_id']."'
		");
		$trans_id = $this->db->getLastId();

		foreach ($data['productraw_datas'] as $key => $value) {
			$niddle1 = ($value['syringe_one'] != '') ? "2" : "0";
			$niddle2 = ($value['syringe_two'] != '') ? "2" : "0";
			$niddle3 = ($value['syringe_three'] != '') ? "2" : "0";
			$niddle4 = ($value['syringe_four'] != '') ? "2" : "0";
			$niddle5 = ($value['syringe_five'] != '') ? "2" : "0";
			$niddle6 = ($value['syringe_six'] != '') ? "2" : "0";
			$niddle7 = ($value['syringe_seven'] != '') ? "2" : "0";

			$this->db->query("INSERT INTO `oc_treatment_entry_trans` SET 
				`parent_id` = '".$trans_id."',
				`medicine_code` = '".$value['input_item_code']."',
				`medicine_name` = '".$this->db->escape($value['item_name'])."',
				`medicine_qty` = '".$value['input_qty']."',
				`unit` = '".$value['input_unit']."',
				`rate` = '".$value['input_purchase_prise']."',
				`value` = '".$value['input_value']."',
				`syringe_one` = '".$value['syringe_one']."',
				`syringe_two` = '".$value['syringe_two']."',
				`syringe_three` = '".$value['syringe_three']."',
				`syringe_four` = '".$value['syringe_four']."',
				`syringe_five` = '".$value['syringe_five']."',
				`syringe_six` = '".$value['syringe_six']."',
				`syringe_seven` = '".$value['syringe_seven']."',
				`needle_one` = '".$niddle1."',
				`needle_two` = '".$niddle2."',
				`needle_three` = '".$niddle3."',
				`needle_four` = '".$niddle4."',
				`needle_five` = '".$niddle5."',
				`needle_six` = '".$niddle6."',
				`needle_seven` = '".$niddle7."'

			");
		}

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');


		$this->db->query("INSERT INTO `treate_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `treate_id` = '".$trans_id."' ");
		
		
	}

	public function deleteinward($order_id) {
		$this->db->query("DELETE FROM oc_inward WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM oc_inwarditem WHERE order_id = '" . (int)$order_id . "'");
	}

	public function getTransectionEntryParent($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM oc_treatment_entry WHERE id = '" . (int)$id . "'");
		return $query->row;
	}


	public function getDoctorAutos($doctor_name) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT * FROM  doctor WHERE 1=1 AND isActive = 'Active' ");
		if($doctor_name != ''){
			$sql .= " AND `doctor_name` LIKE '%" . $this->db->escape($doctor_name) . "%'";
		}
		
		$sql .= " ORDER BY `doctor_name` ";

		//echo $sql;exit;
		

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getChildDoctorAutos($child_doctor_code) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT * FROM  doctor WHERE isActive = 'Active' AND is_parent = 0 ");
		if($child_doctor_code != ''){
			$sql .= " AND `parent_id` = '" . ($child_doctor_code) . "' ";
		}
		
		$sql .= " ORDER BY `doctor_name` ";

		//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getHorseAutos($trainer_id) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT * FROM `horse_to_trainer` WHERE trainer_id = '".$trainer_id."' AND trainer_status = '1' ORDER BY `left_date_of_charge` DESC ");
		

		$query = $this->db->query($sql)->rows;
		return $query;
	}
}
