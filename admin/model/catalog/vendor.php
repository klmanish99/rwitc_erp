<?php
class ModelCatalogVendor extends Model {
	public function addVendor($data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if($data['date_of_incorporation'] != '00-00-0000' && $data['date_of_incorporation'] != '' && $data['date_of_incorporation'] != '01-01-1970'){
			$date_of_incorporation = date('Y-m-d', strtotime($data['date_of_incorporation']));
		} else {
			$date_of_incorporation = '0000-00-00';
		}
		if(!isset($data['percent_text'])){
			$data['percent_text'] = '';
		}
		$this->db->query("INSERT INTO is_vendor SET 
							vendor_name = '" . $this->db->escape($data['vendor_name']) . "',
							place = '" . $this->db->escape($data['place']) . "',
							gst_no = '" . $this->db->escape($data['gst_no']) . "',
							vendor_code = '" . $this->db->escape($data['vendor_code']) . "',
							filename = '" . $this->db->escape($data['filename']) . "', 
							mask = '" . $this->db->escape($data['mask']) . "',
							`uploaded_file_source` = '". $this->db->escape($data['uploaded_file_source']) ."',
							filename2 = '" . $this->db->escape($data['filename2']) . "', 
							mask2 = '" . $this->db->escape($data['mask2']) . "',
							`uploaded_file_source2` = '". $this->db->escape($data['uploaded_file_source2']) ."',
							filename3 = '" . $this->db->escape($data['filename3']) . "', 
							mask3 = '" . $this->db->escape($data['mask3']) . "',
							`uploaded_file_source3` = '". $this->db->escape($data['uploaded_file_source3']) ."',
							filename4 = '" . $this->db->escape($data['filename4']) . "', 
							mask4 = '" . $this->db->escape($data['mask4']) . "',
							`uploaded_file_source4` = '". $this->db->escape($data['uploaded_file_source4']) ."',
							filename5 = '" . $this->db->escape($data['filename5']) . "', 
							mask5 = '" . $this->db->escape($data['mask5']) . "', 
							isActive = '" . $this->db->escape($data['isActive']) . "'
						");
		//supplier_code = '" . $this->db->escape($data['supplier_code']) . "',
		$vendor_id = $this->db->getLastId();

		$this->db->query("DELETE FROM `is_vendor_contact` WHERE `vendor_id` = '".$vendor_id."' ");
		if(isset($data['contact_datas'])){
			foreach($data['contact_datas'] as $ckey => $cvalue){
				$this->db->query("INSERT INTO `is_vendor_contact` SET
						name = '" . $this->db->escape($cvalue['name']) . "',
						designation = '" . $this->db->escape($cvalue['designation']) . "',
						phone_no = '" . $this->db->escape($cvalue['phone_no']) . "',
						mobile1 = '" . $this->db->escape($cvalue['mobile1']) . "',
						mobile2 = '" . $this->db->escape($cvalue['mobile2']) . "',
						email_id = '" . $this->db->escape($cvalue['email_id']) . "',
						vendor_code = '" . $this->db->escape($data['vendor_code']) . "',
						vendor_id = '" . $this->db->escape($vendor_id) . "'
						");
			}
		}

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');


		$this->db->query("INSERT INTO `supplier_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `supp_id` = '".$vendor_id."' ");

		return $vendor_id;
	}

	public function editVendor($vendor_id, $data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if($data['date_of_incorporation'] != '00-00-0000' && $data['date_of_incorporation'] != '' && $data['date_of_incorporation'] != '01-01-1970'){
			$date_of_incorporation = date('Y-m-d', strtotime($data['date_of_incorporation']));
		} else {
			$date_of_incorporation = '0000-00-00';
		}
		// if(isset($data['materials'])){
		// 	$materials = json_encode($data['materials'])
		// }
		if(!isset($data['percent_text'])){
			$data['percent_text'] = '';
		}
		$this->db->query("UPDATE is_vendor SET 
							vendor_name = '" . $this->db->escape($data['vendor_name']) . "',
							place = '" . $this->db->escape($data['place']) . "',
							gst_no = '" . $this->db->escape($data['gst_no']) . "',
							vendor_code = '" . $this->db->escape($data['vendor_code']) . "',
							filename = '" . $this->db->escape($data['filename']) . "', 
							mask = '" . $this->db->escape($data['mask']) . "',
							`uploaded_file_source` = '". $this->db->escape($data['uploaded_file_source']) ."',
							filename2 = '" . $this->db->escape($data['filename2']) . "', 
							mask2 = '" . $this->db->escape($data['mask2']) . "',
							`uploaded_file_source2` = '". $this->db->escape($data['uploaded_file_source2']) ."',
							filename3 = '" . $this->db->escape($data['filename3']) . "', 
							mask3 = '" . $this->db->escape($data['mask3']) . "',
							`uploaded_file_source3` = '". $this->db->escape($data['uploaded_file_source3']) ."',
							filename4 = '" . $this->db->escape($data['filename4']) . "', 
							mask4 = '" . $this->db->escape($data['mask4']) . "',
							`uploaded_file_source4` = '". $this->db->escape($data['uploaded_file_source4']) ."',
							filename5 = '" . $this->db->escape($data['filename5']) . "', 
							mask5 = '" . $this->db->escape($data['mask5']) . "', 
							isActive = '" . $this->db->escape($data['isActive']) . "'
							WHERE vendor_id = '" . (int)$vendor_id . "'");

		$this->db->query("DELETE FROM `is_vendor_contact` WHERE `vendor_id` = '".$vendor_id."' ");
		if(isset($data['contact_datas'])){
			foreach($data['contact_datas'] as $ckey => $cvalue){
				$this->db->query("INSERT INTO `is_vendor_contact` SET
						name = '" . $this->db->escape($cvalue['name']) . "',
						designation = '" . $this->db->escape($cvalue['designation']) . "',
						vendor_code = '" . $this->db->escape($cvalue['vendor_code']) . "',
						phone_no = '" . $this->db->escape($cvalue['phone_no']) . "',
						mobile1 = '" . $this->db->escape($cvalue['mobile1']) . "',
						mobile2 = '" . $this->db->escape($cvalue['mobile2']) . "',
						email_id = '" . $this->db->escape($cvalue['email_id']) . "',
						vendor_id = '" . $this->db->escape($vendor_id) . "'
						");
			}
		}

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');


		$this->db->query("INSERT INTO `supplier_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `supp_id` = '".$vendor_id."' ");
	}

	public function deleteVendor($vendor_id) {
		$this->db->query("DELETE FROM is_vendor WHERE vendor_id = '" . (int)$vendor_id . "'");
		$this->db->query("DELETE FROM is_vendor_contact WHERE vendor_id = '" . (int)$vendor_id . "'");
		//$this->cache->delete('vendor');
	}

	public function getVendor($vendor_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM is_vendor WHERE vendor_id = '" . (int)$vendor_id . "'");
		return $query->row;
	}

	public function getVendors($data = array()) {
		//echo "<pre>"; print_r($data);//exit;
		$sql = "SELECT * FROM is_vendor";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_name'])) {
			$sql .= " AND vendor_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}


		if (!empty($data['filter_vendor_id'])) {
			$sql .= " AND vendor_id = '" . $this->db->escape($data['filter_vendor_id']) . "'";
		}

		if (!empty($data['filter_vendorsort'])) {
			$sql .= " AND LCASE(vendor_name) LIKE '" . $this->db->escape($data['filter_vendorsort']) . "%'";
		}

		if (!empty($data['filter_status'])) {
			$sql .= " AND isActive = '" . $this->db->escape($data['filter_status']) . "' ";
		}
		$sort_data = array(
			'vendor_name',
			'place',
			'phone_no',
			'contact_person',
			'email_id',
			'items',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY vendor_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		$sql .= " ,vendor_id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalVendors($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM is_vendor";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_vendor_id'])) {
			$sql .= " AND vendor_id = '" . $this->db->escape($data['filter_vendor_id']) . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND vendor_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_place'])) {
			$sql .= " AND vendor_place LIKE '%" . $this->db->escape($data['filter_place']) . "%'";
		}

		if (!empty($data['filter_vendorsort'])) {
			$sql .= " AND LCASE(vendor_name) LIKE '" . $this->db->escape($data['filter_vendorsort']) . "%'";
		}

		if (!empty($data['filter_vendorsort'])) {
			$sql .= " AND LCASE(place) LIKE '" . $this->db->escape($data['filter_vendorsort']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getcontact_data($vendor_id) {
		$query = $this->db->query("SELECT * FROM `is_vendor_contact` WHERE `vendor_id` = '".$vendor_id."' ");
		return $query->rows;
	}
}
