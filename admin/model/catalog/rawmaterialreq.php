<?php
class ModelCatalogRawmaterialreq extends Model {
	public function addrawmaterialreq($data) {
		// echo'add';
		// echo'<pre>';
		// print_r($data);
		// exit;
		
		$this->db->query("INSERT INTO oc_rawmaterialreq SET 
							order_no = '" . $this->db->escape($data['hidden_order_no']) . "',
							date = '" . $this->db->escape(date('Y-m-d', strtotime($data['date']))) . "',
							narration = '" . $this->db->escape($data['narration']) . "',
							final_total = '" . $this->db->escape($data['total']) . "',
							final_value = '" . $this->db->escape($data['vals']) . "',
							final_gst_value = '" . $this->db->escape($data['gst_val']) . "'
						");
		$order_id = $this->db->getLastId();
		$this->db->query("DELETE FROM `oc_rawmaterialreqitem` WHERE `order_id` = '".$order_id."'");
		if(isset($data['productraw_datas'])){
			foreach($data['productraw_datas'] as $vkeys => $pvalues){
				
				$product_id = $pvalues['productraw_code'];
				//$productraw_name = $this->getProductName($pvalues['productraw_name']);
				$this->db->query("INSERT INTO `oc_rawmaterialreqitem` SET
					order_id = '".$order_id."',
					order_no = '" . $this->db->escape($data['hidden_order_no']) . "',
					med_id = '".$pvalues['med_id']."',
					productraw_name = '".$this->db->escape($pvalues['productraw_name'])."',
					supplier_name = '".$this->db->escape($pvalues['supplier_name'])."',
					date = '" . $this->db->escape(date('Y-m-d', strtotime($data['date']))) . "',
					product_id = '".$product_id."',
					request = '".$this->db->escape($pvalues['request'])."',
					unit = '".$this->db->escape($pvalues['unit'])."',
					auto_unit = '".$this->db->escape($pvalues['auto_unit'])."',
					packing = '".$this->db->escape($pvalues['packing'])."',
					volume = '".$this->db->escape($pvalues['volume'])."',
					quantity = '".$pvalues['quantity']."',
					ordered_qty = '".$pvalues['ordered_qty']."',
					purchase_price = '".$pvalues['purchase_price']."',
					purchase_value = '".$pvalues['purchase_value']."',
					gst_rate = '".$pvalues['gst_rate']."',
					gst_value = '".$pvalues['gst_value']."',
					total = '".$pvalues['total']."',
					inward_med_treat_quantity = '".$pvalues['current_qty']."'
				");
			}
		}

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');


		$this->db->query("INSERT INTO `indent_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `ind_id` = '".$order_id."' ");


		return $order_id;
	}

	public function editRawmaterialreq($order_id, $data) {
		// echo'edit';
		// echo'<pre>';
		// print_r($data);
		// exit;
		$this->db->query("UPDATE oc_rawmaterialreq SET 
							order_no = '" . $this->db->escape($data['hidden_order_no']) . "',
							date = '" . $this->db->escape(date('Y-m-d', strtotime($data['date']))) . "',
							narration = '" . $this->db->escape($data['narration']) . "',
							final_total = '" . $this->db->escape($data['total']) . "',
							final_value = '" . $this->db->escape($data['vals']) . "',
							final_gst_value = '" . $this->db->escape($data['gst_val']) . "'
						WHERE order_id = '" . $this->db->escape($order_id) . "'
						");	
		$this->db->query("DELETE FROM `oc_rawmaterialreqitem` WHERE `order_id` = '".$order_id."'");
		if(isset($data['productraw_datas'])){
			foreach($data['productraw_datas'] as $vkeys => $pvalues){
				$product_id = $pvalues['productraw_code'];
				$this->db->query("INSERT INTO `oc_rawmaterialreqitem` SET
					order_id = '".$order_id."',
					order_no = '" . $this->db->escape($data['hidden_order_no']) . "',
					med_id = '".$pvalues['med_id']."',
					productraw_name = '".$this->db->escape($pvalues['productraw_name'])."',
					supplier_name = '".$this->db->escape($pvalues['supplier_name'])."',
					date = '" . $this->db->escape(date('Y-m-d', strtotime($data['date']))) . "',
					product_id = '".$product_id."',
					request = '".$this->db->escape($pvalues['request'])."',
					unit = '".$this->db->escape($pvalues['unit'])."',
					auto_unit = '".$this->db->escape($pvalues['auto_unit'])."',
					packing = '".$this->db->escape($pvalues['packing'])."',
					volume = '".$this->db->escape($pvalues['volume'])."',
					quantity = '".$pvalues['quantity']."',
					ordered_qty = '".$pvalues['ordered_qty']."',
					purchase_price = '".$pvalues['purchase_price']."',
					purchase_value = '".$pvalues['purchase_value']."',
					gst_rate = '".$pvalues['gst_rate']."',
					gst_value = '".$pvalues['gst_value']."',
					total = '".$pvalues['total']."',
					inward_med_treat_quantity = '".$pvalues['current_qty']."'
				");
			}
		}

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');


		$this->db->query("INSERT INTO `indent_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `ind_id` = '".$order_id."' ");
		
	}

	public function getRawmaterialreq($order_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM oc_rawmaterialreq WHERE order_id = '" . (int)$order_id . "'");
		return $query->row;
	}

	public function getRawmaterialreqs($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_rawmaterialreq WHERE 1=1 ";
		
		if (!empty($data['filter_rawmaterialreq'])) {
			$sql .= " AND order_id LIKE '%" . $this->db->escape($data['filter_rawmaterialreq']) . "%'";
		}

		if (!empty($data['filter_order_no'])) {
			$sql .= " AND order_no LIKE '%" . $this->db->escape($data['filter_order_no']) . "%'";
		}

		if (!empty($data['filter_status'])) {
			$sql .= " AND status = '" . $this->db->escape($data['filter_status']) . "' ";
		} else {
			$sql .= " AND status = 0 ";
		}

		$sort_data = array(
			'order_no',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getTotalRawmaterialreqs($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_rawmaterialreq";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND order_id = '" . $this->db->escape($data['filter_order_id']) . "'";
		}

		if (!empty($data['filter_order_no'])) {
			$sql .= " AND order_no LIKE '%" . $this->db->escape($data['filter_order_no']) . "%'";
		}

		if (!empty($data['filter_productfinished_category'])) {
			$sql .= " AND productfinished_category LIKE '%" . $this->db->escape($data['filter_productfinished_category']) . "%'";
		}

		if (!empty($data['filter_productfinished_category_id'])) {
			$sql .= " AND productfinished_category_id = '" . $this->db->escape($data['filter_productfinished_category_id']) . "'";
		}

		if (!empty($data['filter_productsort'])) {
			$sql .= " AND UCASE(productfinished_name) LIKE '" . $this->db->escape($data['filter_productsort']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getProductnews($data = array()) {
		/*echo'<pre>';
		print_r($data);
		exit;*/
		$sql = "SELECT * FROM medicine";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_name'])) {
			$sql .= " AND med_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['med_code'])) {
			$sql .= " AND med_code LIKE '%" . $this->db->escape($data['med_code']) . "%'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo'<pre>';print_r($sql);exit;

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getProductcode($data = array()) {
		/*echo'<pre>';
		print_r($data);
		exit;*/
		$sql = "SELECT * FROM medicine";

		$sql .= " WHERE 1=1";

		if (!empty($data['med_code'])) {
			$sql .= " AND med_code = '" . $this->db->escape($data['med_code']) . "'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo'<pre>';print_r($sql);exit;

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getrequest($data = array()) {
		
		//$sql = "SELECT * FROM trainers, doctor UNION";

		$sql = "SELECT name FROM trainers WHERE name LIKE '%" . $this->db->escape($data['requested_by']) . "%' UNION ALL SELECT doctor_name FROM doctor WHERE doctor_name LIKE '%" . $this->db->escape($data['requested_by']) . "%' ";
		
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}
}
