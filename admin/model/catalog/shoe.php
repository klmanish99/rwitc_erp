<?php
class ModelCatalogShoe extends Model {
	public function addShoes($data) {
	/*echo'<pre>';
	print_r($data);
	exit;*/
		$this->db->query("INSERT INTO shoe SEt 
		shoe_name = '" . $this->db->escape($data['shoe_name']) . "',
		shoe_details ='".$this->db->escape($data['shoe_details'])."',
		type ='".$this->db->escape($data['type'])."'
		");
		$id = $this->db->getLastId();
		return $id;
	}


	public function editShoes($id,$data) {
		/*echo'<pre>';
	print_r($data);
	exit;*/
		$this->db->query("UPDATE shoe SEt 
		shoe_name = '" . $this->db->escape($data['shoe_name']) . "',
		shoe_details ='".$this->db->escape($data['shoe_details'])."',
		type ='".$this->db->escape($data['type'])."'
		WHERE id ='".$id."'
		");
	}

	

	public function deleteShoes($id) {
		$this->db->query("DELETE FROM shoe WHERE id = '" . (int)$id . "'");
	}

	public function getShoe($data = array()) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$sql = "SELECT *  FROM  shoe WHERE 1=1 ";

		if (!empty($data['filter_shoe_name'])) {
			$sql .= " AND shoe_name LIKE '" . $this->db->escape($data['filter_shoe_name']) . "%'";
		}

		if (!empty($data['filter_status'])) {
			$sql .= " AND type LIKE '" . $this->db->escape($data['filter_status']) . "%'";
		}

		/*echo'<pre>';
		print_r($sql);
		exit;*/

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	
	}

	public function getShoes($id) {  
		$sql = "SELECT *  FROM  shoe  WHERE id='".$id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}

	public function getTotalShoes($data = array()) {
		$sql = ("SELECT COUNT(*) AS total FROM shoe WHERE 1=1 ");

		if (!empty($data['filter_shoe_name'])) {
			$sql .= " AND shoe_name LIKE '" . $this->db->escape($data['filter_shoe_name']) . "%'";
		}

		if (!empty($data['filter_status'])) {
			$sql .= " AND type LIKE '" . $this->db->escape($data['filter_status']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getShoeAuto($shoe_name) {
		// echo'<pre>';
		// print_r($shoe_name);
		// exit;

		$sql =("SELECT id, shoe_name FROM  shoe");
		if($shoe_name != ''){
			$sql .= " WHERE `shoe_name` LIKE '%" . $this->db->escape($shoe_name) . "%'";
		}
		
		$sql .= " ORDER BY `id` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}

}
