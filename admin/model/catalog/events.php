<?php
class ModelCatalogEvents extends Model {
	public function addEvents($data) {
		$this->db->query("INSERT INTO events SET 
		event_name = '" . $this->db->escape($data['event_name']) . "', 
		description = '" . $this->db->escape($data['description']) . "',
		date_of_event = '" . date('Y-m-d', strtotime($this->db->escape($data['date_of_event']))) . "'
		");

		$event_id = $this->db->getLastId();

		return $event_id;
	}


	public function editEvents($event_id,$data) {
		$this->db->query("UPDATE events SET 
		event_name = '" . $this->db->escape($data['event_name']) . "', 
		description = '" . $this->db->escape($data['description']) . "',
		date_of_event = '" . date('Y-m-d', strtotime($this->db->escape($data['date_of_event']))) . "'
		WHERE event_id ='".$event_id."'
		");
	}

	public function getEvent($date) {
		$sql = "SELECT * FROM  events  WHERE date_of_event = '".$date."' ";
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getEvents($event_id) {
		$query = $this->db->query("SELECT * FROM events WHERE event_id='".$event_id."'");
		return $query->row;
	}
}
