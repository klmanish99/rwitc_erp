<?php
class ModelCatalogService extends Model {
	public function addServices($data) {
		$this->db->query("INSERT INTO service SEt 
		med_type = '" . $this->db->escape($data['med_type']) . "',
		descp ='".$this->db->escape($data['descp'])."',
		limit1 ='".$this->db->escape($data['limit1'])."',
		limit2 ='".$this->db->escape($data['limit2'])."',
		sc ='".$this->db->escape($data['sc'])."',
		isActive ='".$this->db->escape($data['isActive'])."'
		");
		$id = $this->db->getLastId();

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');

		$this->db->query("INSERT INTO `service_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `serv_id` = '".$id."' ");

		return $id;
	}


	public function editServices($id,$data) {
		$this->db->query("UPDATE service SEt 
		med_type = '" . $this->db->escape($data['med_type']) . "',
		descp ='".$this->db->escape($data['descp'])."',
		limit1 ='".$this->db->escape($data['limit1'])."',
		limit2 ='".$this->db->escape($data['limit2'])."',
		sc ='".$this->db->escape($data['sc'])."',
		isActive ='".$this->db->escape($data['isActive'])."'
		WHERE id ='".$id."'
		");

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');

		$this->db->query("INSERT INTO `service_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `serv_id` = '".$id."' ");
	}

	

	public function deleteServices($id) {
		$this->db->query("DELETE FROM service WHERE id = '" . (int)$id . "'");
	}

	public function getService($data = array()) {
		$sql = "SELECT *  FROM  service  WHERE 1=1 ";

		if (!empty($data['filter_medicine_type'])) {
			$sql .= " AND med_type LIKE '" . $this->db->escape($data['filter_medicine_type']) . "%'";
		}

		if (isset($data['filter_status'])) {
			$sql .= " AND isActive = '" . $this->db->escape($data['filter_status']) . "' ";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		//echo $sql;exit;
		$query = $this->db->query($sql)->rows;
		return $query;
	
	}

	public function getServices($id) {  
		$sql = "SELECT *  FROM  service  WHERE id='".$id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}

	public function getTotalCategories() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM service");

		return $query->row['total'];
	}

	public function getServiceAuto($med_type) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT med_type,id FROM  service WHERE 1 = 1");
		if($med_type != ''){
			$sql .= " AND `med_type` LIKE '%" . $this->db->escape($med_type) . "%'";
		}
		
		$sql .= " ORDER BY `id` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}	
}
