<?php
class ModelCatalogTrainerStaff extends Model {
	public function addTrainerStaff($data) {
		// echo "<pre>";
		// print_r($data);
		// exit;
		// if ($data['doctor_name'] == $data['parent']) {
		// 	$isparent = 1;
		// } else{
		// 	$isparent = 0;
		// }

		$this->db->query("INSERT INTO trainer_staff_entry SEt 
		staff = '" . $this->db->escape($data['staff_name']) . "',
		file_number_upload = '" . $this->db->escape($data['file_number_upload']) . "',
		uploaded_file_source = '" . $this->db->escape($data['uploaded_file_source']) . "',
		adhar_no ='".$this->db->escape($data['adhar_number'])."',
		designation ='".$this->db->escape($data['designation'])."',
		address ='".$this->db->escape($data['address'])."',
		blood_group ='".$this->db->escape($data['blood_group'])."',
		trainer_name ='".$this->db->escape($data['trainer_name'])."',
		trainer_id ='".$this->db->escape($data['trainer_id'])."'
		");
		$id = $this->db->getLastId();
		return $id;
	}


	public function editTrainerStaff($id,$data) {
		if ($data['doctor_name'] == $data['parent']) {
			$isparent = 1;
		} else{
			$isparent = 0;
		}

		$this->db->query("UPDATE trainer_staff_entry SET 
		staff = '" . $this->db->escape($data['staff_name']) . "',
		file_number_upload = '" . $this->db->escape($data['file_number_upload']) . "',
		uploaded_file_source = '" . $this->db->escape($data['uploaded_file_source']) . "',
		adhar_no ='".$this->db->escape($data['adhar_number'])."',
		designation ='".$this->db->escape($data['designation'])."',
		address ='".$this->db->escape($data['address'])."',
		blood_group ='".$this->db->escape($data['blood_group'])."',
		trainer_name ='".$this->db->escape($data['trainer_name'])."',
		trainer_id ='".$this->db->escape($data['trainer_id'])."'
		WHERE id ='".$id."'
		");
	}

	

	public function deleteTrainerStaff($id) {
		$this->db->query("DELETE FROM doctor WHERE id = '" . (int)$id . "'");
	}

	public function getTrainerStaffs($data = array()) {
		$sql = "SELECT *  FROM  trainer_staff_entry  WHERE 1=1 ";

		if (!empty($data['filter_doctor_name'])) {
			$sql .= " AND staff LIKE '" . $this->db->escape($data['filter_doctor_name']) . "%'";
		}

		if (!empty($data['filter_trainer_name'])) {
			$sql .= " AND trainer_name LIKE '" . $this->db->escape($data['filter_trainer_name']) . "%'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		/*echo'<pre>';
		print_r($sql);
		exit;*/
		$query = $this->db->query($sql)->rows;
		return $query;
	
	}

	public function getTrainerStaff($id) {  
		$sql = "SELECT *  FROM  trainer_staff_entry  WHERE id='".$id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}

	public function getTotalTrainerStaff() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM trainer_staff_entry");

		if (!empty($data['filter_doctor_name'])) {
			$sql .= " AND staff LIKE '" . $this->db->escape($data['filter_doctor_name']) . "%'";
		}

		if (!empty($data['filter_trainer_name'])) {
			$sql .= " AND trainer_name LIKE '" . $this->db->escape($data['filter_trainer_name']) . "%'";
		}
		
		return $query->row['total'];
	}

	public function getDoctorAuto($doctor_name) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT doctor_name,doctor_code,id FROM  doctor WHERE 1 = 1");
		if($doctor_name != ''){
			$sql .= " AND `doctor_name` LIKE '%" . $this->db->escape($doctor_name) . "%'";
		}
		
		$sql .= " ORDER BY `doctor_code` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getDoctorAutos($doctor_code) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT doctor_name,doctor_code,id FROM  doctor WHERE 1 = 1");
		if($doctor_code != ''){
			$sql .= " AND `doctor_code` LIKE '%" . $this->db->escape($doctor_code) . "%'";
		}
		
		$sql .= " ORDER BY `doctor_code` ";

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getsupplier($data = array()) {
		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM trainers";
			
		$sql .= " WHERE 1=1";
		
		if (!empty($data['filter_parent'])) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_parent']) . "%'";
		}

		$sort_data = array(
			'id',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo "<pre>";print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}
}
