<?php
class ModelCatalogMedicine extends Model {
	public function addMedicines($data) {
	// echo'<pre>';
	// print_r($data);
	// exit;
		$this->db->query("INSERT INTO medicine SEt 
		med_code = '" . $this->db->escape($data['med_code']) . "',
		alpha = '" . $this->db->escape($data['alpha']) . "',
		alpha_no = '" . $this->db->escape($data['alpha_no']) . "',
		med_name ='".$this->db->escape($data['med_name'])."',
		med_type ='".$this->db->escape($data['med_type'])."',
		unit_cost ='".$this->db->escape($data['unit_cost'])."',
		normal ='".$this->db->escape($data['normal'])."',
		store_unit ='".$this->db->escape($data['store_unit'])."',
		brand ='".$this->db->escape($data['brand'])."',
		isActive ='".$this->db->escape($data['isActive'])."',
		reorder_lvl ='".$this->db->escape($data['reorder_lvl'])."',
		reorder_qty ='".$this->db->escape($data['reorder_qty'])."',
		pack_type ='".$this->db->escape($data['pack'])."',
		volume ='".$this->db->escape($data['volume'])."',
		unit ='".$this->db->escape($data['unit'])."',
		gst_rate ='".$this->db->escape($data['gst'])."',
		purchase_price='".$this->db->escape($data['pur_price'])."'
		");
		
		$id = $this->db->getLastId();

		$this->db->query("INSERT INTO `medicine_stock_status` SET med_id = '".$this->db->escape($id)."' ,med_code = '".$this->db->escape($data['med_code'])."',med_name = '".$this->db->escape($data['med_name'])."',med_status = '".$this->db->escape($data['isActive'])."',stock_status = '1' ");

		$this->db->query("DELETE FROM `doctor_cost` WHERE `id` = '".$id."'");
		if(isset($data['final'])){
			foreach($data['final'] as $vkeys => $pfinal){
				
				$this->db->query("INSERT INTO `doctor_cost` SET
					medicine_id = '".$id."',
					doctor_id = '".$pfinal['doctor_id']."',
					cost = '".$pfinal['cost']."'
				");
			}
		}

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');


		$this->db->query("INSERT INTO `medicine_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `med_id` = '".$id."' ");
		return $id;
	}


	public function editMedicines($id,$data) {
	// 	echo'<pre>';
	//  print_r($data);
	// exit;
		$this->db->query("UPDATE medicine SET
		med_code = '" . $this->db->escape($data['med_code']) . "',
		alpha = '" . $this->db->escape($data['alpha']) . "',
		alpha_no = '" . $this->db->escape($data['alpha_no']) . "',
		med_name ='".$this->db->escape($data['med_name'])."',
		med_type ='".$this->db->escape($data['med_type'])."',
		unit_cost ='".$this->db->escape($data['unit_cost'])."',
		cost_a1 ='".$this->db->escape($data['cost_a1'])."',
		cost_a2 ='".$this->db->escape($data['cost_a2'])."',
		cost_a3 ='".$this->db->escape($data['cost_a3'])."',
		cost_a4 ='".$this->db->escape($data['cost_a4'])."',
		cost_a5 ='".$this->db->escape($data['cost_a5'])."',
		cost_a6 ='".$this->db->escape($data['cost_a6'])."',
		cost_a7 ='".$this->db->escape($data['cost_a7'])."',
		cost_a8 ='".$this->db->escape($data['cost_a8'])."',
		cost_a9 ='".$this->db->escape($data['cost_a9'])."',
		cost_a10 ='".$this->db->escape($data['cost_a10'])."',
		normal ='".$this->db->escape($data['normal'])."',
		store_unit ='".$this->db->escape($data['store_unit'])."',
		brand ='".$this->db->escape($data['brand'])."',
		isActive ='".$this->db->escape($data['isActive'])."',
		reorder_lvl ='".$this->db->escape($data['reorder_lvl'])."',
		reorder_qty ='".$this->db->escape($data['reorder_qty'])."',
		pack_type ='".$this->db->escape($data['pack'])."',
		volume ='".$this->db->escape($data['volume'])."',
		unit ='".$this->db->escape($data['unit'])."',
		gst_rate ='".$this->db->escape($data['gst'])."',
		purchase_price='".$this->db->escape($data['pur_price'])."'
		WHERE id ='".$id."'
		");

		$this->db->query("UPDATE medicine_stock_status SET med_code = '".$this->db->escape($data['med_code'])."',med_name = '".$this->db->escape($data['med_name'])."',med_status = '".$this->db->escape($data['isActive'])."',stock_status = '1' WHERE id ='".$id."' ");

		$this->db->query("DELETE FROM `doctor_cost` WHERE `medicine_id` = '".$id."'");
		if(isset($data['final'])){
			foreach($data['final'] as $vkeys => $pfinal){
				
				$this->db->query("INSERT INTO `doctor_cost` SET
					medicine_id = '".$id."',
					doctor_id = '".$pfinal['doctor_id']."',
					cost = '".$pfinal['cost']."'
				");
			}
		}

		$user_datas = $this->db->query("SELECT `firstname`, `lastname` FROM oc_user WHERE `user_id` = '".$data['user_log_id']."' ");
		if ($user_datas->num_rows > 0) {
			$fname = $user_datas->row['firstname'];
			$lname = $user_datas->row['lastname'];
		} else {
			$fname = '';
			$lname = '';
		}

		$date = date('Y-m-d');
		$time = date('h:i:sa');


		$this->db->query("INSERT INTO `medicine_logs` SET `group_id` = '".$data['user_log_grp_id']."', `log_id` = '".$data['user_log_id']."', `fname` = '".$fname."', `lname` = '".$lname."', `log_date` = '".$date."', `log_time` = '".$time."', `med_id` = '".$id."' ");
	}

	

	public function deleteMedicines($id) {
		$this->db->query("DELETE FROM medicine WHERE id = '" . (int)$id . "'");
	}

	public function getMedicine($data = array()) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$sql = "SELECT  *  FROM  medicine  WHERE 1=1 ";

		if (!empty($data['filter_medicine_name'])) {
			$sql .= " AND med_name LIKE '" . $this->db->escape($data['filter_medicine_name']) . "%'";
		}

		if (!empty($data['filter_medicine_id'])) {
			$sql .= " AND med_code = '" . $this->db->escape($data['filter_medicine_id']) . "' ";
		}

		if (isset($data['filter_status'])) {
			$sql .= " AND isActive = '" . $this->db->escape($data['filter_status']) . "' ";
		}
		$sql .= "GROUP BY med_code";
		$sql .= " ORDER BY med_name ASC ";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql)->rows;
		return $query;
	
	}

	public function getMedicines($id) {  
		$sql = "SELECT *  FROM  medicine  WHERE id='".$id."'";
		$query = $this->db->query($sql)->row;
		return $query;
	}

	public function getTotalMedicines() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM medicine");

		return $query->row['total'];
	}

	public function getMedicineAuto($data = array()) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT * FROM  medicine WHERE 1 = 1");
		/*if($med_name != ''){
			$sql .= " AND `med_name` LIKE '%" . $this->db->escape($med_name) . "%'";
		}*/

		if (!empty($data['filter_medicine_name'])) {
			$sql .= " AND `med_name` LIKE '%" . $this->db->escape($data['filter_medicine_name']) . "%'";
		}

		if (!empty($data['filter_item_code'])) {
			$sql .= " AND `med_code` LIKE '%" . $this->db->escape($data['filter_item_code']) . "%'";
		}
		
		$sql .= " ORDER BY `med_code`  ASC LIMIT 0, 5 ";//echo $sql;exit;

		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function getMedicineAutos($med_code) {
		// echo'<pre>';
		// print_r($to_owner);
		// exit;

		$sql =("SELECT med_name,med_code,id FROM  medicine WHERE 1 = 1");
		if($med_code != ''){
			$sql .= " AND `med_code` LIKE '%" . $this->db->escape($med_code) . "%'";
		}

		$sql .= " GROUP BY `med_code` ";
		$sql .= " ORDER BY `med_code`  ASC LIMIT 0, 5 ";
		$query = $this->db->query($sql)->rows;
		return $query;
	}
}
