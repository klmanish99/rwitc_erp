<?php
class ModelUserUserGroup extends Model {
	public function addUserGroup($data) {

		$permission_datas = array();
		if(isset($data['permission']['access'])){
			$permission_datas['access'] = $data['permission']['access'];
		} else {
			$permission_datas['access'] = array();
		}
		if(isset($data['permission']['modify'])){
			$permission_datas['modify'] = $data['permission']['modify'];
		} else {
			$permission_datas['modify'] = array();
		}	
		$permission_datas['access'][] = 'catalog/table';
		if($user_group_id == 1){
			//$permission_datas['access'][] = 'user/user';
			//$permission_datas['access'][] = 'user/user_permission';
			$permission_datas['access'][] = 'tool/error_log';
			$permission_datas['access'][] = 'common/home';

			//$permission_datas['modify'][] = 'user/user';
			//$permission_datas['modify'][] = 'user/user_permission';
			$permission_datas['modify'][] = 'tool/error_log';
			$permission_datas['modify'][] = 'common/home';
		}
		$permission_datas['modify'][] = 'catalog/racetype';
		$permission_datas['modify'][] = 'catalog/owner';
		$permission_datas['modify'][] = 'catalog/trainer';
		$permission_datas['modify'][] = 'catalog/horse';
		$permission_datas['modify'][] = 'catalog/jockey';
		$permission_datas['modify'][] = 'catalog/equipments';
		$permission_datas['modify'][] = 'catalog/color';
		$permission_datas['modify'][] = 'catalog/vendor';
		$permission_datas['modify'][] = 'catalog/medicine';
		$permission_datas['modify'][] = 'catalog/doctor';
		$permission_datas['modify'][] = 'catalog/trainer_staff';
		$permission_datas['modify'][] = 'catalog/service';
		$permission_datas['modify'][] = 'catalog/rawmaterialreq';
		$permission_datas['modify'][] = 'catalog/inward';
		$permission_datas['modify'][] = 'catalog/isb_horse';
		$permission_datas['modify'][] = 'catalog/shoe';
		$permission_datas['modify'][] = 'catalog/bit';
		$permission_datas['modify'][] = 'catalog/horse_at_glance';
		$permission_datas['modify'][] = 'catalog/owner_at_glance';
		$permission_datas['modify'][] = 'catalog/trainer_at_glance';
		$permission_datas['modify'][] = 'catalog/jockey_at_glance';
		$permission_datas['modify'][] = 'catalog/import_list';
		$permission_datas['modify'][] = 'catalog/medicine_transfer';
		$permission_datas['modify'][] = 'catalog/medicine_return';
		$permission_datas['modify'][] = 'catalog/treatment_entry';
		$permission_datas['modify'][] = 'catalog/treatment_entry_single';
		$permission_datas['modify'][] = 'catalog/assign_trainer';
		$permission_datas['modify'][] = 'report/store_reconsilation';

		$permission_datas['modify'][] = 'transaction/stacks';
		$permission_datas['modify'][] = 'transaction/class_entry';
		$permission_datas['modify'][] = 'transaction/prospectus';
		$permission_datas['modify'][] = 'transaction/entries';
		$permission_datas['modify'][] = 'transaction/handicapping';
		$permission_datas['modify'][] = 'transaction/acceptance';
		$permission_datas['modify'][] = 'transaction/ownership_shift_module';
		$permission_datas['modify'][] = 'transaction/cancel_ownership';
		$permission_datas['modify'][] = 'transaction/amount_tran';
		$permission_datas['modify'][] = 'transaction/horse_datas_link';
		$permission_datas['modify'][] = 'transaction/regiteration_module';
		$permission_datas['modify'][] = 'transaction/arrival_charges';
		$permission_datas['modify'][] = 'transaction/multi_arrival_charges';
		$permission_datas['modify'][] = 'transaction/send_mail_charges';
		$permission_datas['modify'][] = 'transaction/multi_horse_transfer';
		$permission_datas['modify'][] = 'transaction/jockey_ban';
		$permission_datas['modify'][] = 'transaction/trainer_ban';


		$permission_datas['modify'][] = 'report/report_horse_undertaking_charge';
		$permission_datas['modify'][] = 'report/inward_po_report';
		$permission_datas['modify'][] = 'report/cancel_own';
		$permission_datas['modify'][] = 'report/arrival_charge_report';
		$permission_datas['modify'][] = 'report/ownership_registration_report';
		$permission_datas['modify'][] = 'report/indent_po_report';
		$permission_datas['modify'][] = 'report/name_change_report';
		$permission_datas['modify'][] = 'report/stock_report';
		$permission_datas['modify'][] = 'report/med_transfer_report';
		$permission_datas['modify'][] = 'report/medicine_report';
		$permission_datas['modify'][] = 'report/medicine_transaction';
		$permission_datas['modify'][] = 'report/horse_treatment_report';
		$permission_datas['modify'][] = 'report/horsewise_treat_entry';
		$permission_datas['modify'][] = 'report/monthly_medicine_transaction';
		$permission_datas['modify'][] = 'report/trainer_license_report';
		$permission_datas['modify'][] = 'report/jockey_license_report';
		$permission_datas['modify'][] = 'transaction/jockey_lisence_trans';
		$permission_datas['modify'][] = 'transaction/trainer_license_trans';
		$permission_datas['modify'][] = 'report/reconsilation_report';


		$permission_datas['modify'][] = 'utility/import_horse';
		$permission_datas['modify'][] = 'utility/import_tally_po';
		$permission_datas['modify'][] = 'utility/is_end';
		$permission_datas['modify'][] = 'utility/is_end_j';
		$permission_datas['modify'][] = 'snippet/import_indent';
		$permission_datas['modify'][] = 'localisation/country';


		
		$this->db->query("INSERT INTO " . DB_PREFIX . "user_group SET name = '" . $this->db->escape($data['name']) . "', permission = '" . (isset($permission_datas) ? $this->db->escape(json_encode($permission_datas)) : '') . "'");
	
		return $this->db->getLastId();
	}

	public function editUserGroup($user_group_id, $data) {


		$permission_datas = array();
		if(isset($data['permission']['access'])){
			$permission_datas['access'] = $data['permission']['access'];
		} else {
			$permission_datas['access'] = array();
		}
		if(isset($data['permission']['modify'])){
			$permission_datas['modify'] = $data['permission']['modify'];
		} else {
			$permission_datas['modify'] = array();
		}	
		$permission_datas['access'][] = 'catalog/table';
		if($user_group_id == 1){
			$permission_datas['access'][] = 'user/user';
			$permission_datas['access'][] = 'user/user_permission';
			$permission_datas['access'][] = 'tool/error_log';
			$permission_datas['access'][] = 'common/home';

			$permission_datas['modify'][] = 'user/user';
			$permission_datas['modify'][] = 'user/user_permission';
			$permission_datas['modify'][] = 'tool/error_log';
			$permission_datas['modify'][] = 'common/home';
		}
		$permission_datas['modify'][] = 'catalog/racetype';
		$permission_datas['modify'][] = 'catalog/owner';
		$permission_datas['modify'][] = 'catalog/trainer';
		$permission_datas['modify'][] = 'catalog/horse';
		$permission_datas['modify'][] = 'catalog/jockey';
		$permission_datas['modify'][] = 'catalog/equipments';
		$permission_datas['modify'][] = 'catalog/color';
		$permission_datas['modify'][] = 'catalog/vendor';
		$permission_datas['modify'][] = 'catalog/medicine';
		$permission_datas['modify'][] = 'catalog/doctor';
		$permission_datas['modify'][] = 'catalog/trainer_staff';
		$permission_datas['modify'][] = 'catalog/service';
		$permission_datas['modify'][] = 'catalog/rawmaterialreq';
		$permission_datas['modify'][] = 'catalog/inward';
		$permission_datas['modify'][] = 'catalog/isb_horse';
		$permission_datas['modify'][] = 'catalog/shoe';
		$permission_datas['modify'][] = 'catalog/bit';
		$permission_datas['modify'][] = 'catalog/horse_at_glance';
		$permission_datas['modify'][] = 'catalog/owner_at_glance';
		$permission_datas['modify'][] = 'catalog/trainer_at_glance';
		$permission_datas['modify'][] = 'catalog/jockey_at_glance';
		$permission_datas['modify'][] = 'catalog/import_list';
		$permission_datas['modify'][] = 'catalog/medicine_transfer';
		$permission_datas['modify'][] = 'catalog/medicine_return';
		$permission_datas['modify'][] = 'catalog/treatment_entry';
		$permission_datas['modify'][] = 'catalog/treatment_entry_single';
		$permission_datas['modify'][] = 'catalog/assign_trainer';
		$permission_datas['modify'][] = 'report/store_reconsilation';

		$permission_datas['modify'][] = 'transaction/stacks';
		$permission_datas['modify'][] = 'transaction/class_entry';
		$permission_datas['modify'][] = 'transaction/prospectus';
		$permission_datas['modify'][] = 'transaction/entries';
		$permission_datas['modify'][] = 'transaction/handicapping';
		$permission_datas['modify'][] = 'transaction/acceptance';
		$permission_datas['modify'][] = 'transaction/ownership_shift_module';
		$permission_datas['modify'][] = 'transaction/cancel_ownership';
		$permission_datas['modify'][] = 'transaction/amount_tran';
		$permission_datas['modify'][] = 'transaction/horse_datas_link';
		$permission_datas['modify'][] = 'transaction/regiteration_module';
		$permission_datas['modify'][] = 'transaction/arrival_charges';
		$permission_datas['modify'][] = 'transaction/multi_arrival_charges';
		$permission_datas['modify'][] = 'transaction/send_mail_charges';
		$permission_datas['modify'][] = 'transaction/multi_horse_transfer';
		$permission_datas['modify'][] = 'transaction/jockey_ban';
		$permission_datas['modify'][] = 'transaction/trainer_ban';


		$permission_datas['modify'][] = 'report/ownership_registration_report';
		$permission_datas['modify'][] = 'report/indent_po_report';
		$permission_datas['modify'][] = 'report/name_change_report';
		$permission_datas['modify'][] = 'report/report_horse_undertaking_charge';
		$permission_datas['modify'][] = 'report/inward_po_report';
		$permission_datas['modify'][] = 'report/cancel_own';
		$permission_datas['modify'][] = 'report/arrival_charge_report';
		$permission_datas['modify'][] = 'report/stock_report';
		$permission_datas['modify'][] = 'report/medicine_transaction';
		$permission_datas['modify'][] = 'report/trainer_license_report';
		$permission_datas['modify'][] = 'report/jockey_license_report';
		$permission_datas['modify'][] = 'report/horse_treatment_report';
		$permission_datas['modify'][] = 'report/horsewise_treat_entry';
		$permission_datas['modify'][] = 'report/Monthly Medicine Transaction';
		$permission_datas['modify'][] = 'transaction/jockey_lisence_trans';
		$permission_datas['modify'][] = 'transaction/trainer_license_trans';
		$permission_datas['modify'][] = 'report/reconsilation_report';
		

		$permission_datas['modify'][] = 'utility/import_horse';
		$permission_datas['modify'][] = 'utility/import_tally_po';
		$permission_datas['modify'][] = 'utility/is_end';
		$permission_datas['modify'][] = 'utility/is_end_j';
		$permission_datas['modify'][] = 'snippet/import_indent';
		$permission_datas['modify'][] = 'localisation/country';
		

		$this->db->query("UPDATE " . DB_PREFIX . "user_group SET name = '" . $this->db->escape($data['name']) . "', permission = '" . (isset($permission_datas) ? $this->db->escape(json_encode($permission_datas)) : '') . "' WHERE user_group_id = '" . (int)$user_group_id . "'");
	}

	public function deleteUserGroup($user_group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");
	}

	public function getUserGroup($user_group_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

		$user_group = array(
			'name'       => $query->row['name'],
			'permission' => json_decode($query->row['permission'], true)
		);

		return $user_group;
	}

	public function getUserGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "user_group";

		$sql .= " ORDER BY name";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUserGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user_group");

		return $query->row['total'];
	}

	public function addPermission($user_group_id, $type, $route) {
		$user_group_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

		if ($user_group_query->num_rows) {
			$data = json_decode($user_group_query->row['permission'], true);

			$data[$type][] = $route;

			$this->db->query("UPDATE " . DB_PREFIX . "user_group SET permission = '" . $this->db->escape(json_encode($data)) . "' WHERE user_group_id = '" . (int)$user_group_id . "'");
		}
	}

	public function removePermission($user_group_id, $type, $route) {
		$user_group_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

		if ($user_group_query->num_rows) {
			$data = json_decode($user_group_query->row['permission'], true);

			$data[$type] = array_diff($data[$type], array($route));

			$this->db->query("UPDATE " . DB_PREFIX . "user_group SET permission = '" . $this->db->escape(json_encode($data)) . "' WHERE user_group_id = '" . (int)$user_group_id . "'");
		}
	}
}